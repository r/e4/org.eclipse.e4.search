/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.compatibility.internal.contributors.impl;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.compatibility.internal.contributors.impl.searchprovider.CompatibilitySearchProvider;
import org.eclipse.platform.discovery.compatibility.tests.internal.fixture.SampleSearchPage;
import org.eclipse.platform.discovery.compatibility.tests.util.testcases.DynamicRegistryContributorTest;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.ISearchProviderDescription;
import org.eclipse.platform.discovery.runtime.internal.xp.IContributionsReader;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.SearchProvidersExtensionParser;

public class DynamicSearchProviderContributorTest extends DynamicRegistryContributorTest<ISearchProviderDescription> {

	@Override
	protected void verifyContribution(ISearchProviderDescription contribution) {
		assertFalse("compatibiliity search providers do not support text search", contribution.supportsTextSearch());
		assertEquals("unexpected object type id", new IdCalculator().calculateObjectTypeId(SampleSearchPage.ID), contribution.getObjectType().getId());
		assertTrue("compatibility search providers do not support destination categories", contribution.getSupportedDestinationCategories().isEmpty());
		assertTrue("unexpecred search provider class", contribution.createInstance().getClass().equals(CompatibilitySearchProvider.class));
	}

	@Override
	protected IContributionsReader<ISearchProviderDescription> getParser(IExtensionRegistry registry) {
		return new SearchProvidersExtensionParser(registry);
	}

	@Override
	protected String expectedContributionId() {
		return new IdCalculator().calculateSearchProviderId(SampleSearchPage.ID);
	}

}
