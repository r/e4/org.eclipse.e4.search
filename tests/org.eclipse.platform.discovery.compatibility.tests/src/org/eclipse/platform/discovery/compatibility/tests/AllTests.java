/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.compatibility.tests;

import org.eclipse.platform.discovery.compatibility.internal.contributors.impl.DynamicObjectTypeContributorTest;
import org.eclipse.platform.discovery.compatibility.internal.contributors.impl.DynamicSearchProviderContributorTest;
import org.eclipse.platform.discovery.compatibility.internal.readers.impl.ExceptionThrowingSearchPageTest;
import org.eclipse.platform.discovery.compatibility.internal.readers.impl.SearchPageParserTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
@SuiteClasses({
	DynamicObjectTypeContributorTest.class,
	DynamicSearchProviderContributorTest.class,
	SearchPageParserTest.class,
	ExceptionThrowingSearchPageTest.class
})
@RunWith(Suite.class)
public class AllTests {
}
