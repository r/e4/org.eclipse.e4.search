/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.compatibility.tests.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.platform.discovery.compatibility.internal.readers.impl.SearchPageParser;
import org.eclipse.platform.discovery.testutils.utils.registry.ExtensionRegistryBuilder;
import org.eclipse.platform.discovery.util.internal.StatusUtils;
import org.eclipse.search.ui.ISearchPage;
import org.mockito.Mockito;

public class CompatibilityRegistryBuilder extends ExtensionRegistryBuilder {
	
	private IExtensionPoint searchPagesExtensionPoint;
	private List<IConfigurationElement> searchPageElements = new ArrayList<IConfigurationElement>();

	public CompatibilityRegistryBuilder() {
		super();
		searchPagesExtensionPoint = setupExtensionPoint(SearchPageParser.SEARCH_PAGES_XP_ID);
	}
	
	@Override
	protected void setupExtensionPointConfigElements() {
		super.setupExtensionPointConfigElements();
		Mockito.when(searchPagesExtensionPoint.getConfigurationElements()).thenReturn(searchPageElements.toArray(new IConfigurationElement[]{}));
	}
	
	@Override
	protected void clearState() {
		searchPageElements.clear();
		super.clearState();
	}
	
	public void addSearchPage(String id, String displayname, ISearchPage page) {
		IConfigurationElement pageElement = searchPageElement(id, displayname);
		setExecutableExtension(pageElement, page, SearchPageParser.CLASS);
		searchPageElements.add(pageElement);
	}

	private IConfigurationElement searchPageElement(String id,
			String displayname) {
		IConfigurationElement pageElement = createConfigurationElement(SearchPageParser.PAGE_ELEMENT);
		setAttribute(pageElement, SearchPageParser.ID, id);
		setAttribute(pageElement, SearchPageParser.LABEL, displayname);
		return pageElement;
	}

	public void addExceptionThrowingSearchPage(String id, String displayname) {
		addExceptionThrowingSearchPage(id, displayname, new CoreException(StatusUtils.statusError("test")));
	}
	
	public void addExceptionThrowingSearchPage(String id, String displayname, CoreException toThrow) {
		IConfigurationElement pageElement = searchPageElement(id, displayname);
		setupCreateExecutableExtensionToThrowException(pageElement, SearchPageParser.CLASS, toThrow);
		searchPageElements.add(pageElement);
	}

}
