/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.compatibility.internal.readers.impl;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.compatibility.internal.readers.ISearchPageDescription;
import org.eclipse.platform.discovery.compatibility.tests.util.CompatibilityParserTest;
import org.eclipse.platform.discovery.compatibility.tests.util.CompatibilityRegistryBuilder;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.AbstractExtensionPointParser;
import org.eclipse.platform.discovery.util.internal.StatusUtils;
import static org.junit.Assert.*;

public class ExceptionThrowingSearchPageTest extends CompatibilityParserTest<ISearchPageDescription>{

	private CoreException expectedCause = new CoreException(StatusUtils.statusError("foo")); 
	@Override
	protected AbstractExtensionPointParser<ISearchPageDescription> createParser(IExtensionRegistry registry) {
		return new SearchPageParser(registry);
	}

	@Override
	protected void setupRegistry(CompatibilityRegistryBuilder registryBuilder) {
		registryBuilder.addExceptionThrowingSearchPage("foo", "bar", expectedCause);
	}

	@Override
	protected void verifyContributions(List<ISearchPageDescription> contributions) {
		try{
			contributions.get(0).getSearchPage();
			fail("illegalstate not thrown");
		}catch (IllegalStateException e) {
			assertEquals("unexpected cause", expectedCause, e.getCause());
		}
	}

}
