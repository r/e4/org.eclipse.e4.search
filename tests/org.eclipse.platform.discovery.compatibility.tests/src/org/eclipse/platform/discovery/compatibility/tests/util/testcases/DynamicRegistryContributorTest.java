/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.compatibility.tests.util.testcases;

import java.text.MessageFormat;

import junit.framework.TestCase;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.platform.discovery.compatibility.internal.DiscoveryCompatibilityPlugin;
import org.eclipse.platform.discovery.runtime.api.IDescriptiveObject;
import org.eclipse.platform.discovery.runtime.internal.xp.IContributionsReader;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleException;

/**
 * @param <T> the type of data which is expected to be contributed
 */
public abstract class DynamicRegistryContributorTest<T extends IDescriptiveObject> extends TestCase {
	
	@Override
	protected void setUp() throws Exception {
		
		super.setUp();
		startCompatibilityIfNotStarted();
	}

	private void startCompatibilityIfNotStarted() throws BundleException {
		startBundleIfNotStarted(DiscoveryCompatibilityPlugin.SYMBOLIC_NAME);
		
	}

	private void startBundleIfNotStarted(String bundleName) throws BundleException {
		Bundle runtimeBundle = Platform.getBundle(bundleName);
		if(runtimeBundle.getState() != Bundle.ACTIVE) {
			runtimeBundle.start();
		}
	}
	
	public void testContribution() {
		String expectedId = expectedContributionId();
		for(T contribution: getParser(Platform.getExtensionRegistry()).readContributions()) {
			if(expectedId.equals(contribution.getId())) {
				verifyContribution(contribution);
				return;
			}
		}
		fail(MessageFormat.format("Contribution with id {0} not found in extension registry", expectedId));
	}

	protected abstract void verifyContribution(T contribution);
	
	protected abstract IContributionsReader<T> getParser(IExtensionRegistry registry);
	
	protected abstract String expectedContributionId();

}
