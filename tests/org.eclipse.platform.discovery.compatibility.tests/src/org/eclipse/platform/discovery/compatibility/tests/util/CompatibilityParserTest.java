/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.compatibility.tests.util;

import org.eclipse.platform.discovery.testutils.utils.testcases.GenericExtensionPointParserTest;

public abstract class CompatibilityParserTest<T> extends GenericExtensionPointParserTest<T, CompatibilityRegistryBuilder>{
	@Override
	protected CompatibilityRegistryBuilder createRegistryBuilder() {
		return new CompatibilityRegistryBuilder();
	}
}
