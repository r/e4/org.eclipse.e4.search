/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.compatibility.internal.readers.impl;

import static org.eclipse.platform.discovery.compatibility.internal.contributors.impl.DescriptiveObjectConstants.ID_ONE;
import static org.eclipse.platform.discovery.compatibility.internal.contributors.impl.DescriptiveObjectConstants.ID_TWO;
import static org.eclipse.platform.discovery.compatibility.internal.contributors.impl.DescriptiveObjectConstants.LABEL_ONE;
import static org.eclipse.platform.discovery.compatibility.internal.contributors.impl.DescriptiveObjectConstants.LABEL_TWO;

import java.util.List;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.compatibility.internal.readers.ISearchPageDescription;
import org.eclipse.platform.discovery.compatibility.tests.util.CompatibilityParserTest;
import org.eclipse.platform.discovery.compatibility.tests.util.CompatibilityRegistryBuilder;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.AbstractExtensionPointParser;
import org.eclipse.search.ui.ISearchPage;
import org.mockito.Mockito;
import static org.junit.Assert.*;

public class SearchPageParserTest extends CompatibilityParserTest<ISearchPageDescription> {

	private ISearchPage page1;
	private ISearchPage page2;
	@Override
	protected AbstractExtensionPointParser<ISearchPageDescription> createParser(IExtensionRegistry registry) {
		return new SearchPageParser(registry);
	}

	@Override
	protected void setupRegistry(CompatibilityRegistryBuilder registryBuilder) {
		page1 = Mockito.mock(ISearchPage.class);
		page2 = null;
		registryBuilder.addSearchPage(ID_ONE, LABEL_ONE, page1);
		registryBuilder.addSearchPage(ID_TWO, LABEL_TWO, page2);
	}

	@Override
	protected void verifyContributions(List<ISearchPageDescription> contributions) {
		assertEquals("Two contributors expected", 2, contributions.size());
		
		verifySearchPage(contributions.get(0), ID_ONE, LABEL_ONE, page1);
		verifySearchPage(contributions.get(1), ID_TWO, LABEL_TWO, page2);
	}

	private void verifySearchPage(ISearchPageDescription page, String expectedId, String expectedDisplayname, ISearchPage expectedSearchPage) {
		assertEquals("unexpected id", expectedId, page.getId());
		assertEquals("unexpected display name", expectedDisplayname, page.getDisplayName());
		assertSame("unexpected search page", expectedSearchPage, page.getSearchPage());

	}
	
}
