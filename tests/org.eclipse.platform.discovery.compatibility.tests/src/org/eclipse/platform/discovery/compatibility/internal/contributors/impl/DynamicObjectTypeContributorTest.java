/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.compatibility.internal.contributors.impl;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.compatibility.tests.internal.fixture.SampleSearchPage;
import org.eclipse.platform.discovery.compatibility.tests.util.testcases.DynamicRegistryContributorTest;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.runtime.internal.xp.IContributionsReader;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.ObjectTypeExtensionParser;

public class DynamicObjectTypeContributorTest extends DynamicRegistryContributorTest<IObjectTypeDescription> {

	@Override
	protected void verifyContribution(IObjectTypeDescription contribution) {
		assertEquals("Unexpected display name", SampleSearchPage.LABEL, contribution.getDisplayName());
	}

	@Override
	protected IContributionsReader<IObjectTypeDescription> getParser(IExtensionRegistry registry) {
		return new ObjectTypeExtensionParser(registry);
	}

	@Override
	protected String expectedContributionId() {
		return new IdCalculator().calculateObjectTypeId(SampleSearchPage.ID);
	}
}
