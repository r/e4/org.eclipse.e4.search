/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.compatibility.tests.internal.fixture;

import org.eclipse.jface.dialogs.DialogPage;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.search.ui.ISearchPage;
import org.eclipse.search.ui.ISearchPageContainer;
import org.eclipse.swt.widgets.Composite;

public class SampleSearchPage extends DialogPage implements ISearchPage {
	
	public static final String ID = "org.eclipse.platform.discovery.compatibility.tests.internal.samplepage";
	public static final String LABEL = "Sample Search Page";
	
	public SampleSearchPage() {
	}

	public SampleSearchPage(String title) {
		super(title);
	}

	public SampleSearchPage(String title, ImageDescriptor image) {
		super(title, image);
	}

	@Override
	public void createControl(Composite parent) {
	}

	@Override
	public boolean performAction() {
		return false;
	}

	@Override
	public void setContainer(ISearchPageContainer container) {
	}

}
