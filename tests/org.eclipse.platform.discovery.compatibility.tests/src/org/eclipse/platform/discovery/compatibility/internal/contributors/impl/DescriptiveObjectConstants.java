/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.compatibility.internal.contributors.impl;

/**
 * Two test id's and display names to be reused by tests dealing with IDescriptiveObject's.
 *
 */
public interface DescriptiveObjectConstants {
	String ID_ONE = "id1";
	String LABEL_ONE = "Pesho";
	
	String ID_TWO = "id2";
	String LABEL_TWO = "Gosho";
}
