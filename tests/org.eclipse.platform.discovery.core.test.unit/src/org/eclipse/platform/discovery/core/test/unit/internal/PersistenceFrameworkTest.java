/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.test.unit.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.platform.discovery.core.internal.favorites.SearchFavoritesContentManager;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.persistence.DestinationItemPair;
import org.eclipse.platform.discovery.runtime.api.persistence.IMementoLoadProvider;
import org.eclipse.platform.discovery.runtime.api.persistence.IMementoStoreProvider;
import org.eclipse.platform.discovery.runtime.internal.persistence.IMementoContentManager;
import org.eclipse.platform.discovery.runtime.internal.persistence.MementoContentManagerException;
import org.eclipse.platform.discovery.runtime.internal.persistence.util.IPersistenceProviderRegistry;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.platform.discovery.util.internal.longop.CurrentThreadOperationRunner;
import org.eclipse.ui.IMemento;


/**
 * @author Iliyan Dimov
 *
 */
public class PersistenceFrameworkTest extends MockObjectTestCase 
{	
	private Mock<IMemento> container = null;
	private DestinationItemPair pair_1;
	private DestinationItemPair pair_2;
	private Mock<ISearchDestination> destination;
	private ILongOperationRunner opRunner;
	
	@Override
	protected void setUp() throws Exception
	{
		super.setUp();
		container = mock(IMemento.class); 
		destination = mock(ISearchDestination.class);
		pair_1 = new DestinationItemPair(destination.proxy(), new Object());
		pair_2 = new DestinationItemPair(destination.proxy(), new Object());
		opRunner = new CurrentThreadOperationRunner(new NullProgressMonitor());
	}

	public void testContentManagerWithWorkingProviders()
		throws Exception
	{		
		// data preparation		
		final IMemento memento1 = (IMemento) mock(IMemento.class).proxy();
		final IMemento memento2 = (IMemento) mock(IMemento.class).proxy();
		
		final Collection<DestinationItemPair> pairsCollection = new ArrayList<DestinationItemPair>();
		pairsCollection.add(pair_1);
		pairsCollection.add(pair_2);
		
		final Collection<IMemento> mementoCollection = new ArrayList<IMemento>();
		mementoCollection.add(memento1);
		mementoCollection.add(memento2);
				
		final Map<IMemento, DestinationItemPair> mementoToPairMap = new HashMap<IMemento, DestinationItemPair>();
		mementoToPairMap.put(memento1, pair_1);
		mementoToPairMap.put(memento2, pair_2);
		
		final Map<DestinationItemPair, IMemento> pairToMementoMap = new HashMap<DestinationItemPair, IMemento>();
		pairToMementoMap.put(pair_1, memento1);
		pairToMementoMap.put(pair_2, memento2);
				
		// create fake memento		
		container.expects(atLeastOnce()).method("getChildren").with(ANYTHING).will(returnValue(mementoCollection.toArray(new IMemento[0])));
			
		// actual storage		
		final Collection<Object> loadedStorage = new ArrayList<Object>(); 
			
		// create store providers
		final Mock<IMementoStoreProvider> storeProviderObjectType1Mock = mock(IMementoStoreProvider.class);
		storeProviderObjectType1Mock.expects(atLeastOnce()).method("canStore").with(same(pair_1)).will(returnValue(true));
		storeProviderObjectType1Mock.expects(atLeastOnce()).method("canStore").with(not(same(pair_1))).will(returnValue(false));
		storeProviderObjectType1Mock.expects(once()).method("store").with(same( container.proxy()), same(pair_1), isA(ILongOperationRunner.class));

		final Mock<IMementoStoreProvider> storeProviderObjectType2Mock = mock(IMementoStoreProvider.class);
		storeProviderObjectType2Mock.expects(atLeastOnce()).method("canStore").with(same(pair_2)).will(returnValue(true));
		storeProviderObjectType2Mock.expects(atLeastOnce()).method("canStore").with(not(same(pair_2))).will(returnValue(false));
		storeProviderObjectType2Mock.expects(once()).method("store").with(same(container.proxy()), same(pair_2), isA(ILongOperationRunner.class));
		
		// create load providers
		final Mock<IMementoLoadProvider> loadProviderObjectType1Mock = mock(IMementoLoadProvider.class);
		loadProviderObjectType1Mock.expects(atLeastOnce()).method("canLoad").with(same(memento1)).will(returnValue(true));	
		loadProviderObjectType1Mock.expects(atLeastOnce()).method("canLoad").with(same(memento2)).will(returnValue(false));	
		loadProviderObjectType1Mock.expects(atLeastOnce()).method("getChildType").will(returnValue(null));		
		loadProviderObjectType1Mock.expects(once()).method("load").with(same(memento1), isA(ILongOperationRunner.class)).will(returnValue(appendToCollection(loadedStorage, pair_1)));

		final Mock<IMementoLoadProvider> loadProviderObjectType2Mock = mock(IMementoLoadProvider.class);
		loadProviderObjectType2Mock.expects(atLeastOnce()).method("canLoad").with(same(memento1)).will(returnValue(false));
		loadProviderObjectType2Mock.expects(atLeastOnce()).method("canLoad").with(same(memento2)).will(returnValue(true));
		loadProviderObjectType2Mock.expects(atLeastOnce()).method("getChildType").will(returnValue(null));	
		loadProviderObjectType2Mock.expects(once()).method("load").with(same(memento2), isA(ILongOperationRunner.class)).will(returnValue(appendToCollection(loadedStorage, pair_2)));
		
		// create provider registry		
		final Mock<IPersistenceProviderRegistry> providerRegistry = mock(IPersistenceProviderRegistry.class); 
		providerRegistry.expects(atLeastOnce()).method("collectAllStoreProviders").
			will(returnValue(new HashSet<IMementoStoreProvider>(Arrays.asList(storeProviderObjectType1Mock.proxy(), storeProviderObjectType2Mock.proxy()))));
		providerRegistry.expects(atLeastOnce()).method("collectAllLoadProviders").
			will(returnValue(new HashSet<IMementoLoadProvider>(Arrays.asList(loadProviderObjectType1Mock.proxy(), loadProviderObjectType2Mock.proxy()))));

		
		// do the save job
		final IMementoContentManager<DestinationItemPair> contentManager = new SearchFavoritesContentManager(providerRegistry.proxy());								
		contentManager.saveContent(container.proxy(), pairsCollection, opRunner);	
		
		// verify save job
		storeProviderObjectType1Mock.verify();
		storeProviderObjectType2Mock.verify();
		
		// do the load job
		contentManager.loadContent(container.proxy(), opRunner);
		
		// verify load job			
		assertTrue(loadedStorage.containsAll(pairsCollection) && (! pairsCollection.isEmpty()));
	}
		
	/**
	 * @throws Exception
	 */
	public void testContentManagerWithDamagedLoadProviders()
		// throws Exception
	{
		// data preparation		
		final IMemento memento1 = (IMemento) mock(IMemento.class).proxy();
		final IMemento memento2 = (IMemento) mock(IMemento.class).proxy();
		
		final Collection<Object> mementoCollection = new ArrayList<Object>();
		mementoCollection.add(memento1);
		mementoCollection.add(memento2);
				
		// create fake memento		
		container.expects(atLeastOnce()).method("getChildren").with(ANYTHING).will(returnValue(mementoCollection.toArray(new IMemento[0])));
	
		// create load providers
		final String loadProviderObjectType1Descriptor = "loadProviderObjectType1"; 
		final String loadProviderObjectType2Descriptor = "loadProviderObjectType2"; 
					
		final Mock<IMementoLoadProvider> loadProviderObjectType1Mock = mock(IMementoLoadProvider.class);
		loadProviderObjectType1Mock.stubs().method("getChildType").will(returnValue(null));		
		loadProviderObjectType1Mock.expects(atLeastOnce()).method("getDescriptor").will(returnValue(loadProviderObjectType1Descriptor));		
		loadProviderObjectType1Mock.expects(atLeastOnce()).method("canLoad").with(ANYTHING).will(returnValue(true));	

		final Mock<IMementoLoadProvider> loadProviderObjectType2Mock = mock(IMementoLoadProvider.class);
		loadProviderObjectType2Mock.stubs().method("getChildType").will(returnValue(null));	
		loadProviderObjectType2Mock.expects(atLeastOnce()).method("getDescriptor").will(returnValue(loadProviderObjectType2Descriptor));		
		loadProviderObjectType2Mock.expects(atLeastOnce()).method("canLoad").with(ANYTHING).will(returnValue(true));
		
		// create provider registry		
		final Mock<IPersistenceProviderRegistry> providerRegistry = mock(IPersistenceProviderRegistry.class); 
		providerRegistry.expects(atLeastOnce()).method("collectAllStoreProviders").
			will(returnValue(new HashSet<IMementoLoadProvider>()));
		providerRegistry.expects(atLeastOnce()).method("collectAllLoadProviders").
			will(returnValue(new HashSet<IMementoLoadProvider>(Arrays.asList(loadProviderObjectType1Mock.proxy(), loadProviderObjectType2Mock.proxy()))));

		try
		{
			// do the job
			final IMementoContentManager<DestinationItemPair> contentManager = new SearchFavoritesContentManager(providerRegistry.proxy());			
			contentManager.loadContent(container.proxy(), opRunner);
			
			fail("Load provider with same loading type exists. "); //$NON-NLS-1$
		}
		catch (MementoContentManagerException cme)
		{
			// verify job
			final String errorMessage = cme.getMessage();			
			
			assertFalse(loadProviderObjectType1Descriptor.equals(loadProviderObjectType2Descriptor));	
			
			assertTrue(errorMessage.contains(loadProviderObjectType1Descriptor));		
			assertTrue(errorMessage.contains(loadProviderObjectType2Descriptor));		
		}
	}	
	
	/**
	 * @throws Exception
	 */
	public void testContentManagerWithDamagedStoreProviders()
	{
		// data preparation		
		final Collection<DestinationItemPair> pairsCollection = new ArrayList<DestinationItemPair>();
		pairsCollection.add(pair_1);
		pairsCollection.add(pair_2);
	
		// create store providers
		final String storeProviderObjectType1Descriptor = "storeProviderObjectType1"; 
		final String storeProviderObjectType2Descriptor = "storeProviderObjectType2"; 
		
		final Mock<IMementoStoreProvider> storeProviderObjectType1Mock = mock(IMementoStoreProvider.class);
		storeProviderObjectType1Mock.expects(atLeastOnce()).method("getDescriptor").will(returnValue(storeProviderObjectType1Descriptor));		
		storeProviderObjectType1Mock.expects(atLeastOnce()).method("canStore").with(ANYTHING).will(returnValue(true));

		final Mock<IMementoStoreProvider> storeProviderObjectType2Mock = mock(IMementoStoreProvider.class);
		storeProviderObjectType2Mock.expects(atLeastOnce()).method("getDescriptor").will(returnValue(storeProviderObjectType2Descriptor));		
		storeProviderObjectType2Mock.expects(atLeastOnce()).method("canStore").with(ANYTHING).will(returnValue(true));
		
		// create provider registry		
		final Mock<IPersistenceProviderRegistry> providerRegistry = mock(IPersistenceProviderRegistry.class); 
		providerRegistry.expects(atLeastOnce()).method("collectAllStoreProviders").
			will(returnValue(new HashSet<IMementoStoreProvider>(Arrays.asList(storeProviderObjectType1Mock.proxy(), storeProviderObjectType2Mock.proxy()))));
		providerRegistry.expects(atLeastOnce()).method("collectAllLoadProviders").
			will(returnValue(new HashSet<IMementoLoadProvider>()));
						
		try
		{
			// do the job
			final IMementoContentManager<DestinationItemPair> contentManager = new SearchFavoritesContentManager(providerRegistry.proxy());				
			contentManager.saveContent(container.proxy(), pairsCollection, opRunner);		
			
			fail("Load provider with same loading type exists. "); //$NON-NLS-1$
		}
		catch (MementoContentManagerException cme)
		{
			// verify job
			final String errorMessage = cme.getMessage();			
			
			assertFalse(storeProviderObjectType1Descriptor.equals(storeProviderObjectType2Descriptor));	
			
			assertTrue(errorMessage.contains(storeProviderObjectType1Descriptor));		
			assertTrue(errorMessage.contains(storeProviderObjectType2Descriptor));			
		}
	}	
	
	private Object appendToCollection(Collection<Object> collection, Object item)
	{
		collection.add(item);
		
		return item;
	}
}
