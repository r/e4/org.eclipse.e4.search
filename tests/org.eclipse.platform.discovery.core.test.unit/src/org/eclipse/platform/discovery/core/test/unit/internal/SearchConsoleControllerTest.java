/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.test.unit.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.platform.discovery.core.api.ISearchContext;
import org.eclipse.platform.discovery.core.api.SearchEvent;
import org.eclipse.platform.discovery.runtime.api.GroupingHierarchy;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.ISearchParameters;
import org.eclipse.platform.discovery.runtime.api.ISearchProvider;
import org.eclipse.platform.discovery.runtime.api.ISearchQuery;
import org.eclipse.platform.discovery.runtime.api.ISearchSubdestination;
import org.eclipse.platform.discovery.runtime.api.SearchFailedException;
import org.eclipse.platform.discovery.runtime.internal.ProviderNotFoundException;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.jmock.core.Constraint;
import org.jmock.core.constraint.IsSame;


public class SearchConsoleControllerTest extends SearchConsoleControllerFixture
{
	private static final String searchTitle = "testSearchTitle";
	private static final String searchDescription = "testSearchDescription";
	
	public void testDestinationsChangeOnObjectSelectionChange()
	{
		view.expects(once()).method("showDestinationsCategories").with(new IsSame(destinationCategories_obj1));
		testController.objectTypeSelected(objectType_1);
		view.verify();

		view.expects(once()).method("showDestinationsCategories").with(new IsSame(destinationCategories_obj2));
		testController.objectTypeSelected(objectType_2);
		view.verify();
	}

	public void testDefaultSearchInvoked() throws ProviderNotFoundException, SearchFailedException
	{
		sessionManager.stubs().method("session").with(eq(defaultSessionId), eq(1)).will(returnValue(testSession.proxy()));
		sessionHistory.stubs().method("historyLimit").will(returnValue(1));
		searchInvokedTest(defaultSessionId, 1);
	}
	
	public void testNonDefaultSearchInvoked() throws ProviderNotFoundException, SearchFailedException
	{
		sessionManager.stubs().method("session").with(eq(searchSessionId), eq(5)).will(returnValue(testSession.proxy()));
		sessionHistory.stubs().method("historyLimit").will(returnValue(5));
		searchInvokedTest(searchSessionId, 5);
	}
	
	private void searchInvokedTest(final String sessionId, final int expectedHistrorySize) throws ProviderNotFoundException, SearchFailedException
	{
		final Mock<ISearchQuery> searchQuery = mock(ISearchQuery.class);
		final Mock<ISearchProvider> searchProvider = mock(ISearchProvider.class);
		searchProviderDescr_1.expects(once()).method("createInstance").will(returnValue(searchProvider.proxy()));

		final Mock<ISearchDestination> searchDestination = mock(ISearchDestination.class);

		providerConfig.stubs().method("getActiveSearchProvider").with(same(objectType_1), same(destinationCategory1)).will(
										returnValue(searchProviderDescr_1.proxy()));
		providerConfig.stubs().method("getDestinationCategoriesForDestination").with(eq(searchDestination.proxy())).will(
				returnValue(Arrays.asList(new IDestinationCategoryDescription[] { destinationCategory1 })));

		final Mock<ISearchSubdestination> subdestination = mock(ISearchSubdestination.class);
		final List<ISearchSubdestination> subdestinations = new ArrayList<ISearchSubdestination>();
		subdestinations.add(subdestination.proxy());
		providerConfig.stubs().method("getAvailableSearchSubdestinations").with(eq(objectType_1), eq(destinationCategory1),
				eq(searchProviderDescr_1.proxy())).will(returnValue(subdestinations));
		providerConfig.stubs().method("isSubdestinationActive").with(eq(subdestination.proxy()), eq(objectType_1),
				eq(destinationCategory1), eq(searchProviderDescr_1.proxy())).will(returnValue(true));

		final Mock<ISearchParameters> searchParameters = mock(ISearchParameters.class);

		searchProvider.expects(once()).method("createQuery").with(eq(searchParameters.proxy()))
				.will(returnValue(searchQuery.proxy()));
		
		final Object searchResult = new Object();
		searchQuery.expects(once()).method("execute").with(eq(operationRunner.proxy())).will(returnValue(searchResult));
		
		final ISearchContext[] shownSearchContext = new ISearchContext[1];
		view.expects(once()).method("showResult").with(new Constraint()
		{
			public boolean eval(Object arg0)
			{
				shownSearchContext[0] = (ISearchContext) arg0;
				return shownSearchContext[0].searchResult() == searchResult;
			}

			public StringBuffer describeTo(StringBuffer arg0)
			{
				return arg0;
			}
		});

		searchParameters.stubs().method("getObjectTypeId").will(returnValue(objectType_1.getId()));
		searchParameters.stubs().method("getSearchDestination").will(returnValue(searchDestination.proxy()));
		sessionHistory.expects(once()).method("track").with(isA(ISearchContext.class));

		final SearchEvent event = new SearchEvent(searchParameters.proxy(), sessionId, searchTitle, searchDescription);
		testController.search(event);
		assertSame("The returned context was not contained by session that the search was performed in", testSession.proxy(), shownSearchContext[0].session());
		final int historyLimit = shownSearchContext[0].session().historyTrack().historyLimit();
		assertEquals("Unexpected history limit", historyLimit, shownSearchContext[0].session().historyTrack().historyLimit());
	}
	
	public void testDestinationsChanged()
	{
		view.expects(once()).method("updateDestinationsSelector");
		testController.searchDestinationsChanged();
	}
	
	public void testDestinationSelectedChanged()
	{
		final GroupingHierarchy group = new GroupingHierarchy("A", "a");
		final Set<GroupingHierarchy> groupings = new HashSet<GroupingHierarchy>(Arrays.asList(new GroupingHierarchy[]{group}));
		
		final Mock<ISearchProvider> searchProvider = mock(ISearchProvider.class);
		searchProvider.expects(once()).method("getGroupingHierarchies").will(returnValue(groupings));
		searchProviderDescr_1.expects(once()).method("createInstance").will(returnValue(searchProvider.proxy()));

		final Mock<ISearchDestination> searchDestination = mock(ISearchDestination.class);

		providerConfig.stubs().method("getActiveSearchProvider").with(same(objectType_1), same(destinationCategory1)).will(
										returnValue(searchProviderDescr_1.proxy()));
		providerConfig.stubs().method("getDestinationCategoriesForDestination").with(eq(searchDestination.proxy())).will(
				returnValue(Arrays.asList(new IDestinationCategoryDescription[] { destinationCategory1 })));
		providerConfig.stubs().method("getAvailableSearchSubdestinations").will(returnValue(new ArrayList<ISearchSubdestination>()));
		
		view.expects(once()).method("showGroupingHierarchies").with(new Constraint()
		{
			public boolean eval(Object arg0)
			{
				if (!(arg0 instanceof List<?>))
				{
					return false;
				}
				return ((List<?>) arg0).iterator().next() == group;
			}

			public StringBuffer describeTo(StringBuffer arg0)
			{
				return arg0;
			}
		});
		testController.searchDestinationSelected(objectType_1, searchDestination.proxy());
	}
	
	public void testDestinationsDeselected()
	{
		view.expects(once()).method("showGroupingHierarchies").with(new Constraint()
		{
			public boolean eval(Object arg0)
			{
				if (!(arg0 instanceof List))
				{
					return false;
				}
				return ((List<?>) arg0).size() == 0;
			}

			public StringBuffer describeTo(StringBuffer arg0)
			{
				return arg0;
			}
		});
		testController.searchDestinationSelected(objectType_1, null);
	}
	
	public void testSubdestinationActivationChanged()
	{
		final GroupingHierarchy group = new GroupingHierarchy("A", "a");
		final Set<GroupingHierarchy> groupings = new HashSet<GroupingHierarchy>(Arrays.asList(new GroupingHierarchy[]{group}));
		
		final Mock<ISearchProvider> searchProvider = mock(ISearchProvider.class);
		searchProvider.expects(once()).method("getGroupingHierarchies").will(returnValue(groupings));
		searchProviderDescr_1.expects(once()).method("createInstance").will(returnValue(searchProvider.proxy()));

		final Mock<ISearchDestination> searchDestination = mock(ISearchDestination.class);

		providerConfig.stubs().method("getActiveSearchProvider").with(same(objectType_1), same(destinationCategory1)).will(
										returnValue(searchProviderDescr_1.proxy()));
		providerConfig.stubs().method("getDestinationCategoriesForDestination").with(eq(searchDestination.proxy())).will(
				returnValue(Arrays.asList(new IDestinationCategoryDescription[] { destinationCategory1 })));
		providerConfig.stubs().method("getAvailableSearchSubdestinations").will(returnValue(new ArrayList<ISearchSubdestination>()));
		
		view.expects(once()).method("showGroupingHierarchies").with(new Constraint()
		{
			public boolean eval(Object arg0)
			{
				if (!(arg0 instanceof List))
				{
					return false;
				}
				return ((List<?>) arg0).iterator().next() == group;
			}

			public StringBuffer describeTo(StringBuffer arg0)
			{
				return arg0;
			}
		});
		testController.subdestinationActivationChanged(objectType_1, searchDestination.proxy(), null, false);
	}
}
