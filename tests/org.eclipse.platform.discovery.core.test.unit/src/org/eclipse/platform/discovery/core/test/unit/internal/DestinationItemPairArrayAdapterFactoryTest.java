/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.test.unit.internal;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.platform.discovery.core.api.ISearchContext;
import org.eclipse.platform.discovery.core.internal.ContextStructuredSelection;
import org.eclipse.platform.discovery.core.internal.favorites.DestinationItemPairArrayAdapterFactory;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.ISearchParameters;
import org.eclipse.platform.discovery.runtime.api.persistence.IDestinationItemPairAdapter;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.platform.discovery.util.internal.longop.CurrentThreadOperationRunner;

public class DestinationItemPairArrayAdapterFactoryTest extends	MockObjectTestCase 
{
	private DestinationItemPairArrayAdapterFactory factory;
	private Mock<ISearchDestination> destination;
	private Object item1;
	private Mock<ISearchContext> context;
	private Mock<ISearchParameters> parameters;
	private List<Object> items;
	private ILongOperationRunner opRunner;
	
	@Override
	protected void setUp() throws Exception 
	{
		items = new ArrayList<Object>();
		item1 = new Object();
		items.add(item1);
		factory = new DestinationItemPairArrayAdapterFactory();
		destination = mock(ISearchDestination.class);
		context = mock(ISearchContext.class);
		parameters = mock(ISearchParameters.class);
		parameters.stubs().method("getSearchDestination").withNoArguments().will(returnValue(destination.proxy()));
		context.stubs().method("searchParameters").withNoArguments().will(returnValue(parameters.proxy()));
		opRunner = new CurrentThreadOperationRunner(new NullProgressMonitor());
	}
	
	public void testGetAdapterList()
	{
		assertEquals(1, factory.getAdapterList().length);
		assertEquals(IDestinationItemPairAdapter.class, factory.getAdapterList()[0]);
	}
	
	public void testGetAdapterWithAdaptableObject()
	{
		assertNull(factory.getAdapter(new Object(), IDestinationItemPairAdapter.class));
	}
	
	public void testGetAdapterWithObjectAdapterType()
	{
		assertNull(factory.getAdapter(new ContextStructuredSelection(new LinkedList<Object>(), null), Object.class));
	}
	
	public void testGetAdapterWithCorrectParams()
	{
		final Object item2 = new Object();
		items.add(item2);
		final IDestinationItemPairAdapter result = (IDestinationItemPairAdapter)factory
			.getAdapter(new ContextStructuredSelection(items, context.proxy()), IDestinationItemPairAdapter.class);
		assertNotNull(result);
		assertEquals(2, result.adapt(opRunner).length);
		assertEquals(destination.proxy(), result.adapt(opRunner)[0].getDestination());
		assertEquals(item1, result.adapt(opRunner)[0].getItem());
		assertEquals(destination.proxy(), result.adapt(opRunner)[1].getDestination());
		assertEquals(item2, result.adapt(opRunner)[1].getItem());
	}
}
