/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.test.unit.internal;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.runtime.IAdapterManager;
import org.eclipse.platform.discovery.core.api.ISearchFavoritesMasterController;
import org.eclipse.platform.discovery.core.internal.favorites.IPersistenceUtil;
import org.eclipse.platform.discovery.core.internal.favorites.ISearchFavoritesControllerOutputView;
import org.eclipse.platform.discovery.core.internal.favorites.SearchFavoritesController;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.persistence.DestinationItemPair;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;
import org.eclipse.platform.discovery.util.api.env.IErrorHandler;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;


public class SearchFavoritesControllerTestFixture {
	
	public Mock<IPersistenceUtil> persistenceUtil;
	public Mock<ISearchFavoritesControllerOutputView> favoritesView;
	public Mock<ILongOperationRunner>opRunner;
	public Mock<IDiscoveryEnvironment> env;
	public Mock<IErrorHandler> errorHandler;
	public	Mock<IAdapterManager> adapterManager;
	public ISearchFavoritesMasterController controller;
	public Set<DestinationItemPair> loadedFromPeristence;
	public Mock<ISearchDestination> searchDestination;

	public final Object expectedObjectToShowInView = new Object();
	
	
	public SearchFavoritesControllerTestFixture(MockObjectTestCase tc) {
		persistenceUtil = tc.mock(IPersistenceUtil.class);
		favoritesView = tc.mock(ISearchFavoritesControllerOutputView.class);
		searchDestination = tc.mock(ISearchDestination.class);
		
		env = tc.mock(IDiscoveryEnvironment.class);
		errorHandler = tc.mock(IErrorHandler.class);
		env.stubs().method("errorHandler").will(tc.returnValue(errorHandler.proxy()));
		opRunner = tc.mock(ILongOperationRunner.class);
		env.stubs().method("operationRunner").will(tc.returnValue(opRunner.proxy()));
		
		adapterManager = tc.mock(IAdapterManager.class);
		
		loadedFromPeristence = new HashSet<DestinationItemPair>();
		loadedFromPeristence.add(new DestinationItemPair(searchDestination.proxy(), expectedObjectToShowInView));
		persistenceUtil.stubs().method("loadItems").with(tc.same(opRunner.proxy())).will(tc.onConsecutiveCalls(tc.returnValue(Collections.emptySet()), tc.returnValue(loadedFromPeristence)));

		favoritesView.expects(tc.once()).method("showFavorites").with(tc.collectionWithElements(0));
	
		controller = new SearchFavoritesController(favoritesView.proxy(), env.proxy(), persistenceUtil.proxy()) {

			@Override
			protected IAdapterManager getAdapterManager() {
				return adapterManager.proxy();
			}
		};
		tc.verify();
	}
}
