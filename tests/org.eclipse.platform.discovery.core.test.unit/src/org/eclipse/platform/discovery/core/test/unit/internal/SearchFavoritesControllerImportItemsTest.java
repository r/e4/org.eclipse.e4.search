/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.test.unit.internal;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.platform.discovery.runtime.api.persistence.DestinationItemPair;

public class SearchFavoritesControllerImportItemsTest extends AbstractSearchFavoritesControllerImportTest {
	
	private Set<DestinationItemPair> itemsToImport;
	
	public void setUp() {
		super.setUp();
		itemsToImport = new HashSet<DestinationItemPair>();
		itemsToImport.add(new DestinationItemPair(searchDestination.proxy(), new Object()));
		itemsToImport.add(new DestinationItemPair(searchDestination.proxy(), new Object()));
	}

	public void testImportToFavorites() {
		fixture.persistenceUtil.expects(once()).method("addItems").with(eq(itemsToImport), same(fixture.opRunner.proxy()));
		abstractDoImport();
	}

	@Override
	protected void abstractDoImport() {
		fixture.controller.importData(itemsToImport.toArray(new DestinationItemPair[itemsToImport.size()]));
	}
}
