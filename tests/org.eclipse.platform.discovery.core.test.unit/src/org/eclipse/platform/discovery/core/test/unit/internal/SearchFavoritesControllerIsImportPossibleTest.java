/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.test.unit.internal;

import org.eclipse.platform.discovery.runtime.api.persistence.IDestinationItemPairAdapter;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;


public class SearchFavoritesControllerIsImportPossibleTest extends MockObjectTestCase
{
	private SearchFavoritesControllerTestFixture fixture;
	private final Object objectToImport = new Object();
	
	public void setUp() 
	{
		fixture = new SearchFavoritesControllerTestFixture(this);
	}
	
	
	public void testIsImportPossibleWithSupportedData()
	{
		final Mock<IDestinationItemPairAdapter> adapter = mock(IDestinationItemPairAdapter.class);
		fixture.adapterManager.stubs().method("getAdapter").with(same(objectToImport), same(IDestinationItemPairAdapter.class)).will(returnValue(adapter.proxy()));
		assertTrue(fixture.controller.isImportPossible(objectToImport));
	}
	
	public void testIsImportPossibleWithUnsupportedData()
	{
		fixture.adapterManager.stubs().method("getAdapter").with(same(objectToImport), same(IDestinationItemPairAdapter.class)).will(returnValue(null));
		fixture.adapterManager.stubs().method("loadAdapter").with(same(objectToImport), same(IDestinationItemPairAdapter.class.getName())).will(returnValue(null));
		assertFalse(fixture.controller.isImportPossible(objectToImport));
	}
}
