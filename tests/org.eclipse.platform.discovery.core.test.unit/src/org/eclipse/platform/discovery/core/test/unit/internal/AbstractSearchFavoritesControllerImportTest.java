/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.test.unit.internal;

import java.io.IOException;

import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.internal.persistence.MementoContentManagerException;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.ui.WorkbenchException;


public abstract class AbstractSearchFavoritesControllerImportTest extends MockObjectTestCase {
	
	protected SearchFavoritesControllerTestFixture fixture;
	
	protected Mock<ISearchDestination> searchDestination;

	
	public void setUp() {
		searchDestination = mock(ISearchDestination.class);
		fixture = new SearchFavoritesControllerTestFixture(this);
		fixture.favoritesView.expects(once()).method("showFavorites").with(collectionWithElements(1, fixture.expectedObjectToShowInView));
	}

	private void testImportWithExceptionFromPersistenceUtil(Exception exc) {
		//don't check for arguments here: basically after the first failure the code should stop saving any other
	    //objects. We configure the the mock to fail at first invocation and that's why the expectation is "once()".
		//The arguments are not checked, because the code uses sets, which do not provide stable order during
		//iteration
		fixture.persistenceUtil.expects(once()).method("addItems").withAnyArguments().will(throwException(exc));
		fixture.errorHandler.expects(once()).method("handleException").with(eq(exc));
		abstractDoImport();
	}

	public abstract void testImportToFavorites();
	
	protected abstract void abstractDoImport();

	
	public final void testAddItemsWithWbException()
	{
		final WorkbenchException e = new WorkbenchException("");
		testImportWithExceptionFromPersistenceUtil(e);
	}
	
	public final void testAddItemsWithIOException()
	{
		final IOException e = new IOException();
		testImportWithExceptionFromPersistenceUtil(e);
	}
	
	public final void testAddItemsWithMementoException()
	{
		final MementoContentManagerException e = new MementoContentManagerException();
		testImportWithExceptionFromPersistenceUtil(e);
	}


}
