/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.test.unit.internal;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.platform.discovery.runtime.internal.persistence.MementoContentManagerException;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.ui.WorkbenchException;


public class SearchFavoritesControllerDeleteTest extends MockObjectTestCase {
	private SearchFavoritesControllerTestFixture fixture;
	private Set<Object> itemsToDelete = new HashSet<Object>();
	
	@Override
	public void setUp() 
	{
		fixture = new SearchFavoritesControllerTestFixture(this);
		fixture.favoritesView.expects(once()).method("showFavorites").with(collectionWithElements(1, fixture.expectedObjectToShowInView));
	}

	public void testFavoriteItemsDelete() 
	{
		fixture.persistenceUtil.expects(once()).method("deleteItems").with(same(itemsToDelete), same(fixture.opRunner.proxy()));
		fixture.controller.deleteItems(itemsToDelete);
	}
	public void testDeleteItemsWithWbExc()
	{
		final WorkbenchException exc = new WorkbenchException("test");
		testDeleteWithExceptionFromPersistenceUtil(exc);
	}
	
	public void testDeleteItemsWithIOExc()
	{
		final IOException exc = new IOException();
		testDeleteWithExceptionFromPersistenceUtil(exc);
	}
	
	public void testDeleteItemsWithMementoExc()
	{
		final MementoContentManagerException exc = new MementoContentManagerException();
		testDeleteWithExceptionFromPersistenceUtil(exc);
	}

	private void testDeleteWithExceptionFromPersistenceUtil(Exception exc) {
		//don't check for arguments here: basically after the first failure the code should stop saving any other
	    //objects. We configure the the mock to fail at first invocation and that's why the expectation is "once()".
		//The arguments are not checked, because the code uses sets, which do not provide stable order during
		//iteration
		fixture.persistenceUtil.expects(once()).method("deleteItems").withAnyArguments().will(throwException(exc));
		fixture.errorHandler.expects(once()).method("handleException").with(eq(exc));
		fixture.controller.deleteItems(itemsToDelete);
	}
}
