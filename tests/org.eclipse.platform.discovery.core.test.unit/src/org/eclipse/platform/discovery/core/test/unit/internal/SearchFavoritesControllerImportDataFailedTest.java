/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.test.unit.internal;

import org.eclipse.platform.discovery.runtime.api.persistence.DestinationItemPair;
import org.eclipse.platform.discovery.runtime.api.persistence.IDestinationItemPairAdapter;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;


public class SearchFavoritesControllerImportDataFailedTest extends MockObjectTestCase 
{
	private final Object optimisticallyAllowed = new Object();
	private final Object completelyDenied = new Object();
	protected SearchFavoritesControllerTestFixture fixture;
	private Mock<IDestinationItemPairAdapter> adapter;

	
	
	@Override
	protected void setUp() throws Exception 
	{
		fixture = new SearchFavoritesControllerTestFixture(this);
		fixture.adapterManager.stubs().method("getAdapter").with(same(completelyDenied), same(IDestinationItemPairAdapter.class)).will(returnValue(null));
		fixture.adapterManager.stubs().method("loadAdapter").with(same(completelyDenied), same(IDestinationItemPairAdapter.class.getName())).will(returnValue(null));

		adapter = mock(IDestinationItemPairAdapter.class);

		fixture.adapterManager.stubs().method("getAdapter").with(
				same(optimisticallyAllowed), same(IDestinationItemPairAdapter.class))
				.will(returnValue(adapter.proxy()));
	}
	
	public void testImportData_OptimisticCheckFailed()
	{
		fixture.favoritesView.expects(once()).method("showUnsupportedContentWindow");
		fixture.controller.importData(completelyDenied);
	}
	
	public void testImportData_RealImportFailed() {
		adapter.stubs().method("adapt").with(isA(ILongOperationRunner.class)).will(returnValue(null));
		fixture.favoritesView.expects(once()).method("showUnsupportedContentWindow");

		fixture.controller.importData(optimisticallyAllowed);
	}
	
	public void testImportDataWithEmptyArray()
	{
		adapter.stubs().method("adapt").with(isA(ILongOperationRunner.class)).will(returnValue(new DestinationItemPair[]{}));
		fixture.favoritesView.expects(once()).method("showNoContentFoundWindow");
		fixture.controller.importData(optimisticallyAllowed);
	}
	
	public void testImportDataWithException()
	{
		final RuntimeException e = new RuntimeException();
		fixture.adapterManager.stubs().method("getAdapter").with(same(optimisticallyAllowed), same(IDestinationItemPairAdapter.class)).will(throwException(e));
		fixture.errorHandler.expects(once()).method("handleException").with(same(e));
		fixture.controller.importData(optimisticallyAllowed);
	}
}
