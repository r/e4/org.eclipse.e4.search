/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.test.unit.internal;

import java.util.Arrays;
import java.util.List;

import org.eclipse.platform.discovery.core.api.ISearchContext;
import org.eclipse.platform.discovery.core.internal.ISearchSession;
import org.eclipse.platform.discovery.core.internal.SearchConsoleController;
import org.eclipse.platform.discovery.core.internal.console.ISearchConsoleControllerOutputView;
import org.eclipse.platform.discovery.runtime.internal.ISearchProviderConfiguration;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.ISearchProviderDescription;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.testutils.utils.model.DestinationCategoryDescriptionBuilder;
import org.eclipse.platform.discovery.testutils.utils.model.ObjectTypeDescriptionBuilder;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;
import org.eclipse.platform.discovery.util.api.env.IErrorHandler;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.platform.discovery.util.internal.session.IHistoryTrack;
import org.eclipse.platform.discovery.util.internal.session.ISessionManager;


public abstract class SearchConsoleControllerFixture extends MockObjectTestCase {

	protected static final String searchSessionId = "searchSessionId";
	protected static final String defaultSessionId = "defaultSessionId"; 
	
	protected SearchConsoleController testController;
	protected Mock<ISearchProviderConfiguration> providerConfig;
	protected Mock<ISearchConsoleControllerOutputView> view;

	protected IObjectTypeDescription objectType_1;
	protected IObjectTypeDescription objectType_2;
	protected static final String OBJECT_TYPE_1_ID = "object1id";
	protected static final String OBJECT_TYPE_2_ID = "object2id";

	protected Mock<ISearchProviderDescription> searchProviderDescr_1;
	protected IDestinationCategoryDescription destinationCategory1;
	protected IDestinationCategoryDescription destinationCategory2;
	protected IDestinationCategoryDescription destinationCategory3;

	protected List<IDestinationCategoryDescription> destinationCategories_obj1;
	protected List<IDestinationCategoryDescription> destinationCategories_obj2;
	
	protected Mock<IDiscoveryEnvironment> consoleEnvironment;
	protected Mock<IErrorHandler> errorHandler;
	protected Mock<ILongOperationRunner> operationRunner;
	
	protected Mock<ISessionManager<ISearchSession>> sessionManager;
	protected Mock<IHistoryTrack<ISearchContext>> sessionHistory;
	protected Mock<ISearchSession> testSession;
	
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		searchProviderDescr_1 = mock(ISearchProviderDescription.class);
		setupObjectTypes();
		setupSearchProviderConfig();

		view = mock(ISearchConsoleControllerOutputView.class);
		final List<IObjectTypeDescription> objectTypes = Arrays.asList(new IObjectTypeDescription[] {
				objectType_1, objectType_2 });
		view.expects(once()).method("showObjectTypes").with(eq(objectTypes));
		
		setupSessions();
		setupConsoleEnvironment();
		testController = new SearchConsoleController(providerConfig.proxy(), view.proxy(), consoleEnvironment.proxy(), sessionManager.proxy(), defaultSessionId);
	}
	
	private void setupSessions()
	{
		sessionHistory = mock(IHistoryTrack.class);
		
		testSession = mock(ISearchSession.class);
		testSession.stubs().method("historyTrack").will(returnValue(sessionHistory.proxy()));
		
		sessionManager = mock(ISessionManager.class);
	}
	
	private void setupConsoleEnvironment()
	{
		operationRunner = mock(ILongOperationRunner.class);
		errorHandler = mock(IErrorHandler.class);

		consoleEnvironment = mock(IDiscoveryEnvironment.class);
		consoleEnvironment.stubs().method("operationRunner").will(returnValue(operationRunner.proxy()));
		consoleEnvironment.stubs().method("errorHandler").will(returnValue(errorHandler.proxy()));
	}

	private void setupObjectTypes()
	{
		objectType_1 = new ObjectTypeDescriptionBuilder().withId(OBJECT_TYPE_1_ID).object();
		objectType_2 = new ObjectTypeDescriptionBuilder().withId(OBJECT_TYPE_2_ID).object();
	}

	private void setupSearchProviderConfig()
	{
		final List<IObjectTypeDescription> objectTypes = Arrays.asList(new IObjectTypeDescription[] {
				objectType_1, objectType_2 });
		providerConfig = mock(ISearchProviderConfiguration.class);
		providerConfig.stubs().method("getObjectTypes").will(returnValue(objectTypes));

		destinationCategory1 = new DestinationCategoryDescriptionBuilder().object();
		destinationCategory2 = new DestinationCategoryDescriptionBuilder().object();
		destinationCategories_obj1 = Arrays.asList(new IDestinationCategoryDescription[] {destinationCategory1, destinationCategory2});
		providerConfig.stubs().method("getAvailableDestinationCategoriesForObjectType").with(eq(objectType_1)).will(
				returnValue(destinationCategories_obj1));

		destinationCategory3 = new DestinationCategoryDescriptionBuilder().object();
		destinationCategories_obj2 = Arrays.asList(new IDestinationCategoryDescription[] { destinationCategory3 });
		providerConfig.stubs().method("getAvailableDestinationCategoriesForObjectType").with(eq(objectType_2)).will(
				returnValue(destinationCategories_obj2));
	}
}
