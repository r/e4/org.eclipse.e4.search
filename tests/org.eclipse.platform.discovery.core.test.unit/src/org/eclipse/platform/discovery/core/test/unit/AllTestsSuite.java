/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.test.unit;

import org.eclipse.platform.discovery.core.test.unit.internal.DestinationItemPairArrayAdapterFactoryTest;
import org.eclipse.platform.discovery.core.test.unit.internal.PersistenceFrameworkTest;
import org.eclipse.platform.discovery.core.test.unit.internal.SearchConsoleControllerTest;
import org.eclipse.platform.discovery.core.test.unit.internal.SearchFavoritesControllerDeleteTest;
import org.eclipse.platform.discovery.core.test.unit.internal.SearchFavoritesControllerImportDataFailedTest;
import org.eclipse.platform.discovery.core.test.unit.internal.SearchFavoritesControllerImportDataTest;
import org.eclipse.platform.discovery.core.test.unit.internal.SearchFavoritesControllerImportItemsTest;
import org.eclipse.platform.discovery.core.test.unit.internal.SearchFavoritesControllerIsImportPossibleTest;
import org.eclipse.platform.discovery.core.test.unit.internal.SearchFavoritesPersistenceUtilTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@SuiteClasses({
	PersistenceFrameworkTest.class,			
	SearchConsoleControllerTest.class,
	SearchFavoritesControllerImportDataTest.class,
	SearchFavoritesControllerImportItemsTest.class,
	SearchFavoritesControllerDeleteTest.class,
	SearchFavoritesPersistenceUtilTest.class,
	SearchFavoritesControllerImportDataFailedTest.class,
	SearchFavoritesControllerIsImportPossibleTest.class,
	DestinationItemPairArrayAdapterFactoryTest.class
})
@RunWith(Suite.class)
public class AllTestsSuite
{
}
