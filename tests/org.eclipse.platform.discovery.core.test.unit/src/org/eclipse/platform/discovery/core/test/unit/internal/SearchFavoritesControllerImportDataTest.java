/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.test.unit.internal;


import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.persistence.DestinationItemPair;
import org.eclipse.platform.discovery.runtime.api.persistence.IDestinationItemPairAdapter;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;


public class SearchFavoritesControllerImportDataTest extends AbstractSearchFavoritesControllerImportTest {

	private final Object importedObject = new Object();
	
	private DestinationItemPair[] pairs;
	
	public void setUp() {
		super.setUp();
		pairs = new DestinationItemPair[] {
				new DestinationItemPair(searchDestination.proxy(), new Object()),
				new DestinationItemPair(searchDestination.proxy(), new Object()),
				new DestinationItemPair(((ISearchDestination)mock(ISearchDestination.class).proxy()), new Object())
		};
		final Mock<IDestinationItemPairAdapter> adapter = mock(IDestinationItemPairAdapter.class);
		adapter.stubs().method("adapt").with(isA(ILongOperationRunner.class)).will(returnValue(pairs));
		fixture.adapterManager.stubs().method("getAdapter").with(same(importedObject), same(IDestinationItemPairAdapter.class)).will(returnValue(adapter.proxy()));
	}
	
	public void testImportToFavorites() {
		fixture.persistenceUtil.expects(once()).method("addItems").with(collectionWithElements(2, pairs[0], pairs[1]), same(fixture.opRunner.proxy()));
		fixture.persistenceUtil.expects(once()).method("addItems").with(collectionContaining(pairs[2]), same(fixture.opRunner.proxy()));
		abstractDoImport();
	}
	
	@Override
	protected void abstractDoImport() {
		fixture.controller.importData(importedObject);
	}
}
