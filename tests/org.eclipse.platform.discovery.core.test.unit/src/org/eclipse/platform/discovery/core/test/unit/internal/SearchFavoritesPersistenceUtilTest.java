/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.test.unit.internal;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.platform.discovery.core.internal.favorites.SearchFavoritesPersistenceUtil;
import org.eclipse.platform.discovery.core.internal.favorites.SearchFavoritesPersistenceUtil.IPersistenceContext;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.persistence.DestinationItemPair;
import org.eclipse.platform.discovery.runtime.internal.persistence.IMementoContentManager;
import org.eclipse.platform.discovery.runtime.internal.persistence.MementoContentManagerException;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.WorkbenchException;
import org.jmock.core.Constraint;


public class SearchFavoritesPersistenceUtilTest extends MockObjectTestCase
{
	private SearchFavoritesPersistenceUtil util;
	private Mock<IPersistenceContext<DestinationItemPair>> persistenceContextMock;
	private File persistenceFile = null;
	private Mock<ILongOperationRunner> operationRunnerMock;
	private Mock<IMemento> memento;
	private Mock<IMementoContentManager<Object>> mementoContentManager;
	private int mementoSavedCount;
	private Mock<ISearchDestination> searchDestination;
	private DestinationItemPair pair_1;
	private DestinationItemPair pair_2;

	@Override
	protected void setUp() throws Exception
	{
		mementoContentManager = mock(IMementoContentManager.class);
		operationRunnerMock = mock(ILongOperationRunner.class);
		memento = mock(IMemento.class);
		mementoSavedCount = 0;
		setupPersistenceContext();
		searchDestination = mock(ISearchDestination.class);
		pair_1 = new DestinationItemPair(searchDestination.proxy(), new Object());
		pair_2 = new DestinationItemPair(searchDestination.proxy(), new Object());

		util = new SearchFavoritesPersistenceUtil(persistenceContextMock.proxy())
		{
			@Override
			protected IMemento provideStorageMemento() throws IOException, WorkbenchException
			{
				return memento.proxy();
			}

			@Override
			protected IMemento createStorageMemento()
			{
				return memento.proxy();
			}

			@Override
			protected void saveMemento(IMemento m) throws IOException
			{
				assertTrue("Unexpected memento", m.equals(memento.proxy()));
				mementoSavedCount++;
			}
		};
	}

	public void testLoadItems() throws WorkbenchException, IOException, MementoContentManagerException
	{
		final Set<DestinationItemPair> itemsToBeLoadedFromMemento = new HashSet<DestinationItemPair>(Arrays.asList(new DestinationItemPair[] { pair_1, pair_2 }));

		mementoContentManager.expects(once()).method("loadContent").with(eq(memento.proxy()), isA(ILongOperationRunner.class)).will(returnValue(itemsToBeLoadedFromMemento));
		final Set<DestinationItemPair> loadedItems = util.loadItems(operationRunnerMock.proxy());
		assertEquals("2 items are expected to be loaded", 2, loadedItems.size());
		assertTrue("Pair 1 not found", loadedItems.contains(pair_1));
		assertTrue("Pair 2 not found", loadedItems.contains(pair_2));
	}

	public void testAddItems() throws WorkbenchException, IOException, MementoContentManagerException
	{
		final Set<DestinationItemPair> itemsToBeLoadedFromMemento = new HashSet<DestinationItemPair>(Arrays.asList(new DestinationItemPair[] { pair_2 }));

		mementoContentManager.expects(once()).method("loadContent").with(eq(memento.proxy()), isA(ILongOperationRunner.class)).will(returnValue(itemsToBeLoadedFromMemento));
		mementoContentManager.expects(once()).method("saveContent").with(eq(memento.proxy()), collectionWithElements(2, pair_1, pair_2), isA(ILongOperationRunner.class));

		util.addItems(new HashSet<DestinationItemPair>(Arrays.asList(new DestinationItemPair[] { pair_1, pair_2 })), operationRunnerMock.proxy());
		assertEquals("Memento should be saved once", 1, mementoSavedCount);
	}

	public void testDeleteItem() throws IOException, WorkbenchException, MementoContentManagerException
	{
		final Set<DestinationItemPair> existingLoadItems = new HashSet<DestinationItemPair>(Arrays.asList(new DestinationItemPair[] { pair_1, pair_2 }));
		final Set<Object> itemsToDelete = new HashSet<Object>(Arrays.asList(new Object[] { pair_1.getItem() }));

		mementoContentManager.expects(once()).method("loadContent").with(eq(memento.proxy()), isA(ILongOperationRunner.class)).will(returnValue(existingLoadItems));
		mementoContentManager.expects(once()).method("saveContent").with(eq(memento.proxy()), collectionWithSingleElement(pair_2), isA(ILongOperationRunner.class));

		util.deleteItems(itemsToDelete, operationRunnerMock.proxy());
		assertEquals("Memento should be saved once", 1, mementoSavedCount);
	}

	private Constraint collectionWithSingleElement(final Object element)
	{
		return new Constraint()
		{
			@SuppressWarnings("unchecked")
			@Override
			public boolean eval(Object arg0)
			{
				final Collection<Object> inCollection = (Collection<Object>) arg0;
				final boolean result = inCollection.size() == 1 && inCollection.contains(element);
				return result;
			}

			@Override
			public StringBuffer describeTo(StringBuffer arg0)
			{
				arg0.append("[<" + element.toString() + ">]");
				return arg0;
			}
		};
	}

	private void setupPersistenceContext() throws IOException
	{
		persistenceContextMock = mock(IPersistenceContext.class);

		persistenceContextMock.stubs().method("getContentManager").will(returnValue(mementoContentManager.proxy()));
		persistenceFile = File.createTempFile("test", null);
		persistenceFile.delete();
		persistenceContextMock.stubs().method("getFile").will(returnValue(persistenceFile));
	}
}
