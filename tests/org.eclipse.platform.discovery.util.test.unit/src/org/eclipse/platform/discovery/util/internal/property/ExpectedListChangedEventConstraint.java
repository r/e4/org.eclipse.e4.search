/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.property;

import java.util.Collection;
import java.util.List;

import org.eclipse.platform.discovery.util.internal.property.IListProperty;
import org.eclipse.platform.discovery.util.internal.property.PropertyListChangedEvent;
import org.eclipse.platform.discovery.util.internal.property.PropertyListChangedEvent.ChangeKind;
import org.jmock.core.Constraint;

class ExpectedListChangedEventConstraint implements Constraint
{
	private final IListProperty<Object> notifier;
	private final PropertyListChangedEvent.ChangeKind kind;
	private final Collection<Object> elements;
	
	public ExpectedListChangedEventConstraint(IListProperty<Object> notifier, ChangeKind kind, List<Object> elements)
	{
		this.notifier = notifier;
		this.kind = kind;
		this.elements = elements;
	}
	
	public boolean eval(final Object arg0) {
		@SuppressWarnings("unchecked")
		final PropertyListChangedEvent<Object> event = (PropertyListChangedEvent<Object>)arg0;
		return (notifier ==  event.getNotifier() && kind == event.getChangeKind() && elements.equals(event.getElements()));
	}

	public StringBuffer describeTo(final StringBuffer arg0) {
		arg0.append("PropertyListChangedEvent with");
		arg0.append(" notifier: ");
		arg0.append(notifier);
		arg0.append(" kind: ");
		arg0.append(kind);
		arg0.append(" elements: ");
		arg0.append(elements.toString());
		return arg0;
	}
}
