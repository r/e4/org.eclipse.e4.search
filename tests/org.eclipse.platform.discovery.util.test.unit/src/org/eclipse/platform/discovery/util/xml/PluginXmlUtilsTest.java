/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.xml;

import org.eclipse.platform.discovery.util.internal.xml.IPluginXmlUtils;
import org.eclipse.platform.discovery.util.internal.xml.PluginXmlUtils;
import org.w3c.dom.Element;

import junit.framework.TestCase;

public class PluginXmlUtilsTest extends TestCase{

	public void testCreatePluginSnippet() {
		
		IPluginXmlUtils utils = new PluginXmlUtils();
		String pesho = "pesho";
		
		Element pluginXML = utils.createPluginSnippet(pesho);
		assertEquals(IPluginXmlUtils.PLUGIN_ELEMENT_NAME, pluginXML.getNodeName());

		Element extension = (Element)pluginXML.getFirstChild();
		assertEquals(IPluginXmlUtils.EXTENSION_ELEMENT_NAME, extension.getNodeName());
		assertEquals(pesho, extension.getAttribute(IPluginXmlUtils.POINT_ATTRIBUTE_NAME));
	}
}
