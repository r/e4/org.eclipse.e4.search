/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.xml;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.eclipse.core.runtime.IAdapterManager;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.util.api.xml.IXmlRepresentable;
import org.eclipse.platform.discovery.util.internal.xml.CollectionTransformer;
import org.eclipse.platform.discovery.util.internal.xml.ICollectionTransformer;
import org.jmock.core.Invocation;
import org.jmock.core.Stub;
import org.w3c.dom.Element;


public abstract class CollectionTransformerTest extends CollectionTransformerAbstractTest {

	private static final String testNamespace= "http://test.namespace.eclipse.org";
	private static final String testItemTag = "test-item";
	private static final String testCollecitonTag = "test-collection";
	
	public static Test suite()
	{
		final TestSuite s = new TestSuite();
		s.addTestSuite(WithNonAdaptableObjects.class);
		s.addTestSuite(WithAdaptableObjects.class);
		s.addTestSuite(WithInstanceOfXmlRepresentable.class);
		return s;
	}
	
	@Override
	protected String testCollecitonTag() {
		return testCollecitonTag;
	}

	@Override
	protected String testItemTag() {
		return testItemTag;
	}

	@Override
	protected String testNamespace() {
		return testNamespace;
	}
	
	protected Mock<IAdapterManager> mgrMock;
	@Override
	public void setUp() {
		super.setUp();
		mgrMock = mock(IAdapterManager.class);
		lorMock.expects(once()).method("run").withAnyArguments().will(invokePassedOperation());
		pmMock.stubs().method("beginTask").withAnyArguments();
		pmMock.stubs().method("done").withAnyArguments();

	}
	
	@Override
	protected ICollectionTransformer createCollectionTransformer() {
		final ICollectionTransformer ct = new CollectionTransformer()
		{
			@Override
			protected IAdapterManager getAdapterManager() {
				return mgrMock.proxy();
			}
		};
		ct.setCollectionTag(testCollecitonTag());
		ct.setItemTag(testItemTag());
		ct.setNamespace(testNamespace());
		return ct;
	}

	public static class WithNonAdaptableObjects extends CollectionTransformerTest
	{
		
		@Override
		public void setUp() {
			super.setUp();
			mgrMock.stubs().method("getAdapter").with(isA(String.class), same(IXmlRepresentable.class)).will(returnValue(null));
			mgrMock.stubs().method("loadAdapter").with(isA(String.class), eq(IXmlRepresentable.class.getName())).will(returnValue(null));
		}

		@Override
		protected Collection<?> createTestList() {
			final Collection<String> collection = new ArrayList<String>(3);
			collection.add("item1");
			collection.add("item2");
			collection.add("item3");;
			return collection;
		}
		
		@Override
		protected void assertExpectedElement(final int i, Element item) {
			final Iterator<?> it = testCollection.iterator();
			String ithToString = null;
			for (int j = 0; j < i + 1; j++)
			{
				ithToString = it.next().toString();
			}
			assertEquals(ithToString, item.getTextContent().trim());
		}
	}
	
	public static class WithAdaptableObjects extends CollectionTransformerTest
	{
		@Override
		public void setUp() {
			super.setUp();
			mgrMock.stubs().method("getAdapter").with(isA(String.class), same(IXmlRepresentable.class)).will(new Stub()
			{
				@Override
				public Object invoke(Invocation arg0) throws Throwable {
					return new XmlRepresentableObject((String)arg0.parameterValues.get(0));
				}

				@Override
				public StringBuffer describeTo(StringBuffer arg0) {
					arg0.append("Return adapter from String to IXmlRepresentable");
					return arg0;
				}
			});
		}

		@Override
		protected Collection<?> createTestList() {
			final Collection<String> collection = new ArrayList<String>(3);
			collection.add("item1");
			collection.add("item2");
			collection.add("item3");;
			return collection;
		}
		
		@Override
		protected void assertExpectedElement(final int i, Element item) {
			final Iterator<?> it = testCollection.iterator();
			String ithToString = null;
			for (int j = 0; j < i + 1; j++)
			{
				ithToString = it.next().toString();
			}
			for (int j = 0; j < i + 1; j++)
			{
				assertNotNull(item.getElementsByTagNameNS(testNamespace(), ithToString).item(0));
			}
		}
		
		
	}
	
	public static class WithInstanceOfXmlRepresentable extends WithAdaptableObjects
	{
		@Override
		protected Collection<?> createTestList() {
			final Collection<Object> collection = new ArrayList<Object>(3);
			collection.add(new XmlRepresentableObject("item1"));
			collection.add(new XmlRepresentableObject("item2"));
			collection.add(new XmlRepresentableObject("item3"));
			return collection;
		}
	}
	
	private static class XmlRepresentableObject implements IXmlRepresentable
	{
		final String s;
		public XmlRepresentableObject(String s)
		{
			this.s = s;
		}
		
		@Override
		public String toString()
		{
			return s;
		}
		@Override
		public String toStringXml(IProgressMonitor pm) {
			return "<" + s +"/>";
		}
	}
}

