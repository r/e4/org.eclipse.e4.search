/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.property;

import java.util.ListIterator;

import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.util.internal.property.Access;
import org.eclipse.platform.discovery.util.internal.property.IListProperty;


public class AccessCheckingListIteratorTests extends MockObjectTestCase 
{
	private AccessCheckingListIterator<Object> targetIterator;
	private Mock<IListProperty<Object>> mockProperty;
	private Mock<ListIterator<Object>> mockDelegateIter;
	private Object readWriteObj = new Object();
	
	@Override
	public void setUp()
	{
		mockDelegateIter = mock(ListIterator.class);
		mockProperty = mock(IListProperty.class);
		targetIterator = new AccessCheckingListIterator<Object>(mockProperty.proxy(), mockDelegateIter.proxy());
	}
	
	public void testIteratorAddAccessReadOnly()
	{
		mockProperty.expects(exactly(2)).method("getListAccess").withNoArguments().will(returnValue(Access.READ_ONLY));
		try
		{
			targetIterator.add(new Object());
		} catch (UnsupportedOperationException uoe)
		{
			
		}
	}
	
	
	public void testIteratorAddAccessNotDenied()
	{
		mockDelegateIter.expects(once()).method("add").with(same(readWriteObj));
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_WRITE));
		targetIterator.add(readWriteObj);
	}
	
	
	public void testIteratorHasNextReadWrite()
	{
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_WRITE));
		mockDelegateIter.expects(once()).method("hasNext").withNoArguments().will(returnValue(true));
		assertTrue(targetIterator.hasNext());
	}
	
	public void testIteratorHasPreviousReadWrite()
	{
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_WRITE));
		mockDelegateIter.expects(once()).method("hasPrevious").withNoArguments().will(returnValue(true));
		assertTrue(targetIterator.hasPrevious());
	}
	
	public void testIteratorHasPreviousReadOnly()
	{
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_ONLY));
		mockDelegateIter.expects(once()).method("hasPrevious").withNoArguments().will(returnValue(true));
		assertTrue(targetIterator.hasPrevious());
	}
	
	
	public void testIteratorNextReadWrite()
	{
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_WRITE));
		mockDelegateIter.expects(once()).method("next").withNoArguments().will(returnValue(readWriteObj));
		assertSame(readWriteObj, targetIterator.next());
	}
	
	public void testNextReadOnly()
	{
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_ONLY));
		mockDelegateIter.expects(once()).method("next").withNoArguments().will(returnValue(readWriteObj));
		assertSame(readWriteObj, targetIterator.next());
	}
	
	
	public void testNextIndexReadWrite()
	{
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_WRITE));
		mockDelegateIter.expects(once()).method("nextIndex").withNoArguments().will(returnValue(26));
		assertEquals(26, targetIterator.nextIndex());
	}
	
	public void testNextIndexReadOnly()
	{
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_ONLY));
		mockDelegateIter.expects(once()).method("nextIndex").withNoArguments().will(returnValue(26));
		assertEquals(26, targetIterator.nextIndex());
	}
	
	
	public void testPreviousReadWrite()
	{
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_WRITE));
		mockDelegateIter.expects(once()).method("previous").withNoArguments().will(returnValue(readWriteObj));
		assertSame(readWriteObj, targetIterator.previous());
	}
	
	public void testPreviousReadOnly()
	{
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_ONLY));
		mockDelegateIter.expects(once()).method("previous").withNoArguments().will(returnValue(readWriteObj));
		assertSame(readWriteObj, targetIterator.previous());
	}
	
	
	public void testPreviousIndexReadWrite()
	{
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_WRITE));
		mockDelegateIter.expects(once()).method("previousIndex").withNoArguments().will(returnValue(26));
		assertEquals(26, targetIterator.previousIndex());
	}
	
	public void testPreviousIndexReadOnly()
	{
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_ONLY));
		mockDelegateIter.expects(once()).method("previousIndex").withNoArguments().will(returnValue(26));
		assertEquals(26, targetIterator.previousIndex());
	}
	
	
	public void testRemoveReadOnly()
	{
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_ONLY));
		try
		{
			targetIterator.remove();
		} catch (UnsupportedOperationException uoe)
		{
			
		}
	}
	
	public void testRemoveReadWrite()
	{
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_WRITE));
		mockDelegateIter.expects(once()).method("remove").withNoArguments();
		targetIterator.remove();
	}
	
	public void testSetReadOnly()
	{
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_ONLY));
		try
		{
			targetIterator.set(readWriteObj);
		} catch (UnsupportedOperationException uoe)
		{
			
		}
	
	}
	
	public void testSetReadWrite()
	{
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_WRITE));
		mockDelegateIter.expects(once()).method("set").with(same(readWriteObj));
		targetIterator.set(readWriteObj);
	}

}
