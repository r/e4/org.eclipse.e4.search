/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.xml;

import java.io.IOException;
import java.util.Collection;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.util.api.longop.ILongOperation;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.platform.discovery.util.internal.xml.ICollectionTransformer;
import org.eclipse.platform.discovery.util.internal.xml.XMLUtils;
import org.jmock.core.Invocation;
import org.jmock.core.Stub;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public abstract class CollectionTransformerAbstractTest extends MockObjectTestCase
{

	protected Collection<?> testCollection;
	
	protected ICollectionTransformer target;
	
	protected Mock<ILongOperationRunner> lorMock;
	
	protected Mock<IProgressMonitor> pmMock;
	
	@Override
	public void setUp()
	{
		target = createCollectionTransformer();
		testCollection = createTestList();
		lorMock = mock(ILongOperationRunner.class);
		pmMock = mock(IProgressMonitor.class);
		//test is abstract. we don't know whether the lor and pm are actully used. so stub it
		lorMock.stubs().method("run").withAnyArguments().will(invokePassedOperation());
		pmMock.stubs().method("beginTask").withAnyArguments();
		pmMock.stubs().method("done").withAnyArguments();
	}
	
	protected abstract String testCollecitonTag();
	
	protected abstract String testItemTag();
	
	protected abstract String testNamespace();
	
	protected abstract Collection<?> createTestList();
	
	protected abstract ICollectionTransformer createCollectionTransformer();
	
	public void testReturnedXMLIsExpected() throws SAXException, IOException
	{
		final String result = target.transform(testCollection.iterator(), lorMock.proxy());
		final Element e = string2RootElement(result);
		assertEquals(testCollecitonTag(), e.getLocalName());
		assertEquals(testNamespace(), e.getNamespaceURI());
		final NodeList children = e.getElementsByTagNameNS(testNamespace(), testItemTag());
		for (int i = 0; i < children.getLength(); i++)
		{
			assertExpectedElement(i, (Element)children.item(i));
		}
	}
	
	protected Stub invokePassedOperation() {
		return new Stub()
		{

			@SuppressWarnings("rawtypes")
			@Override
			public Object invoke(Invocation arg0) throws Throwable {
				return ((ILongOperation)arg0.parameterValues.get(0)).run(pmMock.proxy());
			}

			@Override
			public StringBuffer describeTo(StringBuffer arg0) {
				arg0.append("execute passed long operation");
				return arg0;
			}
		};
	}

	protected abstract void assertExpectedElement(final int i, Element item);

	public Element string2RootElement(String xml) throws SAXException, IOException
	{
		return new XMLUtils().bytes2RootElement(xml.getBytes());
	}
}
