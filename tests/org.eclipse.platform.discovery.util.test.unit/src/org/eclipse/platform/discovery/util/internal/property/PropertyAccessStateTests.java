/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.property;

import org.eclipse.platform.discovery.util.internal.property.Access;
import org.eclipse.platform.discovery.util.internal.property.IProperty;
import org.eclipse.platform.discovery.util.internal.property.IPropertyAttributeListener;
import org.eclipse.platform.discovery.util.internal.property.Property;

public class PropertyAccessStateTests extends AccessAttributeTests<Object, Access>{

	

	@Override
	protected Access getAttributeValue(IProperty<Object> p) {
		return p.getAccess();
	}

	@Override
	protected Access getDefaultValue()
	{
		return Access.READ_WRITE;
	}

	@Override
	protected Access newAttributeDifferentThen(Access differentThen) {
		return Access.values()[(differentThen.ordinal() + 1) % Access.values().length];
	}

	@Override
	protected Access newNonDefaultAttributeValue() {
		return Access.READ_ONLY;
	}

	@Override
	protected IProperty<Object> newTarget() {
		return new Property<Object>();
	}

	@Override
	protected void registerAttributeListener(
			IProperty<Object> target,
			IPropertyAttributeListener<Access> listener,
			boolean current) {
		target.registerAccessListener(listener, current);
		
	}
	
	@Override
	protected boolean removeAttributeListener(
			IProperty<Object> target,
			IPropertyAttributeListener<Access> listener) {
		return target.removeAccessListener(listener);

	}


	@Override
	protected void setAttributeValue(IProperty<Object> p, Access attribute) {
		p.setAccess(attribute);
	}

}
