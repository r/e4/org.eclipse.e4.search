/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.longop;

import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.util.api.longop.ILongOperation;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.platform.discovery.util.api.longop.LongOpCanceledException;
import org.eclipse.platform.discovery.util.internal.longop.SingleForkThreadRunner;
import org.jmock.core.Invocation;
import org.jmock.core.Stub;


public class SingleForkThreadRunnerTests extends TestSuite {

	public static Test suite() {
		final TestSuite s = new TestSuite();
		s.addTestSuite(WithNoNestedOp.class);
		s.addTestSuite(WithSubOpTest.class);
		return s;
	}

	public static class WithNoNestedOp extends AbstractLongOpRunnerTests {
		
		protected Mock<Set<Thread>> trackerMock;
		private Set<Thread> trackerMockSet;
		
		@Override
		public void setUp() throws Exception {
			trackerMock = createThreadTracker();
			super.setUp();
		}

		protected Mock<Set<Thread>> createThreadTracker()
		{
			final Mock<Set<Thread>> tmpMock = mock(Set.class);
			trackerMockSet = new HashSet<Thread>();
			tmpMock.expects(once()).method("add").with(same(Thread.currentThread())).will(new Stub(){
				@Override
				public Object invoke(Invocation arg0) throws Throwable {
					return (boolean)trackerMockSet.add((Thread)arg0.parameterValues.get(0));
				}

				@Override
				public StringBuffer describeTo(StringBuffer arg0) {
					arg0.append("Marks thread as already running a long operation");
					return arg0;
				}
				
			});
			tmpMock.expects(once()).method("remove").with(same(Thread.currentThread())).will(new Stub()
			{
				@Override
				public Object invoke(Invocation arg0) throws Throwable {
					return (boolean)trackerMockSet.remove((Thread)arg0.parameterValues.get(0));
				}

				@Override
				public StringBuffer describeTo(StringBuffer arg0) {
					arg0.append("Marks the thread as not running a long operation");
					return arg0;
				}
			});
			
			tmpMock.stubs().method("contains").withAnyArguments().will(new Stub()
			{
				@Override
				public Object invoke(Invocation arg0) throws Throwable {
					return (boolean)trackerMockSet.contains((Thread)arg0.parameterValues.get(0));
				}

				@Override
				public StringBuffer describeTo(StringBuffer arg0) {
					arg0.append("Checks whether this thread is currently running a long operation");
					return arg0;
				}
				
			});
			return tmpMock;
	
		}
		
		@Override
		protected ILongOperationRunner newRunner(final IProgressMonitor monitor) {
			return new SingleForkThreadRunner(monitor, trackerMock.proxy()) {
				@Override
				public <T> T runInNewThread(ILongOperation<T> op)
						throws LongOpCanceledException,
						InvocationTargetException {
					try {
						return op.run(monitor);
					} catch (RuntimeException e) {
						throw e;
					} catch (LongOpCanceledException e) {
						throw e;
					} catch (Exception e) {
						throw new InvocationTargetException(e);
					}
				}
			};
		}
	}

	public static class WithSubOpTest extends WithNoNestedOp {

		protected ILongOperation<Object> newObjRetOp(final Object o) {
			return new ILongOperation<Object>() {
				public Object run(IProgressMonitor monitor) throws Exception {
					return target.run(new ILongOperation<Object>() {
						@Override
						public Object run(IProgressMonitor monitor)
								throws LongOpCanceledException, Exception {
							assertTrue(monitor instanceof SubProgressMonitor);
							return o;
						}
					});
				}
			};
		}

		protected ILongOperation<Object> newExcThrowingOp(final Exception e) {
			return new ILongOperation<Object>() {
				public Object run(IProgressMonitor monitor) throws Exception {
					return target.run(new ILongOperation<Object>() {
						@Override
						public Object run(IProgressMonitor monitor)
								throws LongOpCanceledException, Exception {
							assertTrue(monitor instanceof SubProgressMonitor);
							throw e;
						}
					});
				}
			};
		}

		protected ILongOperation<Object> newErrThrowingOp(final Error e) {
			return new ILongOperation<Object>() {
				public Object run(IProgressMonitor monitor) throws Exception {
					return target.run(new ILongOperation<Object>() {
						@Override
						public Object run(IProgressMonitor monitor)
								throws LongOpCanceledException, Exception {
							assertTrue(monitor instanceof SubProgressMonitor);
							throw e;
						}
					});
				}
			};
		}
	}
}
