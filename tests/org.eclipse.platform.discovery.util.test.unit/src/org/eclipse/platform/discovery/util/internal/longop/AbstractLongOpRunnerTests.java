/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.longop;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.util.api.longop.ILongOperation;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.platform.discovery.util.api.longop.LongOpCanceledException;


public abstract class AbstractLongOpRunnerTests extends MockObjectTestCase {

	protected ILongOperationRunner target;
	protected IProgressMonitor monitor;
	
	@Override
	protected void setUp() throws Exception
	{
		this.monitor = newMonitor();
		this.target = newRunner(monitor);
	}
	
	abstract protected ILongOperationRunner newRunner(IProgressMonitor monitor);
	
	protected IProgressMonitor newMonitor()
	{
		return new NullProgressMonitor();
	}
	
	public void testRuntimeNotWrapped() throws LongOpCanceledException
	{
		final RuntimeException re = new RuntimeException();
		try {
			target.run(newExcThrowingOp(re));
			failNoExc();
		} catch (InvocationTargetException e) {
			assertEquals(re, e.getCause());
			failITE();
		} catch (RuntimeException e)
		{
			assertSame(re, e);
			//ok, runtime exception cought
		}
		
	}
	
	public void testErrorNotWrapped() throws LongOpCanceledException
	{
		final Error e = new Error();
		try {
			target.run(newErrThrowingOp(e));
			failNoExc();
		} catch (InvocationTargetException ex) {
			failITE();
		} catch (Error er)
		{
			assertSame(e, er);
			//ok, error cought
		}
	}
	
	public void testOpReturnValue() throws LongOpCanceledException, InvocationTargetException
	{
		final Object o = new Object();
		assertSame(o, target.run(newObjRetOp(o)));
	}
	
	public void testCancelledExceptionThrown() throws InvocationTargetException
	{
		final LongOpCanceledException e = new LongOpCanceledException();
		try {
			target.run(newExcThrowingOp(e));
		} catch (LongOpCanceledException ce)
		{
			assertSame(e, ce);
		}
	}
	
	protected void failITE()
	{
		fail("InvokationTargetEception cought");
	}
	
	protected void failNoExc()
	{
		fail("No exception thrown");
	}
	
	protected ILongOperation<Object> newObjRetOp(final Object o)
	{
		return new ILongOperation<Object>()
		{
			public Object run(IProgressMonitor monitor)
					throws Exception {
				return o;
			}
		};
	}
	
	protected ILongOperation<Object> newExcThrowingOp(final Exception e)
	{
		return new ILongOperation<Object>()
		{
			public Object run(IProgressMonitor monitor) throws Exception {
				throw e;
			}
			
		};
	}

	protected ILongOperation<Object> newErrThrowingOp(final Error e)
	{
		return new ILongOperation<Object>()
		{
			public Object run(IProgressMonitor monitor) throws Exception {
				throw e;
			}
			
		};
	}
}


