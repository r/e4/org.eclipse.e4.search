/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.xml;

import java.util.Arrays;
import java.util.Iterator;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.util.api.xml.IXmlRepresentable;
import org.eclipse.platform.discovery.util.internal.longop.CurrentThreadOperationRunner;
import org.eclipse.platform.discovery.util.internal.xml.CollectionTransformer;
import org.eclipse.platform.discovery.util.internal.xml.Messages;
import org.jmock.core.Invocation;
import org.jmock.core.Stub;


public class CollectionTransformerProgressMonitorTest extends MockObjectTestCase
{
	private Mock<IProgressMonitor> monitor;
	private Mock<IXmlRepresentable> xmlRepr1;
	private Mock<IXmlRepresentable> xmlRepr2;
	private CollectionTransformer transformer;

	@Override
	protected void setUp() throws Exception
	{
		monitor = mock(IProgressMonitor.class);
		transformer = new CollectionTransformer();
	}
	
	public void testWithoutPm()
	{
		xmlRepr1 = xmlReprWithPm(false);
		xmlRepr2 = xmlReprWithPm(false);
		transformer.transform(testIterator(), new CurrentThreadOperationRunner(monitor.proxy()));
	}

	public void testWithPmStartedOnce1()
	{
		monitor.expects(once()).method("beginTask").with(eq(Messages.CollectionTransformer_TRANSFORMING_TO_XML), eq(IProgressMonitor.UNKNOWN));
		monitor.expects(once()).method("done");
		xmlRepr1 = xmlReprWithPm(true);
		xmlRepr2 = xmlReprWithPm(false);
		transformer.transform(testIterator(), new CurrentThreadOperationRunner(monitor.proxy()));
	}
	
	public void testWithPmStartedOnce2()
	{
		monitor.expects(once()).method("beginTask").with(eq(Messages.CollectionTransformer_TRANSFORMING_TO_XML), eq(IProgressMonitor.UNKNOWN));
		monitor.expects(once()).method("done");
		xmlRepr1 = xmlReprWithPm(false);
		xmlRepr2 = xmlReprWithPm(true);
		transformer.transform(testIterator(), new CurrentThreadOperationRunner(monitor.proxy()));
	}
	
	public void testWithPmStartedTwice()
	{
		monitor.expects(once()).method("beginTask").with(eq(Messages.CollectionTransformer_TRANSFORMING_TO_XML), eq(IProgressMonitor.UNKNOWN));
		monitor.expects(once()).method("done");
		xmlRepr1 = xmlReprWithPm(true);
		xmlRepr2 = xmlReprWithPm(true);
		transformer.transform(testIterator(), new CurrentThreadOperationRunner(monitor.proxy()));
	}
	
	private Mock<IXmlRepresentable> xmlReprWithPm(final boolean willUsePm)
	{
		final Mock<IXmlRepresentable> result = mock(IXmlRepresentable.class);
		result.stubs().method("toStringXml").will(new Stub()
		{
			@Override
			public Object invoke(Invocation arg0) throws Throwable
			{
				if(willUsePm)
				{
					((SubProgressMonitor)arg0.parameterValues.get(0)).beginTask("", 0);
				}
				return "test";
			}

			@Override
			public StringBuffer describeTo(StringBuffer arg0)
			{
				return arg0;
			}
		});
		return result;
	}

	private Iterator<?> testIterator()
	{
		return Arrays.asList(new IXmlRepresentable[] { xmlRepr1.proxy(), xmlRepr2.proxy() }).iterator();
	}
}
