/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.session;

import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.util.internal.session.ISession;
import org.eclipse.platform.discovery.util.internal.session.ISessionFactory;
import org.eclipse.platform.discovery.util.internal.session.ISessionManager;
import org.eclipse.platform.discovery.util.internal.session.SessionManager;


public class SessionManagerTests extends MockObjectTestCase {
	
	protected ISessionManager<ISession<?>> target;
	private static final String[] sessionIds = {"session1id", "session2id"};
	private ISession<?>[] testSessions;
	private Mock<ISessionFactory<ISession<?>>> factoryMock;
	
	@Override
	public void setUp()
	{
		factoryMock = mock(ISessionFactory.class);
		target = new SessionManager<ISession<?>>(factoryMock.proxy());
		testSessions = new ISession[sessionIds.length];
		for (int i = 0; i < testSessions.length; i++)
		{
			expectCreationOfNewSession(sessionIds[i], 3);
			testSessions[i] = target.session(sessionIds[i], 3);
		}
	}
	
	public void testSessionWithNewId()
	{
		final String newSessionId = "newSessionId";
		expectCreationOfNewSession(newSessionId, 1);
		final ISession<?> newSession = target.session(newSessionId, 1);
		assertEquals(newSessionId, newSession.getId());
		assertEquals(testSessions.length + 1, target.sessionCount());
	}
	
	public void testSessionWithExistingId()
	{
		for(ISession<?> ts : testSessions)
		{
			assertSame(ts, target.session(ts.getId(), 1));
		}
	}
	
	public void expectCreationOfNewSession(String sessionId, final int historySize)
	{
		factoryMock.expects(once()).method("newSession").with(eq(sessionId), eq(historySize)).will(returnValue(newSessionStub(sessionId).proxy()));
	}
	
	public Mock<ISession<?>> newSessionStub(final String sessionId)
	{
		final Mock<ISession<?>> newSession = mock(ISession.class);
		newSession.stubs().method("getId").will(returnValue(sessionId));
		newSession.stubs().method("hashCode").will(returnValue(sessionId.hashCode()));
		
		return newSession;
	}
}
