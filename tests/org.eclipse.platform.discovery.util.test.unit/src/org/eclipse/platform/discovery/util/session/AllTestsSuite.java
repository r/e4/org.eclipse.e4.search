/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.session;

import org.eclipse.platform.discovery.util.internal.longop.LoggerTest;

import junit.framework.Test;
import junit.framework.TestSuite;



public class AllTestsSuite {
		public static Test suite()
		{
			TestSuite suite = new TestSuite();

			suite.addTest(HistoryTrackTests.suite());
			suite.addTestSuite(SessionManagerTests.class);
			suite.addTestSuite(LoggerTest.class);
			return suite;
		}


}
