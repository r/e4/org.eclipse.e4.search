/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.property;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllPropertyTestsSuite {

	public static Test suite()
	{
		final TestSuite suite = new TestSuite();
		// $JUnit-BEGIN$
		suite.addTestSuite(PropertyValueTests.class );
		suite.addTestSuite(NotifyingListTests.class);
		suite.addTestSuite(ListPropertyValueTests.class);
		suite.addTestSuite(AccessCheckingListTests.class);
		suite.addTestSuite(ListPropertyListTests.class);
		suite.addTestSuite(PropertyAccessStateTests.class);
		suite.addTestSuite(ListPropertyListAccessTest.class);
		
		// $JUnit-END$
		return suite;		
	}
}
