/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.longop;

import java.lang.reflect.InvocationTargetException;
import java.util.Set;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.operation.ModalContext;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.util.api.longop.ILongOperation;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.platform.discovery.util.api.longop.LongOpCanceledException;
import org.eclipse.platform.discovery.util.internal.longop.ModalContextLongOpRunner;
import org.eclipse.ui.PlatformUI;


public class ModalContextOpRunnerTests extends TestSuite {

	protected Mock<ISchedulingRule> ruleMock;
	
	public static Test suite() {
		final ModalContextOpRunnerTests tests = new ModalContextOpRunnerTests();
		tests.addTestSuite(WithNoNestedOp.class);
		tests.addTestSuite(WithSubOpTest.class);
		tests.addTestSuite(DoesNotForkWhenAlreadyRunningInModalContext.class);
		return tests;
	}
	
	private static Mock<Set<Thread>> trackerThatCouldBeaccessedBeforeTheSuperConstructor;
	
	public static class WithNoNestedOp extends
			SingleForkThreadRunnerTests.WithNoNestedOp {

		@Override
		protected Mock<Set<Thread>> createThreadTracker() {
			return trackerThatCouldBeaccessedBeforeTheSuperConstructor = super.createThreadTracker();
		}

		@Override
		public void setUp() throws Exception {
			super.setUp();
		}

		@Override
		protected ILongOperationRunner newRunner(final IProgressMonitor monitor) {
			final Mock<ISchedulingRule> ruleMock = mock(ISchedulingRule.class);
			return new TestModalContextOpRunner(monitor, ruleMock.proxy(), trackerMock.proxy());
		}
	}
	
	public static class WithSubOpTest extends SingleForkThreadRunnerTests.WithSubOpTest
	{
		
		@Override
		protected Mock<Set<Thread>> createThreadTracker() {
			return trackerThatCouldBeaccessedBeforeTheSuperConstructor = super.createThreadTracker();
		}
		
		@Override
		public void setUp() throws Exception {
			super.setUp();
		}
	
		@Override
		protected ILongOperationRunner newRunner(final IProgressMonitor monitor) {
			final Mock<ISchedulingRule> ruleMock = mock(ISchedulingRule.class);
			return new TestModalContextOpRunner(monitor, ruleMock.proxy(), trackerMock.proxy());
		}
	}
	
	private static class TestSchedulingRuleOpRunner implements ILongOperationRunner
	{

		ILongOperationRunner wrapped;
		public TestSchedulingRuleOpRunner(ILongOperationRunner wrapped)
		{
			this.wrapped = wrapped;
		}
		
		@Override
		public <T> T run(ILongOperation<T> op) throws LongOpCanceledException,
				InvocationTargetException {
			
			return wrapped.run(op);
		}
	}
	
	private static class TestModalContextOpRunner extends ModalContextLongOpRunner {
		
		public TestModalContextOpRunner(IProgressMonitor monitor,
				ISchedulingRule rule, Set<Thread> tracker) {
			super(monitor, rule);
		}

		@Override
		protected void runInModalContext(IRunnableWithProgress rp,
				IProgressMonitor m)
				throws InvocationTargetException,
				InterruptedException {
			rp.run(m);
		}

		@Override
		protected Set<Thread> newThreadTracker() {
			return trackerThatCouldBeaccessedBeforeTheSuperConstructor.proxy();
		}

		@Override
		protected ILongOperationRunner newSchedulingRuleSynchRunner(ILongOperationRunner wrapped, ISchedulingRule rule)
		{
			return new TestSchedulingRuleOpRunner(wrapped);
		}
	}	
	
	public static class DoesNotForkWhenAlreadyRunningInModalContext extends MockObjectTestCase
	{
		public void testDoesNotFork() throws InvocationTargetException, InterruptedException
		{
			assertFalse("Test has to be rune outside ModalContext thread", ModalContext.isModalContextThread(Thread.currentThread()));
			final ILongOperationRunner runner = new ModalContextLongOpRunner(new NullProgressMonitor(), createSchedulingRule());
			final IRunnableWithProgress testRunnable = new IRunnableWithProgress()
			{
				@Override
				public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException
				{
					assertTrue("Runnable needs to be executed in ModalContext", ModalContext.isModalContextThread(Thread.currentThread()));
					final ILongOperation<Void> checkOperation = createCheckSameThreadOperation(Thread.currentThread());
					try
					{
						runner.run(checkOperation);
					} catch (LongOpCanceledException e)
					{
						throw new IllegalStateException("Unexpected exception", e);
					}
				}
			};
			
			ModalContext.run(testRunnable, true, new NullProgressMonitor(), PlatformUI.getWorkbench().getDisplay());
		}

		private ILongOperation<Void> createCheckSameThreadOperation(final Thread thread)
		{
			return new ILongOperation<Void>()
			{
				@Override
				public Void run(IProgressMonitor monitor) throws LongOpCanceledException, Exception
				{
					if(thread != Thread.currentThread())
					{
						throw new IllegalStateException("Not in same thread");
					}
					return null;
				}
				
			};
		}
		
		private ISchedulingRule createSchedulingRule() {
			return new ISchedulingRule() {
				@Override
				public boolean contains(ISchedulingRule rule) {
					return rule==this;
				}
				@Override
				public boolean isConflicting(ISchedulingRule rule) {
					return rule==this;
				}
			};
		}
	}
	
}