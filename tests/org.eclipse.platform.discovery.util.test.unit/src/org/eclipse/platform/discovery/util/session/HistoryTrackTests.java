/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.session;

import java.util.Arrays;
import java.util.NoSuchElementException;

import org.eclipse.platform.discovery.util.internal.session.HistoryTrack;
import org.eclipse.platform.discovery.util.internal.session.IHistoryTrack;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class HistoryTrackTests extends TestSuite
{
	public static Test suite()
	{
		TestSuite s = new TestSuite();
		s.addTestSuite(TestInstantiationConstraints.class);
		s.addTestSuite(TestEmpty.class);
		s.addTestSuite(TestWithOneElementNonOversized.class);
		s.addTestSuite(TestWithOneElementOversized.class);
		s.addTestSuite(TestNonOversized.class);
		s.addTestSuite(TestOversized.class);
		s.addTestSuite(TestHistoryRenewed.class);
		return s;
	}
	
	private interface HistoryManagerTestCase
	{
		void testIsEmpty();
		void testHasNext();
		void testHasPrevious();
		void testNext();
		void testPrevious();
		void testLast();
		void testFirst();
		void testAsList();
		void testCurrent();
	}
	
	private static abstract class Fixture extends TestCase implements HistoryManagerTestCase
	{
		
		protected Object[] tracked;
		protected int limit;
		public Fixture(int numTracked, int limit) {
			super();
			this.limit = limit;
			reinitializeTracked(numTracked);
		}
		
		protected void reinitializeTracked(int numTracked)
		{
			tracked = new Object[numTracked];
			for (int i = 0; i < tracked.length; i++)
			{
				tracked[i] = new Object();
			}
		}
		
		protected IHistoryTrack<Object> target;
		@Override
		public void setUp()
		{
			target = new HistoryTrack<Object>(limit);
			fillit();
		}
		
		protected void fillit()
		{
			for (int i = 0; i < tracked.length; i++)
			{
				target.track(tracked[i]);
			}
		}
		
		protected void rewind() {
			while (target.hasPrevious())
			{
				target.previous();
			}
		}
		
		protected void noSuchElExcNotThrown()
		{
			fail("NoSuchElementException not thrown");
		}
	}
	
	public static class TestEmpty extends Fixture
	{
		public TestEmpty() {
			super(0,1);
		}

		public void testAsList() {
			assertEquals(0, target.asList().size());
		}

		public void testCurrent() {
			try
			{
				target.current();
				noSuchElExcNotThrown();
			} catch (NoSuchElementException nse)
			{
			}
		}

		public void testFirst() {
			try
			{
				target.first();
				noSuchElExcNotThrown();
			} catch (NoSuchElementException nse)
			{
			}
		}
		
		public void testLast() {
			try
			{
				target.last();
				noSuchElExcNotThrown();
			} catch (NoSuchElementException nse)
			{
			}
		}

		public void testNext() {
			try
			{
				target.next();
				noSuchElExcNotThrown();
			} catch (NoSuchElementException nse)
			{
			}
			
		}

		public void testPrevious() {
			try
			{
				target.previous();
				noSuchElExcNotThrown();
			} catch (NoSuchElementException nse)
			{
			}
		}

		public void testHasNext() {
			assertFalse(target.hasNext());
		}

		public void testHasPrevious() {
			assertFalse(target.hasPrevious());
		}

		public void testIsEmpty() {
			assertTrue(target.isEmpty());
		}

		
		
	}
	
	public static class TestInstantiationConstraints extends TestCase
	{
		public void testLimitLZ()
		{
			try
			{
				new HistoryTrack<Object>(0);
				fail("IllegalArgumentException not thrown");
			} catch(IllegalArgumentException iae)
			{
			}
		}
	}
	
	public static class TestWithOneElementNonOversized extends Fixture {
		public TestWithOneElementNonOversized() {
			super(1,1);
		}

		public void testAsList() {
			assertEquals(1, target.asList().size());
			assertSame(tracked[0], target.asList().get(0));
		}

		public void testCurrent() {
			assertSame(tracked[0], target.current());
		}

		public void testFirst() {
			assertSame(tracked[0], target.first());
		}

		public void testHasNext() {
			assertFalse(target.hasNext());
		}

		public void testHasPrevious() {
			assertFalse(target.hasPrevious());
		}

		public void testIsEmpty() {
			assertFalse(target.isEmpty());
		}

		public void testLast() {
			assertSame(tracked[0], target.last());
		}

		public void testNext() {
			try {
				target.next();
				noSuchElExcNotThrown();
			} catch (NoSuchElementException nse)
			{
			}
			testHasPrevious();
			testHasNext();
		}

		public void testPrevious() {
			try {
				target.previous();
				noSuchElExcNotThrown();
			} catch (NoSuchElementException nse)
			{
				testHasPrevious();
				testHasNext();
			}
		}
	}
	
	public static class TestWithOneElementOversized extends TestWithOneElementNonOversized
	{
		//Lok at TestOversized - we apply the same strategy here. Add one element, which should
		//be the only element in the history, then add a new one which should become the only
		//element in the history and then check that all the methods behave correctly
		@Override
		public void setUp() {
			super.setUp();
			reinitializeTracked(1);
			fillit();
		}
		
	}
	
	public static class TestNonOversized extends Fixture {

		public TestNonOversized() {
			this(3,5);
		}
		
		public TestNonOversized(int trackNum, int limit) {
			super(trackNum,limit);
		}

		public void testAsList() {
			assertEquals(tracked.length, target.asList().size());
			for (int i = 0; i < tracked.length; i++)
			{
				assertEquals(tracked[i], target.asList().get(i));
			}
		}

		public void testCurrent() {
			assertSame(tracked[tracked.length -1], target.current());
		}

		public void testFirst() {
			assertSame(tracked[0], target.first());
		}

		public void testHasNext() {
			assertFalse(target.hasNext());
		}

		public void testHasPrevious() {
			assertTrue(target.hasPrevious());
		}

		public void testIsEmpty() {
			assertFalse(target.isEmpty());
		}

		public void testLast() {
			assertSame(tracked[tracked.length - 1], target.last());
			target.previous();
			assertSame(tracked[tracked.length -1], target.last());
		}

		public void testNext() {
			try {
				target.next();
				noSuchElExcNotThrown();
			} catch (NoSuchElementException nse)
			{
			}
			target.previous();
			assertTrue(target.hasNext());
			assertSame(tracked[tracked.length - 1], target.next());
			assertSame(tracked[tracked.length - 1], target.current());
			
		}

		public void testPrevious() {
			rewind();
			assertSame(tracked[0], target.current());
			try {
				target.previous();
				noSuchElExcNotThrown();
			} catch (NoSuchElementException nse)
			{
			}
			target.next();
			assertTrue(target.hasPrevious());
			assertSame(tracked[0], target.previous());
		}
	}

	public static class TestOversized extends TestNonOversized {

		//set the limits to as much as elements would initially be filled.
		//then fill the array again with the some other number of elements > then the initial.
		//Then refill the history based on the new array and run the non-oversized test.
		//It would work exactly as the array was not oversized, however this test would have
		//already done so and thus no old elements should have been left in the history and thus
		//no mismatch with the array should occur.
		public TestOversized() {
			super(3, 3);
		}
		
		@Override
		public void setUp()
		{
			super.setUp();
			reinitializeTracked(3);
			fillit();
		}
	}

	public static class TestHistoryRenewed extends TestNonOversized {

		public TestHistoryRenewed() {
			super(5, 5);
		}
		
		//In the setup go back few times. Then track a new object.
		//this should have cleared the rest of the history
		//adjust the expected objects to match the new content
		//of the history.Run tests to verify that all methods
		//work correctly with new history
		@Override
		public void setUp()
		{
			super.setUp();
			target.previous();
			target.previous();
			final Object newlyTracked = new Object();
			target.track(newlyTracked);
			tracked = Arrays.copyOf(tracked, tracked.length - 1);
			tracked[tracked.length - 1] = newlyTracked;
		}
	}
}



/*
public class HistoryManagerTest extends MockObjectTestCase {
	
	private IHistoryTrack<Object> target; 
	private Object o[] = new Object[3];
	
	@Override
	public void setUp()
	{
		target = new HistoryManager<Object>();
		for(int i = 0; i < o.length; i++)
		{
			o[i] = new Object();
		}
	
	public void testIsEmpty()
	{
		fillit();
		assertFalse(target.isEmpty());
	}
	
	public void testCurrent()
	{
		fillit();
		assertSame(o[o.length - 1], target.last());
		assertSame(o[o.length -1], target.current());
		assertNull(target.next());
	}
	
	public void testCurrentOversize()
	{
		target.setMaxSize(o.length);
		testCurrent();
	}
	
	public void testPrevious()
	{
		fillit();
		for (int i = o.length - 2; i > 0; i--)
		{
			assertSame(o[i], target.previous());
		}
		assertNull(target.previous());
	}
	
	public void testPreviousOversize()
	{
		fillit();
		target.setMaxSize(o.length);
		target.track(new Object());
		for (int i = o.length - 1; i > 1; i--)
		{
			assertSame(o[i], target.previous());
		}
		assertNull(target.previous());
	}

	public void testFirst()
	{
		fillit();
		rewind();
		assertSame(o[0], target.first());
	}
	
	public void testFirstOversize	()
	{
		fillit();
		target.setMaxSize(o.length);
		assertSame(o[0], target.first());
		target.track(new Object());
		assertSame(o[1], target.first());
	}



	private void fillit() {
		for (int i = 0; i < o.length; i++)
		{
			target.track(o[i]);
		}
	}
	
	private void testEmpty()
	{
		assertFalse(target.hasNext());
		assertFalse(target.hasPrevious());
		assertTrue(target.isEmpty());

		try
		{
			target.previous();
			noSuchElExcNotThrown();
		} catch (NoSuchElementException nse)
		{
		}
		
		try
		{
			target.current();
			noSuchElExcNotThrown();
		} catch (NoSuchElementException nse)
		{
		}
		
		try
		{
			target.next();
			noSuchElExcNotThrown();
		} catch (NoSuchElementException nse)
		{
		}

		try
		{
			target.last();
			noSuchElExcNotThrown();
		} catch (NoSuchElementException nse)
		{
	
		try
		{
			assertNull(target.first());
			noSuchElExcNotThrown();
		} catch (NoSuchElementException nse)
		{
		
		
	}
	

	
//	public void testOrderIsKept()
//	{
//		for (int i = 0; i < o.length; i++)
//		{
//			target.track(o[i]);
//		}
//		assertSame(target.last(), target.current());
//		assertSame(o[o.length-1], target.last());
//		for (int i = o.length - 1; i > 0; i--)
//		{
//			assertSame(o[i-1], target.previous());
//			assertSame(o[i-1], target.current());
//		}
//		assert
//	}
	
	
	
	
}
*/