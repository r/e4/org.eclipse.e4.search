/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.property;

import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.util.internal.property.IProperty;
import org.eclipse.platform.discovery.util.internal.property.IPropertyAttributeListener;
import org.eclipse.platform.discovery.util.internal.property.Property;
import org.eclipse.platform.discovery.util.internal.property.PropertyAttributeChangedEvent;
import org.jmock.core.Constraint;


public abstract class AttributeContractTests<T, A>  extends MockObjectTestCase {

	private IProperty<T> target;
	Mock<IPropertyAttributeListener<A>> mockListener;
	
	protected abstract IProperty<T> newTarget();
	
	protected abstract A newNonDefaultAttributeValue();
	protected abstract A newAttributeDifferentThen(A differentThen);
	
	protected abstract A getAttributeValue(IProperty<T> p);
	
	protected abstract void setAttributeValue(IProperty<T> p, A attribute);
	
	protected abstract void registerAttributeListener(IProperty<T> target, IPropertyAttributeListener<A> listener, boolean current);
	
	protected abstract boolean removeAttributeListener(IProperty<T> target, IPropertyAttributeListener<A> listener);
	
	protected abstract A getDefaultValue();
	
	@Override
	public void setUp()
	{
		setTarget(newTarget());
		mockListener = mock(IPropertyAttributeListener.class);
	}

	
	public void test_defaultAttribute()
	{
		assertEquals(getDefaultValue(), getAttributeValue(target()));
	}
	
	
	public void test_getSetAttribute()
	{
		final A oldValue = getAttributeValue(target());
		final A newValue = newAttributeDifferentThen(oldValue);
		setAttributeValue(target(), newValue);
		assertEquals(newValue, getAttributeValue(target()));
	}
	
	public void test_registerListenerNotifiesForCurrentAttribute()
	{
		mockListener.expects(once()).method("attributeChanged").with(new Constraint() {

			public boolean eval(final Object arg0)
			{
				@SuppressWarnings("unchecked")
				final PropertyAttributeChangedEvent<A> ace = (PropertyAttributeChangedEvent<A>)arg0;
				assertEquals("Current access state differs from old access state", ace.getNewAttribute(), ace.getOldAttribute());
				assertEquals("Current access state reported by the event differs from the notifying property current access state",
					getAttributeValue(target()), ace.getNewAttribute());
				assertTrue("The event reported that it was not a notification for the current state altought the current and old state were equal",
						ace.isCurrent());
				assertEquals("Notifier reported by the event was not the one used in the test", target(), ace.getNotifier());
				return true;
			}

			public StringBuffer describeTo(final StringBuffer arg0)
			{
				arg0.append("AccessChandedEvent delivered as current state notification");
				return arg0;
			}
		});
		registerAttributeListener(target(), mockListener.proxy(), true);
	}
	
	public void test_registerListenerDoesntNotofyForCurrentAttribute()
	{
		registerAttributeListener(target(), mockListener.proxy(), false);
	}
	
	public void test_listenerGetsNotifiedUpponAttributeChange()
	{
		final A oldValue = getAttributeValue(target());
		final A newValue = newAttributeDifferentThen(oldValue);
		mockListener.expects(once()).method("attributeChanged").with(new PropertyAttributeChangedEventConstraint(target(), oldValue, newValue));
		registerAttributeListener(target(), mockListener.proxy(), false);
		setAttributeValue(target(), newValue);
	}
	
	public void test_moreThenOneListenerCouldBeAdded()
	{
		final A oldAttribute = getAttributeValue(target());
		final A newAttribute = newAttributeDifferentThen(oldAttribute);
		final PropertyAttributeChangedEventConstraint rightEvent = new PropertyAttributeChangedEventConstraint(target(), oldAttribute, newAttribute);
		mockListener.expects(once()).method("attributeChanged").with(rightEvent);
		final Mock<IPropertyAttributeListener<A>> secondMockListener = mock(IPropertyAttributeListener.class);
		secondMockListener.expects(once()).method("attributeChanged").with(rightEvent);
		registerAttributeListener(target(), mockListener.proxy(), false);
		registerAttributeListener(target(),secondMockListener.proxy(), false);
		setAttributeValue(target(),newAttribute);
	}
	
	public void test_noNotificationIfNewAttributeIsTheSameAsCurrent()
	{
		registerAttributeListener(target(),mockListener.proxy(), false);
		setAttributeValue(target(), getAttributeValue(target()));
	}
	
	public void test_removeListenerReturnsFalseIfNotRegistered()
	{
		assertFalse(removeAttributeListener(target(), mockListener.proxy()));
	}
	
	public void test_removeListenerReturnsTrueIfRegistered()
	{
		registerAttributeListener(target(),mockListener.proxy(), false);
		assertTrue(removeAttributeListener(target(), mockListener.proxy()));
	}
	
	public void test_ValueChangedBeforeValueListenerNotified()
	{
		final Property<String> testProperty = new Property<String>();
		testProperty.set("OldValue");
		
		testProperty.registerValueListener(new IPropertyAttributeListener<String>(){
			public void attributeChanged(PropertyAttributeChangedEvent<String> event)
			{
				assertTrue(testProperty.get().equals("NewValue"));
			}}, false);
		testProperty.set("NewValue");
	}

	@Override
	public void tearDown()
	{
		
	}
	
	protected void setTarget(IProperty<T> target) {
		this.target = target;
	}

	protected IProperty<T> target() {
		return target;
	}

	private class PropertyAttributeChangedEventConstraint implements Constraint
	{
		private final A oldAttribute;
		private final A newAttribute;
		private final IProperty<T> notifier;
		public PropertyAttributeChangedEventConstraint(IProperty<T> notifier, final A oldAttribute, final A newAttribute)
		{
			this.oldAttribute = oldAttribute;
			this.newAttribute = newAttribute;
			this.notifier = notifier;
		}
		public boolean eval(Object arg0) {
			@SuppressWarnings("unchecked")
			final PropertyAttributeChangedEvent<A> ace = (PropertyAttributeChangedEvent<A>) arg0;
			assertEquals("New attribute value reported by the event was not the set one", newAttribute, ace.getNewAttribute());
			assertEquals("Old attribute value reported by the event was not the " + oldAttribute, oldAttribute, ace.getOldAttribute());
			assertSame("Notifier reported by the event was not the one used in the test", notifier, ace.getNotifier());
			assertFalse("isCurrent was true although this was not current attribute value event uppon listener registration", ace.isCurrent());
			return true;
		}
		public StringBuffer describeTo(final StringBuffer arg0) {
			arg0.append("Attribute changed to " + newAttribute);
			return arg0;
		}
	}

}
