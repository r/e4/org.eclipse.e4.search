/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.property;

import org.eclipse.platform.discovery.util.internal.property.Access;
import org.eclipse.platform.discovery.util.internal.property.IProperty;
import org.eclipse.platform.discovery.util.internal.property.IPropertyAttributeListener;
import org.eclipse.platform.discovery.util.internal.property.Property;

public class PropertyValueTests extends AttributeContractTests<Object, Object>
{

	@Override
	protected Object getAttributeValue(IProperty<Object> p) {
		return p.get();
	}

	@Override
	protected Object getDefaultValue() {
		return null;
	}

	@Override
	protected Object newAttributeDifferentThen(Object differentThen) {
		return new Object();
	}

	@Override
	protected Object newNonDefaultAttributeValue() {
		return new Object();
	}

	@Override
	protected IProperty<Object> newTarget() {
		return new Property<Object>();
	}

	@Override
	protected void registerAttributeListener(IProperty<Object> target,
			IPropertyAttributeListener<Object> listener, boolean current) {
		target.registerValueListener(listener, current);
		
	}

	@Override
	protected boolean removeAttributeListener(IProperty<Object> target,
			IPropertyAttributeListener<Object> listener) {
		return target.removeValueListener(listener);
	}

	@Override
	protected void setAttributeValue(IProperty<Object> p, Object attribute) {
		p.set(attribute);
	}
	
	public void test_getReturnsPreviouslySetValue()
	{
		final Object newValue = newNonDefaultAttributeValue();
		target().set(newValue);
		assertEquals(newValue, target().get());
	}

	
	public void test_setThrowsIllegalStateIfReadOnly()
	{
		target().setAccess(Access.READ_ONLY);
		try
		{
			target().set(newNonDefaultAttributeValue());
			fail("IllegalStateException was not thrown when an attemt to set the value of a read only property was made");
		} catch (UnsupportedOperationException ise)
		{
		}
	}
	
	public void test_getReturnsValueIfReadOnly()
	{
		final Object value = newNonDefaultAttributeValue();
		target().set(value);
		target().setAccess(Access.READ_ONLY);
		assertEquals("Read value doesn't match the value previously set to the property", value, target().get());
	}
	
	@Override
	public void tearDown()
	{
		
	}
}