/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.xml;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

import org.eclipse.core.runtime.IAdapterManager;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.util.api.longop.ILongOperation;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.platform.discovery.util.api.longop.LongOpCanceledException;
import org.eclipse.platform.discovery.util.api.xml.IXmlRepresentable;
import org.eclipse.platform.discovery.util.internal.xml.CollectionTransformer;
import org.jmock.core.Invocation;
import org.jmock.core.Stub;


public class CollectionTransformerAdapterLoadingTest extends MockObjectTestCase
{
	private static final String XML_REPR = "This is a test XML representation";
	private CollectionTransformer transformer;
	private Mock<IAdapterManager> adapterManager;
	private Mock<IXmlRepresentable> xmlRepresentable;

	@Override
	protected void setUp() throws Exception
	{
		xmlRepresentable = mock(IXmlRepresentable.class);
		xmlRepresentable.stubs().method("toStringXml").will(returnValue(XML_REPR));

		adapterManager = mock(IAdapterManager.class);

		transformer = new CollectionTransformer()
		{
			@Override
			protected IAdapterManager getAdapterManager()
			{
				return adapterManager.proxy();
			}
		};
	}

	public void testFactoryIsOnlyLoadedOnce()
	{
		final Object objectToAdapt = new Object();
		adapterManager.expects(once()).method("loadAdapter").with(same(objectToAdapt), eq(IXmlRepresentable.class.getName())).will(returnValue(xmlRepresentable.proxy()));
		adapterManager.expects(exactly(2)).method("getAdapter").with(same(objectToAdapt), eq(IXmlRepresentable.class)).will(new Stub()
		{
			private boolean alreadyInvoked = false;

			@Override
			public Object invoke(Invocation arg0) throws Throwable
			{
				if (alreadyInvoked)
				{
					return xmlRepresentable.proxy();
				}
				alreadyInvoked = true;
				return null;
			}

			@Override
			public StringBuffer describeTo(StringBuffer arg0)
			{
				return arg0;
			}
		});

		final String transf1 = transformer.transform(Arrays.asList(new Object[] { objectToAdapt }).iterator(), new TestOpRunner());
		final String transf2 = transformer.transform(Arrays.asList(new Object[] { objectToAdapt }).iterator(), new TestOpRunner());
		assertEquals("Transofrmations were not same", transf1, transf2);
		assertTrue("Transformation does not contain XML", transf1.indexOf(XML_REPR) > -1);
		assertTrue("Transformation does not contain XML", transf2.indexOf(XML_REPR) > -1);
	}

	private class TestOpRunner implements ILongOperationRunner
	{

		@Override
		public <T> T run(ILongOperation<T> op) throws LongOpCanceledException, InvocationTargetException
		{
			try
			{
				return op.run(new NullProgressMonitor());
			} catch (Exception e)
			{
				throw new InvocationTargetException(e);
			}
		}

	}
}
