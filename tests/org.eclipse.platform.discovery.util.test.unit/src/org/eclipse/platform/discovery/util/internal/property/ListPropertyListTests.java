/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.property;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.util.internal.property.IListPropertyListener;
import org.eclipse.platform.discovery.util.internal.property.ListProperty;
import org.eclipse.platform.discovery.util.internal.property.PropertyListChangedEvent;
import org.eclipse.platform.discovery.util.internal.property.PropertyListChangedEvent.ChangeKind;

public class ListPropertyListTests extends MockObjectTestCase
{
	private ListProperty<Object> target;
	private List<Object> objectsInCollection;
	private Mock<IListPropertyListener<Object>> mockListener;
	
	@Override
	public void setUp()
	{
		target = new ListProperty<Object>();
		objectsInCollection = new ArrayList<Object>();
		for (int i = 0; i < 3; i++)
		{
			objectsInCollection.add(new Object());
		}
		target.get().addAll(objectsInCollection);
		mockListener = mock(IListPropertyListener.class);
		
	}
	
	public void test_resigerListenerDoesntDeliverCurrent()
	{
		target.registerCollectionChangedListener(mockListener.proxy(), false);
	}
	
	public void test_registerListenerDeliversCurrent()
	{
		mockListener.expects(once()).method("listChanged").with(new ExpectedListChangedEventConstraint(target,PropertyListChangedEvent.ChangeKind.ELEMENT_ADDED, objectsInCollection));
		target.registerCollectionChangedListener(mockListener.proxy(), true);
	}
	
	public void testRegisterListenerHasNoEffectIfAlreadyRegistered()
	{
		//call it with true in order to verify that current elements were not
		//delivered as well as that no listener gets notified upon subsequent
		//change
		final Object addedObject = new Object();
		assertTrue(target.registerCollectionChangedListener(mockListener.proxy(), false));
		assertFalse(target.registerCollectionChangedListener(mockListener.proxy(), true));
		mockListener.expects(once()).method("listChanged").with(new ExpectedSingleListChangedEventConstraint(target, ChangeKind.ELEMENT_ADDED, addedObject));
		target.get().add(addedObject);
	}	
}
