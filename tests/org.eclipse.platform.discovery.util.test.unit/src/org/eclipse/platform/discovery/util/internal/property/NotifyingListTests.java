/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.property;


import static org.eclipse.platform.discovery.util.internal.property.PropertyListChangedEvent.ChangeKind.ELEMENT_ADDED;
import static org.eclipse.platform.discovery.util.internal.property.PropertyListChangedEvent.ChangeKind.ELEMENT_REMOVED;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.util.internal.property.PropertyListChangedEvent.ChangeKind;

public class NotifyingListTests extends MockObjectTestCase {
	
	private Mock<List<Object>> mockDelegateList;
	private Mock<ListIterator<Object>> mockDelegateIterator;
	private Mock<IListProperty<Object>> mockProperty;
	private Mock<IListPropertyListener<Object>> mockListener;
	private Set<IListPropertyListener<Object>> listeners;
	private NotifyingList<Object> target;
	
	private final Object[] readWrite = new Object[] {new Object(), new Object(), new Object()};
	
	@Override
	public void setUp()
	{
		mockDelegateIterator = mock(ListIterator.class);
		mockDelegateList = mock(List.class);
		mockDelegateList.stubs().method("toArray").will(returnValue(readWrite));
		mockDelegateList.stubs().method("size").withNoArguments().will(returnValue(readWrite.length));
		mockDelegateList.stubs().method("iterator").withNoArguments().will(returnValue(mockDelegateIterator.proxy()));
		mockDelegateList.stubs().method("listIterator").withNoArguments().will(returnValue(mockDelegateIterator.proxy()));
		mockDelegateList.stubs().method("listIterator").with(eq(0)).will(returnValue(mockDelegateIterator.proxy()));
	
		mockProperty = mock(IListProperty.class);
		mockListener = mock(IListPropertyListener.class);
		mockListener.stubs().method("hashCode").will(returnValue(26));
		mockListener.stubs().method("equals").with(same(mockListener.proxy())).will(returnValue(true));
		listeners = new HashSet<IListPropertyListener<Object>>();
		listeners.add(mockListener.proxy());
		target = new NotifyingList<Object>(mockDelegateList.proxy(), new ListenerMaintainerImpl());
	}
	
	private class ListenerMaintainerImpl implements ListChangeListenerMaintainer<Object>
	{
		
		public void fireEvent(ChangeKind kind, Collection<? extends Object> elements) {
			final PropertyListChangedEvent<Object> event = new PropertyListChangedEvent<Object>(mockProperty.proxy(), kind, elements);
			fireEvent(event);

		}

		public void fireSingleEvent(ChangeKind kind, Object element) {
			final PropertyListChangedEvent<Object> event = new PropertyListChangedEvent<Object>(mockProperty.proxy(), kind, Arrays.asList(new Object[] {element}));
			fireEvent(event);
		}

		private void fireEvent(final PropertyListChangedEvent<Object> event)
		{
			for (IListPropertyListener<Object> l : listeners)
			{
				l.listChanged(event);
			}
		}
	}
	
	public void testAddIndex() {
		mockDelegateList.expects(once()).method("add").with(eq(26), same(readWrite[0]));
		mockListener.expects(once()).method("listChanged").with(new ExpectedSingleListChangedEventConstraint(mockProperty.proxy(), ELEMENT_ADDED, readWrite[0]));
		target.add(26, readWrite[0]);
	}
	
	
	public void testAdd() {
		mockDelegateList.expects(once()).method("add").with(same(readWrite[0])).will(returnValue(true));
		mockListener.expects(once()).method("listChanged").with(new ExpectedSingleListChangedEventConstraint(mockProperty.proxy(), ELEMENT_ADDED, readWrite[0]));
		assertTrue(target.add(readWrite[0]));
	}
	
	public void testAddAll() {
		final List<Object> toAdd = Arrays.asList(readWrite);
		mockDelegateList.expects(once()).method("addAll").with(same(toAdd)).will(returnValue(true));
		mockListener.expects(once()).method("listChanged").with(new ExpectedListChangedEventConstraint(mockProperty.proxy(), ELEMENT_ADDED, toAdd));
		assertTrue(target.addAll(toAdd));
	}
	

	public void testAddAllIndex() {
		final List<Object> toAdd = Arrays.asList(readWrite);
		mockDelegateList.expects(once()).method("addAll").with(eq(26),same(toAdd)).will(returnValue(true));
		mockListener.expects(once()).method("listChanged").with(new ExpectedListChangedEventConstraint(mockProperty.proxy(), ELEMENT_ADDED, toAdd));
		assertTrue(target.addAll(26, toAdd));
	}

	public void testClear() {
		mockDelegateList.expects(once()).method("clear").withNoArguments();
		mockListener.expects(once()).method("listChanged").with(new ExpectedListChangedEventConstraint(mockProperty.proxy(), ELEMENT_REMOVED, Arrays.asList(readWrite)));
		target.clear();
	}

	public void testContains() {
		mockDelegateList.expects(once()).method("contains").with(same(readWrite[0])).will(returnValue(true));
		assertTrue(target.contains(readWrite[0]));
	}

	public void testContainsAll() {
		final Collection<Object> contains = Arrays.asList(readWrite);
		mockDelegateList.expects(once()).method("containsAll").with(eq(Arrays.asList(readWrite))).will(returnValue(true));
		assertTrue(target.containsAll(contains));
	}

	public void testGet() {
		mockDelegateList.expects(once()).method("get").with(eq(26)).will(returnValue(readWrite[0]));
		assertSame(readWrite[0], target.get(26));
	}

	public void testIndexOf() {
		mockDelegateList.expects(once()).method("indexOf").with(eq(readWrite[0])).will(returnValue(26));
		assertEquals(26, target.indexOf(readWrite[0]));
	}

	
	public void testLastIndexOf() {
		mockDelegateList.expects(once()).method("lastIndexOf").with(eq(readWrite[0])).will(returnValue(26));
		assertEquals(26, target.lastIndexOf(readWrite[0]));

	}
	
	public void testIsEmpty() {
		mockDelegateList.expects(once()).method("isEmpty").withNoArguments().will(returnValue(true));
		assertTrue(target.isEmpty());
	}

	public void testIterator() {
		assertEquals("org.eclipse.platform.discovery.util.internal.property.NotifyingList$NotifyingListIterator", target.iterator().getClass().getName());
	}

	public void testListIterator() {
		assertEquals("org.eclipse.platform.discovery.util.internal.property.NotifyingList$NotifyingListIterator", target.listIterator().getClass().getName());
	}

	public void testListIteratorIndex() {
		assertEquals("org.eclipse.platform.discovery.util.internal.property.NotifyingList$NotifyingListIterator", target.listIterator(0).getClass().getName());
	}

	public void testRemoveIndex() {
		mockDelegateList.expects(once()).method("remove").with(eq(26)).will(returnValue(readWrite[0]));
		mockDelegateList.stubs().method("get").with(eq(26)).will(returnValue(readWrite[0]));
		mockListener.expects(once()).method("listChanged").with(new ExpectedSingleListChangedEventConstraint(mockProperty.proxy(), ELEMENT_REMOVED, readWrite[0]));
		assertSame(readWrite[0], target.remove(26));
	}

	public void testRemove() {
		mockDelegateList.expects(once()).method("remove").with(same(readWrite[0])).will(returnValue(true));
		mockDelegateList.stubs().method("contains").with(same(readWrite[0])).will(returnValue(true));
		mockListener.expects(once()).method("listChanged").with(new ExpectedSingleListChangedEventConstraint(mockProperty.proxy(), ELEMENT_REMOVED, readWrite[0]));
		assertSame(true, target.remove(readWrite[0]));
	}

	public void testRemoveAll() {
		final List<Object> reallyRemoved = Arrays.asList(readWrite).subList(0, 2);
		final Collection<Object> toRemoveAll = new ArrayList<Object>(reallyRemoved);
		toRemoveAll.add(new Object());
		toRemoveAll.add(new Object());
		mockDelegateList.expects(once()).method("removeAll").with(same(toRemoveAll)).will(returnValue(true));
		mockListener.expects(once()).method("listChanged").with(new ExpectedListChangedEventConstraint(mockProperty.proxy(), ELEMENT_REMOVED, reallyRemoved));
		assertTrue(target.removeAll(toRemoveAll));
	}

	public void testRetainAll() {
		final List<Object> reallyRemoved = Arrays.asList(readWrite).subList(0, 2);
		final Collection<Object> toRetainAll = new ArrayList<Object>(Arrays.asList(readWrite).subList(2,3));
		toRetainAll.add(new Object());
		toRetainAll.add(new Object());
		mockDelegateList.expects(once()).method("retainAll").with(same(toRetainAll)).will(returnValue(true));
		mockListener.expects(once()).method("listChanged").with(new ExpectedListChangedEventConstraint(mockProperty.proxy(), ELEMENT_REMOVED, reallyRemoved));
		assertTrue(target.retainAll(toRetainAll));
	}
	
	public void testSet() {
		final Object toSet = new Object();
		mockDelegateList.expects(once()).method("set").with(eq(1), same(toSet)).will(returnValue(readWrite[1]));
		mockListener.expects(once()).method("listChanged").with(new ExpectedSingleListChangedEventConstraint(mockProperty.proxy(), ELEMENT_REMOVED, readWrite[1]));
		mockListener.expects(once()).method("listChanged").with(new ExpectedSingleListChangedEventConstraint(mockProperty.proxy(), ELEMENT_ADDED, toSet));
		assertSame(readWrite[1], target.set(1, toSet));
	}

	public void testSize() {
		mockDelegateList.expects(once()).method("size").withNoArguments().will(returnValue(26));
		assertEquals(26, target.size());
	}

	public void testSubList() {
		final List<Object> subList = Arrays.asList(readWrite);
		mockDelegateList.expects(once()).method("subList").with(eq(1),eq(2)).will(returnValue(subList));
		assertSame(subList,target.subList(1,2));
	}

	public void testToArray() {
		mockDelegateList.expects(once()).method("toArray").withNoArguments().will(returnValue(readWrite));
		assertSame(readWrite, target.toArray());
	}

	public void testListIteratorAdd()
	{
		mockDelegateIterator.expects(once()).method("add").with(same(readWrite[0]));
		mockListener.expects(once()).method("listChanged").with(new ExpectedSingleListChangedEventConstraint(mockProperty.proxy(), ELEMENT_ADDED, readWrite[0]));
		target.listIterator().add(readWrite[0]);
	}
	
	public void testListIteratorHasNext() {
		mockDelegateIterator.expects(once()).method("hasNext").withNoArguments().will(returnValue(true));
		assertTrue(target.listIterator().hasNext());
	}

	public void testIteratorHasPrevious() {
		mockDelegateIterator.expects(once()).method("hasPrevious").withNoArguments().will(returnValue(true));
		assertTrue(target.listIterator().hasPrevious());

	}

	public void testIteratorNext()
	{
		mockDelegateIterator.expects(once()).method("next").withNoArguments().will(returnValue(readWrite[0]));
		assertSame(readWrite[0], target.listIterator().next());
	}


	public void testIteratorPrevious()
	{
		mockDelegateIterator.expects(once()).method("previous").withNoArguments().will(returnValue(readWrite[0]));
		assertSame(readWrite[0], target.listIterator().previous());
	}

	public void testIteratorNextIndex()
	{
		mockDelegateIterator.expects(once()).method("nextIndex").withNoArguments().will(returnValue(26));
		assertEquals(26, target.listIterator().nextIndex());
	}

	public void previousIndex()
	{
		mockDelegateIterator.expects(once()).method("previousIndex").withNoArguments().will(returnValue(26));
		assertEquals(26, target.listIterator().previousIndex());
	}

	
	public void testIteratorRemoveAfterPrevious()
	{
		mockDelegateIterator.stubs().method("previous").withNoArguments().will(returnValue(readWrite[0]));
		mockDelegateIterator.expects(once()).method("remove").withNoArguments().id("callToRemove");
		mockListener.expects(once()).method("listChanged").with(new ExpectedSingleListChangedEventConstraint(mockProperty.proxy(), ELEMENT_REMOVED, readWrite[0])).after(mockDelegateIterator, "callToRemove");
		final ListIterator<Object> targetIterator = target.listIterator();
		assertSame(readWrite[0], targetIterator.previous());
		targetIterator.remove();
	}
	
	public void testIteratorRemoveAfterNext()
	{
		mockDelegateIterator.expects(once()).method("next").withNoArguments().will(returnValue(readWrite[0]));
		mockDelegateIterator.expects(once()).method("remove").withNoArguments().id("callToRemove");
		mockListener.expects(once()).method("listChanged").with(new ExpectedSingleListChangedEventConstraint(mockProperty.proxy(), ELEMENT_REMOVED, readWrite[0])).after(mockDelegateIterator, "callToRemove");
		final ListIterator<Object> targetIterator = target.listIterator();
		assertSame(readWrite[0], targetIterator.next());
		targetIterator.remove();
	}
	
	public void testIteratorRemoveAfterSet()
	{
		final ListIterator<Object> targetIterator = target.listIterator();
		testIteratorSet(targetIterator);
		mockDelegateIterator.expects(once()).method("remove").withNoArguments().id("callToRemove");
		mockListener.expects(once()).method("listChanged").with(new ExpectedSingleListChangedEventConstraint(mockProperty.proxy(), ELEMENT_REMOVED, readWrite[1])).after(mockDelegateIterator, "callToRemove");
		targetIterator.remove();
		
	}
	
	public void testIteratorSet()
	{
		final ListIterator<Object> targetIterator = target.listIterator();
		testIteratorSet(targetIterator);
	}
	
	private void testIteratorSet(final ListIterator<Object> targetIterator)
	{
		mockDelegateIterator.expects(once()).method("set").with(same(readWrite[1])).id("callToSet");
		mockDelegateIterator.expects(once()).method("next").withNoArguments().will(returnValue(readWrite[0]));
		mockListener.expects(once()).method("listChanged").with(new ExpectedSingleListChangedEventConstraint(mockProperty.proxy(), ELEMENT_REMOVED, readWrite[0])).after(mockDelegateIterator, "callToSet");
		mockListener.expects(once()).method("listChanged").with(new ExpectedSingleListChangedEventConstraint(mockProperty.proxy(), ELEMENT_ADDED, readWrite[1])).after(mockDelegateIterator, "callToSet");
		assertSame(readWrite[0], targetIterator.next());
		targetIterator.set(readWrite[1]);
	}
}
