/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.property;

import java.util.List;

import org.eclipse.platform.discovery.util.internal.property.Access;
import org.eclipse.platform.discovery.util.internal.property.IListProperty;
import org.eclipse.platform.discovery.util.internal.property.IProperty;
import org.eclipse.platform.discovery.util.internal.property.IPropertyAttributeListener;
import org.eclipse.platform.discovery.util.internal.property.ListProperty;

public class ListPropertyListAccessTest extends AccessAttributeTests<List<Object>, Access>
{

	@Override
	protected Access getAttributeValue(IProperty<List<Object>> p) {
		IListProperty<Object> lp = (IListProperty<Object>)p;
		return lp.getListAccess();
	}

	@Override
	protected Access getDefaultValue() {
		return Access.READ_WRITE;
	}

	@Override
	protected Access newAttributeDifferentThen(Access differentThen) {
		return Access.values()[(differentThen.ordinal() + 1) % Access.values().length];
	}

	@Override
	protected Access newNonDefaultAttributeValue() {
		return Access.READ_ONLY;
	}

	@Override
	protected IProperty<List<Object>> newTarget() {
		return new ListProperty<Object>();
	}

	@Override
	protected void registerAttributeListener(IProperty<List<Object>> target,
			IPropertyAttributeListener<Access> listener, boolean current) {
		final IListProperty<Object> lp = (IListProperty<Object>)target;
		lp.registerListAccessListener(listener, current);
	}

	@Override
	protected boolean removeAttributeListener(IProperty<List<Object>> target,
			IPropertyAttributeListener<Access> listener) {
		final IListProperty<Object> lp = (IListProperty<Object>)target;
		return lp.removeListAccesssListener(listener);
	}

	@Override
	protected void setAttributeValue(IProperty<List<Object>> p, Access attribute) {
		final IListProperty<Object> lp = (IListProperty<Object>)p;
		lp.setListAccess(attribute);
	}
	
}
