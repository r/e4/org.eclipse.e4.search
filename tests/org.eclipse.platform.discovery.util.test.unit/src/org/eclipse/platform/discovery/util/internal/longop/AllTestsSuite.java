/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.longop;

import junit.framework.Test;
import junit.framework.TestSuite;



public class AllTestsSuite extends TestSuite {

	public static Test suite() {
		final ModalContextOpRunnerTests tests = new ModalContextOpRunnerTests();
		tests.addTest(ModalContextOpRunnerTests.suite());
		tests.addTestSuite(SchedulingRuleSynchRunnerTests.class);
		tests.addTest(SingleForkThreadRunnerTests.suite());
		tests.addTestSuite(CurrentThreadOperationRunnerTest.class);
		return tests;
	}
}
