/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.property;

public abstract class AccessAttributeTests<T, A> extends AttributeContractTests<T, A> {

	public void test_setThrowsNPE()
	{
		try
		{
			this.setAttributeValue(target(), null);
			fail("NullPointerException not thrown when null set for access");
		} catch (NullPointerException npe)
		{
			
		}
	}
	
}
