/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.longop;

import org.eclipse.platform.discovery.util.internal.logging.ILogger;
import org.eclipse.platform.discovery.util.internal.logging.Logger;

import junit.framework.TestCase;


public class LoggerTest extends TestCase
{
	public void testLogging()
	{
		final ILogger logger = Logger.instance();
		logger.logError("This is a test error", new RuntimeException("This is a test RTE"));	
	}
}
