/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.longop;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.jobs.IJobManager;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.util.api.longop.ILongOperation;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.platform.discovery.util.internal.longop.ExceptionHelper;
import org.eclipse.platform.discovery.util.internal.longop.SchedulingRuleSynchRunner;
import org.jmock.core.Invocation;
import org.jmock.core.Stub;


public class SchedulingRuleSynchRunnerTests extends AbstractLongOpRunnerTests {

	private Mock<ISchedulingRule> ruleMock;
	private Mock<IJobManager> jmMock;
	private Mock<ILongOperationRunner> decoratedMock;

	public void setUp() throws Exception {
		ruleMock = mock(ISchedulingRule.class);
		jmMock = mock(IJobManager.class);

		decoratedMock = mock(ILongOperationRunner.class);
		decoratedMock.expects(once()).method("run").with(
				isA(ILongOperation.class)).will(new Stub() {

			@SuppressWarnings("rawtypes")
			@Override
			public Object invoke(Invocation arg0) throws Throwable {
				try {
					return ((ILongOperation) arg0.parameterValues.get(0)).run(monitor);
				} catch (Exception e) {
					throw ExceptionHelper.rethrow(e);
				}
			}

			@Override
			public StringBuffer describeTo(StringBuffer arg0) {
				arg0.append("call the passed long running operation");
				return arg0;
			}

		});
		
		final String beginRuleId = "beginRule called";
		
		super.setUp();
		
		jmMock.expects(once()).method("beginRule").with(same(ruleMock.proxy()),
				same(monitor)).id(beginRuleId);
		jmMock.expects(once()).method("endRule").with(same(ruleMock.proxy()))
				.after(jmMock, beginRuleId);
	}

	@Override
	protected ILongOperationRunner newRunner(IProgressMonitor monitor) {
		return new SchedulingRuleSynchRunner(decoratedMock.proxy(), ruleMock.proxy(), jmMock.proxy());
	}

}
