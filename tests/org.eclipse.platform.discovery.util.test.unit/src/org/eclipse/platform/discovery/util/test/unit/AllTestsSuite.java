/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.test.unit;

import org.eclipse.platform.discovery.util.internal.property.AllPropertyTestsSuite;
import org.eclipse.platform.discovery.util.session.SessionManagerTests;
import org.eclipse.platform.discovery.util.xml.CollectionTransformerAdapterLoadingTest;
import org.eclipse.platform.discovery.util.xml.CollectionTransformerProgressMonitorTest;
import org.eclipse.platform.discovery.util.xml.CollectionTransformerTest;
import org.eclipse.platform.discovery.util.xml.PluginXmlUtilsTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	org.eclipse.platform.discovery.util.internal.longop.AllTestsSuite.class,
	CollectionTransformerTest.class,
	AllPropertyTestsSuite.class,
	SessionManagerTests.class,
	CollectionTransformerAdapterLoadingTest.class,
	CollectionTransformerProgressMonitorTest.class,
	PluginXmlUtilsTest.class
})
public class AllTestsSuite{}
