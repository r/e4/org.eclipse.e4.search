/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.property;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.util.internal.property.Access;
import org.eclipse.platform.discovery.util.internal.property.IListProperty;


public class AccessCheckingListTests extends MockObjectTestCase {
	
	private Mock<List<Object>> mockListDelegate;
	private Mock<IListProperty<Object>> mockProperty;
	private AccessCheckingList<Object> target;
	private Object readWriteObj = new Object();
	
	@Override
	public void setUp()
	{
		mockListDelegate = mock(List.class);
		mockProperty = mock(IListProperty.class);
		target = new AccessCheckingList<Object>(mockListDelegate.proxy(), mockProperty.proxy());
	}
	
	public void testAddIndexReadOnly() {
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_ONLY));
		try
		{
			target.add(1, readWriteObj);
		} catch (UnsupportedOperationException uoe)
		{
			
		}
	}
	
	public void testAddIndexReadWrite() {
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_WRITE));
		mockListDelegate.expects(once()).method("add").with(eq(1), same(readWriteObj));
		target.add(1, readWriteObj);
	}

	public void testAddReadOnly() {
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_ONLY));
		try
		{
			target.add(readWriteObj);
		} catch (UnsupportedOperationException uoe)
		{
			
		}
	}
	
	public void testAddReadWrite() {
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_WRITE));
		mockListDelegate.expects(once()).method("add").with(same(readWriteObj)).will(returnValue(true));
		assertTrue(target.add(readWriteObj));
	}

	
	public void testAddAllReadOnly() {
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_ONLY));
		try
		{
			final List<Object> toAdd = new ArrayList<Object>(1);
			toAdd.add(readWriteObj);
			target.addAll(toAdd);
		} catch (UnsupportedOperationException uoe)
		{
			
		}
	}
	
	public void testAddAllReadWrite() {
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_WRITE));
		final List<Object> toAdd = new ArrayList<Object>(1);
		toAdd.add(readWriteObj);
		mockListDelegate.expects(once()).method("addAll").with(same(toAdd)).will(returnValue(true));
		assertTrue(target.addAll(toAdd));
	}


	public void testAddAllIndexReadOnly() {
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_ONLY));
		try
		{
			final List<Object> toAdd = new ArrayList<Object>(1);
			toAdd.add(readWriteObj);
			target.addAll(0, toAdd);
		} catch (UnsupportedOperationException uoe)
		{
			
		}
	}
	
	public void testAddAllIndexReadWrite() {
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_WRITE));
		final List<Object> toAdd = new ArrayList<Object>(1);
		toAdd.add(readWriteObj);
		mockListDelegate.expects(once()).method("addAll").with(eq(0), same(toAdd)).will(returnValue(true));
		assertTrue(target.addAll(0, toAdd));
	}

	public void testClearReadOnly() {
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_ONLY));
		try
		{
			target.clear();
		} catch (UnsupportedOperationException uoe)
		{
			
		}
	
	}
	
	public void testClearReadWrite() {
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_WRITE));
		mockListDelegate.expects(once()).method("clear").withNoArguments();
		target.clear();

	}
	
	public void testRemoveIndexReadOnly() {
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_ONLY));
		try
		{
			target.remove(1);
		} catch (UnsupportedOperationException uoe)
		{
			
		}
	}
	
	public void testRemoveIndexReadWrite() {
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_WRITE));
		mockListDelegate.expects(once()).method("remove").with(eq(1));
		target.remove(1);
	}

	public void testRemoveReadOnly() {
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_ONLY));
		try
		{
			target.remove(readWriteObj);
		} catch (UnsupportedOperationException uoe)
		{
			
		}
	}
	
	public void testRemoveReadWrite() {
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_WRITE));
		mockListDelegate.expects(once()).method("remove").with(same(readWriteObj)).will(returnValue(true));
		assertTrue(target.remove(readWriteObj));
	}

	public void testRemoveAllReadOnly() {
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_ONLY));
		try
		{
			final List<Object> toAdd = new ArrayList<Object>(1);
			toAdd.add(readWriteObj);
			target.removeAll(toAdd);
		} catch (UnsupportedOperationException uoe)
		{
			
		}
	}
	
	public void testRemoveAllReadWrite() {
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_WRITE));
		final List<Object> toAdd = new ArrayList<Object>(1);
		toAdd.add(readWriteObj);
		mockListDelegate.expects(once()).method("removeAll").with(same(toAdd)).will(returnValue(true));
		assertTrue(target.removeAll(toAdd));
	}
	
	public void testRetainAllReadOnly() {
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_ONLY));
		try
		{
			final List<Object> toAdd = new ArrayList<Object>(1);
			toAdd.add(readWriteObj);
			target.retainAll(toAdd);
		} catch (UnsupportedOperationException uoe)
		{
			
		}
	}
	
	public void testRetainAllReadWrite() {
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_WRITE));
		final List<Object> toAdd = new ArrayList<Object>(1);
		toAdd.add(readWriteObj);
		mockListDelegate.expects(once()).method("retainAll").with(same(toAdd)).will(returnValue(true));
		assertTrue(target.retainAll(toAdd));
	}

	public void testSetReadOnly() {
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_ONLY));
		try
		{
			target.set(1, readWriteObj);
		} catch (UnsupportedOperationException uoe)
		{
			
		}
	}
	
	public void testSetReadWrite() {
		mockProperty.stubs().method("getListAccess").withNoArguments().will(returnValue(Access.READ_WRITE));
		mockListDelegate.expects(once()).method("set").with(eq(1), same(readWriteObj));
		target.set(1, readWriteObj);
	}
	
	public void testContains() {
		mockListDelegate.expects(once()).method("contains").with(same(readWriteObj)).will(returnValue(true));
		assertTrue(target.contains(readWriteObj));
	}

	public void testContainsAll() {
		final List<Object> contains = new ArrayList<Object>(1);
		contains.add(readWriteObj);
		mockListDelegate.expects(once()).method("containsAll").with(same(contains)).will(returnValue(true));
		assertTrue(target.containsAll(contains));
	
	}

	public void testGet() {
		mockListDelegate.expects(once()).method("get").with(eq(26)).will(returnValue(readWriteObj));
		assertSame(readWriteObj, target.get(26));
	}

	public void testIndexOf() {
		mockListDelegate.expects(once()).method("indexOf").with(same(readWriteObj)).will(returnValue(26));
		assertEquals(26, target.indexOf(readWriteObj));
	}

	public void testLastIndexOf() {
		mockListDelegate.expects(once()).method("lastIndexOf").with(same(readWriteObj)).will(returnValue(26));
		assertEquals(26, target.lastIndexOf(readWriteObj));
	}
	
	public void testIsEmpty() {
		mockListDelegate.expects(once()).method("isEmpty").withNoArguments().will(returnValue(true));
		assertTrue(target.isEmpty());
	}

	public void testIterator() {
		mockListDelegate.expects(once()).method("listIterator").withNoArguments();
		assertTrue(target.iterator() instanceof AccessCheckingListIterator);
	}


	public void testListIterator() {
		mockListDelegate.expects(once()).method("listIterator").withNoArguments();
		assertTrue(target.listIterator() instanceof AccessCheckingListIterator);
	}

	public void testListIteratorIndex() {
		mockListDelegate.expects(once()).method("listIterator").with(eq(0));
		assertTrue(target.listIterator(0) instanceof AccessCheckingListIterator);
	}

	public void testSize() {
		mockListDelegate.expects(once()).method("size").withNoArguments().will(returnValue(26));
		assertEquals(26, target.size());
	}

	public void testSubList() {
		final List<Object> subList = new ArrayList<Object>(0);
		mockListDelegate.expects(once()).method("subList").with(eq(1),eq(2)).will(returnValue(subList));
		assertSame(subList, target.subList(1,2));
	}

	public void testToArray() {
		final Object[] arr = new Object[] {readWriteObj};
		mockListDelegate.expects(once()).method("toArray").withNoArguments().will(returnValue(arr));
		assertSame(arr, target.toArray());
	}

	public void testToArrayParameterized() {
		final Object[] arr = new Object[0];
		mockListDelegate.expects(once()).method("toArray").with(same(arr)).will(returnValue(arr));
		assertSame(arr, target.toArray(arr));
	}

}
