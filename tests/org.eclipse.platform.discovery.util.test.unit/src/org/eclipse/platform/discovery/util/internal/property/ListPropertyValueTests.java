/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.property;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.platform.discovery.util.internal.property.IProperty;
import org.eclipse.platform.discovery.util.internal.property.IPropertyAttributeListener;
import org.eclipse.platform.discovery.util.internal.property.ListProperty;

public class ListPropertyValueTests extends AttributeContractTests<List<Object>, List<Object>>
{

	@Override
	protected List<Object> getAttributeValue(IProperty<List<Object>> p) {
		return p.get();
	}

	@Override
	protected List<Object> getDefaultValue() {
		return new ArrayList<Object>();
	}

	@Override
	protected List<Object> newAttributeDifferentThen(List<Object> differentThen) {
		return newNonDefaultAttributeValue();
	}

	@Override
	protected List<Object> newNonDefaultAttributeValue() {
		final List<Object> newAttribute = new ArrayList<Object>();
		newAttribute.add(new Object());
		return newAttribute;
	}

	@Override
	protected IProperty<List<Object>> newTarget() {
		return new ListProperty<Object>();
	}

	@Override
	protected void registerAttributeListener(IProperty<List<Object>> target,
			IPropertyAttributeListener<List<Object>> listener, boolean current) {
		target.registerValueListener(listener, current);
		
	}

	@Override
	protected boolean removeAttributeListener(IProperty<List<Object>> target,
			IPropertyAttributeListener<List<Object>> listener) {
		return target.removeValueListener(listener);
	}

	@Override
	protected void setAttributeValue(IProperty<List<Object>> p,
			List<Object> attribute) {
		p.set(attribute);
	}
}
