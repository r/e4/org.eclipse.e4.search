/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.test.unit.internal;

import java.util.List;

import org.eclipse.platform.discovery.runtime.api.IDestinationChangeHandler;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationsProviderDescription;
import org.mockito.Mockito;

public class DestinationsProviderWithChangeHandlerTest extends DestinationsProviderExtensionParserTest {
	
	@Override
	protected void verifyContributions(List<IDestinationsProviderDescription> contributions) {
		
		super.verifyContributions(contributions);
		IDestinationsProviderDescription providerDescr = contributions.get(0);
		
		IDestinationChangeHandler handler = Mockito.mock(IDestinationChangeHandler.class);
		providerDescr.registerDestinationsChangeHandler(handler);
		
		providerDescr.createProvider();
		Mockito.verify(providerInstance, Mockito.times(1)).registerDestinationsChangeHandler(handler);
		
	}
}
