/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.test.unit.internal;

import java.util.List;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.AbstractExtensionPointParser;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.DestinationsCategoryExtensionParser;
import org.eclipse.platform.discovery.testutils.utils.registry.ExtensionRegistryBuilder;
import org.eclipse.platform.discovery.testutils.utils.testcases.AbstractExtensionPointParserTest;
import static org.junit.Assert.*;


public class DestinationsCategoryExtensionParserTest extends AbstractExtensionPointParserTest<IDestinationCategoryDescription>
{
	private static final String CATEGORY_ID = "mycategoryid";
	private static final String CATEGORY_NAME = "My Category";
	private static final Class<? extends ISearchDestination> DESTINATIONS_CLASS = MyTestDestination.class;
	private static final String DESTINATION_PROVIDER_ID = "mydestprovider";
	
	@Override
	protected AbstractExtensionPointParser<IDestinationCategoryDescription> createParser(IExtensionRegistry registry) {
		return new DestinationsCategoryExtensionParser(registry);
	}
	
	@Override
	protected void setupRegistry(ExtensionRegistryBuilder registryBuilder) {
		registryBuilder.addDestinationCategory(CATEGORY_ID, CATEGORY_NAME, DESTINATIONS_CLASS);
		registryBuilder.addDestinationsProvider(DESTINATION_PROVIDER_ID, CATEGORY_ID, null, null, null);
	}
	
	@Override
	protected void verifyContributions(List<IDestinationCategoryDescription> contributions) {
		assertEquals("One destination category expected", 1, contributions.size());
		IDestinationCategoryDescription contribution = contributions.get(0);
		
		assertEquals("Unexpected id", CATEGORY_ID, contribution.getId());
		assertEquals("Unexpected name", CATEGORY_NAME, contribution.getDisplayName());
		assertTrue("Unexpected destinations class", ISearchDestination.class.isAssignableFrom(contribution.getDestinationsClass()));
		final List<String> destProviderIds = contribution.getDestinationProviderIds();
		assertEquals("One destination provider expected", 1, destProviderIds.size());
		assertTrue("Test dest provider not returned", destProviderIds.contains(DESTINATION_PROVIDER_ID));
	}

	private class MyTestDestination implements ISearchDestination
	{
		public String getDisplayName()
		{
			return "TEST";
		}
	}

}
