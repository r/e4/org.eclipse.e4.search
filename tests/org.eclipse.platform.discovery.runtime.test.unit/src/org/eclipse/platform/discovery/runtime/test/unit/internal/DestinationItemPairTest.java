/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.test.unit.internal;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.persistence.DestinationItemPair;
import org.eclipse.platform.discovery.testutils.utils.testcases.EqualsTestCase;
import static org.mockito.Mockito.*;


public class DestinationItemPairTest extends EqualsTestCase<DestinationItemPair> 
{
	private ISearchDestination destination;
	private ISearchDestination destination2;

	private Object item;
	private Object item2;

	private DestinationItemPair pair;
	private DestinationItemPair pair2;
	
	public DestinationItemPairTest()
	{
		super("DestinationItemPairTest",DestinationItemPair.class);
	}
	
	
	@Override
	protected void setUp() throws Exception 
	{
		destination = mock(ISearchDestination.class);
		destination2 = mock(ISearchDestination.class);
		item = new Object();
		item2 = new Object();
		pair = new DestinationItemPair(destination, item);
		pair2 = new DestinationItemPair(destination2, item2);
	}
	
	public void testGetters()
	{
		assertSame("unexpected item returned", item, pair.getItem());
		assertSame("unexpected destination returned", destination, pair.getDestination());
	}
	
	public void pairEqualsWithObject(final Object obj, boolean isEqual)
	{
		assertEquals(isEqual, pair.equals(obj));
	}
	
	public void testEqualsWithObject()
	{
		pairEqualsWithObject(new Object(), false);
	}
	
	public void testEqualsWithNull()
	{
		pairEqualsWithObject(null, false);
	}
	
	public void testEqualsWithSameDestinations()
	{
		final DestinationItemPair pair1 = new DestinationItemPair(destination, new Object());
		
		pairEqualsWithObject(pair1, false);
	}
	
	public void testEqualsWithSameItems()
	{
		final ISearchDestination destination1 = mock(ISearchDestination.class);
		final DestinationItemPair pair1 = new DestinationItemPair(destination1, item);
		
		pairEqualsWithObject(pair1, false);
	}
	
	public void testEqualsSame()
	{
		final DestinationItemPair pair1 = new DestinationItemPair(destination, item);
		pairEqualsWithObject(pair1, true);
	}
	
	public void testEquals()
	{
		pairEqualsWithObject(pair, true);
	}

	public DestinationItemPairTest(Class<DestinationItemPair> testClass) {
		super(testClass);
	}

	@Override
	public DestinationItemPair newAncestorEqualInstance() {
		return new DestinationItemPair(destination, item){
			
		};
	}

	@Override
	public DestinationItemPair newEqualInstance() {
		return pair;
	}

	@Override
	public DestinationItemPair newNonEqualInstance() {
		return pair2;
	}

	@Override
	public Iterator<DestinationItemPair> newObjectIterator(int iterations) {
		final List<DestinationItemPair> list = new ArrayList<DestinationItemPair>(iterations);
		for(int i = 0; i < iterations; i++)
		{
			list.add(new DestinationItemPair(destination, item));
		}
		
		return list.iterator();
	}


	@Override
	public void modifyObjectInstance(DestinationItemPair instance) {
		//not applicable: no internal fields and class is immutable
		
	}
}
