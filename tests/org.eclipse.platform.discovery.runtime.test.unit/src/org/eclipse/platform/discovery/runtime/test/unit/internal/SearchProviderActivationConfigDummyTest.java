/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.test.unit.internal;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import junit.framework.TestCase;

import org.eclipse.platform.discovery.runtime.internal.ProviderNotFoundException;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.ISearchProviderDescription;
import org.eclipse.platform.discovery.runtime.internal.search.activation.SearchProviderActivationConfigDummy;
import org.eclipse.platform.discovery.runtime.internal.xp.ISearchProvidersExtensionParser;

import static org.mockito.Mockito.*;

public class SearchProviderActivationConfigDummyTest extends TestCase {
	
	private static final String OBJECT_TYPE_ID = "Pesho";
	
	
	public void testGetActiveSearchProviderNoCategories() throws ProviderNotFoundException {
		IObjectTypeDescription aDescription = objectTypeDescriptionForId(OBJECT_TYPE_ID);
		ISearchProviderDescription expectedProvider = providerWithNoCategories(aDescription);
		SearchProviderActivationConfigDummy dummy = new SearchProviderActivationConfigDummy(createParser(list( providerWithACategory(aDescription), expectedProvider )));
		assertEquals("Unexpected provider returned", expectedProvider, dummy.getActiveSearchProviderDescription(objectTypeDescriptionForId(OBJECT_TYPE_ID)));
	}
	
	public void testGetActiveSearchProviderNoCategoriesThrowsExpectionForProviderWithCategories() {
		IObjectTypeDescription aDescription = objectTypeDescriptionForId(OBJECT_TYPE_ID);
		SearchProviderActivationConfigDummy dummy = new SearchProviderActivationConfigDummy(createParser(list( providerWithACategory(aDescription) )));
		try{
			dummy.getActiveSearchProviderDescription(objectTypeDescriptionForId(OBJECT_TYPE_ID));
			fail("ProviderNotFound not thrown");
		}catch(ProviderNotFoundException ex) {}
	}
	
	
	private ISearchProvidersExtensionParser createParser(List<ISearchProviderDescription> providers) {
		ISearchProvidersExtensionParser parserMock = mock(ISearchProvidersExtensionParser.class);
		when(parserMock.readContributions()).thenReturn(providers);
		return parserMock;
	}
	
	private ISearchProviderDescription providerWithCateogires(IObjectTypeDescription supportedObjectType, Set<IDestinationCategoryDescription> categories) {
		ISearchProviderDescription providerMock = mock(ISearchProviderDescription.class);
		when(providerMock.getObjectType()).thenReturn(supportedObjectType);
		when(providerMock.getSupportedDestinationCategories()).thenReturn(categories);
		return providerMock;
	}
	
	private ISearchProviderDescription providerWithACategory(IObjectTypeDescription supportedObjectType) {
		return providerWithCateogires(supportedObjectType, aCategory());
	}
	
	
	private Set<IDestinationCategoryDescription> aCategory() {
		Set<IDestinationCategoryDescription> result = new HashSet<IDestinationCategoryDescription>();
		result.add(mock(IDestinationCategoryDescription.class));
		return result;
	}
	
	private ISearchProviderDescription providerWithNoCategories(IObjectTypeDescription supportedObjectType) {
		return providerWithCateogires(supportedObjectType, new HashSet<IDestinationCategoryDescription>());

	}
	
	
	private IObjectTypeDescription objectTypeDescriptionForId(String id) {
		IObjectTypeDescription mock = mock(IObjectTypeDescription.class);
		when(mock.getId()).thenReturn(id);
		return mock;
	}
	
	private List<ISearchProviderDescription> list(ISearchProviderDescription... descriptions) {
		return Arrays.asList(descriptions);
	}
	
}
