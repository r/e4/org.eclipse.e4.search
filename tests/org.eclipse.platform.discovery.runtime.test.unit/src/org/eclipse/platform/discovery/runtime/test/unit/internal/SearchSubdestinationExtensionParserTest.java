/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.test.unit.internal;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.runtime.api.IConflict;
import org.eclipse.platform.discovery.runtime.api.ISearchSubdestination;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.AbstractExtensionPointParser;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.SearchSubdestinationExtensionParser;
import org.eclipse.platform.discovery.testutils.utils.registry.ExtensionRegistryBuilder;
import org.eclipse.platform.discovery.testutils.utils.testcases.AbstractExtensionPointParserTest;
import static org.junit.Assert.*;


public class SearchSubdestinationExtensionParserTest extends AbstractExtensionPointParserTest<ISearchSubdestination>
{
	private static final String CATEGORY_ID_DEFAULT_SELECTED = "mycategory";
	private static final String DISPLAY_NAME_DEFAULT_SELECTED = "My Subdestination";
	private static final String ID_DEFAULT_SELECTED = "mysubdestination";
	private static final String OBJECT_ID_DEFAULT_SELECTED = "myobject";

	private static final String CATEGORY_ID_DEFAULT_NOT_SELECTED = "mycategory_notselected";
	private static final String DISPLAY_NAME_DEFAULT_NOT_SELECTED = "My Subdestination _notselected";
	private static final String ID_DEFAULT_NOT_SELECTED = "mysubdestination_notselected";
	private static final String OBJECT_ID_DEFAULT_NOT_SELECTED = "myobject_notselected";

	private static final String CATEGORY_ID_DEFAULT_NULL_SELECTED = "mycategory_nullselected";
	private static final String DISPLAY_NAME_DEFAULT_NULL_SELECTED = "My Subdestination _nullselected";
	private static final String ID_DEFAULT_NULL_SELECTED = "mysubdestination_nullselected";
	private static final String OBJECT_ID_DEFAULT_NULL_SELECTED = "myobject_nullselected";

	private static final Set<String> EXPECTED_CONFLICTS = new HashSet<String>();
	
	static{
		EXPECTED_CONFLICTS.add("foo");
		EXPECTED_CONFLICTS.add("bar");
		EXPECTED_CONFLICTS.add("baz");
	}
	
	@Override
	protected AbstractExtensionPointParser<ISearchSubdestination> createParser(IExtensionRegistry registry) {
		return new SearchSubdestinationExtensionParser(registry);
	}

	@Override
	protected void setupRegistry(ExtensionRegistryBuilder registryBuilder) {
		registryBuilder.addSearchSubdestination(ID_DEFAULT_SELECTED, DISPLAY_NAME_DEFAULT_SELECTED, CATEGORY_ID_DEFAULT_SELECTED, OBJECT_ID_DEFAULT_SELECTED, "true", "foo", "bar", "baz");
		registryBuilder.addSearchSubdestination(ID_DEFAULT_NOT_SELECTED, DISPLAY_NAME_DEFAULT_NOT_SELECTED, CATEGORY_ID_DEFAULT_NOT_SELECTED, OBJECT_ID_DEFAULT_NOT_SELECTED, "false", "foo", "bar", "baz");
		registryBuilder.addSearchSubdestination(ID_DEFAULT_NULL_SELECTED, DISPLAY_NAME_DEFAULT_NULL_SELECTED, CATEGORY_ID_DEFAULT_NULL_SELECTED, OBJECT_ID_DEFAULT_NULL_SELECTED, null, "foo", "bar", "baz");
	}
	

	@Override
	protected void verifyContributions(List<ISearchSubdestination> contributions) {
		
		assertEquals("Three contributions expected", 3, contributions.size());
		
		verifySearchSubdestination(contributions.get(0), ID_DEFAULT_SELECTED, DISPLAY_NAME_DEFAULT_SELECTED, CATEGORY_ID_DEFAULT_SELECTED, OBJECT_ID_DEFAULT_SELECTED, true, EXPECTED_CONFLICTS);
		verifySearchSubdestination(contributions.get(1), ID_DEFAULT_NOT_SELECTED, DISPLAY_NAME_DEFAULT_NOT_SELECTED, CATEGORY_ID_DEFAULT_NOT_SELECTED, OBJECT_ID_DEFAULT_NOT_SELECTED, false, EXPECTED_CONFLICTS);
		verifySearchSubdestination(contributions.get(2), ID_DEFAULT_NULL_SELECTED, DISPLAY_NAME_DEFAULT_NULL_SELECTED, CATEGORY_ID_DEFAULT_NULL_SELECTED, OBJECT_ID_DEFAULT_NULL_SELECTED, true, EXPECTED_CONFLICTS);

	}

	private void verifySearchSubdestination(ISearchSubdestination subdestination, String expectedId, String expectedDisplayName, String expectedCategoryId,
			String expectedObjectId, boolean isDefaultSelected, Set<String> expectedConflictIds) {
		assertEquals("wrong id", expectedId, subdestination.getId());
		assertEquals("wrong name", expectedDisplayName, subdestination.getDisplayName());
		assertEquals("wrong category", expectedCategoryId, subdestination.getDestinationCategoryId());
		assertEquals("wrong object type", expectedObjectId, subdestination.getObjectTypeId());
		assertEquals("wrong isDefaultSelected", isDefaultSelected, subdestination.isDefaultSelected());
		checkConflicts(subdestination, expectedConflictIds);

	}

	private void checkConflicts(ISearchSubdestination subdest, Set<String> expectedConflictIds) {
		Set<String> conflictingsubd = new HashSet<String>();
		for(IConflict conf:subdest.getConflictingSubd()) {
			assertTrue("Duplicate conflict ids not expected", conflictingsubd.add(conf.getconflictingSubdID()));
		}
		assertEquals("unexpected set of conflicts", expectedConflictIds, conflictingsubd);
	}


}
