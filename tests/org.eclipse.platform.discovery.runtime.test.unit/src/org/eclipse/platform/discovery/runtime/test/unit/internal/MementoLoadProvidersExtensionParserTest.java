/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.test.unit.internal;

import java.util.List;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.runtime.api.persistence.IMementoLoadProvider;
import org.eclipse.platform.discovery.runtime.internal.persistence.model.descriptions.IMementoLoadProviderDescription;
import org.eclipse.platform.discovery.runtime.internal.persistence.xp.MementoLoadProvidersExtensionParser;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.AbstractExtensionPointParser;
import org.eclipse.platform.discovery.testutils.utils.registry.ExtensionRegistryBuilder;
import org.eclipse.platform.discovery.testutils.utils.testcases.AbstractExtensionPointParserTest;
import org.mockito.Mockito;
import static org.junit.Assert.*;


public class MementoLoadProvidersExtensionParserTest extends AbstractExtensionPointParserTest<IMementoLoadProviderDescription>
{
	private IMementoLoadProvider loadProvider;
	
	@Override
	protected void setupRegistry(ExtensionRegistryBuilder registryBuilder) {
		loadProvider = Mockito.mock(IMementoLoadProvider.class);
		registryBuilder.addMementoLoadProvider(loadProvider);
	}

	@Override
	protected void verifyContributions(List<IMementoLoadProviderDescription> contributions) {
		assertEquals("one load provider expected", 1, contributions.size());
		IMementoLoadProviderDescription contribution = contributions.get(0);
		assertSame("Unexpected provider instance created", loadProvider, contribution.createInstance());
	}
	
	@Override
	protected AbstractExtensionPointParser<IMementoLoadProviderDescription> createParser(IExtensionRegistry registry) {
		return new MementoLoadProvidersExtensionParser(registry);
	}
}
