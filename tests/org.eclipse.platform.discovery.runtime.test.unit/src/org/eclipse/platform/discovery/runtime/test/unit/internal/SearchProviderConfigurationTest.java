/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.test.unit.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.platform.discovery.runtime.api.IDestinationsProvider;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.ISearchSubdestination;
import org.eclipse.platform.discovery.runtime.internal.DestinationCategoryNotFoundException;
import org.eclipse.platform.discovery.runtime.internal.ISearchProviderActivationConfig;
import org.eclipse.platform.discovery.runtime.internal.ISearchProviderConfiguration;
import org.eclipse.platform.discovery.runtime.internal.ISubdestinationsActivationConfig;
import org.eclipse.platform.discovery.runtime.internal.IXPParsersFactory;
import org.eclipse.platform.discovery.runtime.internal.ProviderNotFoundException;
import org.eclipse.platform.discovery.runtime.internal.impl.SearchProviderConfiguration;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationsProviderDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.ISearchProviderDescription;
import org.eclipse.platform.discovery.runtime.internal.xp.IDestinationCategoryExtensionParser;
import org.eclipse.platform.discovery.runtime.internal.xp.IDestinationsProviderExtensionParser;
import org.eclipse.platform.discovery.runtime.internal.xp.IObjectTypeExtensionParser;
import org.eclipse.platform.discovery.runtime.internal.xp.ISearchProvidersExtensionParser;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.testutils.utils.model.DestProviderDescriptionBuilder;
import org.eclipse.platform.discovery.testutils.utils.model.DestinationCategoryDescriptionBuilder;
import org.eclipse.platform.discovery.testutils.utils.model.DestinationsProviderBuilder;
import org.eclipse.platform.discovery.testutils.utils.model.ObjectTypeDescriptionBuilder;
import org.eclipse.platform.discovery.testutils.utils.model.SearchDestinationBuilder;
import org.eclipse.platform.discovery.testutils.utils.model.SearchProviderDescriptionBuilder;
import org.eclipse.platform.discovery.testutils.utils.reflection.ReflectionUtils;
import org.eclipse.platform.discovery.util.internal.logging.ILogger;
import org.jmock.core.Constraint;
import org.jmock.core.Invocation;
import org.jmock.core.Stub;
import org.mockito.Mockito;


/**
 * See SearchProviderConfigurationTestFixture.txt for explanations on the test fixture 
 */
public class SearchProviderConfigurationTest extends MockObjectTestCase
{
	
	private Boolean isActiveForSelection = true;
	private ISearchProviderConfiguration searchProviderConfig;
	private Mock<ISearchProvidersExtensionParser> searchProviderExtensionParser;
	private Mock<IObjectTypeExtensionParser> objectTypeExtensionParser;
	private Mock<IDestinationCategoryExtensionParser> destCategoryExtensionParser;
	private Mock<IDestinationsProviderExtensionParser> destProviderExtensionParser;
	
	private Mock<IXPParsersFactory> xpParsersFactory;

	// object types
	private IObjectTypeDescription objectType_1;
	private IObjectTypeDescription objectType_2;
	private static final String OBJECT_TYPE_ID_1 = "objecttype1";
	private static final String OBJECT_TYPE_ID_2 = "objecttype2";
	private static final String OBJECT_TYPE_NAME_1 = "Object 1";
	private static final String OBJECT_TYPE_NAME_2 = "Object 2";

	// destination categories
	private IDestinationCategoryDescription destinationCategory1;
	private IDestinationCategoryDescription destinationCategory2;
	private IDestinationCategoryDescription destinationCategory3;
	private IDestinationCategoryDescription destinationCategory4;
	private final static String DESTINATION_CATEGORY_ID_1 = "destcategory1";
	private final static String DESTINATION_CATEGORY_ID_2 = "destcategory2";
	private final static String DESTINATION_CATEGORY_ID_3 = "destcategory3";
	private final static String DESTINATION_CATEGORY_ID_4 = "destcategory4";
	private final static String DESTINATION_CATEGORY_NAME_1 = "Destination category 1";
	private final static String DESTINATION_CATEGORY_NAME_2 = "Destination category 2";
	private final static String DESTINATION_CATEGORY_NAME_3 = "Destination category 3";
	private final static String DESTINATION_CATEGORY_NAME_4 = "Destination category 4";

	// destinations
	private ISearchDestination searchDestination1;
	private ISearchDestination searchDestination2;
	private ISearchDestination searchDestination3;
	private ISearchDestination searchDestination4;
	private ISearchDestination searchDestination5;
	private ISearchDestination searchDestination6;
	private ISearchDestination searchDestination7;
	private ISearchDestination searchDestination8;
	private static final String SEARCH_DESTINATION_NAME_1 = "Search destination 1";
	private static final String SEARCH_DESTINATION_NAME_2 = "Search destination 2";
	private static final String SEARCH_DESTINATION_NAME_3 = "Search destination 3";
	private static final String SEARCH_DESTINATION_NAME_4 = "Search destination 4";
	private static final String SEARCH_DESTINATION_NAME_5 = "Search destination 5";
	private static final String SEARCH_DESTINATION_NAME_6 = "Search destination 6";
	private static final String SEARCH_DESTINATION_NAME_7 = "Search destination 7";
	private static final String SEARCH_DESTINATION_NAME_8 = "Search destination 8";
	
	// destination providers
	private IDestinationsProviderDescription destinationProviderDescr1;
	private IDestinationsProviderDescription destinationProviderDescr2;
	private IDestinationsProviderDescription destinationProviderDescr3;
	private IDestinationsProviderDescription destinationProviderDescr4;
	private IDestinationsProviderDescription destinationProviderDescr5;
	private IDestinationsProvider destinationProvider1;
	private IDestinationsProvider destinationProvider2;
	private IDestinationsProvider destinationProvider3;
	private IDestinationsProvider destinationProvider4;
	private IDestinationsProvider destinationProvider5;
	private final static String DESTINATION_PROVIDER_ID_1 = "destinationprovider1";
	private final static String DESTINATION_PROVIDER_ID_2 = "destinationprovider2";
	private final static String DESTINATION_PROVIDER_ID_3 = "destinationprovider3";
	private final static String DESTINATION_PROVIDER_ID_4 = "destinationprovider4";
	private final static String DESTINATION_PROVIDER_ID_5 = "destinationprovider5";

	// search providers
	private ISearchProviderDescription provider1;
	private ISearchProviderDescription provider2;
	private ISearchProviderDescription provider3;
	private ISearchProviderDescription provider4;
	private static final String PROVIDER_ID_1 = "provider1";
	private static final String PROVIDER_ID_2 = "provider2";
	private static final String PROVIDER_ID_3 = "provider3";
	private static final String PROVIDER_ID_4 = "provider4";
	private static final String PROVIDER_NAME_1 = "Provider 1";
	private static final String PROVIDER_NAME_2 = "Provider 2";
	private static final String PROVIDER_NAME_3 = "Provider 3";
	private static final String PROVIDER_NAME_4 = "Provider 4";
	
	// subdestinations
	private Mock<ISearchSubdestination> subdestination1;
	private Mock<ISearchSubdestination> subdestination2;
	private Mock<ISearchSubdestination> subdestination3;
	private final static String SUBDESTINATION_ID_1 = "subdestination1";
	private final static String SUBDESTINATION_ID_2 = "subdestination2";
	private final static String SUBDESTINATION_ID_3 = "subdestination3";
	private final static String SUBDESTINATION_NAME_1 = "Subdestination 1";
	private final static String SUBDESTINATION_NAME_2 = "Subdestination 2";
	private final static String SUBDESTINATION_NAME_3 = "Subdestination 3";
	
	@Override
	protected void setUp() throws Exception
	{
		setupObjectTypes();
		setupSearchDestinations();
		setupDestinationProviders();
		setupDestinationCategories();
		setupSearchProviders();
		setupSubdestinations();
		
		setupObjectTypesExtensionParser();
		setupDestCategoryExtensionParser();
		setupDestProvidersExtParser();
		setupSearchProvidersExtParser();
		setupParsersFactory();
		

		searchProviderConfig = new SearchProviderConfiguration(xpParsersFactory.proxy(), createActivationConfig(), createSubdestinationsActivationConfig());
	}

	private void setupParsersFactory() {
		xpParsersFactory = mock(IXPParsersFactory.class);
		xpParsersFactory.stubs().method("createDestinationsProviderParser").will(returnValue(destProviderExtensionParser.proxy()));
		xpParsersFactory.stubs().method("createDestinationsCategoryParser").will(returnValue(destCategoryExtensionParser.proxy()));
		xpParsersFactory.stubs().method("createObjectTypeParser").will(returnValue(objectTypeExtensionParser.proxy()));
		xpParsersFactory.stubs().method("createSearchProviderParser").will(returnValue(searchProviderExtensionParser.proxy()));


	}

	private void setupObjectTypes()
	{
		objectType_1 = new ObjectTypeDescriptionBuilder().withId(OBJECT_TYPE_ID_1).withName(OBJECT_TYPE_NAME_1).object();
		objectType_2 = new ObjectTypeDescriptionBuilder().withId(OBJECT_TYPE_ID_2).withName(OBJECT_TYPE_NAME_2).object();
	}
	
	private void setupSearchDestinations()
	{
		searchDestination1 = stubSearchDestination(SEARCH_DESTINATION_NAME_1);
		searchDestination2 = stubSearchDestination(SEARCH_DESTINATION_NAME_2);
		searchDestination3 = stubSearchDestination(SEARCH_DESTINATION_NAME_3);
		searchDestination4 = stubSearchDestination(SEARCH_DESTINATION_NAME_4);
		searchDestination5 = stubSearchDestination(SEARCH_DESTINATION_NAME_5);
		searchDestination6 = stubSearchDestination(SEARCH_DESTINATION_NAME_6);
		searchDestination7 = stubSearchDestination(SEARCH_DESTINATION_NAME_7);
		searchDestination8 = stubSearchDestination(SEARCH_DESTINATION_NAME_8);
	}
	
	private ISearchDestination stubSearchDestination(final String displayName)
	{
		return new SearchDestinationBuilder().withDisplayName(displayName).object();
	}
	
	private void setupDestinationProviders()
	{
		destinationProvider1 = stubDestinationsProvider(searchDestination1, searchDestination2);
		destinationProviderDescr1 = stubDestinationsProviderDescription(DESTINATION_PROVIDER_ID_1, destinationProvider1);

		destinationProvider2 = stubDestinationsProvider(searchDestination3);
		destinationProviderDescr2 = stubDestinationsProviderDescription(DESTINATION_PROVIDER_ID_2, destinationProvider2);

		destinationProvider3 = stubDestinationsProvider(searchDestination4, searchDestination5);
		destinationProviderDescr3 = stubDestinationsProviderDescription(DESTINATION_PROVIDER_ID_3, destinationProvider3);

		destinationProvider4 = stubDestinationsProvider(searchDestination6, searchDestination7);
		destinationProviderDescr4 = stubDestinationsProviderDescription(DESTINATION_PROVIDER_ID_4, destinationProvider4);

		destinationProvider5 = stubDestinationsProvider(searchDestination8);
		destinationProviderDescr5 = stubDestinationsProviderDescription(DESTINATION_PROVIDER_ID_5, destinationProvider5);
	}
	
	private IDestinationsProvider stubDestinationsProvider(final ISearchDestination... destinations)
	{
		return new DestinationsProviderBuilder().withDestinations(destinations).object();
	}
	
	private IDestinationsProviderDescription stubDestinationsProviderDescription(final String id, final IDestinationsProvider destProviderInstance)
	{
		return new DestProviderDescriptionBuilder().withProvider(destProviderInstance).withId(id).object();
	}
	
	private void setupDestinationCategories()
	{
		destinationCategory1 = stubDestCategoryDescription(DESTINATION_CATEGORY_ID_1, DESTINATION_CATEGORY_NAME_1, DESTINATION_PROVIDER_ID_1);
		destinationCategory2 = stubDestCategoryDescription(DESTINATION_CATEGORY_ID_2, DESTINATION_CATEGORY_NAME_2, DESTINATION_PROVIDER_ID_2, DESTINATION_PROVIDER_ID_3);
		destinationCategory3 = stubDestCategoryDescription(DESTINATION_CATEGORY_ID_3, DESTINATION_CATEGORY_NAME_3, DESTINATION_PROVIDER_ID_4);
		destinationCategory4 = stubDestCategoryDescription(DESTINATION_CATEGORY_ID_4, DESTINATION_CATEGORY_NAME_4, DESTINATION_PROVIDER_ID_5);
	}
	
	private IDestinationCategoryDescription stubDestCategoryDescription(final String id, final String displayName, final String... destProviderIds)
	{
		return new DestinationCategoryDescriptionBuilder().withDestinationClass(ISearchDestination.class).withDestinationProviders(destProviderIds).withId(id).withName(displayName).object();
	}
	
	private void setupSearchProviders()
	{
		provider1 = stubSearchProviderDescription(PROVIDER_ID_1, PROVIDER_NAME_1, true, objectType_1, destinationCategory1, destinationCategory2);
		provider2 = stubSearchProviderDescription(PROVIDER_ID_2, PROVIDER_NAME_2, false, objectType_1, destinationCategory3, destinationCategory4);
		provider3 = stubSearchProviderDescription(PROVIDER_ID_3, PROVIDER_NAME_3, true, objectType_1, destinationCategory2, destinationCategory3);
		provider4 = stubSearchProviderDescription(PROVIDER_ID_4, PROVIDER_NAME_4, false, objectType_2, destinationCategory1, destinationCategory4);
	}
	
	private ISearchProviderDescription stubSearchProviderDescription(String id, String name, boolean supportsTextSearch, IObjectTypeDescription objectType, IDestinationCategoryDescription... categories)
	{
		return new SearchProviderDescriptionBuilder().supportsTextSearch(supportsTextSearch).withDestinationCategories(categories).supportsObjectType(objectType).withId(id).withName(name).object();
	}
	
	private void setupSubdestinations()
	{
		subdestination1 = mock(ISearchSubdestination.class, SUBDESTINATION_NAME_1);
		subdestination1.stubs().method("getId").will(returnValue(SUBDESTINATION_ID_1));
		subdestination1.stubs().method("getDisplayName").will(returnValue(SUBDESTINATION_NAME_1));
		subdestination1.stubs().method("getDestinationCategoryId").will(returnValue(DESTINATION_CATEGORY_ID_1));
		subdestination1.stubs().method("getObjectTypeId").will(returnValue(OBJECT_TYPE_ID_1));
		
		subdestination2 = mock(ISearchSubdestination.class, SUBDESTINATION_NAME_2);
		subdestination2.stubs().method("getId").will(returnValue(SUBDESTINATION_ID_2));
		subdestination2.stubs().method("getDisplayName").will(returnValue(SUBDESTINATION_NAME_2));
		subdestination2.stubs().method("getDestinationCategoryId").will(returnValue(DESTINATION_CATEGORY_ID_2));
		subdestination2.stubs().method("getObjectTypeId").will(returnValue(OBJECT_TYPE_ID_1));
		
		subdestination3 = mock(ISearchSubdestination.class, SUBDESTINATION_NAME_3);
		subdestination3.stubs().method("getId").will(returnValue(SUBDESTINATION_ID_3));
		subdestination3.stubs().method("getDisplayName").will(returnValue(SUBDESTINATION_NAME_3));
		subdestination3.stubs().method("getDestinationCategoryId").will(returnValue(DESTINATION_CATEGORY_ID_2));
		subdestination3.stubs().method("getObjectTypeId").will(returnValue(OBJECT_TYPE_ID_1));
	}
	
	private void setupObjectTypesExtensionParser()
	{
		objectTypeExtensionParser = mock(IObjectTypeExtensionParser.class);
		final List<IObjectTypeDescription> objects = Arrays.asList(new IObjectTypeDescription[]{objectType_1, objectType_2});
		objectTypeExtensionParser.stubs().method("readContributions").will(returnValue(objects));
	}
	
	private void setupDestCategoryExtensionParser()
	{
		destCategoryExtensionParser = mock(IDestinationCategoryExtensionParser.class);
		final List<IDestinationCategoryDescription> categories = new ArrayList<IDestinationCategoryDescription>();
		categories.addAll(Arrays.asList(new IDestinationCategoryDescription[]{destinationCategory1, destinationCategory2, destinationCategory3, destinationCategory4}));
		destCategoryExtensionParser.stubs().method("readContributions").will(returnValue(categories));
	}
	
	private void setupDestProvidersExtParser()
	{
		destProviderExtensionParser = mock(IDestinationsProviderExtensionParser.class);
		final List<IDestinationsProviderDescription> providers = new ArrayList<IDestinationsProviderDescription>();
		providers.addAll(Arrays.asList(new IDestinationsProviderDescription[]{destinationProviderDescr1, destinationProviderDescr2, destinationProviderDescr3, destinationProviderDescr4, destinationProviderDescr5}));
		destProviderExtensionParser.stubs().method("readContributions").will(returnValue(providers));
	}
	
	private void setupSearchProvidersExtParser()
	{
		searchProviderExtensionParser = mock(ISearchProvidersExtensionParser.class);
		final List<ISearchProviderDescription> providers = new ArrayList<ISearchProviderDescription>();
		providers.addAll(Arrays.asList(new ISearchProviderDescription[]{provider1, provider2, provider3, provider4}));
		searchProviderExtensionParser.stubs().method("readContributions").will(returnValue(providers));
	}
	
	private ISearchProviderActivationConfig createActivationConfig()
	{
		final Mock<ISearchProviderActivationConfig> activationConfig = mock(ISearchProviderActivationConfig.class);
		
		activationConfig.stubs().method("getActiveSearchProviderDescription").with(eq(objectType_2), or(eq(destinationCategory1), eq(destinationCategory4))).will(returnValue(provider4));
		activationConfig.stubs().method("getActiveSearchProviderDescription").with(eq(objectType_2), not(or(eq(destinationCategory1), eq(destinationCategory4)))).will(throwException(new ProviderNotFoundException("Provider not found")));
		activationConfig.stubs().method("getActiveSearchProviderDescription").with(eq(objectType_1), eq(destinationCategory1)).will(returnValue(provider1));
		activationConfig.stubs().method("getActiveSearchProviderDescription").with(eq(objectType_1), eq(destinationCategory2)).will(returnValue(provider1));
		activationConfig.stubs().method("getActiveSearchProviderDescription").with(eq(objectType_1), eq(destinationCategory3)).will(returnValue(provider3));
		activationConfig.stubs().method("getActiveSearchProviderDescription").with(eq(objectType_1), eq(destinationCategory4)).will(returnValue(provider2));
		
		return activationConfig.proxy();
	}
	
	private ISubdestinationsActivationConfig createSubdestinationsActivationConfig()
	{
		final Mock<ISubdestinationsActivationConfig> activationConfig = mock(ISubdestinationsActivationConfig.class);
		
		activationConfig.stubs().method("getAvailableSearchSubdestinations").with(eq(objectType_2), ANYTHING, ANYTHING).will(returnValue(new ArrayList<ISearchSubdestination>()));
		activationConfig.stubs().method("getAvailableSearchSubdestinations").with(eq(objectType_1), eq(destinationCategory1), ANYTHING).will(returnValue(Arrays.asList(new ISearchSubdestination[]{subdestination1.proxy()})));
		activationConfig.stubs().method("getAvailableSearchSubdestinations").with(eq(objectType_1), eq(destinationCategory2), ANYTHING).will(returnValue(Arrays.asList(new ISearchSubdestination[]{subdestination2.proxy(), subdestination3.proxy()})));
		activationConfig.stubs().method("getAvailableSearchSubdestinations").with(eq(objectType_1), eq(destinationCategory3), ANYTHING).will(returnValue(new ArrayList<ISearchSubdestination>()));
		activationConfig.stubs().method("getAvailableSearchSubdestinations").with(eq(objectType_1), eq(destinationCategory4), ANYTHING).will(returnValue(new ArrayList<ISearchSubdestination>()));
		
		activationConfig.stubs().method("isSubdestinationActive").with(eq(subdestination2.proxy()), eq(objectType_1), eq(destinationCategory2), eq(provider1)).will(checkValueForSelection());
		Constraint [] constraints = {eq(objectType_1), eq(destinationCategory2), eq(provider1),eq(subdestination2.proxy()), eq(new Boolean(false))};
		activationConfig.stubs().method("activateSubdestination").with(constraints).will(new Stub(){

			public Object invoke(Invocation arg0) throws Throwable {
				isActiveForSelection = new Boolean(false);
				return null;
			}

			public StringBuffer describeTo(StringBuffer arg0) {
				return null;
			}});
		
		return activationConfig.proxy();
	}

	private Stub checkValueForSelection() {
		return new Stub()
		{
			@Override
			public StringBuffer describeTo(StringBuffer buffer)
			{
				return buffer;
			}
			
			@Override
			public Object invoke(Invocation invocation) throws Throwable
			{
				return isActiveForSelection;
			}
		};
	}

	public void testGetAvailableSearchProvidersDescriptionsForObjectType()
	{

		final List<ISearchProviderDescription> descriptions = searchProviderConfig.getAvailableSearchProviderDescriptions(objectType_1);
		assertEquals("Three providers expected", 3, descriptions.size());
		final List<String> ids = new ArrayList<String>();
		for (ISearchProviderDescription d : descriptions)
		{
			assertEquals("Unexpected object type", OBJECT_TYPE_ID_1, d.getObjectType().getId());
			ids.add(d.getId());
		}

		assertTrue("Provider 1 not returned", ids.contains(PROVIDER_ID_1));
		assertTrue("Provider 2 not returned", ids.contains(PROVIDER_ID_2));
		assertTrue("Provider 3 not returned", ids.contains(PROVIDER_ID_3));
		
		final List<String> ids2 = new ArrayList<String>();
		for(ISearchProviderDescription p : searchProviderConfig.getAvailableSearchProviderDescriptions(objectType_2))
		{
			assertEquals("Unexpected object type", OBJECT_TYPE_ID_2, p.getObjectType().getId());
			ids2.add(p.getId());
		}
		assertEquals("One provider expected", 1, ids2.size());
		assertTrue("Provider 4 not returned", ids2.contains(PROVIDER_ID_4));
	}
	
	public void testGetAvailableDestinationCategoriesForObjectType()
	{
		final List<String> cat1Ids = new ArrayList<String>();
		for(IDestinationCategoryDescription cat : searchProviderConfig.getAvailableDestinationCategoriesForObjectType(objectType_1))
		{
			cat1Ids.add(cat.getId());
		}
		assertEquals("4 categories expected", 4, cat1Ids.size());
		assertTrue("Destinationa category 1 not returned", cat1Ids.contains(DESTINATION_CATEGORY_ID_1));
		assertTrue("Destinationa category 1 not returned", cat1Ids.contains(DESTINATION_CATEGORY_ID_2));
		assertTrue("Destinationa category 1 not returned", cat1Ids.contains(DESTINATION_CATEGORY_ID_3));
		assertTrue("Destinationa category 1 not returned", cat1Ids.contains(DESTINATION_CATEGORY_ID_4));

		final List<String> cat2Ids = new ArrayList<String>();
		for(IDestinationCategoryDescription cat : searchProviderConfig.getAvailableDestinationCategoriesForObjectType(objectType_2))
		{
			cat2Ids.add(cat.getId());
		}
		assertEquals("2 categories expected", 2, cat2Ids.size());
		assertTrue("Destinationa category 1 not returned", cat2Ids.contains(DESTINATION_CATEGORY_ID_1));
		assertTrue("Destinationa category 1 not returned", cat2Ids.contains(DESTINATION_CATEGORY_ID_4));
	}
	
	public void testGetActiveSearchProvider() throws ProviderNotFoundException
	{
		assertEquals(searchProviderConfig.getActiveSearchProvider(objectType_1, destinationCategory1).getId(), PROVIDER_ID_1);
		assertEquals(searchProviderConfig.getActiveSearchProvider(objectType_1, destinationCategory2).getId(), PROVIDER_ID_1);
		assertEquals(searchProviderConfig.getActiveSearchProvider(objectType_1, destinationCategory3).getId(), PROVIDER_ID_3);
		assertEquals(searchProviderConfig.getActiveSearchProvider(objectType_1, destinationCategory4).getId(), PROVIDER_ID_2);
		
		assertEquals(searchProviderConfig.getActiveSearchProvider(objectType_2, destinationCategory1).getId(), PROVIDER_ID_4);
		assertEquals(searchProviderConfig.getActiveSearchProvider(objectType_2, destinationCategory4).getId(), PROVIDER_ID_4);
		
		try
		{
			searchProviderConfig.getActiveSearchProvider(objectType_2, destinationCategory3);
			fail("ProviderNotFoundException not thrown");
		}
		catch(ProviderNotFoundException e)
		{
			//expected
		}
		
		try
		{
			searchProviderConfig.getActiveSearchProvider(objectType_2, destinationCategory2);
			fail("ProviderNotFoundException not thrown");
		}
		catch(ProviderNotFoundException e)
		{
			//expected
		}
		
	}
	
	public void testGetObjectTypes()
	{
		final List<String> objectIds = new ArrayList<String>();
		for(IObjectTypeDescription objectType : searchProviderConfig.getObjectTypes())
		{
			objectIds.add(objectType.getId());
		}
		
		assertEquals("2 objects expected", 2, objectIds.size());
		assertTrue("Object 1 not returned", objectIds.contains(OBJECT_TYPE_ID_1));
		assertTrue("Object 2 not returned", objectIds.contains(OBJECT_TYPE_ID_2));
	}
	
	public void testGetDestinationCategories()
	{
		final List<String> catIds = new ArrayList<String>();
		for(IDestinationCategoryDescription cat : searchProviderConfig.getDestinationCategories())
		{
			catIds.add(cat.getId());
		}
		
		assertEquals("4 categories expected", 4, catIds.size());
		assertTrue("Category 1 not returned", catIds.contains(DESTINATION_CATEGORY_ID_1));
		assertTrue("Category 2 not returned", catIds.contains(DESTINATION_CATEGORY_ID_2));
		assertTrue("Category 3 not returned", catIds.contains(DESTINATION_CATEGORY_ID_3));
		assertTrue("Category 4 not returned", catIds.contains(DESTINATION_CATEGORY_ID_4));
	}
	
	public void testGetDestinationProvidersForCategory()
	{
		final List<IDestinationsProviderDescription> providers1 = searchProviderConfig.getDestinationProvidersForCategory(destinationCategory1);
		assertEquals("One destination provider expected", 1, providers1.size());
		assertTrue("Destination provider 1 not returned", providers1.contains(destinationProviderDescr1));
		
		final List<IDestinationsProviderDescription> providers2 = searchProviderConfig.getDestinationProvidersForCategory(destinationCategory2);
		assertEquals("Two destination providers expected", 2, providers2.size());
		assertTrue("Destination provider 2 not returned", providers2.contains(destinationProviderDescr2));
		assertTrue("Destination provider 3 not returned", providers2.contains(destinationProviderDescr3));

		final List<IDestinationsProviderDescription> providers3 = searchProviderConfig.getDestinationProvidersForCategory(destinationCategory3);
		assertEquals("One destination provider expected", 1, providers3.size());
		assertTrue("Destination provider 4 not returned", providers3.contains(destinationProviderDescr4));

		final List<IDestinationsProviderDescription> providers4 = searchProviderConfig.getDestinationProvidersForCategory(destinationCategory4);
		assertEquals("One destination provider expected", 1, providers4.size());
		assertTrue("Destination provider 5 not returned", providers4.contains(destinationProviderDescr5));
	}	
	
	public void testGetSearchDestinations()
	{
		List<ISearchDestination> destinations = searchProviderConfig.getSearchDestinations(destinationCategory1, destinationProvider1);
		assertEquals("Two destinations expected", 2, destinations.size());
		assertTrue("Destination 1 not returned", destinations.contains(searchDestination1));
		assertTrue("Destination 2 not returned", destinations.contains(searchDestination2));
		
		destinations = searchProviderConfig.getSearchDestinations(destinationCategory2, destinationProvider2);
		assertEquals("One destination expected", 1, destinations.size());
		assertTrue("Destination 3 not returned", destinations.contains(searchDestination3));
		
		destinations = searchProviderConfig.getSearchDestinations(destinationCategory2, destinationProvider3);
		assertEquals("Two destinations expected", 2, destinations.size());
		assertTrue("Destination 4 not returned", destinations.contains(searchDestination4));
		assertTrue("Destination 5 not returned", destinations.contains(searchDestination5));
		
		destinations = searchProviderConfig.getSearchDestinations(destinationCategory3, destinationProvider4);
		assertEquals("Two destinations expected", 2, destinations.size());
		assertTrue("Destination 6 not returned", destinations.contains(searchDestination6));
		assertTrue("Destination 7 not returned", destinations.contains(searchDestination7));
		
		destinations = searchProviderConfig.getSearchDestinations(destinationCategory4, destinationProvider5);
		assertEquals("One destination expected", 1, destinations.size());
		assertTrue("Destination 8 not returned", destinations.contains(searchDestination8));
	}
	
	@SuppressWarnings("unchecked")
	public void testGetSearchDestinationsIgnoredWhenDestinationsClassNoLegal() throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException
	{
		final Mock<ILogger> logger = mock(ILogger.class);
		logger.expects(atLeastOnce()).method("logWarn");
		ReflectionUtils.setFieldValue(searchProviderConfig, "logger", logger.proxy());
		final Class<?> illegalClass = String.class;
		Mockito.stub(destinationCategory1.getDestinationsClass()).toReturn((Class<ISearchDestination>) illegalClass);
		assertEquals("No search destinations expected", 0, searchProviderConfig.getSearchDestinations(destinationCategory1, destinationProvider1).size());
	}
	
	public void testGetAvailableSearchSubdestinations()
	{
		final List<ISearchSubdestination> subdest1 = searchProviderConfig.getAvailableSearchSubdestinations(objectType_1, destinationCategory1, provider1);
		assertEquals("One subdestination expected", 1, subdest1.size());
		assertTrue("Subdestination 1 not returned", subdest1.contains(subdestination1.proxy()));
		
		final List<ISearchSubdestination> subdest2 = searchProviderConfig.getAvailableSearchSubdestinations(objectType_1, destinationCategory2, provider1);
		assertEquals("Two subdestination expected", 2, subdest2.size());
		assertTrue("Subdestination 2 not returned", subdest2.contains(subdestination2.proxy()));
		assertTrue("Subdestination 3 not returned", subdest2.contains(subdestination3.proxy()));

		final List<ISearchSubdestination> subdest3 = searchProviderConfig.getAvailableSearchSubdestinations(objectType_1, destinationCategory3, provider2);
		assertEquals("No subdestination expected", 0, subdest3.size());
	}
	
	
	public void testGetActiveSearchSubdestinations()
	{
		
		final List<ISearchSubdestination> subdest2 = searchProviderConfig.getAvailableSearchSubdestinations(objectType_1, destinationCategory2, provider1);
		assertEquals("Two subdestination expected", 2, subdest2.size());
		assertTrue("Subdestination 2 not returned", subdest2.contains(subdestination2.proxy()));
		assertTrue("Subdestination 3 not returned", subdest2.contains(subdestination3.proxy()));

		assertTrue("Subdestination 2 not checked", searchProviderConfig.isSubdestinationActive(subdestination2.proxy(), objectType_1, destinationCategory2, provider1));
		searchProviderConfig.activateSubdestination( objectType_1,  destinationCategory2, provider1, subdestination2.proxy(), false);

		assertFalse("Subdestination 2 is checked", searchProviderConfig.isSubdestinationActive(subdestination2.proxy(), objectType_1, destinationCategory2, provider1));
		
		
		
	}
	public void testGetDestinationCategoriesForDestination() throws DestinationCategoryNotFoundException
	{
		assertEquals("Destination category 1 expected for destination1", DESTINATION_CATEGORY_ID_1, searchProviderConfig.getDestinationCategoriesForDestination(searchDestination1).iterator().next().getId());
		assertEquals("Destination category 1 expected for destination2", DESTINATION_CATEGORY_ID_1, searchProviderConfig.getDestinationCategoriesForDestination(searchDestination2).iterator().next().getId());
		assertEquals("Destination category 2 expected for destination3", DESTINATION_CATEGORY_ID_2, searchProviderConfig.getDestinationCategoriesForDestination(searchDestination3).iterator().next().getId());
		assertEquals("Destination category 2 expected for destination4", DESTINATION_CATEGORY_ID_2, searchProviderConfig.getDestinationCategoriesForDestination(searchDestination4).iterator().next().getId());
		assertEquals("Destination category 2 expected for destination5", DESTINATION_CATEGORY_ID_2, searchProviderConfig.getDestinationCategoriesForDestination(searchDestination5).iterator().next().getId());
		assertEquals("Destination category 3 expected for destination6", DESTINATION_CATEGORY_ID_3, searchProviderConfig.getDestinationCategoriesForDestination(searchDestination6).iterator().next().getId());
		assertEquals("Destination category 3 expected for destination7", DESTINATION_CATEGORY_ID_3, searchProviderConfig.getDestinationCategoriesForDestination(searchDestination7).iterator().next().getId());
		assertEquals("Destination category 4 expected for destination8", DESTINATION_CATEGORY_ID_4, searchProviderConfig.getDestinationCategoriesForDestination(searchDestination8).iterator().next().getId());
	}
	
	public void testGetAvailableDestinationProviders()
	{
		final List<IDestinationsProviderDescription> providers = searchProviderConfig.getAvailableDestinationProviders();
		assertEquals("5 providers expected", 5, providers.size());
		assertTrue("destinationProviderDescr1 not found", providers.contains(destinationProviderDescr1));
		assertTrue("destinationProviderDescr2 not found", providers.contains(destinationProviderDescr2));
		assertTrue("destinationProviderDescr3 not found", providers.contains(destinationProviderDescr3));
		assertTrue("destinationProviderDescr4 not found", providers.contains(destinationProviderDescr4));
		assertTrue("destinationProviderDescr5 not found", providers.contains(destinationProviderDescr5));
	}
}
