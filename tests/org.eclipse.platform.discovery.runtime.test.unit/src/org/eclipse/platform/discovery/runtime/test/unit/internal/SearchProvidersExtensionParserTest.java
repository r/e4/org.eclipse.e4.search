/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.test.unit.internal;

import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.runtime.api.ISearchProvider;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.ISearchProviderDescription;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.AbstractExtensionPointParser;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.SearchProvidersExtensionParser;
import org.eclipse.platform.discovery.testutils.utils.registry.ExtensionRegistryBuilder;
import org.eclipse.platform.discovery.testutils.utils.testcases.AbstractExtensionPointParserTest;
import org.mockito.Mockito;
import static org.junit.Assert.*;


public class SearchProvidersExtensionParserTest extends AbstractExtensionPointParserTest<ISearchProviderDescription>
{
	private static final String CATEGORY_ID_1 = "category1";
	private static final String CATEGORY_ID_2 = "category2";
	
	private static final String OBJECT_ID_1 = "object1";
	private static final String OBJECT_ID_2 = "object2";
	private static final String OBJECT_NAME_1 = "object1";
	private static final String OBJECT_NAME_2 = "object2";
	
	private final static String PROVIDER_ID = "testprovider";
	private final static String PROVIDER_INSTANCE = "org.eclipse.test.MyProvider";
	private final static String PROVIDER_SUPPORTS_TEXT_SEARCH = "true";
	
	private ISearchProvider searchProviderMock;

	@Override
	protected AbstractExtensionPointParser<ISearchProviderDescription> createParser(IExtensionRegistry registry) {
		return new SearchProvidersExtensionParser(registry);
	}

	@Override
	protected void setupRegistry(ExtensionRegistryBuilder registryBuilder) {
		searchProviderMock = Mockito.mock(ISearchProvider.class);
		
		registryBuilder.addObjectType(OBJECT_ID_1, OBJECT_NAME_1);
		registryBuilder.addObjectType(OBJECT_ID_2, OBJECT_NAME_2);
		
		registryBuilder.addDestinationCategory(CATEGORY_ID_1, null);
		registryBuilder.addDestinationCategory(CATEGORY_ID_2, null);
		
		registryBuilder.addSearchProvider(PROVIDER_ID, PROVIDER_INSTANCE, searchProviderMock, OBJECT_ID_1, PROVIDER_SUPPORTS_TEXT_SEARCH, CATEGORY_ID_1);
	}

	@Override
	protected void verifyContributions(List<ISearchProviderDescription> contributions) {
		assertEquals("One provider expeceted", 1, contributions.size());
		final ISearchProviderDescription provider = contributions.get(0);
		
		assertEquals("Unexpected id", PROVIDER_ID, provider.getId());
		assertEquals("Unexpected object type", OBJECT_ID_1, provider.getObjectType().getId());
		assertTrue("Provider expected to support text search", provider.supportsTextSearch());
		assertSame("Unexpected provider created", provider.createInstance(), searchProviderMock);
		assertEquals("Unexpected display name", PROVIDER_INSTANCE, provider.getDisplayName());
		
		final Set<IDestinationCategoryDescription> categories = provider.getSupportedDestinationCategories();
		assertEquals("One category expected", 1, categories.size());
		final IDestinationCategoryDescription cat = categories.iterator().next();
		assertEquals("Unexpected category", CATEGORY_ID_1, cat.getId());
	}
}
