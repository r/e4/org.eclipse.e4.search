/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.test.unit.internal;

import java.util.List;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.runtime.api.persistence.IMementoStoreProvider;
import org.eclipse.platform.discovery.runtime.internal.persistence.model.descriptions.IMementoStoreProviderDescription;
import org.eclipse.platform.discovery.runtime.internal.persistence.xp.MementoStoreProvidersExtensionParser;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.AbstractExtensionPointParser;
import org.eclipse.platform.discovery.testutils.utils.registry.ExtensionRegistryBuilder;
import org.eclipse.platform.discovery.testutils.utils.testcases.AbstractExtensionPointParserTest;
import org.mockito.Mockito;
import static org.junit.Assert.*;


public class MementoStoreProvidersExtensionParserTest extends AbstractExtensionPointParserTest<IMementoStoreProviderDescription>
{		
	private IMementoStoreProvider storeProvider;
	
	@Override
	protected AbstractExtensionPointParser<IMementoStoreProviderDescription> createParser(IExtensionRegistry registry) {
		return new MementoStoreProvidersExtensionParser(registry);
	}

	@Override
	protected void setupRegistry(ExtensionRegistryBuilder registryBuilder) {
		storeProvider = Mockito.mock(IMementoStoreProvider.class);
		registryBuilder.addMementoStoreProvider(storeProvider);
	}

	@Override
	protected void verifyContributions(List<IMementoStoreProviderDescription> contributions) {
		assertEquals("One store provider expeted", 1, contributions.size());
		IMementoStoreProviderDescription storeProviderDescription = contributions.get(0);
		assertEquals("uenxpected store provider returned", storeProvider, storeProviderDescription.createInstance());
	}
}
