/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.test.unit.internal;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.platform.discovery.runtime.api.GroupingHierarchy;
import org.eclipse.platform.discovery.testutils.utils.testcases.EqualsTestCase;


public class GroupingHierarchyTest extends EqualsTestCase<GroupingHierarchy>
{
	public GroupingHierarchyTest()
	{
		super("GroupingHierarchyTest", GroupingHierarchy.class);
	}

	@Override
	public GroupingHierarchy newAncestorEqualInstance()
	{
		// final class
		return null;
	}

	@Override
	public GroupingHierarchy newEqualInstance()
	{
		return new GroupingHierarchy("test", "test");
	}

	@Override
	public GroupingHierarchy newNonEqualInstance()
	{
		// same display name, different id
		return new GroupingHierarchy("test", "newtest");
	}

	@Override
	public Iterator<GroupingHierarchy> newObjectIterator(int iterations)
	{
		final List<GroupingHierarchy> result = new ArrayList<GroupingHierarchy>();
		for(int i = 0; i < iterations; i++)
		{
			result.add(new GroupingHierarchy(Integer.toString(i), new Integer(i)));
		}
		return result.iterator();
	}
	
	public void testGettingFields()
	{
		final GroupingHierarchy h = new GroupingHierarchy("A", "a");
		assertEquals("Unexpected display name", "A", h.getDisplayName());
		assertEquals("Unexpected id", "a", h.getId());
		assertEquals("Unexpected toString", "A", h.toString());
	}

	@Override
	public void modifyObjectInstance(GroupingHierarchy instance) {
		//not applicable: no internal fields and class is immutable
		
	}

}
