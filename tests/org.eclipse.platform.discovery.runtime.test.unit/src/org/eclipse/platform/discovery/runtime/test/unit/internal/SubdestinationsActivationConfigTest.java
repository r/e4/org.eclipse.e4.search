/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.test.unit.internal;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import junit.framework.TestCase;

import org.eclipse.platform.discovery.runtime.api.ISearchSubdestination;
import org.eclipse.platform.discovery.runtime.internal.SubdestinationsActivationConfig;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.ISearchProviderDescription;
import org.eclipse.platform.discovery.runtime.internal.xp.ISearchSubdestinationExtensionParser;
import org.eclipse.platform.discovery.testutils.utils.model.ConflictBuilder;
import org.eclipse.platform.discovery.testutils.utils.model.DestinationCategoryDescriptionBuilder;
import org.eclipse.platform.discovery.testutils.utils.model.ObjectTypeDescriptionBuilder;
import org.eclipse.platform.discovery.testutils.utils.model.SubdestinationBuilder;

public class SubdestinationsActivationConfigTest extends TestCase
{
	
	private final String OBJECT_TYPE_ID = "myobject";
	private final String DEST_CATEGORY_ID = "mydestination";
	private final String SUBDEST_ID = "mysubdestination";

	private final String OTHER_OBJECT_TYPE_ID = "other_myobject";
	private final String OTHER_DEST_CATEGORY_ID = "other_mydestination";
	private final String OTHER_SUBDEST_ID = "other_mysubdestination";
	
	private final String WITH_CONF_SUBDEST_ID = "withconflicts_mysubdestination";
	
	private ISearchSubdestinationExtensionParser parser;
	private IObjectTypeDescription objectType;
	private IDestinationCategoryDescription destinationCategory;
	private ISearchProviderDescription searchProvider;
	private ISearchSubdestination subdestination;
	private ISearchSubdestination otherSubdestination;
	private ISearchSubdestination subdestinationWithConflicts;
	private SubdestinationsActivationConfig config;
	
	@Override
	protected void setUp() throws Exception
	{
		objectType = new ObjectTypeDescriptionBuilder().withId(OBJECT_TYPE_ID).object();
		destinationCategory = new DestinationCategoryDescriptionBuilder().withId(DEST_CATEGORY_ID).object();
		
		subdestination = new SubdestinationBuilder().withDestCategoryId(DEST_CATEGORY_ID).forObjectType(OBJECT_TYPE_ID).withId(SUBDEST_ID).object();
		otherSubdestination = new SubdestinationBuilder().withDestCategoryId(OTHER_DEST_CATEGORY_ID).forObjectType(OTHER_OBJECT_TYPE_ID).withId(OTHER_SUBDEST_ID).object();
		subdestinationWithConflicts = new SubdestinationBuilder().withDestCategoryId(DEST_CATEGORY_ID).forObjectType(OBJECT_TYPE_ID).conflictsTo(new ConflictBuilder().conflictsToSubdestination(SUBDEST_ID).object()).withId(WITH_CONF_SUBDEST_ID).object();
		
		searchProvider = mock(ISearchProviderDescription.class);
		
		parser = mock(ISearchSubdestinationExtensionParser.class);
		when(parser.readContributions()).thenReturn(Arrays.asList(new ISearchSubdestination[]{subdestination, otherSubdestination,subdestinationWithConflicts}));
		
		config = new SubdestinationsActivationConfig(parser);
	}
	
	public void testGetAvailableSubdestinations()
	{
		final List<ISearchSubdestination> result = config.getAvailableSearchSubdestinations(objectType, destinationCategory, searchProvider);
		assertEquals("Two subdestinations expected", 2, result.size());
		assertTrue("Subdestination not found", result.contains(subdestination));
		assertTrue("Subdestination not found", result.contains(subdestinationWithConflicts));

	}
	
	public void testSubdestActivationNotDefaultSelected()
	{
		setDefaultSelected(subdestination, false);
		assertFalse("Subdestination should be disabled by default", config.isSubdestinationActive(subdestination, objectType, destinationCategory, searchProvider));
		assertFalse("Subdestination should be disabled by default", config.isSubdestinationActive(subdestination, objectType, destinationCategory, searchProvider));
		config.activateSubdestination(objectType, destinationCategory, searchProvider, subdestination, true);
		assertTrue("Subdestination should be enabled", config.isSubdestinationActive(subdestination, objectType, destinationCategory, searchProvider));
		config.activateSubdestination(objectType, destinationCategory, searchProvider, subdestination, false);
		assertFalse("Subdestination should be disabled", config.isSubdestinationActive(subdestination, objectType, destinationCategory, searchProvider));
	}
	
	private void setDefaultSelected(ISearchSubdestination subdestination, boolean value) {
		when(subdestination.isDefaultSelected()).thenReturn(value);
	}
	
	public void testSubdestActivationDefaultSelected()
	{
		setDefaultSelected(subdestination, true);
		assertTrue("Subdestination should be enabled by default", config.isSubdestinationActive(subdestination, objectType, destinationCategory, searchProvider));
		assertTrue("Subdestination should be enabled by default", config.isSubdestinationActive(subdestination, objectType, destinationCategory, searchProvider));
		config.activateSubdestination(objectType, destinationCategory, searchProvider, subdestination, false);
		assertFalse("Subdestination should be disabled", config.isSubdestinationActive(subdestination, objectType, destinationCategory, searchProvider));
		config.activateSubdestination(objectType, destinationCategory, searchProvider, subdestination, true);
		assertTrue("Subdestination should be enabled", config.isSubdestinationActive(subdestination, objectType, destinationCategory, searchProvider));
	}
	
	public void testSubdestNotContributed()
	{
		setDefaultSelected(subdestination, true);
		setDefaultSelected(otherSubdestination, false);
		assertFalse("Subdestination should be disabled by default", config.isSubdestinationActive(otherSubdestination, objectType, destinationCategory, searchProvider));
		assertFalse("Subdestination should be disabled by default", config.isSubdestinationActive(otherSubdestination, objectType, destinationCategory, searchProvider));
		config.activateSubdestination(objectType, destinationCategory, searchProvider, otherSubdestination, true);
		assertFalse("Subdestination should be disabled as it is not recognized", config.isSubdestinationActive(otherSubdestination, objectType, destinationCategory, searchProvider));
		config.activateSubdestination(objectType, destinationCategory, searchProvider, otherSubdestination, false);
		assertFalse("Subdestination should be disabled", config.isSubdestinationActive(otherSubdestination, objectType, destinationCategory, searchProvider));
	}
	
	public void testConflictingSubdestinations() {
		_testConflictingSubdestinations(true);
	}
	
	
	public void testConflictingSubdestinationsSymmetry() {
		config.activateSubdestination(objectType, destinationCategory, searchProvider, subdestinationWithConflicts, true);
		config.activateSubdestination(objectType, destinationCategory,searchProvider, subdestination, true);
		assertFalse("Conflicting subdestinations reflexion test failed", 
				config.isSubdestinationActive(subdestinationWithConflicts, objectType, destinationCategory, searchProvider));
	}
	
	public void testConflictingSubdestinationsDefaultSelected() {
		setDefaultSelected(subdestination, true);
		assertTrue("Subdestination should be enabled by default",
				config.isSubdestinationActive(subdestination, objectType, destinationCategory, searchProvider));
		config.activateSubdestination(objectType, destinationCategory, searchProvider,subdestinationWithConflicts, true);
		assertTrue("Subd should be active after activateSubdestination() is called, but it isn't",
				config.isSubdestinationActive(subdestinationWithConflicts, objectType, destinationCategory, searchProvider));
		assertFalse("Conflicting subdestinations should be deactivated on activating a subdestination which has conflicts", 
				config.isSubdestinationActive(subdestination, objectType, destinationCategory, searchProvider));
	}
	
	public void testConflictingSubdestinationsWithCache() {
		_testConflictingSubdestinations(true);
		_testConflictingSubdestinations(false);
	}
	
	private void _testConflictingSubdestinations(boolean firstTime) {
		
		config.activateSubdestination(objectType, destinationCategory,searchProvider, subdestination, true);
		assertTrue("Subd should be active after activateSubdestination() is called, but it isn't",
				config.isSubdestinationActive(subdestination, objectType, destinationCategory, searchProvider));
		config.activateSubdestination(objectType, destinationCategory, searchProvider,
				subdestinationWithConflicts, true);
		assertTrue("Subd should be active after activateSubdestination() is called, but it isn't",
				config.isSubdestinationActive(subdestinationWithConflicts, objectType, destinationCategory, searchProvider));
		assertFalse("Conflicting subdestinations should be deactivated on activating a subdestination which has conflicts", 
				config.isSubdestinationActive(subdestination, objectType, destinationCategory, searchProvider));
	}
}
