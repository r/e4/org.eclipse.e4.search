/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.test.unit.internal;

import java.util.List;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.AbstractExtensionPointParser;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.ObjectTypeExtensionParser;
import org.eclipse.platform.discovery.testutils.utils.registry.ExtensionRegistryBuilder;
import org.eclipse.platform.discovery.testutils.utils.testcases.AbstractExtensionPointParserTest;
import static org.junit.Assert.*;

public class ObjectTypeExtensionParserTest extends AbstractExtensionPointParserTest<IObjectTypeDescription>
{

	private static final String OBJECT_ID = "myobject";
	private static final String OBJECT_NAME = "My Object";

	@Override
	protected AbstractExtensionPointParser<IObjectTypeDescription> createParser(IExtensionRegistry registry) {
		return new ObjectTypeExtensionParser(registry);
	}
	
	@Override
	protected void setupRegistry(ExtensionRegistryBuilder registryBuilder) {
		registryBuilder.addObjectType(OBJECT_ID, OBJECT_NAME);
	}
	
	@Override
	protected void verifyContributions(List<IObjectTypeDescription> contributions) {
		assertEquals("Unexpected number of object types", 1, contributions.size());
		IObjectTypeDescription objectType = contributions.get(0);
		assertEquals("Unexpected id", OBJECT_ID, objectType.getId());
		assertEquals("Unexpected name", OBJECT_NAME, objectType.getDisplayName());		
	}
}
