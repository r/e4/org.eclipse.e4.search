package org.eclipse.platform.discovery.runtime.test.unit.internal;

import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.runtime.internal.SearchProviderConfigurationFactory;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class SearchProviderConfigurationFactoryTest
{
	@Mock
	private IExtensionRegistry registry_1;
	@Mock
	private IExtensionRegistry registry_2;

	private SearchProviderConfigurationFactory factory;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		factory = new SearchProviderConfigurationFactory();
	}

	@Test
	public void testSameConfigurationReturnedForRegistry()
	{
		assertSame("Different configuration instances returned for different registries", factory.getSearchProviderConfiguration(registry_1), factory.getSearchProviderConfiguration(registry_1));
	}

	@Test
	public void testDifferentConfigurationsReturnedForDifferentRegistries()
	{
		assertNotSame("Same configuration instances returned for different registries", factory.getSearchProviderConfiguration(registry_1), factory.getSearchProviderConfiguration(registry_2));
	}
}
