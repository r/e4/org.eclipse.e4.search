/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.test.unit;

import org.eclipse.platform.discovery.runtime.test.unit.internal.DescriptiveObjectEqualsHashcodeTest;
import org.eclipse.platform.discovery.runtime.test.unit.internal.DestinationItemPairTest;
import org.eclipse.platform.discovery.runtime.test.unit.internal.DestinationsCategoryExtensionParserTest;
import org.eclipse.platform.discovery.runtime.test.unit.internal.DestinationsProviderExtensionParserTest;
import org.eclipse.platform.discovery.runtime.test.unit.internal.DestinationsProviderWithChangeHandlerTest;
import org.eclipse.platform.discovery.runtime.test.unit.internal.GroupingHierarchyTest;
import org.eclipse.platform.discovery.runtime.test.unit.internal.MementoLoadProvidersExtensionParserTest;
import org.eclipse.platform.discovery.runtime.test.unit.internal.MementoStoreProvidersExtensionParserTest;
import org.eclipse.platform.discovery.runtime.test.unit.internal.ObjectTypeExtensionParserTest;
import org.eclipse.platform.discovery.runtime.test.unit.internal.SearchProviderActivationConfigDummyTest;
import org.eclipse.platform.discovery.runtime.test.unit.internal.SearchProviderConfigurationTest;
import org.eclipse.platform.discovery.runtime.test.unit.internal.SearchProvidersExtensionParserTest;
import org.eclipse.platform.discovery.runtime.test.unit.internal.SearchSubdestinationExtensionParserTest;
import org.eclipse.platform.discovery.runtime.test.unit.internal.SubdestinationsActivationConfigTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@SuiteClasses({
	SearchProviderConfigurationTest.class,
	DestinationsCategoryExtensionParserTest.class,
	ObjectTypeExtensionParserTest.class,
	DestinationsProviderExtensionParserTest.class,
	DestinationsProviderWithChangeHandlerTest.class,
	MementoLoadProvidersExtensionParserTest.class,
	MementoStoreProvidersExtensionParserTest.class,		
	SearchProvidersExtensionParserTest.class,
	SearchSubdestinationExtensionParserTest.class,
	GroupingHierarchyTest.class,
	SubdestinationsActivationConfigTest.class,
	DestinationItemPairTest.class,
	DescriptiveObjectEqualsHashcodeTest.class,
	SearchProviderActivationConfigDummyTest.class
})
@RunWith(Suite.class)
public class AllTestsSuite{}
