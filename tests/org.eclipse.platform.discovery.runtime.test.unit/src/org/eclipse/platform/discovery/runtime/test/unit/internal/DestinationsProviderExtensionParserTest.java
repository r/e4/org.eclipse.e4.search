/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.test.unit.internal;

import java.util.List;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.runtime.api.IDestinationsProvider;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationsProviderDescription;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.AbstractExtensionPointParser;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.DestinationsProviderExtensionParser;
import org.eclipse.platform.discovery.testutils.utils.registry.ExtensionRegistryBuilder;
import org.eclipse.platform.discovery.testutils.utils.testcases.AbstractExtensionPointParserTest;
import org.mockito.Mockito;
import static org.junit.Assert.*;


public class DestinationsProviderExtensionParserTest extends AbstractExtensionPointParserTest<IDestinationsProviderDescription> {
	
	private static final String PROVIDER_ID = "mydestprovider";
	private static final String PROVIDER_CAT = "mycategory";
	private static final String PROVIDER_FQNAME = "org.eclipse.test.MyTestProvider";
	private static final String PROVIDER_PREF_PAGE = "org.eclipse.test.MyPrefPage";

	protected IDestinationsProvider providerInstance;
	
	@Override
	protected AbstractExtensionPointParser<IDestinationsProviderDescription> createParser(IExtensionRegistry registry) {
		return new DestinationsProviderExtensionParser(registry);
	}
	
	@Override
	protected void setupRegistry(ExtensionRegistryBuilder registryBuilder) {
		providerInstance = Mockito.mock(IDestinationsProvider.class, "destination provider instance");
		registryBuilder.addDestinationsProvider(PROVIDER_ID, PROVIDER_CAT, PROVIDER_PREF_PAGE, PROVIDER_FQNAME, providerInstance);
	}

	@Override
	protected void verifyContributions(List<IDestinationsProviderDescription> contributions) {
		assertEquals("one destination provider expected", 1, contributions.size());
		IDestinationsProviderDescription providerDesc = contributions.get(0);
		
		assertEquals("Unexpected id", PROVIDER_ID, providerDesc.getId());
		assertEquals("Unexpected category", PROVIDER_CAT, providerDesc.getDestinationCategoryId());
		assertEquals("Unexpected display name", PROVIDER_FQNAME, providerDesc.getDisplayName());
		assertEquals("Unexpected preference page id",PROVIDER_PREF_PAGE, providerDesc.getPreferencePageId());
		
		assertSame("uenxpected provider returned", providerInstance, providerDesc.createProvider());
	}
}
