/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.test.unit.internal;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.platform.discovery.runtime.api.impl.DescriptiveObject;
import org.eclipse.platform.discovery.testutils.utils.testcases.EqualsTestCase;


public class DescriptiveObjectEqualsHashcodeTest extends EqualsTestCase<DescriptiveObject>{
	private static final String ID1 = "id1";
	private static final String LABEL1 = "Pesho";

	private static final String ID2 = "id2";
	private static final String LABEL2 = "Gosho";
	
	
	public DescriptiveObjectEqualsHashcodeTest() {
		super(DescriptiveObject.class);
	}

	@Override
	public Iterator<DescriptiveObject> newObjectIterator(int iterations) {
		List<DescriptiveObject> descriptions = new ArrayList<DescriptiveObject>();
		for(int i=0; i<iterations; i++) {
			descriptions.add(newEqualInstance());
		}
		return descriptions.iterator();
	}

	@Override
	public DescriptiveObject newEqualInstance() {
		return new DescriptiveObject(ID1, LABEL1);
	}

	@Override
	public DescriptiveObject newAncestorEqualInstance() {
		return new DescriptiveObject(ID1, LABEL1) {
		};
	}

	@Override
	public DescriptiveObject newNonEqualInstance() {
		return new DescriptiveObject(ID2, LABEL2);
	}

	@Override
	public void modifyObjectInstance(DescriptiveObject instance) {
		//not applicable, class is immutable
	}
}
