/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.testutils.utils.reflection;

import java.lang.reflect.Field;

/**
 * Reflection utility to be used inside of testing code.
 * 
 * @author Joerg Dehmel
 */
public final class ReflectionUtils
{
	private ReflectionUtils()
	{
		// creation prohibited
	}

	/**
	 * Provides the value of a field.
	 * 
	 * @param instance
	 *            instance that contains the field
	 * @param fieldName
	 *            name of the field
	 * @return field value, null if field not set in the instance
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	public static Object getFieldValue(final Object instance, final String fieldName) throws NoSuchFieldException, IllegalArgumentException,
									IllegalAccessException
	{
		assert instance != null && fieldName != null;

		final Field field = getField(instance.getClass(), fieldName);
		field.setAccessible(true);
		return field.get(instance);
	}

	/**
	 * Sets a new value of an instance field. This method doesn't only considers fields declared in the passed instance but also those declared in the
	 * whole super-hierarchy of the instance.
	 * 
	 * @param instance
	 *            the object thats field value has to be set
	 * @param fieldName
	 *            the name of the instance variable
	 * @param fieldValue
	 *            the new value to be set
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static void setFieldValue(final Object instance, final String fieldName, final Object fieldValue) throws SecurityException,
									NoSuchFieldException, IllegalArgumentException, IllegalAccessException
	{
		assert instance != null && fieldValue != null && fieldName != null;

		final Field field = getField(instance.getClass(), fieldName);
		field.setAccessible(true);
		field.set(instance, fieldValue);
	}
	
	
	private static Field getField(final Class<?> instanceClass, final String fieldName) throws NoSuchFieldException
	{
		assert fieldName != null;

		try
		{
			return instanceClass.getDeclaredField(fieldName);
		} catch (NoSuchFieldException e)
		{
			final Class<?> superClass = instanceClass.getSuperclass();
			if (superClass != null)
			{
				return getField(superClass, fieldName);
			}
			throw e;
		}
	}
}
