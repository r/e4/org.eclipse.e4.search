package org.eclipse.platform.discovery.testutils.utils.matchers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;

public class Matchers {
	
	public static <T> Matcher<T[]> arrayEqualsWithoutOrder(final T... elements) {
		return new BaseMatcher<T[]>() {

			@Override
			public boolean matches(Object item) {
				@SuppressWarnings("unchecked")
				T[] args = (T[])item;
				return set(elements).equals(set(args));
			}
			
			private <Q> Set<Q> set(Q... elems) {
				return new HashSet<Q>(Arrays.asList(elems));
			}

			@Override
			public void describeTo(Description description) {
				description.appendText(Arrays.toString(elements));
			}
		};
	}
}
