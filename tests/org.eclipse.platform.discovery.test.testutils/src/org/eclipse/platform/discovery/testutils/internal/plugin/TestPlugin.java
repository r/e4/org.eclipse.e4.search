/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.testutils.internal.plugin;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

public class TestPlugin extends AbstractUIPlugin
{
	private static TestPlugin INSTANCE;
	private static String pluginId;
	
	public TestPlugin()
	{
		super();
		INSTANCE = this;
	}
	
	public static TestPlugin getDefault()
	{
		return INSTANCE;
	}
	
	@Override
	public void start(BundleContext context) throws Exception
	{
		super.start(context);
		pluginId = context.getBundle().getSymbolicName();
	}
	
	@Override
	public void stop(BundleContext context) throws Exception
	{
		super.stop(context);
		INSTANCE = null;
	}

	public static void logError(Throwable e)
	{
		TestPlugin.getDefault().getLog().log(createStatus(IStatus.ERROR, e, e.getMessage()));
	}
	
	public static void logError(String message, Throwable e)
	{
		TestPlugin.getDefault().getLog().log(createStatus(IStatus.ERROR, e, message));
	}
	
	public static void logWarning(final String string)
	{
		TestPlugin.getDefault().getLog().log(createStatus(IStatus.WARNING, string));
	}
	
	public static void logInfo(final String string)
	{
		TestPlugin.getDefault().getLog().log(createStatus(IStatus.INFO, string));
	}
	
	private static IStatus createStatus(final int severity, final Throwable cause, final String message)
	{
		return new Status(severity, pluginId, message, cause);
	}
	
	private static IStatus createStatus(final int severity, final String message)
	{
		return new Status(severity, pluginId, message);
	}

}
