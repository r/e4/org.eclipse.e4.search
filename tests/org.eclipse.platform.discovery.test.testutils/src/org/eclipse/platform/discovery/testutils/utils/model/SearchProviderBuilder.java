/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.testutils.utils.model;

import java.util.Arrays;
import java.util.HashSet;

import org.eclipse.platform.discovery.runtime.api.GroupingHierarchy;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.ISearchParameters;
import org.eclipse.platform.discovery.runtime.api.ISearchProvider;
import org.eclipse.platform.discovery.runtime.api.ISearchQuery;
import org.mockito.Mockito;

public class SearchProviderBuilder extends Builder<ISearchProvider>
{
	public SearchProviderBuilder()
	{
		super(Mockito.mock(ISearchProvider.class));
	}
	
	public SearchProviderBuilder createsQuery(final ISearchQuery query)
	{
		Mockito.stub(object().createQuery(Mockito.any(ISearchParameters.class))).toReturn(query);
		return this;
	}
	
	@SuppressWarnings("unchecked")
	public SearchProviderBuilder withGroupingHierarchies(final GroupingHierarchy... hierarchies)
	{
		Mockito.stub(object().getGroupingHierarchies(Mockito.any(ISearchDestination.class), Mockito.anySet())).toReturn(new HashSet<GroupingHierarchy>(Arrays.asList(hierarchies)));
		return this;
	}

}
