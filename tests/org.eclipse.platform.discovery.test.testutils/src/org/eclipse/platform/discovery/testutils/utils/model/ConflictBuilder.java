/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.testutils.utils.model;

import org.eclipse.platform.discovery.runtime.api.IConflict;
import org.mockito.Mockito;

public class ConflictBuilder extends Builder<IConflict>
{
	public ConflictBuilder()
	{
		super(Mockito.mock(IConflict.class));
	}

	public ConflictBuilder conflictsToSubdestination(final String subdestinationId)
	{
		Mockito.stub(object().getconflictingSubdID()).toReturn(subdestinationId);
		return this;
	}
}
