/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.testutils.utils.model;

import org.eclipse.platform.discovery.runtime.api.IDescriptiveObject;
import org.mockito.Mockito;

public abstract class DescriptiveObjectBuilder<T extends IDescriptiveObject> extends Builder<T>
{
	public DescriptiveObjectBuilder(T object)
	{
		super(object);
	}

	public DescriptiveObjectBuilder<T> withId(final String id)
	{
		Mockito.stub(object().getId()).toReturn(id);
		return this;
	}

	public DescriptiveObjectBuilder<T> withName(final String name)
	{
		Mockito.stub(object().getDisplayName()).toReturn(name);
		return this;
	}
}
