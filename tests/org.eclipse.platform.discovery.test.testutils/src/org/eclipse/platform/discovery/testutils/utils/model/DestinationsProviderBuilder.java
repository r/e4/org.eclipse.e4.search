/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.testutils.utils.model;

import java.util.Arrays;
import java.util.HashSet;

import org.eclipse.platform.discovery.runtime.api.IDestinationsProvider;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.mockito.Mockito;

public class DestinationsProviderBuilder extends Builder<IDestinationsProvider>
{
	public DestinationsProviderBuilder()
	{
		super(Mockito.mock(IDestinationsProvider.class));
	}

	public DestinationsProviderBuilder withDestinations(final ISearchDestination... destinations)
	{
		Mockito.stub(object().getSearchDestinations()).toReturn(new HashSet<ISearchDestination>(Arrays.asList(destinations)));
		return this;
	}
}
