/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.testutils.utils.testcases;

import org.eclipse.platform.discovery.testutils.utils.registry.ExtensionRegistryBuilder;


public abstract class AbstractExtensionPointParserTest<T> extends GenericExtensionPointParserTest<T, ExtensionRegistryBuilder> {
	@Override
	protected ExtensionRegistryBuilder createRegistryBuilder() {
		return new ExtensionRegistryBuilder();
	}
	
}
