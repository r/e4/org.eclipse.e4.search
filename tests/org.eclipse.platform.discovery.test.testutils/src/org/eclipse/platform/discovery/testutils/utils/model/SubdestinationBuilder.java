/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.testutils.utils.model;

import java.util.Arrays;
import java.util.HashSet;

import org.eclipse.platform.discovery.runtime.api.IConflict;
import org.eclipse.platform.discovery.runtime.api.ISearchSubdestination;
import org.mockito.Mockito;

public class SubdestinationBuilder extends DescriptiveObjectBuilder<ISearchSubdestination>
{
	public SubdestinationBuilder()
	{
		super(Mockito.mock(ISearchSubdestination.class));
	}

	public SubdestinationBuilder withDestCategoryId(final String destCategoryId)
	{
		Mockito.stub(object().getDestinationCategoryId()).toReturn(destCategoryId);
		return this;
	}

	public SubdestinationBuilder forObjectType(final String objectTypeId)
	{
		Mockito.stub(object().getObjectTypeId()).toReturn(objectTypeId);
		return this;
	}

	public SubdestinationBuilder defaultSelected(final boolean defaultSelected)
	{
		Mockito.stub(object().isDefaultSelected()).toReturn(defaultSelected);
		return this;
	}

	public SubdestinationBuilder conflictsTo(IConflict... conflicts)
	{
		Mockito.stub(object().getConflictingSubd()).toReturn(new HashSet<IConflict>(Arrays.asList(conflicts)));
		return this;
	}
}
