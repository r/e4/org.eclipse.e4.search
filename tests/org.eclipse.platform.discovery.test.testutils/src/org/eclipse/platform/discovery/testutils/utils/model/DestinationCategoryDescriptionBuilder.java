/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.testutils.utils.model;

import java.util.Arrays;

import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.mockito.Mockito;

public class DestinationCategoryDescriptionBuilder extends DescriptiveObjectBuilder<IDestinationCategoryDescription>
{
	public DestinationCategoryDescriptionBuilder()
	{
		super(Mockito.mock(IDestinationCategoryDescription.class));
	}

	public DestinationCategoryDescriptionBuilder withDestinationProviders(final String... providersIds)
	{
		Mockito.stub(object().getDestinationProviderIds()).toReturn(Arrays.asList(providersIds));
		return this;
	}

	public DestinationCategoryDescriptionBuilder withDestinationClass(final Class<ISearchDestination> destClass)
	{
		Mockito.stub(object().getDestinationsClass()).toReturn(destClass);
		return this;
	}
}
