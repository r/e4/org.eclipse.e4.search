/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.testutils.utils.model;

import java.util.Arrays;
import java.util.HashSet;

import org.eclipse.platform.discovery.runtime.api.ISearchProvider;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.ISearchProviderDescription;
import org.mockito.Mockito;

public class SearchProviderDescriptionBuilder extends DescriptiveObjectBuilder<ISearchProviderDescription>
{
	public SearchProviderDescriptionBuilder()
	{
		super(Mockito.mock(ISearchProviderDescription.class));
	}

	public SearchProviderDescriptionBuilder supportsTextSearch(final boolean supportsTextSearch)
	{
		Mockito.stub(object().supportsTextSearch()).toReturn(supportsTextSearch);
		return this;
	}

	public SearchProviderDescriptionBuilder withSearchProviderInstance(final ISearchProvider searchProvider)
	{
		Mockito.stub(object().createInstance()).toReturn(searchProvider);
		return this;
	}

	public SearchProviderDescriptionBuilder withDestinationCategories(final IDestinationCategoryDescription... categories)
	{
		Mockito.stub(object().getSupportedDestinationCategories()).toReturn(new HashSet<IDestinationCategoryDescription>(Arrays.asList(categories)));
		return this;
	}

	public SearchProviderDescriptionBuilder supportsObjectType(final IObjectTypeDescription objectType)
	{
		Mockito.stub(object().getObjectType()).toReturn(objectType);
		return this;
	}
}
