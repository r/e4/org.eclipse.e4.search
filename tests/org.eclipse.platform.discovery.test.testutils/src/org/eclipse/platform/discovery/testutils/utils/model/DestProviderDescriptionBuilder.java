/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.testutils.utils.model;

import org.eclipse.platform.discovery.runtime.api.IDestinationsProvider;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationsProviderDescription;
import org.mockito.Mockito;

public class DestProviderDescriptionBuilder extends DescriptiveObjectBuilder<IDestinationsProviderDescription>
{
	public DestProviderDescriptionBuilder()
	{
		super(Mockito.mock(IDestinationsProviderDescription.class));
	}

	public DestProviderDescriptionBuilder withProvider(final IDestinationsProvider destProviderInstance)
	{
		Mockito.stub(object().createProvider()).toReturn(destProviderInstance);
		return this;
	}

	public DestProviderDescriptionBuilder withParefPageId(final String prefPageId)
	{
		Mockito.stub(object().getPreferencePageId()).toReturn(prefPageId);
		return this;
	}
}
