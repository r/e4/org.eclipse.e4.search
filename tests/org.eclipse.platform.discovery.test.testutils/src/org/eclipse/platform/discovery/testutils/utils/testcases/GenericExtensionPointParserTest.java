/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.testutils.utils.testcases;

import java.util.List;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.AbstractExtensionPointParser;
import org.eclipse.platform.discovery.testutils.utils.registry.ExtensionRegistryBuilder;
import org.junit.Before;
import org.junit.Test;

public abstract class GenericExtensionPointParserTest<T, E extends ExtensionRegistryBuilder> {
	private AbstractExtensionPointParser<T> parser;
	
	private E extensionRegistryBuilder = createRegistryBuilder();

	protected abstract E createRegistryBuilder();
	
	@Before
	public final void setUp() {
		
		setupRegistry(extensionRegistryBuilder);
		parser = createParser(extensionRegistryBuilder.getRegistry());
	}

	protected abstract AbstractExtensionPointParser<T> createParser(IExtensionRegistry registry);
	
	protected abstract void setupRegistry(E registryBuilder); 

	@Test
	public void testReadObjects() {
		verifyContributions(parser.readContributions());
	}

	protected abstract void verifyContributions(List<T> contributions);
}
