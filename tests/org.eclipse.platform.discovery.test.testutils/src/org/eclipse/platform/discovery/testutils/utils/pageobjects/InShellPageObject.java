/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.testutils.utils.pageobjects;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swtbot.swt.finder.SWTBot;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotShell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.FormToolkit;

/**
 * Basic implementation of a page object which creates UI in a test shell
 * 
 * @author Danail Branekov
 * 
 */
public abstract class InShellPageObject
{
	private SWTBotShell botShell;
	private static final String TEST_SHELL_LABEL = "Testing Shell";
	private FormToolkit formToolkit;

	public void open()
	{
		final Shell shell = createShell();
		botShell = new SWTBotShell(shell);
		botShell.activate();
	}

	public void close()
	{
		botShell.close();
	}

	/**
	 * Implementors should create the UI to be tested here. This method is invoked from the UI thread
	 * 
	 * @param parent
	 *            the parent shell; by default its layout is set to {@link FillLayout}
	 * @param formToolkit
	 *            toolkit for creating UI stuff
	 */
	protected abstract void createContent(final Shell parent, final FormToolkit formToolkit);

	/**
	 * Implementers may dispose resources upon shell disposal via overriding this method. The method is invoked from the UI thread
	 * 
	 * @param e
	 *            the dispose event
	 */
	protected void dispose(DisposeEvent e)
	{
		this.formToolkit.dispose();
	}

	protected Display display()
	{
		return PlatformUI.getWorkbench().getDisplay();
	}

	protected Shell createShell()
	{
		final Shell[] theShell = new Shell[1];
		final Runnable createShellRunnable = new Runnable()
		{
			@Override
			public void run()
			{
				formToolkit = new FormToolkit(display());
				theShell[0] = new Shell(display(), SWT.RESIZE);
				theShell[0].setText(TEST_SHELL_LABEL);
				theShell[0].setLayout(new FillLayout());
				createContent(theShell[0], formToolkit);
				theShell[0].layout(true);
				theShell[0].addDisposeListener(new DisposeListener()
				{
					@Override
					public void widgetDisposed(DisposeEvent e)
					{
						dispose(e);
					}
				});
				theShell[0].open();
			}
		};
		display().syncExec(createShellRunnable);
		return theShell[0];
	}

	protected SWTBot bot()
	{
		return botShell.bot();
	}

	protected SWTBotShell shell()
	{
		return botShell;
	}
	
	public Shell getShell() {
	    return botShell.widget;
	}
}
