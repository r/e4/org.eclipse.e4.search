/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import javax.xml.transform.sax.TransformerHandler;

import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.ui.internal.tooltip.FormTextBuilder;
import org.eclipse.platform.discovery.util.internal.xml.IXMLUtils;
import org.jmock.core.Constraint;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.AttributesImpl;


public class FormTextBuilderTest extends MockObjectTestCase
{
	private Mock<IXMLUtils> xmlUtils;
	private Mock<TransformerHandler> transformerHandler;
	private FormTextBuilder formTextBuilder;

	@Override
	protected void setUp() throws Exception
	{
		transformerHandler = mock(TransformerHandler.class);
		transformerHandler.expects(once()).method("startDocument");
		transformerHandler.expects(once()).method("startElement").with(eq(""), eq(""), eq("form"), emptyAttributesConstraint());

		xmlUtils = mock(IXMLUtils.class);
		xmlUtils.expects(once()).method("createTransformerHandler").will(returnValue(transformerHandler.proxy()));

		formTextBuilder = new FormTextBuilder()
		{
			@Override
			protected IXMLUtils xmlUtils()
			{
				return xmlUtils.proxy();
			}
		};
	}

	public void testStartParagraphWithVSpace()
	{
		transformerHandler.expects(once()).method("startElement").with(eq(""), eq(""), eq("p"), vspaceAttributesConstraint(true));
		formTextBuilder.startParagraph(true);
	}
	
	public void testStartParagraphWithoutVSpace()
	{
		transformerHandler.expects(once()).method("startElement").with(eq(""), eq(""), eq("p"), vspaceAttributesConstraint(false));
		formTextBuilder.startParagraph(false);
	}

	public void testEndParagraph()
	{
		transformerHandler.expects(once()).method("endElement").with(eq(""), eq(""), eq("p"));
		formTextBuilder.endParagraph();
	}

	public void testAppendBoldedText()
	{
		final String text = "This is my test string";
		transformerHandler.expects(once()).method("startElement").with(eq(""), eq(""), eq("b"), emptyAttributesConstraint());
		transformerHandler.expects(once()).method("characters").with(charArrConstraint(text.toCharArray()), eq(0), eq(text.length()));
		transformerHandler.expects(once()).method("endElement").with(eq(""), eq(""), eq("b"));
		formTextBuilder.appendBoldedText(text);
	}
	
	public void testAppendText()
	{
		final String text = "This is my test string";
		transformerHandler.expects(once()).method("characters").with(charArrConstraint(text.toCharArray()), eq(0), eq(text.length()));
		formTextBuilder.appendText(text);
	}
	
	public void testAppendProperty()
	{
		final String propertyName = "My property";
		final String propertyValue = "My value";
		
		transformerHandler.expects(once()).method("startElement").with(eq(""), eq(""), eq("p"), vspaceAttributesConstraint(false));
		transformerHandler.expects(once()).method("startElement").with(eq(""), eq(""), eq("b"), emptyAttributesConstraint());
		transformerHandler.expects(once()).method("characters").with(charArrConstraint(propertyName.toCharArray()), eq(0), eq(propertyName.length()));
		transformerHandler.expects(once()).method("endElement").with(eq(""), eq(""), eq("b"));
		transformerHandler.expects(once()).method("characters").with(charArrConstraint(" ".toCharArray()), eq(0), eq(1));
		transformerHandler.expects(once()).method("characters").with(charArrConstraint(propertyValue.toCharArray()), eq(0), eq(propertyValue.length()));
		transformerHandler.expects(once()).method("endElement").with(eq(""), eq(""), eq("p"));
		
		formTextBuilder.appendProperty(propertyName, propertyValue);
	}
	
	public void testGetText()
	{
		transformerHandler.expects(once()).method("endElement").with(eq(""), eq(""), eq("form"));
		transformerHandler.expects(once()).method("endDocument");
		
		formTextBuilder.getText();
	}
	
	private Constraint charArrConstraint(final char[] charArray)
	{
		return new Constraint()
		{
			@Override
			public boolean eval(Object arg0)
			{
				final char[] input = (char[]) arg0;
				if (input.length != charArray.length)
				{
					return false;
				}

				for (int i = 0; i < charArray.length; i++)
				{
					if (charArray[i] != input[i])
					{
						return false;
					}
				}

				return true;
			}

			@Override
			public StringBuffer describeTo(StringBuffer arg0)
			{
				return arg0;
			}
		};
	}

	private Constraint emptyAttributesConstraint()
	{
		return new Constraint()
		{
			@Override
			public boolean eval(Object arg0)
			{
				return ((Attributes) arg0).getLength() == 0;
			}

			@Override
			public StringBuffer describeTo(StringBuffer arg0)
			{
				return arg0;
			}
		};
	}
	
	private Constraint vspaceAttributesConstraint(final boolean value)
	{
		return new Constraint()
		{
			@Override
			public boolean eval(Object arg0)
			{
				final AttributesImpl attr = (AttributesImpl)arg0;
				return attr.getLength() == 1 && attr.getValue(0).equals(Boolean.toString(value)) && attr.getQName(0).equals("vspace"); 
			}

			@Override
			public StringBuffer describeTo(StringBuffer arg0)
			{
				return arg0;
			}
		};
	}
	
}
