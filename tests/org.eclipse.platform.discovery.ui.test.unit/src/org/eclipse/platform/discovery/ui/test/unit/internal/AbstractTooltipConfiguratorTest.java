/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.ui.api.IFormTextBuilder;
import org.eclipse.platform.discovery.ui.internal.tooltip.AbstractTooltipConfigurator;
import org.eclipse.platform.discovery.ui.internal.tooltip.IToolTipConfigurator;
import org.eclipse.platform.discovery.ui.internal.tooltip.AbstractTooltipConfigurator.TooltipFormTextBuilder;


public abstract class AbstractTooltipConfiguratorTest extends MockObjectTestCase
{
	protected Mock<IFormTextBuilder> textBuilder;
	private IToolTipConfigurator configurator;
	protected static final String TEXT_BUILDER_TEST_TEXT = "This is a test!";
	
	@Override
	protected void setUp() throws Exception
	{
		textBuilder = mock(IFormTextBuilder.class);
		configurator = createConfigurator();
	}
	
	protected IToolTipConfigurator createConfigurator()
	{
		return new AbstractTooltipConfigurator(){
			@Override
			protected void createContent(final IFormTextBuilder tooltipBuilder)
			{
				assertTrue("Unexpected tooltip content", tooltipBuilder.equals(textBuilder.proxy()));
			}
			
			@Override
			protected TooltipFormTextBuilder createNewTooltipTextBuilder()
			{
				return createFormTextBuilderMock(textBuilder.proxy());
				
			}
		};
	}
	
	public void testGetFormText()
	{
		assertEquals("Unexpected text", TEXT_BUILDER_TEST_TEXT, configurator.getFormText());
	}
	
	public void testGetTitleCaption()
	{
		assertNull("Title caption shoulkd be null in the abstract implementation", configurator.getTitleCaption());
	}
	
	public void testGetTitleImage()
	{
		assertNull("Title image shoulkd be null in the abstract implementation", configurator.getTitleImage());
	}
	
	protected TooltipFormTextBuilder createFormTextBuilderMock(final IFormTextBuilder delegate)
	{
		final InvocationHandler invHandler = new InvocationHandler()
		{
			@Override
			public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
			{
				if(method.getName().equals("getText"))
				{
					return TEXT_BUILDER_TEST_TEXT;
				}
				
				final Method delegationMethod = IFormTextBuilder.class.getMethod(method.getName(), method.getParameterTypes());
				return delegationMethod.invoke(delegate, args);
			}
		};
		
		return (TooltipFormTextBuilder)Proxy.newProxyInstance(this.getClass().getClassLoader(), new Class[]{TooltipFormTextBuilder.class}, invHandler);
	}
}
