/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import java.util.List;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.AbstractExtensionPointParser;
import org.eclipse.platform.discovery.testutils.utils.registry.ExtensionRegistryBuilder;
import org.eclipse.platform.discovery.testutils.utils.testcases.AbstractExtensionPointParserTest;
import org.eclipse.platform.discovery.ui.api.IAdvancedSearchParamsUiContributor;
import org.eclipse.platform.discovery.ui.internal.search.advancedparams.IAdvancedSearchParamsUiContributorDescr;
import org.eclipse.platform.discovery.ui.internal.xp.impl.AdvancedSearchParamsUiContribXpParser;
import org.mockito.Mockito;
import static org.junit.Assert.*;

public class AdvancedSearchParamsUiContribXpParserTest extends AbstractExtensionPointParserTest<IAdvancedSearchParamsUiContributorDescr>
{
	private static final String CONTRIBUTOR_ID = "mycontributor";
	private static final String SEARCH_PROVIDER_ID = "mysearchprovider";
	private static final String CONTRIBUTOR_CLASS = "org.eclipse.test.MyContributor";
	
	private IAdvancedSearchParamsUiContributor contributor;
	
	@Override
	protected AbstractExtensionPointParser<IAdvancedSearchParamsUiContributorDescr> createParser(IExtensionRegistry registry) {
		return new AdvancedSearchParamsUiContribXpParser(registry);
	}

	@Override
	protected void setupRegistry(ExtensionRegistryBuilder registryBuilder) {
		contributor = Mockito.mock(IAdvancedSearchParamsUiContributor.class);
		registryBuilder.addAdvancedSearchParamsUiContributor(CONTRIBUTOR_ID, SEARCH_PROVIDER_ID, CONTRIBUTOR_CLASS, contributor);
	}

	@Override
	protected void verifyContributions(List<IAdvancedSearchParamsUiContributorDescr> contributions) {
		assertEquals("One contributor expected", 1, contributions.size());
		final IAdvancedSearchParamsUiContributorDescr contributorDescr = contributions.get(0);
		assertEquals("Unexpected id", CONTRIBUTOR_ID, contributorDescr.getId());
		assertEquals("Unexpected search provider id", SEARCH_PROVIDER_ID, contributorDescr.getSearchProviderId());
		assertSame("Unexpected custom ui contributor", contributor, contributorDescr.createContributor());		
	}
}
