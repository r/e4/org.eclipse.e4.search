/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import java.util.List;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.AbstractExtensionPointParser;
import org.eclipse.platform.discovery.testutils.utils.registry.ExtensionRegistryBuilder;
import org.eclipse.platform.discovery.testutils.utils.testcases.AbstractExtensionPointParserTest;
import org.eclipse.platform.discovery.ui.api.ISearchResultCustomUiCreator;
import org.eclipse.platform.discovery.ui.internal.selector.ICustomResultUiContributorDescription;
import org.eclipse.platform.discovery.ui.internal.xp.impl.CustomResultUiXpParser;
import org.mockito.Mockito;
import static org.junit.Assert.*;


public class CustomResultUiXpParserTest extends AbstractExtensionPointParserTest<ICustomResultUiContributorDescription>
{
	private static final String CONTRIBUTOR_ID = "mycontributor";
	private static final String SEARCH_PROVIDER_ID = "mysearchprovider";
	private static final String CREATOR_CLASS = "org.eclipse.test.MyUiCreator";

	private ISearchResultCustomUiCreator uiCreator;

	@Override
	protected AbstractExtensionPointParser<ICustomResultUiContributorDescription> createParser(IExtensionRegistry registry) {
		return new CustomResultUiXpParser(registry);
	}

	@Override
	protected void setupRegistry(ExtensionRegistryBuilder registryBuilder) {
		uiCreator = Mockito.mock(ISearchResultCustomUiCreator.class);
		registryBuilder.addCustomResultUiContributor(CONTRIBUTOR_ID, SEARCH_PROVIDER_ID, CREATOR_CLASS, uiCreator);
	}

	@Override
	protected void verifyContributions(List<ICustomResultUiContributorDescription> contributions) {
		assertEquals("One contributor expected", 1, contributions.size());
		ICustomResultUiContributorDescription contributorDescription = contributions.get(0);
		assertEquals("unexpected id", CONTRIBUTOR_ID, contributorDescription.getId());
		assertEquals("unexpected search provider id", SEARCH_PROVIDER_ID, contributorDescription.getSearchProviderId());
		assertSame("unexpected ui creator", uiCreator, contributorDescription.createUiCreator());

	}
	
}
