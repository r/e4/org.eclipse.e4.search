package org.eclipse.platform.discovery.ui.test.unit.internal;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.runtime.api.IDestinationsProvider;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.ISearchProvider;
import org.eclipse.platform.discovery.runtime.api.ISearchSubdestination;
import org.eclipse.platform.discovery.runtime.internal.ISearchProviderConfiguration;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.ISearchProviderDescription;
import org.eclipse.platform.discovery.testutils.utils.registry.ExtensionRegistryBuilder;
import org.eclipse.platform.discovery.ui.internal.view.SearchConsoleView;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class SubdestinationsSelectionPersistedTest
{
	private SearchConsoleViewExtended consoleView;
	private IExtensionRegistry extensionRegistry;

	@Mock
	private IDestinationsProvider destinationsProvider;
	@Mock
	private ISearchDestination searchDestination;
	@Mock
	private ISearchProviderDescription searchProviderDescr;
	@Mock
	private IObjectTypeDescription objectType;
	@Mock
	private IDestinationCategoryDescription destCategory;
	@Mock
	private ISearchProviderDescription searchProvider;
	@Mock
	private ISearchSubdestination subdestination;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		consoleView = new SearchConsoleViewExtended();

		stubObjectType();
		stubDestinationCategory();
		stubDestinationsProvider();
		stubSearchProvider();
		stubSubdestination();

		extensionRegistry = stubExtensionsRegistry();
	}

	private void stubDestinationsProvider()
	{
		Mockito.stub(destinationsProvider.getSearchDestinations()).toReturn(new HashSet<ISearchDestination>(Arrays.asList(new ISearchDestination[] { searchDestination })));
	}

	private void stubSubdestination()
	{
		Mockito.stub(subdestination.getId()).toReturn("my.subdestination");
		final String destCategoryId = destCategory.getId();
		Mockito.stub(subdestination.getDestinationCategoryId()).toReturn(destCategoryId);
	}

	private void stubSearchProvider()
	{
		Mockito.stub(searchProvider.getId()).toReturn("my.search.provider.id");
		Mockito.stub(searchProvider.getObjectType()).toReturn(objectType);
	}

	private void stubDestinationCategory()
	{
		Mockito.stub(destCategory.getId()).toReturn("my.category.id");
		Mockito.stub(destCategory.getDestinationProviderIds()).toReturn(Arrays.asList(new String[] { "my.dest.provider.id" }));
		Mockito.stub(destCategory.getDestinationsClass()).toReturn(ISearchDestination.class);
	}

	private void stubObjectType()
	{
		Mockito.stub(objectType.getId()).toReturn("my.object.id");
	}

	private IExtensionRegistry stubExtensionsRegistry()
	{
		final ExtensionRegistryBuilder registryBuilder = new ExtensionRegistryBuilder();
		registryBuilder.addObjectType(objectType.getId(), objectType.getDisplayName());
		registryBuilder.addDestinationCategory(destCategory.getId(), destCategory.getDisplayName());
		registryBuilder.addSearchProvider(searchProvider.getId(), "foo", Mockito.mock(ISearchProvider.class), objectType.getId(), "true", destCategory.getId());
		registryBuilder.addSearchSubdestination(subdestination.getId(), subdestination.getDisplayName(), subdestination.getDestinationCategoryId(), objectType.getId(), "false");
		return registryBuilder.getRegistry();
	}

	@Test
	public void testSubdestinationsSelectionPersisted()
	{
		consoleView.getSearchProviderConfiguration().activateSubdestination(objectType, destCategory, searchProvider, subdestination, true);
		assertTrue("Subdestination is still inactive", consoleView.getSearchProviderConfiguration().isSubdestinationActive(subdestination, objectType, destCategory, searchProvider));
	}

	private class SearchConsoleViewExtended extends SearchConsoleView
	{
		@Override
		protected IExtensionRegistry extensionRegistry()
		{
			return extensionRegistry;
		}

		@Override
		protected ISearchProviderConfiguration getSearchProviderConfiguration()
		{
			return super.getSearchProviderConfiguration();
		}
	}
}
