/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import java.util.List;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.AbstractExtensionPointParser;
import org.eclipse.platform.discovery.testutils.utils.registry.ExtensionRegistryBuilder;
import org.eclipse.platform.discovery.testutils.utils.testcases.AbstractExtensionPointParserTest;
import org.eclipse.platform.discovery.ui.internal.search.advancedparams.IAdvancedSearchParamsUiContributorDescr;
import org.eclipse.platform.discovery.ui.internal.xp.impl.AdvancedSearchParamsUiContribXpParser;
import static org.junit.Assert.*;

public class ExceptionThrowingAdvancedUiContributorTest extends AbstractExtensionPointParserTest<IAdvancedSearchParamsUiContributorDescr> {

	@Override
	protected AbstractExtensionPointParser<IAdvancedSearchParamsUiContributorDescr> createParser(IExtensionRegistry registry) {
		return new AdvancedSearchParamsUiContribXpParser(registry);
	}

	@Override
	protected void setupRegistry(ExtensionRegistryBuilder registryBuilder) {
		registryBuilder.addExceptionThrowingAdvancedSearchParamsUiContributor("foo", "bar", "baz");
	}

	@Override
	protected void verifyContributions(List<IAdvancedSearchParamsUiContributorDescr> contributions) {
		IAdvancedSearchParamsUiContributorDescr theContributorDescription = contributions.get(0);
		try{
			theContributorDescription.createContributor();
			fail("illegalstate not thrown");
		}catch(IllegalStateException ex) {
			//ok
		}
	}

}
