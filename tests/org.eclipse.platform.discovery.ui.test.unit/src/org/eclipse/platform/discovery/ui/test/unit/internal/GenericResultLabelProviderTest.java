/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.ui.api.IGenericViewCustomization;
import org.eclipse.platform.discovery.ui.api.IMasterDiscoveryView;
import org.eclipse.platform.discovery.ui.api.IResultsViewAccessor;
import org.eclipse.platform.discovery.ui.api.ITooltipProvider;
import org.eclipse.platform.discovery.ui.api.ISearchConsoleCustomization;
import org.eclipse.platform.discovery.ui.api.impl.GenericViewCustomizationImpl;
import org.eclipse.platform.discovery.ui.internal.tooltip.FormTextBuilder;
import org.eclipse.platform.discovery.ui.internal.view.result.impl.GenericResultLabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.PlatformUI;


public class GenericResultLabelProviderTest extends MockObjectTestCase
{
	private static final String TEXT_1 = "Text1";
	private static final String TEXT_2 = "Text2";

	private Image image1;
	private Image image2;

	private Mock<ILabelProvider> labelProvider1;
	private Mock<ILabelProvider> labelProvider2;
	private Mock<ITooltipProvider> tooltipProvider1;
	private Mock<ITooltipProvider> tooltipProvider2;

	private Object element1, element2, element3;

	private Set<IGenericViewCustomization> customizations;
	private GenericResultLabelProvider testedProvider;

	@Override
	protected void setUp() throws Exception
	{
		element1 = new Object();
		element2 = new Object();
		element3 = new Object();

		setupImages();
		setupLabelProviders();
		setupTooltipProviders();
		setupCustomizations();
		
		testedProvider = new GenericResultLabelProvider(new ArrayList<IGenericViewCustomization>(customizations));
	}

	private void setupImages()
	{
		image1 = new Image(PlatformUI.getWorkbench().getDisplay(), GenericResultContentProviderTest.class.getResourceAsStream("resources/img1.gif"));
		assertNotNull(image1);
		image2 = new Image(PlatformUI.getWorkbench().getDisplay(), GenericResultContentProviderTest.class.getResourceAsStream("resources/img2.gif"));
		assertNotNull(image2);
	}

	private void setupCustomizations()
	{
		customizations = new HashSet<IGenericViewCustomization>();
		customizations.add(viewCustomization(labelProvider1.proxy(), tooltipProvider1.proxy()));		
		customizations.add(viewCustomization(labelProvider2.proxy(), tooltipProvider2.proxy()));
		customizations.add(viewCustomization(null, null));
	}
	
	private ISearchConsoleCustomization viewCustomization(final ILabelProvider lProvider, final ITooltipProvider tooltipProvider)
	{
		return new TestCustomization(){

			public boolean acceptSearchProvider(String searchProviderId)
			{
				throw new UnsupportedOperationException();
			}

			public ITreeContentProvider getContentProvider()
			{
				throw new UnsupportedOperationException();
			}

			public ILabelProvider getLabelProvider()
			{
				return lProvider;
			}

			public ITooltipProvider getTooltipProvider()
			{
				return tooltipProvider;
			}

			public void installAction(IContributedAction contributedAction, IResultsViewAccessor viewAccessor)
			{
				throw new UnsupportedOperationException();
			}

			public void setMasterView(IMasterDiscoveryView masterView)
			{
				throw new UnsupportedOperationException();
			}};
	}

	private void setupTooltipProviders()
	{
		tooltipProvider1 = mock(ITooltipProvider.class);
		tooltipProvider2 = mock(ITooltipProvider.class);
	}

	private void setupLabelProviders()
	{
		labelProvider1 = mock(ILabelProvider.class);
		labelProvider1.stubs().method("getImage").with(eq(element1)).will(returnValue(image1));
		labelProvider1.stubs().method("getImage").with(eq(element2)).will(returnValue(null));
		labelProvider1.stubs().method("getImage").with(eq(element3)).will(returnValue(null));
		labelProvider1.stubs().method("getText").with(eq(element1)).will(returnValue(TEXT_1));
		labelProvider1.stubs().method("getText").with(eq(element2)).will(returnValue(null));
		labelProvider1.stubs().method("getText").with(eq(element3)).will(returnValue(null));

		labelProvider2 = mock(ILabelProvider.class);
		labelProvider2.stubs().method("getImage").with(eq(element1)).will(returnValue(null));
		labelProvider2.stubs().method("getImage").with(eq(element2)).will(returnValue(image2));
		labelProvider2.stubs().method("getImage").with(eq(element3)).will(returnValue(null));
		labelProvider2.stubs().method("getText").with(eq(element1)).will(returnValue(null));
		labelProvider2.stubs().method("getText").with(eq(element2)).will(returnValue(TEXT_2));
		labelProvider2.stubs().method("getText").with(eq(element3)).will(returnValue(null));
	}
	
	public void testGetText()
	{
		assertEquals("Unexpected text", TEXT_1, testedProvider.getText(element1));
		assertEquals("Unexpected text", TEXT_2, testedProvider.getText(element2));
		assertNull("Unexpected text", testedProvider.getText(element3));
	}

	public void testGetToolTipTextForUI()
	{
		tooltipProvider1.expects(once()).method("createTooltipContent").with(isA(FormTextBuilder.class), eq(element1));
		tooltipProvider2.expects(once()).method("createTooltipContent").with(isA(FormTextBuilder.class), eq(element1));
		testedProvider.getToolTipText(element1);
	}
	
	public void testGetImage()
	{
		assertTrue("Unexpected image", image1 == testedProvider.getImage(element1));
		assertTrue("Unexpected image", image2 == testedProvider.getImage(element2));
		assertNull("Unexpected image", testedProvider.getImage(element3));
	}
	
	@Override
	protected void tearDown() throws Exception
	{
		image1.dispose();
		image2.dispose();
	}
	
	private abstract class TestCustomization extends GenericViewCustomizationImpl implements ISearchConsoleCustomization
	{
	}
}
