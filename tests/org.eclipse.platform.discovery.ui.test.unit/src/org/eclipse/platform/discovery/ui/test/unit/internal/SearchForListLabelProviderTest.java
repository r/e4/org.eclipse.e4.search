/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.ui.internal.view.impl.DisplayableObjectLabelProvider;


public class SearchForListLabelProviderTest extends MockObjectTestCase
{
	private static final String OBJECT_NAME = "TestObjectName";

	private DisplayableObjectLabelProvider provider;

	@Override
	protected void setUp() throws Exception
	{
		provider = new DisplayableObjectLabelProvider();
	}

	public void testGetName()
	{
		final Mock<IObjectTypeDescription> objectType1 = mock(IObjectTypeDescription.class);
		objectType1.stubs().method("getDisplayName").will(returnValue(OBJECT_NAME));

		assertEquals(OBJECT_NAME, provider.getText(objectType1.proxy()));
	}
}
