/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.eclipse.core.runtime.ListenerList;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.ui.api.IGenericViewCustomization;
import org.eclipse.platform.discovery.ui.api.IResultsViewAccessor;
import org.eclipse.platform.discovery.ui.api.impl.GenericViewCustomizationImpl;
import org.eclipse.platform.discovery.ui.internal.view.dnd.impl.DragSrcInteractionListener;
import org.eclipse.platform.discovery.ui.internal.view.dnd.impl.LocalSelectionTransferSetter;
import org.eclipse.platform.discovery.ui.internal.view.dnd.impl.XmlDiscoverySelectionTransferSetter;
import org.eclipse.platform.discovery.ui.internal.view.result.impl.DiscoveryTreeViewerFactory;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;
import org.eclipse.platform.discovery.util.api.env.IErrorHandler;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;


public class DiscoveryTreeViewerFactoryTest extends MockObjectTestCase
{
	private Mock<IDoubleClickListener> dblClickListener;
	private DiscoveryTreeViewerFactory treeViewerFactory;
	private Mock<IDiscoveryEnvironment> environment;
	private IGenericViewCustomization viewCustomization;
	private Shell testShell;

	@Override
	protected void setUp() throws Exception
	{
		dblClickListener = mock(IDoubleClickListener.class);
		testShell = new Shell(PlatformUI.getWorkbench().getDisplay());
		setupEnv();
		setupCustomization();
		treeViewerFactory = new DiscoveryTreeViewerFactory();
	}

	private void setupCustomization()
	{
		viewCustomization = new GenericViewCustomizationImpl()
		{
			@Override
			public IDoubleClickListener getDoubleClickListener()
			{
				return dblClickListener.proxy();
			}
		};
	}

	private void setupEnv()
	{
		environment = mock(IDiscoveryEnvironment.class);
		environment.stubs().method("operationRunner").will(returnValue(mock(ILongOperationRunner.class).proxy()));
		environment.stubs().method("errorHandler").will(returnValue(mock(IErrorHandler.class).proxy()));
	}

	public void testDoubleClickOnTree() throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException
	{
		final HashSet<IGenericViewCustomization> cust = new HashSet<IGenericViewCustomization>(Arrays.asList(new IGenericViewCustomization[] { viewCustomization }));
		final IResultsViewAccessor accessor = treeViewerFactory.createTreeViewer(testShell, cust, new HashSet<IContributedAction>(), environment.proxy());
		final ListenerList list = getDoubleClickListeners(accessor.getTreeViewer());
		assertTrue("Double click listener not registered", Arrays.asList(list.getListeners()).contains(dblClickListener.proxy()));
	}

	private ListenerList getDoubleClickListeners(final StructuredViewer viewer) throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
	{
		final Field f = StructuredViewer.class.getDeclaredField("doubleClickListeners");
		f.setAccessible(true);
		return (ListenerList) f.get(viewer);
	}
	
	public void testSupportedTransferDataSetters()
	{
		final List<Class<?>> setters = new ArrayList<Class<?>>();
		treeViewerFactory = new DiscoveryTreeViewerFactory(){
			@Override
			protected DragSrcInteractionListener.ITransferDataSetter<? extends Transfer>[] supportedTransferDataSetters(IDiscoveryEnvironment env)
			{
				DragSrcInteractionListener.ITransferDataSetter<? extends Transfer>[] superSetters = super.supportedTransferDataSetters(env);
				for(DragSrcInteractionListener.ITransferDataSetter<?> s : superSetters)
				{
					setters.add(s.getClass());
				}
				
				return superSetters;
			}
		};
		
		final Shell parentShell = new Shell(PlatformUI.getWorkbench().getDisplay());
		treeViewerFactory.createTreeViewer(parentShell, new HashSet<IGenericViewCustomization>(), new HashSet<IContributedAction>(), environment.proxy());
		assertEquals("2 setters expected", 2, setters.size());
		assertTrue("LocalSelectionTransferSetter not available", setters.contains(LocalSelectionTransferSetter.class));
		assertTrue("XmlDiscoverySelectionTransferSetter not available", setters.contains(XmlDiscoverySelectionTransferSetter.class));
	}
}
