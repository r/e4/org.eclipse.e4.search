/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal.dnd;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.platform.discovery.ui.internal.view.dnd.ISourceDndInteractionEvent;
import org.eclipse.swt.dnd.TransferData;

public class DndTestFixture
{
	public static class TestTransfer extends LocalSelectionTransfer
	{
		private final IRunnableWithResult<Boolean, TransferData> supportedTransferDatasRunnable;

		public TestTransfer(final IRunnableWithResult<Boolean, TransferData> supportedTransferDatasRunnable)
		{
			this.supportedTransferDatasRunnable = supportedTransferDatasRunnable;
		}
		
		@Override
		public boolean isSupportedType(TransferData transferData)
		{
			return this.supportedTransferDatasRunnable.run(transferData);
		}
	}
	
	public interface IRunnableWithResult<T, P>
	{
		public T run(P p);
	}
	
	public static class TestDNDInteractionEvent implements ISourceDndInteractionEvent
	{
		private boolean doit = false;
		private Object data = null;
		private Integer detail = 0;
		private TransferData dataType;
		
		@Override
		public boolean getDoIt()
		{
			return this.doit;
		}

		@Override
		public void setDoIt(boolean doit)
		{
			this.doit = doit;
		}

		@Override
		public Object getData()
		{
			return this.data;
		}

		@Override
		public void setData(Object data)
		{
			this.data = data;
		}

		@Override
		public TransferData getDataType()
		{
			return this.dataType;
		}

		public void setDataType(final TransferData dataType)
		{
			this.dataType = dataType;
		}
		
		@Override
		public Integer getInteractionDetail()
		{
			return this.detail;
		}

		@Override
		public void setInteractionDetail(Integer detail)
		{
			this.detail = detail;
		}
	}
}

