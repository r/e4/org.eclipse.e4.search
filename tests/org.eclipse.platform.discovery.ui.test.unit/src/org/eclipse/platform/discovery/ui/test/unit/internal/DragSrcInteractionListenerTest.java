/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.ui.internal.view.dnd.impl.DragSrcInteractionListener;
import org.eclipse.platform.discovery.ui.test.unit.internal.dnd.DndTestFixture.IRunnableWithResult;
import org.eclipse.platform.discovery.ui.test.unit.internal.dnd.DndTestFixture.TestDNDInteractionEvent;
import org.eclipse.platform.discovery.ui.test.unit.internal.dnd.DndTestFixture.TestTransfer;
import org.eclipse.platform.discovery.util.api.env.IErrorHandler;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;


public class DragSrcInteractionListenerTest extends MockObjectTestCase {
	
	private Mock<DragSrcInteractionListener.ISelectionObtainer> selectionObtainer;
	private IStructuredSelection selection;
	private DragSrcInteractionListener listener;

	private Object item1;
	private Object item2;
	private Object item3;

	private Transfer transfer;
	private Mock<DragSrcInteractionListener.ITransferDataSetter<Transfer>> transferSetter;
	private Mock<IRunnableWithResult<Boolean, TransferData>> supportedDatasRunnable;
	
	
	private TestDNDInteractionEvent dragSourceEvent;
	private Mock<IErrorHandler> errorHandler;

	@Override
	protected void setUp() throws Exception
	{
		selectionObtainer = mock(DragSrcInteractionListener.ISelectionObtainer.class);

		item1 = new Object();
		item2 = new Object();
		item3 = new Object();

		selection = new StructuredSelection(new Object[] { item1, item2, item3 });

		selectionObtainer.stubs().method("getSelection").will(returnValue(selection));

		supportedDatasRunnable = mock(IRunnableWithResult.class);
		transfer = new TestTransfer(supportedDatasRunnable.proxy());
		transferSetter = mock(DragSrcInteractionListener.ITransferDataSetter.class, "Transfer setter");
		
		transferSetter.stubs().method("getTransfer").will(returnValue(transfer));
		
		errorHandler = mock(IErrorHandler.class);
		
		@SuppressWarnings("unchecked")
		final DragSrcInteractionListener.ITransferDataSetter<Transfer>[] transfers = new DragSrcInteractionListener.ITransferDataSetter[]{transferSetter.proxy() };
		
		listener = new DragSrcInteractionListener(selectionObtainer.proxy(), transfers, errorHandler.proxy());
		dragSourceEvent = new TestDNDInteractionEvent();
	}
	
	public void testDragStart()
	{
		transferSetter.expects(once()).method("dragStarted").with(eq(selection), eq(dragSourceEvent));
		listener.start(dragSourceEvent);
		assertTrue("doit should be set to true", dragSourceEvent.getDoIt());
	}
	
	public void testDragStartWithEmptySelection()
	{
		final IStructuredSelection sel = new StructuredSelection();
		selectionObtainer.stubs().method("getSelection").will(returnValue(sel));
		listener.start(dragSourceEvent);
		assertFalse("doit should be set to false for empty selection", dragSourceEvent.getDoIt());
	}
	
	public void testDragSetDataWithSupportedType()
	{
		supportedDatasRunnable.stubs().method("run").withAnyArguments().will(returnValue(true));
		transferSetter.expects(once()).method("setData").with(eq(selection), eq(dragSourceEvent));
		listener.process(dragSourceEvent);
	}
	
	public void testDragSetDataWithUnsupportedType()
	{
		supportedDatasRunnable.stubs().method("run").withAnyArguments().will(returnValue(false));
		listener.process(dragSourceEvent);
	}
	
	public void testGetTransfers()
	{
		assertEquals("One transfer expected", 1, listener.getTransfers().length);
		assertTrue("One transfer expected", listener.getTransfers()[0] == transfer);
	}
}
