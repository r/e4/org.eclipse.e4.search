/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.platform.discovery.core.internal.selectors.IItemSelector;
import org.eclipse.platform.discovery.runtime.api.IDestinationChangeHandler;
import org.eclipse.platform.discovery.runtime.api.IDestinationsProvider;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.internal.ISearchProviderConfiguration;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationsProviderDescription;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.ui.internal.view.impl.SearchConsoleDestinationsSelector;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.FormToolkit;


public class SearchConsoleDestinationsSelectorTest extends MockObjectTestCase
{
	private SearchConsoleDestinationsSelector selector;
	private Mock<ISearchProviderConfiguration> searchProviderConfig;
	private Mock<IDestinationChangeHandler> destinationsChangeHandler;
	private FormToolkit formToolkit;
	private Composite parentComposite;
	private Mock<IItemSelector<ISearchDestination, IDestinationCategoryDescription>> selectionChangeHandlerChecker;
	private Mock<IDestinationCategoryDescription> firstDestinationCategory;
	private Mock<IDestinationsProviderDescription> firstDestProviderDescription;
	private Mock<IDestinationsProvider> firstDestinationsProvider;
	private Mock<IDestinationCategoryDescription> secondDestinationCategory;
	private Mock<IDestinationsProviderDescription> secondDestProviderDescription;
	private Mock<IDestinationsProvider> secondDestinationsProvider;
	private Mock<ISearchDestination> searchDestination;

	@Override
	protected void setUp() throws Exception
	{
		setupSearchProviderConfig();
		destinationsChangeHandler = mock(IDestinationChangeHandler.class);
		selectionChangeHandlerChecker = mock(IItemSelector.class);
		formToolkit = new FormToolkit(PlatformUI.getWorkbench().getDisplay());
		parentComposite = formToolkit.createComposite(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());
		
		
		selector = new SearchConsoleDestinationsSelector(parentComposite, formToolkit, 10, new Composite(parentComposite, SWT.NONE), searchProviderConfig.proxy())
		{
			@Override
			public void handleSelectionChange(ISearchDestination newSelection)
			{
				selectionChangeHandlerChecker.proxy().handleSelectionChange(newSelection);
			}
		};
		selector.registerDestinationsChangeHandler(destinationsChangeHandler.proxy());
		
		super.setUp();
	}

	private void setupSearchProviderConfig()
	{
		searchProviderConfig = mock(ISearchProviderConfiguration.class);
		firstDestinationCategory = mock(IDestinationCategoryDescription.class);
		firstDestinationCategory.stubs().method("getDisplayName").will(returnValue("FirstDestinationCategory"));
		secondDestinationCategory = mock(IDestinationCategoryDescription.class);
		secondDestinationCategory.stubs().method("getDisplayName").will(returnValue("FirstDestinationCategory"));
		
		firstDestProviderDescription = mock(IDestinationsProviderDescription.class);
		secondDestProviderDescription = mock(IDestinationsProviderDescription.class);
		firstDestinationsProvider = mock(IDestinationsProvider.class);
		secondDestinationsProvider = mock(IDestinationsProvider.class);
		searchDestination = mock(ISearchDestination.class);
		searchDestination.stubs().method("getDisplayName").will(returnValue("SearchDestination"));
		
		firstDestProviderDescription.stubs().method("createProvider").will(returnValue(firstDestinationsProvider.proxy()));
		secondDestProviderDescription.stubs().method("createProvider").will(returnValue(secondDestinationsProvider.proxy()));
		
		final List<IDestinationsProviderDescription> allDestProviders = new ArrayList<IDestinationsProviderDescription>();
		allDestProviders.add(firstDestProviderDescription.proxy());
		searchProviderConfig.stubs().method("getDestinationProvidersForCategory").with(eq(firstDestinationCategory.proxy())).will(returnValue(allDestProviders));
		
		final List<IDestinationsProviderDescription> allDestProviders_2 = new ArrayList<IDestinationsProviderDescription>();
		allDestProviders_2.add(secondDestProviderDescription.proxy());
		searchProviderConfig.stubs().method("getDestinationProvidersForCategory").with(eq(secondDestinationCategory.proxy())).will(returnValue(allDestProviders_2));
		
		final List<ISearchDestination> allDestinations = new ArrayList<ISearchDestination>();
		allDestinations.add(searchDestination.proxy());
		searchProviderConfig.stubs().method("getSearchDestinations").with(eq(firstDestinationCategory.proxy()), eq(firstDestinationsProvider.proxy())).will(returnValue(allDestinations));
		searchProviderConfig.stubs().method("getSearchDestinations").with(eq(secondDestinationCategory.proxy()), eq(secondDestinationsProvider.proxy())).will(returnValue(allDestinations));
	}
	
	public void testSetInputRegistersChangeHandler()
	{
		firstDestinationsProvider.expects(once()).method("registerDestinationsChangeHandler").with(same(destinationsChangeHandler.proxy()));
		selectionChangeHandlerChecker.stubs().method("handleSelectionChange");
		selector.setInput(Arrays.asList(new IDestinationCategoryDescription[]{firstDestinationCategory.proxy()}));
	}
	
	public void testChangingInputUnregistersChangeHandler()
	{
		testSetInputRegistersChangeHandler();
		firstDestinationsProvider.verify();
		
		firstDestinationsProvider.reset();
		firstDestinationsProvider.expects(once()).method("unregisterDestinationsChangeHandler").with(same(destinationsChangeHandler.proxy()));
		secondDestinationsProvider.expects(once()).method("registerDestinationsChangeHandler").with(same(destinationsChangeHandler.proxy()));
		
		selectionChangeHandlerChecker.expects(once()).method("handleSelectionChange");
		selector.setInput(Arrays.asList(new IDestinationCategoryDescription[]{secondDestinationCategory.proxy()}));
	}
	
	public void testDisposeUnregistersListeners()
	{
		testSetInputRegistersChangeHandler();
		firstDestinationsProvider.verify();
		
		firstDestinationsProvider.reset();
		firstDestinationsProvider.expects(once()).method("unregisterDestinationsChangeHandler").with(same(destinationsChangeHandler.proxy()));

		parentComposite.dispose();
	}

	@Override
	protected void tearDown() throws Exception
	{
		super.tearDown();
		firstDestinationsProvider.reset();
		secondDestinationsProvider.reset();
		// Stub the unregister method because it is called on parent composite dispose
		firstDestinationsProvider.stubs().method("unregisterDestinationsChangeHandler");
		secondDestinationsProvider.stubs().method("unregisterDestinationsChangeHandler");
		formToolkit.dispose();
	}
}
