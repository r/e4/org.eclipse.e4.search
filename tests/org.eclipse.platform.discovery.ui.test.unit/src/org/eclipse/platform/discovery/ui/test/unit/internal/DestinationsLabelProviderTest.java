/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.ui.internal.view.impl.DestinationsLabelProvider;


public class DestinationsLabelProviderTest extends MockObjectTestCase
{
	private DestinationsLabelProvider testLabelProvider;
	
	@Override
	protected void setUp() throws Exception
	{
		testLabelProvider = new DestinationsLabelProvider();
	}
	
	public void testGetImageReturnsNull()
	{
		assertNull("Null is expected for image", testLabelProvider.getImage(null));
	}
	
	public void testGetTextForDestination()
	{
		final Mock<ISearchDestination> destination = mock(ISearchDestination.class);
		destination.stubs().method("getDisplayName").will(returnValue("TestDestination"));
		assertEquals("Unexpected text", "TestDestination", testLabelProvider.getText(destination.proxy()));
	}
	
	public void testGetTextForDestinationCategory()
	{
		final Mock<IDestinationCategoryDescription> category = mock(IDestinationCategoryDescription.class);
		category.stubs().method("getDisplayName").will(returnValue("TestCategory"));
		assertEquals("Unexpected text", "TestCategory", testLabelProvider.getText(category.proxy()));
	}
}
