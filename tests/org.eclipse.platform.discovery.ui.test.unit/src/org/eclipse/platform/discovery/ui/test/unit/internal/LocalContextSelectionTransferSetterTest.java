/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.platform.discovery.core.internal.IContextStructuredSelection;
import org.eclipse.platform.discovery.ui.internal.dnd.LocalContextSelectionTransfer;
import org.eclipse.platform.discovery.ui.internal.view.dnd.impl.DragSrcInteractionListener;
import org.eclipse.platform.discovery.ui.internal.view.dnd.impl.LocalContextSelectionTransferSetter;
import org.eclipse.swt.dnd.Transfer;


public class LocalContextSelectionTransferSetterTest extends LocalSelectionTransferSetterFixture<LocalContextSelectionTransferSetter>
{
	@Override
	protected void setUp() throws Exception
	{
		super.setUp();
	}
	
	@Override
	protected DragSrcInteractionListener.ITransferDataSetter<? extends Transfer> createTransferSetter()
	{
		return new LocalContextSelectionTransferSetter(){
			@Override
			public LocalContextSelectionTransfer getTransfer()
			{
				return LocalContextSelectionTransfer.getTransfer();
			}
		};
	}

	@Override
	protected IStructuredSelection createSelection()
	{
		return (IContextStructuredSelection)mock(IContextStructuredSelection.class).proxy();
	}

	@Override
	protected IStructuredSelection getTransferSelection()
	{
		return LocalContextSelectionTransfer.getTransfer().getSelection();
	}
}
