/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import org.eclipse.platform.discovery.core.api.ISearchFavoritesMasterController;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.ui.internal.view.impl.FavoritesDropInteractionListener;
import org.eclipse.platform.discovery.ui.test.unit.internal.dnd.DndTestFixture;
import org.eclipse.platform.discovery.ui.test.unit.internal.dnd.DndTestFixture.IRunnableWithResult;
import org.eclipse.platform.discovery.ui.test.unit.internal.dnd.DndTestFixture.TestDNDInteractionEvent;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;


public class FavoritesDropInteractionListenerTest extends MockObjectTestCase
{
	private FavoritesDropInteractionListener dropListener;
	private TestDNDInteractionEvent dropEvent;
	private TransferData transferData;
	private Transfer transfer1;
	private Mock<IRunnableWithResult<Boolean, TransferData>> transfer1SupportedDataTypeRunnable;
	private Transfer transfer2;
	private Mock<IRunnableWithResult<Boolean, TransferData>> transfer2SupportedDataTypeRunnable;
	private Mock<ISearchFavoritesMasterController> controller;
	
	@Override
	protected void setUp() throws Exception
	{
		transferData = new TransferData();
		
		transfer1SupportedDataTypeRunnable = mock(IRunnableWithResult.class);
		transfer1 = new DndTestFixture.TestTransfer(transfer1SupportedDataTypeRunnable.proxy());
		
		transfer2SupportedDataTypeRunnable = mock(IRunnableWithResult.class);
		transfer2SupportedDataTypeRunnable.stubs().method("run").with(eq(transferData)).will(returnValue(false));
		transfer2 = new DndTestFixture.TestTransfer(transfer2SupportedDataTypeRunnable.proxy());
		controller = mock(ISearchFavoritesMasterController.class);
		
		dropListener = new FavoritesDropInteractionListener(new Transfer[]{transfer1, transfer2}, controller.proxy());

		dropEvent  = new TestDNDInteractionEvent();
		dropEvent.setDataType(transferData);
	}

	public void testDragEnterWithUnsupportedDataType()
	{
		transfer1SupportedDataTypeRunnable.stubs().method("run").with(eq(transferData)).will(returnValue(false));
		
		dropListener.start(dropEvent);
		assertTrue("DND.DROP_NONE not set to event datail", dropEvent.getInteractionDetail() == DND.DROP_NONE);
	}


	public void testDropFiresEvent()
	{
		controller.expects(once()).method("importData").with(same(dropEvent.getData()));
		
		dropListener.process(dropEvent);
	}
}
