/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.ui.api.IGenericViewCustomization;
import org.eclipse.platform.discovery.ui.api.ISearchFavoritesViewCustomization;
import org.eclipse.platform.discovery.ui.api.impl.GenericViewCustomizationImpl;
import org.eclipse.platform.discovery.ui.internal.view.favorites.FavoritesContentProvider;
import org.eclipse.platform.discovery.util.internal.logging.ILogger;


public class FavoritesContentProviderTest extends MockObjectTestCase
{
	private FavoritesContentProvider testedProvider;
	private ISearchFavoritesViewCustomization viewCust;
	private Mock<ITreeContentProvider> contentProvider;
	private Mock<ILogger> logger;
	private Object favoritesGroup_1;
	private Object favoritesGroup_2;
	private Object favoritesItem_1;
	private Object favoritesItem_2;
	private Object favoritesItem_3;
	
	private Object favoritesItem_1_Child;
	private Object favoritesItem_2_Child;
	private Object favoritesItem_3_Child;

	@Override
	protected void setUp() throws Exception
	{
		favoritesGroup_1 = new Object();
		favoritesGroup_2 = new Object();
		favoritesItem_1 = new Object();
		favoritesItem_2 = new Object();
		favoritesItem_3 = new Object();
		favoritesItem_1_Child = new Object();
		favoritesItem_2_Child = new Object();
		favoritesItem_3_Child = new Object();

		contentProvider = mock(ITreeContentProvider.class);
		contentProvider.expects(never()).method("inputChanged");
		logger = mock(ILogger.class);
		
		setupViewCustomization();

		testedProvider = new FavoritesContentProvider(Arrays.asList(new IGenericViewCustomization[] { viewCust }))
		{
			@Override
			protected ILogger logger()
			{
				return logger.proxy();
			}
		};
	}

	private void setupViewCustomization()
	{
		viewCust = new TestCustomization();
	}

	public void testSetInputAndGetElements()
	{
		testedProvider.inputChanged(null, null, new HashSet<Object>(Arrays.asList(new Object[] { favoritesItem_1, favoritesItem_2, favoritesItem_3 })));

		// Expect the two groups
		final Set<Object> groups = new HashSet<Object>(Arrays.asList(testedProvider.getElements(null)));
		assertEquals("Two elements expected", 2, groups.size());
		assertTrue("Group1 not found", groups.contains(favoritesGroup_1));
		assertTrue("Group2 not found", groups.contains(favoritesGroup_2));
	}

	public void testSetNullInput()
	{
		testSetInputAndGetElements();
		testedProvider.inputChanged(null, null, null);
		assertEquals("No elements expected", 0, testedProvider.getElements(null).length);
	}

	public void testGetChildren()
	{
		contentProvider.expects(once()).method("getChildren").with(eq(favoritesItem_1)).will(returnValue(new Object[] { favoritesItem_1_Child }));
		contentProvider.expects(once()).method("getChildren").with(eq(favoritesItem_2)).will(returnValue(new Object[] { favoritesItem_2_Child }));
		contentProvider.expects(once()).method("getChildren").with(eq(favoritesItem_3)).will(returnValue(new Object[] { favoritesItem_3_Child }));

		testedProvider.inputChanged(null, null, new HashSet<Object>(Arrays.asList(new Object[] { favoritesItem_1, favoritesItem_2, favoritesItem_3 })));
		
		Set<Object> children = new HashSet<Object>(Arrays.asList(testedProvider.getChildren(favoritesGroup_1)));
		assertEquals("Two children expected", 2, children.size());
		assertTrue("Item1 not found", children.contains(favoritesItem_1));
		assertTrue("Item2 not found", children.contains(favoritesItem_2));

		children = new HashSet<Object>(Arrays.asList(testedProvider.getChildren(favoritesGroup_2)));
		assertEquals("One childr expected", 1, children.size());
		assertTrue("Item2 not found", children.contains(favoritesItem_3));
		
		children = new HashSet<Object>(Arrays.asList(testedProvider.getChildren(favoritesItem_1)));
		assertEquals("One child expected", 1, children.size());
		assertTrue("Unexpected favorites1 child", children.iterator().next() == favoritesItem_1_Child);
		children = new HashSet<Object>(Arrays.asList(testedProvider.getChildren(favoritesItem_2)));
		assertEquals("One child expected", 1, children.size());
		assertTrue("Unexpected favorites2 child", children.iterator().next() == favoritesItem_2_Child);
		children = new HashSet<Object>(Arrays.asList(testedProvider.getChildren(favoritesItem_3)));
		assertEquals("One child expected", 1, children.size());
		assertTrue("Unexpected favorites3 child", children.iterator().next() == favoritesItem_3_Child);
	}
	
	public void testInputChange()
	{
		testGetChildren();
		contentProvider.reset();
		
		contentProvider.expects(once()).method("getChildren").with(eq(favoritesItem_1)).will(returnValue(new Object[] { favoritesItem_1_Child }));
		testedProvider.inputChanged(null, null, new HashSet<Object>(Arrays.asList(new Object[]{favoritesItem_1, favoritesItem_2})));
		
		// Expect one group
		final Set<Object> groups = new HashSet<Object>(Arrays.asList(testedProvider.getElements(null)));
		assertEquals("One elements expected", 1, groups.size());
		assertTrue("Group1 not found", groups.contains(favoritesGroup_1));
		
		Set<Object> children = new HashSet<Object>(Arrays.asList(testedProvider.getChildren(favoritesGroup_1)));
		assertEquals("Two children expected", 2, children.size());
		assertTrue("Item1 not found", children.contains(favoritesItem_1));
		assertTrue("Item2 not found", children.contains(favoritesItem_2));
		
		children = new HashSet<Object>(Arrays.asList(testedProvider.getChildren(favoritesItem_1)));
		assertEquals("One child expected", 1, children.size());
		assertTrue("Unexpected favorites1 child", children.iterator().next() == favoritesItem_1_Child);
	}
	
	public void testHasChildren()
	{
		contentProvider.expects(once()).method("hasChildren").with(eq(favoritesItem_1)).will(returnValue(true));
		contentProvider.expects(once()).method("hasChildren").with(eq(favoritesItem_2)).will(returnValue(true));
		testedProvider.inputChanged(null, null, new HashSet<Object>(Arrays.asList(new Object[] { favoritesItem_1, favoritesItem_2, favoritesItem_3 })));
		assertTrue("Children expected for group1", testedProvider.hasChildren(favoritesGroup_1));
		assertTrue("Children expected for group1", testedProvider.hasChildren(favoritesGroup_2));
		assertTrue("Children expected for item1", testedProvider.hasChildren(favoritesItem_1));
		assertTrue("Children not expected for item2", testedProvider.hasChildren(favoritesItem_2));
	}

	public void testWithFavoriteItemWithoutGroup()
	{
		viewCust = new TestCustomization(){
			@Override
			public Object itemGroup(Object arg0)
			{
				if (arg0 == favoritesItem_1)
				{
					return null;
				}

				throw new IllegalArgumentException("Unexpected favorites item");
			}
		};
		testedProvider = new FavoritesContentProvider(Arrays.asList(new IGenericViewCustomization[] { viewCust }))
		{
			@Override
			protected ILogger logger()
			{
				return logger.proxy();
			}
		};
		
		logger.expects(once()).method("logWarn");
		testedProvider.inputChanged(null, null, new HashSet<Object>(Arrays.asList(new Object[]{favoritesItem_1})));
		assertEquals("No element expected", 0, testedProvider.getElements(null).length);
	}
	
	private class TestCustomization extends GenericViewCustomizationImpl implements ISearchFavoritesViewCustomization
	{
		@Override
		public Object itemGroup(Object item)
		{
			if (item == favoritesItem_1 || item == favoritesItem_2)
			{
				return favoritesGroup_1;
			}
			if (item == favoritesItem_3)
			{
				return favoritesGroup_2;
			}

			throw new IllegalArgumentException("Unexpected favorites item");
		}

		@Override
		public Set<Object> itemsFor(Object itemsSource)
		{
			return new HashSet<Object>(Arrays.asList(new Object[] { itemsSource }));
		}

		@Override
		public ITreeContentProvider getContentProvider()
		{
			return contentProvider.proxy();
		}
	}

}
