/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.easymock.EasyMock;
import org.easymock.IArgumentMatcher;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.platform.discovery.ui.api.IGenericViewCustomization;
import org.eclipse.platform.discovery.ui.internal.view.impl.SearchConsoleSelectionProvider;
import org.eclipse.platform.discovery.ui.internal.view.impl.SearchConsoleSelectionProvider.IViewCustomizationsObtainer;


public class SearchConsoleSelectionProviderTest extends TestCase
{
	private ISelectionChangedListener selectionListener;
	private SearchConsoleSelectionProvider selProvider;
	private ISelection selection;
	private IViewCustomizationsObtainer custObtainer;
	private IGenericViewCustomization viewCustomization;
	private List<IGenericViewCustomization> allCustomizations;

	@Override
	protected void setUp() throws Exception
	{
		selectionListener = EasyMock.createMock(ISelectionChangedListener.class);
		viewCustomization = EasyMock.createMock(IGenericViewCustomization.class);
		custObtainer = EasyMock.createMock(IViewCustomizationsObtainer.class);
		allCustomizations = new ArrayList<IGenericViewCustomization>();
		EasyMock.expect(custObtainer.viewCustomizations()).andStubReturn(allCustomizations);
		EasyMock.replay(custObtainer);
		selProvider = new SearchConsoleSelectionProvider(custObtainer);
		selection = EasyMock.createMock(ISelection.class);
	}

	public void testRegisterSelListener()
	{
		selectionListener.selectionChanged(selChangedEventExpectation(selection));
		EasyMock.expectLastCall();
		EasyMock.replay(selectionListener);
		
		selProvider.addSelectionChangedListener(selectionListener);
		selProvider.setSelection(selection);
		
		EasyMock.verify(selectionListener);
	}
	
	public SelectionChangedEvent selChangedEventExpectation(final ISelection expectedSelection)
	{
		EasyMock.reportMatcher(new IArgumentMatcher()
		{
			@Override
			public void appendTo(StringBuffer buffer)
			{
			}

			@Override
			public boolean matches(Object argument)
			{
				final SelectionChangedEvent event = (SelectionChangedEvent) argument;
				return event.getSelection().equals(expectedSelection);
			}
		});
		return null;
	}

	public void testUnregisterSelListener()
	{
		EasyMock.replay(selectionListener);
		selProvider.addSelectionChangedListener(selectionListener);
		selProvider.removeSelectionChangedListener(selectionListener);
		selProvider.setSelection(selection);
		EasyMock.verify(selectionListener);
	}

	public void testMultiThreadAccess() throws InterruptedException
	{
		final boolean[] continueExecution_1 = new boolean[] { false };
		final boolean[] listenerIsWorking_1 = new boolean[] { false };
		final boolean[] continueExecution_2 = new boolean[] { false };
		final boolean[] listenerIsWorking_2 = new boolean[] { false };
		final boolean[] modificationStarted = new boolean[] { false };

		final ISelectionChangedListener listener_1 = blockingListner(continueExecution_1, listenerIsWorking_1);
		selProvider.addSelectionChangedListener(listener_1);
		final ISelectionChangedListener listener_2 = blockingListner(continueExecution_2, listenerIsWorking_2);
		selProvider.addSelectionChangedListener(listener_2);

		final Thread notifyingThread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				selProvider.setSelection(new StructuredSelection());
			}
		});
		notifyingThread.setName("Listener notifying thread");
		notifyingThread.setDaemon(true);

		final Thread modifyingThread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				while (!listenerIsWorking_1[0] && !listenerIsWorking_2[0])
				{
					try
					{
						Thread.sleep(10);
					} catch (InterruptedException e)
					{
						throw new RuntimeException(e);
					}
				}

				modificationStarted[0] = true;
				selProvider.removeSelectionChangedListener(listener_1);
				selProvider.removeSelectionChangedListener(listener_2);
			}
		});
		modifyingThread.setName("Listener modifying thread");
		modifyingThread.setDaemon(true);

		final String[] failMsg = new String[1];
		final UncaughtExceptionHandler excHandler = new UncaughtExceptionHandler()
		{
			@Override
			public void uncaughtException(Thread t, Throwable e)
			{
				failMsg[0] = "Thread " + t.getName() + " threw uncaught exception: " + e.getMessage();
			}
		};
		notifyingThread.setUncaughtExceptionHandler(excHandler);
		modifyingThread.setUncaughtExceptionHandler(excHandler);

		notifyingThread.start();
		modifyingThread.start();

		while (!modificationStarted[0])
		{
			Thread.sleep(50);
		}

		continueExecution_1[0] = true;
		continueExecution_2[0] = true;

		modifyingThread.join();
		notifyingThread.join();

		if (failMsg[0] != null)
		{
			fail(failMsg[0]);
		}
	}
	
	public void testSetSelectionNotifiesCustomizations()
	{
		viewCustomization.selectionChanged(EasyMock.eq(selection));
		EasyMock.expectLastCall();
		EasyMock.replay(viewCustomization);
		
		allCustomizations.add(viewCustomization);
		selProvider.setSelection(selection);
		
		EasyMock.verify(viewCustomization);
	}

	private ISelectionChangedListener blockingListner(final boolean[] continueExecutionFlag, final boolean[] listenerIsWorkingFlag)
	{
		return new ISelectionChangedListener()
		{
			@Override
			public void selectionChanged(SelectionChangedEvent event)
			{
				listenerIsWorkingFlag[0] = true;
				while (!continueExecutionFlag[0])
				{
					try
					{
						Thread.sleep(50);
					} catch (InterruptedException e)
					{
						throw new RuntimeException(e);
					}
				}
			}
		};
	}
}
