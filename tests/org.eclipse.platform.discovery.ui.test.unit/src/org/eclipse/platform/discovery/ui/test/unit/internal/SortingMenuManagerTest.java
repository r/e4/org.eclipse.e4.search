/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import org.eclipse.jface.action.ContributionItem;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.ui.internal.view.SortingMenuManager;


public class SortingMenuManagerTest extends MockObjectTestCase {

	
	public void testItemsSorted() {
		final IContributionItem item1 = new TestContributionItem("id1");
		final IContributionItem item2 = new TestContributionItem("id2");
		final IContributionItem item3 = new TestContributionItem("id3");
		
		final IMenuManager target = new SortingMenuManager();
		target.add(item3);
		target.add(item2);
		target.add(item1);
		assertEquals(item1.getId(), target.getItems()[0].getId());
		assertEquals(item2.getId(), target.getItems()[1].getId());
		assertEquals(item3.getId(),	target.getItems()[2].getId());
		
	}
	
	public void testItemsSortedWithNullId() {
		final IContributionItem item1 = new TestContributionItem("id1");
		final IContributionItem item2 = new TestContributionItem(null);
		final IContributionItem item3 = new TestContributionItem("id3");
		
		
		final IMenuManager target = new SortingMenuManager();
		target.add(item3);
		target.add(item2);
		target.add(item1);
		assertEquals(item2.getId(), target.getItems()[0].getId());
		assertEquals(item1.getId(), target.getItems()[1].getId());
		assertEquals(item3.getId(),	target.getItems()[2].getId());
	}
	
	private class TestContributionItem extends ContributionItem {

		public TestContributionItem(String id) {
			super(id);
		}
	}
}
