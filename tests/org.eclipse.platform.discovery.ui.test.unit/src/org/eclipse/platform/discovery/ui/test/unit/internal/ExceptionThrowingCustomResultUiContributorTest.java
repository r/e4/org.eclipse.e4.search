/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import java.util.List;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.AbstractExtensionPointParser;
import org.eclipse.platform.discovery.testutils.utils.registry.ExtensionRegistryBuilder;
import org.eclipse.platform.discovery.testutils.utils.testcases.AbstractExtensionPointParserTest;
import org.eclipse.platform.discovery.ui.internal.selector.ICustomResultUiContributorDescription;
import org.eclipse.platform.discovery.ui.internal.xp.impl.CustomResultUiXpParser;
import static org.junit.Assert.*;

public class ExceptionThrowingCustomResultUiContributorTest extends AbstractExtensionPointParserTest<ICustomResultUiContributorDescription>{

	@Override
	protected AbstractExtensionPointParser<ICustomResultUiContributorDescription> createParser(IExtensionRegistry registry) {
		return new CustomResultUiXpParser(registry);
	}

	@Override
	protected void setupRegistry(ExtensionRegistryBuilder registryBuilder) {
		registryBuilder.addExceptionThrowingCustomResultUiContributor("asd", "foo", "bar");
	}

	@Override
	protected void verifyContributions(List<ICustomResultUiContributorDescription> contributions) {
		ICustomResultUiContributorDescription theContributorDescription = contributions.get(0);
		try{
			theContributorDescription.createUiCreator();
			fail("illegalstate not thrown");
		}catch(IllegalStateException ex) {
			//ok
		}
	}

}
