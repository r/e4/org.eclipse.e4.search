/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.ui.internal.view.impl.SearchForSelectionChangeListener;


public class SearchForSelectionChangeListenerTest extends MockObjectTestCase
{
	private SearchForSelectionChangeListener listener;
	private IObjectTypeDescription handledObject;
	
	public void testSelectionChanged()
	{
		final Mock<IObjectTypeDescription> objectMock = mock(IObjectTypeDescription.class);
		
		listener = new SearchForSelectionChangeListener(){
			@Override
			protected void objectTypeSelected(IObjectTypeDescription objectType)
			{
				handledObject = objectType;
			}};
		
		final Mock<IStructuredSelection> selectionMock = mock(IStructuredSelection.class);
		selectionMock.stubs().method("getFirstElement").will(returnValue(objectMock.proxy()));
		
		final Mock<ISelectionProvider> selectionProvider = mock(ISelectionProvider.class);
		final SelectionChangedEvent eventMock = new SelectionChangedEvent(selectionProvider.proxy(), selectionMock.proxy());
		listener.selectionChanged(eventMock);
		assertSame("Unexpected object handled", objectMock.proxy(), handledObject);
	}
}
