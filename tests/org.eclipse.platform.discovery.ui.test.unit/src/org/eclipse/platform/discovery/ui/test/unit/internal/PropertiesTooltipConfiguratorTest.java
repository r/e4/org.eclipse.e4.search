/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.platform.discovery.ui.internal.tooltip.IToolTipConfigurator;
import org.eclipse.platform.discovery.ui.internal.tooltip.PropertiesTooltipConfigurator;


public class PropertiesTooltipConfiguratorTest extends AbstractTooltipConfiguratorTest
{
	private static final String PROPERTY_NAME = "PropName";
	private static final String PROPERTY_VALUE = "PropValue";
	private Map<String, String> properties = new HashMap<String, String>();
	
	@Override
	protected void setUp() throws Exception
	{
		properties = new HashMap<String, String>();
		properties.put(PROPERTY_NAME, PROPERTY_VALUE);
		super.setUp();
	}
	
	@Override
	protected IToolTipConfigurator createConfigurator()
	{
		return new PropertiesTooltipConfigurator(){
			@Override
			protected TooltipFormTextBuilder createNewTooltipTextBuilder()
			{
				return createFormTextBuilderMock(textBuilder.proxy());
			}

			@Override
			protected Map<String, String> tooltipProperties()
			{
				return properties;
			}
		};
	}
	
	@Override
	public void testGetFormText()
	{
		textBuilder.expects(once()).method("appendProperty").with(eq(PROPERTY_NAME), eq(PROPERTY_VALUE));
		super.testGetFormText();
	}
}
