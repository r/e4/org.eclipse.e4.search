/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.ui.api.ISearchFavoritesViewCustomization;
import org.eclipse.platform.discovery.ui.internal.view.SearchFavoritesItemsAdder;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.jmock.core.Invocation;
import org.jmock.core.Stub;


public class SearchFavoritesItemsAdderTest extends MockObjectTestCase
{
	private SearchFavoritesItemsAdder adder;
	private Mock<ISearchFavoritesViewCustomization> viewCustomization;
	private Mock<ILongOperationRunner> opRunner;
	private boolean itemsAreAdded;
	private Set<Object> expectedItemsToAdd;
	private Mock<IStructuredSelection> structuredSelection;

	@Override
	protected void setUp() throws Exception
	{
		viewCustomization = mock(ISearchFavoritesViewCustomization.class);
		opRunner = mock(ILongOperationRunner.class);
		itemsAreAdded = false;
		expectedItemsToAdd = new HashSet<Object>();

		structuredSelection = mock(IStructuredSelection.class);

		final List<ISearchFavoritesViewCustomization> customizations = new ArrayList<ISearchFavoritesViewCustomization>();
		customizations.add(viewCustomization.proxy());

		adder = new SearchFavoritesItemsAdder(customizations, opRunner.proxy())
		{
			@Override
			protected void doAddItems(Set<Object> items, ILongOperationRunner operationRunner)
			{
				assertTrue("Unexpected opRunner", operationRunner == opRunner.proxy());
				assertEquals("Unexpected items", expectedItemsToAdd, items);
				itemsAreAdded = true;
			}
		};
	}

	public void testCanAddSelection_NonStructuredSelection()
	{
		assertFalse(adder.canAddSelection((ISelection)mock(ISelection.class).proxy()));
	}

	public void testCanAddSelection_UnsupportedSelection()
	{
		final Object sourceItem = new Object();
		viewCustomization.stubs().method("itemsFor").with(eq(sourceItem)).will(returnValue(new HashSet<Object>()));
		setObjectsToSelection(new HashSet<Object>(Arrays.asList(new Object[] { sourceItem })));

		assertFalse(adder.canAddSelection(structuredSelection.proxy()));
	}

	public void testCanAddSelection_SupportedSelection()
	{
		final Object sourceItem = new Object();
		final Object bItem = new Object();
		viewCustomization.stubs().method("itemsFor").with(eq(sourceItem)).will(returnValue(new HashSet<Object>(Arrays.asList(new Object[] { bItem }))));
		setObjectsToSelection(new HashSet<Object>(Arrays.asList(new Object[] { sourceItem })));

		assertTrue(adder.canAddSelection(structuredSelection.proxy()));
	}

	public void testAddItems()
	{
		final Object sourceItem = "sourceItem";
		final Object bItem = "bItem";

		viewCustomization.stubs().method("itemsFor").with(eq(sourceItem)).will(returnValue(new HashSet<Object>(Arrays.asList(new Object[] { bItem }))));
		setObjectsToSelection(new HashSet<Object>(Arrays.asList(new Object[] { sourceItem })));

		expectedItemsToAdd.add(bItem);

		adder.addItems(structuredSelection.proxy());
		assertTrue("Items are not added", itemsAreAdded);
	}
	
	public void testAddItemsWithUnknownItems()
	{
		final Object sourceItem = "sourceItem";

		viewCustomization.stubs().method("itemsFor").with(eq(sourceItem)).will(returnValue(new HashSet<Object>()));
		setObjectsToSelection(new HashSet<Object>(Arrays.asList(new Object[] { sourceItem })));

		try
		{
			adder.addItems(structuredSelection.proxy());
			fail("IllegalArgumentException expected");
		}
		catch(IllegalArgumentException e)
		{
			//expected
		}
		assertFalse("Items are not added", itemsAreAdded);
	}

	private void setObjectsToSelection(final Set<Object> objects)
	{
		structuredSelection.stubs().method("iterator").will(new Stub()
		{
			@Override
			public Object invoke(Invocation arg0) throws Throwable
			{
				return objects.iterator();
			}

			@Override
			public StringBuffer describeTo(StringBuffer arg0)
			{
				return arg0;
			}
		});
	}
}
