/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import junit.framework.TestCase;

import org.easymock.EasyMock;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.ui.api.IResultsViewAccessor;
import org.eclipse.platform.discovery.ui.internal.view.EnablePropertiesForAllViewCustomization;
import org.eclipse.platform.discovery.ui.internal.view.impl.OpenPropsViewAction;


public class EnablePropertiesForAllViewCustomizationTest extends
		TestCase {
	
	private IContributedAction action;
	private IResultsViewAccessor accessor;
	private ISelection selection;
	
	private EnablePropertiesForAllViewCustomization custUnderTest;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		selection = EasyMock.createMock(ISelection.class);
		custUnderTest = new EnablePropertiesForAllViewCustomization()
		{
			@Override
			protected ISelection getSelection(final IResultsViewAccessor accessor)
			{
				return selection;
			}
		};
		accessor = EasyMock.createMock(IResultsViewAccessor.class);
	}
	
	/**
	 * The customization shall accept all search providers, so that the properties view can be opened on any object
	 */
	public void testAcceptSearchProvider() {
		for(String str: new String[]{"asd","anything",null}) {
			assertTrue(custUnderTest.acceptSearchProvider(str));
		}
	}
	
	public void testInstallAction() {
		EasyMock.expect(selection.isEmpty()).andStubReturn(false);

		IMenuManager mgr = EasyMock.createNiceMock(IMenuManager.class); 
		EasyMock.expect(accessor.getMenuManager()).andStubReturn(mgr);
		EasyMock.expect(mgr.find(OpenPropsViewAction.ID)).andReturn(null);
		mgr.add(EasyMock.isA(OpenPropsViewAction.class));
		EasyMock.expectLastCall();
		EasyMock.replay(accessor, selection, mgr);

		custUnderTest.installAction(action, accessor); 
		EasyMock.reset(mgr);
		EasyMock.expect(mgr.find(OpenPropsViewAction.ID)).andReturn(EasyMock.createMock(IContributionItem.class));
		EasyMock.replay(mgr);
		custUnderTest.installAction(action, accessor);
//		accessor.getMenuManager() should not be called
	}
	
	public void testInstallActionOnEmptySelection()
	{
		EasyMock.expect(selection.isEmpty()).andReturn(true);
		EasyMock.replay(accessor, selection);
		
		custUnderTest.installAction(action, accessor); 
//		accessor.getMenuManager() should not be called
	}

}
