/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.platform.discovery.runtime.internal.ISearchProviderConfiguration;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.ui.internal.view.impl.DisplayableObjectsContentProvider;


public class SearchForListContentProviderTest extends MockObjectTestCase
{
	private static final String OBJECT_TYPE1 = "objectType1";
	private static final String OBJECT_TYPE2 = "objectType2";
	private static final String OBJECT_TYPE3 = "objectType3";
	
	private DisplayableObjectsContentProvider<IObjectTypeDescription> contentProvider;
	private Mock<ISearchProviderConfiguration> searchProviderConfig;
	
	@Override
	protected void setUp() throws Exception
	{
		searchProviderConfig = mock(ISearchProviderConfiguration.class);
		final List<IObjectTypeDescription> objects = new ArrayList<IObjectTypeDescription>();
		objects.add(createObjectTypeWithId(OBJECT_TYPE1));
		objects.add(createObjectTypeWithId(OBJECT_TYPE2));
		objects.add(createObjectTypeWithId(OBJECT_TYPE3));
		searchProviderConfig.stubs().method("getObjectTypes").will(returnValue(objects));
		
		contentProvider = new DisplayableObjectsContentProvider<IObjectTypeDescription>(objects);
		contentProvider.inputChanged(null, null, objects);
	}
	
	public void testGetElements()
	{
		final Set<String> expectedIds = new HashSet<String>(Arrays.asList(new String[]{OBJECT_TYPE1, OBJECT_TYPE2, OBJECT_TYPE3}));
		final Object[] result = contentProvider.getElements(null);
		assertEquals(expectedIds.size(), result.length);
		for(Object entry : result)
		{
			final IObjectTypeDescription objectType = (IObjectTypeDescription)entry;
			assertTrue(expectedIds.contains(objectType.getId()));
		}
	}
	
	private IObjectTypeDescription createObjectTypeWithId(final String id)
	{
		final Mock<IObjectTypeDescription> object = mock(IObjectTypeDescription.class);
		object.stubs().method("getId").will(returnValue(id));
		object.stubs().method("getDisplayName").will(returnValue(id));
		
		return object.proxy();
	}
}
