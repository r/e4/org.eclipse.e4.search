/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit;

import org.eclipse.platform.discovery.ui.test.unit.internal.CustomResultUiXpParserTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.AdvancedSearchParamsUiContribXpParserTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.CustomSearchParamsUiXpParserTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.DestinationsContentProviderTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.DestinationsLabelProviderTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.DiscoveryTreeViewerFactoryTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.DragSrcInteractionListenerTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.EnablePropertiesForAllViewCustomizationTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.ExceptionThrowingAdvancedUiContributorTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.ExceptionThrowingCustomResultUiContributorTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.FormTextBuilderTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.GenericResultContentProviderTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.GenericResultLabelProviderTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.LocalContextSelectionTransferSetterTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.LocalSelectionTransferSetterTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.PropertiesTooltipConfiguratorTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.SearchConsoleDestinationsSelectorTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.SearchForListContentProviderTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.SearchForListLabelProviderTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.SearchForSelectionChangeListenerTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.FavoritesContentProviderTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.FavoritesDropInteractionListenerForLocalSelectionTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.FavoritesDropInteractionListenerTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.SearchFavoritesItemsAdderTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.SearchConsoleSelectionProviderTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.SortingMenuManagerTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.ToolTipConfiguratorTest;
import org.eclipse.platform.discovery.ui.test.unit.internal.XmlDiscoverySelectionTransferSetterTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@SuiteClasses({
		SearchForListLabelProviderTest.class,
		SearchForListContentProviderTest.class,
		SearchForSelectionChangeListenerTest.class,
		DestinationsLabelProviderTest.class,
		DestinationsContentProviderTest.class,
		CustomResultUiXpParserTest.class,
		GenericResultContentProviderTest.class,
		GenericResultLabelProviderTest.class,
		DragSrcInteractionListenerTest.class,
		SearchConsoleSelectionProviderTest.class,
		AdvancedSearchParamsUiContribXpParserTest.class,
		FavoritesContentProviderTest.class,
		FavoritesDropInteractionListenerTest.class,
		FavoritesDropInteractionListenerForLocalSelectionTest.class,
		DiscoveryTreeViewerFactoryTest.class,
		LocalSelectionTransferSetterTest.class,
		XmlDiscoverySelectionTransferSetterTest.class,
		LocalContextSelectionTransferSetterTest.class,
		SearchFavoritesItemsAdderTest.class,
		SearchConsoleDestinationsSelectorTest.class,
		FormTextBuilderTest.class,
		ToolTipConfiguratorTest.class,
		PropertiesTooltipConfiguratorTest.class,
		EnablePropertiesForAllViewCustomizationTest.class,
		SortingMenuManagerTest.class,
		ExceptionThrowingAdvancedUiContributorTest.class,
		ExceptionThrowingCustomResultUiContributorTest.class,
		CustomSearchParamsUiXpParserTest.class
		})
@RunWith(Suite.class)
public class AllTestsSuite
{
}
