/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.ui.internal.view.dnd.ISourceDndInteractionEvent;
import org.eclipse.platform.discovery.ui.internal.view.dnd.impl.XmlDiscoverySelectionTransferSetter;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.platform.discovery.util.internal.xml.ICollectionTransformer;
import org.jmock.core.Constraint;


public class XmlDiscoverySelectionTransferSetterTest extends MockObjectTestCase
{
	private Mock<ILongOperationRunner> opRunner;
	private Mock<ICollectionTransformer> transformer;
	private IStructuredSelection selection;
	private Object selectedObject;
	private XmlDiscoverySelectionTransferSetter setter;
	
	@Override
	protected void setUp() throws Exception
	{
		opRunner = mock(ILongOperationRunner.class);
		transformer = mock(ICollectionTransformer.class);
		selectedObject = new Object();
		selection = new StructuredSelection(selectedObject);
		
		setter = new XmlDiscoverySelectionTransferSetter(opRunner.proxy()){
			@Override
			protected ICollectionTransformer createTransformer()
			{
				return transformer.proxy();
			}
		};
	}
	
	public void testSetData()
	{
		final Mock<ISourceDndInteractionEvent> event = mock(ISourceDndInteractionEvent.class);
		final String dataObject = "Test data";
		transformer.expects(once()).method("setCollectionTag").with(eq("discovery-selection")); //$NON-NLS-1$
		transformer.expects(once()).method("setItemTag").with(eq("selection-item")); //$NON-NLS-1$
		transformer.expects(once()).method("setNamespace").with(eq("http://eclipse.org/platform.discovery")); //$NON-NLS-1$
		transformer.expects(once()).method("transform").with(iteratorConstraint(selectedObject), eq(opRunner.proxy())).will(returnValue(dataObject));
		
		event.expects(once()).method("setData").with(eq(dataObject));
		
		setter.setData(selection, event.proxy());
	}
	
	private Constraint iteratorConstraint(final Object... expectedObjects)
	{
		return new Constraint(){
			@SuppressWarnings("unchecked")
			public boolean eval(Object arg0)
			{
				final Iterator<Object> it = (Iterator<Object>)arg0;
				final List<Object> selectedObjects = new ArrayList<Object>();
				while(it.hasNext())
				{
					selectedObjects.add(it.next());
				}
				
				final boolean result = (selectedObjects.size() == expectedObjects.length) && selectedObjects.containsAll(Arrays.asList(expectedObjects));
				return result;
			}

			public StringBuffer describeTo(StringBuffer arg0)
			{
				return arg0;
			}}; 
	}
	
}
