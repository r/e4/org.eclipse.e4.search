/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import org.eclipse.platform.discovery.ui.internal.tooltip.IToolTipConfigurator;
import org.eclipse.platform.discovery.ui.internal.tooltip.ToolTipConfigurator;

public class ToolTipConfiguratorTest extends AbstractTooltipConfiguratorTest
{
	private static final String LINE1 = "line1";
	private static final String LINE2 = "line2";
	private static final String LINE3 = "line3";

	@Override
	protected IToolTipConfigurator createConfigurator()
	{
		return new ToolTipConfigurator()
		{
			@Override
			protected String textContent()
			{
				return LINE1 + "\n" + LINE2 + "\n" + LINE3;
			}
			
			@Override
			protected TooltipFormTextBuilder createNewTooltipTextBuilder()
			{
				return createFormTextBuilderMock(textBuilder.proxy());
			}
		};
	}
	
	@Override
	public void testGetFormText()
	{
		textBuilder.expects(exactly(3)).method("startParagraph");
		textBuilder.expects(exactly(3)).method("endParagraph");
		textBuilder.expects(once()).method("appendText").with(eq(LINE1));
		textBuilder.expects(once()).method("appendText").with(eq(LINE2));
		textBuilder.expects(once()).method("appendText").with(eq(LINE3));
		super.testGetFormText();
	}
}
