/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.platform.discovery.ui.internal.view.dnd.impl.DragSrcInteractionListener;
import org.eclipse.platform.discovery.ui.internal.view.dnd.impl.LocalSelectionTransferSetter;
import org.eclipse.swt.dnd.Transfer;


public class LocalSelectionTransferSetterTest extends LocalSelectionTransferSetterFixture<LocalSelectionTransferSetter>
{
	@Override
	protected void setUp() throws Exception
	{
		super.setUp();
	}
	
	@Override
	protected DragSrcInteractionListener.ITransferDataSetter<? extends Transfer> createTransferSetter()
	{
		return new LocalSelectionTransferSetter(){
			@Override
			public LocalSelectionTransfer getTransfer()
			{
				return LocalSelectionTransfer.getTransfer();
			}
		};
	}

	@Override
	protected IStructuredSelection createSelection()
	{
		return (IStructuredSelection)mock(IStructuredSelection.class).proxy();
	}

	@Override
	protected ISelection getTransferSelection()
	{
		return LocalSelectionTransfer.getTransfer().getSelection();
	}
}
