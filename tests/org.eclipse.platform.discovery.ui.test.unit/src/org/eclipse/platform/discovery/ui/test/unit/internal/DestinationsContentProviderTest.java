/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.ui.internal.view.impl.DestinationsContentProvider;


public class DestinationsContentProviderTest extends MockObjectTestCase
{
	private DestinationsContentProvider contentProvider;
	
	private Mock<ISearchDestination> destination_1;
	private Mock<ISearchDestination> destination_2;
	private Mock<ISearchDestination> destination_3;
	private final static String DESTINATION_NAME_1 = "First destination";
	private final static String DESTINATION_NAME_2 = "Second destination";
	private final static String DESTINATION_NAME_3 = "Third destination";
	
	private Mock<IDestinationCategoryDescription> category_1;
	private Mock<IDestinationCategoryDescription> category_2;
	private final static String CATEGORY_ID_1 = "category1";
	private final static String CATEGORY_ID_2 = "category2";
	private final static String CATEGORY_NAME_1 = "First category";
	private final static String CATEGORY_NAME_2 = "Second category";

	@Override
	protected void setUp() throws Exception
	{
		setupSearchDestinations();
		setupSearchCategories();
		
		contentProvider = new DestinationsContentProvider(){
			@Override
			protected List<ISearchDestination> getSearchDestinationsForCategory(IDestinationCategoryDescription category)
			{
				return getMockedDestinationForCategory(category);
			}};
	}

	private void setupSearchCategories()
	{
		category_1 = mock(IDestinationCategoryDescription.class);
		category_1.stubs().method("getId").will(returnValue(CATEGORY_ID_1));
		category_1.stubs().method("getDisplayName").will(returnValue(CATEGORY_NAME_1));
		category_2 = mock(IDestinationCategoryDescription.class);
		category_2.stubs().method("getId").will(returnValue(CATEGORY_ID_2));
		category_2.stubs().method("getDisplayName").will(returnValue(CATEGORY_NAME_2));
	}

	private void setupSearchDestinations()
	{
		destination_1 = mock(ISearchDestination.class);
		destination_1.stubs().method("getDisplayName").will(returnValue(DESTINATION_NAME_1));
		destination_2 = mock(ISearchDestination.class);
		destination_2.stubs().method("getDisplayName").will(returnValue(DESTINATION_NAME_2));
		destination_3 = mock(ISearchDestination.class);
		destination_3.stubs().method("getDisplayName").will(returnValue(DESTINATION_NAME_3));
	}
	
	private List<ISearchDestination> getMockedDestinationForCategory(final IDestinationCategoryDescription category)
	{
		final List<ISearchDestination> result = new ArrayList<ISearchDestination>();
		if(category.getId().equals(CATEGORY_ID_1))
		{
			result.add(destination_1.proxy());
			result.add(destination_2.proxy());
		}
		else if(category.getId().equals(CATEGORY_ID_2))
		{
			result.add(destination_3.proxy());
		}
		
		return result; 
	}
	
	public void testGetChildrenForCategory()
	{
		final List<String> destinationNames = new ArrayList<String>();
		for(Object o : contentProvider.getChildren(category_1.proxy()))
		{
			destinationNames.add(((ISearchDestination)o).getDisplayName());
		}
		
		assertEquals("Two destinations expected for category1", 2, destinationNames.size());
		assertTrue("Destination1 expected to be in the result", destinationNames.contains(DESTINATION_NAME_1));
		assertTrue("Destination2 expected to be in the result", destinationNames.contains(DESTINATION_NAME_2));
		
		destinationNames.clear();
		for(Object o : contentProvider.getChildren(category_2.proxy()))
		{
			destinationNames.add(((ISearchDestination)o).getDisplayName());
		}
		
		assertEquals("Two destinations expected for category1", 1, destinationNames.size());
		assertTrue("Destination1 expected to be in the result", destinationNames.contains(DESTINATION_NAME_3));
	}
	
	public void testGetChildrenForDestination()
	{
		assertTrue("Empty array expected for destination children", contentProvider.getChildren(destination_1.proxy()).length == 0);
		assertTrue("Empty array expected for destination children", contentProvider.getChildren(destination_2.proxy()).length == 0);
		assertTrue("Empty array expected for destination children", contentProvider.getChildren(destination_3.proxy()).length == 0);
	}
	
	public void testGetParentForCategory()
	{
		assertNull("No parent expected for category", contentProvider.getParent(category_1.proxy()));
		assertNull("No parent expected for category", contentProvider.getParent(category_2.proxy()));
	}
	
	public void testHasChildren()
	{
		assertTrue("Children expected for category", contentProvider.hasChildren(category_1.proxy()));
		assertFalse("No children expected for category", contentProvider.hasChildren(destination_1.proxy()));
	}
	
	public void testGetElements()
	{
		final List<IDestinationCategoryDescription> categories = Arrays.asList(new IDestinationCategoryDescription[]{category_1.proxy(), category_2.proxy()});
		final Object[] elements = contentProvider.getElements(categories);
		assertEquals("Two elements expected", 2, elements.length);
		final List<String> elementIds = new ArrayList<String>();
		for(Object o : elements)
		{
			elementIds.add(((IDestinationCategoryDescription)o).getId());
		}
		
		assertTrue("Category 1 expected to be contained in the result", elementIds.contains(CATEGORY_ID_1));
		assertTrue("Category 2 expected to be contained in the result", elementIds.contains(CATEGORY_ID_2));
	}
}
