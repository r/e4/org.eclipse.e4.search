/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.ui.internal.view.dnd.ISourceDndInteractionEvent;
import org.eclipse.platform.discovery.ui.internal.view.dnd.impl.DragSrcInteractionListener;
import org.eclipse.swt.dnd.Transfer;


public abstract class LocalSelectionTransferSetterFixture<T extends DragSrcInteractionListener.ITransferDataSetter<? extends Transfer>> extends MockObjectTestCase
{
	protected abstract DragSrcInteractionListener.ITransferDataSetter<? extends Transfer> createTransferSetter();
	//protected abstract Transfer getTransfer();
	protected abstract IStructuredSelection createSelection(); 
	
	private IStructuredSelection selection;
	private Mock<ISourceDndInteractionEvent> event;
	private DragSrcInteractionListener.ITransferDataSetter<? extends Transfer> transferSetter;
	//private Transfer transfer;
	protected abstract ISelection getTransferSelection();
	
	@Override
	protected void setUp() throws Exception
	{
		selection = createSelection();
		event = mock(ISourceDndInteractionEvent.class);
		transferSetter = createTransferSetter();
		//transfer = getTransfer();
	}
	
	public void testSetData()
	{
		event.expects(once()).method("setData").with(eq(selection));
		transferSetter.setData(selection, event.proxy());
		assertSame("Unexpected selection transfer selection", selection, getTransferSelection());
	}
	
	public void testDragStarted()
	{
		transferSetter.dragStarted(selection, event.proxy());
		assertSame("Unexpected selection transfer selection", selection, getTransferSelection());
	}
}
