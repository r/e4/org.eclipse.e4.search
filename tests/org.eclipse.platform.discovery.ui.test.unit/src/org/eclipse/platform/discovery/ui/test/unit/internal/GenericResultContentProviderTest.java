/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.jface.text.TextViewer;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.ui.api.IGenericViewCustomization;
import org.eclipse.platform.discovery.ui.api.IMasterDiscoveryView;
import org.eclipse.platform.discovery.ui.api.IResultsViewAccessor;
import org.eclipse.platform.discovery.ui.api.ITooltipProvider;
import org.eclipse.platform.discovery.ui.api.ISearchConsoleCustomization;
import org.eclipse.platform.discovery.ui.api.impl.GenericViewCustomizationImpl;
import org.eclipse.platform.discovery.ui.internal.view.result.impl.GenericResultContentProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.jmock.core.Constraint;


public class GenericResultContentProviderTest extends MockObjectTestCase
{
	private Mock<ITreeContentProvider> contentProvider1;
	private Mock<ITreeContentProvider> contentProvider2;

	private Object element1, child1;
	private Object element2, child2, child3;

	private Set<IGenericViewCustomization> customizations;
	private GenericResultContentProvider testedProvider;

	@Override
	protected void setUp() throws Exception
	{
		element1 = new Object();
		child1 = new Object();
		element2 = new Object();
		child2 = new Object();
		child3 = new Object();

		setupContentProviders();
		setupCustomizations();

		testedProvider = new GenericResultContentProvider(new ArrayList<IGenericViewCustomization>(customizations));
	}

	private void setupCustomizations()
	{
		customizations = new HashSet<IGenericViewCustomization>();
		customizations.add(viewCustomizations(contentProvider1.proxy()));
		customizations.add(viewCustomizations(contentProvider2.proxy()));
		customizations.add(viewCustomizations(null));
	}

	final ISearchConsoleCustomization viewCustomizations(final ITreeContentProvider cp)
	{
		return new TestCustomization()
		{

			public boolean acceptSearchProvider(String searchProviderId)
			{
				throw new UnsupportedOperationException();
			}

			public ITreeContentProvider getContentProvider()
			{
				return cp;
			}

			public ILabelProvider getLabelProvider()
			{
				throw new UnsupportedOperationException();
			}

			public ITooltipProvider getTooltipProvider()
			{
				throw new UnsupportedOperationException();
			}

			public void installAction(IContributedAction contributedAction, IResultsViewAccessor viewAccessor)
			{
				throw new UnsupportedOperationException();
			}

			public void setMasterView(IMasterDiscoveryView masterView)
			{
				throw new UnsupportedOperationException();
			}
		};
	}

	private void setupContentProviders()
	{
		contentProvider1 = mock(ITreeContentProvider.class);
		contentProvider1.stubs().method("getElements").will(returnValue(new Object[] { element1 }));
		contentProvider1.stubs().method("getChildren").with(eq(element1)).will(returnValue(new Object[] { child1 }));
		contentProvider1.stubs().method("getChildren").with((eq(child1))).will(returnValue(new Object[0]));
		contentProvider1.stubs().method("getChildren").with(new Constraint()
		{
			public boolean eval(Object arg0)
			{
				return arg0 != element1 && arg0 != child1;
			}

			public StringBuffer describeTo(StringBuffer arg0)
			{
				return arg0;
			}
		}).will(returnValue(new Object[0]));

		contentProvider2 = mock(ITreeContentProvider.class);
		contentProvider2.stubs().method("getElements").will(returnValue(new Object[] { element2 }));
		contentProvider2.stubs().method("getChildren").with(eq(element2)).will(returnValue(new Object[] { child2, child3 }));
		contentProvider2.stubs().method("getChildren").with(eq(child2)).will(returnValue(new Object[0]));
		contentProvider2.stubs().method("getChildren").with(eq(child3)).will(returnValue(new Object[0]));
		contentProvider2.stubs().method("getChildren").with(new Constraint()
		{
			public boolean eval(Object arg0)
			{
				return arg0 != element2 && arg0 != child2 && arg0 != child3;
			}

			public StringBuffer describeTo(StringBuffer arg0)
			{
				return arg0;
			}
		}).will(returnValue(new Object[0]));
	}

	public void testGetElements()
	{
		final Object[] firstChildren = testedProvider.getElements(null);
		assertEquals("Two children expected", 2, firstChildren.length);
		Set<Object> childrentSet = new HashSet<Object>(Arrays.asList(firstChildren));
		assertTrue("Element1 not found", childrentSet.contains(element1));
		assertTrue("Element2 not found", childrentSet.contains(element2));

		childrentSet = new HashSet<Object>(Arrays.asList(testedProvider.getChildren(element1)));
		assertEquals("One child expected", 1, childrentSet.size());
		assertTrue("Child1 not found", childrentSet.contains(child1));

		
		childrentSet = new HashSet<Object>(Arrays.asList(testedProvider.getChildren(element2)));
		assertEquals("Two children expected", 2, childrentSet.size());
		assertTrue("Child2 not found", childrentSet.contains(child2));
		assertTrue("Child3 not found", childrentSet.contains(child3));
	}

	public void testDispose()
	{
		contentProvider1.expects(once()).method("dispose");
		contentProvider2.expects(once()).method("dispose");
		testedProvider.dispose();
	}

	public void testInputChanged()
	{
		//final Mock<Viewer> viewer = mock(Viewer.class);
		
		final Viewer viewer = createDummyViewer();
		final Object oldInput = new Object();
		final Object newInput = new Object();
		contentProvider1.expects(once()).method("inputChanged").with(eq(viewer), eq(oldInput), eq(newInput));
		contentProvider2.expects(once()).method("inputChanged").with(eq(viewer), eq(oldInput), eq(newInput));
		testedProvider.inputChanged(viewer, oldInput, newInput);
	}
	
	private Viewer createDummyViewer()
	{
		final Shell s = new Shell(PlatformUI.getWorkbench().getDisplay());
		return new TextViewer(s, SWT.NONE);
	}

	public void testHasChildren()
	{
		final Object object = new Object();
		contentProvider1.expects(once()).method("hasChildren").with(same(object)).will(returnValue(false));
		contentProvider2.expects(once()).method("hasChildren").with(same(object)).will(returnValue(false));
		assertFalse(testedProvider.hasChildren(object));

		contentProvider1.reset();
		contentProvider2.reset();
		contentProvider1.expects(atMostOnce()).method("hasChildren").with(same(object)).will(returnValue(true));
		contentProvider2.expects(atMostOnce()).method("hasChildren").with(same(object)).will(returnValue(false));
		assertTrue(testedProvider.hasChildren(object));

		contentProvider1.reset();
		contentProvider2.reset();
		contentProvider1.expects(atMostOnce()).method("hasChildren").with(same(object)).will(returnValue(false));
		contentProvider2.expects(atMostOnce()).method("hasChildren").with(same(object)).will(returnValue(true));
		assertTrue(testedProvider.hasChildren(object));
	}

	private abstract class TestCustomization extends GenericViewCustomizationImpl implements ISearchConsoleCustomization
	{
	}
}
