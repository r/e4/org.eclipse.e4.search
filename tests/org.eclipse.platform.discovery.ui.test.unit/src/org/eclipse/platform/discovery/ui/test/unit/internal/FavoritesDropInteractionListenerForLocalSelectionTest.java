/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.unit.internal;

import org.eclipse.platform.discovery.core.api.ISearchFavoritesMasterController;
import org.eclipse.platform.discovery.core.internal.IContextStructuredSelection;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.ui.internal.dnd.LocalContextSelectionTransfer;
import org.eclipse.platform.discovery.ui.internal.view.impl.FavoritesDropInteractionListener;
import org.eclipse.platform.discovery.ui.test.unit.internal.dnd.DndTestFixture;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;


public class FavoritesDropInteractionListenerForLocalSelectionTest extends MockObjectTestCase
{
	private DndTestFixture.TestDNDInteractionEvent dropEvent;
	private FavoritesDropInteractionListener dropListener;
	private Mock<ISearchFavoritesMasterController> controller;
	private Mock<IContextStructuredSelection> selection;

	@Override
	protected void setUp() throws Exception 
	{
		dropEvent = new DndTestFixture.TestDNDInteractionEvent();
		controller = mock(ISearchFavoritesMasterController.class);
		selection = mock(IContextStructuredSelection.class);
		
		dropEvent.setDataType(LocalContextSelectionTransfer.getTransfer().getSupportedTypes()[0]);
		LocalContextSelectionTransfer.getTransfer().setSelection(selection.proxy());
		dropListener = new FavoritesDropInteractionListener(new Transfer[]{LocalContextSelectionTransfer.getTransfer()}, controller.proxy());
	}
	
	public void testDragEnterWithSupportedDataTypeAndAcceptableSelection()
	{
		controller.expects(once()).method("isImportPossible").with(same(selection.proxy())).will(returnValue(true));
		
		dropListener.start(dropEvent);
		assertTrue("DND.DROP_DEFAULT not set to event datail", dropEvent.getInteractionDetail() == DND.DROP_DEFAULT);
	}
	
	public void testDragEnterWithSupportedDataTypeAndUnacceptableSelection()
	{
		controller.expects(once()).method("isImportPossible").with(same(selection.proxy())).will(returnValue(false));
		
		dropListener.start(dropEvent);
		assertTrue("DND.DROP_DEFAULT not set to event datail", dropEvent.getInteractionDetail() == DND.DROP_NONE);
	}
		
}
