/*******************************************************************************
 * Copyright (c) 2012 SAP AG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.destprefs.test.unit.xpparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

import java.util.List;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.destprefs.api.ISearchDestinationConfigurator;
import org.eclipse.platform.discovery.destprefs.internal.xpparser.ISearchDestinationsConfiguratorDescription;
import org.eclipse.platform.discovery.destprefs.internal.xpparser.SearchDestinationsConfiguratorXPParser;
import org.eclipse.platform.discovery.destprefs.test.unit.SearchConfiguratorsFixture;
import org.eclipse.platform.discovery.runtime.internal.ISearchProviderConfiguration;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.AbstractExtensionPointParser;
import org.eclipse.platform.discovery.testutils.utils.registry.ExtensionRegistryBuilder;
import org.eclipse.platform.discovery.testutils.utils.testcases.AbstractExtensionPointParserTest;

public class SearchDestinationsConfiguratorXPParserTest extends AbstractExtensionPointParserTest<ISearchDestinationsConfiguratorDescription> {
  
	private SearchConfiguratorsFixture configuratorsFixture = new SearchConfiguratorsFixture();
	
    @Override
    protected AbstractExtensionPointParser<ISearchDestinationsConfiguratorDescription> createParser(IExtensionRegistry registry) {
        return new SearchDestinationsConfiguratorXPParser(registry) {
            @Override
            protected ISearchProviderConfiguration searchProviderConfiguration() {
                return configuratorsFixture.searchProviderConfiguration;
            }
        };
    }

    @Override
    protected void setupRegistry(final ExtensionRegistryBuilder registryBuilder) {
        configuratorsFixture.setup(registryBuilder);
    }

    @Override
    protected void verifyContributions(final List<ISearchDestinationsConfiguratorDescription> contributions) {
        assertEquals("3 contributions expected", 3, contributions.size());
        verifyConfigurator(configuratorsFixture.CATEGORY_1, configuratorsFixture.DEST_PROVIDER_CAT1_1, configuratorsFixture.CONFIGURATOR_1_1_FQNAME, configuratorsFixture.configurator_1_1, contributions);
        verifyConfigurator(configuratorsFixture.CATEGORY_1, configuratorsFixture.DEST_PROVIDER_CAT1_2, configuratorsFixture.CONFIGURATOR_1_2_FQNAME, configuratorsFixture.configurator_1_2, contributions);
        verifyConfigurator(configuratorsFixture.CATEGORY_2, configuratorsFixture.DEST_PROVIDER_CAT2, configuratorsFixture.CONFIGURATOR_2_FQNAME, configuratorsFixture.configurator_2, contributions);
    }

    private void verifyConfigurator(final String destCategory, final String destProviderId, final String configuratorFQName,
            final ISearchDestinationConfigurator<?> configurator, final List<ISearchDestinationsConfiguratorDescription> contributions) {
        final ISearchDestinationsConfiguratorDescription configuratorDescr = findConfiguratorDescription(destProviderId, contributions);
        assertEquals("Unexpected destination category", destCategory, configuratorDescr.destinationCategoryId());
        assertSame("Unexpected configurator instance", configurator, configuratorDescr.createConfigurator());
    }

    private ISearchDestinationsConfiguratorDescription findConfiguratorDescription(final String destProviderId,
            final List<ISearchDestinationsConfiguratorDescription> contributions) {
        for (ISearchDestinationsConfiguratorDescription descr : contributions) {
            if (descr.destinationProviderId().equals(destProviderId)) {
                return descr;
            }
        }
        fail("Could not find configurator for destination provider " + destProviderId);
        throw new IllegalStateException();
    }
}
