/*******************************************************************************
 * Copyright (c) 2012 SAP AG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.destprefs.test.unit.prefpage.ui;

import static org.eclipse.swtbot.swt.finder.finders.UIThreadRunnable.syncExec;

import java.util.Collection;

import org.eclipse.platform.discovery.destprefs.internal.i18n.DestPrefsMessages;
import org.eclipse.platform.discovery.destprefs.internal.prefpage.ui.CategoryDestinationProviderPair;
import org.eclipse.platform.discovery.destprefs.internal.prefpage.ui.DestinationsPreferencePage;
import org.eclipse.platform.discovery.destprefs.internal.prefpage.ui.IDestinationConfiguratorsPresenter;
import org.eclipse.platform.discovery.destprefs.internal.prefpage.ui.IDestinationConfiguratorsView;
import org.eclipse.platform.discovery.testutils.utils.pageobjects.InShellPageObject;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swtbot.swt.finder.results.VoidResult;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotButton;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTree;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTreeItem;
import org.eclipse.ui.forms.widgets.FormToolkit;

public class DestinationsPreferencePagePageObject extends InShellPageObject {
    private final IDestinationConfiguratorsPresenter presenter;
    private DestinationsPreferencePage page;

    public DestinationsPreferencePagePageObject(final IDestinationConfiguratorsPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    protected void createContent(Shell parent, FormToolkit formToolkit) {
        page = new DestinationsPreferencePage(this.presenter);
        page.createControl(parent);
    }

    public IDestinationConfiguratorsView getView() {
        return page;
    }

    public boolean canAddDestination() {
        return addDestinationButton().isEnabled();
    }

    public void addDestination() {
        addDestinationButton().click();
    }

    public boolean canEditDestination() {
        return editDestinationButton().isEnabled();
    }

    public void editDestination() {
        editDestinationButton().click();
    }

    public boolean canRemoveDestination() {
        return removeDestinationButton().isEnabled();
    }

    public void removeDestination() {
        removeDestinationButton().click();
    }

    public boolean canTestDestination() {
        return testDestinationButton().isEnabled();
    }

    public void testDestination() {
        testDestinationButton().click();
    }

    public CategoryPageObject getCategory(final String categoryName) {
        return new CategoryPageObject(categoryTreeItem(categoryName));
    }
    
    private SWTBotTreeItem categoryTreeItem(String category) {
        for (SWTBotTreeItem item : destinationsTree().getAllItems()) {
            if (item.getText().startsWith(category)) {
                return item;
            }
        }
        throw new RuntimeException("Could not find category " + category);
    }

    public DestinationPageObject getDestination(final String categoryName, final String destinationName) {
        return getCategory(categoryName).getDestination(destinationName);
    }
    
    public int getMessageType() {
        return page.getMessageType();
    }

    public String getMessage() {
        return page.getMessage();
    }

    public void setInput(final Collection<CategoryDestinationProviderPair> input) {
        syncExec(new VoidResult() {
            @Override
            public void run() {
                page.setInput(input);
            }
        });
    }

    public void setAddEnabled(final boolean enabled) {
        syncExec(new VoidResult() {
            @Override
            public void run() {
                page.setAddEnabled(enabled);
            }
        });
    }

    public void setEditEnabled(final boolean enabled) {
        syncExec(new VoidResult() {
            @Override
            public void run() {
                page.setEditEnabled(enabled);
            }
        });
    }

    public void setRemoveEnabled(final boolean enabled) {
        syncExec(new VoidResult() {
            @Override
            public void run() {
                page.setRemoveEnabled(enabled);
            }
        });
    }

    public void setTestEnabled(final boolean enabled) {
        syncExec(new VoidResult() {
            @Override
            public void run() {
                page.setTestEnabled(enabled);
            }
        });
    }

    public String getDisplayedStatus() {
        return bot().clabel().getText();
    }
    
    private SWTBotTree destinationsTree() {
        return bot().tree();
    }

    private SWTBotButton addDestinationButton() {
        return bot().button(DestPrefsMessages.DestinationsPrefPage_AddButton);
    }

    private SWTBotButton removeDestinationButton() {
        return bot().button(DestPrefsMessages.DestinationsPrefPage_DeleteButton);
    }

    private SWTBotButton editDestinationButton() {
        return bot().button(DestPrefsMessages.DestinationsPrefPage_EditButton);
    }

    private SWTBotButton testDestinationButton() {
        return bot().button(DestPrefsMessages.DestinationsPrefPage_TestButton);
    }
}
