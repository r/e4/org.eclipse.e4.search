package org.eclipse.platform.discovery.destprefs.test.unit.prefpage.ui;

import java.text.MessageFormat;

import org.eclipse.swtbot.swt.finder.widgets.SWTBotTreeItem;

public class CategoryPageObject {
    private SWTBotTreeItem treeItem;

    CategoryPageObject(final SWTBotTreeItem treeItem) {
        this.treeItem = treeItem;
    }

    public void select() {
        this.treeItem.select();
    }

    public DestinationPageObject getDestination(final String destinationName) {
        this.treeItem.expand();
        for(SWTBotTreeItem destNode : this.treeItem.getItems()) {
            if(destinationName.equals(destNode.getText())) {
                return new DestinationPageObject(destNode);
            }
        }
        throw new RuntimeException(MessageFormat.format("Could not find destination {0} for category {1}", destinationName, this.getName()));
    }

    public String getName() {
        return this.treeItem.getText();
    }

    public boolean isDestinationsVisible() {
        return this.treeItem.isExpanded();
    }
}
