package org.eclipse.platform.discovery.destprefs.test.unit;

import static org.eclipse.platform.discovery.testutils.utils.matchers.Matchers.arrayEqualsWithoutOrder;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.verify;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.destprefs.internal.DestinationsManager;
import org.eclipse.platform.discovery.destprefs.internal.IPreferenceDialogCreator;
import org.eclipse.platform.discovery.destprefs.internal.xpparser.ISearchDestinationsConfiguratorDescription;
import org.eclipse.platform.discovery.destprefs.internal.xpparser.SearchDestinationsConfiguratorXPParser;
import org.eclipse.platform.discovery.runtime.internal.ISearchProviderConfiguration;
import org.eclipse.platform.discovery.runtime.internal.SearchProviderConfigurationFactory;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.AbstractExtensionPointParser;
import org.eclipse.platform.discovery.testutils.utils.registry.ExtensionRegistryBuilder;
import org.eclipse.swt.widgets.Shell;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class DestinationsManagerTest {
	
	private static final String PREFPAGE_ID = "prefpage_id";
	private static final String DEST_CATEGORY_ID = "category";

	private DestinationsManager destinationsManager;
	
	@Mock
	private AbstractExtensionPointParser<ISearchDestinationsConfiguratorDescription> destConfigurator;
	
	@Mock
	private IPreferenceDialogCreator preferenceDialogCreator;
	@Mock
	private Shell shell;

	private ExtensionRegistryBuilder registryBuilder;

	private ISearchProviderConfiguration searchConfig;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		registryBuilder = new ExtensionRegistryBuilder();
	}
	
	@Test
	public void noPreferencePages() {
		
		IExtensionRegistry registry = registryBuilder.getRegistry();
		init(registry);
		
		destinationsManager.manageDestinations(shell, null);
		
		verify(preferenceDialogCreator).openPreferenceDialog(same(shell), eq(DestinationsManager.GENERIC_PREFPAGE_ID), argThat(arrayEqualsWithoutOrder(DestinationsManager.GENERIC_PREFPAGE_ID)));
	}
	
	@Test
	public void onePreferencePageNoSelection() {
		
		registryBuilder.addDestinationCategory(DEST_CATEGORY_ID, DEST_CATEGORY_ID);
		registryBuilder.addDestinationsProvider("destprovider", DEST_CATEGORY_ID, PREFPAGE_ID, "foo", null);
		
		IExtensionRegistry registry = registryBuilder.getRegistry();
		init(registry);
		
		destinationsManager.manageDestinations(shell, null);
		
		verify(preferenceDialogCreator).openPreferenceDialog(same(shell), eq(DestinationsManager.GENERIC_PREFPAGE_ID),  argThat(arrayEqualsWithoutOrder(DestinationsManager.GENERIC_PREFPAGE_ID, PREFPAGE_ID)));
	}
	
	@Test
	public void onePreferencePageAndDestCategorySelected() {
		registryBuilder.addDestinationCategory(DEST_CATEGORY_ID, DEST_CATEGORY_ID);
		registryBuilder.addDestinationsProvider("destprovider", DEST_CATEGORY_ID, PREFPAGE_ID, "foo", null);
		
		IExtensionRegistry registry = registryBuilder.getRegistry();
		init(registry);
		
		IDestinationCategoryDescription category = getCategory(DEST_CATEGORY_ID);
		
		destinationsManager.manageDestinations(shell, category);
		
		verify(preferenceDialogCreator).openPreferenceDialog(same(shell), eq(PREFPAGE_ID),  argThat(arrayEqualsWithoutOrder(DestinationsManager.GENERIC_PREFPAGE_ID, PREFPAGE_ID)));

	}
	
	@Test
	public void oneDestinationNoPreferencePageAndDestCategorySelected() {
		registryBuilder.addDestinationCategory(DEST_CATEGORY_ID, DEST_CATEGORY_ID);
		registryBuilder.addDestinationsProvider("destprovider", DEST_CATEGORY_ID, null, "foo", null);
		
		IExtensionRegistry registry = registryBuilder.getRegistry();
		init(registry);
		
		IDestinationCategoryDescription category = getCategory(DEST_CATEGORY_ID);
		
		destinationsManager.manageDestinations(shell, category);
		
		verify(preferenceDialogCreator).openPreferenceDialog(same(shell), eq(DestinationsManager.GENERIC_PREFPAGE_ID),  argThat(arrayEqualsWithoutOrder(DestinationsManager.GENERIC_PREFPAGE_ID)));

	}
	
	@Test
	public void oneConfiguratorNoSelection() {
		
		registryBuilder.addDestinationCategory(DEST_CATEGORY_ID, DEST_CATEGORY_ID);
		registryBuilder.addDestinationsProvider("destprovider", DEST_CATEGORY_ID, null, "foo", null);
		registryBuilder.addSearchDestinationsConfigurator("configurator", "destprovider", "bar", null);

		IExtensionRegistry registry = registryBuilder.getRegistry();
		init(registry);
		
		destinationsManager.manageDestinations(shell, null);
		
		verify(preferenceDialogCreator).openPreferenceDialog(same(shell), eq(DestinationsManager.GENERIC_PREFPAGE_ID),  argThat(arrayEqualsWithoutOrder(DestinationsManager.GENERIC_PREFPAGE_ID)));
	}
	
	@Test
	public void oneConfiguratorAndDestCategorySelected() {
		
		registryBuilder.addDestinationCategory(DEST_CATEGORY_ID, DEST_CATEGORY_ID);
		registryBuilder.addDestinationsProvider("destprovider", DEST_CATEGORY_ID, null, "foo", null);
		registryBuilder.addSearchDestinationsConfigurator("configurator", "destprovider", "bar", null);

		IExtensionRegistry registry = registryBuilder.getRegistry();
		init(registry);
		
		IDestinationCategoryDescription category = getCategory(DEST_CATEGORY_ID);

		destinationsManager.manageDestinations(shell, category);
		
		verify(preferenceDialogCreator).openPreferenceDialog(same(shell), eq(DestinationsManager.GENERIC_PREFPAGE_ID),  argThat(arrayEqualsWithoutOrder(DestinationsManager.GENERIC_PREFPAGE_ID)));
	}
	
	@Test
	public void configuratorTakesPrecedenceOverPrefPage() {
		
		registryBuilder.addDestinationCategory(DEST_CATEGORY_ID, DEST_CATEGORY_ID);
		registryBuilder.addDestinationsProvider("destprovider", DEST_CATEGORY_ID, PREFPAGE_ID, "foo", null);
		registryBuilder.addSearchDestinationsConfigurator("configurator", "destprovider", "bar", null);

		IExtensionRegistry registry = registryBuilder.getRegistry();
		init(registry);
		
		IDestinationCategoryDescription category = getCategory(DEST_CATEGORY_ID);

		destinationsManager.manageDestinations(shell, category);
		
		verify(preferenceDialogCreator).openPreferenceDialog(same(shell), eq(DestinationsManager.GENERIC_PREFPAGE_ID),  argThat(arrayEqualsWithoutOrder(DestinationsManager.GENERIC_PREFPAGE_ID,PREFPAGE_ID)));
	}
	
	

	private IDestinationCategoryDescription getCategory(String destCategoryId) {
		for(IDestinationCategoryDescription desc: searchConfig.getDestinationCategories()) {
			if(desc.getId().equals(destCategoryId)) {
				return desc;
			}
		}
		
		fail("dest category not found:"+destCategoryId);return null;
 	}

	private void init(IExtensionRegistry registry) {
		searchConfig = new SearchProviderConfigurationFactory().getSearchProviderConfiguration(registry);
		destinationsManager = new DestinationsManager(new SearchDestinationsConfiguratorXPParser(registry), preferenceDialogCreator, searchConfig);

	}
}
