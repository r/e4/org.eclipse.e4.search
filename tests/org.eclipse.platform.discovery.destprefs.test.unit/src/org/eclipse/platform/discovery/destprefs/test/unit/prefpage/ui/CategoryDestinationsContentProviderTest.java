/*******************************************************************************
 * Copyright (c) 2012 SAP AG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.destprefs.test.unit.prefpage.ui;

import static org.junit.Assert.*;

import java.util.*;

import org.eclipse.platform.discovery.destprefs.internal.prefpage.ui.CategoryDestinationProviderPair;
import org.eclipse.platform.discovery.destprefs.internal.prefpage.ui.CategoryDestinationsContentProvider;
import org.eclipse.platform.discovery.destprefs.internal.prefpage.ui.nodes.CategoryNode;
import org.eclipse.platform.discovery.destprefs.internal.prefpage.ui.nodes.DestinationNode;
import org.eclipse.platform.discovery.runtime.api.IDestinationsProvider;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationsProviderDescription;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class CategoryDestinationsContentProviderTest {

    private CategoryDestinationsContentProvider contentProvider;

    @Mock private ISearchDestination DESTINATION_1;
    @Mock private ISearchDestination DESTINATION_2;
    @Mock private ISearchDestination DESTINATION_3;
    private final static String DESTINATION_NAME_1 = "First destination";
    private final static String DESTINATION_NAME_2 = "Second destination";
    private final static String DESTINATION_NAME_3 = "Third destination";

    @Mock private IDestinationCategoryDescription CATEGORY_1;
    @Mock private IDestinationCategoryDescription CATEGORY_2;
    private final static String CATEGORY_NAME_1 = "First category";
    private final static String CATEGORY_NAME_2 = "Second category";

    @Mock private IDestinationsProviderDescription DESTINATION_PROVIDER_1;
    @Mock private IDestinationsProviderDescription DESTINATION_PROVIDER_2;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        setupSearchDestinations();
        setupDestinationProviders();
        setupCategories();

        contentProvider = new CategoryDestinationsContentProvider();
    }

    private void setupSearchDestinations() {
        Mockito.when(DESTINATION_1.getDisplayName()).thenReturn(DESTINATION_NAME_1);
        Mockito.when(DESTINATION_2.getDisplayName()).thenReturn(DESTINATION_NAME_2);
        Mockito.when(DESTINATION_3.getDisplayName()).thenReturn(DESTINATION_NAME_3);
    }

    private void setupCategories() {
        Mockito.when(CATEGORY_1.getDisplayName()).thenReturn(CATEGORY_NAME_1);
        Mockito.when(CATEGORY_2.getDisplayName()).thenReturn(CATEGORY_NAME_2);
    }
    
    private void setupDestinationProviders() {
        final Set<ISearchDestination> destinations1 = new HashSet<ISearchDestination>();
        destinations1.add(DESTINATION_1);
        destinations1.add(DESTINATION_2);

        final Set<ISearchDestination> destinations2 = new HashSet<ISearchDestination>();
        destinations2.add(DESTINATION_3);
        
        final IDestinationsProvider provider1 = Mockito.mock(IDestinationsProvider.class);
        Mockito.when(provider1.getSearchDestinations()).thenReturn(destinations1);
        final IDestinationsProvider provider2 = Mockito.mock(IDestinationsProvider.class);
        Mockito.when(provider2.getSearchDestinations()).thenReturn(destinations2);
        
        Mockito.when(DESTINATION_PROVIDER_1.createProvider()).thenReturn(provider1);
        Mockito.when(DESTINATION_PROVIDER_2.createProvider()).thenReturn(provider2);
    }

    @Test
    public void testGetElements() {
        final CategoryDestinationProviderPair categDescPair_1 = new CategoryDestinationProviderPair(CATEGORY_1, DESTINATION_PROVIDER_1);
        final CategoryDestinationProviderPair categDescPair_2 = new CategoryDestinationProviderPair(CATEGORY_2, DESTINATION_PROVIDER_2);
        
        final List<CategoryDestinationProviderPair> pairs = Arrays.asList(new CategoryDestinationProviderPair[] { categDescPair_1,
                categDescPair_2 });
        final Object[] elements = contentProvider.getElements(pairs);
        assertEquals("Two elements expected", 2, elements.length);
        final List<String> elementDisplayNames = new ArrayList<String>();
        for (Object object : elements) {
            elementDisplayNames.add(((CategoryNode) object).getDisplayName());
        }

        assertTrue("Category 1 expected to be contained in the result", elementDisplayNames.contains(CATEGORY_NAME_1));
        assertTrue("Category 2 expected to be contained in the result", elementDisplayNames.contains(CATEGORY_NAME_2));
    }
    
    @Test
    public void testHasChildren() {
        final CategoryNode categoryNode = new CategoryNode(CATEGORY_NAME_1, DESTINATION_PROVIDER_1);
        final DestinationNode destinationNode = new DestinationNode(categoryNode, DESTINATION_1);
        
        assertTrue("Children expected for category node", contentProvider.hasChildren(categoryNode));
        assertFalse("No children expected for destination node", contentProvider.hasChildren(destinationNode));
    }
    
    @Test
    public void testGetChildren() {
        final CategoryNode categoryNode1 = new CategoryNode(CATEGORY_NAME_1, DESTINATION_PROVIDER_1);
        final CategoryNode categoryNode2 = new CategoryNode(CATEGORY_NAME_2, DESTINATION_PROVIDER_2);

        assertFalse("Array should not be empty", contentProvider.getChildren(categoryNode1).length == 0);
        assertTrue("Array should have 2 children", contentProvider.getChildren(categoryNode1).length == 2);
        assertTrue("Array should have 1 child", contentProvider.getChildren(categoryNode2).length == 1);
        
        final List<String> destinationDisplayNames = new ArrayList<String>();
        final Object[] elements = contentProvider.getChildren(categoryNode1);
        for (Object object : elements) {
            destinationDisplayNames.add(((DestinationNode) object).getDestination().getDisplayName());
        }
        
        assertTrue("Destination 1 expected to be contained in the result", destinationDisplayNames.contains(DESTINATION_NAME_1));
        assertTrue("Destination 2 expected to be contained in the result", destinationDisplayNames.contains(DESTINATION_NAME_2));
        assertFalse("Destination 3 not expected to be contained in the result", destinationDisplayNames.contains(DESTINATION_NAME_3));
    }
    
}
