package org.eclipse.platform.discovery.destprefs.test.unit.prefpage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.platform.discovery.destprefs.api.ISearchDestinationConfigurator;
import org.eclipse.platform.discovery.destprefs.api.ISearchDestinationTester;
import org.eclipse.platform.discovery.destprefs.internal.prefpage.ui.CategoryDestinationProviderPair;
import org.eclipse.platform.discovery.destprefs.internal.prefpage.ui.DestinationConfiguratorSelection;
import org.eclipse.platform.discovery.destprefs.internal.prefpage.ui.DestinationConfiguratorsPresenter;
import org.eclipse.platform.discovery.destprefs.internal.prefpage.ui.IDestinationConfiguratorsView;
import org.eclipse.platform.discovery.destprefs.internal.xpparser.SearchDestinationsConfiguratorXPParser;
import org.eclipse.platform.discovery.destprefs.test.unit.SearchConfiguratorsFixture;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.internal.ISearchProviderConfiguration;
import org.eclipse.platform.discovery.testutils.utils.registry.ExtensionRegistryBuilder;
import org.eclipse.swt.widgets.Shell;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class DestinationConfiguratorsPresenterTest {
	
	private ExtensionRegistryBuilder registryBuilder;
	@Mock private IDestinationConfiguratorsView view;
	@Mock private Shell viewShell;
	private SearchConfiguratorsFixture fixture;
	private SearchDestinationsConfiguratorXPParser parser;

	private DestinationConfiguratorsPresenter presenter;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		registryBuilder  = new ExtensionRegistryBuilder();
		fixture = new SearchConfiguratorsFixture();
		fixture.setup(registryBuilder);
		parser =  new SearchDestinationsConfiguratorXPParser( registryBuilder.getRegistry() ) {
            @Override
            protected ISearchProviderConfiguration searchProviderConfiguration() {
                return fixture.searchProviderConfiguration;
            }
        };
		presenter = new DestinationConfiguratorsPresenter(parser, fixture.searchProviderConfiguration);
		
		Mockito.when(view.getShell()).thenReturn(viewShell);
		presenter.setView(view);
		
		Mockito.verify(view).setInput(Mockito.argThat(matchesFixture()));
	}
	
	private Matcher<Collection<CategoryDestinationProviderPair>> matchesFixture() {
		return new BaseMatcher <Collection<CategoryDestinationProviderPair>> () {

			private Collection<CategoryDestinationProviderPair> actual;

			@SuppressWarnings("unchecked")
			@Override
			public boolean matches(Object item) {
				actual = (Collection<CategoryDestinationProviderPair>)item;
				return actual.containsAll(expected()) && actual.size()==expected().size();
			}

			private Collection<CategoryDestinationProviderPair> expected() {
				List<CategoryDestinationProviderPair> result = new ArrayList<CategoryDestinationProviderPair>();
				
				result.add(new CategoryDestinationProviderPair(fixture.categoryDesc1, fixture.searchProviderConfiguration.getDestinationProvidersForCategory(fixture.categoryDesc1).get(0)));
				result.add(new CategoryDestinationProviderPair(fixture.categoryDesc1, fixture.searchProviderConfiguration.getDestinationProvidersForCategory(fixture.categoryDesc1).get(1)));
				result.add(new CategoryDestinationProviderPair(fixture.categoryDesc2, fixture.searchProviderConfiguration.getDestinationProvidersForCategory(fixture.categoryDesc2).get(0)));

				return result;
			}

			@Override
			public void describeTo(Description description) {
				description.appendText(actual.toString());
			}
			
		};
	}


	@Test
	public void destinationSelectedTogglesEnablement() {
		presenter.selectionChanged(new DestinationConfiguratorSelection(fixture.DEST_PROVIDER_CAT1_1, fixture.destination1));
		Mockito.verify(view).setAddEnabled(Mockito.eq(true));
		Mockito.verify(view).setEditEnabled(Mockito.eq(true));
		Mockito.verify(view).setRemoveEnabled(Mockito.eq(true));
		
		//test is disabled if get
		Mockito.verify(view).setTestEnabled(Mockito.eq(false));
	}
	
	@Test
	public void add() {
		presenter.selectionChanged(new DestinationConfiguratorSelection(fixture.DEST_PROVIDER_CAT1_1, null));

		ISearchDestinationConfigurator<?> expectedConfigurator = fixture.configurator_1_1;
		IStatus result = Status.OK_STATUS;
		Mockito.when(expectedConfigurator.createDestination(Mockito.same(viewShell))).thenReturn(result);
		
		presenter.addDestination();	
		
		Mockito.verify(expectedConfigurator).createDestination(Mockito.same(viewShell));
		Mockito.verify(view).setStatus(Mockito.same(result));
		Mockito.verify(view, Mockito.times(2)).setInput(Mockito.argThat(matchesFixture()));

	}
	
	@Test
	public void addNotOkDoesNotSetInput() {
		presenter.selectionChanged(new DestinationConfiguratorSelection(fixture.DEST_PROVIDER_CAT1_1, fixture.destination1));

		ISearchDestinationConfigurator<?> expectedConfigurator = fixture.configurator_1_1;
		IStatus result = Status.CANCEL_STATUS;
		Mockito.when(expectedConfigurator.createDestination(Mockito.same(viewShell))).thenReturn(result);
		
		presenter.addDestination();	
		
		Mockito.verify(expectedConfigurator).createDestination(Mockito.same(viewShell));
		Mockito.verify(view).setStatus(Mockito.same(result));
		Mockito.verify(view, Mockito.times(1)).setInput(Mockito.argThat(matchesFixture()));

	}
	
	@Test
	public void destinationCategorySelectedTogglesEnablement() {
		presenter.selectionChanged(new DestinationConfiguratorSelection(fixture.DEST_PROVIDER_CAT1_1, null));

		Mockito.verify(view).setAddEnabled(Mockito.eq(true));
		Mockito.verify(view).setEditEnabled(Mockito.eq(false));
		Mockito.verify(view).setRemoveEnabled(Mockito.eq(false));
		Mockito.verify(view).setTestEnabled(Mockito.eq(false));

	}
	
	@Test
	public void testEnablementWhenHasTester() {
		presenter.selectionChanged(new DestinationConfiguratorSelection(fixture.DEST_PROVIDER_CAT1_1, null));
		@SuppressWarnings("unchecked")
		ISearchDestinationTester<ISearchDestination> tester = Mockito.mock(ISearchDestinationTester.class);
		Mockito.when(fixture.configurator_1_1.getSearchDestinationTester()).thenReturn(tester);
		presenter.selectionChanged(new DestinationConfiguratorSelection(fixture.DEST_PROVIDER_CAT1_1, fixture.destination1));

		Mockito.verify(view).setTestEnabled(Mockito.eq(true));

	}
	
	@Test
	public void edit() {
		presenter.selectionChanged(new DestinationConfiguratorSelection(fixture.DEST_PROVIDER_CAT1_1, fixture.destination1));

		ISearchDestinationConfigurator<ISearchDestination> expectedConfigurator = fixture.configurator_1_1;
		IStatus result = Status.OK_STATUS;
		Mockito.when(expectedConfigurator.editDestination(Mockito.same(viewShell), Mockito.same(fixture.destination1))).thenReturn(result);
		
		presenter.editDestination();	
		
		Mockito.verify(expectedConfigurator).editDestination(Mockito.same(viewShell), Mockito.same(fixture.destination1));
		Mockito.verify(view).setStatus(Mockito.same(result));
		Mockito.verify(view, Mockito.times(2)).setInput(Mockito.argThat(matchesFixture()));

	}
	
	@Test
	public void remove() {
		presenter.selectionChanged(new DestinationConfiguratorSelection(fixture.DEST_PROVIDER_CAT1_1, fixture.destination1));

		ISearchDestinationConfigurator<ISearchDestination> expectedConfigurator = fixture.configurator_1_1;
		IStatus result = Status.OK_STATUS;
		Mockito.when(expectedConfigurator.deleteDestination(Mockito.same(viewShell), Mockito.same(fixture.destination1))).thenReturn(result);
		
		presenter.removeDestination();	
		
		Mockito.verify(expectedConfigurator).deleteDestination(Mockito.same(viewShell), Mockito.same(fixture.destination1));
		Mockito.verify(view).setStatus(Mockito.same(result));
		Mockito.verify(view, Mockito.times(2)).setInput(Mockito.argThat(matchesFixture()));
	}
	
	@Test
	public void testClearSelection() {
	    presenter.selectionChanged(new DestinationConfiguratorSelection(null, null));
        Mockito.verify(view).setAddEnabled(Mockito.eq(false));
        Mockito.verify(view).setEditEnabled(Mockito.eq(false));
        Mockito.verify(view).setRemoveEnabled(Mockito.eq(false));
        Mockito.verify(view).setTestEnabled(Mockito.eq(false));
	}
	
	@Test
	public void test() {
		presenter.selectionChanged(new DestinationConfiguratorSelection(fixture.DEST_PROVIDER_CAT1_1, fixture.destination1));

		ISearchDestinationConfigurator<ISearchDestination> expectedConfigurator = fixture.configurator_1_1;
		IStatus result = Status.OK_STATUS;
		
		@SuppressWarnings("unchecked")
		ISearchDestinationTester<ISearchDestination> destTester = Mockito.mock(ISearchDestinationTester.class);
		
		Mockito.when(expectedConfigurator.getSearchDestinationTester()).thenReturn(destTester);
		Mockito.when(destTester.test(Mockito.same(viewShell), Mockito.same(fixture.destination1))).thenReturn(result);
		
		presenter.testDestination();	
		
		Mockito.verify(destTester).test(Mockito.same(viewShell), Mockito.same(fixture.destination1));
		Mockito.verify(view).setStatus(Mockito.same(result));
		Mockito.verify(view, Mockito.times(1)).setInput(Mockito.argThat(matchesFixture()));

	}
	
}
