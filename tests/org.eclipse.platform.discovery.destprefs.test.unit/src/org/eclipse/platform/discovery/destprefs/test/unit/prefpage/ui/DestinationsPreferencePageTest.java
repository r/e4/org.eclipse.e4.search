/*******************************************************************************
 * Copyright (c) 2012 SAP AG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.destprefs.test.unit.prefpage.ui;

import static org.eclipse.swtbot.swt.finder.finders.UIThreadRunnable.syncExec;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.platform.discovery.destprefs.internal.prefpage.ui.CategoryDestinationProviderPair;
import org.eclipse.platform.discovery.destprefs.internal.prefpage.ui.DestinationConfiguratorSelection;
import org.eclipse.platform.discovery.destprefs.internal.prefpage.ui.IDestinationConfiguratorsPresenter;
import org.eclipse.platform.discovery.runtime.api.IDestinationsProvider;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationsProviderDescription;
import org.eclipse.swtbot.swt.finder.results.VoidResult;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public class DestinationsPreferencePageTest {
    private static final String CATEGORY_ID = "my.category.id";
    private static final String CATEGORY_NAME = "My Category";
    private static final String DESTINATION_PROVIDER_ID = "my.dest.provider.id";
    private static final String DESTINATION_NAME = "My Destination";
    private String currentDestinationName;

    private DestinationsPreferencePagePageObject pageObject;
    @Mock
    private IDestinationConfiguratorsPresenter presenter;
    @Mock
    private IDestinationCategoryDescription category;
    @Mock
    private IDestinationsProviderDescription destinationProviderDescr;
    @Mock
    private IDestinationsProvider destinationProvider;
    @Mock
    private ISearchDestination destination;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        stubDestinations();
        pageObject = new DestinationsPreferencePagePageObject(presenter);

        pageObject.open();
        pageObject.setInput(createPageInput());
        Mockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                pageObject.getView().setAddEnabled(true);
                pageObject.getView().setRemoveEnabled(true);
                pageObject.getView().setEditEnabled(true);
                pageObject.getView().setTestEnabled(true);
                return null;
            }
        }).when(presenter).selectionChanged(Mockito.any(DestinationConfiguratorSelection.class));

    }

    private void stubDestinations() {
        currentDestinationName = DESTINATION_NAME;
        Mockito.stub(destination.getDisplayName()).toAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                return currentDestinationName;
            }
        });
        Mockito.stub(destinationProvider.getSearchDestinations()).toReturn(
                new HashSet<ISearchDestination>(Arrays.asList(new ISearchDestination[] { destination })));
        Mockito.stub(destinationProviderDescr.createProvider()).toReturn(destinationProvider);
        Mockito.stub(destinationProviderDescr.getId()).toReturn(DESTINATION_PROVIDER_ID);
        Mockito.stub(category.getId()).toReturn(CATEGORY_ID);
        Mockito.stub(category.getDisplayName()).toReturn(CATEGORY_NAME);
    }

    private Collection<CategoryDestinationProviderPair> createPageInput() {
        final Collection<CategoryDestinationProviderPair> result = new ArrayList<CategoryDestinationProviderPair>();
        result.add(new CategoryDestinationProviderPair(category, destinationProviderDescr));
        return result;
    }

    @After
    public void tearDown() {
        pageObject.close();
    }

    @Test
    public void testNoDestinationsSelected() {
        assertFalse("Adding destination is unexpectedly enabled", pageObject.canAddDestination());
        assertFalse("Removing destination is unexpectedly enabled", pageObject.canRemoveDestination());
        assertFalse("Editing destination is unexpectedly enabled", pageObject.canEditDestination());
        assertFalse("Testing destination is unexpectedly enabled", pageObject.canTestDestination());
    }

    @Test
    public void testEnableAdd() {
        pageObject.setAddEnabled(true);
        assertTrue("Adding destination is unexpectedly disabled", pageObject.canAddDestination());
    }

    @Test
    public void testEnableEdit() {
        pageObject.setEditEnabled(true);
        assertTrue("Editing destination is unexpectedly disabled", pageObject.canEditDestination());
    }

    @Test
    public void testEnableRemove() {
        pageObject.setRemoveEnabled(true);
        assertTrue("Removing destination is unexpectedly disabled", pageObject.canRemoveDestination());
    }

    @Test
    public void testEnableTest() {
        pageObject.setTestEnabled(true);
        assertTrue("Testingdestination is unexpectedly disabled", pageObject.canTestDestination());
    }

    @Test
    public void testSetErrorStatus() throws InterruptedException {
        setStatusTest(createStatus(IStatus.ERROR, "Some error"));
    }

    @Test
    public void testSetWarningStatus() throws InterruptedException {
        setStatusTest(createStatus(IStatus.WARNING, "Some warning"));
    }

    @Test
    public void testSetCancelStatus() throws InterruptedException {
        setStatusTest(createStatus(IStatus.CANCEL, "Cancelled"));
    }

    @Test
    public void testSetOkStatus() throws InterruptedException {
        setStatusTest(createStatus(IStatus.OK, "Success"));
    }

    @Test
    public void testSelectCategory() {
        pageObject.getCategory(CATEGORY_NAME).select();
        Mockito.verify(presenter, Mockito.atLeastOnce()).selectionChanged(Mockito.argThat(selectionMatcher(DESTINATION_PROVIDER_ID, null)));
    }

    @Test
    public void testSelectDestination() {
        selectTheDestination();
        Mockito.verify(presenter, Mockito.atLeastOnce()).selectionChanged(Mockito.argThat(selectionMatcher(DESTINATION_PROVIDER_ID, destination)));
    }

    @Test
    public void testAddDestination() {
        selectTheDestination();
        Mockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                pageObject.setInput(createPageInput());
                return null;
            }
        }).when(presenter).addDestination();

        pageObject.addDestination();
        Mockito.verify(presenter).addDestination();
        assertCategoryIsExpanded();
        assertDestinationIsSelected(DESTINATION_NAME);
    }

    @Test
    public void testEditDestination() {
        selectTheDestination();
        final String newDestinationName = "ModifiedDestinationName";
        Mockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                currentDestinationName = newDestinationName;
                pageObject.setInput(createPageInput());
                return null;
            }
        }).when(presenter).editDestination();

        pageObject.editDestination();
        Mockito.verify(presenter).editDestination();

        assertCategoryIsExpanded();
        assertDestinationIsSelected(newDestinationName);
    }

    private void assertCategoryIsExpanded() {
        final CategoryPageObject category = pageObject.getCategory(CATEGORY_NAME);
        assertTrue("Category does not display destinations after edit", category.isDestinationsVisible());
    }

    private void assertDestinationIsSelected(final String destinationName) {
        final DestinationPageObject destination = pageObject.getCategory(CATEGORY_NAME).getDestination(destinationName);
        assertTrue("Destination is not selected", destination.isSelected());
    }

    @Test
    public void testRemoveDestination() {
        selectTheDestination();
        Mockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                pageObject.setInput(createPageInput());
                return null;
            }
        }).when(presenter).removeDestination();

        pageObject.removeDestination();
        Mockito.verify(presenter).removeDestination();
        assertCategoryIsExpanded();
        Mockito.verify(presenter).selectionChanged(Mockito.argThat(emptySelectionMatcher()));
        
    }

    private Matcher<DestinationConfiguratorSelection> emptySelectionMatcher() {
        return new BaseMatcher<DestinationConfiguratorSelection>() {
            @Override
            public boolean matches(Object item) {
                final DestinationConfiguratorSelection selection = (DestinationConfiguratorSelection) item;
                return selection.destination == null && selection.destProviderId == null;
            }

            @Override
            public void describeTo(Description description) {
            }
        };
    }

    @Test
    public void testTestDestination() {
        selectTheDestination();
        pageObject.testDestination();
        Mockito.verify(presenter).testDestination();
    }

    @Test
    public void testSelectionPreservedOnDestinationEdit() {
        selectTheDestination();
        final String newDestinationName = "ModifiedDestinationName";

        Mockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                currentDestinationName = newDestinationName;
                pageObject.setInput(createPageInput());
                return null;
            }
        }).when(presenter).editDestination();
        pageObject.editDestination();

        final DestinationPageObject selectedDestination = pageObject.getCategory(CATEGORY_NAME).getDestination(newDestinationName);
        assertTrue("Modified destination lost its selection", selectedDestination.isSelected());
    }

    private void selectTheDestination() {
        pageObject.getCategory(CATEGORY_NAME).getDestination(DESTINATION_NAME).select();
    }

    private Matcher<DestinationConfiguratorSelection> selectionMatcher(String destinationProviderId, final ISearchDestination destination) {
        return new BaseMatcher<DestinationConfiguratorSelection>() {
            @Override
            public boolean matches(Object item) {
                final DestinationConfiguratorSelection selection = (DestinationConfiguratorSelection) item;
                return selection.destProviderId != null && selection.destProviderId.equals(DESTINATION_PROVIDER_ID) && (selection.destination == destination);
            }

            @Override
            public void describeTo(Description description) {
            }
        };
    }

    private void setStatusTest(final IStatus status) {
        syncExec(new VoidResult() {
            @Override
            public void run() {
                pageObject.getView().setStatus(status);
            }
        });
        assertEquals("Unexpected status displayed", status.getSeverity() == IStatus.OK ? "" : status.getMessage(), pageObject.getDisplayedStatus());
    }

    private IStatus createStatus(int severity, String message) {
        final IStatus status = Mockito.mock(IStatus.class);
        Mockito.stub(status.getSeverity()).toReturn(severity);
        Mockito.stub(status.getMessage()).toReturn(message);
        return status;
    }
}
