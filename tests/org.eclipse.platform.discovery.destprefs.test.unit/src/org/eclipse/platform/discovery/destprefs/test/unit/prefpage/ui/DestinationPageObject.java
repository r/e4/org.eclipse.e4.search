package org.eclipse.platform.discovery.destprefs.test.unit.prefpage.ui;

import org.eclipse.swtbot.swt.finder.widgets.SWTBotTreeItem;

public class DestinationPageObject {
    private final SWTBotTreeItem destTreeNode;

    DestinationPageObject(final SWTBotTreeItem destTreeNode) {
        this.destTreeNode = destTreeNode;
    }

    public void select() {
        this.destTreeNode.select();
    }
    
    public String getName() {
        return this.destTreeNode.getText();
    }

    public boolean isSelected() {
        return this.destTreeNode.isSelected();
    }
}
