/*******************************************************************************
 * Copyright (c) 2012 SAP AG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.destprefs.test.unit;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.platform.discovery.destprefs.api.ISearchDestinationConfigurator;
import org.eclipse.platform.discovery.runtime.api.IDestinationsProvider;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.internal.ISearchProviderConfiguration;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationsProviderDescription;
import org.eclipse.platform.discovery.testutils.utils.registry.ExtensionRegistryBuilder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public class SearchConfiguratorsFixture {
	public final String CATEGORY_1 = "category_one";
	public final String CATEGORY_2 = "category_two";

	public final String DEST_PROVIDER_CAT1_1 = "destProviderCat1_1";
	public final String DEST_PROVIDER_CAT1_2 = "destProviderCat1_2";
	public final String DEST_PROVIDER_CAT2 = "destProviderCat2";

	public final String CONFIGURATOR_1_1_ID = "configurator_1_1";
	public final String CONFIGURATOR_1_2_ID = "configurator_1_2";
	public final String CONFIGURATOR_2_ID = "configurator_2";

	public final String CONFIGURATOR_1_1_FQNAME = "org.eclipse.platform.discovery.destprefs.configurator_1_1";
	public final String CONFIGURATOR_1_2_FQNAME = "org.eclipse.platform.discovery.destprefs.configurator_1_2";
	public final String CONFIGURATOR_2_FQNAME = "org.eclipse.platform.discovery.destprefs.configurator_2";

	@Mock
	public ISearchDestinationConfigurator<ISearchDestination> configurator_1_1;
	@Mock
	public ISearchDestinationConfigurator<ISearchDestination> configurator_1_2;
	@Mock
	public ISearchDestinationConfigurator<ISearchDestination> configurator_2;
	@Mock
	public ISearchProviderConfiguration searchProviderConfiguration;
	@Mock
	public IDestinationsProvider destprovider1;
	@Mock
	public ISearchDestination destination1;
	
	public IDestinationCategoryDescription categoryDesc1;
	public IDestinationCategoryDescription categoryDesc2;


	public void setup(ExtensionRegistryBuilder registryBuilder) {
		MockitoAnnotations.initMocks(this);
		setupSearchProviderConfiguration();

		setupDestProvider();

		registryBuilder.addDestinationCategory(CATEGORY_1, CATEGORY_1);
		registryBuilder.addDestinationCategory(CATEGORY_2, CATEGORY_2);

		registryBuilder
		.addDestinationsProvider(DEST_PROVIDER_CAT1_1, CATEGORY_1, null, "someprovider_1_1", destprovider1);
		registryBuilder
		.addDestinationsProvider(DEST_PROVIDER_CAT1_2, CATEGORY_1, null, "someprovider_1_2", destprovider1);
		registryBuilder.addDestinationsProvider(DEST_PROVIDER_CAT2, CATEGORY_2, null, "someprovider2", destprovider1);

		registryBuilder.addSearchDestinationsConfigurator(CONFIGURATOR_1_1_ID, DEST_PROVIDER_CAT1_1, CONFIGURATOR_1_1_FQNAME, configurator_1_1);
		registryBuilder.addSearchDestinationsConfigurator(CONFIGURATOR_1_2_ID, DEST_PROVIDER_CAT1_2, CONFIGURATOR_1_2_FQNAME, configurator_1_2);
		registryBuilder.addSearchDestinationsConfigurator(CONFIGURATOR_2_ID, DEST_PROVIDER_CAT2, CONFIGURATOR_2_FQNAME, configurator_2);
	}

	private void setupDestProvider() {
		Set<ISearchDestination> destinations = new HashSet<ISearchDestination>();
		destinations.add(destination1);
		Mockito.when(destprovider1.getSearchDestinations()).thenReturn(destinations);
	}

	private void setupSearchProviderConfiguration() {
		final List<IDestinationCategoryDescription> allDestinationCategories = destinationCategories();
		Mockito.stub(searchProviderConfiguration.getDestinationCategories()).toReturn(allDestinationCategories);
		Mockito.when(searchProviderConfiguration.getDestinationProvidersForCategory(Mockito.any(IDestinationCategoryDescription.class))).thenAnswer(
				new Answer<List<IDestinationsProviderDescription>>() {
					@Override
					public List<IDestinationsProviderDescription> answer(InvocationOnMock invocation) throws Throwable {
						final List<IDestinationsProviderDescription> result = new ArrayList<IDestinationsProviderDescription>();
						final IDestinationCategoryDescription category = (IDestinationCategoryDescription) invocation.getArguments()[0];
						if (category.getId().equals(CATEGORY_1)) {
							result.add(destinationProvider(DEST_PROVIDER_CAT1_1));
							result.add(destinationProvider(DEST_PROVIDER_CAT1_2));
						} else if (category.getId().equals(CATEGORY_2)) {
							result.add(destinationProvider(DEST_PROVIDER_CAT2));
						}
						return result;
					}

				});

	}

	private IDestinationsProviderDescription destinationProvider(final String destProviderId) {
		final IDestinationsProviderDescription provider = Mockito.mock(IDestinationsProviderDescription.class);
		Mockito.stub(provider.getId()).toReturn(destProviderId);
		return provider;
	}

	private List<IDestinationCategoryDescription> destinationCategories() {
		final List<IDestinationCategoryDescription> result = new ArrayList<IDestinationCategoryDescription>();
		categoryDesc1 = mockDestinationCategory(CATEGORY_1);
		categoryDesc2 = mockDestinationCategory(CATEGORY_2);
		result.add(categoryDesc1);
		result.add(categoryDesc2);
		return result;
	}

	private IDestinationCategoryDescription mockDestinationCategory(final String categoryId) {
		final IDestinationCategoryDescription category = Mockito.mock(IDestinationCategoryDescription.class);
		Mockito.stub(category.getId()).toReturn(categoryId);
		return category;
	}

}
