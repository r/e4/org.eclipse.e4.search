/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.integration.test.unit.internal;

import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.core.api.ISearchContext;
import org.eclipse.platform.discovery.integration.internal.plugin.DiscoveryIntegrationMessages;
import org.eclipse.platform.discovery.integration.internal.plugin.IViewCustomizationConfiguration;
import org.eclipse.platform.discovery.integration.internal.viewcustomization.IActionWithAvailability;
import org.eclipse.platform.discovery.integration.internal.viewcustomization.SaveInFavoritesContributedAction;
import org.eclipse.platform.discovery.integration.internal.viewcustomization.SaveInFavoritesViewCustomization;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.ui.api.IMasterDiscoveryView;
import org.eclipse.platform.discovery.ui.api.IResultsViewAccessor;
import org.eclipse.platform.discovery.ui.api.ISearchFavoritesViewCustomization;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;


public class SaveInFavoritesViewCustomizationTest extends MockObjectTestCase
{
	private SaveInFavoritesViewCustomization customization;
	private Mock<IMasterDiscoveryView> masterView;
	private Mock<IContributedAction> contributedAction;
	private TreeViewer treeViewer;
	private Mock<ISelection> viewerSelection;
	private Mock<ISearchContext> searchContext;
	private Mock<IViewCustomizationConfiguration> viewCustomizationConfig;
	private Mock<ISearchFavoritesViewCustomization> favoritesCustomization;
	private Mock<IActionWithAvailability> menuAction;
	private Mock<IDiscoveryEnvironment> environment;
	private Mock<IResultsViewAccessor> viewAccessor;
	private Mock<IMenuManager> menuManager;

	@Override
	protected void setUp() throws Exception
	{
		contributedAction = mock(IContributedAction.class);
		menuAction = mock(IActionWithAvailability.class);

		environment = mock(IDiscoveryEnvironment.class);
		masterView = mock(IMasterDiscoveryView.class);
		masterView.stubs().method("getEnvironment").will(returnValue(environment.proxy()));

		setupViewAccessor();

		favoritesCustomization = mock(ISearchFavoritesViewCustomization.class);
		viewCustomizationConfig = mock(IViewCustomizationConfiguration.class);
		viewCustomizationConfig.stubs().method("availableSearchFavoritesCustomizations").will(returnValue(Arrays.asList(new ISearchFavoritesViewCustomization[] { favoritesCustomization.proxy() })));

		customization = new SaveInFavoritesViewCustomization()
		{
			@Override
			protected IActionWithAvailability createSaveAction(IContributedAction contributedAction, ISelection selection, ISearchContext searchContext, List<ISearchFavoritesViewCustomization> sbCustomizations, IDiscoveryEnvironment environment)
			{
				assertTrue("Unexpected action", contributedAction == SaveInFavoritesViewCustomizationTest.this.contributedAction.proxy());
				assertTrue("Unexpcted selection", selection == viewerSelection.proxy());
				assertTrue("Unexpected environment", environment == SaveInFavoritesViewCustomizationTest.this.environment.proxy());
				assertEquals("One customization expected", 1, sbCustomizations.size());
				assertTrue("Test customization not found", sbCustomizations.contains(favoritesCustomization.proxy()));
				return menuAction.proxy();
			}

			@Override
			protected IViewCustomizationConfiguration viewCustomizationConfiguration()
			{
				return viewCustomizationConfig.proxy();
			}
		};
		customization.setMasterView(masterView.proxy());
	}
	
	@Override
	protected void tearDown() throws Exception
	{
		super.tearDown();
		// Dispose the parent shell created on setupViewAccessor() 
		treeViewer.getControl().getParent().dispose();
	}

	private void setupViewAccessor()
	{
		viewerSelection = mock(ISelection.class);
		searchContext = mock(ISearchContext.class);
		final Shell s = new Shell(PlatformUI.getWorkbench().getDisplay());
		treeViewer = new DummyTreeViewer(s, viewerSelection.proxy(), searchContext.proxy());
		viewAccessor = mock(IResultsViewAccessor.class);
		viewAccessor.stubs().method("getTreeViewer").will(returnValue(treeViewer));

		menuManager = mock(IMenuManager.class);
		viewAccessor.stubs().method("getMenuManager").will(returnValue(menuManager.proxy()));
	}

	public void testAcceptSearchProvider()
	{
		assertTrue(customization.acceptSearchProvider("A"));
		assertTrue(customization.acceptSearchProvider(null));
	}

	public void testInstallActionWithUnknownId()
	{
		contributedAction.stubs().method("getActionId").will(returnValue("unknown"));
		customization.installAction(contributedAction.proxy(), viewAccessor.proxy());
	}

	public void testInstallActionWhenNotAvailable()
	{
		contributedAction.stubs().method("getActionId").will(returnValue(SaveInFavoritesContributedAction.ACTION_ID));
		menuAction.expects(once()).method("isAvailable").will(returnValue(false));
		customization.installAction(contributedAction.proxy(), viewAccessor.proxy());
	}

	public void testInstallAction()
	{
		contributedAction.stubs().method("getActionId").will(returnValue(SaveInFavoritesContributedAction.ACTION_ID));
		menuAction.expects(once()).method("setText").with(eq(DiscoveryIntegrationMessages.SaveInSearchFavoritesAction_Text));
		menuAction.expects(once()).method("isAvailable").will(returnValue(true));
		menuManager.expects(once()).method("add").with(eq(menuAction.proxy()));
		customization.installAction(contributedAction.proxy(), viewAccessor.proxy());
	}
	
	private class DummyTreeViewer extends TreeViewer
	{
		private final ISelection selection;
		private final Object input;

		public DummyTreeViewer(final Composite parent, final ISelection selection, final Object input)
		{
			super(parent);
			this.selection = selection;
			this.input = input;
		}
		
		@Override
		public ISelection getSelection()
		{
			return selection;
		}
		
		@Override
		public Object getInput()
		{
			return input;
		}
	}
}
