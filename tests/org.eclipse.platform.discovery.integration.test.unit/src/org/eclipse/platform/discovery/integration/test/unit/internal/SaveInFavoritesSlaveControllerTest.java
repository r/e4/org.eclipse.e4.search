/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.integration.test.unit.internal;

import java.util.Set;

import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.integration.internal.slavecontrollers.SaveInFavoritesSlaveController;
import org.eclipse.platform.discovery.integration.internal.viewcustomization.SaveInFavoritesContributedAction;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;


public class SaveInFavoritesSlaveControllerTest extends MockObjectTestCase
{
	private SaveInFavoritesSlaveController controller;
	
	@Override
	protected void setUp() throws Exception
	{
		controller = new SaveInFavoritesSlaveController();
	}
	
	public void testCreateActions()
	{
		final Set<IContributedAction> actions = controller.createActions();
		assertEquals("One action expected", 1, actions.size());
		assertTrue("Unexpected action", actions.iterator().next() instanceof SaveInFavoritesContributedAction);
	}
}
