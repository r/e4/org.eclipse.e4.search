/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.integration.test.unit.internal;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.platform.discovery.core.api.ISearchConsoleSlaveController;
import org.eclipse.platform.discovery.core.api.ISearchFavoritesSlaveController;
import org.eclipse.platform.discovery.integration.internal.plugin.ISlaveControllersConfiguration;
import org.eclipse.platform.discovery.integration.internal.slavecontrollers.SlaveControllersConfiguration;
import org.eclipse.platform.discovery.runtime.internal.xp.IContributionsReader;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;


public class SlaveControllersConfigurationTest extends MockObjectTestCase
{
	private Mock<IContributionsReader<ISearchConsoleSlaveController>> seXpParser;
	private Mock<IContributionsReader<ISearchFavoritesSlaveController>> sbXpParser;
	private ISlaveControllersConfiguration config;
	
	@Override
	protected void setUp() throws Exception
	{
		seXpParser = mock(IContributionsReader.class);
		sbXpParser = mock(IContributionsReader.class);
		config = new SlaveControllersConfiguration(){
			@Override
			protected IContributionsReader<ISearchConsoleSlaveController> seXpParser()
			{
				return seXpParser.proxy();
			}
			
			@Override
			protected IContributionsReader<ISearchFavoritesSlaveController> sbXpParser()
			{
				return sbXpParser.proxy();
			}
		};
	}
	
	public void testAvailableSeSlaveControllers()
	{
		final List<ISearchConsoleSlaveController> result = new ArrayList<ISearchConsoleSlaveController>();
		seXpParser.expects(once()).method("readContributions").will(returnValue(result));
		assertTrue("Unexpected result", config.availableSearchConsoleSlaveControllers() == result);
	}
	
	public void testAvailableSbSlaveControllers()
	{
		final List<ISearchFavoritesSlaveController> result = new ArrayList<ISearchFavoritesSlaveController>();
		sbXpParser.expects(once()).method("readContributions").will(returnValue(result));
		assertTrue("Unexpected result", config.availableSearchFavoritesSlaveControllers() == result);
	}
}
