/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.integration.test.unit.internal;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.platform.discovery.integration.internal.plugin.IViewCustomizationConfiguration;
import org.eclipse.platform.discovery.integration.internal.viewcustomization.ViewCustomizationConfiguration;
import org.eclipse.platform.discovery.runtime.internal.xp.IContributionsReader;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.ui.api.ISearchConsoleCustomization;
import org.eclipse.platform.discovery.ui.api.ISearchFavoritesViewCustomization;


public class ViewCustomizationConfigurationTest extends MockObjectTestCase
{
	private Mock<IContributionsReader<ISearchConsoleCustomization>> seXpParser;
	private Mock<IContributionsReader<ISearchFavoritesViewCustomization>> sbXpParser;
	private IViewCustomizationConfiguration config;
	
	@Override
	protected void setUp() throws Exception
	{
		seXpParser = mock(IContributionsReader.class);
		sbXpParser = mock(IContributionsReader.class);
		config = new ViewCustomizationConfiguration(){
			@Override
			protected IContributionsReader<ISearchConsoleCustomization> seXpParser()
			{
				return seXpParser.proxy();
			}
			
			@Override
			protected IContributionsReader<ISearchFavoritesViewCustomization> sbXpParser()
			{
				return sbXpParser.proxy();
			}
		};
	}
	
	public void testAvailableSearchConsoleCustomizations()
	{
		final List<ISearchConsoleCustomization> result = new ArrayList<ISearchConsoleCustomization>();
		seXpParser.expects(once()).method("readContributions").will(returnValue(result));
		assertTrue("Unexpected result", config.availableSearchConsoleCustomizations() == result);
	}
	
	public void testAvailableSearchFavoritesCustomizations()
	{
		final List<ISearchFavoritesViewCustomization> result = new ArrayList<ISearchFavoritesViewCustomization>();
		sbXpParser.expects(once()).method("readContributions").will(returnValue(result));
		assertTrue("Unexpected result", config.availableSearchFavoritesCustomizations() == result);
	}
}
