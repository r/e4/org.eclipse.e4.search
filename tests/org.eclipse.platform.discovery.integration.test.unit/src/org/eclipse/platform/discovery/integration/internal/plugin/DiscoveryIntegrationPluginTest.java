/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.integration.internal.plugin;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.core.api.ISearchConsoleSlaveController;
import org.eclipse.platform.discovery.core.api.ISearchFavoritesMasterController;
import org.eclipse.platform.discovery.core.api.ISearchFavoritesSlaveController;
import org.eclipse.platform.discovery.core.internal.IDiscoveryView;
import org.eclipse.platform.discovery.core.internal.ISearchConsoleView;
import org.eclipse.platform.discovery.core.internal.console.ISearchConsoleController;
import org.eclipse.platform.discovery.core.internal.console.ISearchConsoleControllerOutputView;
import org.eclipse.platform.discovery.core.internal.favorites.IPersistenceUtil;
import org.eclipse.platform.discovery.core.internal.favorites.ISearchFavoritesControllerOutputView;
import org.eclipse.platform.discovery.core.internal.favorites.SearchFavoritesPersistenceUtil;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.persistence.DestinationItemPair;
import org.eclipse.platform.discovery.runtime.internal.ISearchProviderConfiguration;
import org.eclipse.platform.discovery.runtime.internal.persistence.MementoContentManagerException;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.ui.api.IMasterDiscoveryView;
import org.eclipse.platform.discovery.ui.api.ISearchConsoleCustomization;
import org.eclipse.platform.discovery.ui.api.ISearchFavoritesViewCustomization;
import org.eclipse.platform.discovery.ui.api.IViewUiContext;
import org.eclipse.platform.discovery.ui.api.impl.DefaultSessionIds;
import org.eclipse.platform.discovery.ui.internal.view.ICustomizableView;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.WorkbenchException;
import org.jmock.core.Constraint;


public class DiscoveryIntegrationPluginTest extends MockObjectTestCase
{
	private Mock<IWorkbenchPartReference> wbPartReference;
	private Mock<ISearchConsoleView> consoleView;
	private Mock<ISearchConsoleControllerOutputView> consoleViewOutputController;
	private Mock<ISearchConsoleController> consoleController;
	private TestedIntegrationPlugin plugin;
	private Mock<ISlaveControllersConfiguration> slaveControllersConfig;
	private Mock<ISearchConsoleSlaveController> slaveController;
	private Mock<IContributedAction> contributedAction;
	private Mock<IViewCustomizationConfiguration> viewCustomizationsConfig;
	private Mock<ICustomizableView> customizableView;
	private Mock<ISearchConsoleCustomization> searchConsoleViewCustomization;
	private Mock<IMasterDiscoveryView> masterView;
	private Mock<ILongOperationRunner> operationRunner;
	private Mock<ISearchFavoritesControllerOutputView> favoritesControllerView;
	
	private Mock<ISearchFavoritesMasterController> searchFavoritesController;
	private Mock<IDiscoveryView<ISearchFavoritesControllerOutputView, ISearchFavoritesMasterController>> searchFavoritesView;
	private Mock<ISearchFavoritesSlaveController> searchFavoritesSlaveController;
	private Mock<ISearchFavoritesViewCustomization> searchFavoritesViewCustomization;
	
	@Override
	protected void setUp() throws Exception
	{
		wbPartReference = mock(IWorkbenchPartReference.class);
		
		consoleViewOutputController = mock(ISearchConsoleControllerOutputView.class);
		consoleView = mock(ISearchConsoleView.class);
		consoleView.stubs().method("getControllerView").will(returnValue(consoleViewOutputController.proxy()));
		
		consoleController = mock(ISearchConsoleController.class);
		contributedAction = mock(IContributedAction.class);
		customizableView = mock(ICustomizableView.class);
		masterView = mock(IMasterDiscoveryView.class);
		operationRunner = mock(ILongOperationRunner.class);
		
		searchFavoritesView = mock(IDiscoveryView.class);
		favoritesControllerView = mock(ISearchFavoritesControllerOutputView.class);
		searchFavoritesView.stubs().method("getControllerView").will(returnValue(favoritesControllerView.proxy()));
		
		searchFavoritesController = mock(ISearchFavoritesMasterController.class);
		
		setupSlaveControllers();
		setupViewCustomizations();
		
		
		plugin = new TestedIntegrationPlugin();
	}

	private void setupSlaveControllers()
	{
		slaveController = mock(ISearchConsoleSlaveController.class);
		slaveController.stubs().method("createActions").will(returnValue(new HashSet<IContributedAction>(Arrays.asList(new IContributedAction[] { contributedAction.proxy() }))));

		searchFavoritesSlaveController = mock(ISearchFavoritesSlaveController.class);
		searchFavoritesSlaveController.stubs().method("createActions").will(returnValue(new HashSet<IContributedAction>(Arrays.asList(new IContributedAction[] { contributedAction.proxy() }))));
		
		slaveControllersConfig = mock(ISlaveControllersConfiguration.class);
		slaveControllersConfig.expects(atMostOnce()).method("availableSearchConsoleSlaveControllers").will(returnValue(Arrays.asList(new ISearchConsoleSlaveController[] { slaveController.proxy() })));
		slaveControllersConfig.expects(atMostOnce()).method("availableSearchFavoritesSlaveControllers").will(returnValue(Arrays.asList(new ISearchFavoritesSlaveController[] { searchFavoritesSlaveController.proxy() })));
	}
	
	private void setupViewCustomizations()
	{
		searchConsoleViewCustomization = mock(ISearchConsoleCustomization.class);
		searchFavoritesViewCustomization = mock(ISearchFavoritesViewCustomization.class);
		
		viewCustomizationsConfig = mock(IViewCustomizationConfiguration.class);
		viewCustomizationsConfig.expects(atMostOnce()).method("availableSearchConsoleCustomizations").will(returnValue(Arrays.asList(new ISearchConsoleCustomization[]{searchConsoleViewCustomization.proxy()})));
		viewCustomizationsConfig.expects(atMostOnce()).method("availableSearchFavoritesCustomizations").will(returnValue((Arrays.asList(new ISearchFavoritesViewCustomization[]{searchFavoritesViewCustomization.proxy()}))));
	}

	public void testPartOpenedInternalWithUnknownView()
	{
		wbPartReference.stubs().method("getId").will(returnValue("myid"));
		plugin.partOpenedInternal(wbPartReference.proxy());
	}

	public void testSearchConsoleOpened()
	{
		wbPartReference.stubs().method("getId").will(returnValue(DiscoveryIntegrationPlugin.SEARCH_CONSOLE_VIEW_ID));
		wbPartReference.expects(once()).method("getPart").with(eq(false)).will(returnValue(mock(IWorkbenchPart.class).proxy()));

		customizableView.expects(once()).method("registerAction").with(eq(contributedAction.proxy()));
		consoleView.expects(once()).method("getProgressMonitor").will(returnValue(mock(IProgressMonitor.class).proxy()));
		consoleView.expects(once()).method("setEnvironment").with(isA(IDiscoveryEnvironment.class));
		consoleView.expects(once()).method("setDefaultSessionId").with(eq(DefaultSessionIds.mainSearchSessionId));
		consoleView.expects(once()).method("initializationCompleted");
		consoleView.expects(once()).method("registerController").with(same(consoleController.proxy()));
		
		customizableView.expects(once()).method("registerViewCustomization").with(eq(searchConsoleViewCustomization.proxy()));
		customizableView.expects(once()).method("setUiContext").with(isA(IViewUiContext.class));
		
		slaveController.expects(once()).method("setMasterController").with(eq(consoleController.proxy()));
		searchConsoleViewCustomization.expects(once()).method("setMasterView").with(eq(masterView.proxy()));
		consoleView.expects(once()).method("setDestinationsManager");
		
		plugin.partOpenedInternal(wbPartReference.proxy());
	}
	
	public void testSearchFavoritesOpened()
	{
		searchFavoritesView.expects(once()).method("setEnvironment").with(isA(IDiscoveryEnvironment.class));
		searchFavoritesView.expects(once()).method("registerController").with(same(searchFavoritesController.proxy()));
		searchFavoritesView.expects(once()).method("initializationCompleted");
		wbPartReference.stubs().method("getId").will(returnValue(DiscoveryIntegrationPlugin.SEARCH_FAVORITES_VIEW_ID));
		wbPartReference.expects(once()).method("getPart").with(eq(false)).will(returnValue(mock(IWorkbenchPart.class).proxy()));

		customizableView.expects(once()).method("registerAction").with(eq(contributedAction.proxy()));
		searchFavoritesView.expects(once()).method("getProgressMonitor").will(returnValue(mock(IProgressMonitor.class).proxy()));
		customizableView.expects(once()).method("registerViewCustomization").with(eq(searchFavoritesViewCustomization.proxy()));
		customizableView.expects(once()).method("setUiContext").with(isA(IViewUiContext.class));

		searchFavoritesSlaveController.expects(once()).method("setMasterController").with(eq(searchFavoritesController.proxy()));
		searchFavoritesViewCustomization.expects(once()).method("setMasterView").with(eq(masterView.proxy()));
		plugin.partOpenedInternal(wbPartReference.proxy());
	}
	
	public void testCreateFavoritesPersistenceUtilFavoritesViewNotOpened()
	{
		assertTrue("Unexpected util instance", plugin.createFavoritesPersistenceUtil() instanceof SearchFavoritesPersistenceUtil);
	}
	
	public void testCreateFavoritesPersistenceUtilFavoritesViewOpened() throws WorkbenchException, IOException, MementoContentManagerException
	{
		testSearchFavoritesOpened();
		final IPersistenceUtil util = plugin.createFavoritesPersistenceUtil();
		final Set<DestinationItemPair> pairs = new HashSet<DestinationItemPair>();
		final Mock<ISearchDestination> searchDestination = mock(ISearchDestination.class);
		final Object item = new Object(); 
		pairs.add(new DestinationItemPair(searchDestination.proxy(), item));
		
		searchFavoritesController.expects(once()).method("importData").with(arrayWithSingleElementContraint(pairs.iterator().next()));
		util.addItems(pairs, operationRunner.proxy());
		
		final Set<Object> itemsToDelete = new HashSet<Object>(Arrays.asList(new Object[]{item}));
		searchFavoritesController.expects(once()).method("deleteItems").with(eq(itemsToDelete));
		util.deleteItems(itemsToDelete, operationRunner.proxy());
	}
	
	private Constraint arrayWithSingleElementContraint(final Object element)
	{
		return new Constraint()
		{
			@Override
			public StringBuffer describeTo(StringBuffer buffer)
			{
				return buffer;
			}
			
			@Override
			public boolean eval(Object o)
			{
				if(!(o instanceof Object[]))
				{
					return false;
				}
				final Object[] arr = (Object[])o;
				return arr.length == 1 && arr[0].equals(element);
			}
		};
	}
	
	private class TestedIntegrationPlugin extends DiscoveryIntegrationPlugin
	{
		@Override
		protected ISearchConsoleController createSearchConsoleController(final ISearchConsoleControllerOutputView view, final ISearchProviderConfiguration searchProviderConfig, IDiscoveryEnvironment environment)
		{
			assertTrue("Unexpected console view", view == consoleViewOutputController.proxy());
			return consoleController.proxy();
		}
		
		@Override
		protected ISearchFavoritesMasterController createSearchFavoritesController(final ISearchFavoritesControllerOutputView view, final IDiscoveryEnvironment env)
		{
			assertTrue("Unexpected console view", view == favoritesControllerView.proxy());
			return searchFavoritesController.proxy();
		}
		
		@Override
		protected ISearchConsoleView getBusinessViewFromPart(IWorkbenchPart part)
		{
			return consoleView.proxy();
		}
		
		@Override
		protected IDiscoveryView<ISearchFavoritesControllerOutputView, ISearchFavoritesMasterController> getSearchFavoritesViewFromPart(IWorkbenchPart part)
		{
			return searchFavoritesView.proxy();
		}
		
		@Override
		protected ISlaveControllersConfiguration slaveControllersConfig()
		{
			return slaveControllersConfig.proxy();
		}
		
		@Override
		protected IViewCustomizationConfiguration viewCustomizationsConfig()
		{
			return viewCustomizationsConfig.proxy();
		}
		
		@Override
		protected ICustomizableView getCustomizableViewFromPart(IWorkbenchPart part)
		{
			return customizableView.proxy();
		}
		
		@Override
		protected IMasterDiscoveryView getMasterViewFromPart(IWorkbenchPart wbPart)
		{
			return masterView.proxy();
		}
		
		@Override
		protected ILongOperationRunner longOperationRunner(IProgressMonitor monitor)
		{
			return operationRunner.proxy();
		}
		
		@Override
		protected void partOpenedInternal(IWorkbenchPartReference partRef) 
		{
			super.partOpenedInternal(partRef);
		}
	}
}
