/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.integration.test.unit.internal;

import java.util.Arrays;
import java.util.HashSet;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.platform.discovery.core.api.ISearchContext;
import org.eclipse.platform.discovery.core.internal.ContextStructuredSelection;
import org.eclipse.platform.discovery.integration.internal.viewcustomization.IConfigurableContributedAction;
import org.eclipse.platform.discovery.integration.internal.viewcustomization.SaveInFavoritesMenuAction;
import org.eclipse.platform.discovery.runtime.api.ISearchParameters;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.ui.api.ISearchFavoritesViewCustomization;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;
import org.eclipse.platform.discovery.util.api.env.IErrorHandler;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;


public class SaveInFavoritesMenuActionTest extends MockObjectTestCase
{
	private SaveInFavoritesMenuAction action;
	private Mock<IConfigurableContributedAction> contributedAction;
	private IStructuredSelection selection;
	private Mock<ISearchContext> searchContext;
	private Mock<IDiscoveryEnvironment> environment;
	private Mock<ILongOperationRunner> operationRunner;
	private Mock<IErrorHandler> errorHandler;
	private Mock<ISearchParameters> searchParams;
	private Object objectToAdd;
	private Mock<ISearchFavoritesViewCustomization> sbCustomization;

	@Override
	protected void setUp() throws Exception
	{
		objectToAdd = new Object();

		searchParams = mock(ISearchParameters.class);
		searchContext = mock(ISearchContext.class);
		searchContext.stubs().method("searchParameters").will(returnValue(searchParams.proxy()));

		selection = new ContextStructuredSelection(Arrays.asList(new Object[] { objectToAdd }), searchContext.proxy());

		sbCustomization = mock(ISearchFavoritesViewCustomization.class);
		sbCustomization.stubs().method("itemsFor").with(eq(objectToAdd)).will(returnValue(new HashSet<Object>(Arrays.asList(new Object[] { objectToAdd }))));

		operationRunner = mock(ILongOperationRunner.class);
		errorHandler = mock(IErrorHandler.class);
		environment = mock(IDiscoveryEnvironment.class);
		environment.stubs().method("operationRunner").will(returnValue(operationRunner.proxy()));
		environment.stubs().method("errorHandler").will(returnValue(errorHandler.proxy()));

		contributedAction = mock(IConfigurableContributedAction.class);

		setupMenuAction();
	}

	private void setupMenuAction()
	{
		action = new SaveInFavoritesMenuAction(contributedAction.proxy(), selection, searchContext.proxy(), Arrays.asList(new ISearchFavoritesViewCustomization[] { sbCustomization.proxy() }), environment.proxy());
	}

	public void testIsAvailable()
	{
		assertTrue(action.isAvailable());
		sbCustomization.reset();
		sbCustomization.stubs().method("itemsFor").with(eq(objectToAdd)).will(returnValue(new HashSet<Object>()));
		assertFalse(action.isAvailable());
	}

	public void testAddItems()
	{
		contributedAction.expects(once()).method("setErrorHandler").with(eq(errorHandler.proxy()));
		contributedAction.expects(once()).method("setSearchContext").with(eq(searchContext.proxy()));
		contributedAction.expects(once()).method("perform").with(eq(operationRunner.proxy()), collectionContaining(objectToAdd));
		action.run();
	}
}
