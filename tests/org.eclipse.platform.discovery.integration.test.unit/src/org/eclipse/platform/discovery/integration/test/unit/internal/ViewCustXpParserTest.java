/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.integration.test.unit.internal;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.integration.internal.ViewCustXpParser;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.util.internal.StatusUtils;
import org.eclipse.platform.discovery.util.internal.logging.ILogger;


public class ViewCustXpParserTest extends MockObjectTestCase
{
	private static final String XP_ID = "org.eclipse.platform.discovery.integration.searchconsole"; //$NON-NLS-1$
	private static final String XP_ELEMENT_NAME = "slavecontroller"; //$NON-NLS-1$
	private static final String IMPL_ATTR = "impl";
	
	private Mock<ILogger> logger;
	private Mock<IExtensionRegistry> extRegistry;
	private Mock<IExtensionPoint> searchConsoleExtensionPoint;
	private ViewCustXpParser<Object> testParser;
	private Mock<IConfigurationElement> configElement;
	private Mock<IConfigurationElement> configElementWithExc;
	private Object instantiatableObject;
	private CoreException exceptionThrown;

	@Override
	protected void setUp() throws Exception
	{
		instantiatableObject = new Object();
		logger = mock(ILogger.class);
		setupConfigElements();
		setUpExtensionPoint();
		setUpExtensionRegistry();
		
		
		testParser = new ViewCustXpParser<Object>(extRegistry.proxy(), XP_ID, XP_ELEMENT_NAME)
		{
			@Override
			protected ILogger logger()
			{
				return logger.proxy();
			}


			@Override
			protected String implementationAttributeName()
			{
				return IMPL_ATTR;
			}

		};
		
	}

	private void setUpExtensionPoint() {
		searchConsoleExtensionPoint = mock(IExtensionPoint.class);
		searchConsoleExtensionPoint.stubs().method("getConfigurationElements").will(returnValue(new IConfigurationElement[]{configElement.proxy(),configElementWithExc.proxy()}));
	}

	private void setUpExtensionRegistry() {
		extRegistry = mock(IExtensionRegistry.class);
		extRegistry.stubs().method("getExtensionPoint").with(eq(XP_ID)).will(returnValue(searchConsoleExtensionPoint.proxy()));
	}

	private void setupConfigElements()
	{
		
		configElement = mock(IConfigurationElement.class);
		configElement.stubs().method("createExecutableExtension").with(eq(IMPL_ATTR)).will(returnValue(instantiatableObject));
		configElement.stubs().method("getName").will(returnValue(XP_ELEMENT_NAME));
		
		exceptionThrown = new CoreException(StatusUtils.statusError("TEST"));
		configElementWithExc = mock(IConfigurationElement.class);
		configElementWithExc.stubs().method("createExecutableExtension").with(eq(IMPL_ATTR)).will(throwException(exceptionThrown));
		configElementWithExc.stubs().method("getName").will(returnValue(XP_ELEMENT_NAME));

	}
	
	public void testReadSlaveControllers()
	{
		logger.expects(once()).method("logError").with(ANYTHING, eq(exceptionThrown));
		final List<Object> result = testParser.readContributions();
		assertEquals("One element expected", 1, result.size());
		assertTrue("Unexpected slave controller", result.iterator().next() == instantiatableObject);
	}
}
