/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.integration.test.unit;

import org.eclipse.platform.discovery.integration.internal.plugin.DiscoveryIntegrationPluginTest;
import org.eclipse.platform.discovery.integration.test.unit.internal.SaveInFavoritesContributedActionTest;
import org.eclipse.platform.discovery.integration.test.unit.internal.SaveInFavoritesMenuActionTest;
import org.eclipse.platform.discovery.integration.test.unit.internal.SaveInFavoritesSlaveControllerTest;
import org.eclipse.platform.discovery.integration.test.unit.internal.SaveInFavoritesViewCustomizationTest;
import org.eclipse.platform.discovery.integration.test.unit.internal.SlaveControllersConfigurationTest;
import org.eclipse.platform.discovery.integration.test.unit.internal.ViewCustXpParserTest;
import org.eclipse.platform.discovery.integration.test.unit.internal.ViewCustomizationConfigurationTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@SuiteClasses({
	ViewCustXpParserTest.class,
	SlaveControllersConfigurationTest.class,
	DiscoveryIntegrationPluginTest.class,
	ViewCustomizationConfigurationTest.class,
	SaveInFavoritesContributedActionTest.class,
	SaveInFavoritesViewCustomizationTest.class,
	SaveInFavoritesMenuActionTest.class,
	SaveInFavoritesSlaveControllerTest.class
})
@RunWith(Suite.class)
public class AllTestsSuite{}
