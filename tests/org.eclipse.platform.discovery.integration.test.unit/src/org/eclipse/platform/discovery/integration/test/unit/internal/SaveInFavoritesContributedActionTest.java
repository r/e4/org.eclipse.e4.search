/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.integration.test.unit.internal;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.platform.discovery.core.api.ISearchContext;
import org.eclipse.platform.discovery.core.internal.favorites.IPersistenceUtil;
import org.eclipse.platform.discovery.integration.internal.viewcustomization.SaveInFavoritesContributedAction;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.ISearchParameters;
import org.eclipse.platform.discovery.runtime.api.persistence.DestinationItemPair;
import org.eclipse.platform.discovery.runtime.internal.persistence.MementoContentManagerException;
import org.eclipse.platform.discovery.testutils.utils.jmock.Mock;
import org.eclipse.platform.discovery.testutils.utils.jmock.MockObjectTestCase;
import org.eclipse.platform.discovery.util.api.env.IErrorHandler;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.ui.WorkbenchException;


public class SaveInFavoritesContributedActionTest extends MockObjectTestCase
{
	private SaveInFavoritesContributedAction action;
	private Mock<ILongOperationRunner> operationRunner;
	private Set<Object> selectedObjects;
	private Mock<IPersistenceUtil> persistenceUtil;
	private Mock<ISearchParameters> searchParams;
	private Mock<IErrorHandler> errorHandler;
	private Mock<ISearchContext> searchContext;
	private Mock<ISearchDestination> searchDestination;

	@Override
	protected void setUp() throws Exception
	{
		operationRunner = mock(ILongOperationRunner.class);
		selectedObjects = new HashSet<Object>();
		persistenceUtil = mock(IPersistenceUtil.class);
		searchParams = mock(ISearchParameters.class);
		searchContext = mock(ISearchContext.class);
		searchDestination = mock(ISearchDestination.class);
		searchContext.stubs().method("searchParameters").will(returnValue(searchParams.proxy()));
		searchParams.stubs().method("getSearchDestination").will(returnValue(searchDestination.proxy()));
		errorHandler = mock(IErrorHandler.class);
		action = new SaveInFavoritesContributedAction()
		{
			@Override
			protected IPersistenceUtil createPersistenceUtil()
			{
				return persistenceUtil.proxy();
			}
		};
	}
	
	public void testExectuteActionWithNoSearchContext()
	{
		try
		{
			action.perform(operationRunner.proxy(), selectedObjects);
			fail("IllegalStateException not thrown");
		}
		catch(IllegalStateException e)
		{
			//expected
		}
	}
	
	public void testExectuteActionWithNoErrorHandler()
	{
		action.setSearchContext(searchContext.proxy());
		try
		{
			
			action.perform(operationRunner.proxy(), selectedObjects);
			fail("IllegalStateException not thrown");
		}
		catch(IllegalStateException e)
		{
			//expected
		}
	}
	
	public void testPerformAction()
	{
		selectedObjects.add("Test object");
		persistenceUtil.expects(once()).method("addItems").with(eq(toPairs(selectedObjects)), eq(operationRunner.proxy()));
		prepareAction();
		action.perform(operationRunner.proxy(), selectedObjects);
	}
	
	public void testPerformActionWithWbException()
	{
		performActionWithExceptionTest(new WorkbenchException("test"));
	}
	
	public void testPerformActionWithIOException()
	{
		performActionWithExceptionTest(new IOException());
	}
	
	public void testPerformActionWithMementoException()
	{
		performActionWithExceptionTest(new MementoContentManagerException());
	}
	
	public void testGetActionId()
	{
		assertEquals("Unexpected action id", "org.eclipse.platform.discovery.integration.internal.viewcustomization.SaveInFavoritesContributedAction", action.getActionId());
	}
	
	private void performActionWithExceptionTest(final Exception exc)
	{
		selectedObjects.add("Test object");
		persistenceUtil.expects(once()).method("addItems").with(eq(toPairs(selectedObjects)), eq(operationRunner.proxy())).will(throwException(exc));
		errorHandler.expects(once()).method("handleException").with(eq(exc));
		prepareAction();
		action.perform(operationRunner.proxy(), selectedObjects);
	}
	
	private void prepareAction()
	{
		action.setSearchContext(searchContext.proxy());
		action.setErrorHandler(errorHandler.proxy());
	}
	
	private Set<DestinationItemPair> toPairs(final Set<Object> objects)
	{
		final Set<DestinationItemPair> result = new HashSet<DestinationItemPair>();
		for(Object o : objects)
		{
			result.add(new DestinationItemPair(searchDestination.proxy(), o));
		}
		
		return result;
	}
}
