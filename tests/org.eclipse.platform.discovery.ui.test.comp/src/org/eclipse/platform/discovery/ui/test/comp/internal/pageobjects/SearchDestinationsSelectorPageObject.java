/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects;

import static org.eclipse.swtbot.swt.finder.finders.UIThreadRunnable.syncExec;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.platform.discovery.runtime.api.IDestinationsProvider;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.testutils.utils.pageobjects.InShellPageObject;
import org.eclipse.platform.discovery.ui.internal.selector.SearchDestinationsSelector;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.swtbot.swt.finder.results.Result;
import org.eclipse.swtbot.swt.finder.results.VoidResult;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTree;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTreeItem;
import org.eclipse.ui.forms.widgets.FormToolkit;

public class SearchDestinationsSelectorPageObject extends InShellPageObject
{
	private final List<ISearchDestination> destinationsToShow;
	private SearchDestinationsSelector selector;
	private final IDestinationsProvider destProvider;

	public SearchDestinationsSelectorPageObject(final List<ISearchDestination> destinationsToShow, final IDestinationsProvider destProvider)
	{
		this.destinationsToShow = destinationsToShow;
		this.destProvider = destProvider;
	}

	@Override
	protected void createContent(final Shell parent, final FormToolkit formToolkit)
	{
		selector = new SearchDestinationsSelector(parent, formToolkit, 60, null)
		{
			@Override
			protected Set<IDestinationsProvider> getDestinationProvidersForCategory(IDestinationCategoryDescription category)
			{
				return new HashSet<IDestinationsProvider>(Arrays.asList(new IDestinationsProvider[] { destProvider }));
			}

			@Override
			protected List<ISearchDestination> getSearchDestinations(IDestinationCategoryDescription category, IDestinationsProvider destinationsProvider)
			{
				return destinationsToShow;
			}

			@Override
			public void handleSelectionChange(ISearchDestination newSelection)
			{

			}
		};
	}

	public void setInput(final List<IDestinationCategoryDescription> input)
	{
		syncExec(new VoidResult()
		{
			@Override
			public void run()
			{
				selector.setInput(input);
			}
		});
	}

	private SWTBotTree destinationsTree()
	{
		return bot().tree();
	}

	public void selectFirstPossibleDestination()
	{
		destinationsTree().select(findFirstDestination());
	}

	private SWTBotTreeItem findFirstDestination()
	{
		for (SWTBotTreeItem treeItem : getAllVisibleItems())
		{
			if (isDestination(treeItem))
			{
				return treeItem;
			}
		}
		throw new IllegalStateException("Could not find destinations");
	}

	private boolean isDestination(final SWTBotTreeItem treeItem)
	{
		for (ISearchDestination dest : this.destinationsToShow)
		{
			if (dest.getDisplayName().equals(treeItem.getText()))
			{
				return true;
			}
		}
		return false;
	}

	public List<String> getAllItemsNames()
	{
		final List<String> result = new ArrayList<String>();
		for (SWTBotTreeItem item : getAllVisibleItems())
		{
			result.add(item.getText());
		}
		return result;
	}

	private List<SWTBotTreeItem> getAllVisibleItems()
	{
		return getItems(destinationsTree().getAllItems());
	}

	private List<SWTBotTreeItem> getItems(SWTBotTreeItem[] allItems)
	{
		final List<SWTBotTreeItem> result = new ArrayList<SWTBotTreeItem>();
		for (SWTBotTreeItem item : allItems)
		{
			result.add(item);
			result.addAll(getItems(item.getItems()));
		}
		return result;
	}

	public void update()
	{
		syncExec(new VoidResult()
		{
			@Override
			public void run()
			{
				selector.update();
			}
		});
	}

	public void selectDestination(final String destinationName)
	{
		for(SWTBotTreeItem item : getAllVisibleItems())
		{
			if(destinationName.equals(item.getText()) && isDestination(item))
			{
				item.select();
				return;
			}
		}
		throw new WidgetNotFoundException(MessageFormat.format("Could not find destination {0}", destinationName));
	}
	
	

	public ISearchDestination getSelectedDestination()
	{
		return syncExec(new Result<ISearchDestination>()
		{

			@Override
			public ISearchDestination run()
			{
				return selector.getSelectedItem();
			}
		});
	}
}
