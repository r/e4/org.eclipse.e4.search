/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects;

import org.eclipse.platform.discovery.testutils.utils.pageobjects.InShellPageObject;
import org.eclipse.platform.discovery.ui.api.ISearchParametersUI.IConsoleContext;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot.SWTBotSubdSelector;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swtbot.swt.finder.SWTBot;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotCheckBox;
import org.eclipse.ui.forms.widgets.FormToolkit;

public class SubdestinationsSelectorPageObject extends InShellPageObject
{
	private final IConsoleContext consoleContext;
	private SWTBotSubdSelector selector;

	public SubdestinationsSelectorPageObject(final IConsoleContext consoleContext)
	{
		this.consoleContext = consoleContext;
	}

	@Override
	protected void createContent(final Shell parent, final FormToolkit formToolkit)
	{
	}

	@Override
	protected Shell createShell()
	{
		selector = SWTBotSubdSelector.open(new SWTBot().activeShell(), consoleContext);
		return selector.widget;
	}

	public boolean isDisplayed(final String subdestinationName)
	{
		try
		{
			subdestinationCheckbox(subdestinationName);
			return true;
		}
		catch (WidgetNotFoundException e)
		{
			return false;
		}
	}

	public boolean isSelected(final String subdestinationName)
	{
		return subdestinationCheckbox(subdestinationName).isChecked();
	}

	private SWTBotCheckBox subdestinationCheckbox(final String subdestName)
	{
		return selector.subdestinationCheckbox(subdestName);
	}

	public void toggle(final String subdestinationName)
	{
		subdestinationCheckbox(subdestinationName).click();
	}
}
