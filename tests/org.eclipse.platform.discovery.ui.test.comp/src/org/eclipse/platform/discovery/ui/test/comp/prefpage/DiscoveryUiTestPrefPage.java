/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.prefpage;

import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;



public class DiscoveryUiTestPrefPage extends PreferencePage implements IWorkbenchPreferencePage{

	private Button button;
	private Label label;


	@Override
	protected Control createContents(Composite parent)
	{
		final Composite preferencesComposite = new Composite(parent, SWT.NONE);
		preferencesComposite.setLayout(new GridLayout(5, true));

		createPreferencesButtons(preferencesComposite);

		return preferencesComposite;
	}

	private void createPreferencesButtons(final Composite parent)
	{
		label = new Label(parent, SWT.NONE);
		label.setText("MyTestLabel"); //$NON-NLS-1$
		label.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false));
		

		button = new Button(parent, SWT.PUSH);
		button.setText("Button1"); //$NON-NLS-1$
		button.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false));
	}

	public void init(IWorkbench workbench) {
	}

}
