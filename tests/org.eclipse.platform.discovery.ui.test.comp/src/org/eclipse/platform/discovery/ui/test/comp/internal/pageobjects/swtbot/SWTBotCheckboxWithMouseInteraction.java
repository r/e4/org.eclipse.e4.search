/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot;

import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot.utils.MouseUtils;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.swtbot.swt.finder.utils.SWTUtils;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotCheckBox;

/**
 * Extender of {@link SWTBotCheckBox} which is used for checkboxes in a tooltip. In contrast to {@link SWTBotCheckBox} this implementation uses the
 * mouse to check the checkbox thus preventing the tooltip from closing
 * 
 * @author Danail Branekov
 * 
 */
class SWTBotCheckboxWithMouseInteraction extends SWTBotCheckBox
{
	private final MouseUtils mouseUtils;
	
	SWTBotCheckboxWithMouseInteraction(Button w) throws WidgetNotFoundException
	{
		super(w);
		this.mouseUtils = new MouseUtils(SWTUtils.display());
	}

	@Override
	public SWTBotCheckBox click()
	{
		this.toggle();
		return this;
	}

	@Override
	public void deselect()
	{
		if(this.isChecked())
		{
			this.click();
		}
	}

	@Override
	public void select()
	{
		if(this.isChecked())
		{
			return;
		}
		
		this.click();
	}

	@Override
	protected void toggle()
	{
		mouseUtils.clickOn(this);		
	}
}
