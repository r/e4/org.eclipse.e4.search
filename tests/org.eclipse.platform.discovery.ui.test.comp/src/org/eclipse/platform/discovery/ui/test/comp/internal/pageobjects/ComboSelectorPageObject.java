/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects;

import static org.eclipse.swtbot.swt.finder.finders.UIThreadRunnable.syncExec;

import java.util.Collection;
import java.util.List;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.platform.discovery.runtime.api.IDisplayableObject;
import org.eclipse.platform.discovery.testutils.utils.pageobjects.InShellPageObject;
import org.eclipse.platform.discovery.ui.internal.selector.ComboSelector;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swtbot.swt.finder.results.VoidResult;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotCombo;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotLabel;
import org.eclipse.ui.forms.widgets.FormToolkit;

public class ComboSelectorPageObject<T extends IDisplayableObject> extends InShellPageObject
{
	private static final String COMBO_LABEL = "ComboSelectorPageObject";
	private final List<T> comboInput;
	private ComboSelector<T> selector;

	public ComboSelectorPageObject(final List<T> comboInput)
	{
		this.comboInput = comboInput;
	}

	@Override
	protected void createContent(final Shell parent, final FormToolkit formToolkit)
	{
		selector = new ComboSelector<T>(parent, formToolkit, this.comboInput, COMBO_LABEL, 60, null);
	}

	public String[] getItems()
	{
		return combo().items();
	}

	public void select(final String item)
	{
		combo().setSelection(item);
	}

	private SWTBotCombo combo()
	{
		return bot().comboBox();
	}

	public void setInput(final Collection<T> input)
	{
		syncExec(new VoidResult()
		{
			@Override
			public void run()
			{
				selector.setInput(input);
			}
		});
	}

	public String getSelectedItem()
	{
		return combo().selection();
	}

	public void registerSelectionListener(final ISelectionChangedListener listener)
	{
		selector.registerSelectionChangedListener(listener);
	}

	public boolean isComboEnabled()
	{
		return combo().isEnabled();
	}

	public boolean isLabelEnabled()
	{
		return comboLabel().isEnabled();
	}

	private SWTBotLabel comboLabel()
	{
		return bot().label();
	}

	public void setEnabled(final boolean enabled)
	{
		syncExec(new VoidResult()
		{
			@Override
			public void run()
			{
				selector.setEnabled(enabled);
			}
		});
	}
}
