/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects;

import java.util.Arrays;
import java.util.HashSet;

import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.testutils.utils.pageobjects.InShellPageObject;
import org.eclipse.platform.discovery.ui.api.IGenericViewCustomization;
import org.eclipse.platform.discovery.ui.api.IResultsViewAccessor;
import org.eclipse.platform.discovery.ui.internal.view.result.impl.DiscoveryTreeViewerFactory;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot.SWTBotDiscoveryTree;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;
import org.eclipse.platform.discovery.util.internal.ContractChecker;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.forms.widgets.FormToolkit;

public class DiscoveredItemsInShellPageObject extends InShellPageObject
{
	private final IGenericViewCustomization viewCustomization;
	private final IDiscoveryEnvironment discoveryEnv;
	private SWTBotDiscoveryTree discoveryTree;

	public DiscoveredItemsInShellPageObject(final IDiscoveryEnvironment discoveryEnv, final IGenericViewCustomization viewCustomization)
	{
		this.viewCustomization = viewCustomization;
		this.discoveryEnv = discoveryEnv;
	}

	@Override
	protected void createContent(final Shell parent, FormToolkit formToolkit)
	{
		final DiscoveryTreeViewerFactory f = new DiscoveryTreeViewerFactory();
		final IResultsViewAccessor accessor = f.createTreeViewer(parent, new HashSet<IGenericViewCustomization>(Arrays.asList(viewCustomization)), new HashSet<IContributedAction>(), discoveryEnv);
		this.discoveryTree = new SWTBotDiscoveryTree(accessor);
	}

	public SWTBotDiscoveryTree tree()
	{
		ContractChecker.nullCheckField(this.discoveryTree, "treeViewer");
		return this.discoveryTree;
	}
}
