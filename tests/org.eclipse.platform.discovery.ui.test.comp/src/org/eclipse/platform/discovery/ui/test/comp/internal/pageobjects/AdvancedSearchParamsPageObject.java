package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects;

import static org.eclipse.swtbot.swt.finder.finders.UIThreadRunnable.syncExec;

import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.ISearchParameters;
import org.eclipse.platform.discovery.runtime.internal.ISearchProviderConfiguration;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.testutils.utils.pageobjects.InShellPageObject;
import org.eclipse.platform.discovery.ui.api.IAdvancedSearchParamsUiContributor;
import org.eclipse.platform.discovery.ui.api.IViewUiContext;
import org.eclipse.platform.discovery.ui.internal.search.advancedparams.AdvancedSearchParamsDisplayer;
import org.eclipse.platform.discovery.ui.internal.xp.IAdvancedSearchParamsUiContribXpParser;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot.SWTBotSection;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot.SwtBotUtils;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;
import org.eclipse.platform.discovery.util.internal.property.IPropertyAttributeListener;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.swtbot.swt.finder.results.BoolResult;
import org.eclipse.swtbot.swt.finder.results.Result;
import org.eclipse.swtbot.swt.finder.results.VoidResult;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.hamcrest.Matcher;

public class AdvancedSearchParamsPageObject<T extends Control> extends InShellPageObject
{
	private final ISearchProviderConfiguration searchProviderConfiguration;
	private final IAdvancedSearchParamsUiContribXpParser xpParser;
	private final SwtBotUtils botUtils;
	private final Matcher<T> widgetMatcher;
	private Composite parameterComposite;
	private AdvancedSearchParamsDisplayer displayer;

	public AdvancedSearchParamsPageObject(final ISearchProviderConfiguration searchProviderConfiguration, final IAdvancedSearchParamsUiContribXpParser xpParser, final Matcher<T> widgetMatcher)
	{
		this.searchProviderConfiguration = searchProviderConfiguration;
		this.xpParser = xpParser;
		botUtils = new SwtBotUtils();
		this.widgetMatcher = widgetMatcher;
	}

	@Override
	protected void createContent(final Shell parentShell, final FormToolkit formToolkit)
	{
		final Composite parentComposite = new Composite(parentShell, SWT.NONE);
		parentComposite.setLayout(new FillLayout());
		displayer = new AdvancedSearchParamsDisplayer(parentComposite, formToolkit)
		{
			@Override
			protected ISearchProviderConfiguration searchProviderConfiguration()
			{
				return searchProviderConfiguration;
			}

			@Override
			protected Composite createParametersContainingComposite(final Composite parent)
			{
				parameterComposite = super.createParametersContainingComposite(parent);
				return parameterComposite;
			}

			@Override
			protected IAdvancedSearchParamsUiContribXpParser createAdvancedSearchUiContribXpParser()
			{
				return xpParser;
			}
		};
	}

	public void registerSizePropertyChangeListener(IPropertyAttributeListener<Point> listener, boolean notifyCurrent)
	{
		displayer.registerSizePropertyChangeListener(listener, notifyCurrent);
	}

	public IAdvancedSearchParamsUiContributor getUiContributor()
	{
		return displayer.getUiContributor();
	}

	public void setSearchParameters(final ISearchParameters searchParams)
	{
		displayer.setParams(searchParams);
	}

	public void setInput(final IObjectTypeDescription objectType, final ISearchDestination destination, final IDiscoveryEnvironment environment, final IViewUiContext viewUiContext)
	{
		syncExec(new VoidResult()
		{

			@Override
			public void run()
			{
				displayer.update(objectType, destination, environment, viewUiContext);
			}
		});
	}

	public Composite getParametersComposite()
	{
		return this.parameterComposite;
	}

	public Composite getParametersCompositeParent()
	{
		return syncExec(new Result<Composite>()
		{

			@Override
			public Composite run()
			{
				return getParametersComposite().getParent();
			}
		});
	}

	public boolean isCustomUiAvailable()
	{
		try
		{
			final T customUi = botUtils.findOneChildControl(shell().widget, widgetMatcher, true);
			return syncExec(new BoolResult()
			{

				@Override
				public Boolean run()
				{
					return customUi.isVisible();
				}
			});
		}
		catch (WidgetNotFoundException e)
		{
			return false;
		}
	}

	public String getSectionTitle()
	{
		return findSection().getText();
	}

	public boolean isSectionVisible()
	{
		try
		{
			findSection();
			return true;
		}
		catch (WidgetNotFoundException e)
		{
			return false;
		}
	}

	public void hideCustomUi()
	{
		findSection().collapse();
	}

	public void showCustomUi()
	{
		findSection().expand();
	}

	public void enable()
	{
		setEnabled(true);
	}

	public void disable()
	{
		setEnabled(false);
	}

	public void setParams(final ISearchParameters searchParams)
	{
		displayer.setParams(searchParams);
	}

	private SWTBotSection findSection()
	{
		return new SWTBotSection(botUtils.findOneChildControlOfExactType(shell().widget, Section.class, true));
	}

	private void setEnabled(final boolean enabled)
	{
		syncExec(new VoidResult()
		{
			@Override
			public void run()
			{
				displayer.setEnabled(enabled);
			}
		});
	}
}
