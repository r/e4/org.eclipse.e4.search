/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot;

import java.util.Arrays;
import java.util.HashSet;

import org.eclipse.platform.discovery.ui.api.IResultsViewAccessor;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.swtbot.swt.finder.results.VoidResult;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTree;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTreeItem;

/**
 * Extension of {@link SWTBotTree} which provides additional features for the results tree in the search console
 * 
 * @author Danail Branekov
 * 
 */
public class SWTBotDiscoveryTree extends SWTBotTree
{
	private final IResultsViewAccessor accessor;

	public SWTBotDiscoveryTree(final IResultsViewAccessor accessor)
	{
		super(accessor.getTreeViewer().getTree());
		this.accessor = accessor;
	}

	public boolean isTooltipDisplayed(final String toolipHeadCaption)
	{
		try
		{
			final SWTBotTooltip tooltip = SWTBotTooltip.tooltip();
			tooltip.bot().label(toolipHeadCaption);
			return true;
		}
		catch (WidgetNotFoundException e)
		{
			return false;
		}
	}

	public boolean isTreeItemDisplayed(final String treeItemLabel)
	{
		return discoveredTreeItem(treeItemLabel) != null;
	}

	public SWTBotDiscoveredTreeItem discoveredTreeItem(final String tiLabel)
	{
		for (SWTBotTreeItem ti : this.getAllItems())
		{
			if (ti.getText().equals(tiLabel))
			{
				return new SWTBotDiscoveredTreeItem(ti.widget);
			}
		}
		return null;
	}

	public void setInput(final Object... items)
	{
		syncExec(new VoidResult()
		{
			@Override
			public void run()
			{
				accessor.getTreeViewer().setInput(new HashSet<Object>(Arrays.asList(items)));
			}
		});
	}
}
