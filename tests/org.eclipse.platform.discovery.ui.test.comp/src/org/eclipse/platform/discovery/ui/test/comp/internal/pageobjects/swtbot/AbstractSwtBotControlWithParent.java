package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.swtbot.swt.finder.results.Result;
import org.eclipse.swtbot.swt.finder.widgets.AbstractSWTBotControl;

public class AbstractSwtBotControlWithParent<T extends Control> extends AbstractSWTBotControl<T>{

	public AbstractSwtBotControlWithParent(T w) throws WidgetNotFoundException {
		super(w);
	}
	
	public AbstractSwtBotControlWithParent<Composite> getParent() {
		Composite composite = syncExec(new Result<Composite>() {
			@Override
			public Composite run() {
				return widget.getParent();
			}
		});
		return new AbstractSwtBotControlWithParent<Composite>(composite);
	}
	

}
