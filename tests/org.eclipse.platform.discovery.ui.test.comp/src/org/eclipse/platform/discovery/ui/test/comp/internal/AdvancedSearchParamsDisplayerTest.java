/*******************************************************************************
 * Copyright (c) 2010, 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import junit.framework.Assert;

import org.eclipse.platform.discovery.runtime.api.GroupingHierarchy;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.ISearchParameters;
import org.eclipse.platform.discovery.runtime.api.ISearchSubdestination;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.ui.api.IAdvancedSearchParamsUiContributor;
import org.eclipse.platform.discovery.ui.api.IViewUiContext;
import org.eclipse.platform.discovery.ui.internal.plugin.DiscoveryUIMessages;
import org.eclipse.platform.discovery.ui.internal.search.advancedparams.IAdvancedSearchParamsUiContributorDescr;
import org.eclipse.platform.discovery.ui.internal.view.impl.SearchParameters;
import org.eclipse.platform.discovery.ui.internal.xp.IAdvancedSearchParamsUiContribXpParser;
import org.eclipse.platform.discovery.ui.test.comp.internal.fixture.SearchConsoleTestFixture;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.AdvancedSearchParamsPageObject;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;
import org.eclipse.platform.discovery.util.internal.property.IPropertyAttributeListener;
import org.eclipse.platform.discovery.util.internal.property.PropertyAttributeChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class AdvancedSearchParamsDisplayerTest
{
	private final static String TEST_BUTTON_TEXT = "Custom B"; //$NON-NLS-1$
	private static final Object CUSTOM_SEARCH_PARAM_ID_1 = new Object();
	private static final Object CUSTOM_SEARCH_PARAM_ID_2 = new Object();
	private static final Object CUSTOM_SEARCH_PARAM_VALUE_1 = new Object();
	private static final Object CUSTOM_SEARCH_PARAM_VALUE_2 = new Object();

	private static SearchConsoleTestFixture fixture;
	private MyUiContributor searchParamsUiContributor;
	private AdvancedSearchParamsPageObject<Button> paramsPageObject;

	@Mock
	private IAdvancedSearchParamsUiContributorDescr searchParamsUiContribDescr;

	@Mock
	private IAdvancedSearchParamsUiContribXpParser xpParser;

	@BeforeClass
	public static void initFixture()
	{
		fixture = new SearchConsoleTestFixture();
	}
	
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		setupUiContributors();
		setupXpParser();

		paramsPageObject = new AdvancedSearchParamsPageObject<Button>(fixture.searchProviderConfiguration, xpParser, searchParamsUiContributor.buttonMatcher());
		paramsPageObject.open();
	}

	@After
	public void tearDown()
	{
		paramsPageObject.close();
	}

	private void setupUiContributors()
	{
		searchParamsUiContributor = new MyUiContributor();

		final String searchProviderId = fixture.unsupportingTextSearchProviderDescription.getId();
		Mockito.stub(searchParamsUiContribDescr.getSearchProviderId()).toReturn(searchProviderId);
		Mockito.stub(searchParamsUiContribDescr.createContributor()).toReturn(searchParamsUiContributor);
	}

	private void setupXpParser()
	{
		Mockito.stub(xpParser.readContributions()).toReturn(Arrays.asList(new IAdvancedSearchParamsUiContributorDescr[] { searchParamsUiContribDescr }));
	}

	@Test
	public void testUiIsCreatedOnUpdatingDisplayer()
	{
		setInput(fixture.objectType1, fixture.destination1);
		Assert.assertSame("Unexpected UI contributor", searchParamsUiContributor, paramsPageObject.getUiContributor()); //$NON-NLS-1$
		Assert.assertEquals("One UI creation expected", 1, searchParamsUiContributor.createdUiCount); //$NON-NLS-1$
	}

	@Test
	public void testOldSearchParametersCompositeIsDisposedOnUpdate()
	{
		setInput(fixture.objectType1, fixture.destination1);
		final Composite firstComposite = paramsPageObject.getParametersComposite();
		setInput(fixture.objectType1, fixture.destination1);
		final Composite secondComposite = paramsPageObject.getParametersComposite();
		Assert.assertEquals("Two UI creations expected", 2, searchParamsUiContributor.createdUiCount); //$NON-NLS-1$

		Assert.assertNotNull(firstComposite);
		Assert.assertNotNull(secondComposite);
		Assert.assertTrue("First params composite was not disposed", firstComposite.isDisposed()); //$NON-NLS-1$
		Assert.assertTrue("Params composite was not recreated", firstComposite != secondComposite); //$NON-NLS-1$
	}

	@Test
	public void testSectionIsDisposedNoDestinationSpecified()
	{
		setInput(fixture.objectType1, fixture.destination1);
		final Composite initialParametersComposite = paramsPageObject.getParametersComposite();
		final Composite initialParametersCompositeParent = paramsPageObject.getParametersCompositeParent();
		setInput(fixture.objectType1, null);

		Assert.assertNull("No contributor expected when no UI displayed", paramsPageObject.getUiContributor()); //$NON-NLS-1$
		Assert.assertFalse("Section unexpectedly displayed", paramsPageObject.isSectionVisible());
		Assert.assertTrue("Containing composite is not disposed", initialParametersComposite.isDisposed()); //$NON-NLS-1$
		Assert.assertTrue("Section is not disposed", initialParametersCompositeParent.isDisposed()); //$NON-NLS-1$
	}

	@Test
	public void testCustomUIIsDisplayedByDefault()
	{
		setInput(fixture.objectType1, fixture.destination1);
		Assert.assertTrue("Custom UI not available", paramsPageObject.isCustomUiAvailable());
	}

	@Test
	public void testCustomUIHidesWhenNoCustomUi()
	{
		setInput(fixture.objectType1, fixture.destination1);
		setInput(fixture.objectType2, fixture.destination3);
		Assert.assertFalse("Custom UI unexpectedly available", paramsPageObject.isCustomUiAvailable());
	}

	@Test
	public void testSectionTitleChanges()
	{
		setInput(fixture.objectType1, fixture.destination1);
		Assert.assertEquals("Unexpected section title", DiscoveryUIMessages.CUSTOM_PARAMS_SECTION_EXPAND_TITLE, paramsPageObject.getSectionTitle());
		paramsPageObject.showCustomUi();
		Assert.assertEquals("Unexpected section title", DiscoveryUIMessages.CUSTOM_PARAMS_SECTION_COLAPSE_TITLE, paramsPageObject.getSectionTitle());
		paramsPageObject.hideCustomUi();
		Assert.assertEquals("Unexpected section title", DiscoveryUIMessages.CUSTOM_PARAMS_SECTION_EXPAND_TITLE, paramsPageObject.getSectionTitle());
	}

	@Test
	public void testVisibilityIsHandledByCustomUI()
	{
		setInput(fixture.objectType1, fixture.destination1);
		Assert.assertFalse("Custom UI shown right after setting input", searchParamsUiContributor.visible);

		paramsPageObject.showCustomUi();
		Assert.assertTrue("Custom UI is not shown", searchParamsUiContributor.visible);

		paramsPageObject.hideCustomUi();
		Assert.assertFalse("Custom UI is still visible", searchParamsUiContributor.visible);
	}

	@Test
	public void testSetEnabled()
	{
		setInput(fixture.objectType1, fixture.destination1);
		paramsPageObject.showCustomUi();

		paramsPageObject.disable();
		Assert.assertFalse("Section should be not available when displayer disabled", paramsPageObject.isSectionVisible());

		paramsPageObject.enable();
		Assert.assertTrue("Section should be visible when displayer enabled", paramsPageObject.isSectionVisible());
	}

	@Test
	public void testUpdateSearchParametersWithCustomParams()
	{
		setInput(fixture.objectType1, fixture.destination1);
		final ISearchDestination searchDestination = Mockito.mock(ISearchDestination.class);
		final ISearchParameters searchParams = new SearchParameters("a", searchDestination, "A", new GroupingHierarchy("A", "A"), new HashSet<ISearchSubdestination>()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		paramsPageObject.setParams(searchParams);

		final Map<Object, Object> customParams = searchParams.getCustomParameters();
		Assert.assertEquals("Two custom parameters expected", 2, customParams.size()); //$NON-NLS-1$
		Assert.assertTrue("Unexpected custom parameter", customParams.get(CUSTOM_SEARCH_PARAM_ID_1) == CUSTOM_SEARCH_PARAM_VALUE_1); //$NON-NLS-1$
		Assert.assertTrue("Unexpected custom parameter", customParams.get(CUSTOM_SEARCH_PARAM_ID_2) == CUSTOM_SEARCH_PARAM_VALUE_2); //$NON-NLS-1$
	}

	@Test
	public void testSectionExapndNotifiesResizeListener()
	{
		final Point oldSectionSize[] = new Point[1];
		final Point newSectionSize[] = new Point[1];

		setInput(fixture.objectType1, fixture.destination1);

		paramsPageObject.registerSizePropertyChangeListener(new IPropertyAttributeListener<Point>()
		{
			@Override
			public void attributeChanged(final PropertyAttributeChangedEvent<Point> event)
			{
				oldSectionSize[0] = event.getOldAttribute();
				newSectionSize[0] = event.getNewAttribute();
			}
		}, true);

		Assert.assertNotNull("Listener not notified upon registration", newSectionSize[0]);
		Assert.assertTrue("Listener not notified upon registration", newSectionSize[0].y > 0);

		paramsPageObject.showCustomUi();
		Assert.assertNotNull("Listener not notified upon expanding the section", oldSectionSize[0]);
		Assert.assertTrue("Listener not notified upon expanding the section", newSectionSize[0].y > oldSectionSize[0].y);

		paramsPageObject.hideCustomUi();
		Assert.assertTrue("Listener not notified upon collapsing the section", newSectionSize[0].y < oldSectionSize[0].y);
	}

	private void setInput(final IObjectTypeDescription objectType, final ISearchDestination destination)
	{
		paramsPageObject.setInput(objectType, destination, fixture.environment, fixture.viewUiContext);
	}

	private class MyUiContributor implements IAdvancedSearchParamsUiContributor
	{
		public int createdUiCount = 0;
		public boolean visible = false;

		public void createUi(Composite parent, ISearchDestination searchDestination, FormToolkit formToolkit, IDiscoveryEnvironment env, final IViewUiContext uiContext)
		{
			createdUiCount++;
			final Button b = formToolkit.createButton(parent, TEST_BUTTON_TEXT, SWT.PUSH);
			b.setLayoutData(fillingFormData());
		}

		@Override
		public Map<Object, Object> getParameters()
		{
			final Map<Object, Object> result = new HashMap<Object, Object>();
			result.put(CUSTOM_SEARCH_PARAM_ID_1, CUSTOM_SEARCH_PARAM_VALUE_1);
			result.put(CUSTOM_SEARCH_PARAM_ID_2, CUSTOM_SEARCH_PARAM_VALUE_2);
			return result;
		}

		@Override
		public void handleVisibilityChange(boolean visible)
		{
			this.visible = visible;
		}

		@Override
		public void setEnabled(boolean enable)
		{
		}

		private FormData fillingFormData()
		{
			final FormData fd = new FormData();
			fd.top = new FormAttachment(0, 0);
			fd.left = new FormAttachment(0, 0);
			fd.right = new FormAttachment(100, 0);

			return fd;
		}

		Matcher<Button> buttonMatcher()
		{
			return new BaseMatcher<Button>()
			{
				@Override
				public boolean matches(Object item)
				{
					if (item instanceof Button)
					{
						final String buttonText = ((Button) item).getText();
						return TEST_BUTTON_TEXT.equals(buttonText);
					}
					return false;
				}

				@Override
				public void describeTo(Description description)
				{
				}
			};
		}
	}
}
