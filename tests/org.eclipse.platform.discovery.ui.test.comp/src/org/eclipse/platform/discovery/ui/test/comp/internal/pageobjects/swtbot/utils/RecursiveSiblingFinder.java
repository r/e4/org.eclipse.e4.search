/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot.utils;

import java.util.Arrays;
import java.util.LinkedList;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.swtbot.swt.finder.results.ArrayResult;

public class RecursiveSiblingFinder implements ArrayResult<Widget>
{
	private final Control seedControl;

	public RecursiveSiblingFinder(final Control control)
	{
		this.seedControl = control;
	}

	@Override
	public Widget[] run()
	{
		return children(seedControl.getParent());
	}

	private Widget[] children(final Composite parent)
	{
		final LinkedList<Widget> result = new LinkedList<Widget>();
		for (Widget w : parent.getChildren())
		{
			result.add(w);
			if (w instanceof Composite)
			{
				result.addAll(Arrays.asList(children((Composite) w)));
			}
		}
		return result.toArray(new Widget[result.size()]);
	}

}
