package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects;

import org.eclipse.platform.discovery.testutils.utils.pageobjects.InShellPageObject;
import org.eclipse.platform.discovery.ui.internal.SlidingComposite;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot.SWTBotSash;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot.SwtBotCanvas;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot.SwtBotUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.forms.widgets.FormToolkit;

public class SlidingCompositePageObject extends InShellPageObject{

	protected final String UPPER_LABEL = "UpperLabel";
	protected final String BOTTOM_LABEL = "BottomLabel";
	protected final String TEST_SHELL = "TEST SHELL";

	private final SwtBotUtils botUtils;
	
	private SlidingComposite slidingComposite;
	
	private boolean upperPartVisible;
	
	public SlidingCompositePageObject() {
		botUtils = new SwtBotUtils();
	}
	
	/**
	 * Represents the 'Hidden' state of the sliding composite, i.e. whether the 'First' child composite is hidden.
	 * In case of vertical orientation, the first composite is the left composite.
	 * In case of horizontal orientation, the first composite is the upper composite.
	 */
	public enum HiddenState{HIDDEN, NOT_HIDDEN}
	
	@Override
	protected void createContent(Shell parent, final FormToolkit formToolkit)
	{
		upperPartVisible = true;
		final Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new FillLayout(SWT.HORIZONTAL));
		composite.setSize(parent.getClientArea().width, parent.getClientArea().height);
		slidingComposite = createSlidingComposite(formToolkit, composite);
	}
	
	private SlidingComposite createSlidingComposite(FormToolkit formToolkit, final Composite parent) {
		return new SlidingComposite(parent, formToolkit)
		{
			@Override
  			protected void configureBottomComposite(Composite bottomComposite)
			{
				getFormToolkit().createLabel(bottomComposite, BOTTOM_LABEL);
			}

			@Override
			protected void configureUpperComposite(Composite upperComposite)
			{
				getFormToolkit().createLabel(upperComposite, UPPER_LABEL);
			}

			@Override
			protected void switchUpperVisibilityOff()
			{
				super.switchUpperVisibilityOff();
				upperPartVisible = false;
			}
			
			@Override
			protected void switchUpperVisibilityOn()
			{
				super.switchUpperVisibilityOn();
				upperPartVisible = true;
			}

			@Override
			protected void onOrientationChange(ORIENTATION newOrientation)
			{
			}
		};
	}
	
	/** Get the current sash orientation */
	public SashOrientation getSashOrientation() {
		return getSash().getOrientation();
	}
	
	private SWTBotSash getSash() {
		return SWTBotSash.sash(shell(), false);
	}
	
	/** Resize the test window/sliding composite so it has the specified width and height*/ 
	public void resize(int width, int height) {
		botUtils.setSize(shell(), width, height);
	}

	
	/** Moves the sash by the specified offset in pixels.
	 * If the composite is wide and therefore the current orientation is vertical, it will move the sash left/right by the specified offset.
	 * Otherwise, it will move the sash up/down by the specified offset.
	 */
	public void moveSashBy(int offset) {
		getSash().move(offset);
	};
	
	/**
	 * Get the sash offset in relation to the origin of the composite 
	 * If the composite is wide and therefore the current orientation is vertical, it will return the offset in relation to the left side of the composite.
	 * Otherwise, it will return the offset in relation to the upper side of the composite.
	 * @return a number between 0 and the size of the composite.
	 */
	public int getSashOffset() {
		return getSash().getOffset();
	}
	
	/**
	 * Toggles the 'hidden' state. If the SlidingComposite is currently not hidden, this will hide the sash as well as the first composite.
	 * (In case of vertical orientation this is the left composite. Otherwise it is the upper composite.)
	 * If the Sliding Composite is currently hidden, this will reveal the sash and the first composite.
	 */
	public void toggleHidden() {
		getHideButton().click();
	}
	
	private SwtBotCanvas getHideButton() {
		return SwtBotCanvas.canvas(shell().widget, true);
	}
	
	public HiddenState getHiddenState() {
		/*
		 * SWT getVisible() and isVisible() methods return true for the label and the first composite, even though they are
		 * in a hidden state, and really invisible to the user. This is because in the implementation we do not actually set
		 * the composite to isVisible=false
		 * However the sash.isVisible method behaves as expected.
		 * Thus we consider the SlidingComposite to be in the revealed state if both
		 * - sash.isVisible() returns true;
		 * - switchUpperVisibilityOn() was called.
		 */
		if((getSash().isVisible() && upperPartVisible))
		{
			return HiddenState.NOT_HIDDEN;
		}
		
		return HiddenState.HIDDEN;
	}

	public boolean isHideControlVisible() {
		return getHideButton().isVisible();
	}

	public void cancelCurrentOperation() {
		slidingComposite.getProgressMonitor().setCanceled(true);
	}
}
