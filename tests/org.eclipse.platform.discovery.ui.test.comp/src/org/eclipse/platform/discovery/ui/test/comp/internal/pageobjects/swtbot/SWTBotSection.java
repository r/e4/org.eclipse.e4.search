/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot;

import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.swtbot.swt.finder.finders.ChildrenControlFinder;
import org.eclipse.swtbot.swt.finder.matchers.WidgetOfType;
import org.eclipse.swtbot.swt.finder.results.BoolResult;
import org.eclipse.swtbot.swt.finder.widgets.AbstractSWTBotControl;
import org.eclipse.ui.forms.widgets.AbstractHyperlink;
import org.eclipse.ui.forms.widgets.Section;

/**
 * SWTBot for {@link Section}. The implementation expands and collapses the section via mouse interaction. The reason for this is that calling
 * {@link Section#setExpanded(boolean)} does broadcast selection events. This is important for the search console since the section title is expected
 * to change when the section expansion state changes
 * 
 * @author Danail Branekov
 * 
 */
public class SWTBotSection extends AbstractSWTBotControl<Section>
{
	public SWTBotSection(Section w) throws WidgetNotFoundException
	{
		super(w);
	}

	public void toggle()
	{
		AbstractHyperlink hyperlink = new ChildrenControlFinder(widget).findControls(WidgetOfType.widgetOfType(AbstractHyperlink.class)).get(0);
		new SWTBotHyperlink(hyperlink).click();
	}

	private boolean isExpanded()
	{
		return syncExec(new BoolResult()
		{
			@Override
			public Boolean run()
			{
				return widget.isExpanded();
			}
		});
	}

	public void expand()
	{
		if (isExpanded())
		{
			return;
		}
		toggle();
	}

	public void collapse()
	{
		if (!isExpanded())
		{
			return;
		}
		toggle();
	}
}
