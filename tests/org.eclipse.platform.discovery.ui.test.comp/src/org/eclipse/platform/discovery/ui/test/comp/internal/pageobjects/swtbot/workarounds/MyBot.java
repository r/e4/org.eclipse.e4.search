package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot.workarounds;

import org.eclipse.swt.widgets.Widget;
import org.eclipse.swtbot.swt.finder.SWTBot;
import org.eclipse.swtbot.swt.finder.finders.ChildrenControlFinder;
import org.eclipse.swtbot.swt.finder.finders.MenuFinder;
import org.eclipse.swtbot.swt.finder.resolvers.DefaultParentResolver;

public class MyBot extends SWTBot {
	
	public MyBot(Widget parent) {
		super(new ChildrenControlFinder(parent, new MyChildrentResolver(), new DefaultParentResolver()), new MenuFinder());
	}
}
