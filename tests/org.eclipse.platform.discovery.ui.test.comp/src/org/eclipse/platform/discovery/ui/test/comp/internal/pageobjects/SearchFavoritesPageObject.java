package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects;

import static org.eclipse.swtbot.swt.finder.finders.UIThreadRunnable.syncExec;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.platform.discovery.core.api.ISearchContext;
import org.eclipse.platform.discovery.core.api.ISearchFavoritesMasterController;
import org.eclipse.platform.discovery.core.internal.ContextStructuredSelection;
import org.eclipse.platform.discovery.core.internal.IContextStructuredSelection;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.ISearchParameters;
import org.eclipse.platform.discovery.testutils.utils.pageobjects.InShellPageObject;
import org.eclipse.platform.discovery.ui.api.ISearchFavoritesViewCustomization;
import org.eclipse.platform.discovery.ui.internal.dnd.LocalContextSelectionTransfer;
import org.eclipse.platform.discovery.ui.internal.view.SearchFavoritesView;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot.SWTBotToolItem;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot.SwtBotUtils;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot.utils.DndUtil;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DropTargetListener;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.swtbot.swt.finder.results.Result;
import org.eclipse.swtbot.swt.finder.results.VoidResult;
import org.eclipse.swtbot.swt.finder.utils.SWTUtils;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTree;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTreeItem;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.mockito.Mockito;

public class SearchFavoritesPageObject extends InShellPageObject
{
	private final static String FAVORITES_LABEL = "This is the favorites view";
	private static final String DRAG_SOURCE_TREE = "Helper DND tree";

	private final ISearchFavoritesViewCustomization viewCustomization;
	private final IDiscoveryEnvironment env;
	private SearchFavoritesView favoritesView;
	private final SwtBotUtils botUtils;
	private TreeViewer dragHelperTreeViewer;
	private final DndUtil dndUtil;
	private final ITreeContentProvider dragHelperContentProvider;

	public SearchFavoritesPageObject(final ISearchFavoritesViewCustomization viewCustomization, final ITreeContentProvider dragHelperContentProvider, final IDiscoveryEnvironment env)
	{
		this.viewCustomization = viewCustomization;
		this.dragHelperContentProvider = dragHelperContentProvider;
		this.env = env;
		botUtils = new SwtBotUtils();
		dndUtil = new DndUtil(display());
	}

	@Override
	protected void createContent(final Shell parent, final FormToolkit formToolkit)
	{
		final Composite favoritesComposite = new Composite(parent, SWT.BORDER);
		favoritesComposite.setLayout(new FillLayout(SWT.VERTICAL));
		final Label l = new Label(favoritesComposite, SWT.NONE);
		l.setText(FAVORITES_LABEL);

		final IWorkbenchPartSite wbPartSite = Mockito.mock(IWorkbenchPartSite.class);
		final IViewSite viewSite = Mockito.mock(IViewSite.class);
		final IActionBars actionBars = Mockito.mock(IActionBars.class);
		Mockito.stub(actionBars.getToolBarManager()).toReturn(null);
		Mockito.stub(viewSite.getActionBars()).toReturn(actionBars);

		favoritesView = new SearchFavoritesView()
		{
			@Override
			public IWorkbenchPartSite getSite()
			{
				return wbPartSite;
			}

			@Override
			public IViewSite getViewSite()
			{
				return viewSite;
			}
		};
		favoritesView.registerViewCustomization(viewCustomization);
		favoritesView.createPartControl(favoritesComposite);
		favoritesView.setEnvironment(env);

		final Composite helperComposite = new Composite(parent, SWT.BORDER);
		helperComposite.setLayout(new FillLayout(SWT.VERTICAL));
		dragHelperTreeViewer = createDragHelperViewer(helperComposite);
		parent.layout(true, true);
	}

	private TreeViewer createDragHelperViewer(final Composite parent)
	{
		final Label l = new Label(parent, SWT.NONE);
		l.setText(DRAG_SOURCE_TREE);

		final TreeViewer viewer = new TreeViewer(parent, SWT.BORDER);
		viewer.getControl().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		viewer.setContentProvider(dragHelperContentProvider);
		viewer.setLabelProvider(viewCustomization.getLabelProvider());
		viewer.addDragSupport(DND.DROP_COPY | DND.DROP_MOVE, new Transfer[] { LocalContextSelectionTransfer.getTransfer() }, new DragSourceAdapter()
		{
			@Override
			public void dragSetData(DragSourceEvent event)
			{
				if (LocalContextSelectionTransfer.getTransfer().isSupportedType(event.dataType))
				{
					final IContextStructuredSelection selection = structuredSelection();
					LocalContextSelectionTransfer.getTransfer().setSelection(selection);
					event.data = selection;
				}
			}

			@Override
			public void dragStart(DragSourceEvent event)
			{
				event.doit = !viewer.getSelection().isEmpty();
				if (event.doit)
				{
					LocalContextSelectionTransfer.getTransfer().setSelection(structuredSelection());
					LocalSelectionTransfer.getTransfer().setSelection(structuredSelection());
				}
			}

			@SuppressWarnings("unchecked")
			private IContextStructuredSelection structuredSelection()
			{
				final Iterator<Object> it = ((IStructuredSelection) viewer.getSelection()).iterator();
				final List<Object> selectedObjects = new ArrayList<Object>();
				while (it.hasNext())
				{
					selectedObjects.add(it.next());
				}
				final ISearchDestination destination = Mockito.mock(ISearchDestination.class);

				final ISearchParameters parameters = Mockito.mock(ISearchParameters.class);
				Mockito.stub(parameters.getSearchDestination()).toReturn(destination);

				final ISearchContext ctx = Mockito.mock(ISearchContext.class);
				Mockito.stub(ctx.searchParameters()).toReturn(parameters);
				return new ContextStructuredSelection(selectedObjects, ctx);
			}
		});
		viewer.setInput(new Object[0]);
		viewer.refresh();

		return viewer;
	}

	public boolean canCancelRunningOperation()
	{
		if (!isOperationRunning())
		{
			return false;
		}

		return findCancelButton().isEnabled();
	}

	public boolean isOperationRunning()
	{
		// The progress bar appears after 500ms of work (check org.eclipse.jface.action.StatusLine.beginTask(String, int) implementation for details).
		// This is why the delay here is needed
		SWTUtils.sleep(1000);

		try
		{
			findCancelButton();
			return true;
		}
		catch (WidgetNotFoundException e)
		{
			return false;
		}
	}

	private SWTBotToolItem findCancelButton()
	{
		return new SWTBotToolItem(botUtils.findOneChildControlOfExactType(shell().widget, ToolItem.class, true));
	}

	public void completeInitialization()
	{
		syncExec(new VoidResult()
		{
			@Override
			public void run()
			{
				favoritesView.initializationCompleted();
			}
		});
	}

	public void showFavorites(final Set<Object> favorites)
	{
		syncExec(new VoidResult()
		{
			@Override
			public void run()
			{
				favoritesView.showFavorites(favorites);
			}
		});
	}

	public void registerController(final ISearchFavoritesMasterController masterController)
	{
		syncExec(new VoidResult()
		{
			@Override
			public void run()
			{
				favoritesView.registerController(masterController);
				favoritesView.initializationCompleted();
				favoritesView.showFavorites(new HashSet<Object>());
				shell().widget.layout(true, true);
			}
		});
	}

	public void dragToFavorites(final Object draggedItem)
	{
		final SWTBotTree dragHelperTree = new SWTBotTree(dragHelperTreeViewer.getTree());
		final SWTBotTreeItem draggedTreeItem = dragHelperTree.getTreeItem(draggedItem.toString());
		dndUtil.dragAndDrop(draggedTreeItem, findFavoritesTree());
	}

	private SWTBotTree findFavoritesTree()
	{
		return new SWTBotTree(botUtils.findSibling(bot().label(FAVORITES_LABEL), Tree.class));
	}

	public boolean isFavoritesItemDisplayed(final Object itemToSearchFor, final Object parentItem)
	{
		return isChildItemDisplayed(itemToSearchFor, parentItem);
	}

	private boolean isChildItemDisplayed(final Object itemToSearchFor, final Object parentItem)
	{
		final SWTBotTreeItem rootItem = findFavoritesRootItem(parentItem);
		try
		{
			findItem(rootItem, itemToSearchFor);
			return true;
		}
		catch (WidgetNotFoundException e)
		{
			return false;
		}
	}

	public boolean isFavoritesRootItemDisplayed(Object itemToSearchFor)
	{
		try
		{
			findFavoritesRootItem(itemToSearchFor);
			return true;
		}
		catch (WidgetNotFoundException e)
		{
			return false;
		}
	}

	private SWTBotTreeItem findFavoritesRootItem(final Object itemToSearchFor)
	{
		final SWTBotTreeItem treeItem = findItemAmong(findFavoritesTree().getAllItems(), itemToSearchFor);
		if (treeItem == null)
		{
			throw new WidgetNotFoundException(MessageFormat.format("Root favorites item {0} not found", itemToSearchFor.toString()));
		}
		return treeItem;
	}

	private SWTBotTreeItem findItem(final SWTBotTreeItem parent, final Object itemToSearchFor)
	{
		final SWTBotTreeItem treeItem = findItemAmong(parent.getItems(), itemToSearchFor);
		if (treeItem == null)
		{
			throw new WidgetNotFoundException(MessageFormat.format("Child {0} of parent {1} not found", itemToSearchFor.toString(), parent.toString()));
		}
		return treeItem;
	}

	private SWTBotTreeItem findItemAmong(final SWTBotTreeItem[] allItems, final Object itemToSearchFor)
	{
		for (SWTBotTreeItem item : allItems)
		{
			if (getData(item.widget) == itemToSearchFor)
			{
				return item;
			}
		}

		return null;
	}

	private Object getData(final TreeItem widget)
	{
		return syncExec(new Result<Object>()
		{
			@Override
			public Object run()
			{
				return widget.getData();
			}
		});
	}

	public void dragFromFavorites(final Object parent, final Object itemToDrag)
	{
		final SWTBotTreeItem treeItem = findItemAmong(findFavoritesRootItem(parent).getItems(), itemToDrag);
		dndUtil.dragAndDrop(treeItem, new SWTBotTree(dragHelperTreeViewer.getTree()));
	}

	public DragHelperTreePageObject getDragHelper()
	{
		return new DragHelperTreePageObject();
	}

	public class DragHelperTreePageObject
	{
		public void addDropSupport(final int operations, final Transfer[] transferTypes, final DropTargetListener dropTargetListener)
		{
			syncExec(new VoidResult()
			{
				@Override
				public void run()
				{
					dragHelperTreeViewer.addDropSupport(operations, transferTypes, dropTargetListener);
				}
			});
		}
	}
}
