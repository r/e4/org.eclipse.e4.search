/*******************************************************************************
 * Copyright (c) 2010, 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import junit.framework.Assert;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.platform.discovery.core.api.ISearchFavoritesMasterController;
import org.eclipse.platform.discovery.core.internal.ContextStructuredSelection;
import org.eclipse.platform.discovery.core.internal.IContextStructuredSelection;
import org.eclipse.platform.discovery.ui.api.ISearchFavoritesViewCustomization;
import org.eclipse.platform.discovery.ui.test.comp.internal.fixture.SearchConsoleTestFixture;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.SearchFavoritesPageObject;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.Transfer;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public class SearchFavoritesViewTest
{
	private static SearchConsoleTestFixture fixture;
	
	private ISearchFavoritesViewCustomization viewCustomization;
	private ITreeContentProvider dragHelperViewerContentProvider;
	private ITreeContentProvider favoritesContentProvider;
	private ILabelProvider labelProvider;

	private Object favoritesItemSource;
	private Object firstFavoriteItem;
	private Object secondFavoriteItem;
	private Object favoritesGroup;

	private SearchFavoritesPageObject searchFavorites;
	private IDiscoveryEnvironment environment;

	@BeforeClass
	public static void initFixture()
	{
		fixture = new SearchConsoleTestFixture();
	}
	
	@Before
	public void setUp()
	{
		favoritesItemSource = new String("favoritesItemSource");
		firstFavoriteItem = new String("firstFavoriteItem");
		secondFavoriteItem = new String("secondFavoriteItem");
		favoritesGroup = new String("favoritesGroup");

		setupContentProvider();
		setupLabelProvider();
		setupViewCustomization();

		environment = fixture.environment;
		searchFavorites = new SearchFavoritesPageObject(viewCustomization, dragHelperViewerContentProvider, environment);
		searchFavorites.open();
	}

	@After
	public void tearDown()
	{
		searchFavorites.close();
	}

	private void setupLabelProvider()
	{
		labelProvider = Mockito.mock(ILabelProvider.class);
		Mockito.stub(labelProvider.getText(Mockito.any())).toAnswer(new Answer<String>()
		{
			@Override
			public String answer(InvocationOnMock invocation) throws Throwable
			{
				return invocation.getArguments()[0].toString();
			}
		});
	}

	private void setupContentProvider()
	{
		favoritesContentProvider = Mockito.mock(ITreeContentProvider.class);
		Mockito.stub(favoritesContentProvider.getChildren(Mockito.any())).toReturn(new Object[0]);
		Mockito.stub(favoritesContentProvider.getParent(Mockito.any())).toReturn(null);
		Mockito.stub(favoritesContentProvider.hasChildren(Mockito.any())).toReturn(false);

		dragHelperViewerContentProvider = Mockito.mock(ITreeContentProvider.class);
		Mockito.stub(dragHelperViewerContentProvider.getChildren(Mockito.any())).toReturn(new Object[0]);
		Mockito.stub(dragHelperViewerContentProvider.getParent(Mockito.any())).toReturn(null);
		Mockito.stub(dragHelperViewerContentProvider.hasChildren(Mockito.any())).toReturn(false);
		Mockito.stub(dragHelperViewerContentProvider.getElements(Mockito.any())).toReturn(new Object[] { firstFavoriteItem, secondFavoriteItem });
	}

	private void setupViewCustomization()
	{
		viewCustomization = Mockito.mock(ISearchFavoritesViewCustomization.class);
		Mockito.stub(viewCustomization.getContentProvider()).toReturn(favoritesContentProvider);
		Mockito.stub(viewCustomization.getLabelProvider()).toReturn(labelProvider);
		Mockito.stub(viewCustomization.itemGroup(Mockito.any())).toReturn(favoritesGroup);
		Mockito.stub(viewCustomization.itemsFor(Mockito.any())).toAnswer(new Answer<Set<Object>>()
		{
			@Override
			public Set<Object> answer(InvocationOnMock invocation) throws Throwable
			{
				final Object biSrc = invocation.getArguments()[0];
				if (biSrc == favoritesItemSource)
				{
					return new HashSet<Object>(Arrays.asList(new Object[] { firstFavoriteItem, secondFavoriteItem }));
				}

				return new HashSet<Object>(Arrays.asList(new Object[] { biSrc }));
			}
		});
	}

	@Test
	public void testViewIsBusyWhenOpened()
	{
		Assert.assertTrue("Operation is not running", searchFavorites.isOperationRunning());
		searchFavorites.completeInitialization();
		Assert.assertFalse("Operation is still running", searchFavorites.isOperationRunning());
	}

	@Test
	public void testDragToFavorites()
	{
		searchFavorites.registerController(new FavoritesController());
		searchFavorites.dragToFavorites(firstFavoriteItem);
		Assert.assertTrue("Grouping item not displayed", searchFavorites.isFavoritesRootItemDisplayed(favoritesGroup));
		Assert.assertTrue("First favorite item not displayed", searchFavorites.isFavoritesItemDisplayed(firstFavoriteItem, favoritesGroup));
	}

	@Test
	public void testDragFromFavorites()
	{
		searchFavorites.registerController(new FavoritesController());
		final Set<Object> favoritesToShow = new HashSet<Object>();
		favoritesToShow.add(firstFavoriteItem);
		searchFavorites.showFavorites(favoritesToShow);

		final Collection<Object> droppedObjects = new ArrayList<Object>();
		searchFavorites.getDragHelper().addDropSupport(DND.DROP_COPY | DND.DROP_MOVE, new Transfer[] { LocalSelectionTransfer.getTransfer() }, new DropTargetAdapter()
		{
			@SuppressWarnings("unchecked")
			@Override
			public void drop(DropTargetEvent event)
			{
				if (LocalSelectionTransfer.getTransfer().isSupportedType(event.currentDataType))
				{
					final IStructuredSelection sel = (IStructuredSelection) LocalSelectionTransfer.getTransfer().getSelection();
					final Iterator<Object> it = sel.iterator();
					while (it.hasNext())
					{
						droppedObjects.add(it.next());
					}
				}
			}
		});
		searchFavorites.dragFromFavorites(favoritesGroup, firstFavoriteItem);
		Assert.assertEquals("One dropped item expected", 1, droppedObjects.size());
		Assert.assertEquals("Unexpected item dropped", firstFavoriteItem, droppedObjects.iterator().next());
	}

	private class FavoritesController implements ISearchFavoritesMasterController
	{
		@Override
		public IDiscoveryEnvironment getEnvironment()
		{
			return environment;
		}

		@Override
		public void deleteItems(Set<Object> data)
		{
		}

		@SuppressWarnings("unchecked")
		@Override
		public void importData(Object data)
		{
			Assert.assertTrue(data instanceof IContextStructuredSelection);
			final ContextStructuredSelection selection = (ContextStructuredSelection) data;
			final Set<Object> affectedObjects = new HashSet<Object>();
			affectedObjects.addAll(selection.toList());
			searchFavorites.showFavorites(affectedObjects);
		}

		@Override
		public boolean isImportPossible(Object data)
		{
			return true;
		}
	}
}
