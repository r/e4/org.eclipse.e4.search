/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.swtbot.swt.finder.utils.SWTUtils;
import org.eclipse.swtbot.swt.finder.utils.internal.Assert;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotLabel;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotShell;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

public class SWTBotTooltip extends SWTBotShell
{
	private final SwtBotUtils botUtils;

	public SWTBotTooltip(Shell shell) throws WidgetNotFoundException
	{
		super(shell);
		Assert.isTrue(SWTUtils.hasStyle(shell, SWT.TOOL), "Expecting a tooltip shell.");
		botUtils = new SwtBotUtils();
	}

	public SWTBotLabel header()
	{
		return new SWTBotLabel((Label) botUtils.findOneChildControlOfExactType(this.widget, Label.class, true));
	}

	/**
	 * A tooltip can be one at a time ("singleton") and therefore this static method tries to find the singleton instance
	 * 
	 * @return the currently tooltip currently displayed
	 * @throw {@link WidgetNotFoundException} if the tooltip is not found
	 */
	public static SWTBotTooltip tooltip()
	{
		final SWTBotShell tooltipShell = new SwtBotUtils().findShell(new BaseMatcher<Shell>()
		{
			@Override
			public boolean matches(Object item)
			{
				if (item instanceof SWTBotShell)
				{
					return SWTUtils.hasStyle(((SWTBotShell) item).widget, SWT.TOOL); // A tooltip's shell has the SWT.TOOL style
				}
				return false;
			}

			@Override
			public void describeTo(Description description)
			{
			}
		});
		return new SWTBotTooltip(tooltipShell.widget);
	}
}
