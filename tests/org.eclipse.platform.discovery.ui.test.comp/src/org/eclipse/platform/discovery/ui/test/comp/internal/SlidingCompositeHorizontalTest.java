/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal;

import static org.junit.Assert.assertEquals;

import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.SashOrientation;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.SlidingCompositePageObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


@SuppressWarnings("nls")
public class SlidingCompositeHorizontalTest {

	private final SlidingCompositePageObject slidingComposite = new SlidingCompositePageObject();
	
	@Before
	public void setUp() {
		slidingComposite.open();
		makeComositeWide();
	}
 
	private void makeComositeWide() {
		slidingComposite.resize(600, 100);
	}
	
	private void makeCompositeHigh() {
		slidingComposite.resize(100, 500);
	}
	
	@Test
	public void testResizeCauseReorientationIfWidthIsGreaterThanHeihgt() throws Exception {
		
		assertEquals("unexpected orientation", SashOrientation.VERTICAL, slidingComposite.getSashOrientation());

		makeCompositeHigh();
		
		assertEquals("unexpected orientation", SashOrientation.HORIZONTAL, slidingComposite.getSashOrientation());
	}
	
	@After
	public void tearDown() {
		slidingComposite.close();
	}

	
	//FIXME review with Dani
//	public void testResizingShellDoesNotHideButton() throws NotFoundException, MultipleFoundException 
//	{
//		final ShellTester shellTester = ShellTester.getShellTester();
//		final Point initialSize = shellTester.getSize(testingShell);
//		Sash sash = getSlidingSash();
//		
//
//		getHideButton();
//		getDisplay().syncExec(new Runnable()
//		{
//			@Override
//			public void run()
//			{
//				testComposite.upperCompositeSizeChanged(new PropertyAttributeChangedEvent<Point>(this, new Point(0, 0), new Point(initialSize.x, initialSize.y)));
//			}
//		});
//		
//		getDisplay().syncExec(new Runnable()
//		{
//			@Override
//			public void run()
//			{
//				testingShell.setSize(200,100);
//			}
//		});
//		
//		sash = getSlidingSash();
//		final int sashXPos = getSashXPosition(sash);
//		final Rectangle clientArea = shellTester.getClientArea(testingShell);
//
//		final int bottomDelta = clientArea.width - sashXPos;
//		// Assert that the X coordinate of the sash is equal to the width of the shell client area. Acceptable deviation is 2 pixels 
//		assertTrue("Unacceptable X coordinate of sash. Deviation: " + Math.abs(bottomDelta), Math.abs(bottomDelta) <= 2);
//	}
	
}
