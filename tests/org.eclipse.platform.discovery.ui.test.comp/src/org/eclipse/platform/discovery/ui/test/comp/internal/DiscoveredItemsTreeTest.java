/*******************************************************************************
 * Copyright (c) 2010, 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal;

import java.text.MessageFormat;

import junit.framework.Assert;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.platform.discovery.ui.api.IFormTextBuilder;
import org.eclipse.platform.discovery.ui.api.IGenericViewCustomization;
import org.eclipse.platform.discovery.ui.api.ITooltipProvider;
import org.eclipse.platform.discovery.ui.api.impl.GenericViewCustomizationImpl;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.DiscoveredItemsInShellPageObject;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;
import org.eclipse.platform.discovery.util.internal.longop.CurrentThreadOperationRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class DiscoveredItemsTreeTest
{
	private static final String TOOLTIP_TEMPLATE = "Tooltip for {0}";

	private IGenericViewCustomization viewCustomization;
	private Object displayedItem;

	private DiscoveredItemsInShellPageObject viewerPageObject;

	@Before
	public void setUp() throws Exception
	{
		displayedItem = new Object();
		viewCustomization = createViewCustomization();
		viewerPageObject = new DiscoveredItemsInShellPageObject(discoveryEnv(), viewCustomization);
		viewerPageObject.open();
		viewerPageObject.tree().setInput(displayedItem);
	}

	@After
	public void tearDown()
	{
		viewerPageObject.close();
	}

	private IGenericViewCustomization createViewCustomization()
	{
		return new GenericViewCustomizationImpl()
		{
			@Override
			public ITreeContentProvider getContentProvider()
			{
				return new MyContentProvider();
			}

			@Override
			public ILabelProvider getLabelProvider()
			{
				return new LabelProvider();
			}

			@Override
			public ITooltipProvider getTooltipProvider()
			{
				return new ITooltipProvider()
				{
					@Override
					public void createTooltipContent(IFormTextBuilder tooltipTextBuilder, Object element)
					{
						tooltipTextBuilder.appendText(tooltipText(element));
					}
				};
			}
		};
	}

	private IDiscoveryEnvironment discoveryEnv()
	{
		final IDiscoveryEnvironment env = Mockito.mock(IDiscoveryEnvironment.class);
		final IProgressMonitor monitor = new NullProgressMonitor();
		Mockito.when(env.progressMonitor()).thenReturn(monitor);
		Mockito.when(env.operationRunner()).thenReturn(new CurrentThreadOperationRunner(monitor));
		return env;
	}

	private class MyContentProvider extends ArrayContentProvider implements ITreeContentProvider
	{
		@Override
		public Object[] getChildren(Object parentElement)
		{
			return getElements(parentElement);
		}

		@Override
		public Object getParent(Object element)
		{
			return null;
		}

		@Override
		public boolean hasChildren(Object element)
		{
			return getElements(element).length > 0;
		}
	}
	
	@Test
	public void testItemIsDisplayed()
	{
		Assert.assertTrue("Item is not displayed", viewerPageObject.tree().isTreeItemDisplayed(displayedItem.toString()));
	}

	@Test
	public void testTooltip()
	{
		hoverOnTreeItem(displayedItem.toString());
		Assert.assertTrue("Tooltip not displayed", viewerPageObject.tree().isTooltipDisplayed(displayedItem.toString()));
		moveAwayFromTreeItem(displayedItem.toString());
		Assert.assertFalse("Tooltip still displayed", viewerPageObject.tree().isTooltipDisplayed(displayedItem.toString()));
	}

	private String tooltipText(final Object object)
	{
		return MessageFormat.format(TOOLTIP_TEMPLATE, object.toString());
	}

	private void hoverOnTreeItem(final Object object)
	{
		viewerPageObject.tree().discoveredTreeItem(object.toString()).hoverMouse();
	}

	private void moveAwayFromTreeItem(final Object object)
	{
		viewerPageObject.tree().discoveredTreeItem(object.toString()).moveMouseAway();
	}
}
