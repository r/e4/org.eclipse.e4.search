/*******************************************************************************
 * Copyright (c) 2010, 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.ISearchSubdestination;
import org.eclipse.platform.discovery.runtime.internal.ISearchProviderConfiguration;
import org.eclipse.platform.discovery.runtime.internal.ProviderNotFoundException;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.ISearchProviderDescription;
import org.eclipse.platform.discovery.ui.api.ISearchParametersUI.IConsoleContext;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.SubdestinationsSelectorPageObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public class SubdestinationsSelectorTest
{
	@Mock
	private ISearchProviderConfiguration providerConfiguration;
	@Mock
	private IObjectTypeDescription searchObjectType;
	@Mock
	private IDestinationCategoryDescription destinationCategory;
	@Mock
	private ISearchProviderDescription searchProviderDescription;
	@Mock
	private ISearchSubdestination subDestination_1;
	@Mock
	private ISearchSubdestination subDestination_2;
	@Mock
	private ISearchSubdestination subDestination_3;
	@Mock
	private ISearchDestination searchDestination;

	private static final String SUBDESTINATION_NAME_1 = "Subdestination 1";
	private static final String SUBDESTINATION_NAME_2 = "Subdestination 2";
	private static final String SUBDESTINATION_NAME_3 = "Subdestination 3";

	private SubdestinationsSelectorPageObject subdestinationsSelector;

	private Map<ISearchSubdestination, Boolean> subdestinationsActivationState;

	@Before
	public void setUp() throws ProviderNotFoundException
	{
		MockitoAnnotations.initMocks(this);
		
		subdestinationsActivationState = new HashMap<ISearchSubdestination, Boolean>();
		setupSubdestinations();
		setupProviderConfiguration();
		subdestinationsSelector = new SubdestinationsSelectorPageObject(createConsoleContext());
	}

	@After
	public void tearDown()
	{
		subdestinationsSelector.close();
	}

	private IConsoleContext createConsoleContext()
	{
		final IConsoleContext context = Mockito.mock(IConsoleContext.class);
		Mockito.stub(context.destinationCategory()).toReturn(destinationCategory);
		Mockito.stub(context.searchDestination()).toReturn(searchDestination);
		Mockito.stub(context.searchProvider()).toReturn(searchProviderDescription);
		Mockito.stub(context.searchProviderConfiguration()).toReturn(providerConfiguration);
		Mockito.stub(context.selectedObjectType()).toReturn(searchObjectType);
		Mockito.doAnswer(new ActivationChangeAnswer()).when(context).notifySubdestinationActivationChange(Mockito.isA(ISearchSubdestination.class), Mockito.anyBoolean());

		return context;
	}

	private void setupSubdestinations()
	{
		Mockito.stub(subDestination_1.getDisplayName()).toReturn(SUBDESTINATION_NAME_1);
		Mockito.stub(subDestination_2.getDisplayName()).toReturn(SUBDESTINATION_NAME_2);
		Mockito.stub(subDestination_3.getDisplayName()).toReturn(SUBDESTINATION_NAME_3);
	}

	private void setupProviderConfiguration() throws ProviderNotFoundException
	{
		final List<ISearchSubdestination> subDestinations = Arrays.asList(new ISearchSubdestination[] { subDestination_1, subDestination_2, subDestination_3 });
		Mockito.stub(providerConfiguration.getAvailableSearchSubdestinations(searchObjectType, destinationCategory, searchProviderDescription)).toReturn(subDestinations);
		Mockito.when(providerConfiguration.isSubdestinationActive(Mockito.isA(ISearchSubdestination.class), Mockito.same(searchObjectType), Mockito.same(destinationCategory), Mockito.same(searchProviderDescription))).thenAnswer(new Answer<Boolean>()
		{
			@Override
			public Boolean answer(InvocationOnMock invocation) throws Throwable
			{
				final ISearchSubdestination subdest = (ISearchSubdestination) invocation.getArguments()[0];
				final Boolean isActive = subdestinationsActivationState.get(subdest);
				return isActive == null ? false : isActive;
			}
		});

	}

	@Test
	public void testAllSubdestinationsAreShown()
	{
		subdestinationsSelector.open();

		verifySubdestinationDisplayed(SUBDESTINATION_NAME_1);
		verifySubdestinationDisplayed(SUBDESTINATION_NAME_2);
		verifySubdestinationDisplayed(SUBDESTINATION_NAME_3);
	}

	@Test
	public void testInitiallySelectedSubdestinationsAreChecked()
	{
		setActive(subDestination_1, true);
		setActive(subDestination_2, false);
		setActive(subDestination_3, true);
		subdestinationsSelector.open();

		Assert.assertTrue("Subdestination 1 should be selected", subdestinationsSelector.isSelected(SUBDESTINATION_NAME_1));
		Assert.assertFalse("Subdestination 2 should be selected", subdestinationsSelector.isSelected(SUBDESTINATION_NAME_2));
		Assert.assertTrue("Subdestination 3 should not be selected", subdestinationsSelector.isSelected(SUBDESTINATION_NAME_3));
	}

	@Test
	public void testToggleSelectedSubdestination()
	{
		setActive(subDestination_1, true);
		subdestinationsSelector.open();

		subdestinationsSelector.toggle(SUBDESTINATION_NAME_1);
		verifySubdestinationActivationSet(subDestination_1, false);
	}

	@Test
	public void testToggleUnselectedSubdestinations()
	{
		setActive(subDestination_3, false);
		subdestinationsSelector.open();

		subdestinationsSelector.toggle(SUBDESTINATION_NAME_3);
		verifySubdestinationActivationSet(subDestination_3, true);
	}

	@Test
	public void testSelectWithConflicting()
	{
		setActive(subDestination_1, true);
		setActive(subDestination_2, false);
		subdestinationsSelector.open();

		subdestinationsSelector.toggle(SUBDESTINATION_NAME_2);

		Assert.assertFalse("Button for subd1 should be deselected because it conflicts with subd2", subdestinationsSelector.isSelected(SUBDESTINATION_NAME_1));
		Assert.assertTrue("Button for subd2 should be selected", subdestinationsSelector.isSelected(SUBDESTINATION_NAME_2));
	}

	private void verifySubdestinationDisplayed(final String subdestinationName)
	{
		Assert.assertTrue(MessageFormat.format("Subdestination {0} not displayed", subdestinationName), subdestinationsSelector.isDisplayed(subdestinationName));
	}

	private void verifySubdestinationActivationSet(final ISearchSubdestination subDestination, final Boolean expectedState)
	{
		Assert.assertEquals(MessageFormat.format("Unexpected subdestination {0} activation state", subDestination.getDisplayName()), expectedState, subdestinationsActivationState.get(subDestination));
	}
	
	private void setActive(final ISearchSubdestination subd, final boolean activationState)
	{
		subdestinationsActivationState.put(subd, activationState);
	}

	private class ActivationChangeAnswer implements Answer<Void>
	{
		@Override
		public Void answer(InvocationOnMock invocation) throws Throwable
		{
			final ISearchSubdestination subdest = (ISearchSubdestination) invocation.getArguments()[0];
			subdestinationsActivationState.put(subdest, (Boolean) invocation.getArguments()[1]);
			// simulate subdestination_2 conflict with subdestination_1
			if(subdest == subDestination_2)
			{
				subdestinationsActivationState.put(subDestination_1, false);
			}
			return null;
		}
	}
}
