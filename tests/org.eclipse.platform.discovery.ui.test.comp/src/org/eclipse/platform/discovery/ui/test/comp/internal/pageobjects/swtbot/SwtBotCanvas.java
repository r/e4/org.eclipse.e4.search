package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot;

import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.swtbot.swt.finder.widgets.AbstractSWTBotControl;
import org.hamcrest.SelfDescribing;

public class SwtBotCanvas extends AbstractSWTBotControl<Canvas> {

	private SwtBotCanvas(Canvas w, SelfDescribing description) throws WidgetNotFoundException {
		super(w, description);
	}

	private SwtBotCanvas(Canvas w) throws WidgetNotFoundException {
		super(w);
	}
	
	public static SwtBotCanvas canvas(Widget parent, boolean shouldBeVisible) {
		return new SwtBotCanvas(new SwtBotUtils().findOneChildControlOfExactType(parent, Canvas.class, shouldBeVisible));
	}
	
	@Override
	public AbstractSWTBotControl<Canvas> click() {
		return super.click(true);
	}
}
