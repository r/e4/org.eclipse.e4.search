/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal;

import static org.junit.Assert.*;

import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.ErrorHandlerPageObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ErrorHandlerTest {

	private static final String TITLE = "title";
	private static final String DETAILS = "details";
	

	private ErrorHandlerPageObject errorHandler;

	@Before
	public void setUp() {
		errorHandler = new ErrorHandlerPageObject();
	}
	
	@After
	public void tearDown() {
		if (errorHandler.isOpen()) {
			errorHandler.close();
		}
	}
	
	@Test
	public void showErrorMessage()	{
		errorHandler.showError(TITLE, DETAILS);
		assertEquals("Unexpected title", TITLE, errorHandler.getTitleShown());
		assertEquals("Unexpected details", DETAILS, errorHandler.getDetailsShown());
		
		assertTrue("Error message details text should be read only.", errorHandler.isDetailsReadOnly());
	}
	
	@Test
	public void handleException()	{
		Exception exception = new Exception("TGiF");
		errorHandler.handleException(exception,TITLE);
		
		assertEquals("Unexpected title", TITLE, errorHandler.getTitleShown());
		errorHandler.getLogger().checkExceptionWasLogged(exception);
	}

}
