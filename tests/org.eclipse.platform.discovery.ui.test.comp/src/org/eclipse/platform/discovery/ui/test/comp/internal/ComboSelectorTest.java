/*******************************************************************************
 * Copyright (c) 2010, 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.platform.discovery.runtime.api.IDisplayableObject;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.ComboSelectorPageObject;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Mockito;

public class ComboSelectorTest
{
	private static final String FIRST_OBJECT_LABEL = "First";
	private static final String SECOND_OBJECT_LABEL = "Second";
	private static final String THIRD_OBJECT_LABEL = "Third";

	private ComboSelectorPageObject<IDisplayableObject> selector;

	@Before
	public void setUp()
	{
		final IDisplayableObject firstObject = createDisplayableObject(FIRST_OBJECT_LABEL);
		final IDisplayableObject secondObject = createDisplayableObject(SECOND_OBJECT_LABEL);
		final IDisplayableObject thirdObject = createDisplayableObject(THIRD_OBJECT_LABEL);
		final List<IDisplayableObject> input = Arrays.asList(new IDisplayableObject[] { firstObject, secondObject, thirdObject });
		selector = new ComboSelectorPageObject<IDisplayableObject>(input);
		selector.open();
	}

	@After
	public void tearDown()
	{
		selector.close();
	}

	private IDisplayableObject createDisplayableObject(final String objectLabel)
	{
		final IDisplayableObject object = Mockito.mock(IDisplayableObject.class);
		Mockito.stub(object.getDisplayName()).toReturn(objectLabel);
		return object;
	}

	@Test
	public void testCorrectObjectsDisplayed() throws Exception
	{
		final List<String> items = Arrays.asList(selector.getItems());
		assertEquals("Three items expected", 3, items.size());
		assertEquals("Unexpected first item", FIRST_OBJECT_LABEL, items.get(0));
		assertEquals("Unexpected second item", SECOND_OBJECT_LABEL, items.get(1));
		assertEquals("Unexpected third item", THIRD_OBJECT_LABEL, items.get(2));
	}

	@Test
	public void testSetInput() throws Exception
	{
		final String anotherObjectLabel = "ANOTHER_OBJECT";
		final IDisplayableObject anotherObject = createDisplayableObject(anotherObjectLabel);
		final List<IDisplayableObject> newInput = new ArrayList<IDisplayableObject>();
		newInput.add(anotherObject);
		selector.setInput(newInput);

		assertEquals("One item expected", 1, selector.getItems().length);
		assertEquals("Unexpeted item", anotherObjectLabel, selector.getItems()[0]);
	}

	@Test
	public void testGetSelectedItem() throws Exception
	{
		selector.select(THIRD_OBJECT_LABEL);
		assertEquals("Unexpected selected item", THIRD_OBJECT_LABEL, selector.getSelectedItem());
	}

	@Test
	public void testHandleSelectionMethod() throws Exception
	{
		ISelectionChangedListener listener = Mockito.mock(ISelectionChangedListener.class);
		selector.registerSelectionListener(listener);
		selector.select(SECOND_OBJECT_LABEL);

		Mockito.verify(listener, Mockito.times(1)).selectionChanged(Mockito.argThat(eventWithSelection(SECOND_OBJECT_LABEL)));
	}

	@Test
	public void testSetEnabled()
	{
		assertTrue("Combo should be enabled by default", selector.isComboEnabled());
		assertTrue("Label should be enabled by default", selector.isLabelEnabled());

		selector.setEnabled(false);
		assertFalse("Combo should be disabled now", selector.isComboEnabled());
		assertFalse("Label should be disabled now", selector.isLabelEnabled());

		selector.setEnabled(true);
		assertTrue("Combo should be enabled now", selector.isComboEnabled());
		assertTrue("Label should be enabled now", selector.isLabelEnabled());
	}
	
	private Matcher<SelectionChangedEvent> eventWithSelection(final String expectedSelection)
	{
		return new ArgumentMatcher<SelectionChangedEvent>()
		{
			@Override
			public boolean matches(Object argument)
			{
				final SelectionChangedEvent event = (SelectionChangedEvent) argument;
				final IStructuredSelection selection = (IStructuredSelection) event.getSelection();
				final IDisplayableObject selectedObject = (IDisplayableObject) selection.getFirstElement();
				return (selection.size() == 1) && (expectedSelection.equals(selectedObject.getDisplayName()));
			}
		};
	}
}
