/*******************************************************************************
 * Copyright (c) 2010, 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal;

import java.text.MessageFormat;
import java.util.Collection;

import junit.framework.Assert;

import org.eclipse.platform.discovery.core.api.ISearchContext;
import org.eclipse.platform.discovery.core.internal.ISearchSession;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.ISearchParameters;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.ui.api.IGenericViewCustomization;
import org.eclipse.platform.discovery.ui.api.IViewUiContext;
import org.eclipse.platform.discovery.ui.internal.plugin.DiscoveryUIMessages;
import org.eclipse.platform.discovery.ui.internal.search.advancedparams.IAdvancedSearchParamsDisplayer;
import org.eclipse.platform.discovery.ui.internal.view.impl.ITabbedSessionDisplayer;
import org.eclipse.platform.discovery.ui.internal.view.impl.SearchConsoleSelectionProvider;
import org.eclipse.platform.discovery.ui.test.comp.internal.fixture.SearchConsoleTestFixture;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.SearchConsolePageObject;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class SearchConsoleViewTest
{
	private SearchConsolePageObject consolePageObject;
	private static SearchConsoleTestFixture fixture;

	@Mock
	private IAdvancedSearchParamsDisplayer searchParamsDisplayer;

	private final static String KEYWORD_SEARCH_PARAMETER = "my keyword param"; //$NON-NLS-1$
	private final static String testSessionId = "testsessionid"; //$NON-NLS-1$

	protected ITabbedSessionDisplayer<ISearchContext> sessionDisplayer;
	protected IViewUiContext uiContext;

	@BeforeClass
	public static void initFixture()
	{
		fixture = new SearchConsoleTestFixture();
	}
	
	@SuppressWarnings("unchecked")
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		sessionDisplayer = Mockito.mock(ITabbedSessionDisplayer.class);
		searchParamsDisplayer = Mockito.mock(IAdvancedSearchParamsDisplayer.class);

		consolePageObject = new SearchConsolePageObject(fixture.searchProviderConfiguration, sessionDisplayer, searchParamsDisplayer, fixture.environment, fixture.viewUiContext);
		consolePageObject.open();
	}

	@After
	public void tearDown()
	{
		consolePageObject.close();
	}

	@Test
	public void testSelectingObjectsChangesDestinations()
	{
		Assert.assertNull("No objects selected expected by default", consolePageObject.getSelectedObject());
		Assert.assertEquals("No destination categories expected to be visible by default", 0, consolePageObject.getCategories().size());

		// select the first object type
		selectObject(fixture.objectType1);
		consolePageObject.expandDestinations();
		assertSingleCategoryVisible(fixture.category1);
		assertDestinationsVisible(fixture.category1, fixture.destination1, fixture.destination2);

		// select the second object type
		selectObject(fixture.objectType2);
		consolePageObject.expandDestinations();
		assertSingleCategoryVisible(fixture.category2);
		assertDestinationsVisible(fixture.category2, fixture.destination3, fixture.destination4);
	}
	
	private void assertDestinationsVisible(final IDestinationCategoryDescription category, final ISearchDestination... expectedDestinations)
	{
		final Collection<String> visibleDestinations = consolePageObject.getDestinationsForCategory(category.getDisplayName());
		Assert.assertEquals(MessageFormat.format("Unexpected destinations caount for category {0}", category.getDisplayName()), expectedDestinations.length, visibleDestinations.size());
		for(ISearchDestination expectedDest : expectedDestinations)
		{
			Assert.assertTrue(MessageFormat.format("Destination {0} not visible", expectedDest.getDisplayName()), visibleDestinations.contains(expectedDest.getDisplayName()));
		}
	}

	private void assertSingleCategoryVisible(final IDestinationCategoryDescription category)
	{
		final Collection<String> visibleCategories = consolePageObject.getCategories();
		Assert.assertTrue(MessageFormat.format("Category {0} not available", category.getDisplayName()), visibleCategories.contains(category.getDisplayName()));
		Assert.assertEquals("Expected to display exactly one category", 1, visibleCategories.size());
	}

	@Test
	public void testSelectingObjectsNotifiesAdvancedParametersComponent()
	{
		selectObject(fixture.objectType1);
		Mockito.verify(searchParamsDisplayer, Mockito.atLeastOnce()).update(fixture.objectType1, null, fixture.environment, fixture.viewUiContext);
	}

	@Test
	public void testUiComponentsEnableProgressively()
	{
		Assert.assertTrue("Search for list should be enabled", consolePageObject.canSelectObject());
		Assert.assertFalse("Search in tree should be disabled", consolePageObject.canSelectDestination());
		Assert.assertFalse("Keywords text should be disabled", consolePageObject.canEnterSearchText());
		Assert.assertFalse("Subdestinations should be disabled", consolePageObject.canSelectSubdestination());
		Assert.assertFalse("Search button should be disabled", consolePageObject.canSearch());
		Assert.assertFalse("Group by combo should be disabled", consolePageObject.canSelectHierarchy());

		// select the first object type
		selectObject(fixture.objectType1);
		Assert.assertTrue("Search in list should be enabled", consolePageObject.canSelectDestination());
		Assert.assertFalse("Keywords text should be disabled", consolePageObject.canEnterSearchText());
		Assert.assertFalse("Subdestinations should be disabled", consolePageObject.canSelectSubdestination());
		Assert.assertFalse("Search button should be disabled", consolePageObject.canSearch());
		Assert.assertFalse("Group by combo should be disabled", consolePageObject.canSelectHierarchy());

		// select first destination
		consolePageObject.selectFirstPossibleDestination();
		Assert.assertFalse("Keywords text should be disabled", consolePageObject.canEnterSearchText());
		Assert.assertTrue("Subdestinations should be enabled", consolePageObject.canSelectDestination());
		Assert.assertTrue("Search button should be enabled", consolePageObject.canSearch());
		Assert.assertFalse("Group by combo should not be disabled", consolePageObject.canSelectHierarchy());

		// Select the second object type and third destination. This would select a search provider which has grouping hierarchy capability and
		// supports text search
		selectObject(fixture.objectType2);
		consolePageObject.selectFirstPossibleDestination();
		Assert.assertTrue("Group by combo should be enabled", consolePageObject.canSelectHierarchy());
		Assert.assertTrue("Keywords text should be enabled", consolePageObject.canEnterSearchText());

		// Select the first object type and first destination. This would determine a search provider which has no grouping hierarchy capability and
		// does not support text search
		selectObject(fixture.objectType1);
		consolePageObject.selectFirstPossibleDestination();
		Assert.assertFalse("Group by combo should be disabled", consolePageObject.canSelectHierarchy());
		Assert.assertFalse("Keywords text should be disabled", consolePageObject.canEnterSearchText());
	}

	@Test
	public void testGroupingHieararchyComboDisplaysFirstElementByDefault()
	{
		selectObject(fixture.objectType2);
		consolePageObject.selectFirstPossibleDestination();
		Assert.assertEquals("Group by combo does not display the item expected", fixture.groupingHierarchy1.getDisplayName(), consolePageObject.getSelectedGroupingHierarchy());
	}

	@Test
	public void testSubdestinationsButtonEnablement()
	{
		selectObject(fixture.objectType1);
		consolePageObject.selectFirstPossibleDestination();
		Assert.assertTrue("Subdestinations button should be enabled when subdestinations available", consolePageObject.canSelectSubdestination());

		selectObject(fixture.objectType2);
		consolePageObject.selectFirstPossibleDestination();
		Assert.assertFalse("Subdestinations button should not be enabled when subdestinations not available", consolePageObject.canSelectSubdestination());
	}

	private void selectObject(final IObjectTypeDescription object)
	{
		consolePageObject.selectObject(object.getDisplayName());
	}

	@Test
	public void testChangingObjectTypesDisablesKeywordAndSubdestinationsSection()
	{
		selectObject(fixture.objectType2);
		consolePageObject.selectFirstPossibleDestination();
		Assert.assertTrue("Keywords text should be enabled", consolePageObject.canEnterSearchText());
		Assert.assertFalse("Subdestinations should be disabled", consolePageObject.canSelectSubdestination());

		selectObject(fixture.objectType1);
		consolePageObject.selectFirstPossibleDestination();
		Assert.assertFalse("Keywords text should be disabled after object type change", consolePageObject.canEnterSearchText());
		Assert.assertTrue("Subdestinations should be enabled", consolePageObject.canSelectSubdestination());
	}

	@Test
	public void testKeywordsEnablementChangesWhenActiveProvidersChange()
	{
		// By default, there is nothing selected, the keyword text is set to SearchConsoleView_NoObjectTypeSelectedHint
		Assert.assertEquals(MessageFormat.format("The keyword text should be set to \"{0}\" when disabled", DiscoveryUIMessages.SearchConsoleView_NoObjectTypeSelectedHint), DiscoveryUIMessages.SearchConsoleView_NoObjectTypeSelectedHint, consolePageObject.getKeyword());
		Assert.assertFalse("Keywords text should not be enabled", consolePageObject.canEnterSearchText());

		// Select object 3
		// When there is only one destination it should be selected by default and the keyword text is set to
		// DiscoveryUIMessages.SearchConsoleView_EnterKeywordHint
		selectObject(fixture.objectType3);
		Assert.assertEquals(MessageFormat.format("The keyword text should be set to \"{0}\"", DiscoveryUIMessages.SearchConsoleView_EnterKeywordHint), DiscoveryUIMessages.SearchConsoleView_EnterKeywordHint, consolePageObject.getKeyword());
		Assert.assertTrue("Keywords text should not be enabled", consolePageObject.canEnterSearchText());

		// Select object 2
		selectObject(fixture.objectType2);
		// When there is no destination selected, the keyword text is set to DiscoveryUIMessages.SearchConsoleView_NoDestinationSelectedHint when the current search provider does not support text search
		Assert.assertEquals(MessageFormat.format("The keyword text should be set to \"{0}\" when disabled", DiscoveryUIMessages.SearchConsoleView_NoDestinationSelectedHint), DiscoveryUIMessages.SearchConsoleView_NoDestinationSelectedHint, consolePageObject.getKeyword());
		Assert.assertFalse("Keywords text should not be enabled", consolePageObject.canEnterSearchText());

		consolePageObject.selectFirstPossibleDestination();
		Assert.assertTrue("Keywords text should be enabled", consolePageObject.canEnterSearchText());
		consolePageObject.enterKeyword("HELLO!");

		// Select object 1
		selectObject(fixture.objectType1);
		consolePageObject.selectFirstPossibleDestination();
		Assert.assertFalse("Keywords text should not be enabled", consolePageObject.canEnterSearchText());
		// When the keyword text is disabled, its text is set to DiscoveryUIMessages.SearchConsoleView_UnsuuportedTextSearch when the current search
		// provider does not support text search
		Assert.assertEquals(MessageFormat.format("The keyword text should be set to \"{0}\" when disabled", DiscoveryUIMessages.SearchConsoleView_UnsupportedTextSearch), DiscoveryUIMessages.SearchConsoleView_UnsupportedTextSearch, consolePageObject.getKeyword());
	}

	@Test
	public void testSearch()
	{
		selectObject(fixture.objectType2);
		consolePageObject.selectDestination(fixture.category2.getDisplayName(), fixture.destination4.getDisplayName());
		consolePageObject.enterKeyword(KEYWORD_SEARCH_PARAMETER);
		consolePageObject.search();

		consolePageObject.verifySearchPerformed(fixture.objectType2.getId(), fixture.destination4.getDisplayName(), KEYWORD_SEARCH_PARAMETER);
		Mockito.verify(searchParamsDisplayer).setParams(Mockito.any(ISearchParameters.class));
	}

	@Test
	public void testChangingDestinationsUpdatesAdvancedSearchParams()
	{
		Mockito.stub(searchParamsDisplayer.isEnabled()).toReturn(true);
		selectObject(fixture.objectType1);
		consolePageObject.selectFirstPossibleDestination();
		Mockito.verify(searchParamsDisplayer).update(fixture.objectType1, fixture.destination1, fixture.environment, fixture.viewUiContext);
	}

	@Test
	public void testShowResultInSession()
	{
		final ISearchSession session = Mockito.mock(ISearchSession.class);
		Mockito.stub(session.getId()).toReturn(testSessionId);

		final String searchProviderId = fixture.supportingTextSearchProviderDescription.getId();
		final ISearchContext searchContext = Mockito.mock(ISearchContext.class);
		Mockito.stub(searchContext.searchProviderId()).toReturn(searchProviderId);
		Mockito.stub(searchContext.session()).toReturn(session);

		consolePageObject.showResult(searchContext);
		Mockito.verify(sessionDisplayer).display(session);
	}

	@Test
	public void testViewRegistersProperSelectionProvider()
	{
		Mockito.verify(consolePageObject.getViewSiteMock()).setSelectionProvider(Mockito.any(SearchConsoleSelectionProvider.class));
	}

	@Test
	public void testManageDestinations()
	{
		consolePageObject.verifyManageDestinationsWorksCorrectly();
	}

	@Test
	public void testObjectTypeWithNoDestinations()
	{
		selectObject(fixture.objectTypeNoDestinations);
		Assert.assertTrue("keyword text should be enabled", consolePageObject.canEnterSearchText());
		Assert.assertFalse("Search should be disabled because there is no search criteria", consolePageObject.canSearch());

		consolePageObject.enterKeyword("asd");
		Assert.assertTrue("Search should be enabled because search criteria was provided", consolePageObject.canSearch());
	}

	@Test
	public void testRegisteringIllegalViewCustomization()
	{
		final IGenericViewCustomization illegalCustomization = Mockito.mock(IGenericViewCustomization.class);
		try
		{
			consolePageObject.registerViewCustomization(illegalCustomization);
			Assert.fail("IllegalArgumentException for illegal customization"); //$NON-NLS-1$
		}
		catch (IllegalArgumentException e)
		{
			// expected
		}
	}
}
