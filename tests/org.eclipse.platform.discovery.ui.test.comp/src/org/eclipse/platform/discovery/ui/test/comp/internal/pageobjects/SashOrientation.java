package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects;

public enum SashOrientation{HORIZONTAL, VERTICAL}