/*******************************************************************************
 * Copyright (c) 2010, 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal.fixture;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.platform.discovery.core.internal.ISearchSession;
import org.eclipse.platform.discovery.runtime.api.GroupingHierarchy;
import org.eclipse.platform.discovery.runtime.api.IDestinationsProvider;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.ISearchProvider;
import org.eclipse.platform.discovery.runtime.api.ISearchSubdestination;
import org.eclipse.platform.discovery.runtime.internal.DestinationCategoryNotFoundException;
import org.eclipse.platform.discovery.runtime.internal.ISearchProviderConfiguration;
import org.eclipse.platform.discovery.runtime.internal.ProviderNotFoundException;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationsProviderDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.ISearchProviderDescription;
import org.eclipse.platform.discovery.testutils.utils.model.DestProviderDescriptionBuilder;
import org.eclipse.platform.discovery.testutils.utils.model.DestinationCategoryDescriptionBuilder;
import org.eclipse.platform.discovery.testutils.utils.model.DestinationsProviderBuilder;
import org.eclipse.platform.discovery.testutils.utils.model.ObjectTypeDescriptionBuilder;
import org.eclipse.platform.discovery.testutils.utils.model.SearchDestinationBuilder;
import org.eclipse.platform.discovery.testutils.utils.model.SearchProviderBuilder;
import org.eclipse.platform.discovery.testutils.utils.model.SearchProviderDescriptionBuilder;
import org.eclipse.platform.discovery.testutils.utils.model.SubdestinationBuilder;
import org.eclipse.platform.discovery.ui.api.IViewUiContext;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;
import org.eclipse.platform.discovery.util.api.env.IErrorHandler;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.platform.discovery.util.internal.session.ISessionManager;
import org.junit.BeforeClass;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 * Search console tests fixture. The fuxture contains lots of mocks which initialization takes some time. Therefore be advised that it is a good idea
 * to instantiate the fixture in a {@link BeforeClass} annotated method
 * 
 * @author Danail Branekov
 */
public class SearchConsoleTestFixture
{
	public static final String TEST_SEARCH_DEST_PROVIDER_PREFERENCE_PAGE_ID = "org.eclipse.platform.discovery.ui.test.comp.page1";

	public final static int VIEWERS_OFFSET = 60;

	private final String OBJECT_TYPE_ID1 = "org.eclipse.platform.discovery.objecttype1";
	private final String OBJECT_TYPE_ID2 = "org.eclipse.platform.discovery.objecttype2";
	private final String OBJECT_TYPE_ID3 = "org.eclipse.platform.discovery.objecttype3";
	private final String OBJECT_TYPE_NO_DESTINATIONS_ID = "org.eclipse.platform.discovery.nodestinations_objecttype";
	private final String OBJECT_TYPE_NAME_1 = "First object type";
	private final String OBJECT_TYPE_NAME_2 = "Second object type";
	private final String OBJECT_TYPE_NAME_3 = "Third object type";
	private final String OBJECT_TYPE_NO_DESTINATIONS_NAME = "Destination-less object type";

	public IObjectTypeDescription objectType1;
	public IObjectTypeDescription objectType2;
	public IObjectTypeDescription objectType3;
	public IObjectTypeDescription objectTypeNoDestinations;

	public IDestinationsProviderDescription destProviderDescr_cat1;
	public IDestinationsProviderDescription destProviderDescr_cat2;
	public IDestinationsProviderDescription destProviderDescr_cat3;

	public IDestinationsProvider destProvider_cat1;
	public IDestinationsProvider destProvider_cat2;
	public IDestinationsProvider destProvider_cat3;

	private final String DESTINATION_NAME1 = "First destination";
	private final String DESTINATION_NAME2 = "Second destination";
	private final String DESTINATION_NAME3 = "Third destination";
	private final String DESTINATION_NAME4 = "Fourth destination";
	private final String DESTINATION_NAME5 = "Fifth destination";

	public ISearchDestination destination1;
	public ISearchDestination destination2;
	public ISearchDestination destination3;
	public ISearchDestination destination4;
	public ISearchDestination destination5;

	private final static String CATEGORY_ID_1 = "category1";
	private final static String CATEGORY_ID_2 = "category2";
	private final static String CATEGORY_ID_3 = "category3";
	private final static String CATEGORY_NAME_1 = "First category";
	private final static String CATEGORY_NAME_2 = "Second category";
	private final static String CATEGORY_NAME_3 = "Third category";

	public IDestinationCategoryDescription category1;
	public IDestinationCategoryDescription category2;
	public IDestinationCategoryDescription category3;

	private static final String SUBDESTINATION_NAME_1 = "Subdestination 1";
	private static final String SUBDESTINATION_NAME_2 = "Subdestination 2";

	public ISearchSubdestination subdestination1;
	public ISearchSubdestination subdestination2;
	public Map<ISearchSubdestination, Boolean> subdestinationsActivityMap;

	private final static String SUPPORTING_TEXT_SEARCH_SEARCH_PROVIDER_ID = "SUPPORTING_TEXT_SEARCH_SEARCH_PROVIDER_ID";
	private final static String UNSUPPORTING_TEXT_SEARCH_SEARCH_PROVIDER_ID = "UNSUPPORTING_TEXT_SEARCH_SEARCH_PROVIDER_ID";
	private final static String SEARCH_PROVIDER_NO_DESTINATIONS_ID = "SEARCH_PROVIDER_NO_DESTINATIONS_ID";

	public ISearchProviderDescription supportingTextSearchProviderDescription;
	public ISearchProviderDescription unsupportingTextSearchProviderDescription;
	public ISearchProviderDescription searchProviderForObjTypeWithNoDestinations_Description;

	public ISearchProvider supportingTextSearchProviderInstance;
	public ISearchProvider unsupportingTextSearchProviderInstance;
	public ISearchProvider searchProviderForObjTypeWithNoDestinations_Instance;

	public ISearchProviderConfiguration searchProviderConfiguration;

	public GroupingHierarchy groupingHierarchy1;
	public GroupingHierarchy groupingHierarchy2;

	@Mock
	public IErrorHandler errorHandler;
	@Mock
	public ILongOperationRunner operationRunner;
	@Mock
	public IDiscoveryEnvironment environment;
	@Mock
	public ISessionManager<ISearchSession> sessionManager;
	@Mock
	public IViewUiContext viewUiContext;

	public NullProgressMonitor nullProgressMonitor;

	public SearchConsoleTestFixture()
	{
		MockitoAnnotations.initMocks(this);
		nullProgressMonitor = new NullProgressMonitor();
		setupEnvironment();
		setupUiContext();
		setupObjectTypes();
		setupDestinations();
		setupDestinationProviders();
		setupDestinationCategories();
		setupSubdestinations();
		setupGroupingHierarchy();
		setupProviderInstances();
		setupProviderDescriptions();
		searchProviderConfiguration = createTestSearchProviderConfig();
	}

	private void setupUiContext()
	{
		Mockito.stub(viewUiContext.getSecondColumnPosition()).toReturn(60);
		Mockito.stub(viewUiContext.controlsSpacing()).toReturn(60);
	}

	private void setupEnvironment()
	{
		Mockito.stub(environment.errorHandler()).toReturn(errorHandler);
		Mockito.stub(environment.operationRunner()).toReturn(operationRunner);
		Mockito.stub(environment.progressMonitor()).toReturn(nullProgressMonitor);
	}

	private void setupObjectTypes()
	{
		objectType1 = stubObjectType(OBJECT_TYPE_ID1, OBJECT_TYPE_NAME_1);
		objectType2 = stubObjectType(OBJECT_TYPE_ID2, OBJECT_TYPE_NAME_2);
		objectType3 = stubObjectType(OBJECT_TYPE_ID3, OBJECT_TYPE_NAME_3);
		objectTypeNoDestinations = stubObjectType(OBJECT_TYPE_NO_DESTINATIONS_ID, OBJECT_TYPE_NO_DESTINATIONS_NAME);
	}

	private IObjectTypeDescription stubObjectType(final String id, final String displayName)
	{
		return new ObjectTypeDescriptionBuilder().withId(id).withName(displayName).object();
	}

	private void setupDestinations()
	{
		destination1 = stubDestination(DESTINATION_NAME1);
		destination2 = stubDestination(DESTINATION_NAME2);
		destination3 = stubDestination(DESTINATION_NAME3);
		destination4 = stubDestination(DESTINATION_NAME4);
		destination5 = stubDestination(DESTINATION_NAME5);
	}

	private ISearchDestination stubDestination(final String displayName)
	{
		return new SearchDestinationBuilder().withDisplayName(displayName).object();
	}

	private void setupSubdestinations()
	{
		subdestination1 = stubSubdestination(SUBDESTINATION_NAME_1);
		subdestination2 = stubSubdestination(SUBDESTINATION_NAME_2);

		subdestinationsActivityMap = new HashMap<ISearchSubdestination, Boolean>();
		subdestinationsActivityMap.put(subdestination1, false);
		subdestinationsActivityMap.put(subdestination2, false);
	}

	private ISearchSubdestination stubSubdestination(final String displayName)
	{
		return new SubdestinationBuilder().withName(displayName).object();
	}

	private ISearchProviderConfiguration createTestSearchProviderConfig()
	{
		return new ISearchProviderConfiguration()
		{
			public void activateSubdestination(IObjectTypeDescription searchObjectType, IDestinationCategoryDescription destinationCategory, ISearchProviderDescription searchProviderDescription, ISearchSubdestination subDestination, boolean activate)
			{
				subdestinationsActivityMap.put(subDestination, new Boolean(activate));
			}

			public ISearchProviderDescription getActiveSearchProvider(IObjectTypeDescription objectType, IDestinationCategoryDescription destinationCategory) throws ProviderNotFoundException
			{
				if (destinationCategory == null)
				{
					if (objectType.getId().equals(objectTypeNoDestinations.getId()))
					{
						return searchProviderForObjTypeWithNoDestinations_Description;
					}
					else
					{
						throw new ProviderNotFoundException("Provider not found");
					}
				}
				if (objectType.getId().equals(objectType2.getId()) && (destinationCategory.getId().equals(CATEGORY_ID_2)))
				{
					return supportingTextSearchProviderDescription;
				}
				if (objectType.getId().equals(objectType1.getId()) && (destinationCategory.getId().equals(CATEGORY_ID_1)))
				{
					return unsupportingTextSearchProviderDescription;
				}
				if (objectType.getId().equals(objectType3.getId()) && (destinationCategory.getId().equals(CATEGORY_ID_3)))
				{
					return supportingTextSearchProviderDescription;
				}

				throw new ProviderNotFoundException("Provider not found");
			}

			public List<IDestinationCategoryDescription> getAvailableDestinationCategoriesForObjectType(IObjectTypeDescription objectType)
			{
				if (objectType.getId().equals(objectType1.getId()))
				{
					return Arrays.asList(new IDestinationCategoryDescription[] { category1 });
				}
				else if (objectType.getId().equals(objectType2.getId()))
				{
					return Arrays.asList(new IDestinationCategoryDescription[] { category2 });
				}
				else if (objectType.getId().equals(objectType3.getId()))
				{
					return Arrays.asList(new IDestinationCategoryDescription[] { category3 });
				}

				return Collections.emptyList();
			}

			public List<ISearchSubdestination> getAvailableSearchSubdestinations(IObjectTypeDescription objectType, IDestinationCategoryDescription destinationCategory, ISearchProviderDescription searchProvider)
			{
				if (objectType.getId().equals(OBJECT_TYPE_ID1))
				{
					return Arrays.asList(subdestination1, subdestination2);
				}
				return new ArrayList<ISearchSubdestination>();
			}

			public List<IDestinationCategoryDescription> getDestinationCategories()
			{
				return Arrays.asList(new IDestinationCategoryDescription[] { category1, category2, category3 });
			}

			public List<IDestinationCategoryDescription> getDestinationCategoriesForDestination(ISearchDestination destination) throws DestinationCategoryNotFoundException
			{
				if (destination.equals(destination1))
				{
					return Arrays.asList(new IDestinationCategoryDescription[] { category1 });
				}
				if (destination.equals(destination5))
				{
					return Arrays.asList(new IDestinationCategoryDescription[] { category3 });
				}
				else
				{
					return Arrays.asList(new IDestinationCategoryDescription[] { category2 });
				}
			}

			public boolean isSubdestinationActive(ISearchSubdestination subdestination, IObjectTypeDescription objectType, IDestinationCategoryDescription destCategory, ISearchProviderDescription searchProvider)
			{
				return subdestinationsActivityMap.get(subdestination);
			}

			public List<ISearchProviderDescription> getAvailableSearchProviderDescriptions(IObjectTypeDescription searchObjectType)
			{
				throw new UnsupportedOperationException("Unexpected invocation");
			}

			public List<IObjectTypeDescription> getObjectTypes()
			{
				return Arrays.asList(new IObjectTypeDescription[] { objectType1, objectType2, objectType3, objectTypeNoDestinations });
			}

			public List<IDestinationsProviderDescription> getDestinationProvidersForCategory(IDestinationCategoryDescription category)
			{
				if (category.equals(category1))
				{
					return Arrays.asList(new IDestinationsProviderDescription[] { destProviderDescr_cat1 });
				}
				if (category.equals(category2))
				{
					return Arrays.asList(new IDestinationsProviderDescription[] { destProviderDescr_cat2 });
				}
				else
				{
					return Arrays.asList(new IDestinationsProviderDescription[] { destProviderDescr_cat3 });
				}
			}

			public List<ISearchDestination> getSearchDestinations(IDestinationCategoryDescription category, IDestinationsProvider providerDescription)
			{
				return new ArrayList<ISearchDestination>(providerDescription.getSearchDestinations());
			}

			public List<IDestinationsProviderDescription> getAvailableDestinationProviders()
			{
				return Arrays.asList(new IDestinationsProviderDescription[] { destProviderDescr_cat1, destProviderDescr_cat2, destProviderDescr_cat3 });
			}
		};
	}

	private void setupGroupingHierarchy()
	{
		groupingHierarchy1 = new GroupingHierarchy("Grouping 1", "Grouping 1");
		groupingHierarchy2 = new GroupingHierarchy("Grouping 2", "Grouping 2");
	}

	private void setupProviderInstances()
	{
		supportingTextSearchProviderInstance = new SearchProviderBuilder().withGroupingHierarchies(groupingHierarchy1, groupingHierarchy2).object();
		unsupportingTextSearchProviderInstance = new SearchProviderBuilder().withGroupingHierarchies().object();
		searchProviderForObjTypeWithNoDestinations_Instance = new SearchProviderBuilder().withGroupingHierarchies().object();
	}

	private void setupProviderDescriptions()
	{
		supportingTextSearchProviderDescription = stubSearchProviderDescription(true, SUPPORTING_TEXT_SEARCH_SEARCH_PROVIDER_ID, supportingTextSearchProviderInstance, category2, category3);
		unsupportingTextSearchProviderDescription = stubSearchProviderDescription(false, UNSUPPORTING_TEXT_SEARCH_SEARCH_PROVIDER_ID, unsupportingTextSearchProviderInstance, category1);
		searchProviderForObjTypeWithNoDestinations_Description = stubSearchProviderDescription(true, SEARCH_PROVIDER_NO_DESTINATIONS_ID, searchProviderForObjTypeWithNoDestinations_Instance);
	}

	private ISearchProviderDescription stubSearchProviderDescription(final boolean supportsTextSearch, final String id, final ISearchProvider searchProvInstance, final IDestinationCategoryDescription... destCategories)
	{
		return new SearchProviderDescriptionBuilder().supportsTextSearch(supportsTextSearch).withSearchProviderInstance(searchProvInstance).withDestinationCategories(destCategories).withId(id).object();
	}

	private void setupDestinationCategories()
	{
		category1 = stubDestinationCategoryDescription(CATEGORY_ID_1, CATEGORY_NAME_1, UNSUPPORTING_TEXT_SEARCH_SEARCH_PROVIDER_ID);
		category2 = stubDestinationCategoryDescription(CATEGORY_ID_2, CATEGORY_NAME_2, SUPPORTING_TEXT_SEARCH_SEARCH_PROVIDER_ID);
		category3 = stubDestinationCategoryDescription(CATEGORY_ID_3, CATEGORY_NAME_3, SUPPORTING_TEXT_SEARCH_SEARCH_PROVIDER_ID);
	}

	private IDestinationCategoryDescription stubDestinationCategoryDescription(final String id, final String displayName, final String destProviderId)
	{
		return new DestinationCategoryDescriptionBuilder().withDestinationProviders(destProviderId).withId(id).withName(displayName).object();
	}

	private void setupDestinationProviders()
	{
		destProvider_cat1 = stubDestinationsProvider(destination1, destination2);
		destProvider_cat2 = stubDestinationsProvider(destination3, destination4);
		destProvider_cat3 = stubDestinationsProvider(destination5);

		destProviderDescr_cat1 = stubDestinationsProviderDescription(destProvider_cat1, TEST_SEARCH_DEST_PROVIDER_PREFERENCE_PAGE_ID);
		destProviderDescr_cat2 = stubDestinationsProviderDescription(destProvider_cat2, null);
		destProviderDescr_cat3 = stubDestinationsProviderDescription(destProvider_cat3, null);
	}

	private IDestinationsProvider stubDestinationsProvider(final ISearchDestination... destinations)
	{
		return new DestinationsProviderBuilder().withDestinations(destinations).object();
	}

	private IDestinationsProviderDescription stubDestinationsProviderDescription(final IDestinationsProvider destProviderInstance, final String prefPageId)
	{
		return new DestProviderDescriptionBuilder().withProvider(destProviderInstance).withParefPageId(prefPageId).object();
	}
}
