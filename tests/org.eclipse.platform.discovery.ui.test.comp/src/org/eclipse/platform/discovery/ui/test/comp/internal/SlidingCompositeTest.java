/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.SashOrientation;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.SlidingCompositePageObject;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.SlidingCompositePageObject.HiddenState;
import org.eclipse.swt.widgets.Display;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

@SuppressWarnings("nls")
public class SlidingCompositeTest
{
	private SlidingCompositePageObject slidingComposite;
	
	@Before
	public void setUp()
	{
		slidingComposite = new SlidingCompositePageObject();
		slidingComposite.open();
		makeCompositeHigh();
	}

	private void makeCompositeHigh()
	{
		slidingComposite.resize(200, 300);
	}
	
	private void makeCompositeWide()
	{
		slidingComposite.resize(500, 100);
	}
	
	@After
	public void tearDown()
	{
		slidingComposite.close();
	}

	@Test
	public void testResizeCauseReorientationIfWidthIsGreaterThanHeight() 
	{
		assertEquals("Sash is not horizontal",SashOrientation.HORIZONTAL, slidingComposite.getSashOrientation());
		
		makeCompositeWide();
		
		assertEquals("Sash is not vertical",SashOrientation.VERTICAL, slidingComposite.getSashOrientation());
	}

	
	@Test
	public void testMovingSashResizesBothParts()
	{
		final int MOVE_STEP = 15;

		slidingComposite.moveSashBy(-MOVE_STEP);
		int initialOffset = slidingComposite.getSashOffset();
		
		slidingComposite.moveSashBy(MOVE_STEP);
		int newOffset = slidingComposite.getSashOffset();
		
		int delta = newOffset - initialOffset;
		assertTrue("new offset should be bigger than initial offset because sash was dragged downwards", delta>0);
		
		int deviation = MOVE_STEP - delta;
		assertTrue("Unacceptable bottom composite size; Deviation: " + deviation, Math.abs(deviation) <= 2);
	}

	@Test
	public void testHidingUpperPart()
	{
		assertEquals("Unexpected hidden state", HiddenState.NOT_HIDDEN, slidingComposite.getHiddenState());
		
		slidingComposite.toggleHidden();
		
		assertEquals("Unexpected hidden state", HiddenState.HIDDEN, slidingComposite.getHiddenState());

	}

	@Test
	public void testShowingUpperPart()
	{
		assertEquals("Unexpected hidden state", HiddenState.NOT_HIDDEN, slidingComposite.getHiddenState());

		slidingComposite.toggleHidden();
		slidingComposite.toggleHidden();
		
		assertEquals("Unexpected hidden state", HiddenState.NOT_HIDDEN, slidingComposite.getHiddenState());

	}

	@Test
	public void testShowUpperPartPreservesSashPosition()
	{
		slidingComposite.moveSashBy(100);
		int initialOffset = slidingComposite.getSashOffset();

		slidingComposite.toggleHidden();
		slidingComposite.toggleHidden();
		
		int newOffset = slidingComposite.getSashOffset();

		assertEquals("Sash not put in the original location", initialOffset, newOffset);
	}

	@Test
	public void testSashCannotBeMovedLowerThanUpperComposite()
	{
		int initialSashOffset = slidingComposite.getSashOffset();
		slidingComposite.moveSashBy(-13);
		
		assertFalse(initialSashOffset==slidingComposite.getSashOffset());
		slidingComposite.moveSashBy(20);
		
		assertEquals("The sash location does not equal to initial location", initialSashOffset, slidingComposite.getSashOffset());
	}
	
	
	@Test
	public void testMovingSlashTooUpDoesNotHideButton() 
	{
		slidingComposite.moveSashBy(-10000);
		
		assertTrue("Hide button went out of shell", slidingComposite.isHideControlVisible());
	}
	
	
	@Test
	public void testResizingShellDoesNotHideButton()
	{
		final int width = 40;
		slidingComposite.resize(width, 600);
		slidingComposite.moveSashBy(600);

		// Set the shell height to a value which would normally hide the sash; The test would expect that the sliding composite would react to this
		// resize operation as placing the sash right above the progress monitor so that the user can operate with it
		final int newHeight = 50;
		slidingComposite.resize(width, newHeight);

		final int sashOffset = slidingComposite.getSashOffset();
		final int distanceFromCompositeBottom = newHeight - sashOffset;

		// The progress monitor is about 25 points plus 5 points tolerance
		assertTrue("Hide button is not at the bottom of the shell; delta = " + distanceFromCompositeBottom, distanceFromCompositeBottom < 30 && distanceFromCompositeBottom > 20);
	}	
	

	@Test
	public void testMovingSashOverProgressMonitor()
	{
		slidingComposite.resize(40, 50);
		// The sliding composite is now so small that the sash is right above the progress monitor. Get its current offset
		final int initialOffset = slidingComposite.getSashOffset();
		// Try moving the sash a bit
		slidingComposite.moveSashBy(10);
		// It is expected The sash offset was not affected since the composite should guarantee that the user cannot drag the sash over the progress monitor
		assertEquals("Sash unexpectedly moved probably over the progress monitor", initialOffset, slidingComposite.getSashOffset());
	}
	
	@Test
	public void testProgressMonitorCanBeCancelledOutOfUiThread()
	{
		assertNull("Test should be executed out of UI thread", Display.getCurrent());
		slidingComposite.cancelCurrentOperation();
	}
}
