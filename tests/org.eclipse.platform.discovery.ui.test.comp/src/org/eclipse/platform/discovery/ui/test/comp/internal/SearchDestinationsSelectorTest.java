/*******************************************************************************
 * Copyright (c) 2010, 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.platform.discovery.runtime.api.IDestinationsProvider;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.SearchDestinationsSelectorPageObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class SearchDestinationsSelectorTest
{
	private static final String DESTINATION_NAME_1 = "My First Destination";
	private static final String DESTINATION_NAME_2 = "My Second Destination";
	private static final String CATEGORY_NAME = "My Category";

	private List<ISearchDestination> destinationsToShow;
	private SearchDestinationsSelectorPageObject selector;
	private ISearchDestination destination_1;
	private ISearchDestination destination_2;

	@Before
	public void setUp()
	{
		destination_1 = mockDestination(DESTINATION_NAME_1);
		destination_2 = mockDestination(DESTINATION_NAME_2);
		destinationsToShow = new ArrayList<ISearchDestination>(Arrays.asList(new ISearchDestination[] { destination_1 }));
		selector = new SearchDestinationsSelectorPageObject(destinationsToShow, createDestinationsProvider(destination_1, destination_2));
		selector.open();
		selector.setInput(Arrays.asList(new IDestinationCategoryDescription[] { mockCategory() }));
	}

	@After
	public void tearDown()
	{
		selector.close();
	}

	@Test
	public void testUpdatingSelector()
	{
		selector.selectFirstPossibleDestination();
		verifyDisplayedItems(CATEGORY_NAME, DESTINATION_NAME_1);

		destinationsToShow.add(destination_2);
		selector.update();

		selector.selectFirstPossibleDestination();
		verifyDisplayedItems(CATEGORY_NAME, DESTINATION_NAME_1, DESTINATION_NAME_2);
	}

	@Test
	public void testUpdatingCurrentlySelectedItem()
	{
		selector.selectFirstPossibleDestination();
		verifyDisplayedItems(CATEGORY_NAME, DESTINATION_NAME_1);

		destinationsToShow.clear();
		destinationsToShow.add(destination_2);
		selector.update();
		selector.selectFirstPossibleDestination();

		selector.selectFirstPossibleDestination();
		verifyDisplayedItems(CATEGORY_NAME, DESTINATION_NAME_2);
	}

	@Test
	public void testSelectionIsPreservedWhenSameDestinationsAreShown()
	{
		destinationsToShow.add(destination_2);
		selector.update();
		selector.selectDestination(DESTINATION_NAME_2);

		destinationsToShow.remove(destination_1);
		selector.update();

		final ISearchDestination selectedDestination = selector.getSelectedDestination();
		assertSame("Destination 2 is not selected after update", destination_2, selectedDestination);
	}

	private void verifyDisplayedItems(final String category, final String... destinations)
	{
		final List<String> itemNames = selector.getAllItemsNames();
		final int expectedItemsCount = destinations.length + 1;
		assertEquals(MessageFormat.format("{0} items expected", expectedItemsCount), expectedItemsCount, itemNames.size());
		assertTrue(MessageFormat.format("Destination category '{0}' not found", category), itemNames.contains(category));
		for (String destName : destinations)
		{
			assertTrue(MessageFormat.format("Destinaion '{0}' not shown", destName), itemNames.contains(destName));
		}
	}

	private IDestinationCategoryDescription mockCategory()
	{
		final IDestinationCategoryDescription cat = Mockito.mock(IDestinationCategoryDescription.class);
		Mockito.stub(cat.getDisplayName()).toReturn(CATEGORY_NAME);
		return cat;
	}

	private IDestinationsProvider createDestinationsProvider(final ISearchDestination... destinations)
	{
		final Set<ISearchDestination> destinationSet = new HashSet<ISearchDestination>(Arrays.asList(destinations));
		final IDestinationsProvider provider = Mockito.mock(IDestinationsProvider.class);
		Mockito.stub(provider.getSearchDestinations()).toReturn(destinationSet);
		return provider;
	}

	private ISearchDestination mockDestination(String destinationName)
	{
		final ISearchDestination dest = Mockito.mock(ISearchDestination.class);
		Mockito.stub(dest.getDisplayName()).toReturn(destinationName);
		return dest;
	}
}
