/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot;

import org.eclipse.platform.discovery.ui.api.ISearchParametersUI.IConsoleContext;
import org.eclipse.platform.discovery.ui.internal.view.impl.SubdestinationsSelectedListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swtbot.swt.finder.SWTBot;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.swtbot.swt.finder.finders.UIThreadRunnable;
import org.eclipse.swtbot.swt.finder.results.Result;
import org.eclipse.swtbot.swt.finder.results.VoidResult;
import org.eclipse.swtbot.swt.finder.utils.SWTUtils;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotCheckBox;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotShell;

/**
 * SWTBot for subdestination selector
 * 
 * @author Danail Branekov
 * 
 */
public class SWTBotSubdSelector extends SWTBotShell
{
	private static SubdestinationsSelectedListener subdestinationsListener;

	private SWTBotSubdSelector(Shell shell) throws WidgetNotFoundException
	{
		super(shell);
	}

	public static SWTBotSubdSelector open(final SWTBotShell parent, final IConsoleContext consoleContext)
	{
		final SWTBot bot = new SWTBot();
		subdestinationsListener = new SubdestinationsSelectedListener(parent.widget, consoleContext);
		UIThreadRunnable.syncExec(new VoidResult()
		{
			@Override
			public void run()
			{
				subdestinationsListener.widgetSelected(createSelectionEvent());
			}
		});
		return new SWTBotSubdSelector(getShell(bot.getFocusedWidget()));
	}

	public SWTBotCheckBox subdestinationCheckbox(final String subdestName)
	{
		return new SWTBotCheckboxWithMouseInteraction(bot().checkBox(subdestName).widget);
	}

	private static SelectionEvent createSelectionEvent()
	{
		final Rectangle rect = clientArea(display().getActiveShell());
		final Event e = new Event();
		e.x = rect.x + (rect.width / 2);
		e.y = rect.y + (rect.height / 2);
		e.widget = display().getActiveShell();

		return new SelectionEvent(e);
	}

	private static Rectangle clientArea(final Shell shell)
	{
		return UIThreadRunnable.syncExec(new Result<Rectangle>()
		{
			@Override
			public Rectangle run()
			{
				return shell.getClientArea();
			}
		});
	}

	private static Shell getShell(final Control control)
	{
		return UIThreadRunnable.syncExec(new Result<Shell>()
		{
			@Override
			public Shell run()
			{
				return control.getShell();
			}
		});
	}

	private static Display display()
	{
		return SWTUtils.display();
	}
}
