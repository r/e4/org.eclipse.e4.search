/*******************************************************************************
 * Copyright (c) 2010, 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.platform.discovery.runtime.api.IDisplayableObject;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.InteractiveComboSelectorPageObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class InteractiveComboSelectorTest
{
	private static final String OBJECT1_LABEL = "Object1";
	private static final String OBJECT2_LABEL = "Object2";

	private IDisplayableObject object1;
	private IDisplayableObject object2;
	private List<IDisplayableObject> objectsSet;
	private InteractiveComboSelectorPageObject selector;

	@Before
	public void setUp()
	{
		object1 = mockDisplayableObject(OBJECT1_LABEL);
		object2 = mockDisplayableObject(OBJECT2_LABEL);
		objectsSet = new ArrayList<IDisplayableObject>();
		objectsSet.add(object1);
		objectsSet.add(object2);
		selector = new InteractiveComboSelectorPageObject(objectsSet);
		selector.open();
	}

	private IDisplayableObject mockDisplayableObject(final String name)
	{
		final IDisplayableObject dispObject = Mockito.mock(IDisplayableObject.class);
		Mockito.stub(dispObject.getDisplayName()).toReturn(name);
		return dispObject;
	}

	@After
	public void tearDown()
	{
		selector.close();
	}

	@Test
	public void testSelectorIsDisabledWhenEmptyInputIsSet()
	{
		assertTrue("Selector should be enabled by default", selector.isEnabled());
		selector.setInput(new ArrayList<IDisplayableObject>());
		assertFalse("Selector should be disabled for empty input", selector.isEnabled());

		selector.setInput(objectsSet);
		assertTrue("Selector should be enabled", selector.isEnabled());
	}

	@Test
	public void testSelectorPreservesSelectedItemOnSelectionChange()
	{
		selector.select(OBJECT2_LABEL);
		objectsSet.remove(object1);
		selector.setInput(objectsSet);
		assertEquals("Unexpected object selected", OBJECT2_LABEL, selector.getSelection());
	}
}
