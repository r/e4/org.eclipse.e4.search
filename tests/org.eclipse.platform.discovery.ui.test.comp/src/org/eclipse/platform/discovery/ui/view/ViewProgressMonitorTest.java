/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.view;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.ViewProgressMonitorPageObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ViewProgressMonitorTest
{
	private ViewProgressMonitorPageObject monitor;

	@Before
	public void setUp()
	{
		monitor = new ViewProgressMonitorPageObject();
		monitor.open();
	}

	@After
	public void tearDown()
	{
		monitor.close();
	}

	@Test
	public void testBeginTaskDisablesUI()
	{
		monitor.beginTask("MyTask", IProgressMonitor.UNKNOWN);
		assertFalse("Button should be disabled while the task is running", monitor.isControlsEnabled());
		monitor.done();
		assertTrue("Button should be enabled when the task is done", monitor.isControlsEnabled());
	}

	@Test
	public void testPressingCancelButtonCancelsMonitor()
	{
		monitor.beginTask("MyTask", IProgressMonitor.UNKNOWN);
		monitor.cancel();
		assertTrue("Progress monitor is not cancelled", monitor.isCanceled());
	}
}
