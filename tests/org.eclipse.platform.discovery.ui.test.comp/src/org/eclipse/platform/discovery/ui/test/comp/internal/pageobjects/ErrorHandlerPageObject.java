package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.platform.discovery.ui.api.impl.ErrorHandler;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot.SwtBotUtils;
import org.eclipse.platform.discovery.util.api.env.IErrorHandler;
import org.eclipse.platform.discovery.util.internal.logging.ILogger;
import org.eclipse.swtbot.eclipse.finder.waits.Conditions;
import org.eclipse.swtbot.swt.finder.SWTBot;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotShell;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotText;
import org.mockito.Mockito;

public class ErrorHandlerPageObject {
	
	private final SwtBotUtils botUtils;
	private final SWTBot bot;

	private final IErrorHandler errorHandler;
	private final LoggerPageObject logger;

	private SWTBotShell errorShell;
	
	public ErrorHandlerPageObject() {
		
		final ILogger loggerMock = Mockito.mock(ILogger.class);
		logger = new LoggerPageObject(loggerMock);
		
		errorHandler = new ErrorHandler() {
			@Override
			protected ILogger logger() {
				return loggerMock;
			}
		};
		
		bot = new SWTBot();
		
		botUtils = new SwtBotUtils();
	}

	public boolean isOpen() {
		return errorShell!=null;
	}

	public void close() {
		errorShell.bot().button(IDialogConstants.OK_LABEL).click();
		bot.waitUntil(Conditions.shellCloses(errorShell));
		errorShell = null;
	}

	public void showError(String title, String details) {
		errorHandler.showError(title, details);
		errorShell = bot.shell(title);
		
	}

	public String getTitleShown() {
		return errorShell.getText();
	}



	public String getDetailsShown() {
			SWTBotText text = detailsText();
			return text.getText();
	}

	private SWTBotText detailsText() {
		return errorShell.bot().text();
	}


	public boolean isDetailsReadOnly() {
		return botUtils.isReadOnly(detailsText());
	}
	
	public LoggerPageObject getLogger() {
		return logger;
	}

	public void handleException(Exception exception, String errorDialogTitle) {
		errorHandler.handleException(errorDialogTitle, exception);
		errorShell = bot.shell(errorDialogTitle);
	}
	
	public static class LoggerPageObject {
		
		private final ILogger loggerMock;

		private LoggerPageObject(ILogger mock) {
			this.loggerMock = mock;
		}

		public void checkExceptionWasLogged(Exception ex) {
			verify(loggerMock, times(1)).logError(anyString(), same(ex));
			verifyNoMoreInteractions(loggerMock);
		}
	}
	
}
