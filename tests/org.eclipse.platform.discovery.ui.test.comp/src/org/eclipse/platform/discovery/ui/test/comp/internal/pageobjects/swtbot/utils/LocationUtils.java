/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.swtbot.swt.finder.finders.UIThreadRunnable;
import org.eclipse.swtbot.swt.finder.results.Result;
import org.eclipse.swtbot.swt.finder.widgets.AbstractSWTBot;
import org.eclipse.swtbot.swt.finder.widgets.AbstractSWTBotControl;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTreeItem;

class LocationUtils
{
    static <T extends Widget> Rectangle absoluteLocation(final AbstractSWTBot<T> item) {
        AbstractSWTBot<?> bot = botFor(item);
        Object result = null;
        try {
            Method m = AbstractSWTBot.class.getDeclaredMethod("absoluteLocation");
            m.setAccessible(true);
            result = m.invoke(bot);
        } catch (SecurityException e) {
            // do nothing
        } catch (NoSuchMethodException e) {
            // do nothing
        } catch (IllegalArgumentException e) {
            // do nothing
        } catch (IllegalAccessException e) {
            // do nothing
        } catch (InvocationTargetException e) {
            // do nothing
        }
        return (Rectangle) result;
    }
    
	private static AbstractSWTBot<?> botFor(final AbstractSWTBot<?> bot) {
        if (bot instanceof SWTBotTreeItem) {
            return new SWTBotTreeItemForDnd(((SWTBotTreeItem) bot).widget);
        } 
        if(bot instanceof AbstractSWTBot) {
        	final Widget w = bot.widget;
        	if(w instanceof Control) {
        		return new AbstractSWTBotControl<Control>((Control)w);
        	}
        }
        
        return bot;
    }
    
    /**
     * Subclass to return the correct absolute location.
     * 
     * @author mchauvin
     */
    private static class SWTBotTreeItemForDnd extends SWTBotTreeItem {

        public SWTBotTreeItemForDnd(TreeItem treeItem) throws WidgetNotFoundException {
            super(treeItem);
        }

        @Override
        protected Rectangle absoluteLocation() {
            return UIThreadRunnable.syncExec(new Result<Rectangle>() {
                public Rectangle run() {
                    return display.map(widget.getParent(), null, widget.getBounds());
                }
            });
        }

    }
}
