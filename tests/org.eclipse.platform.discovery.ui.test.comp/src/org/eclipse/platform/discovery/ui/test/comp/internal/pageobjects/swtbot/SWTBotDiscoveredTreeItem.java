/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot;

import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot.utils.MouseUtils;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.swtbot.swt.finder.utils.SWTUtils;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTreeItem;

/**
 * SWTBot which provides features specific to items displayed in the result section of the search console
 * @author Danail Branekov
 */
public class SWTBotDiscoveredTreeItem extends SWTBotTreeItem
{
	private final MouseUtils mouseUtils;

	public SWTBotDiscoveredTreeItem(final TreeItem treeItem) throws WidgetNotFoundException
	{
		super(treeItem);
		this.mouseUtils = new MouseUtils(SWTUtils.display());
	}

	public void hoverMouse()
	{
		mouseUtils().hoverOn(this);
	}

	public void moveMouseAway()
	{
		mouseUtils().moveAwayFrom(this);
	}

	private MouseUtils mouseUtils()
	{
		return mouseUtils;
	}
}
