/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal;

import static org.junit.Assert.*;

import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.TabbedSessionDisplayerPageObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class TabbedSessionDisplayerTest
{
	private final TabbedSessionDisplayerPageObject tabbedSessionDisplayer = new TabbedSessionDisplayerPageObject();
	
	private static final String NON_NAVIGATABLE_SESSION_ID = "not navigatable";
	private static final String NON_NAVIGATABLE_HISTORY_ITEM = "nonnavigatable history item"; 

	private static final String NAVIGATABLE_SESSION_ID = "navigatable";
	private static final String NAVIGATABLE_HISTORY_ITEM_1 = "navigatable history item 1"; 
	private static final String NAVIGATABLE_HISTORY_ITEM_2 = "navigatable history item 2"; 

	@Before
	public void setUp() {
		tabbedSessionDisplayer.addSession(NAVIGATABLE_SESSION_ID, NAVIGATABLE_HISTORY_ITEM_1, NAVIGATABLE_HISTORY_ITEM_2);
		tabbedSessionDisplayer.addUnnavigableSession(NON_NAVIGATABLE_SESSION_ID, NON_NAVIGATABLE_HISTORY_ITEM);
	}
	
	@After
	public void tearDown() {
		tabbedSessionDisplayer.tearDown();
	}
	
	@Test
	public void tabsOpen() {
		tabbedSessionDisplayer.displaySessions(new String[]{NAVIGATABLE_SESSION_ID, NON_NAVIGATABLE_SESSION_ID}, new boolean[]{true, true});
		
		assertTrue("Tab should be open",tabbedSessionDisplayer.isSessionOpen(NAVIGATABLE_SESSION_ID));
		assertTrue("Tab should be open",tabbedSessionDisplayer.isSessionOpen(NON_NAVIGATABLE_SESSION_ID));
	}

	@Test
	public void nextAndPrevoiusAreDisplayedForNavigableSession()
	{
		tabbedSessionDisplayer.displaySessions(new String[]{NAVIGATABLE_SESSION_ID}, new boolean[]{true});

		assertTrue("Could not find previous button", tabbedSessionDisplayer.isNavigateToNextAvailable());
		assertTrue("Could not find next button", tabbedSessionDisplayer.isNavigateToPreviousAvailable());
	}

	@Test
	public void nextAndPrevoiusAreNotDisplayedForNonNavigableSession()
	{
		tabbedSessionDisplayer.displaySessions(new String[]{NON_NAVIGATABLE_SESSION_ID}, new boolean[]{true});

		assertFalse("Could not find previous button", tabbedSessionDisplayer.isNavigateToNextAvailable());
		assertFalse("Could not find next button", tabbedSessionDisplayer.isNavigateToPreviousAvailable());
	}
	
	@Test
	public void uncloseableTab()
	{
		tabbedSessionDisplayer.displaySessions(new String[]{NON_NAVIGATABLE_SESSION_ID}, new boolean[]{false});
		
		tabbedSessionDisplayer.closeSession(NON_NAVIGATABLE_SESSION_ID);
		
		assertTrue("Tab should still be open because it is not closeable",tabbedSessionDisplayer.isSessionOpen(NON_NAVIGATABLE_SESSION_ID));
	}
	
	@Test
	public void closeableTab()
	{
		tabbedSessionDisplayer.displaySessions(new String[]{NON_NAVIGATABLE_SESSION_ID}, new boolean[]{true});
		
		tabbedSessionDisplayer.closeSession(NON_NAVIGATABLE_SESSION_ID);
	
		assertFalse("Tab was not closed", tabbedSessionDisplayer.isSessionOpen(NON_NAVIGATABLE_SESSION_ID));
	}

	@Test
	public void navButtonsDisappearOnSelectingNonNavigableSession()
	{
		tabbedSessionDisplayer.displaySessions(new String[]{NAVIGATABLE_SESSION_ID, NON_NAVIGATABLE_SESSION_ID}, new boolean[]{false, false});
		tabbedSessionDisplayer.selectSession(NAVIGATABLE_SESSION_ID);
		
		tabbedSessionDisplayer.selectSession(NON_NAVIGATABLE_SESSION_ID);
		
		assertFalse("Session is not navigable, buttons not expected", tabbedSessionDisplayer.isNavigateToPreviousAvailable());
		assertFalse("Session is not navigable, buttons not expected", tabbedSessionDisplayer.isNavigateToNextAvailable());
	}
	
	@Test
	public void navButtonsReappearOnSelectingNavigableSession()
	{
		tabbedSessionDisplayer.displaySessions(new String[]{NAVIGATABLE_SESSION_ID, NON_NAVIGATABLE_SESSION_ID}, new boolean[]{false, false});
		
		tabbedSessionDisplayer.selectSession(NON_NAVIGATABLE_SESSION_ID);
		tabbedSessionDisplayer.selectSession(NAVIGATABLE_SESSION_ID);
		
		assertTrue("Session is navigable, but nav buttons are not available", tabbedSessionDisplayer.isNavigateToPreviousAvailable());
		assertTrue("Session is navigable, but nav buttons are not available", tabbedSessionDisplayer.isNavigateToNextAvailable());
	}
	
	@Test
	public void navigateHistory()
	{
		tabbedSessionDisplayer.displaySessions(new String[]{NAVIGATABLE_SESSION_ID}, new boolean[]{false});
		
		assertFalse("Previous button should be disabled by default", tabbedSessionDisplayer.isNavigateToPreviousEnabled());
		assertTrue("Next button should be enabled by default", tabbedSessionDisplayer.isNavigateToNextEnabled());
		
		tabbedSessionDisplayer.navigateToNext();
		assertTrue("Previous button should be enabled now", tabbedSessionDisplayer.isNavigateToPreviousEnabled());
		assertFalse("Next button should be disabled now", tabbedSessionDisplayer.isNavigateToNextEnabled());
		
		tabbedSessionDisplayer.navigateToPrevious();
		assertFalse("Previous button should be disabled now", tabbedSessionDisplayer.isNavigateToPreviousEnabled());
		assertTrue("Next button should be enabled now", tabbedSessionDisplayer.isNavigateToNextEnabled());
	}
}
