/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot;

import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot.utils.MouseUtils;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.swtbot.swt.finder.widgets.AbstractSWTBot;
import org.eclipse.swtbot.swt.finder.widgets.AbstractSWTBotControl;
import org.eclipse.ui.forms.widgets.AbstractHyperlink;

public class SWTBotHyperlink extends AbstractSWTBotControl<AbstractHyperlink>
{
	private final MouseUtils mouseUtils;

	public SWTBotHyperlink(AbstractHyperlink h) throws WidgetNotFoundException
	{
		super(h);
		this.mouseUtils = new MouseUtils(display);
	}

	public AbstractSWTBot<AbstractHyperlink> click()
	{
		mouseUtils.clickOn(this);
		return this;
	}

}
