/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot;

import static org.eclipse.swtbot.swt.finder.finders.UIThreadRunnable.syncExec;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot.utils.RecursiveSiblingFinder;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.swtbot.swt.finder.SWTBot;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.swtbot.swt.finder.finders.ChildrenControlFinder;
import org.eclipse.swtbot.swt.finder.finders.ControlFinder;
import org.eclipse.swtbot.swt.finder.utils.SWTUtils;
import org.eclipse.swtbot.swt.finder.widgets.AbstractSWTBot;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotShell;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;

public class SwtBotUtils {
	
	public boolean isReadOnly(AbstractSWTBot<? extends Widget> widget) {
		return SWTUtils.hasStyle(widget.widget, SWT.READ_ONLY);
	}
	
	public void setSize(final AbstractSWTBot<? extends Control> control, final int width, final int height) {
		control.display.syncExec(new Runnable() {
			@Override
			public void run() {
				control.widget.setSize(width, height);
			}
		});
	}
	
	public <ResultType extends Widget> ResultType findOneChildControlOfType(Widget parent, Class<ResultType> clazz, boolean shouldBeVisible)
			throws WidgetNotFoundException, MultipleWidgetsFoundException {
		return findOneChild(new ChildrenControlFinder(parent), new InstanceOf<ResultType>(clazz), shouldBeVisible);
	}
	
	public <ResultType extends Widget> ResultType findOneChildControlOfExactType(Widget parent, Class<ResultType> clazz, boolean shouldBeVisible)
			throws WidgetNotFoundException, MultipleWidgetsFoundException {
		return findOneChild(new ChildrenControlFinder(parent), new ClassEquals<ResultType>(clazz), shouldBeVisible);
	}
	
	public <ResultType extends Widget> ResultType findOneChildControl(Widget parent, Matcher<ResultType> matcher, boolean shouldBeVisible)
			throws WidgetNotFoundException, MultipleWidgetsFoundException {
		return findOneChild(new ChildrenControlFinder(parent), matcher, shouldBeVisible);
	}
	
	private <ResultType extends Widget> ResultType findOneChild(ControlFinder finder, Matcher<ResultType> matcher, boolean shouldBeVisible) {
		finder.shouldFindInVisibleControls = !shouldBeVisible;
		List<ResultType> result = finder.findControls(matcher);
		if(result.size()==0) {
			throw new WidgetNotFoundException(null);
		}
		if(result.size()>1) {
			throw new MultipleWidgetsFoundException(result.toString());
		}
		return result.get(0);
	}
	
	public SWTBotShell findShell(final Matcher<Shell> shellMather)	{
		final Collection<SWTBotShell> foundShells = new LinkedList<SWTBotShell>();
		for(SWTBotShell shell : new SWTBot().shells()) {
			if(shellMather.matches(shell)) {
				foundShells.add(shell);
			}
		}
		if(foundShells.size() == 0) {
			throw new WidgetNotFoundException(null);
		}
		if(foundShells.size() > 1) {
			throw new MultipleWidgetsFoundException(null);
		}
		return foundShells.iterator().next();
	}
	
	/**
	 * Finds a widget of a given type which belongs to the same composite as the widget specified 
	 * @param siblingWidget the widget which belongs to the composite to search in
	 * @param targetClass the type of the widget to search for
	 * @return the sibling widget found
	 * @throws WidgetNotFoundException if no such sibling is found
	 */
	@SuppressWarnings("unchecked")
	public <T extends Widget> T findSibling(final AbstractSWTBot<? extends Control> siblingWidget, final Class<T> targetClass) {
		final Matcher<T> instanceofMatcher = new InstanceOf<T>(targetClass);
		for (Widget sibling : syncExec(new RecursiveSiblingFinder(siblingWidget.widget))) {
			if(sibling == siblingWidget.widget) {
				// Not interested in the widget passed as parameter
				continue;
			}
			
			if(instanceofMatcher.matches(sibling)) {
				return (T) sibling;
			}
		}

		throw new WidgetNotFoundException(MessageFormat.format("No sibling to {0} of type {1} not found", siblingWidget.toString(), targetClass.getSimpleName()));
	}
	
	private static class InstanceOf <T extends Widget> extends BaseMatcher<T> {
		private final Class<T> clazz;

		public InstanceOf(Class<T> clazz) {
			this.clazz = clazz;
		}

		@Override
		public boolean matches(Object item) {
			return clazz.isAssignableFrom(item.getClass());
		}

		@Override
		public void describeTo(Description description) {
		}
		
	}
	
	private static class ClassEquals <T extends Widget> extends BaseMatcher<T> {
		private final Class<T> clazz;

		public ClassEquals(Class<T> clazz) {
			this.clazz = clazz;
		}

		@Override
		public boolean matches(Object item) {
			return clazz.equals(item.getClass());
		}

		@Override
		public void describeTo(Description description) {
		}
		
	}
}
