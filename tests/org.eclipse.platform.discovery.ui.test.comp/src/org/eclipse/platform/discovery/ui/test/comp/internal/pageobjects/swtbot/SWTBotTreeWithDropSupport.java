/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot;

import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.dnd.DropTargetListener;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotTree;

public class SWTBotTreeWithDropSupport extends SWTBotTree
{
	private final TreeViewer viewer;

	public SWTBotTreeWithDropSupport(final TreeViewer viewer) throws WidgetNotFoundException
	{
		super(viewer.getTree());
		this.viewer = viewer;
	}
	
	public void addDropSupport(final int operations, final Transfer[] transferTypes, final DropTargetListener listener)
	{
		viewer.addDropSupport(operations, transferTypes, listener);
	}
}
