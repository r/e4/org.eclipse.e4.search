/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot;

import org.eclipse.swt.widgets.Text;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.swtbot.swt.finder.results.StringResult;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotText;

public class SWTBotTextWithMessage extends SWTBotText
{
	public SWTBotTextWithMessage(Text w) throws WidgetNotFoundException
	{
		super(w);
	}

	public String getMessage()
	{
		return syncExec(new StringResult()
		{
			@Override
			public String run()
			{
				return widget.getMessage();
			}
		});
	}

}
