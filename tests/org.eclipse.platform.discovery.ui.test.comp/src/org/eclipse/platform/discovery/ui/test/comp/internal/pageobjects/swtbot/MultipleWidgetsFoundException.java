package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot;

/**
 * Thrown to indicate multiple widgets were found when only one was expected.
 * This seems to be missing from the current version of SWTBot.
 */
@SuppressWarnings("serial")
public class MultipleWidgetsFoundException extends RuntimeException {
	public MultipleWidgetsFoundException(String message) {
		super(message);
	}
}
