/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp;

import org.eclipse.platform.discovery.ui.test.comp.internal.AdvancedSearchParamsDisplayerTest;
import org.eclipse.platform.discovery.ui.test.comp.internal.ComboSelectorTest;
import org.eclipse.platform.discovery.ui.test.comp.internal.DiscoveredItemsTreeTest;
import org.eclipse.platform.discovery.ui.test.comp.internal.ErrorHandlerTest;
import org.eclipse.platform.discovery.ui.test.comp.internal.InteractiveComboSelectorTest;
import org.eclipse.platform.discovery.ui.test.comp.internal.OpenPropsViewActionTest;
import org.eclipse.platform.discovery.ui.test.comp.internal.SearchConsoleViewTest;
import org.eclipse.platform.discovery.ui.test.comp.internal.SearchDestinationsSelectorTest;
import org.eclipse.platform.discovery.ui.test.comp.internal.SearchFavoritesViewTest;
import org.eclipse.platform.discovery.ui.test.comp.internal.SlidingCompositeHorizontalTest;
import org.eclipse.platform.discovery.ui.test.comp.internal.SlidingCompositeTest;
import org.eclipse.platform.discovery.ui.test.comp.internal.SubdestinationsSelectorTest;
import org.eclipse.platform.discovery.ui.test.comp.internal.TabbedSessionDisplayerTest;
import org.eclipse.platform.discovery.ui.test.comp.internal.TextControlTest;
import org.eclipse.platform.discovery.ui.view.ViewProgressMonitorTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ErrorHandlerTest.class, SlidingCompositeTest.class, SlidingCompositeHorizontalTest.class, TabbedSessionDisplayerTest.class, OpenPropsViewActionTest.class, ComboSelectorTest.class, 
		TextControlTest.class, SearchDestinationsSelectorTest.class, InteractiveComboSelectorTest.class, ViewProgressMonitorTest.class, SubdestinationsSelectorTest.class, 
		DiscoveredItemsTreeTest.class, AdvancedSearchParamsDisplayerTest.class, SearchFavoritesViewTest.class, SearchConsoleViewTest.class })
public class AllSWTBotTests {

}
