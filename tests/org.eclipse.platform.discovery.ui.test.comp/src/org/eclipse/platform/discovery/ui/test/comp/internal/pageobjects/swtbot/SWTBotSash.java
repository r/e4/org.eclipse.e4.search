/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot;

import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.SashOrientation;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot.utils.DndUtil;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.swtbot.swt.finder.results.Result;
import org.eclipse.swtbot.swt.finder.widgets.AbstractSWTBot;
import org.eclipse.swtbot.swt.finder.widgets.AbstractSWTBotControl;
import org.hamcrest.SelfDescribing;

public class SWTBotSash extends AbstractSWTBotControl<Sash>{
	
	private SWTBotSash(Sash sash) throws WidgetNotFoundException {
		super(sash);
	}
	
	private SWTBotSash(Sash sash, SelfDescribing selfDescribing) {
		super(sash, selfDescribing);
	}
	
	public static SWTBotSash sash(AbstractSWTBot<? extends Composite> parent, boolean shouldBeVisible) {
		return new SWTBotSash(new SwtBotUtils().findOneChildControlOfType(parent.widget, Sash.class, shouldBeVisible));
	}
	
	public SashOrientation getOrientation() {
		Rectangle bounds = getBounds();
		if(bounds.height<bounds.width) {
			return SashOrientation.HORIZONTAL;
		}else{
			return SashOrientation.VERTICAL;
		}
	}
	
	private Rectangle getBounds() {
		return syncExec(new Result<Rectangle>() {
			@Override
			public Rectangle run() {
				return widget.getBounds();
			}
		});
	}
	
	public void move(final int offSet) {
		Point absoluteSashCenter = widgetCenterAbsolute();
		int targetX = getOrientation().equals(SashOrientation.VERTICAL) ? absoluteSashCenter.x + offSet : absoluteSashCenter.x;
		int targetY = getOrientation().equals(SashOrientation.VERTICAL) ? absoluteSashCenter.y : absoluteSashCenter.y + offSet;

		new DndUtil(display).dragAndDrop(this, new Point(targetX, targetY));
	}
	
	private Point widgetCenterAbsolute() {
		return widgetCenter(absoluteLocation());
	}
	
	private Point widgetCenterRelative() {
		return widgetCenter(getBounds());
	}
	
	private Point widgetCenter(Rectangle widgetlocation) {
		int x = widgetlocation.x + widgetlocation.width / 2;
		int y = widgetlocation.y + widgetlocation.height / 2;
		return new Point(x, y);
	}
	
	public int getOffset() {
		Point center = widgetCenterRelative();
		return getOrientation().equals(SashOrientation.VERTICAL) ? Math.abs(center.x) : Math.abs(center.y);
	}
	
}
	
