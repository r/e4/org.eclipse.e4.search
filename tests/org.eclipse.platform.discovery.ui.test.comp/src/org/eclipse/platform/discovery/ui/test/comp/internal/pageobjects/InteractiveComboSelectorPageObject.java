/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects;

import static org.eclipse.swtbot.swt.finder.finders.UIThreadRunnable.syncExec;

import java.util.List;

import org.eclipse.platform.discovery.runtime.api.IDisplayableObject;
import org.eclipse.platform.discovery.testutils.utils.pageobjects.InShellPageObject;
import org.eclipse.platform.discovery.ui.internal.selector.InteractiveComboSelector;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swtbot.swt.finder.results.VoidResult;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotCombo;
import org.eclipse.ui.forms.widgets.FormToolkit;

public class InteractiveComboSelectorPageObject extends InShellPageObject
{
	private static final String COMBO_LABEL = "TEST COMBO";
	private final List<IDisplayableObject> objectsToDisplay;
	private InteractiveComboSelector<IDisplayableObject> selector;

	public InteractiveComboSelectorPageObject(final List<IDisplayableObject> objectsToDisplay)
	{
		this.objectsToDisplay = objectsToDisplay;
	}

	@Override
	protected void createContent(final Shell parent, final FormToolkit formToolkit)
	{
		selector = new InteractiveComboSelector<IDisplayableObject>(parent, formToolkit, this.objectsToDisplay, COMBO_LABEL, 70, null);
	}

	public boolean isEnabled()
	{
		return bot().comboBox().isEnabled();
	}

	public void setInput(final List<IDisplayableObject> input)
	{
		syncExec(new VoidResult()
		{
			@Override
			public void run()
			{
				selector.setInput(input);
			}
		});
	}

	public void select(final String item)
	{
		combo().setSelection(item);
	}

	public String getSelection()
	{
		return combo().getText();
	}

	private SWTBotCombo combo()
	{
		return bot().comboBox();
	}
}
