package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot.workarounds;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.swtbot.swt.finder.resolvers.DefaultChildrenResolver;
import org.eclipse.swtbot.swt.finder.resolvers.IChildrenResolver;

public class MyChildrentResolver extends DefaultChildrenResolver {
	public MyChildrentResolver() {
		super();
		resolver.addResolver(new CtabChildrenResolver());
	}
	
	@Override
	public List<Widget> getChildren(Widget w) {
		List<Widget> result = new ArrayList<Widget>();

		if (!hasChildren(w))
			return result;

		List<?> resolvers = resolver.getResolvers(w.getClass());

		for (Iterator<?> iterator = resolvers.iterator(); iterator.hasNext();) {
			IChildrenResolver resolver = (IChildrenResolver) iterator.next();
			if (resolver.canResolve(w) && resolver.hasChildren(w)) {
				List<Widget> children = resolver.getChildren(w);
				if (children != null) {
					result.addAll(children);
				}
			}
		}
		return result;
	}
	
	private class CtabChildrenResolver implements IChildrenResolver {

		@Override
		public boolean canResolve(Widget w) {
			return w instanceof CTabFolder;
		}

		@Override
		public Class<?>[] getResolvableClasses() {
			return new Class<?>[]{CTabFolder.class};
		}

		@Override
		public List<Widget> getChildren(Widget w) {
			List<Widget> widgets = new ArrayList<Widget>();
			widgets.addAll(Arrays.asList(((Composite) w).getChildren()));
			return widgets;
		}

		@Override
		public boolean hasChildren(Widget w) {
			return true;
		}
		
	}
}
