package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.platform.discovery.testutils.utils.pageobjects.InShellPageObject;
import org.eclipse.platform.discovery.ui.internal.plugin.DiscoveryUIMessages;
import org.eclipse.platform.discovery.ui.internal.view.result.impl.TabbedSessionDisplayer;
import org.eclipse.platform.discovery.ui.internal.view.result.impl.TabbedSessionDisplayer.UI;
import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot.workarounds.MyBot;
import org.eclipse.platform.discovery.util.internal.session.HistoryTrack;
import org.eclipse.platform.discovery.util.internal.session.IHistoryTrack;
import org.eclipse.platform.discovery.util.internal.session.ISession;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swtbot.swt.finder.SWTBot;
import org.eclipse.swtbot.swt.finder.exceptions.WidgetNotFoundException;
import org.eclipse.swtbot.swt.finder.utils.SWTBotPreferences;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotCTabItem;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotToolbarButton;
import org.eclipse.ui.forms.widgets.FormToolkit;


public class TabbedSessionDisplayerPageObject {
	
	private long SWT_TIMEOUT = SWTBotPreferences.TIMEOUT;
	private long OUR_TIMEOUT = 1000;

	private TabbedSessionDisplayer<String> sessionDisplayer;
	
	private Map<String, ISession<String>> idsToSessions = new HashMap<String, ISession<String>>();
	private Map<String, String> itemsToIds = new HashMap<String, String>();
	
	private InShellPageObject inShellPageObject;

	public void addUnnavigableSession(String id, String item) {
		//a session is unnavigable if its history limit is equal to one
		//therefore adding a session with one item renders it unnavigable
		//from the test class perspective though, it is better to have this method separately declared
		addSession(id, item);
	}
	
	public void addSession(String id, String... items) {
		IHistoryTrack<String> track = createHistoryTrack(items);
		mapItemsToSessionId(items, id);
		
		@SuppressWarnings("unchecked")
		ISession<String> session = mock(ISession.class);
		when(session.getId()).thenReturn(id);
		when(session.historyTrack()).thenReturn(track);
		
		idsToSessions.put(id, session);
	}
	
	private void mapItemsToSessionId(String[] items, String id) {
		for(String item: items) {
			itemsToIds.put(item, id);
		}
	}

	private IHistoryTrack<String> createHistoryTrack(String... items) {
		IHistoryTrack<String> track = new HistoryTrack<String>(items.length);
		for(String item: items) {
			track.track(item);
		}
		rewindTrack(track);
		return track;
	}
	
	private void rewindTrack(IHistoryTrack<String> track) {
		while(track.hasPrevious()) {
			track.previous();
		}
	}
	
	public void displaySessions(final String[] ids, final boolean[] closeable) {
		
		if(ids.length!=closeable.length) {
			throw new IllegalArgumentException("ids and closeable must be of equal length");
		}
		
		inShellPageObject = new InShellPageObject()
		{
			@Override
			protected void createContent(final Shell parent, final FormToolkit formToolkit)
			{
				final MyUiFactory uiFactory = new MyUiFactory(){
					@Override
					public FormToolkit getFormTookit()
					{
						return formToolkit;
					}
				};
				sessionDisplayer = new TabbedSessionDisplayer<String>(parent, uiFactory, SWT.NONE);
				
				for(int i = 0; i < ids.length; i++)
				{
					String sessionId = ids[i];
					if(!closeable[i])
					{
						sessionDisplayer.setSessionUnclosable(sessionId);
					}
					sessionDisplayer.display(idsToSessions.get(sessionId));
				}
			}
			
			abstract class MyUiFactory implements TabbedSessionDisplayer.UIFactory<String>
			{

				@Override
				public UI create(final String sc, final Composite parent)
				{
					UI result = mock(UI.class);
					when(result.parent()).thenReturn(parent);
					String sessionId = itemsToIds.get(sc);
					when(result.title()).thenReturn(sessionId);
					return result;
				}
			}
		};
		inShellPageObject.open();
		SWTBotPreferences.TIMEOUT = OUR_TIMEOUT;
	}
	
	public void tearDown() {
		try{
			inShellPageObject.close();
			idsToSessions.clear();
			itemsToIds.clear();
			sessionDisplayer = null;
		} finally {
			SWTBotPreferences.TIMEOUT = SWT_TIMEOUT;
		}
	}

	public boolean isNavigateToNextAvailable() {
		try{
			getNextButton();
			return true;
		}catch(WidgetNotFoundException ex) {
			return false;
		}
	}

	public boolean isNavigateToPreviousAvailable() {
		try{
			getPreviousButton();
			return true;
		}catch(WidgetNotFoundException ex) {
			return false;
		}
		
	}

	private SWTBotToolbarButton getPreviousButton() {
		return getButtonWithTooltip(DiscoveryUIMessages.AbstractSearchResultTab_GoToPrevious);
	}
	
	private SWTBotToolbarButton getNextButton() {
		return getButtonWithTooltip(DiscoveryUIMessages.AbstractSearchResultTab_GoToNext);
	}
	
	private  SWTBotToolbarButton getButtonWithTooltip(String tooltip) {
		return shellBot().toolbarButtonWithTooltip(tooltip);
	}
	
	private SWTBot shellBot() {
		return new MyBot(inShellPageObject.getShell());
	}

	public void closeSession(String sessionId) {
		tabItem(sessionId).close();
	}

	private SWTBotCTabItem tabItem(String sessionId) {
		return shellBot().cTabItem(sessionId);
	}
	
	public boolean isSessionOpen(String sessionId) {
		try{
			tabItem(sessionId);
			return true;
		}catch(WidgetNotFoundException ex) {
			return false;
		}
	}
	
	public void selectSession(String sessionId) {
		tabItem(sessionId).activate();
	}

	public boolean isNavigateToPreviousEnabled() {
		return getPreviousButton().isEnabled();
	}

	public boolean isNavigateToNextEnabled() {
		return getNextButton().isEnabled();
	}

	public void navigateToPrevious() {
		getPreviousButton().click();
	}
	
	public void navigateToNext() {
		getNextButton().click();
	}
	
}
