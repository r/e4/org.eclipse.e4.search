/*******************************************************************************
 * Copyright (c) 2010, 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal;

import static org.junit.Assert.assertEquals;

import org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.TextControlPageObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TextControlTest
{
	private TextControlPageObject textControl;

	@Before
	public void setUp()
	{
		textControl = new TextControlPageObject();
		textControl.open();
	}

	@After
	public void tearDown()
	{
		textControl.close();
	}

	@Test
	public void testTextIsRestoredWhenControlIsEnabled()
	{
		final String testText = "12345_6789";
		textControl.enterText(testText);
		assertEquals("Unexpected text value", testText, textControl.get());
		assertEquals("Unexpected text in SWT text", testText, textControl.getDisplayedText());

		textControl.setEnabled(false);
		assertEquals("SWT text should be empty", "", textControl.getDisplayedText());
		assertEquals("Unexpected text value", "", textControl.get());

		textControl.setEnabled(true);
		assertEquals("Unexpected text value", testText, textControl.get());
		assertEquals("Unexpected text in SWT text", testText, textControl.getDisplayedText());
	}

	@Test
	public void testTextMessageAmongEnabledStates()
	{
		final String message = "MyMessage";
		textControl.setMessage(message);
		textControl.focus();
		assertEquals("Unexpected message", message, textControl.getMessage());
		assertEquals("Unexpected text displayed", "", textControl.getDisplayedText());

		textControl.setEnabled(false);
		assertEquals("Unexpected message", "", textControl.getMessage());
		assertEquals("Unexpected text displayed", message, textControl.getDisplayedText());

		textControl.setEnabled(true);
		assertEquals("Unexpected message", message, textControl.getMessage());
		assertEquals("Unexpected text displayed", "", textControl.getDisplayedText());
	}
}
