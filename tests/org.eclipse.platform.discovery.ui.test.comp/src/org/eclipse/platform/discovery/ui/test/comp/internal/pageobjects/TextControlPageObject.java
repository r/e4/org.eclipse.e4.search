/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects;

import static org.eclipse.swtbot.swt.finder.finders.UIThreadRunnable.syncExec;

import org.eclipse.platform.discovery.testutils.utils.pageobjects.InShellPageObject;
import org.eclipse.platform.discovery.ui.internal.view.impl.TextControl;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swtbot.swt.finder.results.StringResult;
import org.eclipse.swtbot.swt.finder.results.VoidResult;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotText;
import org.eclipse.ui.forms.widgets.FormToolkit;

public class TextControlPageObject extends InShellPageObject
{
	private static final String LABEL_TEXT = "The Label";
	private TextControl textControl;

	@Override
	protected void createContent(final Shell parent, final FormToolkit formToolkit)
	{
		final Label label = formToolkit.createLabel(parent, LABEL_TEXT, SWT.NONE);
		final Text text = formToolkit.createText(parent, "", SWT.NONE);
		textControl = new TextControl(text, label);
	}

	public void enterText(final String text)
	{
		swtBotText().typeText(text);
	}

	public String get()
	{
		return syncExec(new StringResult()
		{
			
			@Override
			public String run()
			{
				return textControl.get();
			}
		});
	}

	public String getDisplayedText()
	{
		return swtBotText().getText();
	}

	public void setEnabled(final boolean enabled)
	{
		syncExec(new VoidResult()
		{
			
			@Override
			public void run()
			{
				textControl.setEnabled(enabled);
			}
		});
	}

	public void setMessage(final String message)
	{
		syncExec(new VoidResult()
		{
			@Override
			public void run()
			{
				textControl.setMessage(message);
			}
		});
	}

	private SWTBotText swtBotText()
	{
		return bot().text();
	}

	public String getMessage()
	{
		return syncExec(new StringResult()
		{
			@Override
			public String run()
			{
				return textControl.getControl().getMessage();
			}
		});
	}

	public void focus()
	{
		swtBotText().setFocus();
	}
}
