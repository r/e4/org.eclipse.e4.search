/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects.swtbot.utils;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;

import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.swtbot.swt.finder.finders.UIThreadRunnable;
import org.eclipse.swtbot.swt.finder.results.VoidResult;
import org.eclipse.swtbot.swt.finder.utils.SWTUtils;
import org.eclipse.swtbot.swt.finder.widgets.AbstractSWTBot;

/**
 * Utility which comes in handy when interaction with the mouse is necessary. Typically you should rely on the SWTBot API but there are few rare cases
 * when it is not sufficient
 * 
 * @author Danail Branekov
 */
public class MouseUtils
{
	private static final int MOVE_TO_WIDGET_OFFSET = 2;
	private static final long HOVER_TIMEOUT = 2000;
	private final Display display;

	public MouseUtils(final Display display)
	{
		this.display = display;
	}

	/**
	 * Moves the mouse to the widget specified. The mouse cursor is moved to 2 points left and 2 point below the widget rectangle top-left corner
	 * 
	 * @param widget
	 *            the widget to move to
	 */
	public void moveMouseTo(final AbstractSWTBot<? extends Widget> widget)
	{
		moveMouseTo(widget, MOVE_TO_WIDGET_OFFSET, MOVE_TO_WIDGET_OFFSET);
	}

	/**
	 * Moves the mouse to the widget specified. The mouse cursor is moved to 2 points left and 2 point below the widget rectangle top-left corner
	 * 
	 * @param widget
	 *            the widget to move to
	 * @param x_offset
	 *            abcis offset relative to the top-left corner of the widget
	 * @param y_offset
	 *            ordinate offset relative to the top-left corner of the widget
	 */
	public void moveMouseTo(final AbstractSWTBot<? extends Widget> widget, final int x_offset, final int y_offset)
	{
		final Point targetPoint = topLeftCorner(widget);
		moveMouseTo(new Point(targetPoint.x + x_offset, targetPoint.y + y_offset));
	}

	private void moveMouseTo(final Point point)
	{
		final Robot awtRobot = awtRobot();
		syncExec(new VoidResult()
		{
			@Override
			public void run()
			{
				awtRobot.mouseMove(point.x, point.y);
			}
		});
		forceAsynchronousEventsToBeProcessed();
	}

	/**
	 * Moves the mouse to the widget and performs a left button click
	 * 
	 * @param widget
	 *            the widget to click on
	 */
	public void clickOn(final AbstractSWTBot<? extends Widget> widget)
	{
		moveMouseTo(widget);
		click();
	}

	/**
	 * Moves the mouse to the widget and performs a left button click
	 * 
	 * @param x_offset
	 *            abcis offset relative to the top-left corner of the widget
	 * @param y_offset
	 *            ordinate offset relative to the top-left corner of the widget
	 * @param widget
	 *            the widget to click on
	 */
	public void clickOn(final AbstractSWTBot<? extends Widget> widget, final int x_offset, final int y_offset)
	{
		moveMouseTo(widget, x_offset, y_offset);
		click();
	}

	/**
	 * Hovers on a widget. The implementation would move the mouse to the widget and wait some time
	 */
	public void hoverOn(AbstractSWTBot<? extends Widget> widget)
	{
		moveMouseTo(widget);
		SWTUtils.sleep(HOVER_TIMEOUT);
	}

	public void moveAwayFrom(AbstractSWTBot<? extends Widget> widget)
	{
		final Rectangle widgetLocation = location(widget);
		final int targetX = (widgetLocation.x > MOVE_TO_WIDGET_OFFSET) ? (widgetLocation.x - MOVE_TO_WIDGET_OFFSET) : (widgetLocation.x + widgetLocation.width + MOVE_TO_WIDGET_OFFSET);
		final int targetY = (widgetLocation.y > MOVE_TO_WIDGET_OFFSET) ? (widgetLocation.y - MOVE_TO_WIDGET_OFFSET) : (widgetLocation.y + widgetLocation.height + MOVE_TO_WIDGET_OFFSET);
		moveMouseTo(new Point(targetX, targetY));
	}

	public void click()
	{
		final Robot awtRobot = awtRobot();
		syncExec(new VoidResult()
		{
			@Override
			public void run()
			{
				awtRobot.mousePress(InputEvent.BUTTON1_MASK);
				awtRobot.mouseRelease(InputEvent.BUTTON1_MASK);
			}
		});
		forceAsynchronousEventsToBeProcessed();
	}

	private Robot awtRobot()
	{
		try
		{
			return new Robot();
		}
		catch (AWTException e)
		{
			throw new RuntimeException(e);
		}
	}

	private Point topLeftCorner(AbstractSWTBot<? extends Widget> widget)
	{
		final Rectangle widgetLocation = location(widget);
		return new Point(widgetLocation.x, widgetLocation.y);
	}

	private Rectangle location(AbstractSWTBot<? extends Widget> widget)
	{
		return LocationUtils.absoluteLocation(widget);
	}

	private void syncExec(VoidResult toExecute)
	{
		UIThreadRunnable.syncExec(display, toExecute);
	}

	void forceAsynchronousEventsToBeProcessed()
	{
		display.syncExec(new Runnable()
		{
			public void run()
			{
			}
		});
	}

}
