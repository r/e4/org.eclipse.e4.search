/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal;

import static org.junit.Assert.assertEquals;

import org.eclipse.platform.discovery.ui.internal.view.impl.OpenPropsViewAction;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.junit.Before;
import org.junit.Test;

public class OpenPropsViewActionTest  {
	private OpenPropsViewAction action;
	
	@Before
	public void setUp() {
		action = new OpenPropsViewAction();
	}
	
	@Test
	public void getId() {
		assertEquals(OpenPropsViewAction.ID, action.getId());
	}
	
	@Test
	public void propsIsOpened() {
		
		PlatformUI.getWorkbench().getDisplay().syncExec(new Runnable() {
			@Override
			public void run() {
				closePropsIfOpen();
				
				action.run();
				
				IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				assertEquals("properties view is not the active part", "Properties", page.getActivePart().getTitle());
			}

		});
	}
	
	private void closePropsIfOpen() {
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		if (page.getActivePart().getTitle().equals("Properties")) {
			//close it, so we can verify search console opened it
			page.hideView((IViewPart) page.getActivePart());
		}
	}
}
