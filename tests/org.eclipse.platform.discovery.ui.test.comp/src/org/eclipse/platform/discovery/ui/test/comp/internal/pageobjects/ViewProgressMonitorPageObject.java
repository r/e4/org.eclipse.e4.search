/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.test.comp.internal.pageobjects;

import static org.eclipse.swtbot.swt.finder.finders.UIThreadRunnable.syncExec;

import java.util.Arrays;

import org.eclipse.platform.discovery.testutils.utils.pageobjects.InShellPageObject;
import org.eclipse.platform.discovery.ui.internal.view.ViewProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swtbot.swt.finder.results.BoolResult;
import org.eclipse.swtbot.swt.finder.results.VoidResult;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotToolbarButton;
import org.eclipse.ui.forms.widgets.FormToolkit;

public class ViewProgressMonitorPageObject extends InShellPageObject
{
	private static final String TEST_BUTTON_TEXT = "ButtonToDisable";
	private ViewProgressMonitor testMonitor;

	@Override
	protected void createContent(final Shell parent, final FormToolkit formToolkit)
	{
		parent.setLayout(new FillLayout(SWT.VERTICAL));
		final Button buttonToDisable = formToolkit.createButton(parent, TEST_BUTTON_TEXT, SWT.PUSH);
		buttonToDisable.setText(TEST_BUTTON_TEXT);
		testMonitor = new ViewProgressMonitor(parent, Arrays.asList(new Control[] { buttonToDisable }));
	}

	public void beginTask(final String name, final int totalWork)
	{
		syncExec(new VoidResult()
		{
			@Override
			public void run()
			{
				testMonitor.beginTask(name, totalWork);
			}
		});
	}

	public boolean isControlsEnabled()
	{
		return bot().button().isEnabled();
	}

	public void done()
	{
		syncExec(new VoidResult()
		{
			@Override
			public void run()
			{
				testMonitor.done();
			}
		});
	}

	public void cancel()
	{
		cancelButton().click();
	}

	private SWTBotToolbarButton cancelButton()
	{
		return bot().toolbarButton();
	}

	public boolean isCanceled()
	{
		return syncExec(new BoolResult()
		{
			
			@Override
			public Boolean run()
			{
				return testMonitor.isCanceled();
			}
		});
	}
}
