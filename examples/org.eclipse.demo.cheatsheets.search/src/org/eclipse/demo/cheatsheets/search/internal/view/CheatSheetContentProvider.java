/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.view;

import org.eclipse.demo.cheatsheets.search.internal.runtime.ICheatSheetCategory;
import org.eclipse.demo.cheatsheets.search.internal.runtime.ICheatSheetItem;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.platform.discovery.core.api.ISearchContext;

/**
 * The content provider for displaying cheat sheets. Used in both search console and search favorites
 * @author danail
 */
public class CheatSheetContentProvider implements ITreeContentProvider
{
	@Override
	public void dispose()
	{
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
	{
	}

	@Override
	public Object[] getElements(Object inputElement)
	{
		if(inputElement instanceof ISearchContext)
		{
			return getElements(((ISearchContext)inputElement).searchResult());
		}
		if (inputElement instanceof ICheatSheetCategory)
		{
			return ((ICheatSheetCategory) inputElement).getChildren().toArray();
		}
		return new Object[0];
	}

	@Override
	public Object[] getChildren(Object parentElement)
	{
		return getElements(parentElement);
	}

	@Override
	public Object getParent(Object element)
	{
		if (element instanceof ICheatSheetItem)
		{
			return ((ICheatSheetItem) element).getParent();
		}
		return null;
	}

	@Override
	public boolean hasChildren(Object element)
	{
		return element instanceof ICheatSheetCategory && ((ICheatSheetCategory) element).getChildren().size() > 0;
	}
}
