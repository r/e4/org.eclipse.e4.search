/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.slave;

import java.util.Set;

import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.core.api.ISearchFavoritesMasterController;
import org.eclipse.platform.discovery.core.api.ISearchFavoritesSlaveController;

/**
 * The slave controller for the search favorites customization.
 * @author danail
 *
 */
public class FavoritesSlaveController extends CSSlaveController implements ISearchFavoritesSlaveController
{
	private ISearchFavoritesMasterController controller;

	@Override
	public void setMasterController(ISearchFavoritesMasterController masterController)
	{
		this.controller = masterController;
	}
	
	@Override
	public Set<IContributedAction> createActions()
	{
		final Set<IContributedAction> actions = super.createActions();
		actions.add(new DeleteFromFavoritesAction(controller));
		
		return actions;
	}
}
