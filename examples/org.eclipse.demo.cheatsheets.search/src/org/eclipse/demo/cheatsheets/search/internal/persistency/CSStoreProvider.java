/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.persistency;

import org.eclipse.demo.cheatsheets.search.destinations.CSDestination;
import org.eclipse.demo.cheatsheets.search.internal.runtime.ICheatSheet;
import org.eclipse.platform.discovery.runtime.api.persistence.DestinationItemPair;
import org.eclipse.platform.discovery.runtime.api.persistence.IMementoStoreProvider;
import org.eclipse.platform.discovery.runtime.api.persistence.MementoStoreProviderException;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.ui.IMemento;

/**
 * Store provider for favorites cheat sheets. Used for persisting favorite cheat sheets in the search favorites view
 * @author danail
 */
public class CSStoreProvider implements IMementoStoreProvider, CSPersistency
{
	@Override
	public String getDescriptor()
	{
		return "Cheat Sheets Store Provider"; //$NON-NLS-1$
	}

	@Override
	public boolean canStore(DestinationItemPair resource)
	{
		return resource.getItem() instanceof ICheatSheet;
	}

	@Override
	public void store(IMemento container, DestinationItemPair resource, ILongOperationRunner opRunner) throws MementoStoreProviderException
	{
		final IMemento memento = container.createChild(CHEAT_SHEET_PERS_TYPE, CHEAT_SHEET_PERS_TAG_ID);
		memento.putString(CHEAT_SHEET_ID_ATTRIBUTE, ((ICheatSheet)resource.getItem()).getID());
		memento.putString(CHEAT_SHEET_DESTINATION_ATTRIBUTE, ((CSDestination)resource.getDestination()).getDestinationID());
	}

}
