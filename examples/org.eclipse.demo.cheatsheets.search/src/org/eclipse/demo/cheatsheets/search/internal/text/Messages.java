/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.text;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS
{
	private static final String BUNDLE_NAME = "org.eclipse.demo.cheatsheets.search.internal.text.messages"; //$NON-NLS-1$
	public static String AllCategoriesCDDestination_DisplayName;
	public static String FavoritesCustomization_CheatSheetGroupName;
	public static String CheatSheetSearchProvider_ByCategoryGrouping;
	public static String CheatSheetSearchProvider_FlatListGrouping;
	public static String DeleteFromFavorites;
	public static String RunCheatSheetAction_Label;
	
	static
	{
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages()
	{
	}
}
