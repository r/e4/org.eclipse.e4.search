/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.persistency;

import java.text.MessageFormat;

import org.eclipse.demo.cheatsheets.search.destinations.CSDestination;
import org.eclipse.demo.cheatsheets.search.destinations.LocalCSDestinationsProvider;
import org.eclipse.demo.cheatsheets.search.internal.runtime.CheatSheetImpl;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.persistence.DestinationItemPair;
import org.eclipse.platform.discovery.runtime.api.persistence.IMementoLoadProvider;
import org.eclipse.platform.discovery.runtime.api.persistence.MementoLoadProviderException;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.internal.cheatsheets.registry.CheatSheetElement;
import org.eclipse.ui.internal.cheatsheets.registry.CheatSheetRegistryReader;

/**
 * Load provider for cheat sheets. Used for loading favorite cheat sheets items in the search favorites view
 * @author danail
 */
@SuppressWarnings("restriction")
public class CSLoadProvider implements IMementoLoadProvider, CSPersistency
{
	@Override
	public String getDescriptor()
	{
		return "Cheat Sheet Load Provider"; //$NON-NLS-1$
	}

	@Override
	public String getChildType()
	{
		return CHEAT_SHEET_PERS_TYPE;
	}

	@Override
	public boolean canLoad(IMemento container)
	{
		return container != null && CHEAT_SHEET_PERS_TYPE.equals(container.getType()) && CHEAT_SHEET_PERS_TAG_ID.equals(container.getID());
	}

	@Override
	public DestinationItemPair load(IMemento container, ILongOperationRunner opRunner) throws MementoLoadProviderException
	{
		final String csID = container.getString(CHEAT_SHEET_ID_ATTRIBUTE);
		final String destinationId = container.getString(CHEAT_SHEET_DESTINATION_ATTRIBUTE);
		final CheatSheetElement sheet = CheatSheetRegistryReader.getInstance().findCheatSheet(csID);
		if (sheet == null)
		{
			throw new MementoLoadProviderException(MessageFormat.format("Cheat sheet with id={0} not found", csID)); //$NON-NLS-1$
		}

		for (ISearchDestination dest : new LocalCSDestinationsProvider().getSearchDestinations())
		{
			if (((CSDestination) dest).getDestinationID().equals(destinationId))
			{
				return new DestinationItemPair(dest, new CheatSheetImpl(sheet, null));
			}
		}

		throw new MementoLoadProviderException(MessageFormat.format("Destination with id={0} not found", destinationId)); //$NON-NLS-1$
	}

}
