/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.runtime;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.ui.internal.cheatsheets.registry.CheatSheetCollectionElement;

@SuppressWarnings("restriction")
public class CheatSheetCategoryImpl implements ICheatSheetCategory
{
	private static final String NULL = "null"; //$NON-NLS-1$
	private final ICheatSheetCategory parent;
	private Collection<ICheatSheetItem> children;
	private CheatSheetCollectionElement csCategory;

	public CheatSheetCategoryImpl(final CheatSheetCollectionElement csCategory, final ICheatSheetCategory parent)
	{
		this.csCategory = csCategory;
		this.parent = parent;
		this.children = new ArrayList<ICheatSheetItem>();
	}
	
	@Override
	public String getID()
	{
		return csCategory == null ? NULL : csCategory.getId();
	}

	@Override
	public String getName()
	{
		return csCategory == null ? NULL : csCategory.getLabel(null);
	}

	@Override
	public ICheatSheetCategory getParent()
	{
		return this.parent;
	}

	@Override
	public Collection<ICheatSheetItem> getChildren()
	{
		return this.children;
	}
	
	@SuppressWarnings("nls")
	@Override
	public String toString()
	{
		return "Category " + getName() + "; id=" + getID();
	}

	@Override
	public CheatSheetCollectionElement getCollectionElement()
	{
		return this.csCategory;
	}
}
