/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.search;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.demo.cheatsheets.search.destinations.AllCategoriesDest;
import org.eclipse.demo.cheatsheets.search.destinations.CSDestination;
import org.eclipse.demo.cheatsheets.search.internal.runtime.CSLoader;
import org.eclipse.demo.cheatsheets.search.internal.runtime.CheatSheetCategoryImpl;
import org.eclipse.demo.cheatsheets.search.internal.runtime.ICheatSheet;
import org.eclipse.demo.cheatsheets.search.internal.runtime.ICheatSheetCategory;
import org.eclipse.demo.cheatsheets.search.internal.runtime.ICheatSheetItem;
import org.eclipse.demo.cheatsheets.search.internal.runtime.RootCSItem;
import org.eclipse.platform.discovery.runtime.api.ISearchParameters;
import org.eclipse.platform.discovery.runtime.api.SearchCancelledException;
import org.eclipse.platform.discovery.runtime.api.SearchFailedException;
import org.eclipse.platform.discovery.runtime.api.impl.SearchQuery;
import org.eclipse.platform.discovery.util.api.longop.ILongOperation;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.platform.discovery.util.api.longop.LongOpCanceledException;

/**
 * The cheat sheet search query. Instantiated by the search provider 
 * @author danail
 */
class CheatSheetSearchQuery extends SearchQuery
{
	private ISearchParameters searchParams;

	CheatSheetSearchQuery(final ISearchParameters searchParams)
	{
		this.searchParams = searchParams;
	}

	@Override
	public Object execute(final ILongOperationRunner lor) throws SearchFailedException, SearchCancelledException
	{
		try
		{
			return lor.run(queryOperation());
		} catch (LongOpCanceledException e)
		{
			throw new IllegalStateException("Operation does not support cancellation"); //$NON-NLS-1$
		} catch (InvocationTargetException e)
		{
			throw new SearchFailedException(e.getCause());
		}
	}

	private ILongOperation<ICheatSheetCategory> queryOperation()
	{
		final CheatSheetMatcher matcher = new CheatSheetMatcher(searchParams.getKeywordString());
		return new QueryOperation(matcher);
	}

	private class QueryOperation implements ILongOperation<ICheatSheetCategory>
	{
		private final CheatSheetMatcher mathcer;

		public QueryOperation(CheatSheetMatcher matcher)
		{
			this.mathcer = matcher;
		}

		@Override
		public ICheatSheetCategory run(IProgressMonitor monitor) throws LongOpCanceledException, Exception
		{
			// Check whether the user chose flat-list results grouping
			if(searchParams.getGroupingHierarchy().equals(LocalCheatSheetSearchProvider.FLAT_LIST))
			{
				return flatListSheets();
			}
			else
			{
				return byCategoriesSheets();
			}
		}

		private ICheatSheetCategory byCategoriesSheets()
		{
			final ICheatSheetCategory result = new RootCSItem();
			for(ICheatSheetCategory cat : relevantCategories())
			{
				final Collection<ICheatSheetItem> children = getCategoryChildren(cat);
				if(children.size() > 0)
				{
					final ICheatSheetCategory newCat = new CheatSheetCategoryImpl(cat.getCollectionElement(), null);
					newCat.getChildren().addAll(children);
					result.getChildren().add(newCat);
				}
			}
			
			return result;
		}

		private Collection<ICheatSheetItem> getCategoryChildren(final ICheatSheetCategory category)
		{
			final Collection<ICheatSheetItem> result = new ArrayList<ICheatSheetItem>();
			for (ICheatSheetItem item : category.getChildren())
			{
				if (item instanceof ICheatSheetCategory)
				{
					final ICheatSheetCategory childCat = new CheatSheetCategoryImpl(((ICheatSheetCategory) item).getCollectionElement(), category);
					if(getCategoryChildren(childCat).size() > 0)
					{
						result.add(childCat);
					}
				}
				if (item instanceof ICheatSheet && mathcer.matches((ICheatSheet)item))
				{
					result.add((ICheatSheet) item);
				}
			}
			return result;
		}

		private ICheatSheetCategory flatListSheets()
		{
			final ICheatSheetCategory result = new RootCSItem();
			for(ICheatSheetCategory cat : relevantCategories())
			{
				result.getChildren().addAll(extractCheatSheets(cat));
			}
			return result;
		}

		private Collection<ICheatSheet> extractCheatSheets(ICheatSheetCategory category)
		{
			final Collection<ICheatSheet> result = new ArrayList<ICheatSheet>();
			for (ICheatSheetItem item : category.getChildren())
			{
				if (item instanceof ICheatSheetCategory)
				{
					result.addAll(extractCheatSheets((ICheatSheetCategory) item));
				}
				if (item instanceof ICheatSheet && mathcer.matches((ICheatSheet)item))
				{
					result.add((ICheatSheet) item);
				}
			}
			return result;
		}
		
		private Collection<ICheatSheetCategory> relevantCategories()
		{
			final CSDestination destination = (CSDestination) searchParams.getSearchDestination();
			final Collection<ICheatSheetCategory> result = new ArrayList<ICheatSheetCategory>();
			for(ICheatSheetItem item : new CSLoader().loadCheatSheets().getChildren())
			{
				assert item instanceof ICheatSheetCategory;
				final ICheatSheetCategory cat = (ICheatSheetCategory)item;
				if(destination.getClass().equals(AllCategoriesDest.class) || destination.getCategory().getID().equals(cat.getID()))
				{
					result.add(cat);
				}
			}
			
			return result;
		}
	}
	
	/**
	 * Very dummy matcher implementation. Would return <code>true</code> if the expression is empty or is contained in the analyzed string
	 * @author danail
	 */
	private class CheatSheetMatcher
	{
		private final String expression;

		public CheatSheetMatcher(final String expression)
		{
			this.expression = expression;
		}
		
		public boolean matches(final ICheatSheet sheet)
		{
			if(expression.equals("")) //$NON-NLS-1$
			{
				return true;
			}
			
			return sheet.getName().toLowerCase().contains(expression);
		}
	}

}
