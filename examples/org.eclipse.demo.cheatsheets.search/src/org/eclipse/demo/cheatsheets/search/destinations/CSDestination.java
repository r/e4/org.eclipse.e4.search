/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.destinations;

import org.eclipse.demo.cheatsheets.search.internal.runtime.ICheatSheetCategory;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;

/**
 * Cheat sheets search destination
 * @author danail 
 */
public class CSDestination implements ISearchDestination
{
	private final ICheatSheetCategory csCat;

	public CSDestination(final ICheatSheetCategory csCat)
	{
		this.csCat = csCat;
	}
	
	public ICheatSheetCategory getCategory()
	{
		return csCat;
	}

	@Override
	public String getDisplayName()
	{
		return csCat.getName();
	}
	
	public String getDestinationID()
	{
		return csCat.getID();
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
		{
			return false;
		}
		if (!obj.getClass().equals(this.getClass()))
		{
			return false;
		}

		final CSDestination destination = (CSDestination) obj;
		if(this.csCat == null)
		{
			return destination.csCat == null; 
		}
		
		return this.csCat.getID().equals(destination.csCat.getID());
	}

	@Override
	public int hashCode()
	{
		if(this.csCat == null)
		{
			return -1;
		}
		
		return this.csCat.getID().hashCode();
	}
}
