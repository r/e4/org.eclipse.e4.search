/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.view;

import org.eclipse.demo.cheatsheets.search.internal.runtime.ICheatSheet;
import org.eclipse.demo.cheatsheets.search.internal.runtime.ICheatSheetCategory;
import org.eclipse.demo.cheatsheets.search.internal.runtime.ICheatSheetItem;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.cheatsheets.CheatSheetPlugin;
import org.eclipse.ui.internal.cheatsheets.ICheatSheetResource;

/**
 * The label provider for displaying cheat sheets
 * @author danail
 *
 */
@SuppressWarnings("restriction")
public class CheatSheetLabelProvider implements ILabelProvider
{
	@Override
	public void addListener(ILabelProviderListener listener)
	{
	}

	@Override
	public void dispose()
	{
	}

	@Override
	public boolean isLabelProperty(Object element, String property)
	{
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener)
	{
	}

	@Override
	public Image getImage(Object element)
	{
		if (element instanceof ICheatSheetCategory)
		{
			return PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_FOLDER);
		}

		if (element instanceof ICheatSheet)
		{
			final ICheatSheet cs = (ICheatSheet) element;
			if (cs.isComposite())
			{
				return CheatSheetPlugin.getPlugin().getImageRegistry().get(ICheatSheetResource.COMPOSITE_OBJ);
			}
			return CheatSheetPlugin.getPlugin().getImageRegistry().get(ICheatSheetResource.CHEATSHEET_OBJ);
		}

		return null;
	}

	@Override
	public String getText(Object element)
	{
		if (element instanceof ICheatSheetItem)
		{
			return ((ICheatSheetItem) element).getName();
		}
		return null;
	}

}
