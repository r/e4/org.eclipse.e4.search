/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.view.favorites;

import org.eclipse.demo.cheatsheets.search.internal.runtime.CheatSheetCategoryImpl;
import org.eclipse.demo.cheatsheets.search.internal.text.Messages;

/**
 * Artificial node displayed as parent of all favorite cheat sheets
 * @author danail
 */
public class ChatSheatGroupNode extends CheatSheetCategoryImpl
{
	public ChatSheatGroupNode()
	{
		super(null, null);
	}
	
	@Override
	public String getName()
	{
		return Messages.FavoritesCustomization_CheatSheetGroupName;
	}
}
