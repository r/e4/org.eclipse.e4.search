/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.slave;

import java.util.Set;

import org.eclipse.demo.cheatsheets.search.internal.runtime.ICheatSheet;
import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;

/**
 * The action for running cheat sheets 
 * @author danail
 */
public class RunCheatSheetAction implements IContributedAction
{
	public static final String ACTION_ID = RunCheatSheetAction.class.getName();
	
	@Override
	public void perform(ILongOperationRunner operationRunner, Set<Object> selectedObjects)
	{
		final ICheatSheet cs = (ICheatSheet)selectedObjects.iterator().next();
		new org.eclipse.ui.cheatsheets.OpenCheatSheetAction(cs.getID()).run();
	}

	@Override
	public Object getActionId()
	{
		return ACTION_ID;
	}
}
