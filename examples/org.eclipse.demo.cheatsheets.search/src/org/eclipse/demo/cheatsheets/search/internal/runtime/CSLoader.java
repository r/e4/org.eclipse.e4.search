/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.runtime;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.ui.internal.cheatsheets.registry.CheatSheetCollectionElement;
import org.eclipse.ui.internal.cheatsheets.registry.CheatSheetElement;
import org.eclipse.ui.internal.cheatsheets.registry.CheatSheetRegistryReader;

/**
 * Utility for loading favorite cheat sheets
 * 
 * @author danail
 */
@SuppressWarnings("restriction")
public class CSLoader
{
	public ICheatSheetCategory loadCheatSheets()
	{
		final ICheatSheetCategory loadedItems = new RootCSItem();
		CheatSheetRegistryReader regReader = CheatSheetRegistryReader.getInstance();
		final CheatSheetCollectionElement csCollection = regReader.getCheatSheets();
		fillCategory(loadedItems, csCollection);
		return loadedItems;
	}

	private void fillCategory(final ICheatSheetCategory csCategory, CheatSheetCollectionElement csCollection)
	{
		final Collection<CheatSheetElement> cheatSheets = new ArrayList<CheatSheetElement>();
		for (Object o : csCollection.getCheatSheets())
		{
			cheatSheets.add((CheatSheetElement) o);
		}

		final Collection<CheatSheetCollectionElement> subCategories = new ArrayList<CheatSheetCollectionElement>();
		for (Object o : csCollection.getChildren())
		{
			subCategories.add((CheatSheetCollectionElement) o);
		}

		for (CheatSheetCollectionElement elem : subCategories)
		{
			final ICheatSheetCategory cat = new CheatSheetCategoryImpl(elem, csCategory);
			csCategory.getChildren().add(cat);
			fillCategory(cat, elem);
		}

		for (CheatSheetElement elem : cheatSheets)
		{
			csCategory.getChildren().add(new CheatSheetImpl(elem, csCategory));
		}
	}

}
