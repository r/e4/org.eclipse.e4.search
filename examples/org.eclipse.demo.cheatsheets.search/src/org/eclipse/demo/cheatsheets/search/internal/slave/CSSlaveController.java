/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.slave;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.core.api.ISlaveController;

/**
 * Basic implementation of slave controller for cheat sheets feature. Contributes an action for running a cheat sheet
 * @author danail
 *
 */
public class CSSlaveController implements ISlaveController
{
	@Override
	public Set<IContributedAction> createActions()
	{
		final Set<IContributedAction> result = new HashSet<IContributedAction>();
		result.add(new RunCheatSheetAction());
		return result;
	}
	
}
