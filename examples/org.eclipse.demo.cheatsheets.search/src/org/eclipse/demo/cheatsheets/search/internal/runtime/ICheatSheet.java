/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.runtime;

/**
 * Interface for the cheat sheet item itself
 * @author danail
 */
public interface ICheatSheet extends ICheatSheetItem
{
	public String getDescription();
	/**
	 * Whether the cheat sheet is composite
	 * @see org.eclipse.ui.internal.cheatsheets.registry.CheatSheetElement#isComposite()
	 */
	@SuppressWarnings("restriction")
	public boolean isComposite();
}
