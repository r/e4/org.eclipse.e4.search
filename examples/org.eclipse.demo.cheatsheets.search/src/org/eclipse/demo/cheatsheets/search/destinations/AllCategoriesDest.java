/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.destinations;

import org.eclipse.demo.cheatsheets.search.internal.runtime.ICheatSheetCategory;
import org.eclipse.demo.cheatsheets.search.internal.text.Messages;

/**
 * Destination for all cheat sheets categories
 * @author danail
 */
public class AllCategoriesDest extends CSDestination
{
	public AllCategoriesDest(ICheatSheetCategory csCat)
	{
		super(csCat);
	}
	
	@Override
	public String getDisplayName()
	{
		return Messages.AllCategoriesCDDestination_DisplayName;
	}
	
	@Override
	public String getDestinationID()
	{
		return AllCategoriesDest.class.getName();
	}
}
