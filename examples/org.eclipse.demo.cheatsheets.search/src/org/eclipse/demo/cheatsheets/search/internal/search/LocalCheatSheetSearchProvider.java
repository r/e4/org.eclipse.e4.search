/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.search;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.demo.cheatsheets.search.internal.text.Messages;
import org.eclipse.platform.discovery.runtime.api.GroupingHierarchy;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.ISearchParameters;
import org.eclipse.platform.discovery.runtime.api.ISearchQuery;
import org.eclipse.platform.discovery.runtime.api.ISearchSubdestination;
import org.eclipse.platform.discovery.runtime.api.impl.SearchProvider;

/**
 * The cheat sheets search provider 
 * @author danail
 */
public class LocalCheatSheetSearchProvider extends SearchProvider
{
	static final GroupingHierarchy FLAT_LIST;
	static final GroupingHierarchy BY_CATEGORIES;
	private static final Set<GroupingHierarchy> HIERARCHIES;
	
	static
	{
		FLAT_LIST = new GroupingHierarchy(Messages.CheatSheetSearchProvider_FlatListGrouping, "flat-list"); //$NON-NLS-1$
		BY_CATEGORIES = new GroupingHierarchy(Messages.CheatSheetSearchProvider_ByCategoryGrouping, "by-categories"); //$NON-NLS-1$
		HIERARCHIES = new HashSet<GroupingHierarchy>(Arrays.asList(new GroupingHierarchy[]{FLAT_LIST, BY_CATEGORIES}));
	}
	
	@Override
	public ISearchQuery createQuery(final ISearchParameters searchParameters)
	{
		return new CheatSheetSearchQuery(searchParameters);
	}

	
	@Override
	public Set<GroupingHierarchy> getGroupingHierarchies(ISearchDestination soco, Set<ISearchSubdestination> subdestinations)
	{
		return HIERARCHIES;
	}
}
