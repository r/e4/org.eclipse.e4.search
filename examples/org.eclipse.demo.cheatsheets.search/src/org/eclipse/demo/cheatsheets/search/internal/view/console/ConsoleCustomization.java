/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.view.console;

import org.eclipse.demo.cheatsheets.search.internal.search.LocalCheatSheetSearchProvider;
import org.eclipse.demo.cheatsheets.search.internal.view.CheatSheetCustomization;
import org.eclipse.platform.discovery.ui.api.ISearchConsoleCustomization;

/**
 * The search console view customization
 * @author danail
 *
 */
public class ConsoleCustomization extends CheatSheetCustomization implements ISearchConsoleCustomization
{
	@Override
	public boolean acceptSearchProvider(String searchProviderId)
	{
		return searchProviderId.equals(LocalCheatSheetSearchProvider.class.getName());
	}
}
