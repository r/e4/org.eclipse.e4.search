/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.view;

import org.eclipse.demo.cheatsheets.search.internal.runtime.ICheatSheet;
import org.eclipse.platform.discovery.ui.api.IFormTextBuilder;
import org.eclipse.platform.discovery.ui.api.ITooltipProvider;

/**
 * The provider of tooltip content for cheat sheet items
 * @author danail
 */
public class ConsoleTooltipProvider implements ITooltipProvider
{
	@Override
	public void createTooltipContent(IFormTextBuilder tooltipTextBuilder, Object element)
	{
		if(element instanceof ICheatSheet)
		{
			final ICheatSheet cs = (ICheatSheet)element;
			tooltipTextBuilder.appendProperty("Name", cs.getName()); //$NON-NLS-1$
			tooltipTextBuilder.startParagraph(true);
			tooltipTextBuilder.appendText(cs.getDescription().trim());
			tooltipTextBuilder.endParagraph();
		}
	}
}
