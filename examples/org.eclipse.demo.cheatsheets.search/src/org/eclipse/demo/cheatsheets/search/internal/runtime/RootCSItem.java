/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.runtime;


/**
 * Artificial cheat sheet item. Used as a parent for all cheat sheet items out there
 * @author danail
 */
public class RootCSItem extends CheatSheetCategoryImpl implements ICheatSheetCategory
{
	public RootCSItem()
	{
		super(null, null);
	}
	
	@Override
	public String toString()
	{
		return "ROOT Category item"; //$NON-NLS-1$
	}
}
