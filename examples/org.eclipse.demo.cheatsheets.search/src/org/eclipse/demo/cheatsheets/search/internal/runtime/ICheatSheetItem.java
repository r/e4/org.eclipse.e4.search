/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.runtime;

/**
 * Abstract interface for a cheat sheet items. The item can be either the cheat sheet itself, or the category it belongs to, or the parent category of an category
 * @author danail
 *
 */
public interface ICheatSheetItem
{
	/**
	 * The cheat sheet ID
	 */
	public String getID();

	/**
	 * The item name
	 */
	public String getName();

	/**
	 * The parent
	 * 
	 * @return the parent or null
	 */
	public ICheatSheetCategory getParent();
}
