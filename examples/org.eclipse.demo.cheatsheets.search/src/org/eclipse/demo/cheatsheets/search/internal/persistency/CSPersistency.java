/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.persistency;

@SuppressWarnings("nls")
interface CSPersistency
{
	static final String CHEAT_SHEET_PERS_TAG_ID = "cheat-sheet";
	static final String CHEAT_SHEET_PERS_TYPE = "cheat-sheet-type";
	static final String CHEAT_SHEET_ID_ATTRIBUTE = "cheat-sheet-id";
	static final String CHEAT_SHEET_DESTINATION_ATTRIBUTE = "cheat-sheet-destination";
}
