/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.view;

import java.util.Arrays;
import java.util.HashSet;

import org.eclipse.demo.cheatsheets.search.internal.runtime.ICheatSheet;
import org.eclipse.demo.cheatsheets.search.internal.slave.RunCheatSheetAction;
import org.eclipse.demo.cheatsheets.search.internal.text.Messages;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.ui.api.IGenericViewCustomization;
import org.eclipse.platform.discovery.ui.api.IMasterDiscoveryView;
import org.eclipse.platform.discovery.ui.api.IResultsViewAccessor;
import org.eclipse.platform.discovery.ui.api.ITooltipProvider;

/**
 * The base implementation of cheat sheet view customization
 * @author danail
 */
public class CheatSheetCustomization implements IGenericViewCustomization
{
	protected IMasterDiscoveryView masterView;

	@Override
	public ITreeContentProvider getContentProvider()
	{
		return new CheatSheetContentProvider();
	}

	@Override
	public ILabelProvider getLabelProvider()
	{
		return new CheatSheetLabelProvider();
	}

	@Override
	public ITooltipProvider getTooltipProvider()
	{
		return new ConsoleTooltipProvider();
	}

	@Override
	public void setMasterView(IMasterDiscoveryView masterView)
	{
		this.masterView = masterView;
	}

	@Override
	public void installAction(final IContributedAction contributedAction, final IResultsViewAccessor viewAccessor)
	{
		if (contributedAction.getActionId().equals(RunCheatSheetAction.ACTION_ID))
		{
			final IStructuredSelection sel = (IStructuredSelection) viewAccessor.getTreeViewer().getSelection();
			if (!(sel.getFirstElement() instanceof ICheatSheet))
			{
				return;
			}

			final IAction action = new Action(Messages.RunCheatSheetAction_Label)
			{
				@Override
				public void run()
				{
					doRunCheatSheet(contributedAction, viewAccessor.getTreeViewer().getSelection());
				}
			};
			viewAccessor.getMenuManager().add(action);
		}

	}

	@Override
	public IDoubleClickListener getDoubleClickListener()
	{
		return new IDoubleClickListener()
		{
			@Override
			public void doubleClick(DoubleClickEvent event)
			{
				doRunCheatSheet(new RunCheatSheetAction(), event.getSelection());
			}
		};
	}

	private void doRunCheatSheet(final IContributedAction runAction, final ISelection selection)
	{
		if (!(selection instanceof IStructuredSelection))
		{
			return;
		}

		final Object obj = ((IStructuredSelection) selection).getFirstElement();
		if (!(obj instanceof ICheatSheet))
		{
			return;
		}

		runAction.perform(masterView.getEnvironment().operationRunner(), new HashSet<Object>(Arrays.asList(new Object[] { obj })));
	}

	@Override
	public void postResultDisplayed(IResultsViewAccessor viewAccessor)
	{
		viewAccessor.getTreeViewer().expandToLevel(2);
	}

	@Override
	public void selectionChanged(ISelection selection)
	{
	}
}
