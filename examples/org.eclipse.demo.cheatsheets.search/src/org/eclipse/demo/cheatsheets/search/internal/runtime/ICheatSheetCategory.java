/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.runtime;

import java.util.Collection;

import org.eclipse.ui.internal.cheatsheets.registry.CheatSheetCollectionElement;

/**
 * Interface for cheat sheets categories
 * @author danail
 */
@SuppressWarnings("restriction")
public interface ICheatSheetCategory extends ICheatSheetItem
{
	/**
	 * Retrieve a possibly empty collection of children of this category 
	 */
	public Collection<ICheatSheetItem> getChildren();
	
	/**
	 * Retrieves the {@link CheatSheetCollectionElement} instance representing the category
	 * @return
	 */
	public CheatSheetCollectionElement getCollectionElement();
	
}
