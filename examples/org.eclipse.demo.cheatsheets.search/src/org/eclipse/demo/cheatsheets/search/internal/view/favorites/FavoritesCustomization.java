/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.view.favorites;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.demo.cheatsheets.search.internal.runtime.ICheatSheet;
import org.eclipse.demo.cheatsheets.search.internal.runtime.ICheatSheetCategory;
import org.eclipse.demo.cheatsheets.search.internal.runtime.ICheatSheetItem;
import org.eclipse.demo.cheatsheets.search.internal.runtime.RootCSItem;
import org.eclipse.demo.cheatsheets.search.internal.slave.DeleteFromFavoritesAction;
import org.eclipse.demo.cheatsheets.search.internal.text.Messages;
import org.eclipse.demo.cheatsheets.search.internal.view.CheatSheetCustomization;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.ui.api.IResultsViewAccessor;
import org.eclipse.platform.discovery.ui.api.ISearchFavoritesViewCustomization;

/**
 * The search favorites view customization
 * @author danail
 *
 */
public class FavoritesCustomization extends CheatSheetCustomization implements ISearchFavoritesViewCustomization
{
	private static final ICheatSheetCategory CHEAT_SHEETS_GROUP_NODE = new RootCSItem()
	{
		@Override
		public String getName()
		{
			return Messages.FavoritesCustomization_CheatSheetGroupName;
		}
	};

	@Override
	public Set<Object> itemsFor(Object itemsSource)
	{
		if(itemsSource instanceof ICheatSheet)
		{
			return new HashSet<Object>(Arrays.asList(new Object[] { itemsSource }));
		}
		
		return Collections.emptySet();
	}

	@Override
	public Object itemGroup(Object item)
	{
		if (item instanceof ICheatSheet)
		{
			return CHEAT_SHEETS_GROUP_NODE;
		}

		return null;
	}
	
	@Override
	public void installAction(IContributedAction contributedAction, IResultsViewAccessor viewAccessor)
	{
		super.installAction(contributedAction, viewAccessor);
		if(contributedAction.getActionId().equals(DeleteFromFavoritesAction.ACTION_ID))
		{
			installDeleteCheatSheetMenuAction(contributedAction, viewAccessor);
		}
	}

	private void installDeleteCheatSheetMenuAction(final IContributedAction contributedAction, final IResultsViewAccessor viewAccessor)
	{
		final IStructuredSelection selection = (IStructuredSelection)viewAccessor.getTreeViewer().getSelection();
		if(selection.isEmpty() || selection.size() > 1)
		{
			return;
		}
		
		final Object selectedObject = selection.getFirstElement();
		if(!(selectedObject instanceof ICheatSheetItem))
		{
			return;
		}
		
		final Set<Object> oSet = new HashSet<Object>();
		oSet.add(selectedObject);
		
		final IAction action = new Action()
		{
			@Override
			public void run()
			{
				contributedAction.perform(masterView.getEnvironment().operationRunner(), oSet);
			}
		};
		
		action.setText(Messages.DeleteFromFavorites);
		
		viewAccessor.getMenuManager().add(action);
	}
	
	
}
