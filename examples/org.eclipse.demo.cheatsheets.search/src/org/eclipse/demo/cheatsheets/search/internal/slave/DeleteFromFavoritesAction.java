/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.slave;

import java.util.Set;

import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.core.api.ISearchFavoritesMasterController;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;

/**
 * Action for deleting a cheat sheet from the search favorites view
 * @author danail
 *
 */
public class DeleteFromFavoritesAction implements IContributedAction
{
	public static final String ACTION_ID = DeleteFromFavoritesAction.class.getName();
	
	private final ISearchFavoritesMasterController controller;

	public DeleteFromFavoritesAction(final ISearchFavoritesMasterController controller)
	{
		this.controller = controller;
	}
	
	@Override
	public void perform(ILongOperationRunner operationRunner, Set<Object> selectedObjects)
	{
		controller.deleteItems(selectedObjects);
	}

	@Override
	public Object getActionId()
	{
		return ACTION_ID;
	}

}
