/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.destinations;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.demo.cheatsheets.search.internal.runtime.CSLoader;
import org.eclipse.demo.cheatsheets.search.internal.runtime.ICheatSheetCategory;
import org.eclipse.demo.cheatsheets.search.internal.runtime.ICheatSheetItem;
import org.eclipse.platform.discovery.runtime.api.IDestinationChangeHandler;
import org.eclipse.platform.discovery.runtime.api.IDestinationsProvider;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;

/**
 * Provider for cheat sheets search destinations
 * @author danail 
 */
public class LocalCSDestinationsProvider implements IDestinationsProvider
{
	@Override
	public Set<ISearchDestination> getSearchDestinations()
	{
		final Set<ISearchDestination> result = new HashSet<ISearchDestination>();
		final ICheatSheetCategory rootCategory = new CSLoader().loadCheatSheets();
		result.add(new AllCategoriesDest(rootCategory));
		
		for(ICheatSheetItem item : rootCategory.getChildren())
		{
			if(item instanceof ICheatSheetCategory)
			{
				result.add(new CSDestination((ICheatSheetCategory)item));
			}
		}
		
		return result;
	}

	@Override
	public void registerDestinationsChangeHandler(IDestinationChangeHandler handler)
	{
	}

	@Override
	public void unregisterDestinationsChangeHandler(IDestinationChangeHandler handler)
	{
	}
}
