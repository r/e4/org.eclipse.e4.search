/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.runtime;

import org.eclipse.ui.internal.cheatsheets.registry.CheatSheetElement;

@SuppressWarnings("restriction")
public class CheatSheetImpl implements ICheatSheet
{
	private CheatSheetElement csElement;
	private ICheatSheetCategory parent;

	public CheatSheetImpl(final CheatSheetElement csElement, final ICheatSheetCategory parent)
	{
		this.csElement = csElement;
		this.parent = parent;
	}
	
	@Override
	public String getID()
	{
		return this.csElement.getID();
	}

	@Override
	public String getName()
	{
		return this.csElement.getLabel(null);
	}

	@Override
	public ICheatSheetCategory getParent()
	{
		return this.parent;
	}
	
	@SuppressWarnings("nls")
	@Override
	public String toString()
	{
		return "Cheetsheet " + getName() + "; id=" + getID();
	}

	@Override
	public String getDescription()
	{
		return csElement.getDescription();
	}

	@Override
	public boolean isComposite()
	{
		return csElement.isComposite();
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null)
		{
			return false;
		}
		
		if (!obj.getClass().equals(this.getClass()))
		{
			return false;
		}

		final CheatSheetImpl cs = (CheatSheetImpl)obj;
		return this.csElement.equals(cs.csElement);
	}

	@Override
	public int hashCode()
	{
		return this.csElement.hashCode();
	}
}