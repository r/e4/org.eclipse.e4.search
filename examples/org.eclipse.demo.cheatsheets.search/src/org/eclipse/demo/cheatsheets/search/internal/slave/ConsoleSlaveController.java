/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.demo.cheatsheets.search.internal.slave;

import org.eclipse.platform.discovery.core.api.ISearchConsoleMasterController;
import org.eclipse.platform.discovery.core.api.ISearchConsoleSlaveController;

/**
 * The slave controller for the search console customization 
 * @author danail
 */
public class ConsoleSlaveController extends CSSlaveController implements ISearchConsoleSlaveController
{
	@Override
	public void setMasterController(ISearchConsoleMasterController masterController)
	{
	}
}
