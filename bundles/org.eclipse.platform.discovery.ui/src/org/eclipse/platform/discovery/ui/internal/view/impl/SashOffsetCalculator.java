/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.impl;

import org.eclipse.platform.discovery.ui.internal.SlidingComposite.IOffsetCalculator;
import org.eclipse.platform.discovery.ui.internal.SlidingComposite.ORIENTATION;
import org.eclipse.platform.discovery.ui.internal.util.DiscoveryUiUtil;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swt.widgets.ScrollBar;


public class SashOffsetCalculator implements IOffsetCalculator
{
	private final ORIENTATION currentOrientation;
	private final Sash sash;
	private final boolean upperVisible;
	private final ScrolledComposite scrolledUpperComposite;
	private final Canvas hideButton;
	private final Point paramsSectionSize;

	/**
	 * Constructor
	 * 
	 * @param sash
	 *            the sash
	 * @param scrolledUpperComposite
	 *            the scrolled composite which hosts the search parameters UI.
	 * @param paramsSectionSize
	 *            the size of the custom parameters section
	 * @param hideButton
	 *            the button which hides the parameters section
	 * @param orientation
	 *            current orientation
	 * @param upperPartVisible
	 *            true if the parameters section is visible, false otherwise
	 */
	public SashOffsetCalculator(final Sash sash, final ScrolledComposite scrolledUpperComposite, final Point paramsSectionSize, final Canvas hideButton, final ORIENTATION orientation,
									final Boolean upperPartVisible)
	{
		this.currentOrientation = orientation;
		this.sash = sash;
		this.upperVisible = upperPartVisible;
		this.scrolledUpperComposite = scrolledUpperComposite;
		this.hideButton = hideButton;
		this.paramsSectionSize = paramsSectionSize;
	}

	@Override
	public int determineDefaultOffest()
	{
		final DiscoveryUiUtil uiUtil = DiscoveryUiUtil.instance();
		final int parentSize = uiUtil.getHeightOrWidthDependingOnOrientation(sash.getParent(), currentOrientation);
		final int paramsSectionSizeValue = uiUtil.pointComponent(paramsSectionSize, currentOrientation);
		final int delta = parentSize - paramsSectionSizeValue - sashComponentHeight();

		int offset;
		if (delta < sashComponentHeight())
		{
			offset = parentSize;
		} else
		{
			offset = paramsSectionSizeValue + sashComponentHeight();
			if (currentOrientation == ORIENTATION.HORIZONTAL && scrolledCompositeVerticalBar().isVisible())
			{
				offset += scrolledCompositeVerticalBar().getSize().x;
			}
		}

		return offset;
	}

	@Override
	public int effectiveOffset(final int desiredOffset)
	{
		int effectiveOffset = 0;
		final DiscoveryUiUtil uiUtil = DiscoveryUiUtil.instance();
		final Composite sashParent = sash.getParent();

		if (currentOrientation == ORIENTATION.VERTICAL)
		{
			final int maxVerticalOffset = uiUtil.getHeightOrWidthDependingOnOrientation(sashParent, currentOrientation) - sashComponentHeight();
			effectiveOffset = Math.min(Math.max(Math.min(desiredOffset, maxVerticalOffset), sashComponentHeight()), paramsSectionSize.y + sashComponentHeight());
		} else
		{
			if (upperVisible)
			{
				final int paramsSectionSizeValue = uiUtil.pointComponent(paramsSectionSize, currentOrientation);
				final int verticalBarWidth = scrolledCompositeVerticalBar().isVisible() ? scrolledCompositeVerticalBar().getSize().x : 0;
				int horizontalOffset = Math.min(paramsSectionSizeValue + sashComponentHeight() + verticalBarWidth, desiredOffset);
				horizontalOffset = Math.min(horizontalOffset, uiUtil.getHeightOrWidthDependingOnOrientation(sashParent, currentOrientation) - 1);
				
				effectiveOffset = horizontalOffset;
			} else
			{
				effectiveOffset = sashComponentHeight();
			}
		}
		return Math.max(effectiveOffset, sashComponentHeight());
	}

	public int sashComponentHeight()
	{
		final int buttonSize = DiscoveryUiUtil.instance().getHeightOrWidthDependingOnOrientation(hideButton, currentOrientation);
		final int sashSize = DiscoveryUiUtil.instance().getHeightOrWidthDependingOnOrientation(sash, currentOrientation);
		return buttonSize + (sash.getVisible() ? sashSize : 0) + 1;
	}

	private ScrollBar scrolledCompositeVerticalBar()
	{
		return scrolledUpperComposite.getVerticalBar();
	}
}
