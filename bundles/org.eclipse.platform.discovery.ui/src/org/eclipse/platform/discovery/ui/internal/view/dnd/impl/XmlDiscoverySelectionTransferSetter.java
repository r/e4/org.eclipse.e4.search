/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.dnd.impl;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.platform.discovery.ui.internal.view.dnd.ISourceDndInteractionEvent;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.platform.discovery.util.internal.xml.CollectionTransformer;
import org.eclipse.platform.discovery.util.internal.xml.ICollectionTransformer;
import org.eclipse.swt.dnd.TextTransfer;


public class XmlDiscoverySelectionTransferSetter implements DragSrcInteractionListener.ITransferDataSetter<TextTransfer>
{
	private final ILongOperationRunner runner;
	
	public XmlDiscoverySelectionTransferSetter(ILongOperationRunner runner) {
		this.runner = runner;
	}

	@Override
	public void setData(IStructuredSelection data, ISourceDndInteractionEvent event)
	{
		final ICollectionTransformer trans = createTransformer();
		trans.setCollectionTag("discovery-selection"); //$NON-NLS-1$
		trans.setItemTag("selection-item"); //$NON-NLS-1$
		trans.setNamespace("http://eclipse.org/platform.discovery"); //$NON-NLS-1$
		event.setData(trans.transform(data.iterator(), runner));
	}
	
	protected ICollectionTransformer createTransformer()
	{
		return new CollectionTransformer();
	}

	@Override
	public void dragStarted(IStructuredSelection selection, ISourceDndInteractionEvent event)
	{
		// nothing to set
	}

	@Override
	public TextTransfer getTransfer()
	{
		return TextTransfer.getInstance();
	}
}
