/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.api.impl;


public class DefaultSessionIds {
	public static final String mainSearchSessionId = DefaultSessionIds.class.getCanonicalName() + ".mainSearchSession"; //$NON-NLS-1$
	public static final String subSearchSessionId = DefaultSessionIds.class.getCanonicalName() + ".subSearchSession"; //$NON-NLS-1$
}
