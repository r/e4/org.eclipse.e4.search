/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.tooltip;

import java.io.StringWriter;

import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.sax.TransformerHandler;

import org.eclipse.platform.discovery.ui.api.IFormTextBuilder;
import org.eclipse.platform.discovery.util.internal.xml.IXMLUtils;
import org.eclipse.platform.discovery.util.internal.xml.XMLUtils;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;


/**
 * Implementation of the {@link IFormTextBuilder} interface
 * 
 * @author Danail Branekov
 */
public class FormTextBuilder implements org.eclipse.platform.discovery.ui.internal.tooltip.AbstractTooltipConfigurator.TooltipFormTextBuilder
{
	private static final String EMPTY_STR = ""; //$NON-NLS-1$
	private static final String FORM_PREFIX = "form"; //$NON-NLS-1$
	private static final String PARAG_PREFIX = "p"; //$NON-NLS-1$
	private static final String BOLD_PREFIX = "b"; //$NON-NLS-1$
	private TransformerHandler transformerHandler;
	private StringWriter stringWriter;

	public FormTextBuilder()
	{
		try
		{
			stringWriter = new StringWriter();
			transformerHandler = xmlUtils().createTransformerHandler(stringWriter);
			transformerHandler.startDocument();
			transformerHandler.startElement(EMPTY_STR, EMPTY_STR, FORM_PREFIX, new AttributesImpl());
		} catch (SAXException e)
		{
			throw new IllegalStateException(e);
		} catch (TransformerConfigurationException e)
		{
			throw new IllegalStateException(e);
		}
	}

	protected IXMLUtils xmlUtils()
	{
		return new XMLUtils();
	}

	@Override
	public void startParagraph(final boolean appendVerticalSpace)
	{
		final AttributesImpl attr = new AttributesImpl();
		attr.addAttribute(EMPTY_STR, EMPTY_STR, "vspace", "CDATA", Boolean.toString(appendVerticalSpace)); //$NON-NLS-1$ //$NON-NLS-2$
		try
		{
			this.transformerHandler.startElement(EMPTY_STR, EMPTY_STR, PARAG_PREFIX, attr);
		} catch (SAXException e)
		{
			throw new RuntimeException(e);
		}
	}

	@Override
	public void endParagraph()
	{
		try
		{
			this.transformerHandler.endElement(EMPTY_STR, EMPTY_STR, PARAG_PREFIX);
		} catch (SAXException e)
		{
			throw new RuntimeException(e);
		}
	}

	@Override
	public void appendBoldedText(final String text)
	{
		try
		{
			this.transformerHandler.startElement(EMPTY_STR, EMPTY_STR, BOLD_PREFIX, new AttributesImpl());
			this.transformerHandler.characters(text.toCharArray(), 0, text.length());
			this.transformerHandler.endElement(EMPTY_STR, EMPTY_STR, BOLD_PREFIX);
		} catch (SAXException e)
		{
			throw new RuntimeException(e);
		}
	}

	@Override
	public void appendText(final String text)
	{
		try
		{
			this.transformerHandler.characters(text.toCharArray(), 0, text.length());
		} catch (SAXException e)
		{
			throw new RuntimeException(e);
		}
	}

	private void endDocument()
	{
		try
		{
			transformerHandler.endElement(EMPTY_STR, EMPTY_STR, FORM_PREFIX);
			transformerHandler.endDocument();
		} catch (SAXException e)
		{
			throw new IllegalStateException(e);
		}
	}

	@Override
	public String getText()
	{
		endDocument();
		return stringWriter.toString();
	}

	@Override
	public void appendProperty(final String propertyName, final String propertyValue)
	{
		startParagraph(false);
		appendBoldedText(propertyName);
		appendText(separator());
		appendText(propertyValue);
		endParagraph();
	}

	/**
	 * Returns the separator string for added properties
	 * 
	 * @see #appendProperty(String, String)
	 */
	protected String separator()
	{
		return " "; //$NON-NLS-1$
	}
}
