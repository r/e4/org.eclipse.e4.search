/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.api.impl;

import java.util.Iterator;
import java.util.List;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;

public class LabeledSelection implements ILabeledSelection {
	private final ILabelProvider lp;

	private final IStructuredSelection del;

	
	public LabeledSelection(ILabelProvider v, IStructuredSelection del) {
		super();
		this.lp = v;
		this.del = del;
	}
	
	@Override
	public ILabelProvider getLabelProvider()
	{
		return lp;
	}
	
	public Object getFirstElement() {
		return del.getFirstElement();
	}

	public boolean isEmpty() {
		return del.isEmpty();
	}

	public Iterator<?> iterator() {
		return del.iterator();
	}

	public int size() {
		return del.size();
	}

	public Object[] toArray() {
		return del.toArray();
	}

	public List<?> toList() {
		return del.toList();
	}
}
