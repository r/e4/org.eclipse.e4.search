/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal;

import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;

public abstract class SlidingCompositeStatePainter {
	
	protected static final int TRIANGLE_SIDE = 10;
	protected static final int TRIANGLE_HEIGHT = 5;
	protected static final int IDENTATION = 5;
	protected static final int ARC_BIG = 10;
	protected static final int ARC_MEDIUM = 6;
	protected static final int ARC_SMALL = 4;
	protected static final int ONE_PIXEL = 1;
	protected static final int TOP_RECTANGLE_WIDTH = 30;
	protected static final int WHITE_LINE_WIDTH = 2;
	protected static final String COLOR_WHITE = "WHITE"; //$NON-NLS-1$
	protected static final String BLUE_FILL = "BLUE_FILL"; //$NON-NLS-1$
	protected static final String BLUE_BORDER = "BLUE_BORDER"; //$NON-NLS-1$
	protected static final String COLOR_BLACK = "BLACK"; //$NON-NLS-1$
	protected final int SASH_HEIGHT = 2;
	protected int targetPosition = 0;
	protected FormToolkit formToolkit;
	
	public SlidingCompositeStatePainter(final FormToolkit formToolkit)
	{
		this.formToolkit = formToolkit;
	}
	
	public abstract void arrangeControls(FormData hideButtonFormData, FormData sashFormData, 
			FormData upperScrolledCompositeFormData, FormData bottomCompositeFormData, int targetPosition , int buttonHeight);
	
	public abstract  void printSliderButtonDown(final PaintEvent e, final Rectangle clientArea);
	public abstract void printSliderButtonUp(final PaintEvent e, final Rectangle clientArea);
	
	abstract int[] getArrowUpPolygon(final Rectangle clientArea);
	abstract int[] getArrowDownPolygon(final Rectangle clientArea);
	
	public abstract void arrangeControlsOnUpperVisibilityOff( FormData hideButtonFormData, FormData bottomCompositeFormData, int button_height);
	
	/**
	 * Adjusts the UI components layout data. This method must not call {@link Composite#layout()} method as this is responsibility of the caller
	 */
	public abstract void adjustControlsLayoutData(FormData upperScrolledCompositeData, FormData hideButtonData, FormData sashData, FormData bottomCompositeData, Canvas hideButton, int buttonHeight) ;
	
	protected int getHorizontalSize(final Rectangle clientArea) {
		return clientArea.width ;
	}

	protected int getVerticalSize(final Rectangle clientArea) {
		return clientArea.height ;
	}

	protected int getVerticalBeginPosition(final Rectangle clientArea) {
		return clientArea.y ;
	}

	protected int getHorizontalBeginPosition(final Rectangle clientArea) {
		return clientArea.x;
	}
	
	protected Color getTriangleColor() {
		return getColourById(COLOR_BLACK, new RGB(0, 0, 0));
	}

	protected Color getSliderFillColor() {
		return getColourById(BLUE_FILL, new RGB(234, 237, 243));
	}

	protected Color getSliderBorderColor() {
		return getColourById(BLUE_BORDER, new RGB(181, 189, 210));
	}
	
	protected Color getUnderBorderColor() {
		return getColourById(COLOR_WHITE, new RGB(255, 255, 255));
	}
	
	/**
	 * Gets the colour with the <code>key</code> specified from the formtoolkit. If the colour is not available, it is created with the RGB specified
	 * @param key the colour key
	 * @param the RGB value to be used when the colour is not available in the formtoolkit
	 */
	private Color getColourById(final String key, final RGB colourRgb)
	{
		final Color existingColour = formToolkit.getColors().getColor(key);
		return existingColour == null ? formToolkit.getColors().createColor(key, colourRgb) : existingColour;
	}
}
