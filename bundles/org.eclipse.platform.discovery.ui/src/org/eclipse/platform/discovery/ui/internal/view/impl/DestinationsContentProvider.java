/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.platform.discovery.runtime.api.IDescriptiveObject;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;


/**
 * The content provider used for the destinations tree viewer. Its input element is set of {@link IDestinationCategoryDescription}
 * 
 * @author Danail Branekov
 */
public abstract class DestinationsContentProvider implements ITreeContentProvider
{
	private final List<ISearchDestination> destinations;

	public DestinationsContentProvider()
	{
		destinations = new ArrayList<ISearchDestination>();
	}

	public Object[] getChildren(Object parentElement)
	{
		if (parentElement instanceof ISearchDestination)
		{
			return new Object[0];
		}

		final List<ISearchDestination> destinations = getSearchDestinationsForCategory((IDestinationCategoryDescription) parentElement);
		return sortSearchDestinations(destinations).toArray();
	}

	public Object getParent(Object element)
	{
		return null;
	}

	public boolean hasChildren(Object element)
	{
		return getChildren(element).length > 0;
	}

	@SuppressWarnings("unchecked")
	public Object[] getElements(Object inputElement)
	{
		assert inputElement instanceof List;

		final List<IDestinationCategoryDescription> categories = (List<IDestinationCategoryDescription>) inputElement;
		return sortDestinationCategories(categories).toArray();
	}

	public void dispose()
	{
		// nothing to dispose
	}

	@SuppressWarnings("unchecked")
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
	{
		destinations.clear();
		
		if(newInput != null)
		{
			assert (newInput instanceof List);
			destinations.addAll((List<ISearchDestination>) newInput);
		}
	}

	/**
	 * Gets the search destination for the destination category specified
	 */
	protected abstract List<ISearchDestination> getSearchDestinationsForCategory(final IDestinationCategoryDescription category);

	/**
	 * Sorts the specified displayed objects
	 */
	protected List<IDestinationCategoryDescription> sortDestinationCategories(final List<IDestinationCategoryDescription> categories)
	{
		final IDestinationCategoryDescription[] categoriesArr = categories.toArray(new IDestinationCategoryDescription[categories.size()]);
		Arrays.sort(categoriesArr, new Comparator<IDescriptiveObject>()
		{
			public int compare(IDescriptiveObject o1, IDescriptiveObject o2)
			{
				return o1.getDisplayName().compareTo(o2.getDisplayName());
			}
		});

		return Arrays.asList(categoriesArr);
	}
	
	private List<ISearchDestination> sortSearchDestinations(List<ISearchDestination> destinations)
	{
		final ISearchDestination[] destArr = destinations.toArray(new ISearchDestination[destinations.size()]);
		Arrays.sort(destArr, new Comparator<ISearchDestination>(){
			public int compare(ISearchDestination o1, ISearchDestination o2)
			{
				return o1.getDisplayName().compareTo(o2.getDisplayName());
			}});
		
		return Arrays.asList(destArr);
	}
	
}
