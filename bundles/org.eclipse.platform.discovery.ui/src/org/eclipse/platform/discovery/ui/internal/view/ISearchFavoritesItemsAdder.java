/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view;

import org.eclipse.jface.viewers.ISelection;

/**
 * Interface for adding items in the search favorites
 * 
 * @author Danail Branekov
 */
public interface ISearchFavoritesItemsAdder
{
	/**
	 * Checks whether the items contained in the selection specified could be added to the search favorites
	 * 
	 * @param selection
	 *            the selection to check
	 * @return <code>true</code> in case the items from the selection can be added, false otherwise
	 */
	public boolean canAddSelection(final ISelection selection);

	/**
	 * Adds the items specified in the search favorties. Will throw a runtime exception in case the selection is not acceptable (i.e. {@link #canAddSelection(ISelection)} returns false)
	 * 
	 * @param selectedItems
	 *            selection with the items to add
	 */
	public void addItems(final ISelection selectedItems);
}
