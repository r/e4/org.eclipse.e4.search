/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.core.api.ISearchFavoritesMasterController;
import org.eclipse.platform.discovery.core.internal.favorites.ISearchFavoritesControllerOutputView;
import org.eclipse.platform.discovery.ui.api.IGenericViewCustomization;
import org.eclipse.platform.discovery.ui.api.IMasterDiscoveryView;
import org.eclipse.platform.discovery.ui.api.IResultsViewAccessor;
import org.eclipse.platform.discovery.ui.api.ISearchFavoritesViewCustomization;
import org.eclipse.platform.discovery.ui.api.impl.LabeledSelection;
import org.eclipse.platform.discovery.ui.internal.dnd.LocalContextSelectionTransfer;
import org.eclipse.platform.discovery.ui.internal.plugin.DiscoveryUIMessages;
import org.eclipse.platform.discovery.ui.internal.util.ControlBackgroundImageManager;
import org.eclipse.platform.discovery.ui.internal.view.dnd.impl.DragSrcInteractionListener;
import org.eclipse.platform.discovery.ui.internal.view.favorites.FavoritesContentProvider;
import org.eclipse.platform.discovery.ui.internal.view.impl.AbstractDiscoveryView;
import org.eclipse.platform.discovery.ui.internal.view.impl.SearchFavoritesDropListener;
import org.eclipse.platform.discovery.ui.internal.view.result.impl.DiscoveryTreeViewerFactory;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.PlatformUI;


/**
 * The search favorites view.<br>
 * The view can be customized via the "org.eclipse.platform.discovery.integration.searchfavorites" extension point.<br>
 * Favorite items can be added via drag and drop. The view supports the standard eclipse {@link LocalSelectionTransfer} transfer type. In order the drop data to be accepted, the selection should contain only elements for which there is at least one contributed instance of the
 * {@link ISearchFavoritesViewCustomization} interface. If this is not the case, drop is rejected.<br>
 * The tree viewer of the view expects a set of objects as input.
 * 
 * @author Danail Branekov
 * 
 */
public class SearchFavoritesView extends AbstractDiscoveryView<ISearchFavoritesControllerOutputView, ISearchFavoritesMasterController> implements ISearchFavoritesControllerOutputView, ICustomizableView, IMasterDiscoveryView
{
	private ISearchFavoritesMasterController controller;
	private ViewProgressMonitor viewProgressMonitor;
	private TreeViewer treeViewer;
	private DiscoveryTreeViewerFactory treeViewerFactory;
	private Composite treeViewerComposite;
	private Composite parentComposite;
	private ControlBackgroundImageManager treeViewerBgImageManager;
	private ControlBackgroundImageManager treeViewerCompositeBgImageManager;

	public SearchFavoritesView()
	{
		super();
	}

	@Override
	public void createPartControl(final Composite parent)
	{
		super.createPartControl(parent);
		parentComposite = parent;
		final Composite composite = formToolkit.createComposite(parent);
		composite.setLayout(createGridLayout());

		treeViewerFactory = createTreeViewerFactory();
		
		treeViewerComposite = formToolkit.createComposite(composite);
		treeViewerCompositeBgImageManager = new ControlBackgroundImageManager(treeViewerComposite);
		treeViewerComposite.setBackground(getGradientEnd());
		treeViewerComposite.setForeground(getGradientStart());
		treeViewerComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		treeViewerComposite.addListener(SWT.Resize, new Listener(){
			@Override
			public void handleEvent(Event event) {
				treeViewerCompositeBgImageManager.setBackgroundGradient(getGradientStart(), getGradientEnd(), true);
			}
		});
		
		treeViewerComposite.setLayout(createGridLayout());
		// Only disable the composite in which the tree viewer is located in. Thus the progress monitor is not disabled
		viewProgressMonitor = new ViewProgressMonitor(composite,  Arrays.asList(new Control[] { treeViewerComposite }));
		viewProgressMonitor.getControl().setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 1, 1));
		this.getProgressMonitor().beginTask(DiscoveryUIMessages.INITIALIZATION_IN_PROGRESS_TASK, IProgressMonitor.UNKNOWN);
	}

		private GridLayout createGridLayout()
	{
		final GridLayout layout = new GridLayout(1, true);
		layout.marginHeight = 0;
		layout.marginWidth = 0;

		return layout;
	}

	protected DiscoveryTreeViewerFactory createTreeViewerFactory()
	{
		return new DiscoveryTreeViewerFactory()
		{
			@Override
			protected ITreeContentProvider contentProvider(final List<IGenericViewCustomization> viewCustomizations)
			{
				return new FavoritesContentProvider(viewCustomizations);
			}

			@Override
			public IResultsViewAccessor createTreeViewer(final Composite parent, final Set<? extends IGenericViewCustomization> viewCustomizations, final Set<IContributedAction> actions, final IDiscoveryEnvironment env)
			{
				final IResultsViewAccessor accessor = super.createTreeViewer(parent, viewCustomizations, actions, env);
				return accessor;
			}
			
			@Override
			protected DragSrcInteractionListener.ISelectionObtainer treeViewerSelectionObtainer(final TreeViewer viewer)
			{
				return new DragSrcInteractionListener.ISelectionObtainer()
				{
					@Override
					public IStructuredSelection getSelection()
					{
						return (IStructuredSelection) viewer.getSelection();
					}
				};
			}

			@Override
			protected IToolBarManager createToolbar(Composite parent, int style) 
			{
				return SearchFavoritesView.this.getViewSite().getActionBars().getToolBarManager();
			}
		};
	}

	@Override
	public void showFavorites(final Set<Object> favorites)
	{
		// Updates the tree in the UI thread. This is required as the method is called by the controller (possibly out of the UI thread) and accesses the tree viewer directly
		final Runnable runnable = new Runnable()
		{
			@Override
			public void run()
			{
				final ITreeContentProvider cp = (ITreeContentProvider)treeViewer.getContentProvider();
				final List<Object> oldRootElements = Arrays.asList(cp.getElements(treeViewer.getInput()));
				
				final List<Object> alreadyExpanded = Arrays.asList(treeViewer.getExpandedElements());
				treeViewer.setInput(favorites);
				
				final List<Object> newRootElements = Arrays.asList(cp.getElements(treeViewer.getInput()));//the new elements
				final List<Object> toExpand = new ArrayList<Object>();
				toExpand.addAll(newRootElements);
				toExpand.removeAll(oldRootElements);
				toExpand.addAll(alreadyExpanded);
				treeViewer.setExpandedElements(toExpand.toArray());
				treeViewer.refresh();
			}
		};
		PlatformUI.getWorkbench().getDisplay().asyncExec(runnable);
	}

	@Override
	public IProgressMonitor getProgressMonitor()
	{
		return this.viewProgressMonitor;
	}

	@Override
	public void initializationCompleted()
	{
		final IResultsViewAccessor accessor = treeViewerFactory.createTreeViewer(treeViewerComposite, viewCustomizations(), viewActions(), getEnvironment());
		treeViewer = accessor.getTreeViewer();
		treeViewerBgImageManager = new ControlBackgroundImageManager(treeViewer.getControl());
		
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener()
		{
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				setResultSelection(new LabeledSelection(
						(ILabelProvider)treeViewer.getLabelProvider(),
						(IStructuredSelection)event.getSelection()));
			}
		});
		
		treeViewer.getControl().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		treeViewerBgImageManager.setBackgroundGradient(getGradientStart(), getGradientEnd(), true);
		treeViewer.getTree().addListener(SWT.Resize, new Listener(){
			@Override
			public void handleEvent(Event event) {
				treeViewerBgImageManager.setBackgroundGradient(getGradientStart(), getGradientEnd(), true);
			}
		});
		
		installSearchFavoritesDropSupport(treeViewer);
		parentComposite.layout(true, true);

		super.initializationCompleted();
	}

	
	private void installSearchFavoritesDropSupport(final TreeViewer viewer)
	{
		final Transfer[] transfers = new Transfer[]{ LocalContextSelectionTransfer.getTransfer(), FileTransfer.getInstance()};
		viewer.addDropSupport(DND.DROP_COPY | DND.DROP_MOVE, transfers, new SearchFavoritesDropListener(transfers, controller));
	}

	@Override
	public void registerViewCustomization(final IGenericViewCustomization customization)
	{
		if (!(customization instanceof ISearchFavoritesViewCustomization))
		{
			throw new IllegalArgumentException("The view customization should be instance of ISearchFavoritesViewCustomization"); //$NON-NLS-1$
		}

		super.registerViewCustomization(customization);
	}
	
	@Override
	public void setFocus()
	{
		if(treeViewer != null)
		{
			treeViewer.getTree().setFocus();
		}
	}

	@Override
	public void setStatusMessage(final String message)
	{
		this.viewProgressMonitor.setMessage(message);
	}

	@Override
	public ISearchFavoritesControllerOutputView getControllerView() {
		return this;
	}

	@Override
	public void registerController(ISearchFavoritesMasterController controller) {
		this.controller = controller;
	}

	@Override
	public void showUnsupportedContentWindow() {
		showErrorMessageInDialog(DiscoveryUIMessages.SearchFavoritesView_UnsupportedDataContentMsg);
	}

	@Override
	public void showNoContentFoundWindow() {
		showErrorMessageInDialog(DiscoveryUIMessages.SearchFavoritesView_EmptyDataMsg);
	}
	
	private void showErrorMessageInDialog(final String message)
	{
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable()
		{
			@Override
			public void run()
			{
				final MessageDialog errDialog = new MessageDialog(parentComposite.getShell(), DiscoveryUIMessages.ErrorHandler_DIALOG_TITLE, null, message, MessageDialog.ERROR, new String[] { IDialogConstants.OK_LABEL }, 0);
				errDialog.open();
			}
		});
	}
	
	@Override
	public void dispose()
	{
		super.dispose();
		treeViewerCompositeBgImageManager.disposeResources();
		treeViewerBgImageManager.disposeResources();
	}
}
