/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.dialogs.ControlEnableState;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;


/**
 * {@link IProgressMonitor} implementation which is to be embedded in UI. It contains a {@link StatusLineManager} instance<br>
 * Whenever {@link #beginTask(String, int)} is invoked, status line managed is set to be cancelable and the specified composite is disabled. It is enabled back on {@link #done()}
 * 
 * @author Danail Branekov
 */
public class ViewProgressMonitor extends ForkInUiProgressMonitor implements IProgressMonitor
{
	private List<ControlEnableState> enableState;
	private final List<Control> controlsToDisable;
	private final StatusLineManager statusLineManager;
	
	/**
	 * Constructor
	 * 
	 * @param parent
	 *            the parent composite
	 * @param compositesToDisableWhileWorking
	 *            a list of composites which will be disabled while work is in progress
	 */

	public ViewProgressMonitor(final Composite parent, final List<Control> compositesToDisableWhileWorking)
	{
		statusLineManager = new StatusLineManager();
		statusLineManager.createControl(parent, SWT.NONE);
		this.controlsToDisable = compositesToDisableWhileWorking;
		
	}
	
	public void beginTask(String name, int totalWork)
	{
		this.statusLineManager.setCancelEnabled(true);
		enableState = new ArrayList<ControlEnableState>();
		for (Control c : controlsToDisable)
		{
			enableState.add(disableControls(c));
		}
		
		super.beginTask(name, totalWork);
	}

	public void done()
	{
		for (ControlEnableState state : enableState)
		{
			state.restore();
		}

		super.done();
	}

	public Control getControl()
	{
		return statusLineManager.getControl();
	}

	protected ControlEnableState disableControls(final Control control)
	{
		return ControlEnableState.disable(control);
	}

	@Override
	protected IProgressMonitor delegate()
	{
		return statusLineManager.getProgressMonitor();
	}

	public void setMessage(final String message)
	{
		this.statusLineManager.setMessage(message);
		
	}

	/**
	 * Sets the message, along with a image corresponding to error/warning/info severity. If another argument is provided in severity, the method returns.
	 */
	public void setMessage(final String message, final int severity) {
		String imageCode = null;
		switch (severity) {
		case (IStatus.INFO):
			imageCode = Dialog.DLG_IMG_MESSAGE_INFO;
			break;
		case (IStatus.WARNING):
			imageCode = Dialog.DLG_IMG_MESSAGE_WARNING;
			break;
		case (IStatus.ERROR):
			imageCode = Dialog.DLG_IMG_MESSAGE_ERROR;
			break;
		}
		if(imageCode==null) {
			return;
		}else{
			this.statusLineManager.setMessage(JFaceResources.getImage(imageCode), message);
		}
	}
}
