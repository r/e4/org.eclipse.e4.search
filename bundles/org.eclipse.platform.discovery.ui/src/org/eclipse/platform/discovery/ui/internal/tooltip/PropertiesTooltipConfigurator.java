/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.tooltip;

import java.util.Map;

import org.eclipse.platform.discovery.ui.api.IFormTextBuilder;
import org.eclipse.ui.forms.widgets.FormText;


/**
 * Extender of the {@link ToolTipConfigurator} interface which transforms an input paramaters map into a {@link FormText} string.<br>
 * Each line of the result text represents "key : value" of each map entry having the "key" bolded with a &ltb&gt tag. Properties are appended to the text via the {@link #appendProperty(String, String)} method
 * 
 * @author Danail Branekov
 * 
 */
public abstract class PropertiesTooltipConfigurator extends AbstractTooltipConfigurator implements IToolTipConfigurator
{
	protected abstract Map<String, String> tooltipProperties();

	@Override
	protected void createContent(final IFormTextBuilder tooltipBuilder)
	{
		final Map<String, String> props = tooltipProperties();
		for (String key : props.keySet())
		{
			tooltipBuilder.appendProperty(key, props.get(key));
		}
	}
}
