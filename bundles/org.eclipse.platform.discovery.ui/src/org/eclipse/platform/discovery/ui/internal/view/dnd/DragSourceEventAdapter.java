/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.dnd;

import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.TransferData;


/**
 * Implementation which adapts a {@link DragSourceEvent} to {@link IDndInteractionEvent}
 * @author Danail Branekov
 */
public class DragSourceEventAdapter implements ISourceDndInteractionEvent
{
	private final DragSourceEvent dragEvent;

	public DragSourceEventAdapter(final DragSourceEvent dragEvent)
	{
		this.dragEvent = dragEvent;
	}

	@Override
	public boolean getDoIt()
	{
		return this.dragEvent.doit;
	}

	@Override
	public void setDoIt(boolean doit)
	{
		this.dragEvent.doit = doit;
	}

	@Override
	public Object getData()
	{
		return this.dragEvent.data;
	}

	@Override
	public void setData(Object data)
	{
		this.dragEvent.data = data;
	}

	@Override
	public TransferData getDataType()
	{
		return this.dragEvent.dataType;
	}

	@Override
	public Integer getInteractionDetail()
	{
		return this.dragEvent.detail;
	}

	@Override
	public void setInteractionDetail(Integer detail)
	{
		this.dragEvent.detail = detail;
	}
}
