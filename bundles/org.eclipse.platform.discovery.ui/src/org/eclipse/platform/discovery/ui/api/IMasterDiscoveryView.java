/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.api;

import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;

/**
 * The interface for the master view in the discovery framework. View customizations can communicate with discovery view through this interface
 * 
 * @author Danail Branekov
 * @see ISeViewCustomization
 */
public interface IMasterDiscoveryView
{
	/**
	 * Retrieves the view environment
	 */
	public IDiscoveryEnvironment getEnvironment();
	
	/**
	 * Sets the string specified as status message
	 * @param message
	 */
	public void setStatusMessage(final String message);
}
