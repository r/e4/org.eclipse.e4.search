/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.result.impl;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.platform.discovery.core.api.ISearchContext;
import org.eclipse.platform.discovery.ui.api.IGenericViewCustomization;
import org.eclipse.platform.discovery.ui.api.ISearchConsoleCustomization;
import org.eclipse.platform.discovery.ui.api.ISearchResultCustomUiCreator;
import org.eclipse.platform.discovery.ui.api.impl.ILabeledSelection;
import org.eclipse.platform.discovery.ui.internal.selector.ICustomResultUiContributorDescription;
import org.eclipse.platform.discovery.ui.internal.view.SearchConsoleView;
import org.eclipse.platform.discovery.ui.internal.view.result.impl.TabbedSessionDisplayer.UI;
import org.eclipse.platform.discovery.ui.internal.view.result.impl.TabbedSessionDisplayer.UIFactory;
import org.eclipse.platform.discovery.ui.internal.xp.ICustomResultUiXpParser;
import org.eclipse.platform.discovery.ui.internal.xp.impl.CustomResultUiXpParser;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;


public class ContributedUIFactory implements UIFactory<ISearchContext>
{
	private final ICustomResultUiXpParser customResultXpParser;
	private final ISelectionChangedListener resultSelChangedListener;
	private final SearchConsoleView sv;

	public ContributedUIFactory(final SearchConsoleView sv)
	{
		this.sv = sv;
		customResultXpParser = newCustomResultXpParser();
		resultSelChangedListener = new ISelectionChangedListener()
		{
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				sv.setResultSelection((ILabeledSelection)event.getSelection());
			}
		};
	}
	
	protected ICustomResultUiXpParser newCustomResultXpParser() {
		return new CustomResultUiXpParser(Platform.getExtensionRegistry());
	}

	@Override
	public UI create(final ISearchContext sc, Composite parent) {
		return new ContributedUI(sc, parent);
	}

	@Override
	public FormToolkit getFormTookit() {
		return sv.getFormToolkit();
	}
	
	private class ContributedUI implements UI
	{
		private final String restoreDataKey = ContributedUI.class.getCanonicalName() + ".restore"; //$NON-NLS-1$
		final ISearchResultCustomUiCreator srcuc;
		final ISearchContext sc;
		final Composite parent;
		public ContributedUI(ISearchContext sc, Composite parent) {
			srcuc = getUiCreator(sc.searchProviderId());
			this.sc = sc;
			this.parent = parent;
		}

		@Override
		public void createControls() {
			srcuc.createSearchUi(parent, sc,
					sv.getFormToolkit(), 
					sv.viewActions(),
					relevantViewCustomizations(sv.viewCustomizations(), sc.searchProviderId()));
			srcuc.registerResultSelectionChangedListener(resultSelChangedListener);
		}

		@Override
		public void restoreControlsState() {
			srcuc.restore(sc.data().get(restoreDataKey));
		}

		@Override
		public void saveControlsState() {
			sc.data().put(restoreDataKey, srcuc.restoreData());
		}

		@Override
		public Composite parent() {
			return parent;
		}

		@Override
		public void dispose() {
			parent.dispose();
		}

		@Override
		public String title() {
			return sc.title();
		}
		
	}
	
	private Set<ISearchConsoleCustomization> relevantViewCustomizations(final Set<IGenericViewCustomization> allCustomizations, final String searchProviderId)
	{
		final Set<ISearchConsoleCustomization> result = new HashSet<ISearchConsoleCustomization>();
		for(IGenericViewCustomization viewC : allCustomizations)
		{
			if(((ISearchConsoleCustomization)viewC).acceptSearchProvider(searchProviderId))
			{
				result.add((ISearchConsoleCustomization)viewC);
			}
		}
		
		return result;
	}
	
	private ICustomResultUiContributorDescription getUiCreatorContributor(final String searchProviderId)
	{
		final Set<ICustomResultUiContributorDescription> contributors = new HashSet<ICustomResultUiContributorDescription>();
		for (ICustomResultUiContributorDescription descr : customResultXpParser.readContributions())
		{
			if (descr.getSearchProviderId().equals(searchProviderId))
			{
				contributors.add(descr);
			}
		}

		if (contributors.size() > 1)
		{
			throw new IllegalStateException("More than one result contribution for search provider " + searchProviderId); //$NON-NLS-1$
		}

		if (contributors.size() == 1)
		{
			return contributors.iterator().next();
		}

		return null;
	}

	private ISearchResultCustomUiCreator getUiCreator(final String searchProviderId)
	{
		final ICustomResultUiContributorDescription uiContributor = getUiCreatorContributor(searchProviderId);
		if (uiContributor != null)
		{
			return uiContributor.createUiCreator();
		}

		return fallbackUiCreator(searchProviderId);
	}

	public ISearchResultCustomUiCreator fallbackUiCreator(final String searchProviderId)
	{
		return new GenericResultUiCreator(searchProviderId, sv.getEnvironment());
	}
}