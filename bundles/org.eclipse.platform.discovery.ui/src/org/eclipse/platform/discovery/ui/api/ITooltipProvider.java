/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.api;

/**
 * Interface for tooltip providers
 * 
 * @author Danail Branekov
 */
public interface ITooltipProvider
{
	/**
	 * Creates the tooltip content via the {@link IFormTextBuilder} interface<br>
	 * 
	 * @param tooltipTextBuilder
	 * @param element
	 *            the element for which the tooltip content is to be provided
	 * @see IFormTextBuilder
	 * 
	 */
	public void createTooltipContent(final IFormTextBuilder tooltipTextBuilder, final Object element);
}
