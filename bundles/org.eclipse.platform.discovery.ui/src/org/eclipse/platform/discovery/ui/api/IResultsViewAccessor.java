/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.api;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.viewers.TreeViewer;

/**
 * Interface used by to access discovery tree viewer elements
 * 
 * @author Danail Branekov
 */
public interface IResultsViewAccessor
{
	/**
	 * Retrieves the tree viewer.
	 */
	public TreeViewer getTreeViewer();

	/**
	 * Retrieves the toolbar manager
	 * All actions and contributions should implement <c>getId</c> method. Otherwise exception will be thrown.
	 */
	public IToolBarManager getToolbarManager();
	
	/**
	 * Retrieves the menu manager of the tree viewer. Clients may use it to manage the tree viewer context menu
	 * @return
	 */
	public IMenuManager getMenuManager();
}

