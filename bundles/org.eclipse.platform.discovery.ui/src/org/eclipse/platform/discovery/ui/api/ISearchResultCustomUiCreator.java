/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.api;

import java.util.Set;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.core.api.ISearchContext;
import org.eclipse.platform.discovery.ui.api.impl.SearchResultCustomUiCreator;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;


/**
 * Interface for creating custom search result UI<br>
 * Do keep in mind that this interface might be a subject of a future change. Therefore, in order to avoid compilation problems, it is recommended to extend {@link SearchResultCustomUiCreator} instead of implementing this interface
 * 
 * @author Danail Branekov
 */
public interface ISearchResultCustomUiCreator
{
	
	/**
	 * Creates the search UI which will display the search result specified.
	 * 
	 * @param parent
	 *            the parent composite. Do note that its layout is not set. Implementors may set it to an appropriate one
	 * @param searchContext
	 *            search context which carries information about the search and its parameters
	 * @param formToolkit
	 *            form toolkit to create controls
	 * @param actions
	 *            a set of actions the result UI should be able to perform; empty set if none
	 * @param viewCustomizations
	 *            a set of view customizations which should customize the result UI; empty set if none
	 * @return the newly created composite which contains the UI components displaying the search result
	 */
	public Composite createSearchUi(final Composite parent, final ISearchContext searchContext, final FormToolkit formToolkit, final Set<IContributedAction> actions, final Set<ISearchConsoleCustomization> viewCustomizations);
	
	/**
	 * Registers a {@link ISelectionChangedListener} which will be notified whenever the search result selection changes
	 * @param selChangedListener
	 */
	public void registerResultSelectionChangedListener(final ISelectionChangedListener selChangedListener);
	
	/**
	 * Retrieves the state of the currently displayed result. At a later point of time this data may be passed to the restore() method
	 * @see #restore(Object)
	 */
	public Object restoreData();

	/**
	 * Restores the state specified by <code>data</code>
	 * @param data
	 * @see #restoreData()
	 */
	public void restore(Object data);
}
