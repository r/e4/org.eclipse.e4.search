/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.api.impl;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

/**
 * Label provider which provides the text and image of {@link ILabeledSelection} 
 */
public class LabeledSelectionLabelProvider extends LabelProvider {
	@Override
	public Image getImage(Object element) {
		if(element instanceof ILabeledSelection) {
			final ILabelProvider d = (ILabelProvider)((ILabeledSelection)element).getLabelProvider();
			return d.getImage(((ILabeledSelection)element).getFirstElement()); 
		}
		return null;
	}

	@Override
	public String getText(Object element) {
		if(element instanceof ILabeledSelection) {
			final ILabelProvider d = (ILabelProvider)((ILabeledSelection)element).getLabelProvider();
			return d.getText(((ILabeledSelection)element).getFirstElement()); 
		}
		return null;
	}
}
