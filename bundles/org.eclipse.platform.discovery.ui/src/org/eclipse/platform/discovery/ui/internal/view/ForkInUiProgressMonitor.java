/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

/**
 * Progress monitor implementation which would delegate invocations always in the UI thread. If the caller thread is a nonUI thread, delegations are executed in a {@link Display#asyncExec(Runnable)} block. The only exception is the {@link #isCanceled()} method which in case called
 * outside UI will be executed in a {@link Display#syncExec(Runnable)} block
 * 
 * @author Danail Branekov
 * 
 */
public abstract class ForkInUiProgressMonitor implements IProgressMonitor
{
	protected abstract IProgressMonitor delegate();

	private IProgressMonitor forkingDelegate(final boolean synchronous)
	{
		final InvocationHandler handler = new DelegatingInvocationHandler(delegate(), synchronous);
		return (IProgressMonitor) Proxy.newProxyInstance(delegate().getClass().getClassLoader(), new Class[] { IProgressMonitor.class }, handler);
	}

	public void beginTask(String name, int totalWork)
	{
		forkingDelegate(false).beginTask(name, totalWork);
	}

	public void done()
	{
		forkingDelegate(false).done();
	}

	public void internalWorked(double work)
	{
		forkingDelegate(false).internalWorked(work);
	}

	public boolean isCanceled()
	{
		return forkingDelegate(true).isCanceled();
	}

	public void setCanceled(boolean value)
	{
		forkingDelegate(false).setCanceled(value);
	}

	public void setTaskName(String name)
	{
		forkingDelegate(false).setTaskName(name);
	}

	public void subTask(String name)
	{
		forkingDelegate(false).subTask(name);
	}

	public void worked(int work)
	{
		forkingDelegate(false).worked(work);
	}

	private class DelegatingInvocationHandler implements InvocationHandler
	{
		private IProgressMonitor pm;
		private final boolean synchronous;

		public DelegatingInvocationHandler(final IProgressMonitor pm, final boolean synchronous)
		{
			this.pm = pm;
			this.synchronous = synchronous;
		}

		@Override
		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
		{
			final Object[] resultHolder = new Object[1];
			final Runnable runnable = invocationRunnable(method, args, resultHolder);
			if (Display.getCurrent() == null)
			{
				execInUi(runnable);
			} else
			{
				runnable.run();
			}

			return resultHolder[0];
		}
		
		private void execInUi(final Runnable runnable)
		{
			if(synchronous)
			{
				PlatformUI.getWorkbench().getDisplay().syncExec(runnable);
			}
			else
			{
				PlatformUI.getWorkbench().getDisplay().asyncExec(runnable);				
			}
		}

		private Runnable invocationRunnable(final Method method, final Object[] args, final Object[] resultHolder)
		{
			return new Runnable()
			{
				@Override
				public void run()
				{
					try
					{
						resultHolder[0] = method.invoke(pm, args);
					} catch (IllegalAccessException e)
					{
						throw new IllegalStateException(e);
					} catch (InvocationTargetException e)
					{
						throw new IllegalStateException(e);
					}
				}
			};
		}

	}
}
