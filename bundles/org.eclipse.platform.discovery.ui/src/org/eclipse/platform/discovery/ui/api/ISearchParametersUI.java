/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.api;

import java.util.List;
import java.util.Map;

import org.eclipse.platform.discovery.runtime.api.GroupingHierarchy;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.ISearchParameters;
import org.eclipse.platform.discovery.runtime.api.ISearchSubdestination;
import org.eclipse.platform.discovery.runtime.internal.ISearchProviderConfiguration;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.ISearchProviderDescription;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;

/**
 * The UI via which the user enters the search parameters.
 * 
 * @author Danail Branekov
 *
 */
public interface ISearchParametersUI {
	
	/**
	 * The context via which search parameters communicate with the search console input parameters section
	 */
	public interface IConsoleContext
	{
		/**
		 * Notifies the console that the search can be performed (i.e. the user provided sufficient input data)
		 */
		public void notifyComplete(final boolean complete);
		
		/**
		 * Provides the currently selected object type; the framework guarantees that this is not <code>null</code>
		 */
		public IObjectTypeDescription selectedObjectType();
		
		/**
		 * Provides the currently selected destination category; the framework guarantees that this is not <code>null</code>
		 */
		public IDestinationCategoryDescription destinationCategory();
		
		/**
		 * Provides the currently selected search destination. May be <code>null</code> if the user has not selected destination yet
		 */
		public ISearchDestination searchDestination();
		
		/**
		 * The search provider which is eligible to perform the search with the current input parameters (object type, destination category, search destination)
		 */
		public ISearchProviderDescription searchProvider();
		
		/**
		 * Notifies the console that the user changed the activation state of the subdestination specified
		 * @param subDestination the subdestination
		 * @param newActivationState the new activation state
		 */
		public void notifySubdestinationActivationChange(final ISearchSubdestination subDestination, final boolean newActivationState);
		
		/**
		 * Provides the console search provider configuration; never null
		 * @see ISearchProviderConfiguration
		 */
		public ISearchProviderConfiguration searchProviderConfiguration();
		
		/**
		 * The console aligns its fixed parameters UI (object type, search destination) in two columns. The digit returned by this method represents the 
		 * relative offset at which the second column starts. Contributors may have this value in mind in order to align their custom user interface gracefully 
		 */
		public int secondColumnLeftOffset();
	}
	
	/**
	 * Creates the UI.
	 * @param parent - the parent composite. The layout of this composite is not set by default. Implementation
	 * 		should set it.
	 * @param toolkit - the toolkit to create UI controls 
	 */
	public void createUI(final Composite parent, final FormToolkit toolkit, final IConsoleContext consoleContext);
	
	/**
	 * Notifies the parameters UI that a search provider has been selected. Implementors may perform some UI related routines (e.g. enable/disable UI elements) based on the search provider specified. 
	 * @param destination - the selected search provider. May be null if the user has selected a destination category (instead of destination) therefore unselecting destination, and effectively unselecting the search provider.
	 */
	public void searchProviderSelected(final ISearchProviderDescription selectedProvider);

	
	
	/**
	 * Gets specific parameters. The parameters are passed to the search provider, when performing search. 
	 * @see ISearchParameters#getCustomParameters()
	 */
	public Map<Object, Object> getParameters();

	/**
	 * Gets the search keyword. The keyword is passed to the search provider via the search parameters object. Therefore search provider and search UI implementations should handle the value returned as appropriate. The search console does not impose any restrictions on this value
	 * @see ISearchParameters#getKeywordString()
	 */
	public String getKeyword();
	
	/**
	 * Gets the grouping to display the search result. The hierarchy is passed to the search provider via the search parameters object. Therefore search provider and search UI implementations should handle the value returned as appropriate. The search console does not impose any restrictions on this value
	 * @see ISearchParameters#getGroupingHierarchy()
	 */
	public GroupingHierarchy getGroupingHierarchy();
	
	/**
	 * Displays the hierarchies which can be used to present the search result
	 */
	public void showGroupingHierarchies(final List<GroupingHierarchy> groupingHierarchies);	
}
