/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view;

import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.ui.api.IGenericViewCustomization;
import org.eclipse.platform.discovery.ui.api.IViewUiContext;


/**
 * Interface for views which could be customized
 * @author Danail Branekov
 */
public interface ICustomizableView
{
	/**
	 * Registers the action specified to the view
	 * @param action the action to register
	 */
	public void registerAction(final IContributedAction action);
	
	/**
	 * Registers a view customization to the view
	 * @param customization the view customization; must not be null
	 */
	public void registerViewCustomization(final IGenericViewCustomization customization);
	
	/**
	 * Sets the UI context
	 */
	public void setUiContext(final IViewUiContext uiContext);
}
