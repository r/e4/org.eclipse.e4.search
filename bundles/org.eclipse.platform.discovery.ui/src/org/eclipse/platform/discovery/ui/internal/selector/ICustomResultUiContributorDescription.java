/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.selector;

import org.eclipse.platform.discovery.ui.api.ISearchResultCustomUiCreator;

/**
 * Interface for describing custom search result UI contributions
 * 
 * @author Danail Branekov
 */
public interface ICustomResultUiContributorDescription
{
	/**
	 * The ID of the contributions, corresponds of the "id" element of the org.eclipse.platform.discovery.ui.customresultui
	 */
	public String getId();

	/**
	 * The ID of the search provider for which this contribution provides search UI
	 * 
	 * @return
	 */
	public String getSearchProviderId();

	/**
	 * Creates the {@link ISearchResultCustomUiCreator} instance which actually creates the very UI
	 * 
	 * @return
	 */
	public ISearchResultCustomUiCreator createUiCreator();
}
