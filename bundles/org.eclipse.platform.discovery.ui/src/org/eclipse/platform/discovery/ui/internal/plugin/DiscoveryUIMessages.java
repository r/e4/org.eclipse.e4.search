/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.plugin;

import org.eclipse.osgi.util.NLS;

public class DiscoveryUIMessages extends NLS
{
	private static final String BUNDLE_NAME = "org.eclipse.platform.discovery.ui.internal.plugin.DiscoveryUIMessages"; //$NON-NLS-1$

	public static String SEARCH_FOR_LIST_VIEWER_LABEL;
	public static String SEARCH_IN_LIST_VIEWER_LABEL;
	public static String SEARCH_KEYWORD_LABEL;
	public static String SEARCH_BUTTON_LABEL;

	public static String SearchFavoritesView_EmptyDataMsg;

	public static String SearchFavoritesView_UnsupportedDataContentMsg;

	public static String SearchConsoleView_EnterKeywordHint;
	public static String SearchConsoleView_NoDestinationSelectedHint;

	public static String SearchConsoleView_NoObjectTypeSelectedHint;
	public static String SearchConsoleView_UnsupportedTextSearch; 

	public static String SearchConsoleView_MANAGED_CONFIGS_LINK;
	public static String DESTINATION_CONFIGURATION_PREF_CATEGORY;
	public static String CUSTOM_PARAMS_SECTION_EXPAND_TITLE;
	public static String CUSTOM_PARAMS_SECTION_COLAPSE_TITLE;
	public static String GROUP_BY_LABEL;
	public static String INITIALIZATION_IN_PROGRESS_TASK; 
	public static String SEARCH_RESULT_TAB;

	public static String SlidingComposite_TooltipMessage_Click_To_Hide;
	public static String SlidingComposite_TooltipMessage_Click_To_Restore;

	public static String SlidingComposite_TooltipMessage_Drag_To_Resize;
	public static String ErrorHandler_UNEXPECTED_ERROR_MSG;
	public static String ErrorHandler_SEE_LOG_HINT_MSG;

	public static String ErrorHandler_SentenceCombiner;

	public static String ErrorHandler_SSL_EXC_DIALOG_TITLE;
	public static String ErrorHandler_DIALOG_TITLE;
	public static String AbstractDiscoveryDocPropertySection_NoDocumentationAvailable;

	public static String AbstractDiscoveryPropertySection_copyContentMenuItem;
	public static String AbstractDiscoveryPropertySection_tooltipCopyHint;
	public static String AbstractDiscoveryPropertySection_URLDisplayName;
	
	public static String AbstractSearchResultTab_GoToNext;

	public static String AbstractSearchResultTab_GoToPrevious;

	public static String NoResultsUiCreator_NO_RESULTS_FOUND;

	public static String SearchConsoleView_SearchResultsAvailable;
	public static String SearchConsoleView_SearchFinished;
	
	public static String SolutionApplier_RetryButton;
	public static String SolutionApplier_DialogMessage;
	public static String Properties_MenuItem;
	
	static
	{
		NLS.initializeMessages(BUNDLE_NAME, DiscoveryUIMessages.class);
	}
}
