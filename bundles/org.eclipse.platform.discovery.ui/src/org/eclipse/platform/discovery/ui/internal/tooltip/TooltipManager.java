/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.tooltip;

import org.eclipse.jface.text.AbstractHoverInformationControlManager;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Widget;


/**
 * Extender of {@link AbstractHoverInformationControlManager} for displaying tooltips on widgets
 * @author Danail Branekov
 *
 * @param <T> parameter for the widget class
 */
public abstract class TooltipManager<T extends Widget> extends AbstractHoverInformationControlManager
{
	protected TooltipManager(final IInformationControlCreator creator)
	{
		super(creator);
	}


	/**
	 * Retrieves the widget for which tooltip should be displayed or null
	 */
	protected abstract T hoveredWidget();

	/**
	 * Retrieves the bounds of the widget hovered. 
	 * @param hoveredWidget the widget; never null
	 */
	protected abstract Rectangle hoveredWidgetBounds(final T hoveredWidget);

	@Override
	protected void computeInformation()
	{
		final T widget = hoveredWidget();
		if(widget == null)
		{
			this.setInformation(null, null);
		}
		else
		{
			this.setInformation(createInformation(widget), informationArea());
		}
	}

	/**
	 * Creates the information object for the widget specified which will be set in {@link #setInformation(Object, org.eclipse.swt.graphics.Rectangle)} 
	 * @param control the control to create information for; not null
	 */
	protected abstract IToolTipConfigurator createInformation(final T widget);
	
	@Override
	public void install(Control subjectControl)
	{
		// Disable the default tooltip - we will show a custom one
		subjectControl.setToolTipText(""); //$NON-NLS-1$
		
		super.install(subjectControl);
	}
	
	private Rectangle informationArea()
	{
		final Rectangle bounds = hoveredWidgetBounds(hoveredWidget());
		final Point hoverLocation = getHoverEventLocation();
		return new Rectangle(hoverLocation.x, hoverLocation.y, bounds.width, bounds.height);
	}
}
