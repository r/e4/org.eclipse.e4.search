/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.xp.impl;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.AbstractExtensionPointParser;
import org.eclipse.platform.discovery.ui.api.IAdvancedSearchParamsUiContributor;
import org.eclipse.platform.discovery.ui.internal.search.advancedparams.IAdvancedSearchParamsUiContributorDescr;
import org.eclipse.platform.discovery.ui.internal.xp.IAdvancedSearchParamsUiContribXpParser;

/**
 * Implementation of the {@link IAdvancedSearchParamsUiContribXpParser} interface
 * 
 * @author Danail Branekov
 */
public class AdvancedSearchParamsUiContribXpParser extends AbstractExtensionPointParser<IAdvancedSearchParamsUiContributorDescr> implements IAdvancedSearchParamsUiContribXpParser
{
	public static final String XP_ID = "org.eclipse.platform.discovery.ui.advancedsearchparams"; //$NON-NLS-1$
	public static final String XP_ELEMENT_NAME = "advancedsearchparams"; //$NON-NLS-1$
	public static final String ID_ATTR_NAME = "id"; //$NON-NLS-1$
	public static final String SEARCH_PROVIDER_ID_ATTR_NAME = "searchproviderid"; //$NON-NLS-1$
	public static final String CONTRIBUTOR_CLASS_NAME_ATTR_NAME = "uicontributorclass"; //$NON-NLS-1$

	public AdvancedSearchParamsUiContribXpParser(final IExtensionRegistry extRegistry)
	{
		super(extRegistry, XP_ID, XP_ELEMENT_NAME);
	}
	
	@Override
	protected IAdvancedSearchParamsUiContributorDescr createObject(final IConfigurationElement element) throws CoreException {
		return new IAdvancedSearchParamsUiContributorDescr()
		{

			public IAdvancedSearchParamsUiContributor createContributor()
			{
				try
				{
					return (IAdvancedSearchParamsUiContributor) element.createExecutableExtension(CONTRIBUTOR_CLASS_NAME_ATTR_NAME);
				} catch (CoreException e)
				{
					throw new IllegalStateException(e);
				}
			}

			public String getId()
			{
				return element.getAttribute(ID_ATTR_NAME);
			}

			public String getSearchProviderId()
			{
				return element.getAttribute(SEARCH_PROVIDER_ID_ATTR_NAME);
			}
		};
	}

}
