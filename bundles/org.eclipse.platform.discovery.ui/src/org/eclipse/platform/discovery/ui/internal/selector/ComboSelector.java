/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.selector;

import java.util.Collection;
import java.util.List;

import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.platform.discovery.core.internal.ISingleValueSelector;
import org.eclipse.platform.discovery.runtime.api.IDisplayableObject;
import org.eclipse.platform.discovery.ui.internal.view.SearchConsoleView;
import org.eclipse.platform.discovery.ui.internal.view.impl.DisplayableObjectLabelProvider;
import org.eclipse.platform.discovery.ui.internal.view.impl.DisplayableObjectsContentProvider;
import org.eclipse.platform.discovery.ui.internal.view.impl.IControlValueSelector;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.widgets.FormToolkit;


/**
 * Implementation of the {@link ISingleValueSelector} interface which creates a combo box. The combo box displays the items to select from<br>
 * As the UI it creates uses {@link FormData} layout data, this selector should be used in composites which have {@link FormLayout} layouts
 */
public class ComboSelector<T extends IDisplayableObject> implements IControlValueSelector<T, Combo>
{
	private final ComboViewer comboViewer;
	private final Label comboLabelControl;

	/**
	 * Constructor
	 * 
	 * @param parent
	 *            the parent composite
	 * @param formToolkit
	 * @param formToolkit
	 *            form toolkit to create UI elements
	 * @param input
	 *            a set of items which represent the combo initial input
	 * @param secondColumnPosition
	 *            "Y" position of the combo left side
	 */
	public ComboSelector(final Composite parent, final FormToolkit formToolkit, final List<T> input, final String comboLabel, final int secondColumnPosition, final Control upperNeighbour)
	{
		comboLabelControl =  new Label(parent, SWT.FLAT);
		final FormData comboLabelFormData = new FormData();
		final FormAttachment topAttachment = (upperNeighbour==null)? new FormAttachment(0, SearchConsoleView.UI_IN_CONTROL_SPACING) : new FormAttachment(upperNeighbour, SearchConsoleView.UI_IN_CONTROL_SPACING) ; 
		
		
		comboLabelFormData.top = topAttachment;
		comboLabelFormData.left = new FormAttachment(0, SearchConsoleView.UI_IN_CONTROL_SPACING);
		comboLabelControl.setText(comboLabel);
		comboLabelControl.setLayoutData(comboLabelFormData);

		comboViewer = createComboViewer(parent, formToolkit, input);
		final FormData comboFormData = new FormData();
		comboFormData.top = topAttachment;
		comboFormData.left = new FormAttachment(0, secondColumnPosition + 2*SearchConsoleView.UI_IN_CONTROL_SPACING);
		comboFormData.right = new FormAttachment(100, - SearchConsoleView.UI_IN_CONTROL_SPACING);
		comboViewer.getControl().setLayoutData(comboFormData);
		comboViewer.setInput(input);
	}

	@SuppressWarnings("unchecked")
	public T getSelectedItem()
	{
		IStructuredSelection selection = (IStructuredSelection) comboViewer.getSelection();
		if (selection.isEmpty())
		{
			return null;
		}
		return (T) selection.getFirstElement();
	}

	public void registerSelectionChangedListener(ISelectionChangedListener listener)
	{
		comboViewer.addSelectionChangedListener(listener);
	}

	public void setInput(final Collection<T> input)
	{
		comboViewer.setInput(input);
	}

	private ComboViewer createComboViewer(final Composite parent, FormToolkit formToolkit, final List<T> input)
	{
		final ComboViewer viewer = new ComboViewer(parent, SWT.READ_ONLY | SWT.SINGLE );
		viewer.setLabelProvider(comboLabelProvider());
		viewer.setContentProvider(comboContentProvider(input));
		formToolkit.adapt(viewer.getControl(), true, true);

		return viewer;
	}

	/**
	 * Get the content provider for the combo box
	 */
	protected IStructuredContentProvider comboContentProvider(final List<T> input)
	{
		return new DisplayableObjectsContentProvider<T>(input);
	}

	/**
	 * Get the label provider for the combo box
	 */
	protected IBaseLabelProvider comboLabelProvider()
	{
		return new DisplayableObjectLabelProvider();		
	}
	
	protected ComboViewer getComboViewer()
	{
		return comboViewer;
	}

	public void setEnabled(boolean enabled)
	{
		getComboViewer().getControl().setEnabled(enabled);
		this.comboLabelControl.setEnabled(enabled);
	}
	
	@Override
	public boolean isEnabled()
	{
		return getComboViewer().getControl().isEnabled();
	}

	@Override
	public Combo getControl() {
		return (Combo)getComboViewer().getControl();
	}
}
