/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view;

import java.util.List;
import java.util.Set;

import org.eclipse.platform.discovery.core.internal.console.ISearchConsoleController;
import org.eclipse.platform.discovery.runtime.api.IDestinationChangeHandler;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.internal.DestinationCategoryNotFoundException;
import org.eclipse.platform.discovery.runtime.internal.ProviderNotFoundException;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.ISearchProviderDescription;
import org.eclipse.platform.discovery.ui.internal.plugin.DiscoveryUIMessages;
import org.eclipse.platform.discovery.ui.internal.selector.ComboSelector;
import org.eclipse.platform.discovery.ui.internal.view.impl.IControlValueSelector;
import org.eclipse.platform.discovery.ui.internal.view.impl.SearchConsoleDestinationsSelector;
import org.eclipse.platform.discovery.ui.internal.view.impl.SearchForSelectionChangeListener;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.forms.widgets.FormToolkit;

public class SearchProviderSelector implements ISearchProviderSelector {
	
	private IControlValueSelector<IObjectTypeDescription, Combo> searchForComboViewer;
	private SearchConsoleDestinationsSelector destinationsSelector;
	
	private ISearchConsoleController controller;
	private final ISearchProviderSelector.IConsoleContext consoleContext;
	private final Composite selectorComposite;
	
	@Override
	public void setController(ISearchConsoleController controller) {
		this.controller = controller;
	}

	public SearchProviderSelector(Composite parent, FormToolkit toolkit, ISearchProviderSelector.IConsoleContext consoleContext){
		this.consoleContext = consoleContext;
		this.selectorComposite = new Composite(parent, SWT.NONE);
		this.selectorComposite.setLayout(new FormLayout());
		createSearchForViewer(toolkit);
		createDestinationsSelector(toolkit);
	}


	/**
	 * Creates the "search for" composite. It contains
	 * <ul>
	 * <li>The "search for" label</li>
	 * <li>The "search for" list viewer</li>
	 * </ul>
	 * @param secondColumnPosition the "x" position of the second column 
	 */
	private void createSearchForViewer(FormToolkit toolkit) {
		searchForComboViewer = new ComboSelector<IObjectTypeDescription>(this.selectorComposite, toolkit, getObjectTypes(), DiscoveryUIMessages.SEARCH_FOR_LIST_VIEWER_LABEL, consoleContext.secondColumnLeftOffset(), null );
		searchForComboViewer.registerSelectionChangedListener(new SearchForSelectionChangeListener(){
			@Override
			protected void objectTypeSelected(IObjectTypeDescription objectType)
			{
				controller.objectTypeSelected(objectType);
				consoleContext.notifyComplete(isSearchProviderAvailable(objectType, destinationsSelector.getSelectedItem()));
			}
		});
	}
	
	/**
	 * Creates the "search in" composite. It contains
	 * <ul>
	 * <li>The "search in" label</li>
	 * <li>The "search in" list viewer</li>
	 * </ul>
	 * @param secondColumnPosition the "x" position of the second column 
	 * @param searchParametersUI 
	 */
	private void createDestinationsSelector(FormToolkit toolkit) {
		destinationsSelector = new SearchConsoleDestinationsSelector(this.selectorComposite, toolkit, consoleContext.secondColumnLeftOffset(), searchForComboViewer.getControl(), consoleContext.searchProviderConfiguration())
		{
			public void handleSelectionChange(final ISearchDestination newSelection)
			{
				final IObjectTypeDescription selectedObjectType = searchForComboViewer.getSelectedItem();
				if(selectedObjectType == null)
				{
					return;
				}

				consoleContext.notifyComplete(isSearchProviderAvailable(selectedObjectType, newSelection));
			}

		};
		destinationsSelector.registerDestinationsChangeHandler(new IDestinationChangeHandler()
		{
			@Override
			public void handleDestinationsChange()
			{
				controller.searchDestinationsChanged();
			}
		});
		destinationsSelector.setEnabled(false);
	}
	
	private List<IObjectTypeDescription> getObjectTypes() {
		return consoleContext.searchProviderConfiguration().getObjectTypes();
	}
	
	@Override
	public void setDestinations(List<IDestinationCategoryDescription> categories) {
		destinationsSelector.setEnabled(categories.size() > 0);
		destinationsSelector.setInput(categories);
	}

	public void setObjectTypes(Set<IObjectTypeDescription> objectTypes) {
		searchForComboViewer.setInput(objectTypes);
	}

	public Control getDestinationsControl() {
		return destinationsSelector.getControl();
	}


	/* (non-Javadoc)
	 * @see org.eclipse.platform.discovery.ui.internal.view.ISearchProviderSelector#getSelectedObjectType()
	 */
	@Override
	public IObjectTypeDescription getSelectedObjectType() {
		return searchForComboViewer.getSelectedItem();
	}


	/* (non-Javadoc)
	 * @see org.eclipse.platform.discovery.ui.internal.view.ISearchProviderSelector#getSelectedDestination()
	 */
	@Override
	public ISearchDestination getSelectedDestination() {
		return destinationsSelector.getSelectedItem();
	}

	@Override
	public void updateDestinationsUI() {
		destinationsSelector.update();
	}


	@Override
	public ISearchProviderDescription getSelectedSearchProvider() {
		return getSelectedSearchProvider(searchForComboViewer.getSelectedItem(), destinationsSelector.getSelectedItem());
	}

	private ISearchProviderDescription getSelectedSearchProvider(final IObjectTypeDescription selectedObjectType, final ISearchDestination selectedDestination) {
		if(selectedObjectType == null)
		{
			return null;
		}
		try {
			return consoleContext.searchProviderConfiguration().getActiveSearchProvider(selectedObjectType, getSelectedCategory(destinationsSelector.getSelectedItem()));
		} catch (ProviderNotFoundException e) {
			return null;
		}
	}

	@Override
	public void setObjectTypes(List<IObjectTypeDescription> objectTypes) {
		searchForComboViewer.setInput(objectTypes);
	}
	
	private boolean isSearchProviderAvailable(final IObjectTypeDescription selectedObjectType, final ISearchDestination selectedDestination) {
		return getSelectedSearchProvider(selectedObjectType, selectedDestination) != null;
	}
	
	private IDestinationCategoryDescription getSelectedCategory(final ISearchDestination selectedDestination)
	{
		if(selectedDestination == null)
		{
			return null;
		}
		
		try {
			return consoleContext.searchProviderConfiguration().getDestinationCategoriesForDestination(selectedDestination).iterator().next();
		} catch (DestinationCategoryNotFoundException e) {
			throw new IllegalStateException(e);
		}
	}

	@Override
	public Composite getControl() {
		return this.selectorComposite;
	}

	@Override
	public IDestinationCategoryDescription getActiveDestinationCategory() {
		return destinationsSelector.getActiveDestinationCategory();
	}
}
