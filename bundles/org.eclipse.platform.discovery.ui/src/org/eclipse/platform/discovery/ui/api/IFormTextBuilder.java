/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.api;

import org.eclipse.ui.forms.widgets.FormText;

/**
 * Interface for managing form text content. The form text content is generally a {@link FormText} (a kind of XML). Therefore the methods this interface provides would wrap any XML related exceptions into runtime exceptions
 * 
 * @author Danail Branekov
 */
public interface IFormTextBuilder
{
	/**
	 * Starts a paragraph (opens a &ltp&gt element)
	 * @param appendVerticalSpace whether to add vertical space to paragraph
	 * @see FormText
	 */
	public void startParagraph(boolean appendVerticalSpace);

	/**
	 * Ends a paragraph (closes a &ltp&gt element)
	 */
	public void endParagraph();

	/**
	 * Appends bolded text
	 * 
	 * @param text
	 *            the text to append
	 */
	public void appendBoldedText(final String text);

	/**
	 * Append regular text
	 * 
	 * @param text
	 *            text to append
	 */
	public void appendText(final String text);

	/**
	 * Appends property. the property name will be bolded. The property value will be with regular text
	 * 
	 * @param propertyName
	 * @param propertyValue
	 */
	public void appendProperty(final String propertyName, final String propertyValue);
}
