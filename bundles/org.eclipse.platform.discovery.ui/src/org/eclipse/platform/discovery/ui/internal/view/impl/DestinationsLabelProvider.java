/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.impl;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.swt.graphics.Image;


/**
 * The label provider used for the destinations tree viewer
 * @author Danail Branekov
 */
public class DestinationsLabelProvider implements ILabelProvider
{

	public void addListener(ILabelProviderListener listener)
	{
		// listeners not supported
	}

	public void dispose()
	{
		// nothing to dispose
	}

	public Image getImage(Object element)
	{
		return null;
	}

	public String getText(Object element)
	{
		if(element instanceof IDestinationCategoryDescription)
		{
			return ((IDestinationCategoryDescription)element).getDisplayName();
		}
		
		return ((ISearchDestination)element).getDisplayName();
	}

	public boolean isLabelProperty(Object element, String property)
	{
		return false;
	}

	public void removeListener(ILabelProviderListener listener)
	{
		// no listeners not supported
	}
}
