/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal;

import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.ui.forms.widgets.FormToolkit;

public class VerticalStatePainter extends SlidingCompositeStatePainter {

	public VerticalStatePainter(final FormToolkit formToolkit) {
		super(formToolkit);
	}

	@Override
	public int[] getArrowDownPolygon(Rectangle clientArea) {
		final int topAngleX = getTopAngleX(clientArea) ;
		final int topAngleY = getTopAngleY(clientArea) - TRIANGLE_HEIGHT;
		final int leftAngleX = getLeftAngleX(topAngleX);
		final int botomY = getBottomY(topAngleY);
		final int rightAngleX = getRightAngleX(topAngleX);
		final int [] arrowDown = { leftAngleX, topAngleY, rightAngleX, topAngleY, topAngleX, botomY };
		return arrowDown;
	}

	private int getBottomY(final int topAngleY) {
		return topAngleY + TRIANGLE_HEIGHT;
	}

	private int getLeftAngleX(final int topAngleX) {
		return topAngleX - TRIANGLE_SIDE / 2;
	}

	private int getTopAngleY(Rectangle clientArea) {
		return clientArea.y + clientArea.height/2;
	}

	private int getTopAngleX(Rectangle clientArea) {
		return clientArea.width / 2;
	}

	@Override
	public int[] getArrowUpPolygon(Rectangle clientArea) {
		final int topAngleX = getTopAngleX(clientArea) ;
        final int topAngleY = getTopAngleY(clientArea);
        final int leftAngleX= getLeftAngleX(topAngleX);
        final int botomY= getBottomY(topAngleY);
        final int rightAngleX= getRightAngleX(topAngleX);
        final int [] arrowUp = {topAngleX, topAngleY, leftAngleX, botomY, rightAngleX, botomY};
        return arrowUp;
	}

	private int getRightAngleX(final int topAngleX) {
		return topAngleX + TRIANGLE_SIDE/2;
	}

	@Override
	public void printSliderButtonDown(PaintEvent e, Rectangle clientArea) {
		final int X_COORD = getHorizontalBeginPosition(clientArea);
		final int Y_COORD = getVerticalBeginPosition(clientArea);
		final int HEIGHT = getVerticalSize(clientArea);
		final int WIDTH = getHorizontalSize(clientArea);

		final int HALF_HEIGHT = HEIGHT / 2;
		final int HALF_WIDTH = WIDTH / 2;

		// change BG color draw border rectangles
		e.gc.setForeground(getSliderBorderColor());
		e.gc.drawRoundRectangle(X_COORD, Y_COORD, WIDTH - ONE_PIXEL,	HALF_HEIGHT, ARC_MEDIUM, ARC_MEDIUM);
		e.gc.drawRoundRectangle(HALF_WIDTH - X_COORD - TOP_RECTANGLE_WIDTH / 2,	Y_COORD, TOP_RECTANGLE_WIDTH, 
					HEIGHT - IDENTATION, ARC_BIG, ARC_BIG);
			
			// change BG color and fill rectangles
			e.gc.setBackground(getSliderFillColor());
			e.gc.fillRoundRectangle(X_COORD + ONE_PIXEL + WHITE_LINE_WIDTH, Y_COORD + ONE_PIXEL, WIDTH - 2 * ONE_PIXEL - 2 * WHITE_LINE_WIDTH,
					HALF_HEIGHT - (ONE_PIXEL + WHITE_LINE_WIDTH), ARC_MEDIUM,	ARC_MEDIUM);

			e.gc.fillRectangle(X_COORD + ONE_PIXEL + WHITE_LINE_WIDTH, Y_COORD + ONE_PIXEL, WIDTH - 2 * ONE_PIXEL - 2 * WHITE_LINE_WIDTH,
					HALF_HEIGHT - 2 * ONE_PIXEL - 2 * WHITE_LINE_WIDTH);
			
			e.gc.fillRoundRectangle(HALF_WIDTH - X_COORD - TOP_RECTANGLE_WIDTH / 2 + ONE_PIXEL + WHITE_LINE_WIDTH, Y_COORD + ONE_PIXEL,
					TOP_RECTANGLE_WIDTH - 2 * WHITE_LINE_WIDTH - ONE_PIXEL, HEIGHT- (IDENTATION + WHITE_LINE_WIDTH + ONE_PIXEL), ARC_SMALL, ARC_SMALL);
	
			e.gc.setForeground(getUnderBorderColor());
			e.gc.setLineWidth(WHITE_LINE_WIDTH);
			e.gc.drawLine(X_COORD + ONE_PIXEL + WHITE_LINE_WIDTH, HALF_HEIGHT + ONE_PIXEL - WHITE_LINE_WIDTH, 
					HALF_WIDTH - X_COORD- TOP_RECTANGLE_WIDTH / 2 + ONE_PIXEL + WHITE_LINE_WIDTH, HALF_HEIGHT + ONE_PIXEL - WHITE_LINE_WIDTH);
	
			e.gc.drawLine(HALF_WIDTH + TOP_RECTANGLE_WIDTH / 2, HALF_HEIGHT	+ ONE_PIXEL - WHITE_LINE_WIDTH, 
					WIDTH - ONE_PIXEL - WHITE_LINE_WIDTH, HALF_HEIGHT + ONE_PIXEL - WHITE_LINE_WIDTH);
	
			e.gc.drawLine(HALF_WIDTH - X_COORD - TOP_RECTANGLE_WIDTH / 2 + WHITE_LINE_WIDTH, Y_COORD + HALF_HEIGHT, 
					HALF_WIDTH - X_COORD - TOP_RECTANGLE_WIDTH / 2 + WHITE_LINE_WIDTH, Y_COORD	+ HALF_HEIGHT + ONE_PIXEL);
	
			e.gc.drawLine(HALF_WIDTH + TOP_RECTANGLE_WIDTH / 2 - ONE_PIXEL, Y_COORD	+ HALF_HEIGHT,
					HALF_WIDTH + TOP_RECTANGLE_WIDTH / 2 - ONE_PIXEL, Y_COORD + HALF_HEIGHT + ONE_PIXEL);
			
			e.gc.setForeground(getSliderBorderColor());
			e.gc.setLineWidth(ONE_PIXEL);
			e.gc.setBackground(getTriangleColor());
			e.gc.drawLine(X_COORD, Y_COORD, X_COORD + WIDTH, Y_COORD);
			e.gc.fillPolygon(getArrowDownPolygon(clientArea));
		
	}

	@Override
	public void printSliderButtonUp(PaintEvent e, Rectangle clientArea) {
		final int X_COORD = getHorizontalBeginPosition(clientArea);
		final int Y_COORD = getVerticalBeginPosition(clientArea);
		final int HEIGHT = getVerticalSize(clientArea);
		final int WIDTH = getHorizontalSize(clientArea);

		final int HALF_HEIGHT = HEIGHT / 2;
		final int HALF_WIDTH = WIDTH / 2;

		// change BG color draw border rectangles
		e.gc.setForeground(getSliderBorderColor());
		e.gc.drawRoundRectangle(X_COORD, HALF_HEIGHT, WIDTH - ONE_PIXEL, HALF_HEIGHT, ARC_MEDIUM, ARC_MEDIUM);
			e.gc.drawRoundRectangle(HALF_WIDTH - X_COORD - TOP_RECTANGLE_WIDTH / 2, Y_COORD + IDENTATION, TOP_RECTANGLE_WIDTH, HEIGHT - IDENTATION,
					ARC_BIG, ARC_BIG);
			// change BG color and fill rectangles
			e.gc.setBackground(getSliderFillColor());
			e.gc.fillRoundRectangle(X_COORD + ONE_PIXEL + WHITE_LINE_WIDTH, Y_COORD + HALF_HEIGHT + ONE_PIXEL + WHITE_LINE_WIDTH, WIDTH - 2
					* ONE_PIXEL - 2 * WHITE_LINE_WIDTH, HALF_HEIGHT - 2 * ONE_PIXEL	- WHITE_LINE_WIDTH, ARC_MEDIUM, ARC_MEDIUM);
			e.gc.fillRectangle(X_COORD + ONE_PIXEL + WHITE_LINE_WIDTH, Y_COORD 	+ HALF_HEIGHT + ONE_PIXEL + 2 * WHITE_LINE_WIDTH, WIDTH - 2
					* ONE_PIXEL - 2 * WHITE_LINE_WIDTH, HALF_HEIGHT - 2 * ONE_PIXEL - 2 * WHITE_LINE_WIDTH);
			
			e.gc.fillRoundRectangle(HALF_WIDTH - X_COORD - TOP_RECTANGLE_WIDTH / 2 + ONE_PIXEL + WHITE_LINE_WIDTH, Y_COORD + IDENTATION
					+ ONE_PIXEL + WHITE_LINE_WIDTH, TOP_RECTANGLE_WIDTH - 2* WHITE_LINE_WIDTH - ONE_PIXEL, HEIGHT - IDENTATION, ARC_SMALL, ARC_SMALL);
			
			e.gc.setForeground(getUnderBorderColor());
			e.gc.setLineWidth(WHITE_LINE_WIDTH);
			e.gc.drawLine(X_COORD + ONE_PIXEL + WHITE_LINE_WIDTH, Y_COORD + HALF_HEIGHT + WHITE_LINE_WIDTH, 
					HALF_WIDTH - X_COORD - TOP_RECTANGLE_WIDTH / 2 + ONE_PIXEL + WHITE_LINE_WIDTH, Y_COORD + HALF_HEIGHT + WHITE_LINE_WIDTH);
			
			e.gc.drawLine(HALF_WIDTH + TOP_RECTANGLE_WIDTH / 2, Y_COORD + HALF_HEIGHT + WHITE_LINE_WIDTH, WIDTH - ONE_PIXEL
					- WHITE_LINE_WIDTH, Y_COORD + HALF_HEIGHT + WHITE_LINE_WIDTH);

			e.gc.drawLine(HALF_WIDTH - X_COORD - TOP_RECTANGLE_WIDTH / 2 + WHITE_LINE_WIDTH, Y_COORD + HALF_HEIGHT, HALF_WIDTH - X_COORD
					- TOP_RECTANGLE_WIDTH / 2 + WHITE_LINE_WIDTH, Y_COORD + HALF_HEIGHT + ONE_PIXEL);
			
			e.gc.drawLine(HALF_WIDTH + TOP_RECTANGLE_WIDTH / 2 - ONE_PIXEL, Y_COORD + HALF_HEIGHT, 	HALF_WIDTH + TOP_RECTANGLE_WIDTH / 2 
					- ONE_PIXEL, Y_COORD  + HALF_HEIGHT + ONE_PIXEL);
			
			e.gc.setForeground(getSliderBorderColor());
			e.gc.setLineWidth(ONE_PIXEL);
			e.gc.setBackground(getTriangleColor());
			e.gc.drawLine(X_COORD, Y_COORD + HEIGHT - ONE_PIXEL, X_COORD + WIDTH, Y_COORD + HEIGHT - ONE_PIXEL);
			e.gc.fillPolygon(getArrowUpPolygon(clientArea));
		
	}

	@Override
	public void arrangeControls(FormData hideButtonFormData, FormData sashFormData, 
			FormData upperScrolledCompositeFormData, FormData bottomCompositeFormData, int targetPosition , int buttonHeight) {
		hideButtonFormData.bottom = new FormAttachment(0, targetPosition);
		hideButtonFormData.top = new FormAttachment(0, targetPosition - buttonHeight);
		sashFormData.top = hideButtonFormData.bottom;
		sashFormData.bottom = new FormAttachment(0, targetPosition + SASH_HEIGHT );
		upperScrolledCompositeFormData.bottom = hideButtonFormData.top;
		bottomCompositeFormData.top = sashFormData.bottom;
		
	}

	@Override
	public void arrangeControlsOnUpperVisibilityOff( FormData hideButtonFormData, FormData bottomCompositeFormData,
			int buttonHeight) {
		hideButtonFormData.top = new FormAttachment(0, 0);
		hideButtonFormData.bottom = new FormAttachment(0, buttonHeight);
		bottomCompositeFormData.top = hideButtonFormData.bottom;
		
	}

	@Override
	public void adjustControlsLayoutData(FormData upperScrolledCompositeData,	FormData hideButtonData, FormData sashData,	
			FormData bottomCompositeData, Canvas hideButton, int buttonHeight) {
		upperScrolledCompositeData.bottom = new FormAttachment(hideButton);
		hideButtonData.height = buttonHeight;
		sashData.height = SASH_HEIGHT;
		
	}

}
