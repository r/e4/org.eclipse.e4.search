/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.dnd;


/**
 * Interface for interaction listeners
 * @author Danail Branekov
 * 
 * @param <T> the type of data the interaction will process
 * @param <D> the data processed by this interaction
 * @param <E> the event type this listener handles
 */
public interface IInteractionListener<E extends IInteractionEvent<T, D, Integer>, T, D>
{
	/**
	 * Interaction is starting
	 * @param event the interaction start event
	 */
	public void start(E event);
	
	/**
	 * The interaction is done
	 * @param event the interaction end event
	 */
	public void finish(E event);
	
	/**
	 * Start interaction processing
	 * @param event
	 */
	public void process(E event);
}
