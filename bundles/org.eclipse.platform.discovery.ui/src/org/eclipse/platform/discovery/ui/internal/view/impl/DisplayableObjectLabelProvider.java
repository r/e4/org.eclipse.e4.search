/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.impl;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.platform.discovery.runtime.api.IDisplayableObject;


/**
 * Label provider which provides labels for {@link IDisplayableObject} instances
 * @author Danail Branekov
 */
public class DisplayableObjectLabelProvider extends LabelProvider
{
	@Override
	public String getText(Object element)
	{
		assert element instanceof IDisplayableObject;
		
		final IDisplayableObject objectType = (IDisplayableObject)element;
		return objectType.getDisplayName();
	}
}
