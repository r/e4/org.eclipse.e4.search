/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.result.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.core.api.ISearchContext;
import org.eclipse.platform.discovery.core.internal.ContextStructuredSelection;
import org.eclipse.platform.discovery.ui.api.IGenericViewCustomization;
import org.eclipse.platform.discovery.ui.api.IResultsViewAccessor;
import org.eclipse.platform.discovery.ui.internal.tooltip.TooltipCreator;
import org.eclipse.platform.discovery.ui.internal.tooltip.TreeViewerTooltipManager;
import org.eclipse.platform.discovery.ui.internal.util.ControlBackgroundImageManager;
import org.eclipse.platform.discovery.ui.internal.view.SortingMenuManager;
import org.eclipse.platform.discovery.ui.internal.view.dnd.impl.DragSrcInteractionListener;
import org.eclipse.platform.discovery.ui.internal.view.dnd.impl.DragSrcListener;
import org.eclipse.platform.discovery.ui.internal.view.dnd.impl.LocalSelectionTransferSetter;
import org.eclipse.platform.discovery.ui.internal.view.dnd.impl.XmlDiscoverySelectionTransferSetter;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;


/**
 * Factory class for creating a tree viewer component which has all the contributed label/content providers, context menu actions, a toolbar if necessary, drag and drop support
 * 
 * @author Danail Branekov
 */
public class DiscoveryTreeViewerFactory 
{

	private List<IGenericViewCustomization> availableViewCustomizations;
	private List<IContributedAction> availableActions;
	private IResultsViewAccessor viewAccessor;

	private ControlBackgroundImageManager treeBgImageManager;

	/**
	 * Creates the UI
	 * 
	 * @param parent
	 *            the parent composite. It is expected that its layout is {@link GridLayout}
	 * @param viewCustomizations
	 *            a set of view customizations
	 * @param actions
	 *            a set of actions to be installed
	 * @param env
	 *            the environment
	 * @return an instance of {@link IResultsViewAccessor} which can be used to access the created UI components
	 */
	public IResultsViewAccessor createTreeViewer(final Composite parent, final Set<? extends IGenericViewCustomization> viewCustomizations, final Set<IContributedAction> actions, IDiscoveryEnvironment env)
	{
		this.availableViewCustomizations = new ArrayList<IGenericViewCustomization>(viewCustomizations);
		this.availableActions = new ArrayList<IContributedAction>(actions);

		final IToolBarManager toolbarManager = createToolbar(parent, SWT.TOOL|SWT.FLAT|SWT.RIGHT_TO_LEFT);
		
		final TreeViewer treeViewer = createTreeViewerInternal(parent, contentProvider(availableViewCustomizations), labelProvider(availableViewCustomizations));
		installDoubleClickListeners(treeViewer, availableViewCustomizations);
		
		final MenuManager menuManager = createMenuManager(treeViewer);
		
		this.viewAccessor = viewAccessor(treeViewer, toolbarManager, menuManager);
		installDragSupportForResultTreeViewer(viewAccessor.getTreeViewer(), env);
		installActions(availableViewCustomizations, availableActions, viewAccessor);
		installTooltip(viewAccessor, getGenericResultLabelProvider(viewCustomizations));
		return viewAccessor;
	}
	
	protected IToolBarManager createToolbar(final Composite parent, final int style)
	{
		return null;
	}

	private void installTooltip(final IResultsViewAccessor va, final CellLabelProvider labelProvider)
	{
		final TreeViewerTooltipManager tooltipManager = new TreeViewerTooltipManager(va.getTreeViewer(), labelProvider, new TooltipCreator());
		tooltipManager.install(va.getTreeViewer().getControl());
	}
	
	private GenericResultLabelProvider getGenericResultLabelProvider(final Set<? extends IGenericViewCustomization> viewCustomizations)
	{
		List<IGenericViewCustomization> genericViewCustomizations = new ArrayList<IGenericViewCustomization>();
		
		for( IGenericViewCustomization c : viewCustomizations)
		{
			genericViewCustomizations.add((IGenericViewCustomization)c);
		}
		
		return new GenericResultLabelProvider(genericViewCustomizations);
	}
	
	private void installDoubleClickListeners(final TreeViewer treeViewer, final List<IGenericViewCustomization> customizations)
	{
		for(IGenericViewCustomization viewC : customizations)
		{
			final IDoubleClickListener listener = viewC.getDoubleClickListener();
			if(viewC.getDoubleClickListener() != null)
			{
				treeViewer.addDoubleClickListener(listener);
			}
		}
	}

	protected ILabelProvider labelProvider(final List<IGenericViewCustomization> viewCustomizations)
	{
		return new GenericResultLabelProvider(viewCustomizations);
	}

	protected ITreeContentProvider contentProvider(final List<IGenericViewCustomization> viewCustomizations)
	{
		return new GenericResultContentProvider(viewCustomizations);
	}

	private TreeViewer createTreeViewerInternal(final Composite parent, final ITreeContentProvider cProvider, final ILabelProvider lProvider)
	{
		final TreeViewer viewer = new TreeViewer(parent, SWT.MULTI /*| SWT.BORDER */);
		viewer.setContentProvider(cProvider);

		viewer.setLabelProvider(lProvider);
		treeBgImageManager = new ControlBackgroundImageManager(viewer.getControl());
		treeBgImageManager.setBackground(viewer.getControl().getBackground());
		viewer.getControl().addListener(SWT.Resize, new Listener()
		{
			@Override
			public void handleEvent(Event event)
			{
				treeBgImageManager.setBackground(viewer.getControl().getBackground());
			}
		});
		viewer.getControl().addDisposeListener(new DisposeListener()
		{
			@Override
			public void widgetDisposed(DisposeEvent e)
			{
				DiscoveryTreeViewerFactory.this.dispose();
			}
		});

		return viewer;
	}

	public void dispose() 
	{
		treeBgImageManager.disposeResources();
	}
	
	private void installDragSupportForResultTreeViewer(final TreeViewer viewer, final IDiscoveryEnvironment env)
	{
		int supportedOperations = DND.DROP_MOVE | DND.DROP_COPY;
		final DragSrcListener listener = new DragSrcListener(treeViewerSelectionObtainer(viewer), supportedTransferDataSetters(env), env.errorHandler()){
			@Override
			public void dragStart(DragSourceEvent event)
			{
				// When the drag starts the tooltip might be displayed. When the user moves the mouse the tooltip will hide but the viewer 
				// would not have refreshed itself (we are in the main thread now => no event loop) which results in a "polluted" viewer. In order to avoid this, schedule a viewer redraw   
				viewer.getControl().redraw();
				super.dragStart(event);
			}
		};
		viewer.addDragSupport(supportedOperations, listener.getTransfers(), listener);
	}
	
	/**
	 * Creates a {@link DragSrcInteractionListener.ISelectionObtainer} instance which is responsible for obtaining the tree viewer selection
	 * @param viewer
	 * @return
	 */
	protected DragSrcInteractionListener.ISelectionObtainer treeViewerSelectionObtainer(final TreeViewer viewer)
	{
		return new DragSrcInteractionListener.ISelectionObtainer()
		{
			@Override
			public IStructuredSelection getSelection()
			{
				final IStructuredSelection selection = (StructuredSelection) viewer.getSelection();
				final ContextStructuredSelection contextSelection = new ContextStructuredSelection(selection.toList(), (ISearchContext) viewer.getInput());
				return contextSelection;
			}
		};
	}

	/**
	 * Create an array of supported transfer drag source data setters
	 * 
	 * @param env
	 *            environment
	 * @return an array of supported transfer data setters or empty array if none
	 */
	@SuppressWarnings("unchecked")
	protected DragSrcInteractionListener.ITransferDataSetter<? extends Transfer>[] supportedTransferDataSetters(final IDiscoveryEnvironment env)
	{
		final LocalSelectionTransferSetter t1 = new LocalSelectionTransferSetter();
		final XmlDiscoverySelectionTransferSetter t2 = new XmlDiscoverySelectionTransferSetter(env.operationRunner());
				
		return new DragSrcInteractionListener.ITransferDataSetter[] { t1, t2 };
	}

	
	private MenuManager createMenuManager(final TreeViewer viewer)
	{
		final MenuManager menuManager = new SortingMenuManager();
		menuManager.setRemoveAllWhenShown(true);
		menuManager.addMenuListener(new IMenuListener()
		{
			@Override
			public void menuAboutToShow(IMenuManager manager)
			{
				installActions(availableViewCustomizations, availableActions, viewAccessor);
			}
		});
		final Menu menu = menuManager.createContextMenu(viewer.getTree());
		viewer.getTree().setMenu(menu);

		return menuManager;
	}

	private IResultsViewAccessor viewAccessor(final TreeViewer treeViewer, final IToolBarManager toolbarManager, final MenuManager manager)
	{
		return new IResultsViewAccessor()
		{
			public IToolBarManager getToolbarManager()
			{
				return toolbarManager;
			}

			public TreeViewer getTreeViewer()
			{
				return treeViewer;
			}

			public IMenuManager getMenuManager()
			{
				return manager;
			}
		};
	}

	private void installActions(final List<IGenericViewCustomization> viewCustomizations, final List<IContributedAction> actions, final IResultsViewAccessor viewAccessor)
	{
		for (IGenericViewCustomization viewc : viewCustomizations)
		{
			for (IContributedAction act : actions)
			{
				viewc.installAction(act, viewAccessor);
			}
		}
	}
}
