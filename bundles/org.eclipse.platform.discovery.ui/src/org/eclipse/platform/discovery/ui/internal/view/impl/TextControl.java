/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.impl;

import org.eclipse.platform.discovery.core.internal.IPropertyControl;
import org.eclipse.platform.discovery.ui.internal.view.IGetControlObject;
import org.eclipse.platform.discovery.util.internal.property.IPropertyAttributeListener;
import org.eclipse.platform.discovery.util.internal.property.Property;
import org.eclipse.platform.discovery.util.internal.property.PropertyAttributeChangedEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;


/**
 * Implementation of the {@link IPropertyControl} interface which uses a {@link Text}
 * 
 * @author Danail Branekov
 */
public class TextControl implements IPropertyControl<String>, IGetControlObject<Text>
{
	private final Property<String> textValue;
	private final Property<String> messageValue;
	private final Text text;
	private final Label associatedLabel;

	/**
	 * Constructor
	 * @param text the text control; cannot be null
	 * @param associatedLabel the associated label control; cannot be null
	 */
	public TextControl(final Text text, final Label associatedLabel)
	{
		this.text = text;
		this.associatedLabel = associatedLabel;
		textValue = new Property<String>();
		messageValue = new Property<String>();
		messageValue.set(""); // default message (empty string) //$NON-NLS-1$
		messageValue.registerValueListener(new MessageChangeListener(){}, false);
		textValue.set(text.getText());
		

		this.text.addModifyListener(new ModifyListener()
		{
			@Override
			public void modifyText(ModifyEvent e)
			{
				if(!TextControl.this.text.isEnabled())
				{
					return;
				}
				
				textValue.set(TextControl.this.text.getText());
			}
		});
	}

	@Override
	public String get()
	{
		return this.text.getText();
	}

	@Override
	public void set(String value)
	{
		this.text.setText(value);
	}

	@Override
	public boolean isEnabled()
	{
		return this.text.isEnabled();
	}

	@Override
	public void setEnabled(boolean enabled)
	{
		this.text.setEnabled(enabled);
		this.associatedLabel.setEnabled(enabled);
		
		if(enabled)
		{
			this.text.setText(enabled ? textValue.get() : ""); //$NON-NLS-1$
			this.text.setMessage(messageValue.get());
		}
		else
		{
			this.text.setText(messageValue.get());
			this.text.setMessage(""); //$NON-NLS-1$
		}
	}

	@Override
	public Text getControl()
	{
		return this.text;
	}
	
	/**
	 * Sets the text control message. If the control is enabled, then the method will delegate to {@link Text#setMessage(String)}. Otherwise, the message will be set as text to the {@link Text} control
	 * 
	 * @param message
	 *            the message to set
	 * @see Text#setMessage(String)
	 */
	public void setMessage(final String message)
	{
		messageValue.set(message);
	}
	
	private class MessageChangeListener implements IPropertyAttributeListener<String>
	{
		@Override
		public void attributeChanged(PropertyAttributeChangedEvent<String> event)
		{
			if(TextControl.this.text.isEnabled())
			{
				TextControl.this.text.setMessage(event.getNewAttribute());
			}
			else
			{
				TextControl.this.text.setText(event.getNewAttribute());
			}
		}
	}

	
}
