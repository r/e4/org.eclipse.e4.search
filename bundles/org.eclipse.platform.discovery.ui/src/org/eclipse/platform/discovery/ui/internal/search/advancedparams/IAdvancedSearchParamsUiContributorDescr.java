/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.search.advancedparams;

import org.eclipse.platform.discovery.ui.api.IAdvancedSearchParamsUiContributor;

/**
 * Description of advanced search UI contributors
 * @author Danail Branekov
 *
 */
public interface IAdvancedSearchParamsUiContributorDescr
{
	/**
	 * The ID of the contributor
	 */
	public String getId();
	
	/**
	 * The search provider id
	 */
	public String getSearchProviderId();
	
	/**
	 * Creates the {@link IAdvancedSearchParamsUiContributor} instance
	 */
	public IAdvancedSearchParamsUiContributor createContributor();
}
