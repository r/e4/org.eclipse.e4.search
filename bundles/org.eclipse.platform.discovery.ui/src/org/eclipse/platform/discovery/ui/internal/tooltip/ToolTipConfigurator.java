/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.tooltip;

import org.eclipse.platform.discovery.ui.api.IFormTextBuilder;
import org.eclipse.ui.forms.widgets.FormText;



/**
 * Abstract implementation of the {@link IToolTipConfigurator} interface which provides means for automatic conversion from a regular string to a string which obeys the {@link FormText} requirements. The implementation will replace the newline characters of the input string with &ltp&gt elements
 * 
 * @author Danail Branekov
 * 
 */
public abstract class ToolTipConfigurator extends AbstractTooltipConfigurator implements IToolTipConfigurator
{
	@Override
	protected void createContent(final IFormTextBuilder tooltipBuilder)
	{
		final String[] lines = textContent().split("\n"); //$NON-NLS-1$
		for (String l : lines)
		{
			tooltipBuilder.startParagraph(true);
			tooltipBuilder.appendText(l);
			tooltipBuilder.endParagraph();
		}
	}

	/**
	 * Retrieves the text to be displayed by the tooltip
	 */
	protected abstract String textContent();
}
