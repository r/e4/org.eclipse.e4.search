/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.api;

/**
 * UI context for views. Contains UI related data
 * 
 * @author Danail Branekov
 */
public interface IViewUiContext
{
	/**
	 * Returns the horizontal position of the second column of controls in the generic search parameters UI. Users can use this value in order to align their labels in order to achieve better integration of their custom search UI
	 */
	public int getSecondColumnPosition();

	/**
	 * Sets the value returned by the {@link #getSecondColumnPosition()}
	 */
	public void setSecondColumnPosition(final int position);
	
	/**
	 * Returns the spacing used to separate UI controls in the search console form layout. Users can use this value in order to implement better UI integration of custom UI
	 */
	public int controlsSpacing();
}
