/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.impl;


import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.platform.discovery.core.api.ISearchFavoritesMasterController;
import org.eclipse.platform.discovery.ui.internal.view.dnd.DropEventAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.DropTargetListener;
import org.eclipse.swt.dnd.Transfer;


/**
 * Search favorites drop listener<br>
 * Supports {@link LocalSelectionTransfer}<br>
 * Will accept a selection in case
 * <ul>
 * <li>it is instance of {@link IStructuredSelection}</li>
 * <li>for each of its elements there is a search favorites customization which provides favorite items 
 * </ul>
 * 
 * @author Danail Branekov
 * 
 */
public class SearchFavoritesDropListener extends FavoritesDropInteractionListener implements DropTargetListener
{
	public SearchFavoritesDropListener(Transfer[] transfers, ISearchFavoritesMasterController controller)
	{
		super(transfers, controller);
	}

	@Override
	public void dragEnter(DropTargetEvent event)
	{
		this.start(new DropEventAdapter(event));
	}

	@Override
	public void dragLeave(DropTargetEvent event)
	{
	}

	@Override
	public void dragOperationChanged(DropTargetEvent event)
	{
	}

	@Override
	public void dragOver(DropTargetEvent event)
	{
	}

	@Override
	public void drop(DropTargetEvent event)
	{
		this.process(new DropEventAdapter(event));
	}

	@Override
	public void dropAccept(DropTargetEvent event)
	{
	}
}

