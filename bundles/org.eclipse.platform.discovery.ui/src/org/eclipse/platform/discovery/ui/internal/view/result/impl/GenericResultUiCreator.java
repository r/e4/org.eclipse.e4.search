/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.result.impl;

import java.util.Set;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.core.api.ISearchContext;
import org.eclipse.platform.discovery.ui.api.IResultsViewAccessor;
import org.eclipse.platform.discovery.ui.api.ISearchConsoleCustomization;
import org.eclipse.platform.discovery.ui.api.impl.LabeledSelection;
import org.eclipse.platform.discovery.ui.api.impl.SearchResultCustomUiCreator;
import org.eclipse.platform.discovery.ui.internal.plugin.DiscoveryUIMessages;
import org.eclipse.platform.discovery.ui.internal.view.dnd.impl.DragSrcInteractionListener;
import org.eclipse.platform.discovery.ui.internal.view.dnd.impl.LocalContextSelectionTransferSetter;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.widgets.FormToolkit;


/**
 * The UI for generically showing the search result
 * 
 * @author Danail Branekov
 */
public class GenericResultUiCreator extends SearchResultCustomUiCreator 
{
	private IDiscoveryEnvironment env;
	private IResultsViewAccessor va;
	
	@Override
	public void restore(Object data) {
		if(!va.getTreeViewer().getTree().isDisposed())
			va.getTreeViewer().setExpandedTreePaths((TreePath[])data);
	}

	@Override
	public Object restoreData() {
		if(va.getTreeViewer().getTree().isDisposed())
			return null;
		return va.getTreeViewer().getExpandedTreePaths();
	}

	public GenericResultUiCreator(final String searchProviderId, IDiscoveryEnvironment env) {
		super();
		this.env = env;
	}

	public Composite createSearchUi(final Composite parent,
			final ISearchContext searchContext, final FormToolkit formToolkit,
			final Set<IContributedAction> actions,
			final Set<ISearchConsoleCustomization> viewCustomizations) {
		DiscoveryTreeViewerFactory f = newDiscoveryTreeViewerFactory();
		parent.setLayout(new GridLayout(1, true));
		if (searchContext.description() != null)
		{
			final Label l = formToolkit.createLabel(parent, searchContext.description());
			l.setLayoutData(new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false));
		}
		va = f.createTreeViewer(parent, viewCustomizations, actions, env );
		va.getTreeViewer().getControl().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		va.getTreeViewer().setInput(searchContext);
		va.getTreeViewer().refresh();
		
		if(va.getTreeViewer().getTree().getItems().length == 0)
		{
			va.getTreeViewer().getTree().dispose();
			formToolkit.createLabel(parent, DiscoveryUIMessages.NoResultsUiCreator_NO_RESULTS_FOUND);
			parent.layout();
		}

		for (ISearchConsoleCustomization viewCustomization : viewCustomizations) {
			viewCustomization.postResultDisplayed(va);
		}
		return parent;
	}
	
	private DiscoveryTreeViewerFactory newDiscoveryTreeViewerFactory()
	{
		return new DiscoveryTreeViewerFactory(){
			@Override
			protected DragSrcInteractionListener.ITransferDataSetter<? extends Transfer>[] supportedTransferDataSetters(IDiscoveryEnvironment env)
			{
				final DragSrcInteractionListener.ITransferDataSetter<? extends Transfer>[] superTypes = super.supportedTransferDataSetters(env);
				@SuppressWarnings("unchecked")
				final DragSrcInteractionListener.ITransferDataSetter<? extends Transfer>[] supportedTypes = new DragSrcInteractionListener.ITransferDataSetter[superTypes.length + 1];
				System.arraycopy(superTypes, 0, supportedTypes, 0, superTypes.length);
				supportedTypes[supportedTypes.length - 1] = new LocalContextSelectionTransferSetter();
				return supportedTypes;
			}
		};
	}

	public void registerResultSelectionChangedListener(
			final ISelectionChangedListener selChangedListener) {
		new LabeledSelectionChangedEventAdapter(va.getTreeViewer(), selChangedListener).registerForSelectionEvent();
	}
	
	private static class LabeledSelectionChangedEventAdapter implements ISelectionChangedListener
	{
		private  final ISelectionChangedListener toAdapt;
		private final StructuredViewer v;
		
		public LabeledSelectionChangedEventAdapter(StructuredViewer v, ISelectionChangedListener toAdapt)
		{
			this.v = v;
			this.toAdapt = toAdapt;
		}
		
		public void registerForSelectionEvent()
		{
			v.addSelectionChangedListener(this);
		}
		
		@Override
		public void selectionChanged(SelectionChangedEvent event) {
			final SelectionChangedEvent delegatedEvent = new SelectionChangedEvent(
					event.getSelectionProvider(),
					new LabeledSelection((ILabelProvider)v.getLabelProvider(),
							(IStructuredSelection) event
									.getSelection()));
			toAdapt.selectionChanged(delegatedEvent);
		}
	}
}
