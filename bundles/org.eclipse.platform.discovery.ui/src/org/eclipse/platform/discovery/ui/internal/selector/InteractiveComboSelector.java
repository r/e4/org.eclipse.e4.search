/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.selector;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.platform.discovery.runtime.api.IDisplayableObject;
import org.eclipse.platform.discovery.util.internal.property.Property;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.forms.widgets.FormToolkit;


/**
 * A combo selector which disables itself when the input is set to empty set.<br>
 * When the input is set to a nonempty collection and this collection contains the last known selected item, then this item will be selected by default<br>
 * As the UI it creates uses {@link FormData} layout data, this selector should be used in composites which have {@link FormLayout} layouts
 * 
 * @author Danail Branekov
 * 
 */
public class InteractiveComboSelector<T extends IDisplayableObject> extends ComboSelector<T>
{
	private final Property<T> lastKnownSelectedItem;

	/**
	 * Constructor
	 * 
	 * @param parent
	 *            the parent composite. DO NOTE that it will be set to invisible in case empty input is specified
	 * @see ComboSelector#ComboSelector(Composite, FormToolkit, Set, String, int)
	 */
	public InteractiveComboSelector(final Composite parent, final FormToolkit formToolkit, final List<T> input, final String comboLabel, final int secondColumnPosition, final Control upperNeighbour)
	{
		super(parent, formToolkit, input, comboLabel, secondColumnPosition, upperNeighbour);
		this.lastKnownSelectedItem = new Property<T>();
		getComboViewer().addSelectionChangedListener(new ISelectionChangedListener()
		{
			@SuppressWarnings("unchecked")
			@Override
			public void selectionChanged(SelectionChangedEvent event)
			{
				lastKnownSelectedItem.set((T) ((IStructuredSelection) event.getSelection()).getFirstElement());
			}
		});
	}

	@Override
	public void setInput(Collection<T> input)
	{
		super.setInput(input);
		this.setEnabled(input.size() > 0);
		if (input.size() > 0)
		{
			setSelectedItem(itemToSelect());
		}
	}

	@SuppressWarnings("unchecked")
	private T itemToSelect()
	{
		int index = 0;
		while (getComboViewer().getElementAt(index) != null)
		{
			if (getComboViewer().getElementAt(index).equals(lastKnownSelectedItem.get()))
			{
				return lastKnownSelectedItem.get();
			}
			index++;
		}

		return (T) getComboViewer().getElementAt(0);
	}

	private void setSelectedItem(final T item)
	{
		getComboViewer().setSelection(new StructuredSelection(item), true);
		lastKnownSelectedItem.set(item);
	}
}
