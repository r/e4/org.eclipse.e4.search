/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.result.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.platform.discovery.ui.api.IGenericViewCustomization;


/**
 * The generic search result UI content provider. 
 * 
 * @author Danail Branekov
 */
public class GenericResultContentProvider implements ITreeContentProvider
{
	private List<ITreeContentProvider> contentProviders;
	private final List<IGenericViewCustomization> customizations;

	/**
	 * Constructor
	 * @param viewCustomizations a set of view customizations; empty set if none 
	 */
	public GenericResultContentProvider(final List<IGenericViewCustomization> viewCustomizations)
	{
		contentProviders = new ArrayList<ITreeContentProvider>();
		customizations = Collections.unmodifiableList(viewCustomizations);
		
		for (IGenericViewCustomization cust : customizations)
		{
			final ITreeContentProvider cp = cust.getContentProvider();
			if (cp != null)
			{
				contentProviders.add(cp);
			}
		}
	}
	
	protected List<IGenericViewCustomization> availableCustomizations()
	{
		return customizations;
	}

	public Object[] getElements(Object inputElement)
	{
		final Collection<Object> result = new ArrayList<Object>();
		for (ITreeContentProvider cp : contentProviders)
		{
			final Object[] objects = cp.getElements(inputElement);
			if (objects == null)
			{
				continue;
			}
			result.addAll(Arrays.asList(objects));
		}

		return result.toArray();
	}

	public void dispose()
	{
		for (ITreeContentProvider cp : contentProviders)
		{
			cp.dispose();
		}
	}

	public void inputChanged(final Viewer viewer, final Object oldInput, final Object newInput)
	{
		for (ITreeContentProvider cp : contentProviders)
		{
			cp.inputChanged(viewer, oldInput, newInput);
		}
	}

	public Object[] getChildren(final Object parentElement)
	{
		final Collection<Object> result = new ArrayList<Object>();
		for (ITreeContentProvider cp : contentProviders)
		{
			final Object[] objects = cp.getChildren(parentElement);
			if (objects == null)
			{
				continue;
			}
			result.addAll(Arrays.asList(objects));
		}

		return result.toArray();
	}

	public Object getParent(Object element)
	{
		// Parent cannot be calculated
		return null;
	}

	public boolean hasChildren(Object element)
	{
		for (ITreeContentProvider cp : contentProviders)
		{
			if(cp.hasChildren(element))
			{
				return true;
			}
		}
		
		return false;
	}
}
