/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.platform.discovery.ui.api.ISearchFavoritesViewCustomization;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;


public abstract class SearchFavoritesItemsAdder implements ISearchFavoritesItemsAdder
{
	private final List<ISearchFavoritesViewCustomization> viewCustomizations;
	private final ILongOperationRunner opRunner;

	public SearchFavoritesItemsAdder(final List<ISearchFavoritesViewCustomization> viewCustomizations, final ILongOperationRunner opRunner)
	{
		this.viewCustomizations = viewCustomizations;
		this.opRunner = opRunner;
	}

	@Override
	public void addItems(final ISelection selectedItems)
	{
		if (!canAddSelection(selectedItems))
		{
			throw new IllegalArgumentException("Selection is not acceptable"); //$NON-NLS-1$
		}

		final IStructuredSelection structuredSelection = (IStructuredSelection) selectedItems;
		final Iterator<?> iterator = structuredSelection.iterator();
		final Set<Object> objectsToAdd = new HashSet<Object>();
		while (iterator.hasNext())
		{
			objectsToAdd.addAll(itemsForSource(iterator.next()));
		}

		doAddItems(objectsToAdd, this.opRunner);
	}

	/**
	 * Adds the item to the persistence store
	 */
	protected abstract void doAddItems(final Set<Object> items, final ILongOperationRunner operationRunner);

	@Override
	public boolean canAddSelection(final ISelection selection)
	{
		if (!(selection instanceof IStructuredSelection))
		{
			return false;
		}

		final Iterator<?> iterator = ((IStructuredSelection) selection).iterator();
		while (iterator.hasNext())
		{
			if (!isFavoritesItemSupported(iterator.next()))
			{
				return false;
			}
		}

		return true;
	}

	private boolean isFavoritesItemSupported(final Object itemsSource)
	{
		return !itemsForSource(itemsSource).isEmpty();
	}

	private Collection<Object> itemsForSource(final Object itemsSource)
	{
		final Set<Object> result = new HashSet<Object>();
		for (ISearchFavoritesViewCustomization cust : viewCustomizations)
		{
			result.addAll(cust.itemsFor(itemsSource));
		}

		return result;
	}

}
