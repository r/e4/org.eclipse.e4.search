/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.tooltip;

import org.eclipse.platform.discovery.ui.api.IFormTextBuilder;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.forms.widgets.FormText;


/**
 * Abstract implementor of the {@link IToolTipConfigurator} interface which provides utility for converting string to {@link FormText} format<br>
 * The API this class provides to its extenders mimics SAX xml processing. Extenders can use its "append", "start", "end" to create the content to be displayed
 * 
 * @author Danail Branekov
 * 
 */
public abstract class AbstractTooltipConfigurator implements IToolTipConfigurator
{
	public interface TooltipFormTextBuilder extends IFormTextBuilder, ITextGetter
	{
	}
	
	protected TooltipFormTextBuilder createNewTooltipTextBuilder()
	{
		return new FormTextBuilder();
	}
	
	@Override
	public final String getFormText()
	{
		final TooltipFormTextBuilder tooltipContentBuilder = createNewTooltipTextBuilder();
		createContent(tooltipContentBuilder);
		return tooltipContentBuilder.getText();
	}

	/**
	 * Extender should implement this method to create the tooltip content using the {@link IFormTextBuilder} interface
	 * 
	 */
	protected abstract void createContent(final IFormTextBuilder tooltipBuilder);
	
	@Override
	public String getTitleCaption()
	{
		return null;
	}

	@Override
	public Image getTitleImage()
	{
		return null;
	}
}
