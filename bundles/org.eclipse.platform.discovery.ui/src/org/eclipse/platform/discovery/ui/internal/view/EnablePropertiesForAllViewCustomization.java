/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view;

import java.util.Collections;
import java.util.Set;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.ui.api.IResultsViewAccessor;
import org.eclipse.platform.discovery.ui.api.ISearchFavoritesViewCustomization;
import org.eclipse.platform.discovery.ui.api.ISearchConsoleCustomization;
import org.eclipse.platform.discovery.ui.api.impl.GenericViewCustomizationImpl;
import org.eclipse.platform.discovery.ui.internal.view.impl.OpenPropsViewAction;


public class EnablePropertiesForAllViewCustomization extends GenericViewCustomizationImpl implements ISearchConsoleCustomization, ISearchFavoritesViewCustomization  {
	
	@Override
	public void installAction(IContributedAction contributedAction, IResultsViewAccessor viewAccessor) {
		if(!getSelection(viewAccessor).isEmpty() && viewAccessor.getMenuManager().find(OpenPropsViewAction.ID) == null) {
			viewAccessor.getMenuManager().add(new OpenPropsViewAction());
		}
	}
	
	@Override
	public boolean acceptSearchProvider(String searchProviderId) {
		//properties context menu view customization is relevant for everybody
		return true;
	}
	
	@Override
	public Object itemGroup(Object item) {
		return null;
	}
	@Override
	public Set<Object> itemsFor(Object itemsSource) {
		return Collections.emptySet();
	}

	protected ISelection getSelection(IResultsViewAccessor accessor) {
		return accessor.getTreeViewer().getSelection();
	}
	

}
