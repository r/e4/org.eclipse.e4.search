/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.platform.discovery.runtime.api.GroupingHierarchy;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.ISearchSubdestination;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.ISearchProviderDescription;
import org.eclipse.platform.discovery.ui.api.ISearchParametersUI;
import org.eclipse.platform.discovery.ui.internal.plugin.DiscoveryUIMessages;
import org.eclipse.platform.discovery.ui.internal.plugin.DiscoveryUIPlugin;
import org.eclipse.platform.discovery.ui.internal.selector.InteractiveComboSelector;
import org.eclipse.platform.discovery.ui.internal.view.SearchConsoleView;
import org.eclipse.platform.discovery.ui.internal.view.impl.SubdestinationsSelectedListener;
import org.eclipse.platform.discovery.ui.internal.view.impl.TextControl;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

public class SearchParametersUI implements ISearchParametersUI {

	private final static String DROP_DOWN_ICON_LOCATION = "icon-dropdown-keyword-ALT.gif"; //$NON-NLS-1$

	private static final int UI_IN_CONTROL_SPACING = 5;
	private Label openSubdestinationsButton;
	private TextControl keywordTextControl;

	private IConsoleContext consoleContext;

	private InteractiveComboSelector<GroupingHierarchy> groupingHierarchySelector;

	@Override
	public void createUI(Composite parent, FormToolkit formToolkit, final IConsoleContext consoleContext) {
		parent.setLayout(new FormLayout());
		
		this.consoleContext = consoleContext;
		
		final Label searchLabel = new Label(parent, SWT.NONE);
		searchLabel.setText(DiscoveryUIMessages.SEARCH_KEYWORD_LABEL);
		final FormData labelFormData = new FormData();
		labelFormData.top = new FormAttachment(0, UI_IN_CONTROL_SPACING);
		labelFormData.left = new FormAttachment(0, UI_IN_CONTROL_SPACING);
		searchLabel.setLayoutData(labelFormData);

		openSubdestinationsButton = createSubdestinationsSelector(parent);
		keywordTextControl = createKeywordTextControl(parent, formToolkit, searchLabel, consoleContext.secondColumnLeftOffset(), openSubdestinationsButton);
		createGroupByComposite(parent, formToolkit, consoleContext.secondColumnLeftOffset(), keywordTextControl.getControl());
	}
	
	private TextControl createKeywordTextControl(final Composite parent, final FormToolkit formToolkit, final Label leftNeighbour, final int leftOffset, final Control rightNeighbour)
	{
		final Text keywordText = formToolkit.createText(parent, "", SWT.BORDER | SWT.SEARCH); //$NON-NLS-1$
		final FormData keywordTextFormData = new FormData();
		// TODO: Think about layouting controls in separate columns
		keywordTextFormData.left = new FormAttachment(0, leftOffset + 2*SearchConsoleView.UI_IN_CONTROL_SPACING);
		keywordTextFormData.right = new FormAttachment(rightNeighbour, -UI_IN_CONTROL_SPACING);
		keywordText.setLayoutData(keywordTextFormData);
		
		final TextControl control = new TextControl(keywordText, leftNeighbour);
		control.setEnabled(false);
		control.setMessage(DiscoveryUIMessages.SearchConsoleView_NoObjectTypeSelectedHint);
		control.getControl().addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				SearchParametersUI.this.consoleContext.notifyComplete(isComplete());
			}
		});
		
		return control;
	}

	private Image dropDownButtonImage(final Display display)
	{
		Image img = imageRegistry().get(DROP_DOWN_ICON_LOCATION);
		if (img == null)
		{
			img = new Image(display, SearchConsoleView.class.getResourceAsStream(DROP_DOWN_ICON_LOCATION));
			imageRegistry().put(DROP_DOWN_ICON_LOCATION, img);
		}
		return img;
	}
	
	private ImageRegistry imageRegistry()
	{
		return DiscoveryUIPlugin.getDefault().getImageRegistry();
	}
	
	private boolean isComplete()
	{
		return (consoleContext.searchProvider() != null) && !keywordTextControl.get().isEmpty();
	}

	@Override
	public Map<Object, Object> getParameters() {
		return Collections.emptyMap();
	}

	@Override
	public String getKeyword() {
		return this.keywordTextControl.get();
	}

	@Override
	public GroupingHierarchy getGroupingHierarchy() {
		return this.groupingHierarchySelector.getSelectedItem();
	}
	

	private Label createSubdestinationsSelector(final Composite parent) {
		final Label selector = new Label(parent, SWT.NONE);
		final FormData subDestButtonFormData = new FormData();
		//subDestButtonFormData.left = new FormAttachment(100, -25);
		subDestButtonFormData.right = new FormAttachment(100, -UI_IN_CONTROL_SPACING);

		selector.setLayoutData(subDestButtonFormData);
		selector.setImage(dropDownButtonImage(parent.getDisplay()));
		selector.setEnabled(false);
		
		selector.addListener(SWT.MouseDown, new SubdestinationsSelectedListener(selector, consoleContext));

		return selector;
	}
	
	private void createGroupByComposite(final Composite parent, final FormToolkit formToolkit, final int leftLabelsSize, final Control upperNeighbour)
	{
		groupingHierarchySelector = new InteractiveComboSelector<GroupingHierarchy>(parent, formToolkit, new ArrayList<GroupingHierarchy>(), DiscoveryUIMessages.GROUP_BY_LABEL, leftLabelsSize , upperNeighbour);
		groupingHierarchySelector.setEnabled(false);
	}

	@Override
	public void showGroupingHierarchies(final List<GroupingHierarchy> groupingHierarchies) {
		groupingHierarchySelector.setInput(groupingHierarchies);
	}

	@Override
	public void searchProviderSelected(ISearchProviderDescription provider) {
		
		
		final ISearchDestination destination = consoleContext.searchDestination();
		keywordTextControl.setEnabled((provider == null ? false : provider.supportsTextSearch()));
		updateKeywordTextMessage(provider, destination);

		if (destination == null || provider == null)
		{
			openSubdestinationsButton.setEnabled(false);
		} else
		{
			final List<ISearchSubdestination> allSubdestintations = consoleContext.searchProviderConfiguration().getAvailableSearchSubdestinations(consoleContext.selectedObjectType(), consoleContext.destinationCategory(), consoleContext.searchProvider());
			openSubdestinationsButton.setEnabled(allSubdestintations.size() > 0);
		}

		consoleContext.notifyComplete(isComplete());				
	}
	
	private void updateKeywordTextMessage(ISearchProviderDescription provider, final ISearchDestination destination)
	{
		if(provider==null) 
		{
			if(consoleContext.selectedObjectType()==null)
			{
				keywordTextControl.setMessage(DiscoveryUIMessages.SearchConsoleView_NoObjectTypeSelectedHint);
				return;
			}
			
			assert destination==null;
			keywordTextControl.setMessage(DiscoveryUIMessages.SearchConsoleView_NoDestinationSelectedHint);
			return;
		}
		
		if(provider.supportsTextSearch())
		{
			keywordTextControl.setMessage(DiscoveryUIMessages.SearchConsoleView_EnterKeywordHint);
			return;
		}
		else
		{
			keywordTextControl.setMessage(DiscoveryUIMessages.SearchConsoleView_UnsupportedTextSearch);
		}
		
		
	}
	
}
