/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.favorites;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.platform.discovery.ui.api.IGenericViewCustomization;
import org.eclipse.platform.discovery.ui.api.ISearchFavoritesViewCustomization;
import org.eclipse.platform.discovery.ui.internal.view.result.impl.GenericResultContentProvider;
import org.eclipse.platform.discovery.util.internal.logging.ILogger;
import org.eclipse.platform.discovery.util.internal.logging.Logger;


/**
 * Content provider for the search favorites view<br>
 * The content provider expects to receive a set of objects as input. Its {@link #getElements(Object)} method will return the elements of this set
 * 
 * @author Danail Branekov
 */
public class FavoritesContentProvider extends GenericResultContentProvider
{
	private final Map<Object, List<Object>> groupsMap;

	public FavoritesContentProvider(List<IGenericViewCustomization> viewCustomizations)
	{
		super(viewCustomizations);
		
		groupsMap = new HashMap<Object, List<Object>>();
	}

	@Override
	public Object[] getElements(Object inputElement)
	{
		return groupsMap.keySet().toArray();
	}
	
	@Override
	public Object[] getChildren(final Object parentElement)
	{
		if(groupsMap.containsKey(parentElement))
		{
			return groupsMap.get(parentElement).toArray();
		}
		
		return super.getChildren(parentElement);
	}

	@Override
	public void inputChanged(final Viewer viewer, final Object oldInput, final Object newInput)
	{
		groupsMap.clear();
		if(newInput == null)
		{
			return;
		}
		
		final Set<Object> newInputSet = new HashSet<Object>();
		newInputSet.addAll((Set<?>) newInput);
		
		for(Object obj : newInputSet)
		{
			installItemToGroup(obj);
		}
		// super.inputChanged should not be invoked as FavoritesContentProvider is responsible for retrieving the input's immediate children.
		// See ITreeContentProvider#getElements() and ITreeContentProvider#getChildren() javadoc for details
	}

	private void installItemToGroup(final Object item)
	{
		final Object group = itemGroup(item);
		if(group == null)
		{
			return;
		}
		
		List<Object> groupedItems = groupsMap.get(group);
		if(groupedItems == null)
		{
			groupedItems = new ArrayList<Object>();
			groupsMap.put(group, groupedItems);
		}
		groupedItems.add(item);
	}
	
	private Object itemGroup(final Object item)
	{
		for(IGenericViewCustomization cust : availableCustomizations())
		{
			final Object group = ((ISearchFavoritesViewCustomization)cust).itemGroup(item);
			if(group != null)
			{
				return group;
			}
		}
		
		logger().logWarn("Item does not belong to any group"); //$NON-NLS-1$
		return null;
	}
	
	@Override
	public boolean hasChildren(final Object element)
	{
		if(groupsMap.containsKey(element))
		{
			return true;
		}
		return super.hasChildren(element);
	}
	
	protected ILogger logger()
	{
		return Logger.instance();
	}
}
