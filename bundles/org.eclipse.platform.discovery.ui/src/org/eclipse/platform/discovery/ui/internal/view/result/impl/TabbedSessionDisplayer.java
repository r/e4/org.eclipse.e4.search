/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.result.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.platform.discovery.ui.internal.plugin.DiscoveryUIMessages;
import org.eclipse.platform.discovery.ui.internal.plugin.DiscoveryUIPlugin;
import org.eclipse.platform.discovery.ui.internal.view.impl.ITabbedSessionDisplayer;
import org.eclipse.platform.discovery.util.internal.property.IPropertyAttributeListener;
import org.eclipse.platform.discovery.util.internal.property.Property;
import org.eclipse.platform.discovery.util.internal.property.PropertyAttributeChangedEvent;
import org.eclipse.platform.discovery.util.internal.session.ISession;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.FormToolkit;


public class TabbedSessionDisplayer<T> implements ITabbedSessionDisplayer<T> {
	
	private static final String FW_IMG_E = "/icons/e/forward_nav.gif"; //$NON-NLS-1$
	private static final String FW_IMG_D = "/icons/d/forward_nav.gif"; //$NON-NLS-1$
	private static final String BW_IMG_E = "/icons/e/backward_nav.gif"; //$NON-NLS-1$
	private static final String BW_IMG_D = "/icons/d/backward_nav.gif"; //$NON-NLS-1$
	
	private static final Image fwImgE;
	private static final Image fwImgD;
	private static final Image bwImgE;
	private static final Image bwImgD;
	
	static {
		fwImgE = new Image(getDisplayForCreateImage(), TabbedSessionDisplayer.class.getResourceAsStream(FW_IMG_E));
		DiscoveryUIPlugin.getDefault().getImageRegistry().put(TabbedSessionDisplayer.class.getCanonicalName() + FW_IMG_E, fwImgE);
		fwImgD = new Image(getDisplayForCreateImage(), TabbedSessionDisplayer.class.getResourceAsStream(FW_IMG_D));
		DiscoveryUIPlugin.getDefault().getImageRegistry().put(TabbedSessionDisplayer.class.getCanonicalName() + FW_IMG_D, fwImgD);
		bwImgE = new Image(getDisplayForCreateImage(), TabbedSessionDisplayer.class.getResourceAsStream(BW_IMG_E));
		DiscoveryUIPlugin.getDefault().getImageRegistry().put(TabbedSessionDisplayer.class.getCanonicalName() + BW_IMG_E, bwImgE);
		bwImgD = new Image(getDisplayForCreateImage(), TabbedSessionDisplayer.class.getResourceAsStream(BW_IMG_D));
		DiscoveryUIPlugin.getDefault().getImageRegistry().put(TabbedSessionDisplayer.class.getCanonicalName() + BW_IMG_D, bwImgD);
	}

	private static Display getDisplayForCreateImage() {
		return PlatformUI.getWorkbench().getDisplay();
	}
	
	private final Set<String> unclosableSessions;
	
	public interface UIFactory<T> {
		UI create(T sc, Composite parent);
		FormToolkit getFormTookit();
	}
	
	public interface UI {
		void createControls();
		void saveControlsState();
		void restoreControlsState();
		Composite parent();
		void dispose();
		String title();
	}

	
	private final CTabFolder tabsFolder;
	private Map<String, SessionTab> displayers;
	private final UIFactory<T> uif;
	
	public TabbedSessionDisplayer(Composite parent, UIFactory<T> uif, int tabFolderStyle)
	{
		displayers = new HashMap<String, SessionTab>();
		tabsFolder = createSessionsTabFolder(parent, uif, tabFolderStyle | SWT.BORDER);
		tabsFolder.addSelectionListener(new SelectionAdapter(){
			@Override
			public void widgetSelected(SelectionEvent e)
			{
				final CTabItem selectedTabItem = (CTabItem)e.item;
				final SessionTab visibleSessionTab = sessionTabForTabItem(selectedTabItem);
				
				for(SessionTab st : displayers.values())
				{
					st.handleVisibilityChange(st.equals(visibleSessionTab));
				}
			}
		});
		
		this.uif = uif;
		this.unclosableSessions = new HashSet<String>();
	}

	public CTabFolder getControl()
	{
		return tabsFolder;
	}
	
	@Override
	public void dispose() {	
		tabsFolder.dispose();
	}
	
	private CTabFolder createSessionsTabFolder(Composite parent, UIFactory<T> uif, int style)
	{
		final CTabFolder t = new CTabFolder(parent, style);
		t.setSimple(false);
		t.setUnselectedCloseVisible(false);
		t.setUnselectedImageVisible(false);
		uif.getFormTookit().adapt(t);
		return t;
	}
	
	private SessionTab sessionTab(final ISession<T> session) {
		SessionTab sd = displayers.get(session.getId());
		if (sd != null)
		{
			return sd;
		} 
		displayers.put(session.getId(), sd = new SessionTab(session));
		sd.tabItem.addDisposeListener(new DisposeListener()
		{
			@Override
			public void widgetDisposed(DisposeEvent e) {
				displayers.remove(session.getId());
			}
		});
		return sd;
	}
	
	@Override
	public void display(ISession<T> session) {
		sessionTab(session).display();
	}
	
	private class SessionTab
	{
		final CTabItem tabItem;
		final private Composite tabComposite;
		final private ISession<T> session;
		private ToolBar toolbar;
		private ToolItem prevButton;
		private ToolItem nextButton;
		private UI currentSessionUI;
		private final Property<Boolean> tabVisibleProperty;
		
		public SessionTab(ISession<T> session) {
			this.session = session;
			tabComposite = uif.getFormTookit().createComposite(
					tabsFolder, SWT.NONE);
			tabItem = new CTabItem(tabsFolder, isUnclosableSession() ? SWT.NONE : SWT.CLOSE);
			tabItem.setControl(tabComposite);
			
			tabComposite.setLayout(new GridLayout(1, true));
			tabVisibleProperty = new Property<Boolean>();
			tabVisibleProperty.set(false);
			tabVisibleProperty.registerValueListener(new IPropertyAttributeListener<Boolean>(){
				@Override
				public void attributeChanged(final PropertyAttributeChangedEvent<Boolean> event)
				{
					if(!isNavigatableSession())
					{
						return;
					}
					
					if(event.getNewAttribute())
					{
						createToolbarWithNavigationButtons();
					}
					else if(isNavigatableSession())
					{
						// Unregister the toolbar as top right control
						tabsFolder.setTopRight(null, SWT.RIGHT);
						toolbar.dispose();
						tabsFolder.layout(true, true);
					}
				}}, false);
		}
		
		public void display() {
			tabsFolder.setSelection(tabItem);
			tabVisibleProperty.set(true);
			installContrubutedUI(uif);
			tabItem.setText(currentSessionUI.title());
			tabsFolder.layout(true, true);
		}

		private void updateNavButtons() {
			prevButton.setEnabled(session.historyTrack().hasPrevious());
			nextButton.setEnabled(session.historyTrack().hasNext());
		}

		private void installContrubutedUI(
				UIFactory<T> uif) {
			if (currentSessionUI != null)
			{
				currentSessionUI.saveControlsState();
				currentSessionUI.dispose();
			}
			final Composite contributedUiParentComposite = uif.getFormTookit().createComposite(tabComposite);
			contributedUiParentComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			currentSessionUI = uif.create(session.historyTrack().current(), contributedUiParentComposite);
			currentSessionUI.createControls();
		}

		private ToolItem createPrevButton()
		{
			final ToolItem pb = new ToolItem(toolbar, SWT.PUSH, 0);
			pb.setToolTipText(DiscoveryUIMessages.AbstractSearchResultTab_GoToPrevious);
			pb.setImage(bwImgE);
			pb.setDisabledImage(bwImgD);
			pb.addSelectionListener(new SelectionAdapter()
			{
				@Override
				public void widgetSelected(SelectionEvent e) {
					session.historyTrack().previous();
					display();
					currentSessionUI.restoreControlsState();
					updateNavButtons();
					
				}
			});
			return pb;
		}
		
		private ToolItem createNextButton()
		{
			final ToolItem nb = new ToolItem(toolbar, SWT.PUSH, 1);
			nb.setToolTipText(DiscoveryUIMessages.AbstractSearchResultTab_GoToNext);
			nb.setImage(fwImgE);
			nb.setDisabledImage(fwImgD);
			nb.addSelectionListener(new SelectionAdapter()
			{
				@Override
				public void widgetSelected(SelectionEvent e) {
					session.historyTrack().next();
					display();
					currentSessionUI.restoreControlsState();
					updateNavButtons();
				}
			});
			return nb;
		}
		
		private ToolBar createToolBar() {
			final ToolBar tb = new ToolBar(tabsFolder, SWT.FLAT | SWT.WRAP | SWT.HORIZONTAL | SWT.RIGHT);
			uif.getFormTookit().adapt(tb);
			tb.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false));
			tabsFolder.setTabHeight(Math.max(tb.computeSize(SWT.DEFAULT, SWT.DEFAULT).y, tabsFolder.getTabHeight()));
			tabsFolder.setTopRight(tb, SWT.RIGHT);
			tabsFolder.redraw();
			
			return tb;
		}
		
		private boolean isNavigatableSession()
		{
			return this.session.historyTrack().historyLimit() > 1;
		}
		
		private boolean isUnclosableSession()
		{
			return unclosableSessions().contains(this.session.getId());
		}
		
		/**
		 * Retrieves the tab item which is bound to this session tab
		 * @return
		 */
		public CTabItem tabItem()
		{
			return this.tabItem;
		}
		
		/**
		 * Handles session tab visibility change
		 * @param visible true if the session tab became visible, false otherwise
		 */
		public void handleVisibilityChange(final boolean visible)
		{
			tabVisibleProperty.set(visible);
		}
		
		private void createToolbarWithNavigationButtons()
		{
			toolbar = createToolBar();
			prevButton = createPrevButton();
			nextButton = createNextButton();
			updateNavButtons();
			
			// We need to adjust the toolbar position manually since the toolbar size initially is too small and remains invisible
			// The code below would adjust the toobar bounds in order to fit it into the parent CTabFolder
			final Point parentSize = toolbar.getParent().getSize();
			final Point tbSize = toolbar.computeSize(SWT.DEFAULT, SWT.DEFAULT);
			final Rectangle tbBounds = toolbar.getBounds();
			toolbar.setBounds(parentSize.x - tbSize.x, tbBounds.y, tbSize.x, tbBounds.height);
		}
	}
	
	/**
	 * Sets the session with the ID specified unclosable (e.g. without close button on the tab). This methods has to be invoked prior {@link #display(ISession)} 
	 * @param sessionId
	 */
	@Override
	public void setSessionUnclosable(String sessionId)
	{
		unclosableSessions().add(sessionId);
	}
	
	private Set<String> unclosableSessions()
	{
		return this.unclosableSessions;
	}
	
	private SessionTab sessionTabForTabItem(final CTabItem tabItem)
	{
		for(SessionTab sessionTab : displayers.values())
		{
			if(sessionTab.tabItem().equals(tabItem))
			{
				return sessionTab;
			}
		}
		
		throw new IllegalStateException("Unknown tab item"); //$NON-NLS-1$
	}
}
