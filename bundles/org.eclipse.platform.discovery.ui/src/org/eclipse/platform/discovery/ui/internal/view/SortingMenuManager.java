/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view;

import java.util.Arrays;
import java.util.Comparator;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.MenuManager;

public class SortingMenuManager extends MenuManager 
{
	private ContributionItemComparator contributionItemComparator;
	
	@Override
	public void add(IContributionItem item) {
		super.add(item);
		final IContributionItem[] currentItems = getItems();
		this.removeAll();
		Arrays.sort(currentItems, getComparator());
		for(IContributionItem i : currentItems)
		{
			super.add(i);
		}
	}

	private class ContributionItemComparator implements Comparator<IContributionItem> {
		@Override
		public int compare(IContributionItem o1, IContributionItem o2) 
		{
			if(o1 == null || o1.getId() == null)
			{
				return -1;
			}
			if(o2 == null || o2.getId() == null)
			{
				return 1;
			}
			return o1.getId().compareTo(o2.getId());
		}
	}
	
	private ContributionItemComparator getComparator()
	{
		if(contributionItemComparator == null)
		{
			contributionItemComparator = new ContributionItemComparator();
		}
		return contributionItemComparator;
	}
	
	@Override
	public void appendToGroup(String groupName, IAction action) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void appendToGroup(String groupName, IContributionItem item) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void prependToGroup(String groupName, IAction action) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void prependToGroup(String groupName, IContributionItem item) {
		throw new UnsupportedOperationException();
	}
}
