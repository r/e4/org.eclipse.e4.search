/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.impl;

import org.eclipse.jface.action.Action;
import org.eclipse.platform.discovery.ui.internal.plugin.DiscoveryUIMessages;
import org.eclipse.platform.discovery.util.internal.logging.Logger;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;


public class OpenPropsViewAction extends Action {
	private static final String ORG_ECLIPSE_UI_VIEWS_PROPERTY_SHEET = "org.eclipse.ui.views.PropertySheet"; //$NON-NLS-1$
	public static final String ID = "z.org.eclipse.platform.discovery.ui.internal.view.openpropsview"; //$NON-NLS-1$

	
	public OpenPropsViewAction() {
		setText(DiscoveryUIMessages.Properties_MenuItem);
	}

	@Override
	public String getId() {
		return ID;
	}
	
	@Override
	public void run() {
		final IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		try{
			window.getActivePage().showView(ORG_ECLIPSE_UI_VIEWS_PROPERTY_SHEET);
		}catch(PartInitException pie)  {//...mmm, pie
			Logger.instance().logError("Failed to initialize properties view.", pie);//$NON-NLS-1$
			return;
		}
	}
	

}
