/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.api;

import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.core.api.ISearchContext;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;


/**
 * Generic view customization interface
 * @author Danail Branekov
 */
public interface IGenericViewCustomization
{
	/**
	 * A content provider to be contributed to the view. The input of this content provider will be set with an instance of {@link ISearchContext} by the framework
	 * 
	 * @return content provider instance or <code>null</code>
	 */
	public ITreeContentProvider getContentProvider();

	/**
	 * A label provider to be contributed to the view
	 * 
	 * @return label provider instance or <code>null</code>
	 */
	public ILabelProvider getLabelProvider();

	/**
	 * A tooltip provider to be contributed to the view
	 * 
	 * @return tooltip provider instance or <code>null</code>
	 */
	public ITooltipProvider getTooltipProvider();

	/**
	 * Sets the master view to the customization. This method will be invoked by the framework right after the customization instantiation thus letting it communicate with the master view immediately
	 * 
	 * @param masterView
	 *            the master view; never null
	 */
	public void setMasterView(final IMasterDiscoveryView masterView);

	/**
	 * Installs the action specified in the results view. Implementations can use the <code>viewAccessor</code> in order to get access the the UI components and install the action specified wherever they want to.<br>
	 * Implementations may also check the ID of the action. If they do not know what to do with this action they may simply ignore it
	 * 
	 * @param contributedAction
	 *            the action to be installed
	 * @param viewAccessor
	 *            view accessor
	 */
	public void installAction(final IContributedAction contributedAction, final IResultsViewAccessor viewAccessor);
	
	/**
	 * Return double click listener to be contributed to the tree viewer
	 * @return a {@link IDoubleClickListener} instance to be contributed to the tree viewer; null if none
	 * @see IResultsViewAccessor#getTreeViewer()
	 */
	public IDoubleClickListener getDoubleClickListener();

	/**
	 * This method will be invoked after result view is populated. Implementators may use it as a hook to 
	 * change result representation
	 * */
	public void postResultDisplayed(final IResultsViewAccessor viewAccessor);
	
	/**
	 * Customizations are given the chance to react upon changes in the selection<br>
	 * This method is to be invoked from the UI thread and therefore it is recommended to execute the implementation outside the UI thread (e.g. using ILongOperationRunner)
	 * @param selection the new selection
	 * @see #setMasterView(IMasterDiscoveryView)
	 * @see IMasterDiscoveryView#getEnvironment()
	 * @see IDiscoveryEnvironment#operationRunner()
	 */
	public void selectionChanged(final ISelection selection);
}
