/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.platform.discovery.runtime.api.IDisplayableObject;


/**
 * Content provider. It provides the {@link IDisplayableObject} instances specified as elements. In addition, the elements are sorted
 * 
 * @author Danail Branekov
 * 
 */

public class DisplayableObjectsContentProvider<T extends IDisplayableObject> implements IStructuredContentProvider
{
	private List<T> curentInput;

	/**
	 * Constructor
	 * 
	 * @param input
	 *            initial set of {@link IDisplayableObject} which will be provided as elements
	 */
	public DisplayableObjectsContentProvider(final List<T> input)
	{
		this.curentInput = input;
	}

	public Object[] getElements(Object inputElement)
	{
		return sortElements(curentInput);
	}

	public void dispose()
	{
	}

	@SuppressWarnings("unchecked")
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
	{
		curentInput = (List<T>) newInput;
	}

	protected Object[] sortElements(final List<T> elements)
	{
		final List<T> objectsList = new ArrayList<T>(elements);
		Collections.sort(objectsList, new Comparator<T>()
		{
			public int compare(T o1, T o2)
			{
				return o1.getDisplayName().compareTo(o2.getDisplayName());
			}
		});

		return objectsList.toArray();
	}
}
