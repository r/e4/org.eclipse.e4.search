/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal;

import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.ui.forms.widgets.FormToolkit;

public class HorizontalStatePainter extends SlidingCompositeStatePainter{

	public HorizontalStatePainter(final FormToolkit formToolkit) {
		super(formToolkit);
	}

	@Override
	public int[] getArrowDownPolygon(Rectangle clientArea) {
		
		final int topAngleX = clientArea.width / 2 ;
		final int topAngleY = clientArea.y + (clientArea.height/2 - TRIANGLE_HEIGHT) ;
		final int botomY = topAngleY + TRIANGLE_HEIGHT;
		final int [] arrowDown = { topAngleX - IDENTATION, topAngleY, topAngleX -  IDENTATION, topAngleY + TRIANGLE_SIDE,	topAngleX, botomY };
		return arrowDown;
	}

	@Override
	public int[] getArrowUpPolygon(Rectangle clientArea) {
		final int topAngleX = clientArea.width / 2 ;
        final int topAngleY = clientArea.y + clientArea.height/2 ;
        final int leftAngleX= topAngleX - TRIANGLE_SIDE/2;
        final int botomY= topAngleY + TRIANGLE_HEIGHT;
        final int [] arrowUp ={topAngleX , topAngleY, leftAngleX + TRIANGLE_SIDE, botomY - TRIANGLE_SIDE, leftAngleX + TRIANGLE_SIDE, botomY};
        return arrowUp;
		
	}

	@Override
	public void printSliderButtonDown(PaintEvent e, Rectangle clientArea) {
		final int X_COORD = getHorizontalBeginPosition(clientArea);
		final int Y_COORD = getVerticalBeginPosition(clientArea);
		final int HEIGHT = getVerticalSize(clientArea);
		final int WIDTH = getHorizontalSize(clientArea);

		final int HALF_HEIGHT = HEIGHT / 2;
		final int HALF_WIDTH = WIDTH / 2;

		// change BG color draw border rectangles
		e.gc.setForeground(getSliderBorderColor());
		e.gc.drawRoundRectangle(X_COORD, Y_COORD, HALF_WIDTH, HEIGHT, ARC_MEDIUM, ARC_MEDIUM);
		e.gc.drawRoundRectangle(X_COORD, HALF_HEIGHT - TOP_RECTANGLE_WIDTH / 2, WIDTH - IDENTATION, TOP_RECTANGLE_WIDTH ,
					ARC_BIG, ARC_BIG);
			// change BG color and fill rectangles
		e.gc.setBackground(getSliderFillColor());
		e.gc.fillRoundRectangle(X_COORD + ONE_PIXEL, Y_COORD + 2*ONE_PIXEL, 
					HALF_WIDTH - (ONE_PIXEL + WHITE_LINE_WIDTH), HEIGHT - 2*ONE_PIXEL - WHITE_LINE_WIDTH,
					ARC_MEDIUM,	ARC_MEDIUM);

		e.gc.fillRectangle(X_COORD + WHITE_LINE_WIDTH, Y_COORD + ONE_PIXEL + WHITE_LINE_WIDTH, 
					HALF_WIDTH - 2*(ONE_PIXEL +WHITE_LINE_WIDTH), HEIGHT - 2 *(ONE_PIXEL + WHITE_LINE_WIDTH));
		
		e.gc.fillRoundRectangle(X_COORD + WHITE_LINE_WIDTH, 
					HALF_HEIGHT - TOP_RECTANGLE_WIDTH / 2 + ONE_PIXEL + WHITE_LINE_WIDTH, 
					HALF_WIDTH , 
					TOP_RECTANGLE_WIDTH - 2* WHITE_LINE_WIDTH - ONE_PIXEL, ARC_SMALL, ARC_SMALL);

		e.gc.setForeground(getUnderBorderColor());
		e.gc.setLineWidth(WHITE_LINE_WIDTH);
			
		e.gc.drawLine(HALF_WIDTH - ONE_PIXEL , Y_COORD + WHITE_LINE_WIDTH, 
						  HALF_WIDTH - ONE_PIXEL, HALF_HEIGHT - TOP_RECTANGLE_WIDTH / 2 + ONE_PIXEL);

		e.gc.drawLine(HALF_WIDTH - ONE_PIXEL , HALF_HEIGHT + TOP_RECTANGLE_WIDTH/2 , 
						HALF_WIDTH - ONE_PIXEL , HEIGHT - 2* ONE_PIXEL);

		e.gc.drawLine(HALF_WIDTH + WHITE_LINE_WIDTH, HALF_HEIGHT - TOP_RECTANGLE_WIDTH / 2 + WHITE_LINE_WIDTH, 
						HALF_WIDTH , HALF_HEIGHT - TOP_RECTANGLE_WIDTH / 2 + WHITE_LINE_WIDTH);
			
		e.gc.drawLine(HALF_WIDTH + WHITE_LINE_WIDTH, HALF_HEIGHT + TOP_RECTANGLE_WIDTH/2 - ONE_PIXEL, 	
					HALF_WIDTH , HALF_HEIGHT + TOP_RECTANGLE_WIDTH/2 - ONE_PIXEL);
			
		e.gc.setForeground(getSliderBorderColor());
		e.gc.setLineWidth(ONE_PIXEL);
		e.gc.setBackground(getTriangleColor());
		e.gc.drawLine(X_COORD + ONE_PIXEL, Y_COORD, X_COORD + ONE_PIXEL, HEIGHT);
		e.gc.fillPolygon(getArrowDownPolygon(clientArea));
	}

	@Override
	public void printSliderButtonUp(PaintEvent e, Rectangle clientArea) {
		final int X_COORD = getHorizontalBeginPosition(clientArea);
		final int Y_COORD = getVerticalBeginPosition(clientArea);
		final int HEIGHT = getVerticalSize(clientArea);
		final int WIDTH = getHorizontalSize(clientArea);

		final int HALF_HEIGHT = HEIGHT / 2;
		final int HALF_WIDTH = WIDTH / 2;

		// change BG color draw border rectangles
		e.gc.setForeground(getSliderBorderColor());
		
			e.gc.drawRoundRectangle(HALF_WIDTH, Y_COORD, HALF_WIDTH , HEIGHT - ONE_PIXEL, ARC_MEDIUM, ARC_MEDIUM);		
			e.gc.drawRoundRectangle(X_COORD + IDENTATION, HALF_HEIGHT - TOP_RECTANGLE_WIDTH / 2, WIDTH - IDENTATION, TOP_RECTANGLE_WIDTH ,
					ARC_BIG, ARC_BIG);
			e.gc.setBackground(getSliderFillColor());
			
			e.gc.fillRoundRectangle(HALF_WIDTH + ONE_PIXEL + WHITE_LINE_WIDTH, 	Y_COORD /*+ ONE_PIXEL*/ + WHITE_LINE_WIDTH, 
					WIDTH - 2* ONE_PIXEL - 2 * WHITE_LINE_WIDTH, HEIGHT - 2 * ONE_PIXEL	- WHITE_LINE_WIDTH, ARC_MEDIUM, ARC_MEDIUM);
			e.gc.fillRoundRectangle(X_COORD + IDENTATION+ ONE_PIXEL + WHITE_LINE_WIDTH, HALF_HEIGHT - TOP_RECTANGLE_WIDTH / 2 + ONE_PIXEL + WHITE_LINE_WIDTH, 
					WIDTH - IDENTATION - 2* WHITE_LINE_WIDTH - ONE_PIXEL, TOP_RECTANGLE_WIDTH - 2* WHITE_LINE_WIDTH - ONE_PIXEL, ARC_SMALL, ARC_SMALL);

			e.gc.setForeground(getUnderBorderColor());
			e.gc.setLineWidth(WHITE_LINE_WIDTH);
			e.gc.drawLine(HALF_WIDTH + 2*ONE_PIXEL , Y_COORD + WHITE_LINE_WIDTH, 
						  HALF_WIDTH + 2*ONE_PIXEL , HALF_HEIGHT - TOP_RECTANGLE_WIDTH / 2 + ONE_PIXEL);

			e.gc.drawLine(HALF_WIDTH + 2*ONE_PIXEL , HALF_HEIGHT + TOP_RECTANGLE_WIDTH/2 , 
						  HALF_WIDTH + 2*ONE_PIXEL , HEIGHT - 2* ONE_PIXEL);

			e.gc.drawLine(HALF_WIDTH + WHITE_LINE_WIDTH, HALF_HEIGHT - TOP_RECTANGLE_WIDTH / 2 + WHITE_LINE_WIDTH, 
						HALF_WIDTH , HALF_HEIGHT - TOP_RECTANGLE_WIDTH / 2 + WHITE_LINE_WIDTH);
			
			e.gc.drawLine(HALF_WIDTH + WHITE_LINE_WIDTH, HALF_HEIGHT + TOP_RECTANGLE_WIDTH/2 - ONE_PIXEL, 	
					HALF_WIDTH , HALF_HEIGHT + TOP_RECTANGLE_WIDTH/2 - ONE_PIXEL);
			
			e.gc.setForeground(getSliderBorderColor());
			e.gc.setLineWidth(ONE_PIXEL);
			e.gc.setBackground(getTriangleColor());
			e.gc.drawLine(WIDTH-ONE_PIXEL, Y_COORD , WIDTH- ONE_PIXEL, HEIGHT);
			e.gc.fillPolygon(getArrowUpPolygon(clientArea));
		}

	@Override
	public void arrangeControls(FormData hideButtonFormData, FormData sashFormData, 
			FormData upperScrolledCompositeFormData, FormData bottomCompositeFormData,
			int targetPosition , int buttonHeight) {
		
		upperScrolledCompositeFormData.right = new FormAttachment(0, targetPosition - buttonHeight);
		hideButtonFormData.left = upperScrolledCompositeFormData.right;
		sashFormData.left = hideButtonFormData.right;
		sashFormData.width = SASH_HEIGHT;
		sashFormData.right = new FormAttachment(0, targetPosition + SASH_HEIGHT );
		bottomCompositeFormData.left = sashFormData.right;
		
	}

	@Override
	public void arrangeControlsOnUpperVisibilityOff( FormData hideButtonFormData, FormData bottomCompositeFormData,
			int buttonHeight) {
		hideButtonFormData.left = new FormAttachment(0, 0);
		hideButtonFormData.right = new FormAttachment(0, buttonHeight);
		bottomCompositeFormData.left = hideButtonFormData.right;
		
	}

	@Override
	public void adjustControlsLayoutData(FormData upperScrolledCompositeData, FormData hideButtonData, FormData sashData,	
			FormData bottomCompositeData, Canvas hideButton, int buttonHeight) {
		upperScrolledCompositeData.right = new FormAttachment(hideButton);
		sashData.width = SASH_HEIGHT;
		hideButtonData.width  = buttonHeight;
		bottomCompositeData.right = new FormAttachment(100, 0);
		
	}
}
