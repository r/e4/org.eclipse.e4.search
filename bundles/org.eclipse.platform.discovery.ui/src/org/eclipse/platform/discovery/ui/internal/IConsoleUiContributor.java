/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal;

import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;

/**
 * Interface for contributing UI components to the search console
 * 
 * @author Danail Branekov
 * 
 */
interface IConsoleUiContributor
{
	/**
	 * Creates the contributed UI composite. Do note that the layout of the <code>parent</code> composite is {@link FormLayout}<br>
	 * 
	 * @param parent
	 *            the parent composite
	 * @return the newly created contributed composite
	 */
	public Composite createContributedComposite(final Composite parent);
}
