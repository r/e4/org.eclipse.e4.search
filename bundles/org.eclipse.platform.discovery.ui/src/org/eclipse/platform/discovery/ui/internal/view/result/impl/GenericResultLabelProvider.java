/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.result.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.platform.discovery.ui.api.IGenericViewCustomization;
import org.eclipse.platform.discovery.ui.api.ITooltipProvider;
import org.eclipse.platform.discovery.ui.internal.tooltip.FormTextBuilder;
import org.eclipse.swt.graphics.Image;


public class GenericResultLabelProvider extends CellLabelProvider implements ILabelProvider
{
	private final List<ILabelProvider> labelProviders;
	private final List<ITooltipProvider> tooltipProviders;

	public GenericResultLabelProvider(final List<IGenericViewCustomization> viewCustomizations)
	{
		labelProviders = new ArrayList<ILabelProvider>();
		tooltipProviders = new ArrayList<ITooltipProvider>();

		for (IGenericViewCustomization customization : viewCustomizations)
		{
			if (customization.getLabelProvider() != null)
			{
				labelProviders.add(customization.getLabelProvider());
			}

			if (customization.getTooltipProvider() != null)
			{
				tooltipProviders.add(customization.getTooltipProvider());
			}
		}
	}
	
	public Image getImage(Object element)
	{
		for (ILabelProvider lProvider : labelProviders)
		{
			final Image img = lProvider.getImage(element);
			if (img != null)
			{
				return img;
			}
		}

		return null;
	}

	public String getText(Object element)
	{
		for (ILabelProvider lProvider : labelProviders)
		{
			final String text = lProvider.getText(element);
			if (text != null)
			{
				return text;
			}
		}

		return null;
	}

	public void addListener(ILabelProviderListener listener)
	{
	}

	public void dispose()
	{
	}

	public boolean isLabelProperty(Object element, String property)
	{
		return false;
	}

	public void removeListener(ILabelProviderListener listener)
	{
	}

	@Override
	public void update(ViewerCell cell)
	{
		cell.setText(getText(cell.getElement()));
		cell.setImage(getImage(cell.getElement()));

	}

	@Override
	public String getToolTipText(final Object element)
	{
		final FormTextBuilder textB = new FormTextBuilder();
		for (ITooltipProvider ttProvider : tooltipProviders)
		{
			ttProvider.createTooltipContent(textB, element);
		}

		return textB.getText();
	}
}
