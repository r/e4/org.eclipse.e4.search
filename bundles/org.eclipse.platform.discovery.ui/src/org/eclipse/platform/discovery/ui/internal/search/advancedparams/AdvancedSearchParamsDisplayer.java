/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.search.advancedparams;

import org.eclipse.core.runtime.Platform;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.ISearchParameters;
import org.eclipse.platform.discovery.runtime.internal.DestinationCategoryNotFoundException;
import org.eclipse.platform.discovery.runtime.internal.ISearchProviderConfiguration;
import org.eclipse.platform.discovery.runtime.internal.ProviderNotFoundException;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.ISearchProviderDescription;
import org.eclipse.platform.discovery.ui.api.IAdvancedSearchParamsUiContributor;
import org.eclipse.platform.discovery.ui.api.IViewUiContext;
import org.eclipse.platform.discovery.ui.internal.plugin.DiscoveryUIMessages;
import org.eclipse.platform.discovery.ui.internal.xp.IAdvancedSearchParamsUiContribXpParser;
import org.eclipse.platform.discovery.ui.internal.xp.impl.AdvancedSearchParamsUiContribXpParser;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;
import org.eclipse.platform.discovery.util.internal.property.IPropertyAttributeListener;
import org.eclipse.platform.discovery.util.internal.property.Property;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.forms.IFormColors;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;


/**
 * Implementation of the {@link IAdvancedSearchParamsDisplayer} interface
 * 
 * @author Danail Branekov
 * 
 */
public abstract class AdvancedSearchParamsDisplayer implements IAdvancedSearchParamsDisplayer
{
	private final FormToolkit formToolkit;
	private final IAdvancedSearchParamsUiContribXpParser searchParamsContribParser;
	private Section _paramsSection;
	private final Property<Point> sectionSizeProperty;
	private final Composite parentComposite;
	private final Property<IAdvancedSearchParamsUiContributor> currentUiContributor;
	private final Property<ISearchDestination> currentSearchDestination;
	private final Property<IDiscoveryEnvironment> currentEnvironment;
	private final Property<IViewUiContext> uiContext;

	/**
	 * Constructor
	 * 
	 * @param parentComposite
	 *            the parent composite. This composite will be set to invisible in case the displayer has nothing to display. The parent composite has {@link FormLayout}
	 * @param toolkit
	 * @see Composite#layout(boolean)
	 */
	public AdvancedSearchParamsDisplayer(final Composite parentComposite, final FormToolkit toolkit)
	{
		this.formToolkit = toolkit;
		this.parentComposite = parentComposite;
		sectionSizeProperty = new Property<Point>();
		sectionSizeProperty.set(new Point(0, 0));
		searchParamsContribParser = createAdvancedSearchUiContribXpParser();
		
		currentUiContributor = new Property<IAdvancedSearchParamsUiContributor>();
		currentSearchDestination = new Property<ISearchDestination>();
		currentEnvironment = new Property<IDiscoveryEnvironment>();
		uiContext = new Property<IViewUiContext>();
	}
	
	public IAdvancedSearchParamsUiContributor getUiContributor()
	{
		return currentUiContributor.get();
	}

	@Override
	public void update(final IObjectTypeDescription objectType, final ISearchDestination searchDestination, final IDiscoveryEnvironment environment, final IViewUiContext uiContext)
	{
		currentUiContributor.set(findUiContributor(objectType, searchDestination));
		currentSearchDestination.set(searchDestination);
		currentEnvironment.set(environment);
		this.uiContext.set(uiContext);

		setEnabled(currentUiContributor.get() != null);
	}
	
	private void updateSectionContent()
	{
		assert currentUiContributor.get() != null;
		
		final Control oldClient = parametersSection().getClient();
		parametersSection().setClient(createParametersComposite(parametersSection(), currentUiContributor.get(), currentSearchDestination.get(), currentEnvironment.get(), uiContext.get()));
		if(oldClient != null)
		{
			oldClient.dispose();
		}
		
		updateParametersSectionText();
		parentComposite.getParent().layout();
		updateSectionSizeProperty(sectionSize(parametersSection()));
	}
	
	private void updateParametersSectionText()
	{
		if(parametersSection().isExpanded())
		{
			parametersSection().setText(DiscoveryUIMessages.CUSTOM_PARAMS_SECTION_COLAPSE_TITLE); 
		}
		else
		{
			parametersSection().setText(DiscoveryUIMessages.CUSTOM_PARAMS_SECTION_EXPAND_TITLE);			
		}
	}
	
	private Composite createParametersComposite(final Composite parent, final IAdvancedSearchParamsUiContributor uiContributor, final ISearchDestination destination, final IDiscoveryEnvironment environment, final IViewUiContext uiContext)
	{
		final Composite parametersComposite = createParametersContainingComposite(parent);
		parametersComposite.setLayoutData(fillingFormData());
		uiContributor.createUi(parametersComposite, destination, formToolkit, environment, uiContext);
		
		return parametersComposite;

	}
	
	protected Composite createParametersContainingComposite(final Composite parent)
	{
		return createCompositeWithFormLayout(parent);
	}
	
	private Section createParametersSection(final Composite parent)
	{
		final Section section = formToolkit.createSection(parent, Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		section.setActiveToggleColor(formToolkit.getHyperlinkGroup().getActiveForeground());
		section.setToggleColor(formToolkit.getColors().getColor(IFormColors.SEPARATOR));
		section.setText(DiscoveryUIMessages.CUSTOM_PARAMS_SECTION_EXPAND_TITLE);
		section.setLayoutData(fillingFormData());
		section.setExpanded(false);
		section.addExpansionListener(new ExpansionAdapter(){
			@Override
			public void expansionStateChanged(ExpansionEvent e)
			{
				updateParametersSectionText();
				section.layout(true);
				updateSectionSizeProperty(sectionSize(section));
				getUiContributor().handleVisibilityChange(section.isExpanded());
			}
		});
		
		return section;
	}
	
	private void updateSectionSizeProperty(final Point size)
	{
		sectionSizeProperty.set(size);
	}
	
	protected IAdvancedSearchParamsUiContribXpParser createAdvancedSearchUiContribXpParser()
	{
		return new AdvancedSearchParamsUiContribXpParser(Platform.getExtensionRegistry());
	}

	protected abstract ISearchProviderConfiguration searchProviderConfiguration();

	private Composite createCompositeWithFormLayout(final Composite parent)
	{
		final Composite c = new Composite(parent, SWT.NONE);
		c.setLayout(new FormLayout());
		return c;
	}

	private IAdvancedSearchParamsUiContributor findUiContributor(final IObjectTypeDescription objectType, final ISearchDestination destination)
	{
		if (destination == null || objectType == null)
		{
			return null;
		}

		try
		{
			final IDestinationCategoryDescription category = searchProviderConfiguration().getDestinationCategoriesForDestination(destination).iterator().next();
			final ISearchProviderDescription searchProvider = searchProviderConfiguration().getActiveSearchProvider(objectType, category);
			for (IAdvancedSearchParamsUiContributorDescr contributorDescr : searchParamsContribParser.readContributions())
			{
				if (contributorDescr.getSearchProviderId().equals(searchProvider.getId()))
				{
					return contributorDescr.createContributor();
				}
			}
		} catch (DestinationCategoryNotFoundException e)
		{
			throw new IllegalStateException(e);
		} catch (ProviderNotFoundException e)
		{
			throw new IllegalStateException(e);
		}

		return null;
	}

	private FormData fillingFormData()
	{
		final FormData fd = new FormData();
		fd.left = new FormAttachment(0, 0);
		fd.right = new FormAttachment(100, 0);
		
		return fd;
	}
	
	public void setEnabled(final boolean enabled)
	{
		if(enabled)
		{
			updateSectionContent();
		}
		else
		{
			disposeSection();
		}
	}
	
	@Override
	public boolean isEnabled()
	{
		return _isSectionShowing();
	}
	
	@Override
	public void setParams(final ISearchParameters searchParams)
	{
		if(getUiContributor() == null)
		{
			return;
		}
		
		
		searchParams.getCustomParameters().putAll(getUiContributor().getParameters());
	}

	@Override
	public void registerSizePropertyChangeListener(final IPropertyAttributeListener<Point> listener, boolean notifyCurrent)
	{
		sectionSizeProperty.registerValueListener(listener, notifyCurrent);
	}
	
	/**
	 * Getter for {@link #_paramsSection} field. If the field is not initialized or the section is disposed, a new section will be created
	 * @return
	 */
	private Section parametersSection()
	{
		if(!_isSectionShowing())
		{
			_paramsSection = createParametersSection(this.parentComposite);
		}
		
		return _paramsSection;
	}
	
	private boolean _isSectionShowing()
	{
		return (_paramsSection != null) && (!_paramsSection.isDisposed());
	}
	
	private void disposeSection()
	{
		if(_isSectionShowing())
		{
			_paramsSection.dispose();
		}
		
		updateSectionSizeProperty(new Point(0, 0));
	}
	
	private Point sectionSize(final Section section)
	{
		return section.computeSize(SWT.DEFAULT, SWT.DEFAULT);
	}
	
}
