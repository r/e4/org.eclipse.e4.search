/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.impl;

import org.eclipse.platform.discovery.ui.internal.view.result.impl.ISessionsDisplayer;
import org.eclipse.platform.discovery.util.internal.session.ISession;


/**
 * Session displayer which shows a result in tabs
 * @author Danail Branekov
 */
public interface ITabbedSessionDisplayer<T> extends ISessionsDisplayer<T>
{
	/**
	 * Sets the session with the ID specified unclosable (e.g. without close button on the tab). This methods has to be invoked prior {@link #display(ISession)} 
	 * @param sessionId
	 */
	public void setSessionUnclosable(final String sessionId);
}
