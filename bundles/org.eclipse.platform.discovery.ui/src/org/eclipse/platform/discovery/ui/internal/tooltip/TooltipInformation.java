/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.tooltip;

import org.eclipse.jface.text.AbstractInformationControl;
import org.eclipse.jface.text.IInformationControl;
import org.eclipse.jface.text.IInformationControlExtension;
import org.eclipse.jface.text.IInformationControlExtension2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormText;
import org.eclipse.ui.forms.widgets.FormToolkit;


public class TooltipInformation extends AbstractInformationControl implements IInformationControl, IInformationControlExtension, IInformationControlExtension2
{
	private final FormToolkit toolkit;
	private Form tooltipForm;

	public TooltipInformation(final Shell parent)
	{
		super(parent, false);
		toolkit = createFormToolkit();
		parent.addDisposeListener(new DisposeListener()
		{
			@Override
			public void widgetDisposed(DisposeEvent e)
			{
				dispose();
			}
		});

		// Call create() as documented in AbstractInformationControl javadoc
		create();
	}

	@Override
	public void setInput(Object input)
	{
		disposeChildren(tooltipForm.getBody());

		if (input == null)
		{
			return;
		}

		assert input instanceof IToolTipConfigurator;
		final IToolTipConfigurator configurator = (IToolTipConfigurator) input;
		tooltipForm.setImage(configurator.getTitleImage());
		tooltipForm.setText(configurator.getTitleCaption());
		final FormText text = createMessageTextField(tooltipForm.getBody(), configurator);
		text.setText(configurator.getFormText(), true, true);
		tooltipForm.layout(true);
		tooltipForm.pack();
		getShell().pack();
	}
	
	@Override
	public Point computeSizeHint()
	{
		return getShell().computeSize(SWT.DEFAULT, SWT.DEFAULT, true);
	}

	private void disposeChildren(final Composite composite)
	{
		for (Control c : composite.getChildren())
		{
			c.dispose();
		}
	}

	@Override
	protected void createContent(Composite parent)
	{
		tooltipForm = createForm(parent);
	}

	protected FormText createMessageTextField(final Composite parent, final IToolTipConfigurator configurator)
	{
		final FormText text = toolkit.createFormText(parent, true);
		final GridData td = new GridData();
		text.setLayoutData(td);

		text.setBackground(tooltipBackgroundColor(parent.getDisplay()));
		return text;
	}

	protected Form createForm(Composite parent)
	{
		final Form form = getFormToolkit().createForm(parent);
		form.setBackground(tooltipBackgroundColor(parent.getDisplay()));
		final GridLayout layout = new GridLayout(1, true);
		form.getBody().setLayout(layout);
		return form;
	}

	protected FormToolkit createFormToolkit()
	{
		return new FormToolkit(PlatformUI.getWorkbench().getDisplay());
	}

	protected FormToolkit getFormToolkit()
	{
		return toolkit;
	}

	private Color tooltipBackgroundColor(final Display display)
	{
		return display.getSystemColor(SWT.COLOR_INFO_BACKGROUND);
	}

	@Override
	public boolean hasContents()
	{
		return this.tooltipForm.getBody().getChildren().length > 0;
	}
	
	public void dispose()
	{
		if(toolkit.getColors()!=null)
			toolkit.dispose();
		super.dispose();
	}	
}
