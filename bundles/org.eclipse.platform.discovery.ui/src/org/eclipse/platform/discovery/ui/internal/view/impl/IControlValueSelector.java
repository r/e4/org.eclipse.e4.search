/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.impl;

import org.eclipse.platform.discovery.core.internal.ISingleValueSelector;
import org.eclipse.platform.discovery.ui.internal.view.IGetControlObject;
import org.eclipse.swt.widgets.Control;


/**
 * Interface which brings the value selector {@link ISingleValueSelector} interface and the {@link IGetControlObject} interface together
 * @author Danail Branekov
 *
 * @param <T> the type of values to select from
 * @param <K> the UI control type which interracts with the user to select the value 
 */
public interface IControlValueSelector<T, K extends Control> extends ISingleValueSelector<T>, IGetControlObject<K>
{

}
