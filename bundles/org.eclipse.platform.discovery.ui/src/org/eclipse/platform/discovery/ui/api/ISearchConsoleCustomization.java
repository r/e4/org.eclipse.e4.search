/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.api;


/**
 * Interface for contributing customizations to the search console view
 * 
 * @author Danail Branekov
 */
public interface ISearchConsoleCustomization extends IGenericViewCustomization
{
	/**
	 * Check whether this customization is relevant to a search provider
	 * 
	 * @param searchProviderId
	 *            the ID of the search provider
	 * @return true in case this customization is relevant for the search provider with the id specified; false otherwise
	 */
	public boolean acceptSearchProvider(final String searchProviderId);
}
