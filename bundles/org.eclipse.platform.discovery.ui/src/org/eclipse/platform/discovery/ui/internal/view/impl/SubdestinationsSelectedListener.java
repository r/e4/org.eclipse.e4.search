/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.platform.discovery.runtime.api.ISearchSubdestination;
import org.eclipse.platform.discovery.ui.api.ISearchParametersUI.IConsoleContext;
import org.eclipse.platform.discovery.ui.internal.selector.SubdestinationsSelector;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;


/**
 * Listener which is notified when subdestinations selection is trigerred
 * 
 * @author Danail Branekov
 * 
 */
public class SubdestinationsSelectedListener implements SelectionListener, Listener
{
	private final Control parentControl;

	private final IConsoleContext consoleContext;

	/**
	 * Constructor
	 * 
	 * @param providerConfiguration
	 *            search provider configuration
	 * @param parentControl
	 *            the control to which the selection ui will be bound to
	 */
	public SubdestinationsSelectedListener(final Control parentControl, final IConsoleContext consoleContext)
	{
		this.parentControl = parentControl;
		this.consoleContext = consoleContext;
	}

	public void widgetDefaultSelected(SelectionEvent e)
	{
		widgetSelected(e);
	}

	public void widgetSelected(SelectionEvent event)
	{
		final List<ISearchSubdestination> subDestinations = consoleContext.searchProviderConfiguration().getAvailableSearchSubdestinations(consoleContext.selectedObjectType(), consoleContext.destinationCategory(), consoleContext.searchProvider());
		final Point showPoint = new Point(event.x, event.y + parentControl.getSize().y);

		final SubdestinationsSelector selector = new SubdestinationsSelector(parentControl, SWT.NONE, subDestinations, showPoint)
		{
			public void handleSelection(ISearchSubdestination item, boolean selected)
			{
				consoleContext.searchProviderConfiguration().activateSubdestination(consoleContext.selectedObjectType(), consoleContext.destinationCategory(), consoleContext.searchProvider(),
							item, selected);
				
				if(selected){
					Set<ISearchSubdestination> inactive =  new HashSet<ISearchSubdestination>();
					for(ISearchSubdestination curr: consoleContext.searchProviderConfiguration().getAvailableSearchSubdestinations(consoleContext.selectedObjectType(), consoleContext.destinationCategory(), consoleContext.searchProvider())) {
						if(!isSubdestinationActive(curr)) {
							inactive.add(curr);
						}
					}
					
					Set<ISearchSubdestination> deactivated = new HashSet<ISearchSubdestination>();
					for(ISearchSubdestination curr: consoleContext.searchProviderConfiguration().getAvailableSearchSubdestinations(consoleContext.selectedObjectType(), consoleContext.destinationCategory(), consoleContext.searchProvider())) {
						if(!isSubdestinationActive(curr)) {
							if(!inactive.contains(curr)) {
								deactivated.add(curr);
							}
						}
					}
					
					for(ISearchSubdestination subd: deactivated) {
						consoleContext.notifySubdestinationActivationChange(subd, false);
					}
					
				}
				consoleContext.notifySubdestinationActivationChange(item, selected);
			}

			@Override
			protected boolean isSubdestinationActive(ISearchSubdestination subdestination)
			{
				return consoleContext.searchProviderConfiguration().isSubdestinationActive(subdestination, consoleContext.selectedObjectType(), consoleContext.destinationCategory(), consoleContext.searchProvider());
			}
		};
		try
		{
			selector.select();
		}
		finally
		{
			selector.dispose();
		}
	}
	
	@Override
	public void handleEvent(Event event) {
		final SelectionEvent selectionEvent = new SelectionEvent(event);
		widgetSelected(selectionEvent);
	}

}
