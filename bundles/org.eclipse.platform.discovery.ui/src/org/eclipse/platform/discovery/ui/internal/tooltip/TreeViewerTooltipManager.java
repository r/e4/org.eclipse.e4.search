/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.tooltip;

import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.TreeItem;

/**
 * Tooltip manager for displaying tooltip on tree items
 * @author Danail Branekov, Mladen Tomov
 */
public class TreeViewerTooltipManager extends TooltipManager<TreeItem>
{
	private final CellLabelProvider labelProvider;
	private final TreeViewer treeViewer;

	public TreeViewerTooltipManager(final TreeViewer treeViewer, final CellLabelProvider labelProvider, final IInformationControlCreator controlCreator)
	{
		super(controlCreator);
		this.labelProvider = labelProvider;
		this.treeViewer = treeViewer;
	}

	@Override
	protected IToolTipConfigurator createInformation(final TreeItem control)
	{
		final String tooltipText = labelProvider.getToolTipText(control.getData());
		if (isTooltipTextEmpty(tooltipText))
		{
			return null;
		}

		return newTooltipConfigurator(tooltipText, control.getText(), control.getImage());
	}

	@Override
	protected TreeItem hoveredWidget()
	{
		return treeViewer.getTree().getItem(getHoverEventLocation());
	}

	private IToolTipConfigurator newTooltipConfigurator(final String tooltipText, final String tooltipCaption, final Image titleImage)
	{
		return new IToolTipConfigurator()
		{

			@Override
			public Image getTitleImage()
			{
				return titleImage;
			}

			@Override
			public String getTitleCaption()
			{
				return tooltipCaption;
			}

			@Override
			public String getFormText()
			{
				return tooltipText;
			}
		};
	}

	private boolean isTooltipTextEmpty(final String tooltipText)
	{
		return tooltipText == null || tooltipText.isEmpty() || newFormTextBuilder().getText().equals(tooltipText);
	}

	protected FormTextBuilder newFormTextBuilder()
	{
		return new FormTextBuilder();
	}

	@Override
	protected Rectangle hoveredWidgetBounds(TreeItem hoveredWidget)
	{
		return hoveredWidget().getBounds();
	}
}
