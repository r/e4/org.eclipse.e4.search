/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.platform.discovery.runtime.api.GroupingHierarchy;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.ISearchParameters;
import org.eclipse.platform.discovery.runtime.api.ISearchSubdestination;


/**
 * Implementation of the {@link ISearchParameters} interface
 * @author Danail Branekov
 */
public class SearchParameters implements ISearchParameters
{
	private final Map<Object, Object> customParameters = new HashMap<Object, Object>();
	private final String keywordString;
	private final GroupingHierarchy groupingHierarchy;
	private final String objectTypeId;
	private final ISearchDestination searchDestination;
	private final Set<ISearchSubdestination> subdestinations;
	
	public SearchParameters(final String objectTypeId, final ISearchDestination searchDestination, final String keyword, final GroupingHierarchy groupingH, final Set<ISearchSubdestination> subdestinations)
	{
		this.objectTypeId = objectTypeId;
		this.searchDestination = searchDestination;
		this.keywordString = keyword;
		this.groupingHierarchy = groupingH;
		this.subdestinations = subdestinations;
	}
	
	public String getKeywordString()
	{
		return this.keywordString;
	}

	public GroupingHierarchy getGroupingHierarchy()
	{
		return this.groupingHierarchy;
	}

	public String getObjectTypeId()
	{
		return this.objectTypeId;
	}

	public ISearchDestination getSearchDestination()
	{
		return this.searchDestination;
	}

	public Set<ISearchSubdestination> getSubdestinations()
	{
		return this.subdestinations;
	}

	@Override
	public Map<Object, Object> getCustomParameters() {
		return customParameters;
	}
	
}
