/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.impl;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.platform.discovery.ui.api.IGenericViewCustomization;


public class SearchConsoleSelectionProvider implements ISelectionProvider
{
	private ISelection selection = new StructuredSelection();
	private final Set<ISelectionChangedListener> selectionListeners = new HashSet<ISelectionChangedListener>();
	private final IViewCustomizationsObtainer customizationsObtainer;
	
	/**
	 * Interface for obtaining the view customizations the selection provider should notify upon selection change 
	 * @author Danail Branekov
	 */
	public interface IViewCustomizationsObtainer
	{
		public List<IGenericViewCustomization> viewCustomizations();
	}
	
	public SearchConsoleSelectionProvider(final IViewCustomizationsObtainer customizationsObtainer)
	{
		this.customizationsObtainer = customizationsObtainer;
	}
	
	public void addSelectionChangedListener(final ISelectionChangedListener listener)
	{
		synchronized (this.selectionListeners)
		{
			selectionListeners.add(listener);
		}
	}

	public ISelection getSelection()
	{
		return selection;
	}
	
	public void removeSelectionChangedListener(final ISelectionChangedListener listener)
	{
		synchronized (this.selectionListeners)
		{
			selectionListeners.remove(listener);
		}
	}

	public void setSelection(final ISelection selection)
	{
		assert selection != null;
		
		this.selection = selection;
		
		for(IGenericViewCustomization customization : customizationsObtainer.viewCustomizations())
		{
			customization.selectionChanged(selection);
		}
		
		final Iterator<ISelectionChangedListener> i = cloneListeners().iterator();
		while (i.hasNext())
		{
			i.next().selectionChanged(new SelectionChangedEvent(this, this.getSelection()));
		}
	}
	
	private Set<ISelectionChangedListener> cloneListeners()
	{
		final Set<ISelectionChangedListener> clonings = new HashSet<ISelectionChangedListener>();
		synchronized (this.selectionListeners)
		{
			clonings.addAll(this.selectionListeners);
		}
		
		return clonings;
	}
}
