/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.core.internal.IDiscoveryView;
import org.eclipse.platform.discovery.ui.api.IGenericViewCustomization;
import org.eclipse.platform.discovery.ui.api.IMasterDiscoveryView;
import org.eclipse.platform.discovery.ui.api.IViewUiContext;
import org.eclipse.platform.discovery.ui.api.impl.ILabeledSelection;
import org.eclipse.platform.discovery.ui.internal.view.ICustomizableView;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.IFormColors;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.views.properties.IPropertySheetPage;
import org.eclipse.ui.views.properties.tabbed.ITabbedPropertySheetPageContributor;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;


public abstract class AbstractDiscoveryView<O, C> extends ViewPart implements IDiscoveryView<O, C>, ICustomizableView, ITabbedPropertySheetPageContributor, IMasterDiscoveryView
{
	private static final String TABBED_PROPERTY_CONTRIBUTOR_ID = "org.eclipse.platform.discovery.ui.view.tabbedprop.searchconsole"; //$NON-NLS-1$
	
	private final Set<IContributedAction> viewActions;
	private final Set<IGenericViewCustomization> viewCustomizations;
	protected FormToolkit formToolkit ;
	private final ISelectionProvider viewSelectionProvider;
	private IViewUiContext uiContext;

	private IDiscoveryEnvironment environment;
	
	public AbstractDiscoveryView()
	{
		super();
		viewActions = new HashSet<IContributedAction>();
		viewCustomizations = new HashSet<IGenericViewCustomization>();
		formToolkit = new FormToolkit(PlatformUI.getWorkbench().getDisplay());
		viewSelectionProvider = consoleViewSelectionProvider();
	}

	public String getContributorId()
	{
		return TABBED_PROPERTY_CONTRIBUTOR_ID;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public Object getAdapter(Class adapter)
	{
		if (adapter == IPropertySheetPage.class)
		{
			return new TabbedPropertySheetPage(this);
		}
		
		return super.getAdapter(adapter);
	}
	
	@Override
	public void createPartControl(final Composite parent)
	{
		this.getSite().setSelectionProvider(viewSelectionProvider);
	}
	
	protected ISelectionProvider consoleViewSelectionProvider()
	{
		return new SearchConsoleSelectionProvider(new SearchConsoleSelectionProvider.IViewCustomizationsObtainer()
		{
			@Override
			public List<IGenericViewCustomization> viewCustomizations()
			{
				return new ArrayList<IGenericViewCustomization>(AbstractDiscoveryView.this.viewCustomizations());
			}
		});
	}
	
	@Override
	public void registerAction(final IContributedAction action)
	{
		this.viewActions.add(action);
	}
	
	public Set<IContributedAction> viewActions()
	{
		return this.viewActions;  
	}
	
	public Set<IGenericViewCustomization> viewCustomizations()
	{
		return this.viewCustomizations;
	}

	@Override
	public void registerViewCustomization(final IGenericViewCustomization customization)
	{
		this.viewCustomizations.add(customization);
	}
	
	@Override
	public void initializationCompleted()
	{
		this.getProgressMonitor().done();
	}
	
	protected Color getGradientStart() {
		return formToolkit.getColors().getColor(IFormColors.H_GRADIENT_START);
	}

	protected Color getGradientEnd() {
		return formToolkit.getColors().getColor(IFormColors.H_GRADIENT_END);
	}
	
	public void setResultSelection(ILabeledSelection sel) {
		viewSelectionProvider.setSelection(sel);		
	}
	
	@Override
	public void dispose()
	{
		super.dispose();
		formToolkit.dispose();
	}
	
	@Override
	public void setUiContext(final IViewUiContext uiContext)
	{
		this.uiContext = uiContext;
	}
	
	protected IViewUiContext uiContext()
	{
		return this.uiContext;
	}
	
	@Override
	public void setEnvironment(final IDiscoveryEnvironment env)
	{
		this.environment = env;
	}

	@Override
	public IDiscoveryEnvironment getEnvironment() {
		return environment;
	}
}
