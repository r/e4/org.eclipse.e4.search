/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.api;

import java.util.Set;

/**
 * Interface for customizing the search favorites view
 * 
 * @author Danail Branekov
 */
public interface ISearchFavoritesViewCustomization extends IGenericViewCustomization
{
	/**
	 * Retrieves a set of items which will be added to the favorites view for the items source specified<br>
	 * The favorites view will display the items returned by this method.
	 * @param itemsSource the items "source" object 
	 * @return set of items or an empty set if none
	 */
	public Set<Object> itemsFor(final Object itemsSource);
	
	/**
	 * Retrieves the the group the item should belong to. The very item will be shown as a member of the group returned<br>
	 * Implementations must not return null because the favorites view will not display the item and would log a warning that the item does not belong to any group
	 * @param item the item
	 * @return the group of items the item specified belongs to
	 */
	public Object itemGroup(final Object item);
}
