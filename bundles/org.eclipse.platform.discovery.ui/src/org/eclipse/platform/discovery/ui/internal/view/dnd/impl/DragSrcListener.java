/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.dnd.impl;

import org.eclipse.platform.discovery.ui.internal.view.dnd.DragSourceEventAdapter;
import org.eclipse.platform.discovery.util.api.env.IErrorHandler;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.Transfer;


public class DragSrcListener extends DragSrcInteractionListener implements DragSourceListener {
	public DragSrcListener(DragSrcInteractionListener.ISelectionObtainer selectionObtainer,
			DragSrcInteractionListener.ITransferDataSetter<? extends Transfer>[] transfers, IErrorHandler errorHandler) {
		super(selectionObtainer, transfers, errorHandler);
	}

	@Override
	public void dragFinished(DragSourceEvent event) {
		this.finish(new DragSourceEventAdapter(event));
	}

	@Override
	public void dragSetData(DragSourceEvent event) {
		this.process(new DragSourceEventAdapter(event));
	}

	@Override
	public void dragStart(DragSourceEvent event) {
		this.start(new DragSourceEventAdapter(event));
	}
}
