/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.tooltip;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.ToolTip;
import org.eclipse.ui.forms.widgets.FormText;


/**
 * This interface give abilities to contribute to {@link ToolTip}<br>
 * @see ToolTipConfigurator
 * @see Tooltip
 * @author Mladen Tomov
 * 
 */
public interface IToolTipConfigurator {
	/**
	 * Returns text, that will be displayed in {@link Tooltip} form. The string returned needs to be fulfil {@link FormText} string requirement. The {@link ToolTipConfigurator} class provides means for automatic conversion a regular string to this format
	 * */
	public abstract String getFormText();

	/**
	 * Returns title's section image
	 * */
	public abstract Image getTitleImage();

	/**
	 * Returns title's section caption
	 * */
	public abstract String getTitleCaption();
}
