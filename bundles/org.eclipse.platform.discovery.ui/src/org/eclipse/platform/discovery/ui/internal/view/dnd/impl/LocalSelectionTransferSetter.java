/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.dnd.impl;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.platform.discovery.ui.internal.view.dnd.ISourceDndInteractionEvent;


public class LocalSelectionTransferSetter implements DragSrcInteractionListener.ITransferDataSetter<LocalSelectionTransfer>
{
	@Override
	public void dragStarted(IStructuredSelection selection, ISourceDndInteractionEvent event)
	{
		getTransfer().setSelection(selection);
	}
	
	@Override
	public LocalSelectionTransfer getTransfer()
	{
		return LocalSelectionTransfer.getTransfer();
	}

	@Override
	public void setData(IStructuredSelection data, ISourceDndInteractionEvent event)
	{
		getTransfer().setSelection(data);
		event.setData(data);
	}
}
