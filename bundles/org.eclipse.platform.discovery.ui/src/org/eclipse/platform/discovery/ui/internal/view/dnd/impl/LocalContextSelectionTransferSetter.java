/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.dnd.impl;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.platform.discovery.core.internal.IContextStructuredSelection;
import org.eclipse.platform.discovery.ui.internal.dnd.LocalContextSelectionTransfer;
import org.eclipse.platform.discovery.ui.internal.view.dnd.ISourceDndInteractionEvent;


/**
 * Setter for the {@link LocalContextSelectionTransfer} transfer type. It is assumed that the data set is instance of {@link IContextStructuredSelection} 
 * @author Iliyan Dimov
 */
public class LocalContextSelectionTransferSetter implements DragSrcInteractionListener.ITransferDataSetter<LocalContextSelectionTransfer>
{
	@Override
	public void setData(IStructuredSelection data, ISourceDndInteractionEvent event)
	{
		assert data instanceof IContextStructuredSelection;
		
		getTransfer().setSelection((IContextStructuredSelection)data);
		event.setData(data);
	}
	
	
	@Override
	public void dragStarted(IStructuredSelection selection, ISourceDndInteractionEvent event)
	{
		assert selection instanceof IContextStructuredSelection; 
		
		getTransfer().setSelection((IContextStructuredSelection)selection);
	}
	

	@Override
	public LocalContextSelectionTransfer getTransfer()
	{
		return LocalContextSelectionTransfer.getTransfer();
	}
}
