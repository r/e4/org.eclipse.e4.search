/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.search.advancedparams;

import org.eclipse.platform.discovery.core.api.IEnablable;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.ISearchParameters;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.ui.api.IAdvancedSearchParamsUiContributor;
import org.eclipse.platform.discovery.ui.api.IViewUiContext;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;
import org.eclipse.platform.discovery.util.internal.property.IPropertyAttributeListener;
import org.eclipse.swt.graphics.Point;


/**
 * Interface which is responsible for managing custom search parameters UI 
 * @author Danail Branekov
 */
public interface IAdvancedSearchParamsDisplayer extends IEnablable
{
	/**
	 * Updates the displayer for the object type and search destination specified
	 * @param objectType object type; can be null
	 * @param searchDestination search destination; it can be null
	 * @param environment the search console environment
	 * @param uiContext UI context
	 */
	public void update(final IObjectTypeDescription objectType, final ISearchDestination searchDestination, final IDiscoveryEnvironment environment, final IViewUiContext uiContext);

	/**
	 * Sets the search parameters via delegating to {@link IAdvancedSearchParamsUiContributor#getParameters()}. If there is no relevant custom UI this method has no effect 
	 * @param searchParams the {@link ISearchParameters} instance to set the custom parameters
	 */
	public void setParams(final ISearchParameters searchParams);
	
	/**
	 * Registers a listener which will be notified upon changes in section size
	 * @param listener listener to register
	 * @param notifyCurrent whether the listener to be notified for the current value
	 */
	public void registerSizePropertyChangeListener(final IPropertyAttributeListener<Point> listener, boolean notifyCurrent);
}
