/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.platform.discovery.runtime.api.IDestinationChangeHandler;
import org.eclipse.platform.discovery.runtime.api.IDestinationsProvider;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.internal.ISearchProviderConfiguration;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationsProviderDescription;
import org.eclipse.platform.discovery.ui.internal.selector.SearchDestinationsSelector;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.forms.widgets.FormToolkit;


/**
 * The search console search destinations selector. Besides the standard search destinations selector functionalities, this implementation handles registering and unregistering the destinations change handler to the destinations providers at the proper point of time
 * 
 * @author Danail Branekov
 * 
 */
public abstract class SearchConsoleDestinationsSelector extends SearchDestinationsSelector
{
	private IDestinationChangeHandler destinationsChangeHandler;
	private final ISearchProviderConfiguration searchProviderConfig;
	final Map<IDestinationCategoryDescription, Set<IDestinationsProvider>> currentDestinationProviders;

	public SearchConsoleDestinationsSelector(final Composite parent, final FormToolkit formToolkit, final int secondColumnPosition, final Control topNeighbourControl, final ISearchProviderConfiguration searchProviderConfig)
	{
		super(parent, formToolkit, secondColumnPosition, topNeighbourControl);
		this.searchProviderConfig = searchProviderConfig;
		this.currentDestinationProviders = new HashMap<IDestinationCategoryDescription, Set<IDestinationsProvider>>();

		parent.addDisposeListener(new DisposeListener()
		{
			@Override
			public void widgetDisposed(DisposeEvent e)
			{
				disposeSelector();
			}
		});
	}
	
	public void registerDestinationsChangeHandler(final IDestinationChangeHandler handler)
	{
		this.destinationsChangeHandler = handler;
	}

	private void disposeSelector()
	{
		unregisterCurrentDestinationsChangeHandler();
		this.destinationsChangeHandler = null;
	}

	@Override
	protected Set<IDestinationsProvider> getDestinationProvidersForCategory(final IDestinationCategoryDescription category)
	{
		final Set<IDestinationsProvider> result = currentDestinationProviders.get(category);
		if (result == null)
		{
			return Collections.emptySet();
		}

		return result;
	}

	@Override
	protected List<ISearchDestination> getSearchDestinations(IDestinationCategoryDescription category, IDestinationsProvider destinationsProvider)
	{
		return this.searchProviderConfig.getSearchDestinations(category, destinationsProvider);
	}

	@Override
	public  void setInput(List<IDestinationCategoryDescription> input)
	{
		if(this.destinationsChangeHandler == null)
		{
			return;
		}
		
		unregisterCurrentDestinationsChangeHandler();

		for (IDestinationCategoryDescription cat : input)
		{
			if (currentDestinationProviders.get(cat) == null)
			{
				currentDestinationProviders.put(cat, new HashSet<IDestinationsProvider>());
			}

			for (IDestinationsProviderDescription providerDescription : this.searchProviderConfig.getDestinationProvidersForCategory(cat))
			{
				final IDestinationsProvider provider = providerDescription.createProvider();
				provider.registerDestinationsChangeHandler(destinationsChangeHandler);
				currentDestinationProviders.get(cat).add(provider);
			}
		}

		super.setInput(input);
	}

	private void unregisterCurrentDestinationsChangeHandler()
	{
		if(this.destinationsChangeHandler == null)
		{
			return;
		}
		
		for (Set<IDestinationsProvider> providersList : currentDestinationProviders.values())
		{
			for (final IDestinationsProvider p : providersList)
			{
				p.unregisterDestinationsChangeHandler(destinationsChangeHandler);
			}
		}
		currentDestinationProviders.clear();
	}
	
}
