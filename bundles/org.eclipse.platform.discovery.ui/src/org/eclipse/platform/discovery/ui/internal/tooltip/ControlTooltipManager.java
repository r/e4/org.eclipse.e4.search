/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.tooltip;

import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Control;

/**
 * Tooltip manager for displaying tooltips on controls
 * 
 * @author Danail Branekov
 */
public abstract class ControlTooltipManager extends TooltipManager<Control>
{
	private final Control control;

	protected ControlTooltipManager(final Control control, final IInformationControlCreator creator)
	{
		super(creator);
		this.control = control;
	}

	@Override
	protected Control hoveredWidget()
	{
		return control;
	}
	
	@Override
	protected Rectangle hoveredWidgetBounds(Control hoveredWidget)
	{
		return control.getBounds();
	}
}
