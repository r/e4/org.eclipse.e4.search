/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view;

import java.util.List;

import org.eclipse.platform.discovery.core.internal.console.ISearchConsoleController;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.internal.ISearchProviderConfiguration;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.ISearchProviderDescription;
import org.eclipse.swt.widgets.Composite;

public interface ISearchProviderSelector extends IGetControlObject<Composite> {
	/**
	 * The context via which search parameters communicate with the search console input parameters section
	 */
	public interface IConsoleContext
	{
		/**
		 * Notifies the console that the search can be performed (i.e. the user provided sufficient input data)
		 */
		public void notifyComplete(final boolean complete);
		
		/**
		 * The console aligns its fixed parameters UI (object type, search destination) in two columns. The digit returned by this method represents the 
		 * relative offset at which the second column starts. Contributors may have this value in mind in order to align their custom user interface gracefully 
		 */
		public int secondColumnLeftOffset();
		
		/**
		 * Provides the console search provider configuration; never null
		 * @see ISearchProviderConfiguration
		 */
		public ISearchProviderConfiguration searchProviderConfiguration();
	}
	
	
	
	public IObjectTypeDescription getSelectedObjectType();

	public ISearchDestination getSelectedDestination();
	
	public ISearchProviderDescription getSelectedSearchProvider();
	
	public IDestinationCategoryDescription getActiveDestinationCategory();
	
	public void setObjectTypes(final List<IObjectTypeDescription> objectTypes);

	public void setDestinations(List<IDestinationCategoryDescription> categories);

	public void updateDestinationsUI();

	public void setController(ISearchConsoleController controller);
}