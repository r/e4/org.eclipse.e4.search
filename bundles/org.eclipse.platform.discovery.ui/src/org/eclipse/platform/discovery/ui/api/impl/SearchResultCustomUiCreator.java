/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.api.impl;

import java.util.Set;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.core.api.ISearchContext;
import org.eclipse.platform.discovery.core.api.ISubSearchContext;
import org.eclipse.platform.discovery.ui.api.ISearchResultCustomUiCreator;
import org.eclipse.platform.discovery.ui.api.ISearchConsoleCustomization;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;


/**
 * Abstract empty implementation of the {@link ISearchResultCustomUiCreator} interface
 * @author Danail Branekov
 *
 */
public class SearchResultCustomUiCreator implements ISearchResultCustomUiCreator
{
	public void showSubResult(ISubSearchContext subSearchContext)
	{
		throw new UnsupportedOperationException("Method not implemented"); //$NON-NLS-1$
	}

	@Override
	public Composite createSearchUi(Composite parent,
			ISearchContext searchContext, FormToolkit formToolkit,
			Set<IContributedAction> actions,
			Set<ISearchConsoleCustomization> viewCustomizations) {
		return null;
	}

	@Override
	public void registerResultSelectionChangedListener(
			ISelectionChangedListener selChangedListener) {
	}

	@Override
	public void restore(Object data) {
		
	}

	@Override
	public Object restoreData() {
		return null;
	}
}
