/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.dnd;

/**
 * Interaction event
 * 
 * @author Danail Branekov
 * 
 * @param <T>
 *            the type of interaction data this event represents
 * @param <D>
 *            the data carried by this event
 * @param <IDetail>
 *            the interaction detail type
 */
public interface IInteractionEvent<T, D, IDetail>
{
	public D getData();
	public void setData(final D data);

	public T getDataType();

	public IDetail getInteractionDetail();
	public void setInteractionDetail(IDetail detail);
}
