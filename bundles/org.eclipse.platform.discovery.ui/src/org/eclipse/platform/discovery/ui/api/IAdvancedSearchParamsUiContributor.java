/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.api;

import java.util.Map;

import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.ui.api.impl.AdvancedSearchUiContributor;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;


/**
 * Interface for contributing advanced search parameter UI<br> This UI is displayed on the advanced section of the search console.
 * Do keep in mind that this interface might be a subject of a future change. Therefore, in order to avoid compilation problems, it is recommended to extend {@link AdvancedSearchUiContributor} instead of implementing this interface
 * 
 * @author Danail Branekov
 */
public interface IAdvancedSearchParamsUiContributor
{
	/**
	 * Creates the UI
	 * 
	 * @param parent
	 *            the parent composite. Its layout is {@link FormLayout}
	 * @param formToolkit
	 *            form toolkit to create controls
	 * @param the
	 *            search destination for which the search is to be performed in
	 * @param environment
	 *            the search console environment
	 * @param uiContext UI context
	 */
	public void createUi(final Composite parent, final ISearchDestination searchDestination, final FormToolkit formToolkit, final IDiscoveryEnvironment environment, final IViewUiContext uiContext);

	/**
	 * Retrieves a map of advanced search parameters which should reflect the user's interraction with the UI 
	 * @return a map of search parameters; empty map if none
	 */
	public Map<Object, Object> getParameters();
	
	/**
	 * Sets the enabled state of the UI. Becoming disabled means that the contributed UI should not allow user interaction. Enabling the UI means that the UI should be able to interact with the user
	 * 
	 * @param enable
	 *            true to enable the UI, false to disable it.
	 */
	public void setEnabled(final boolean enable);

	/**
	 * This method is invoked by the framework whenever the search parameters visibility changes
	 * @param visible true in case the UI was shown; false in case the UI was hidden 
	 */
	public void handleVisibilityChange(final boolean visible);
}
