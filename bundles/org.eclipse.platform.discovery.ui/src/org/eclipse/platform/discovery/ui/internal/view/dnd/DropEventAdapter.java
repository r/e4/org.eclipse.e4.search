/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.dnd;

import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TransferData;


public class DropEventAdapter implements IDndInteractionEvent
{
	private final DropTargetEvent dropEvent;
	
	public DropEventAdapter(final DropTargetEvent dropEvent)
	{
		this.dropEvent = dropEvent;
	}

	@Override
	public Object getData()
	{
		return this.dropEvent.data;
	}

	@Override
	public void setData(Object data)
	{
		this.dropEvent.data = data;
	}

	@Override
	public TransferData getDataType()
	{
		return this.dropEvent.currentDataType;
	}

	@Override
	public Integer getInteractionDetail()
	{
		return this.dropEvent.detail;
	}

	@Override
	public void setInteractionDetail(Integer detail)
	{
		this.dropEvent.detail = detail;
	}
}
