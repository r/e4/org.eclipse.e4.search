/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.api.impl;

import java.text.MessageFormat;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.platform.discovery.ui.internal.plugin.DiscoveryUIMessages;
import org.eclipse.platform.discovery.util.api.env.IErrorHandler;
import org.eclipse.platform.discovery.util.internal.logging.ILogger;
import org.eclipse.platform.discovery.util.internal.logging.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;


/**
 * Implementation of the {@link IErrorHandler} interface
 * @author Danail Branekov
 */
public class ErrorHandler implements IErrorHandler
{
	@Override
	public void handleException(final String title, final Exception exc)
	{
		final String msgAndHint = MessageFormat.format(DiscoveryUIMessages.ErrorHandler_SentenceCombiner, DiscoveryUIMessages.ErrorHandler_UNEXPECTED_ERROR_MSG, DiscoveryUIMessages.ErrorHandler_SEE_LOG_HINT_MSG);
		logger().logError(exc.getMessage(), exc);
		showError(title, msgAndHint);
	}
	
	@Override
	public void handleException(final Exception exc)
	{
		handleException(null, exc);
	}
	
	protected ILogger logger()
	{
		return Logger.instance();
	}
	
	private void showErrorMessageInDialog(final String dialogTitle, final String message)
	{
		PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable()
		{
			@Override
			public void run()
			{
				final ErrorMsgDialog errDialog = new ErrorMsgDialog(null, dialogTitle, message);
				errDialog.open();
			}
		});
	}
	
	@Override
	public void showError(final String title, final String errorMessage)
	{
		final String effectiveTitle = (title != null ? title : DiscoveryUIMessages.ErrorHandler_DIALOG_TITLE);
		showErrorMessageInDialog(effectiveTitle, errorMessage);
	}
	

	/**
	 * Extender of the {@link MessageDialog} class. The error message is shown in a {@link Text} control thus allowing text selection and copying
	 * @author Danail Branekov
	 */
	private class ErrorMsgDialog extends MessageDialog
	{
		final String messageToDisplay;
		
		public ErrorMsgDialog(final Shell parentShell, final String dialogTitle, final String dialogMessage)
		{
			super(parentShell, dialogTitle, null, null, MessageDialog.ERROR, new String[] { IDialogConstants.OK_LABEL }, 0);
			this.messageToDisplay = dialogMessage;
		}
		
		@Override
		protected Control createMessageArea(final Composite composite)
		{
			composite.setLayout(new GridLayout(2, false));
			final Control control = super.createMessageArea(composite);
			final Text messageText = new Text(composite, SWT.MULTI | SWT.WRAP);
			messageText.setText(this.messageToDisplay);
			messageText.setEditable(false);
			GridDataFactory.fillDefaults().align(SWT.FILL, SWT.BEGINNING).grab(true, false).hint(convertHorizontalDLUsToPixels(IDialogConstants.MINIMUM_MESSAGE_AREA_WIDTH), SWT.DEFAULT).applyTo(
											messageText);
			return control;
		}
		
		@Override
		public int open()
		{
			setShellStyle(getShellStyle() | SWT.SHEET);
			return super.open();
		}
	}
}
