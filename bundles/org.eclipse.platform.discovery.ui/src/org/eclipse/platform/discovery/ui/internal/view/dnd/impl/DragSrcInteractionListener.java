/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.dnd.impl;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.platform.discovery.runtime.api.OnDemandLoadException;
import org.eclipse.platform.discovery.ui.internal.view.dnd.IDndInteractionListener;
import org.eclipse.platform.discovery.ui.internal.view.dnd.ISourceDndInteractionEvent;
import org.eclipse.platform.discovery.util.api.env.IErrorHandler;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.Transfer;


public class DragSrcInteractionListener implements IDndInteractionListener<ISourceDndInteractionEvent>
{

	public static interface ITransferDataSetter<T extends Transfer>
	{
		public T getTransfer();
		
		public abstract void setData(IStructuredSelection data, ISourceDndInteractionEvent event);
		
		/**
		 * This method will be called by {@link DragSrcListener} whenever {@link #dragStarted(IStructuredSelection, DragSourceEvent)} method is invoked and the event.doit flag is set to true. Implementors should not modify the event.data. If they want to pass some data to the drop client during the
		 * drag and before drop they should use some other means, e.g. singleton
		 * 
		 * @param selection
		 * @param event
		 */
		public void dragStarted(final IStructuredSelection selection, final ISourceDndInteractionEvent event);
	}

	public interface ISelectionObtainer
	{
		IStructuredSelection getSelection();
	}

	private final ISelectionObtainer selectionObtainer;
	ITransferDataSetter<? extends Transfer>[] transfers;
	private final IErrorHandler errorHandler;

	public DragSrcInteractionListener(DragSrcInteractionListener.ISelectionObtainer selectionObtainer,
			DragSrcInteractionListener.ITransferDataSetter<? extends Transfer>[] transfers, IErrorHandler errorHandler) {
		this.selectionObtainer = selectionObtainer;
		this.transfers = transfers;
		this.errorHandler = errorHandler;
	}

	@Override
	public void finish(ISourceDndInteractionEvent event)
	{
	}

	@Override
	public void process(ISourceDndInteractionEvent event)
	{
		try {
			for (DragSrcInteractionListener.ITransferDataSetter<? extends Transfer> tds : transfers) {
				//do not remove this if statement. If you do not check
				//whether the data is supported, but simply call event.data=<some data>
				//the event would be overriden. If you check the data however and leave
				//the data null, then the framework would call this method again with
				//a different event. This way several datas could be kept in memory
				//simultaneously which allows to transfer objects in different formats
				if (tds.getTransfer().isSupportedType(event.getDataType()))
				{
					tds.setData(selectionObtainer.getSelection(), event);
				}
			}
		} catch (OnDemandLoadException odl) {
			errorHandler.handleException(odl);
		}
	}

	@Override
	public void start(ISourceDndInteractionEvent event)
	{
		final IStructuredSelection selection = selectionObtainer.getSelection(); 
		event.setDoIt(!selection.isEmpty());
		if(event.getDoIt())
		{
			for (DragSrcInteractionListener.ITransferDataSetter<? extends Transfer> tds : transfers) {
				tds.dragStarted(selection, event);
			}
		}
	}
	
	public Transfer[] getTransfers() {
		final Transfer[] ts = new Transfer[transfers.length];
		for (int i = 0; i < transfers.length; i++) {
			ts[i] = transfers[i].getTransfer();
		}
		return ts;
	}
}
