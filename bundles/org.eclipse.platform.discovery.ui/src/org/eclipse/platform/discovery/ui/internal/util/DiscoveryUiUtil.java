/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.util;

import org.eclipse.platform.discovery.ui.internal.SlidingComposite.ORIENTATION;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Control;


/**
 * 
 * Utility class which provides methods for dealing with common SWT UI situations
 * 
 * @author Danail Branekov
 */
public class DiscoveryUiUtil
{
	private static DiscoveryUiUtil instance;

	private DiscoveryUiUtil()
	{

	}

	public static DiscoveryUiUtil instance()
	{
		if (instance == null)
		{
			instance = new DiscoveryUiUtil();
		}
		return instance;
	}

	/**
	 * Gets the height of a control when orientation is vertical or the width of the control when the orientation is horizontal. If {@link Control#getSize()} returned a point of with x or y equal to 0, this indicates that the control size is not computed yet. In this case the method will call
	 * {@link Control#computeSize(int, int)} and will return the data resulted from computation
	 * 
	 * @param control
	 *            the control
	 * @param orientation
	 *            orientation
	 * @return
	 */
	public int getHeightOrWidthDependingOnOrientation(final Control control, final ORIENTATION orientation)
	{
		int size = pointComponent(control.getSize(), orientation);
		if(size == 0)
		{
			size = pointComponent(control.computeSize(SWT.DEFAULT, SWT.DEFAULT), orientation);
		}
		
		return size;  
	}

	public int pointComponent(final Point point, final ORIENTATION orientation)
	{
		switch (orientation)
		{
		case VERTICAL:
			return point.y;
		case HORIZONTAL:
			return point.x;
		default:
			throw new IllegalArgumentException("Unsupported alignment"); //$NON-NLS-1$
		}
	}

}
