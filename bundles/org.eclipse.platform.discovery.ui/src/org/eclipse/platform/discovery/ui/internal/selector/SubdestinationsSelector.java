/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.selector;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import org.eclipse.jface.window.ToolTip;
import org.eclipse.platform.discovery.core.internal.selectors.IStartableItemsSelector;
import org.eclipse.platform.discovery.runtime.api.ISearchSubdestination;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.ui.forms.ManagedForm;
import org.eclipse.ui.forms.widgets.FormToolkit;


/**
 * The very UI which selects subdestinations<br>
 * The UI itself consists of check buttons per each available subdestination. Changing the check buttons selection state immediately activates or
 * deactivates the search subdestination.<br>
 * The active subdestinations are selected by default.<br>
 * The subdestinations are sorted alphabetically by their display name<br>
 * When the UI is started the focus is set to the first check button<br>
 * The creator of the selector instance has to take care to call its {@link #dispose()} method when the selector is no longer required
 * 
 * @author Danail Branekov
 */
public abstract class SubdestinationsSelector extends ToolTip implements IStartableItemsSelector<ISearchSubdestination>
{
	private final List<ISearchSubdestination> subDestinations;

	private final Point showPoint;

	private final LinkedHashMap<Button,ISearchSubdestination> allButtons;
	
	private ManagedForm _managedForm;

	public SubdestinationsSelector(Control control, int style, final List<ISearchSubdestination> subDestinations, final Point showPoint)
	{
		super(control, style, true);
		this.setHideOnMouseDown(false);
		this.subDestinations = Collections.unmodifiableList(subDestinations);
		this.showPoint = showPoint;
		allButtons = new LinkedHashMap<Button, ISearchSubdestination>();
	}

	@Override
	protected Composite createToolTipContentArea(Event event, Composite parent)
	{
		final ManagedForm managedForm = newManagedForm(parent);
		final Composite formComposite = managedForm.getForm().getBody();
		final FormToolkit formToolkit = managedForm.getToolkit();

		formComposite.setLayout(new GridLayout(1, false));

		for (final ISearchSubdestination subDest : sortSubdestinations(subDestinations))
		{
			final Button subdestButton = formToolkit.createButton(formComposite, subDest.getDisplayName(), SWT.CHECK);
			allButtons.put(subdestButton,subDest);
			subdestButton.setSelection(isSubdestinationActive(subDest));
			subdestButton.addSelectionListener(new SubdestinationCheckboxSelectionListener()
			{

				public void widgetSelected(SelectionEvent e)
				{
					assert e.getSource() instanceof Button;
					final Button b = (Button) e.getSource();
					handleSelection(subDest, b.getSelection());
					
					if(b.getSelection()) {
						for(Entry<Button, ISearchSubdestination> curr: allButtons.entrySet()) {
							if(!isSubdestinationActive(curr.getValue()) && !curr.getKey().equals(b)) {
								curr.getKey().setSelection(false);
							}
						}
					}
				}
			});
		}

		return managedForm.getForm();
	}

	protected void setFocus()
	{
		Iterator<Button> iter = allButtons.keySet().iterator();
		iter.next().setFocus();
	}

	protected List<ISearchSubdestination> sortSubdestinations(final List<ISearchSubdestination> subdestinations)
	{
		final ISearchSubdestination[] sortedSubdestinations = new ISearchSubdestination[subdestinations.size()];
		Arrays.sort(subdestinations.toArray(sortedSubdestinations), new Comparator<ISearchSubdestination>()
		{

			public int compare(ISearchSubdestination o1, ISearchSubdestination o2)
			{
				return o1.getDisplayName().compareTo(o2.getDisplayName());
			}
		});

		return Arrays.asList(sortedSubdestinations);
	}

	public void select()
	{
		this.show(showPoint);
		setFocus();
	}
	
	private abstract class SubdestinationCheckboxSelectionListener implements SelectionListener
	{
		public void widgetDefaultSelected(SelectionEvent e)
		{
			widgetSelected(e);
		}
	}
	
	protected abstract boolean isSubdestinationActive(final ISearchSubdestination subdestination);
	
	private ManagedForm newManagedForm(final Composite parent)
	{
		disposeCurrentForm();
		_managedForm = new ManagedForm(parent);
		return _managedForm;
	}
	
	private void disposeCurrentForm()
	{
		if(_managedForm != null && !_managedForm.getForm().isDisposed())
		{
			_managedForm.dispose();
		}
	}
	
	/**
	 * Disposes the selector. If the selector is already disposed, this method has no effect
	 */
	public void dispose()
	{
		disposeCurrentForm();
	}
	
}
