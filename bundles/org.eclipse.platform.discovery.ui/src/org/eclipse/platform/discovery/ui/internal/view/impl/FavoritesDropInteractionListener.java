/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.impl;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.platform.discovery.core.api.ISearchFavoritesMasterController;
import org.eclipse.platform.discovery.ui.internal.dnd.LocalContextSelectionTransfer;
import org.eclipse.platform.discovery.ui.internal.view.dnd.IDndInteractionEvent;
import org.eclipse.platform.discovery.ui.internal.view.dnd.IDndInteractionListener;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;


/**
 * Search favorites drop listener<br>
 * Supports {@link LocalSelectionTransfer}<br>
 * Will accept a selection in case
 * <ul>
 * <li>it is instance of {@link IStructuredSelection}</li>
 * <li>for each of its elements there is a search favorites customization which provides items 
 * </ul>
 * 
 * @author Danail Branekov
 * 
 */
public class FavoritesDropInteractionListener implements IDndInteractionListener<IDndInteractionEvent>
{
	private final Transfer[] transfers;
	private final ISearchFavoritesMasterController controller;
	
	/**
	 * Constructor
	 * @param viewCustomizations a set of search favorites view customizations
	 * @param itemsAddedHandler a handler which will be notified on successful drop
	 */
	public FavoritesDropInteractionListener(final Transfer[] transfers, ISearchFavoritesMasterController controller)
	{
		this.transfers = transfers;
		this.controller = controller;
	}

	@Override
	public void process(IDndInteractionEvent event)
	{
		controller.importData(event.getData());
	}

	@Override
	public void start(IDndInteractionEvent event)
	{
		boolean eventAccepted = false;
		for(Transfer transfer : transfers)
		{
			if(transfer.isSupportedType(event.getDataType()))
			{
				//this check is only for local transfer because only in this transfer 
				//we know about what is selected on dragEnter
				//in transfers event.data is not populated for drag enter that's why we
				//need this selection
				if(transfer instanceof LocalContextSelectionTransfer && ((LocalContextSelectionTransfer)transfer).getSelection()!= null)
				{
					if(controller.isImportPossible(((LocalContextSelectionTransfer)transfer).getSelection()))
					{
						eventAccepted = true;
						break;
					}
				}
				else
				{
					eventAccepted = true;
					break;
				}
			}
		}
		event.setInteractionDetail(eventAccepted ? DND.DROP_DEFAULT : DND.DROP_NONE);
	}

	@Override
	public void finish(IDndInteractionEvent event)
	{
		// nothing to do
	}
}
