/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.util;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Control;

/**
 * 
 * This utility class gives the ability to use various helper methods for painting UI elements. Users of this class are responsible for destroying of allocated resources by explicitly invoking {@link #disposeResources()} method<b> A typical usage for this utility is to create an instance per a
 * control and use {@link #setBackgroundGradient(Color, Color, boolean)} or {@link #setBackground(Color)} methods. Each of this methods would dispose the previous image and create a new one
 * 
 * @author Mladen Tomov, Danail Branekov
 * 
 */
public class ControlBackgroundImageManager
{
	private final Control control;
	private final StatefulImageFactory imageFactory;

	public ControlBackgroundImageManager(final Control control)
	{
		this.control = control;
		imageFactory = new StatefulImageFactory();
	}

	/**
	 * Sets given color as a background of the control
	 * 
	 * @param color
	 *            - color , which is set as background
	 */
	public void setBackground(final Color color)
	{
		control.setBackgroundImage(createSolidColourImage(color));
	}

	/**
	 * Sets the background of a control to be a gradient image
	 * 
	 * @param foreground
	 *            the gradient foreground
	 * @param background
	 *            the gradient background
	 * @param vertical
	 * @see GC#fillGradientRectangle(int, int, int, int, boolean)
	 */
	public void setBackgroundGradient(final Color foreground, final Color background, final boolean vertical)
	{
		control.setBackgroundImage(createGradientImage(foreground, background, vertical));
	}

	private Image createSolidColourImage(final Color color)
	{
		final Rectangle rectangle = control.getBounds();
		final Image img = imageFactory.createNewImage(rectangle);
		if (img != null)
		{
			final GC graphic = new GC(img);
			try
			{
				graphic.setBackground(color);
				graphic.fillRectangle(0, 0, rectangle.width, rectangle.height);
			} finally
			{
				graphic.dispose();
			}
		}
		return img;
	}

	private Image createGradientImage(final Color foreground, final Color background, final boolean vertical)
	{
		final Rectangle rectangle = control.getBounds();
		final Image img = imageFactory.createNewImage(rectangle);
		if (img != null)
		{
			final GC graphic = new GC(img);
			try
			{
				graphic.setForeground(foreground);
				graphic.setBackground(background);
				graphic.fillGradientRectangle(0, 0, rectangle.width, rectangle.height, vertical);
			} finally
			{
				graphic.dispose();
			}
		}
		return img;

	}

	/**
	 * Dispose allocated color for this control
	 * 
	 */
	public void disposeResources()
	{
		imageFactory.disposeImage();
	}

	private class StatefulImageFactory
	{
		private Image _img;

		public Image createNewImage(final Rectangle rectangle)
		{
			final Image oldImage = _img;
			if (oldImage != null && !oldImage.isDisposed())
			{
				oldImage.dispose();
			}

			_img = shouldDrawImage(rectangle) ? new Image(control.getDisplay(), rectangle.width, rectangle.height) : null;
			return _img;
		}

		private boolean shouldDrawImage(final Rectangle rectangle)
		{
			return rectangle.height != 0 && rectangle.width != 0;
		}

		public void disposeImage()
		{
			if ((_img != null) && (!_img.isDisposed()))
			{
				_img.dispose();
			}
		}
	}
}
