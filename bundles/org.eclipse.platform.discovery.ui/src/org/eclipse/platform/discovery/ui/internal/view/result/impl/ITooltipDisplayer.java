/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.internal.view.result.impl;

import org.eclipse.swt.widgets.Item;

public interface ITooltipDisplayer {

	/**
	 * Hides the tooltip 
	 */
	public void hideTooltip();
	/**
	 * Shows tooltip for the <code>item</code>
	 * @param item - the item for wich the tooltip has to be shown
	 */
	public void showTooltip(final Item item);
}
