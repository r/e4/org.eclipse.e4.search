/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.ui.api.impl;

import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.ui.api.IGenericViewCustomization;
import org.eclipse.platform.discovery.ui.api.IMasterDiscoveryView;
import org.eclipse.platform.discovery.ui.api.IResultsViewAccessor;
import org.eclipse.platform.discovery.ui.api.ITooltipProvider;


/**
 * "Empty" implementation of the {@link IGenericViewCustomization} interface
 * @author Danail Branekov
 */
public class GenericViewCustomizationImpl implements IGenericViewCustomization
{ 
	@Override
	public ITreeContentProvider getContentProvider()
	{
		return null;
	}

	@Override
	public IDoubleClickListener getDoubleClickListener()
	{
		return null;
	}

	@Override
	public ILabelProvider getLabelProvider()
	{
		return null;
	}

	@Override
	public ITooltipProvider getTooltipProvider()
	{
		return null;
	}

	@Override
	public void installAction(IContributedAction contributedAction, IResultsViewAccessor viewAccessor)
	{
	}

	@Override
	public void setMasterView(IMasterDiscoveryView masterView)
	{
	}

	@Override
	public void postResultDisplayed(IResultsViewAccessor viewAccessor) {
		
		
	}

	@Override
	public void selectionChanged(ISelection selection)
	{
	}
}
