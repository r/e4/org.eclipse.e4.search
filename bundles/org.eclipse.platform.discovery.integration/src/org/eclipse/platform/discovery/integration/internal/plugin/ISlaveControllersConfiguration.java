/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.integration.internal.plugin;

import java.util.List;

import org.eclipse.platform.discovery.core.api.ISearchConsoleSlaveController;
import org.eclipse.platform.discovery.core.api.ISearchFavoritesSlaveController;
import org.eclipse.platform.discovery.integration.internal.ICustomizationConfig;


public interface ISlaveControllersConfiguration extends ICustomizationConfig
{
	/**
	 * Retrieves a set of available search console "slave" controllers
	 */
	public List<ISearchConsoleSlaveController> availableSearchConsoleSlaveControllers();
	
	/**
	 * Retrieves a set of available search favorites "slave" controllers
	 */
	public List<ISearchFavoritesSlaveController> availableSearchFavoritesSlaveControllers();
}