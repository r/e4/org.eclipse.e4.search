/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.integration.internal.plugin;


import java.util.List;

import org.eclipse.platform.discovery.integration.internal.ICustomizationConfig;
import org.eclipse.platform.discovery.ui.api.ISearchConsoleCustomization;
import org.eclipse.platform.discovery.ui.api.ISearchFavoritesViewCustomization;


/**
 * Interface for accessing the available view customizations
 * @author Danail Branekov
 */ 
public interface IViewCustomizationConfiguration extends ICustomizationConfig
{
	/**
	 * Retrieve a set of available search console view customizations or an empty set if none
	 */
	public List<ISearchConsoleCustomization> availableSearchConsoleCustomizations();
	
	/**
	 * Retrieve a set of available search favorites view customizations or an empty set if none
	 */
	public List<ISearchFavoritesViewCustomization> availableSearchFavoritesCustomizations();
}
