/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.integration.internal;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.runtime.internal.xp.IContributionsReader;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.AbstractExtensionPointParser;
import org.eclipse.platform.discovery.util.internal.logging.ILogger;
import org.eclipse.platform.discovery.util.internal.logging.Logger;


/**
 * Generic parser for the
 * 
 * @author Danail Branekov
 * 
 * @param <T>
 */
public abstract class ViewCustXpParser<T> extends AbstractExtensionPointParser<T> implements IContributionsReader<T>
{
	public ViewCustXpParser(final IExtensionRegistry extRegistry, String xpId, String elementName)
	{
		super(extRegistry, xpId, elementName);
	}


	protected ILogger logger()
	{
		return Logger.instance();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected T createObject(IConfigurationElement element)
			throws CoreException {
		
		return (T) element.createExecutableExtension(implementationAttributeName());
	}


	/**
	 * Extenders should implement this method to specify the attribute name which stands for the instantiatable implementation
	 */
	protected abstract String implementationAttributeName();
	
}