/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.integration.internal.viewcustomization;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.platform.discovery.core.api.ISearchContext;
import org.eclipse.platform.discovery.ui.api.ISearchFavoritesViewCustomization;
import org.eclipse.platform.discovery.ui.internal.view.ISearchFavoritesItemsAdder;
import org.eclipse.platform.discovery.ui.internal.view.SearchFavoritesItemsAdder;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;


/**
 * Implementation of the "Add to search favorites" JFace action<br>
 * This action should appear whenever the selection can be added to the search favorites. In order to determine whether this is the case the action needs all the registered search favorites view customizations
 * 
 * @author Danail Branekov
 */
public class SaveInFavoritesMenuAction extends Action implements IActionWithAvailability
{
	private final ISearchFavoritesItemsAdder itemsAdder;
	private final ISelection selection;

	/**
	 * Constructor
	 * 
	 * @param contributedAction
	 *            the contributed action to performed
	 * @param selection
	 *            the selection on which the action will be performer
	 * @param searchContext
	 *            the current search context
	 * @param sbViewCustomizations
	 *            a set of search favorites view customizations
	 * @param environment
	 *            environment
	 */
	public SaveInFavoritesMenuAction(final IConfigurableContributedAction contributedAction, final ISelection selection, final ISearchContext searchContext, final List<ISearchFavoritesViewCustomization> sbViewCustomizations, final IDiscoveryEnvironment environment)
	{
		super();
		this.selection = selection;

		itemsAdder = createSbAdder(contributedAction, sbViewCustomizations, environment, selection, searchContext);
	}

	/**
	 * Checks whether the action is available for the current selection
	 * 
	 * @return <code>true</code> in case the current selection can be added to the search favorites, <code>false</code> otherwise
	 */
	public boolean isAvailable()
	{
		return itemsAdder.canAddSelection(this.selection);
	}

	@Override
	public void run()
	{
		itemsAdder.addItems(this.selection);
	}

	protected ISearchFavoritesItemsAdder createSbAdder(final IConfigurableContributedAction saveAction, final List<ISearchFavoritesViewCustomization> sbViewCustomizations, final IDiscoveryEnvironment environment, final ISelection selection, final ISearchContext searchCtx)
	{
		return new SearchFavoritesItemsAdder(sbViewCustomizations, environment.operationRunner())
		{
			@Override
			protected void doAddItems(final Set<Object> items, final ILongOperationRunner operationRunner)
			{
				assert selection instanceof IStructuredSelection;
				saveAction.setErrorHandler(environment.errorHandler());
				saveAction.setSearchContext(searchCtx);
				saveAction.perform(environment.operationRunner(), toSet((IStructuredSelection) selection));
			}
		};
	}

	@SuppressWarnings("unchecked")
	private Set<Object> toSet(final IStructuredSelection selection)
	{
		final Set<Object> result = new HashSet<Object>();
		final Iterator<Object> it = selection.iterator();
		while (it.hasNext())
		{
			result.add(it.next());
		}

		return result;
	}
}
