/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.integration.internal.viewcustomization;


import java.util.List;

import org.eclipse.core.runtime.Platform;
import org.eclipse.platform.discovery.integration.internal.ViewCustXpParser;
import org.eclipse.platform.discovery.integration.internal.plugin.IViewCustomizationConfiguration;
import org.eclipse.platform.discovery.runtime.internal.xp.IContributionsReader;
import org.eclipse.platform.discovery.ui.api.ISearchConsoleCustomization;
import org.eclipse.platform.discovery.ui.api.ISearchFavoritesViewCustomization;


public class ViewCustomizationConfiguration implements IViewCustomizationConfiguration
{
	private static final String CUSTOMIZATION_IMPL_ATTRIBUTE = "customizationimpl"; //$NON-NLS-1$
	private static final String CUSTOMIZATION_ELEMENT = "viewcustomization"; //$NON-NLS-1$

	protected IContributionsReader<ISearchConsoleCustomization> seXpParser()
	{
		return new ViewCustXpParser<ISearchConsoleCustomization>(Platform.getExtensionRegistry(),SEARCH_CONSOLE_XP_ID, CUSTOMIZATION_ELEMENT)
		{
			@Override
			protected String implementationAttributeName()
			{
				return CUSTOMIZATION_IMPL_ATTRIBUTE;
			}
		};
	}

	protected IContributionsReader<ISearchFavoritesViewCustomization> sbXpParser()
	{
		return new ViewCustXpParser<ISearchFavoritesViewCustomization>(Platform.getExtensionRegistry(), SEARCH_FAVORITES_XP_ID, CUSTOMIZATION_ELEMENT)
		{
			@Override
			protected String implementationAttributeName()
			{
				return CUSTOMIZATION_IMPL_ATTRIBUTE;
			}
		};
	}

	public List<ISearchConsoleCustomization> availableSearchConsoleCustomizations()
	{
		return seXpParser().readContributions();
	}

	public List<ISearchFavoritesViewCustomization> availableSearchFavoritesCustomizations()
	{
		return sbXpParser().readContributions();
	}
}
