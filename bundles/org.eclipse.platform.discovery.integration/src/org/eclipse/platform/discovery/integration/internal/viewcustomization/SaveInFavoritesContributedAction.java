/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.integration.internal.viewcustomization;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.platform.discovery.core.api.ISearchContext;
import org.eclipse.platform.discovery.core.internal.favorites.IPersistenceUtil;
import org.eclipse.platform.discovery.integration.internal.plugin.DiscoveryIntegrationPlugin;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.ISearchParameters;
import org.eclipse.platform.discovery.runtime.api.persistence.DestinationItemPair;
import org.eclipse.platform.discovery.runtime.internal.persistence.MementoContentManagerException;
import org.eclipse.platform.discovery.util.api.env.IErrorHandler;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.platform.discovery.util.internal.ContractChecker;
import org.eclipse.ui.WorkbenchException;


/**
 * Add in search favorites action<br>
 * In order to successfully perform the action {@link #setSearchContext(ISearchParameters)} and {@link #setErrorHandler(IErrorHandler)} need to be invoked prior {@link #perform(ILongOperationRunner, Set)}. If this requirement is not kept, a runtime exception will be thrown on execution
 * 
 * @author Danail Branekov
 */
public class SaveInFavoritesContributedAction implements IConfigurableContributedAction
{
	/**
	 * The action ID
	 */
	public static final String ACTION_ID = "org.eclipse.platform.discovery.integration.internal.viewcustomization.SaveInFavoritesContributedAction"; //$NON-NLS-1$
	
	private ISearchContext searchContext;
	private IErrorHandler errorHandler;

	@Override
	public Object getActionId()
	{
		return ACTION_ID;
	}

	@Override
	public void perform(final ILongOperationRunner operationRunner, final Set<Object> selectedObjects)
	{
		ContractChecker.nullCheckField(searchContext, "searchContext"); //$NON-NLS-1$
		ContractChecker.nullCheckField(errorHandler, "errorHandler"); //$NON-NLS-1$

		final IPersistenceUtil persistenceUtil = createPersistenceUtil();
		try
		{
			persistenceUtil.addItems(toPairs(selectedObjects, searchContext.searchParameters().getSearchDestination()), operationRunner);
		} catch (WorkbenchException e)
		{
			this.errorHandler.handleException(e);
		} catch (IOException e)
		{
			this.errorHandler.handleException(e);
		} catch (MementoContentManagerException e)
		{
			this.errorHandler.handleException(e);
		}
	}
	
	private Set<DestinationItemPair> toPairs(final Set<Object> items, final ISearchDestination destination)
	{
		final Set<DestinationItemPair> result = new HashSet<DestinationItemPair>(items.size());
		for(Object o : items)
		{
			result.add(new DestinationItemPair(destination, o));
		}
		
		return result;
	}

	/**
	 * Creates a {@link IPersistenceUtil} instance which will be used to store the favorites items in the favorites store
	 */
	protected IPersistenceUtil createPersistenceUtil()
	{
		return DiscoveryIntegrationPlugin.getDefault().createFavoritesPersistenceUtil();
	}

	@Override
	public void setSearchContext(final ISearchContext searchContext)
	{
		this.searchContext = searchContext;
	}

	@Override
	public void setErrorHandler(final IErrorHandler errorHandler)
	{
		this.errorHandler = errorHandler;
	}
}
