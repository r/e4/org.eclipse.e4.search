/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.integration.internal.viewcustomization;

import java.util.List;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.core.api.ISearchContext;
import org.eclipse.platform.discovery.integration.internal.plugin.DiscoveryIntegrationMessages;
import org.eclipse.platform.discovery.integration.internal.plugin.IViewCustomizationConfiguration;
import org.eclipse.platform.discovery.ui.api.IMasterDiscoveryView;
import org.eclipse.platform.discovery.ui.api.IResultsViewAccessor;
import org.eclipse.platform.discovery.ui.api.ISearchConsoleCustomization;
import org.eclipse.platform.discovery.ui.api.ISearchFavoritesViewCustomization;
import org.eclipse.platform.discovery.ui.api.impl.GenericViewCustomizationImpl;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;


public class SaveInFavoritesViewCustomization extends GenericViewCustomizationImpl implements ISearchConsoleCustomization
{
	private IMasterDiscoveryView masterView;

	@Override
	public void installAction(final IContributedAction contributedAction, final IResultsViewAccessor viewAccessor)
	{
		if (!contributedAction.getActionId().equals(SaveInFavoritesContributedAction.ACTION_ID))
		{
			return;
		}

		final ISelection selection = viewAccessor.getTreeViewer().getSelection();
		final ISearchContext searchCtx = (ISearchContext) viewAccessor.getTreeViewer().getInput();
		final List<ISearchFavoritesViewCustomization> favoritesViewCustomizations = viewCustomizationConfiguration().availableSearchFavoritesCustomizations();

		final IActionWithAvailability action = createSaveAction(contributedAction, selection, searchCtx, favoritesViewCustomizations, this.masterView.getEnvironment());

		if (action.isAvailable())
		{
			action.setText(DiscoveryIntegrationMessages.SaveInSearchFavoritesAction_Text);
			viewAccessor.getMenuManager().add(action);
		}
	}

	@Override
	public boolean acceptSearchProvider(final String searchProviderId)
	{
		// accept all the search providers as the "save in favorites action" should be generically available for all items which can be saved there
		return true;
	}

	@Override
	public void setMasterView(final IMasterDiscoveryView masterView)
	{
		super.setMasterView(masterView);
		this.masterView = masterView;
	}

	protected IActionWithAvailability createSaveAction(final IContributedAction contributedAction, final ISelection selection, final ISearchContext searchContext, final List<ISearchFavoritesViewCustomization> sbCustomizations, final IDiscoveryEnvironment environment)
	{
		assert contributedAction instanceof SaveInFavoritesContributedAction;
		return new SaveInFavoritesMenuAction((SaveInFavoritesContributedAction) contributedAction, selection, searchContext, sbCustomizations, environment);
	}

	protected IViewCustomizationConfiguration viewCustomizationConfiguration()
	{
		return new ViewCustomizationConfiguration();
	}
}
