/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.integration.internal.slavecontrollers;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.core.api.ISearchConsoleMasterController;
import org.eclipse.platform.discovery.core.api.ISearchConsoleSlaveController;
import org.eclipse.platform.discovery.integration.internal.viewcustomization.SaveInFavoritesContributedAction;


public class SaveInFavoritesSlaveController implements ISearchConsoleSlaveController
{
	@Override
	public void setMasterController(ISearchConsoleMasterController masterController)
	{
	}

	@Override
	public Set<IContributedAction> createActions()
	{
		final Set<IContributedAction> actions = new HashSet<IContributedAction>();
		actions.add(new SaveInFavoritesContributedAction());
		return actions;
	}
}
