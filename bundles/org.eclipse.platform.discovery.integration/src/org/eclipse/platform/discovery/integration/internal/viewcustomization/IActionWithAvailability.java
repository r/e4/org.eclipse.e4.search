/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.integration.internal.viewcustomization;

import org.eclipse.jface.action.IAction;

/**
 * Extender of the {@link IAction} interface with availability flag
 * 
 * @author Danail Branekov
 */
public interface IActionWithAvailability extends IAction
{
	/**
	 * Checks whether the action is available under the current circumstances
	 * 
	 * @return true if the action is available, false otherwise
	 */
	public boolean isAvailable();
}
