/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.integration.internal.slavecontrollers;

import java.util.List;

import org.eclipse.core.runtime.Platform;
import org.eclipse.platform.discovery.core.api.ISearchConsoleSlaveController;
import org.eclipse.platform.discovery.core.api.ISearchFavoritesSlaveController;
import org.eclipse.platform.discovery.integration.internal.ViewCustXpParser;
import org.eclipse.platform.discovery.integration.internal.plugin.ISlaveControllersConfiguration;
import org.eclipse.platform.discovery.runtime.internal.xp.IContributionsReader;


public class SlaveControllersConfiguration implements ISlaveControllersConfiguration
{
	private static final String SLAVE_CONTROLLER_ELEMENT = "slavecontroller"; //$NON-NLS-1$;
	private static final String IMPL_CLASS_ATTRIBUTE = "implclass"; //$NON-NLS-1$
		
	protected IContributionsReader<ISearchConsoleSlaveController> seXpParser()
	{
		return new ViewCustXpParser<ISearchConsoleSlaveController>(Platform.getExtensionRegistry(),SEARCH_CONSOLE_XP_ID,SLAVE_CONTROLLER_ELEMENT)
		{
			@Override
			protected String implementationAttributeName()
			{
				return IMPL_CLASS_ATTRIBUTE;
			}
		};
	}

	protected IContributionsReader<ISearchFavoritesSlaveController> sbXpParser()
	{
		return new ViewCustXpParser<ISearchFavoritesSlaveController>(Platform.getExtensionRegistry(),SEARCH_FAVORITES_XP_ID, SLAVE_CONTROLLER_ELEMENT)
		{
			@Override
			protected String implementationAttributeName()
			{
				return IMPL_CLASS_ATTRIBUTE;
			}
		};
	}
	
	public List<ISearchConsoleSlaveController> availableSearchConsoleSlaveControllers()
	{
		return seXpParser().readContributions();
	}

	public List<ISearchFavoritesSlaveController> availableSearchFavoritesSlaveControllers()
	{
		return sbXpParser().readContributions();
	}
}
