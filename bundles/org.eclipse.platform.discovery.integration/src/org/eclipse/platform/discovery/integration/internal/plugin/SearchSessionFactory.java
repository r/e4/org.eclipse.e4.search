/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.integration.internal.plugin;

import org.eclipse.platform.discovery.core.api.ISearchContext;
import org.eclipse.platform.discovery.core.internal.ISearchSession;
import org.eclipse.platform.discovery.util.internal.session.HistoryTrack;
import org.eclipse.platform.discovery.util.internal.session.IHistoryTrack;
import org.eclipse.platform.discovery.util.internal.session.ISessionFactory;


public class SearchSessionFactory implements ISessionFactory<ISearchSession> {

	public ISearchSession newSession(final String id, final int historySize) {
		final HistoryTrack<ISearchContext> history = new HistoryTrack<ISearchContext>(historySize);
		return new ISearchSession()
		{

			public IHistoryTrack<ISearchContext> historyTrack() {
				return history;
			}

			public String getId() {
				return id;
			}
		};
	}
}
