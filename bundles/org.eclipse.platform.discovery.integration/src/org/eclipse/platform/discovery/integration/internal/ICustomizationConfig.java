/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.integration.internal;

public interface ICustomizationConfig
{
	/**
	 * The ID of the extension point for customizing the search console view
	 */
	public static final String SEARCH_CONSOLE_XP_ID = "org.eclipse.platform.discovery.integration.searchconsole"; //$NON-NLS-1$
	
	/**
	 * The ID of the extension point for customizing the search favorites view
	 */
	public static final String SEARCH_FAVORITES_XP_ID = "org.eclipse.platform.discovery.integration.searchfavorites"; //$NON-NLS-1$
}
