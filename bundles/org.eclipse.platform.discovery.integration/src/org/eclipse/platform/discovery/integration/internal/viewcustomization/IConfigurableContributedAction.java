/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.integration.internal.viewcustomization;

import org.eclipse.platform.discovery.core.api.IContributedAction;
import org.eclipse.platform.discovery.core.api.ISearchContext;
import org.eclipse.platform.discovery.util.api.env.IErrorHandler;


/**
 * Extender of {@link IContributedAction} which allows certain action configurations
 * 
 * @author Danail Branekov
 */
public interface IConfigurableContributedAction extends IContributedAction
{
	/**
	 * Sets the error handler for reporting errors
	 * 
	 * @param errorHandler
	 *            the error handler
	 */
	public void setErrorHandler(final IErrorHandler errorHandler);

	/**
	 * Sets the current search context
	 * 
	 * @param searchContext
	 *            the current search context
	 */
	public void setSearchContext(final ISearchContext searchContext);
}
