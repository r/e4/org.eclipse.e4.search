/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.plugin;

import org.eclipse.core.runtime.Plugin;
import org.eclipse.ui.IStartup;
import org.osgi.framework.BundleContext;

public class DiscoveryUtilPlugin extends Plugin implements IStartup
{
	// The plug-in ID
	public static final String PLUGIN_ID = "org.eclipse.platform.discovery.util"; //$NON-NLS-1$

	// The shared instance
	private static DiscoveryUtilPlugin plugin;
	
	/**
	 * The constructor
	 */
	public DiscoveryUtilPlugin() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.runtime.Plugins#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.runtime.Plugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static DiscoveryUtilPlugin getDefault() {
		return plugin;
	}

	public void earlyStartup()
	{
		// Nothing to do. However, this pugin has to start as early as possible since its services will be used for common logging
	}
}
