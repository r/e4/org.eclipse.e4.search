/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.property;

import java.util.List;

public interface IListProperty<T> extends IProperty<List<T>>, IAccessControl {
	
	public boolean registerCollectionChangedListener(IListPropertyListener<T> l, boolean current);
	
	public boolean removeCollectionChangedListener(IListPropertyListener<T> l);
	
	public Access getListAccess();
	
	public void setListAccess(Access attribute);

	public void registerListAccessListener(
			IPropertyAttributeListener<Access> listener, boolean current);

	public boolean removeListAccesssListener(
			IPropertyAttributeListener<Access> listener);
}
