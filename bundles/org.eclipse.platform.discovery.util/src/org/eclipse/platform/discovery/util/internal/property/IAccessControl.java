/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.property;

public interface IAccessControl {
	/**
	 * Obtains the current access state of the property
	 * @see Access
	 * @return The access state of the property. Cannot be null.
	 */
	public Access getAccess();
	
	/**
	 * Sets the access state of the property. Setting a new access state fires a notification.
	 * @param access - the new access state of the property.
	 * @throws NullPointerException - if <code>access</code> is null.
	 */
	public void setAccess(final Access access);

	/**
	 * Registers an listener to be notified for changes in the access state of the property. This method
	 * has no effect if the given instance was already registered.
	 * @param l - the listener to be registered.
	 * @param current - true if the listener has to be notified for the current access state, false otherwise
	 * @throws NullPointerException - if <code>l</code> is null.
	 */
	public void registerAccessListener(final IPropertyAttributeListener<Access> l, boolean current);
	
	/**
	 * Removes an already registered listener. If the listener has not been registered this method has
	 * no effect
	 * @param l - the listener to be removed
	 * @return true if the listener was removed, otherwise false i.e. it has not been previously registered
	 * @throws NullPointerException - if <code>l</code> is null
	 */
	public boolean removeAccessListener(final IPropertyAttributeListener<Access> l);
	
}
