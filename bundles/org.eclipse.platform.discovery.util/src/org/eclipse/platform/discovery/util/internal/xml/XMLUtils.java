/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.xml;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.CharArrayReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;

import org.eclipse.platform.discovery.util.internal.CharsetUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


public class XMLUtils implements IXMLUtils {

	public List<Element> findFirstLevelElements(Element elem) {
		final NodeList childNodes = elem.getChildNodes();
		final List<Element> ret = new ArrayList<Element>(childNodes.getLength());
		for (int i = 0; i < childNodes.getLength(); i++) {
			if (childNodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
				ret.add((Element) childNodes.item(i));
			}
		}
		return ret;
	}

	public String toXML(Element xmlElem) {
		try {
			return toXml(new DOMSource(xmlElem),encoding(xmlElem));
		} catch (TransformerException te)
		{
			// trivial transformation - no exception should happen as the
			// xml has already been parsed into dom objects and this
			// is the dom->string transformation.
			throw new RuntimeException(te);
		}
	}
	
	public String toXml(Source xmlSource, final String encoding) throws TransformerException
	{
		return toXml(xmlSource, Charset.forName(encoding));
	}
	
	@Override
	public String toXml(Source xmlSource, Charset charset) throws TransformerException {
		final ByteArrayOutputStream memBuf = new ByteArrayOutputStream();
		final Transformer serializer;
		try {
			serializer = TransformerFactory.newInstance().newTransformer();
		} catch (TransformerConfigurationException e) {
			//not much we could do to recover. It's rather environment problem
			throw new RuntimeException(e);
		}
		serializer.setOutputProperty(OutputKeys.INDENT, "yes"); //$NON-NLS-1$
		serializer.setOutputProperty(OutputKeys.ENCODING, charset.name());
		final StreamResult streamResult = new StreamResult(memBuf);
		serializer.transform(xmlSource, streamResult);
		
		InputStreamReader inputStreamReader = null;
		inputStreamReader = new InputStreamReader(new ByteArrayInputStream(memBuf.toByteArray()), charset); //$JL-I18N$
		
		final StringBuilder builder = new StringBuilder();
		final char[] transfBuf = new char[512];
		try {
			for (int r = inputStreamReader.read(transfBuf); r != -1; r = inputStreamReader
					.read(transfBuf)) {
				builder.append(transfBuf, 0, r);
			}
		} catch (IOException ioe) {
			// work only with memory buffers, no IOException should happen.
			throw new RuntimeException(ioe);
		}
		return builder.toString();
		// don't worry to close memBuf and isReader. memBuf is of type
		// ByteArrayOutputStream and the underlying input stream of isReader
		// is of type ByteArrayInputStream. Closing these streams has no
		// effect as they're both memory buffers.
	}
	
	public String encoding(Element xmlElem)
	{
		final String encoding = xmlElem.getOwnerDocument().getXmlEncoding();
		if (encoding == null)
		{
			//no encoding present, default to UTF-8
			return CharsetUtils.UTF_8; 
		}
		
		return encoding;
	}
	
	public Element bytes2RootElement(byte[] bytes) throws SAXException, IOException {
		DocumentBuilder builder = createDocumentBuilder();
		return builder.parse(new ByteArrayInputStream(bytes)).getDocumentElement();
	}

	public Element charsToRootElement (char[] chars) throws SAXException, IOException {
		DocumentBuilder builder = createDocumentBuilder();
		return builder.parse(new InputSource(new CharArrayReader(chars))).getDocumentElement();
	}

	protected DocumentBuilder createDocumentBuilder() {
		final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		dbf.setIgnoringElementContentWhitespace(true);
		DocumentBuilder builder;
		try {
			builder = dbf.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			throw new RuntimeException(e);
		}
		return builder;
	}
	
	public TransformerHandler createTransformerHandler(final Writer writer) throws TransformerConfigurationException
	{
		final PrintWriter pWriter = new PrintWriter(writer);
		final StreamResult streamResult = new StreamResult(pWriter);
		final SAXTransformerFactory tf = (SAXTransformerFactory) SAXTransformerFactory.newInstance();
		final TransformerHandler hd = tf.newTransformerHandler();
		final Transformer serializer = hd.getTransformer();
		serializer.setOutputProperty(OutputKeys.ENCODING, CharsetUtils.UTF_8);
		serializer.setOutputProperty(OutputKeys.STANDALONE, "yes"); //$NON-NLS-1$
		serializer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes"); //$NON-NLS-1$
		serializer.setOutputProperty(OutputKeys.METHOD, "xml"); //$NON-NLS-1$

		hd.setResult(streamResult);
		return hd;
	}

	@Override
	public List<Element> findFirstLevelElements(Element elem, String nsURI, String localName)
	{
		final List<Element> ret = new ArrayList<Element>();
		for(Element e : findFirstLevelElements(elem))
		{
			if(nsURI.equals(e.getNamespaceURI()) && localName.equals(e.getLocalName()))
			{
				ret.add(e);
			}
		}
		
		return ret;
	}

}
