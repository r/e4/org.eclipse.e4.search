/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.property;

import java.util.ListIterator;

public class AccessCheckingListIterator<T> implements ListIterator<T> {
	private final ListIterator<T> delegate;
	private final IListProperty<T> property;
	
	public AccessCheckingListIterator(IListProperty<T> property, ListIterator<T> delegate) {
		this.delegate = delegate;
		this.property = property;
	}

	public void add(T o) {
		assertCanWrite();
		this.delegate.add(o);
	}

	public boolean hasNext() {
		return delegate.hasNext();
	}

	public boolean hasPrevious() {
		return delegate.hasPrevious();
	}

	public T next() {
		return delegate.next();
	}

	public int nextIndex() {
		return delegate.nextIndex();
	}

	public T previous() {
		return delegate.previous();
	}

	public int previousIndex() {
		return delegate.previousIndex();
	}

	public void remove() {
		assertCanWrite();
		delegate.remove();

	}

	public void set(T o) {
		assertCanWrite();
		delegate.set(o);
	}
	
	private void assertCanWrite()
	{
		if (Access.READ_WRITE != property.getListAccess())
		{
			throw new UnsupportedOperationException(property.getListAccess().name());
		}
	}

}
