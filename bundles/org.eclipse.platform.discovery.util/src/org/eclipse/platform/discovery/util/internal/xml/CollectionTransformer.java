/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.xml;

import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;

import org.eclipse.core.runtime.IAdapterManager;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.platform.discovery.util.api.longop.ILongOperation;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.platform.discovery.util.api.longop.LongOpCanceledException;
import org.eclipse.platform.discovery.util.api.xml.IXmlRepresentable;
import org.eclipse.platform.discovery.util.internal.property.Property;


public class CollectionTransformer implements ICollectionTransformer {

	private String collectionTag;
	private String itemTag;
	private String namespace;
	
	@Override
	public String transform(final Iterator<?> i, final ILongOperationRunner lor) {
		try {
			final Property<Boolean> progressMonitorStarted = new Property<Boolean>();
			return lor.run(new ILongOperation<String>() {
				@Override
				public String run(IProgressMonitor monitor)
					throws LongOpCanceledException, Exception {
					try
					{
						progressMonitorStarted.set(false);
						final StringBuilder b = new StringBuilder();
						b.append('<');
						b.append(collectionTag);
						b.append(" xmlns=\""); //$NON-NLS-1$
						b.append(namespace);
						b.append("\">"); //$NON-NLS-1$
						while (i.hasNext()) {
							b.append('<');
							b.append(itemTag);
							b.append('>');
							b.append(adapt(i.next(), new TransformerSubProgressMonitor(monitor, 0, progressMonitorStarted)));
							b.append("</"); //$NON-NLS-1$
							b.append(itemTag);
							b.append('>');
						}
						b.append("</"); //$NON-NLS-1$
						b.append(collectionTag);
						b.append('>');
						return b.toString();
					}
					finally
					{
						if(progressMonitorStarted.get())
						{
							monitor.done();
						}
					}
				}

			});

		} catch (LongOpCanceledException loce) {
			throw new RuntimeException(loce);
		} catch (InvocationTargetException ite) {
			throw new RuntimeException(ite);
		}
	}

	private String adapt(Object o, IProgressMonitor pm) {
		if (o instanceof IXmlRepresentable) {
			return ((IXmlRepresentable) o).toStringXml(pm);
		}
		
		final IXmlRepresentable xml = (IXmlRepresentable)adapterFor(o, IXmlRepresentable.class);
		if (xml == null) {
			return o.toString();
		}
		return xml.toStringXml(pm);
	}
	
	private Object adapterFor(final Object object, final Class<?> targetClass)
	{
		final Object cachedAdapter = getAdapterManager().getAdapter(object, targetClass);
		return cachedAdapter == null ? getAdapterManager().loadAdapter(object, targetClass.getName()) : cachedAdapter;
	}

	protected IAdapterManager getAdapterManager()
	{
		return Platform.getAdapterManager();
	}
	
	@Override
	public void setCollectionTag(String s) {
		this.collectionTag = s;
	}

	@Override
	public void setItemTag(String s) {
		itemTag = s;

	}

	@Override
	public void setNamespace(String s) {
		namespace = s;
	}
	
	private class TransformerSubProgressMonitor extends SubProgressMonitor
	{
		private final IProgressMonitor parentMonitor;
		private final Property<Boolean> progressMonitorStarted;
		
		public TransformerSubProgressMonitor(final IProgressMonitor monitor, final int ticks, final Property<Boolean> progressMonitorStarted)
		{
			super(monitor, ticks);
			this.parentMonitor = monitor;
			this.progressMonitorStarted = progressMonitorStarted;
		}
		
		@Override
		public void beginTask(String name, int totalWork)
		{
			if(!progressMonitorStarted.get())
			{
				progressMonitorStarted.set(true);
				this.parentMonitor.beginTask(Messages.CollectionTransformer_TRANSFORMING_TO_XML, IProgressMonitor.UNKNOWN);
			}
			
			super.beginTask(name, totalWork);
		}
	}
}
