/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal;

import java.nio.charset.Charset;
import java.util.Arrays;

/** 
 * This utility class declares the names of the standard charsets which all JVMs support, and declares methods for obtaining references to charset objects
 * for the standard charsets.
 * This class exists because there is no such functionality in the JDK. If such is added in Java 7 or beyond, it should be deleted.
 * <p><i>Since this class deals only with static data, and there is no likelyhood anyone will extend it in another class, all methods are declared static.</i></p>
 * @author Dimitar Georgiev
 */
public class CharsetUtils {
	
	public static final String US_ASCII = "US-ASCII"; //$NON-NLS-1$
	public static final String ISO_8859_1 = "ISO-8859-1"; //$NON-NLS-1$
	public static final String UTF_8 = "UTF-8"; //$NON-NLS-1$
	public static final String UTF_16BE = "UTF-16BE"; //$NON-NLS-1$
	public static final String UTF_16LE = "UTF-16LE"; //$NON-NLS-1$
	public static final String UTF_16 = "UTF-16"; //$NON-NLS-1$
	
	/** Return an array of the standard charsets, which are required to be supported on all JVMs
	 *  @see Charset
	 **/
	public static final String[] getStandardCharsets() {
		return Arrays.copyOf(STANDARD_CHARSETS, STANDARD_CHARSETS.length);
	}
	
	public static Charset getUS_ASCII() {
		return Charset.forName(US_ASCII);
	}
	
	public static Charset getISO_8859_1() {
		return Charset.forName(ISO_8859_1);
	}
	
	public static Charset getUTF_8() {
		return Charset.forName(UTF_8);
	}
	
	public static Charset getUTF_16BE() {
		return Charset.forName(UTF_16BE);
	}
	
	public static Charset getUTF_16LE() {
		return Charset.forName(UTF_16LE);
	}
	
	public static Charset getUTF_16() {
		return Charset.forName(UTF_16);
	}
	
	public static boolean isStandardCharset(String charset) {
		//null is not a valid parameter, empty string is not a valid charset name
		ContractChecker.emptyStringCheckParam(charset, "charset"); //$NON-NLS-1$
		
		for(String aStandardCharset: STANDARD_CHARSETS) {
			if(aStandardCharset.equalsIgnoreCase(charset)) { // charset names are case-insensitive
				return true;
			}
		}
		return false;
	}
	
	private static final String[] STANDARD_CHARSETS = new String[]{US_ASCII, ISO_8859_1, UTF_8, UTF_16BE, UTF_16LE, UTF_16};

}
