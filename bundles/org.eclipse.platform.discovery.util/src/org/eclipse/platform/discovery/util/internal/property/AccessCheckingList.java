/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.property;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class AccessCheckingList<T> implements List<T>{

	private final List<T> delegate;
	private final IListProperty<T> property;

	public AccessCheckingList(List<T> delegate, IListProperty<T> property) {
		super();
		this.delegate = delegate;
		this.property = property;
	}
	
	private List<T> delegate()
	{
		return delegate;
	}
	

	@Override
	public boolean equals(Object obj) {
		return delegate().equals(obj);
	}

	@Override
	public int hashCode() {
		return delegate().hashCode();
	}
	
	private Access getListAccess()
	{
		return property.getListAccess();
	}
	
	public void add(int index, T element) {
		assertCanWrite();
		delegate().add(index, element);
	}

	public boolean add(T o) {
		assertCanWrite();
		return delegate().add(o);
	}

	public boolean addAll(Collection<? extends T> c) {
		assertCanWrite();
		return delegate().addAll(c);
	}

	public boolean addAll(int index, Collection<? extends T> c) {
		assertCanWrite();
		return delegate().addAll(index, c);
	}

	public void clear() {
		assertCanWrite();
		delegate().clear();
	}

	public boolean contains(Object o) {
		return delegate().contains(o);
	}

	public boolean containsAll(Collection<?> c) {
		return delegate().containsAll(c);
	}

	public T get(int index) {
		return delegate().get(index);
	}

	public int indexOf(Object o) {
		return delegate().indexOf(o);
	}

	public boolean isEmpty() {
		return delegate().isEmpty();
	}

	public Iterator<T> iterator() {
		return listIterator();
	}

	public int lastIndexOf(Object o) {
		return delegate().lastIndexOf(o);
	}

	public ListIterator<T> listIterator() {
		return new AccessCheckingListIterator<T>(property, delegate().listIterator());
	}

	public ListIterator<T> listIterator(int index) {
		return new AccessCheckingListIterator<T>(property, delegate().listIterator(index));
	}

	public T remove(int index) {
		assertCanWrite();
		return delegate().remove(index);
	}

	public boolean remove(Object o) {
		assertCanWrite();
		return delegate().remove(o);
	}

	public boolean removeAll(Collection<?> c) {
		assertCanWrite();
		return delegate().removeAll(c);
	}

	public boolean retainAll(Collection<?> c) {
		assertCanWrite();
		return delegate().retainAll(c);
	}

	public T set(int index, T element) {
		assertCanWrite();
		return delegate().set(index, element);
	}

	public int size() {
		return delegate().size();
	}

	public List<T> subList(int fromIndex, int toIndex) {
		return delegate().subList(fromIndex, toIndex);
	}

	public Object[] toArray() {
		return delegate().toArray();
	}

	public <E> E[] toArray(E[] a) {
		return delegate().toArray(a);
	}
	
	private void assertCanWrite()
	{
		if (Access.READ_WRITE != getListAccess())
		{
			throw new UnsupportedOperationException(getListAccess().name());
		}
	}


}
