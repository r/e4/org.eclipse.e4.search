/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.api.env;



/**
 * An error handler
 * @author Danail Branekov
 */
public interface IErrorHandler
{
	/**
	 * Handles an exception generically. Implementation will log an error and will display an error dialog with a default title
	 * @param exc exception to handle
	 */
	public void handleException(final Exception exc);
	
	/**
	 * Handles an exception generically. Implementation will log an error and will display an error dialog with the title passed
	 * @param exc exception to handle
	 */
	public void handleException(final String title, final Exception exc);
	
	/**
	 * Shows the message specified as an error
	 * 
	 * @param title
	 *            the title to be used. If null, default title will be used
	 * @param errorMessage
	 *            the error message
	 */
	public void showError(final String title, final String errorMessage);

}
