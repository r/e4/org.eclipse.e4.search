/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.longop;

import java.lang.reflect.InvocationTargetException;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.platform.discovery.util.api.longop.ILongOperation;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.platform.discovery.util.api.longop.LongOpCanceledException;


public abstract class SingleForkThreadRunner implements ILongOperationRunner {
	
	private final Set<Thread> threadTracker;
	private final IProgressMonitor monitor;
	
	public SingleForkThreadRunner(IProgressMonitor monitor, Set<Thread> threadTracker)
	{
		this.monitor = monitor;
		this.threadTracker = threadTracker;
	}

	public abstract <T> T runInNewThread(ILongOperation<T> op) throws LongOpCanceledException, InvocationTargetException;

	public <T> T run(final ILongOperation<T> op) throws LongOpCanceledException, InvocationTargetException {
		if (shouldRunInSameThread())
		{
			return runInSameThread(op);
		} else {
			return runInNewThread(new ThreadTrackingDecorator<T>(op));
		}
	}
	
	protected boolean shouldRunInSameThread()
	{
		return threadTracker.contains(Thread.currentThread());
	}
	
	protected Thread currentThread()
	{
		return Thread.currentThread();
	}
	
	private <T> T runInSameThread(final ILongOperation<T> op) throws LongOpCanceledException, InvocationTargetException
	{
		try {
			return op.run(new SubProgressMonitor(monitor, 0));
		} catch (Exception  e) {
			throw ExceptionHelper.rethrow(e);
		}
	}
	
	private class ThreadTrackingDecorator<T> implements ILongOperation<T>
	{
		private final ILongOperation<T> target;

		public ThreadTrackingDecorator(ILongOperation<T> target)
		{
			this.target = target;
		}
		
		@Override
		public T run(IProgressMonitor monitor) throws LongOpCanceledException,
				Exception {
			threadTracker.add(currentThread());
			try {
				return target.run(monitor);
			} finally {
				threadTracker.remove(currentThread());
			}
		}
		
	}
}
