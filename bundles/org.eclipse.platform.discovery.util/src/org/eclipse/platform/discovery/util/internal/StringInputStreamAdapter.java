/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

/**
 * Adapter class that uses a StringReader to organize an InputStream
 * 
 * @author Danail-B
 * 
 */
public class StringInputStreamAdapter extends InputStream {
	private StringReader reader;

	/**
	 * Constructor
	 * 
	 * @param string
	 * @throws NullPointerException
	 *             when <code>string</code> is null
	 */
	public StringInputStreamAdapter(String string) {
		if (string == null) {
		}

		reader = new StringReader(string);
	}

	@Override
	public int read() throws IOException {
		return reader.read();
	}
}
