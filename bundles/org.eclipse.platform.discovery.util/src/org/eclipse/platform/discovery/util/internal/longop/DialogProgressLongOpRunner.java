/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.longop;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.platform.discovery.util.api.longop.ILongOperation;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.platform.discovery.util.api.longop.LongOpCanceledException;
import org.eclipse.swt.widgets.Shell;


public class DialogProgressLongOpRunner implements ILongOperationRunner {
	private Shell shell;

	public DialogProgressLongOpRunner(Shell activeShell) {
		this.shell = activeShell;
	}

	public <T> T run(ILongOperation<T> op) throws LongOpCanceledException,
			InvocationTargetException {
		final ProgressMonitorDialog dialog = new ProgressMonitorDialog(shell);
		final InDialogRunner runner = new InDialogRunner(dialog, null);
		return runner.run(op);
	}

	private class InDialogRunner extends ModalContextLongOpRunner {
		private final ProgressMonitorDialog dialog;

		public InDialogRunner(ProgressMonitorDialog dialog, ISchedulingRule rule) {
			super(dialog.getProgressMonitor(), rule);
			this.dialog = dialog;
		}

		@Override
		protected void runInModalContext(IRunnableWithProgress rp,
				IProgressMonitor m) throws InvocationTargetException,
				InterruptedException {
			dialog.run(true, false, rp);
		}
	}
}
