/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.api.longop;

import org.eclipse.core.runtime.IProgressMonitor;

public interface ILongOperation<T> {
	public T run(IProgressMonitor monitor) throws LongOpCanceledException, Exception;
	
}
