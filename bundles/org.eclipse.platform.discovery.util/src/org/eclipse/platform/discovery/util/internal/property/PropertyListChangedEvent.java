/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.property;

import java.util.Collection;

public class PropertyListChangedEvent<T> {

	public enum ChangeKind {ELEMENT_ADDED, ELEMENT_REMOVED}
	private final IListProperty<T> notifier;
	private final ChangeKind kind;
	private final Collection<? extends T> elements;
	
	public PropertyListChangedEvent(final IListProperty<T> notifier, final ChangeKind kind, final Collection<? extends T> elements)
	{
		this.notifier = notifier;
		this.kind = kind;
		this.elements = elements;
	}
	
	public ChangeKind getChangeKind()
	{
		return kind;
	}
	
	public Collection<? extends T> getElements()
	{
		return elements;
	}

	public IListProperty<T> getNotifier() {
		return notifier;
	}
}
