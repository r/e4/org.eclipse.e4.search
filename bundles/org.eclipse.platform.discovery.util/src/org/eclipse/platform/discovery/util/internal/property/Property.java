/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.property;


public class Property<T> implements IProperty<T> {
	
	public void registerValueListener(IPropertyAttributeListener<T> l,
			boolean current) {
		value.registerListener(l, current);
	}

	public boolean removeValueListener(IPropertyAttributeListener<T> l) {
		return value.removeListener(l);
	}

	private final PropertyAttribute<Access> access = new PropertyAttribute<Access>(Access.READ_WRITE, this);
	private final PropertyAttribute<T> value = new PropertyAttribute<T>(null, this);
	
	public Access getAccess() {
		return access.get();
	}

	public void setAccess(Access access) {
		if (access == null)
		{
			throw new NullPointerException();
		}
		this.access.set(access);
	}

	public void registerAccessListener(
			IPropertyAttributeListener<Access> l,
			boolean current) {
		access.registerListener(l, current);
	}

	public boolean removeAccessListener(
			IPropertyAttributeListener<Access> l) {
		return access.removeListener(l);
	}


	
	public void set(T value) {
		if (!access.get().equals(Access.READ_WRITE)){
			throw new UnsupportedOperationException();
		}
		this.value.set(value);
		
	}
	
	public T get() {
		return value.get();
	}
}
