/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.api.env;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;


/**
 * Interface for describing common environment
 * @author Danail Branekov
 */
public interface IDiscoveryEnvironment
{
	/**
	 * Retrieves an long operation runner instance
	 * 
	 * @return {@link ILongOperationRunner} instance
	 */
	public ILongOperationRunner operationRunner();

	/**
	 * Retrieves an error handler
	 * 
	 * @return {@link IErrorHandler} instance
	 */
	public IErrorHandler errorHandler();

	/**
	 * Retrieves a progress monitor
	 */
	public IProgressMonitor progressMonitor();
}
