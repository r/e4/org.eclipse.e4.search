/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.property;

/**
 * A property is like java field. However it offer more services then just the ordinary read and write.
 * A property could have several read/write states, listed in the {@link Access} enumeration. Properties
 * are capable of notifying interested parties of changes in their access state and values.
 * @param T - the type of the value stored in this property
 * @see Access
 * @see IAccessListener
 * @see IValueListener
 * @author Hristo Sabev
 *
 */
public interface IProperty<T> extends IAccessControl {


	/**
	 * Sets the new value of this property. This method would trigger notification in case that the passed value
	 * was different then the current value.
	 * @param value - the new value of this property
	 * @throws IllegalStateException - if the state is <code>Access.READ_ONLY</code> or <code>Access.DISABLED</code>
	 */
	public void set(final T value);
	
	/**
	 * Obtains the current value of the property.
	 * @return the current value of the property. The returned value might be null if the property has not been
	 * initialized before calling <code>get()</code>
	 * @throws IllegalStateException - if the state is <code>Access.DISABLED</code>
	 */
	public T get();
	
	public void registerValueListener(final IPropertyAttributeListener<T> l, boolean current);
	
	public boolean removeValueListener(final IPropertyAttributeListener<T> l);
}
