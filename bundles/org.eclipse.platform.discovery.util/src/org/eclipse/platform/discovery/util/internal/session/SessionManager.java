/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.session;

import java.util.IdentityHashMap;
import java.util.Map;

public class SessionManager<T extends ISession<?>> implements ISessionManager<T>
{
	private final ISessionFactory<T> factory;
	private Map<String, T> sessions;
	
	public SessionManager(ISessionFactory<T> factory) {
		this.factory = factory;
		this.sessions = new IdentityHashMap<String, T>();
	}

	@Override
	public T session(String sessionId, int historySize) {
		final T foundSession = sessions.get(sessionId);
		if (foundSession != null)
		{
			return foundSession;
		} else
		{
			final T newSession = factory.newSession(sessionId, historySize);
			sessions.put(sessionId, newSession);
			return newSession;
		}
	}

	@Override
	public int sessionCount() {
		return sessions.size();
	}
}
