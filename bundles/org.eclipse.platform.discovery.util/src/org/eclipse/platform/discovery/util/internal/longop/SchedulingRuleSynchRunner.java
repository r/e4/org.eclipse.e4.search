/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.longop;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.jobs.IJobManager;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.platform.discovery.util.api.longop.ILongOperation;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.platform.discovery.util.api.longop.LongOpCanceledException;


public class SchedulingRuleSynchRunner implements ILongOperationRunner {

	private final ILongOperationRunner wrappedRunner;
	private final ISchedulingRule rule;
	private final IJobManager jm;
	
	public SchedulingRuleSynchRunner(ILongOperationRunner wrapped,
			ISchedulingRule rule, IJobManager jobManager) {
		this.wrappedRunner = wrapped;
		this.rule = rule;
		this.jm = jobManager;
	}

	protected IJobManager jobManager()
	{
		return jm;
	}
	
	@Override
	public <T> T run(ILongOperation<T> op) throws LongOpCanceledException,
			InvocationTargetException {
		return wrappedRunner.run(new RuleSynchronizingDecorator<T>(op));
	}
	
	private class RuleSynchronizingDecorator<T> implements ILongOperation<T>
	{
		private final ILongOperation<T> wrappedOp;
		public RuleSynchronizingDecorator(ILongOperation<T> op)
		{
			wrappedOp = op;
		}
		@Override
		public T run(IProgressMonitor monitor) throws LongOpCanceledException,
				Exception {
			jobManager().beginRule(rule, monitor);
			try {
				return wrappedOp.run(monitor);
			} finally {
				jobManager().endRule(rule);
			}
		}
	}
}
