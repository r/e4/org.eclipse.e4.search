/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class PluginXmlUtils extends XMLUtils implements IPluginXmlUtils{

	@Override
	public Element createPluginSnippet(String extensionPointID) {
		Document factory = createDocumentBuilder().newDocument();
		Element extension = factory.createElement(EXTENSION_ELEMENT_NAME);
		extension.setAttribute(POINT_ATTRIBUTE_NAME, extensionPointID);
		
		Element plugin = factory.createElement(PLUGIN_ELEMENT_NAME);
		plugin.appendChild(extension);
		
		return plugin;
	}

}
