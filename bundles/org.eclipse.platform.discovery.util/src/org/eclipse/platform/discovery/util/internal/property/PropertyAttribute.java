/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.property;

import java.util.Collection;
import java.util.LinkedList;

class PropertyAttribute<A> {
	
	private Collection<IPropertyAttributeListener<A>> attributeListeners = new LinkedList<IPropertyAttributeListener<A>>();
	
	private A attributeValue;
	private final IProperty<?> parentProperty;
	
	public PropertyAttribute(A attributeValue, IProperty<?> parentProperty) {
		if (parentProperty == null)
		{
			throw new NullPointerException();
		}
		this.attributeValue = attributeValue;
		this.parentProperty = parentProperty;
	}
	
	public A get() {
		return attributeValue;
	}

	public void set(final A attributeValue) {
		if (attributeValue == get())
		{
			return;
		} else if (attributeValue != null && attributeValue.equals(get()))
		{
			return;
		}

		// Create the event with both old and new values
		final PropertyAttributeChangedEvent<A> ace = new PropertyAttributeChangedEvent<A>(parentProperty, get(), attributeValue);
		
		// Change the value
		this.attributeValue = attributeValue;
		
		// Notify change listeners
		for (IPropertyAttributeListener<A> l : attributeListeners)
		{
			l.attributeChanged(ace);
		}
	}
	
	public void registerListener(final IPropertyAttributeListener<A> l, boolean current) {
		if (current)
		{
			l.attributeChanged(new PropertyAttributeChangedEvent<A>(parentProperty, get(), get()));
		}
		attributeListeners.add(l);
		
	}

	public boolean removeListener(final IPropertyAttributeListener<A> l) {
		return attributeListeners.remove(l);
	}

	

	
}
