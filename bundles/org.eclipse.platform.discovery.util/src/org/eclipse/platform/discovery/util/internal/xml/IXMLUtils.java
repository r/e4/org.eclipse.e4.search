/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.xml;

import java.io.IOException;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import java.util.List;

import javax.xml.transform.Source;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.sax.TransformerHandler;

import org.w3c.dom.Element;
import org.xml.sax.SAXException;


public interface IXMLUtils {
	
	/**
	 * Finds elements which are direct children of the element specified
	 */
	public List<Element> findFirstLevelElements(Element elem);
	
	/**
	 * Finds elements which are direct children of the element specified and have the specified namespace and local part
	 * 
	 * @param elem
	 *            the element
	 * @param nsURI
	 *            the namespace of the element to search for; must not be null
	 * @param localName
	 *            the local name; must not be null
	 * @return a (possible empty) list of elements
	 * 
	 */
	public List<Element> findFirstLevelElements(Element elem, final String nsURI, final String localName);
	
	public String toXML(Element el);
	
	/**
	 * 
	 * @param xmlSource
	 * @param encoding
	 * @return The String representation of this xml source
	 * @throws TransformerException
	 * @throws UnsupportedCharsetException - No support for this encoding on the JVM
	 * @throws IllegalCharsetNameException - see {@link Charset} javadoc
	 * @throws IllegalArgumentException - encoding is null or empty string
	 */
	public String toXml(Source xmlSource, String encoding) throws TransformerException, UnsupportedCharsetException, IllegalCharsetNameException, IllegalArgumentException;
	
	/**
	 * 
	 * @param xmlSource
	 * @param charset
	 * @return The String representation of this xml source
	 * @throws TransformerException
	 */
	public String toXml(Source xmlSource, Charset charset) throws TransformerException;
	
	/**
	 * @param xmlElem
	 * @return The encoding of the XML element, with a fallback to UTF-8 if not present inside the Document. If present, it is up to the client to check whether the returned
	 * encoding is supported on this JVM.
	 */
	public String encoding(Element xmlElem);
	
	/**Convert the byte array to a XML root element*/
	public Element bytes2RootElement(byte[] bytes) throws SAXException, IOException;
	
	/**Convert the char array to a XML root element*/
	public Element charsToRootElement(char[] chars) throws SAXException, IOException;
	
	/**
	 * Creates a new {@link TransformerHandler} instance which will write content to the writer specified. The transformer handler serializer has the following properties set:
	 * <ul>
	 * <li>OutputKeys.ENCODING = "UTF-8");</li>
	 * <li>OutputKeys.STANDALONE = "yes");</li>
	 * <li>OutputKeys.OMIT_XML_DECLARATION = "yes");</li>
	 * <li>OutputKeys.METHOD = "xml");</li> </ui>
	 * 
	 * @param writer
	 * @return
	 * @throws TransformerConfigurationException
	 */
	public TransformerHandler createTransformerHandler(final Writer writer) throws TransformerConfigurationException;
}
