/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.longop;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.platform.discovery.util.api.longop.ILongOperation;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.platform.discovery.util.api.longop.LongOpCanceledException;


/**
 * Implementation of the {@link ILongOperationRunner} interface which executes the operation in the current thread
 * @author Danail Branekov
 */
public class CurrentThreadOperationRunner implements ILongOperationRunner
{
	private final IProgressMonitor progressMonitor;

	public CurrentThreadOperationRunner(final IProgressMonitor progressMonitor)
	{
		this.progressMonitor = progressMonitor;
	}
	
	@Override
	public <T> T run(ILongOperation<T> op) throws LongOpCanceledException, InvocationTargetException
	{
		try
		{
			return op.run(progressMonitor);
		} 
		catch(LongOpCanceledException e)
		{
			throw e;
		}
		catch(RuntimeException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new InvocationTargetException(e);
		}
	}

}
