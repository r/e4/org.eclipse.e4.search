/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.property;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.eclipse.platform.discovery.util.internal.property.PropertyListChangedEvent.ChangeKind;


public class NotifyingList<T> implements List<T>{

	private final ListChangeListenerMaintainer<T> eventing;
	
	public <E> E[] toArray(E[] a) {
		return delegate().toArray(a);
	}

	private final List<T> delegate;

	public NotifyingList(List<T> delegate, ListChangeListenerMaintainer<T> eventing) {
		this.delegate = delegate;
		this.eventing = eventing;
	}
	
	private List<T> delegate()
	{
		return delegate;
	}
	

	@Override
	public boolean equals(Object obj) {
		return delegate().equals(obj);
	}

	@Override
	public int hashCode() {
		return delegate().hashCode();
	}


	
	public void add(int index, T element) {
		delegate().add(index, element);
		eventing.fireSingleEvent(ChangeKind.ELEMENT_ADDED, element);
	}

	public boolean add(T o) {
		final boolean res = delegate().add(o);
		eventing.fireSingleEvent(ChangeKind.ELEMENT_ADDED, o);
		return res;
	}

	public boolean addAll(Collection<? extends T> c) {
		final boolean res = delegate().addAll(c);
		eventing.fireEvent(ChangeKind.ELEMENT_ADDED, c);
		return res;
	}

	public boolean addAll(int index, Collection<? extends T> c) {
		final boolean res = delegate().addAll(index, c);
		eventing.fireEvent(ChangeKind.ELEMENT_ADDED, c);
		return res;
	}


	public void clear() {
		final List<T> cleared = new ArrayList<T>(delegate());
		delegate().clear();
		eventing.fireEvent(ChangeKind.ELEMENT_REMOVED, cleared);
	}

	public boolean contains(Object o) {
		return delegate().contains(o);
	}

	public boolean containsAll(Collection<?> c) {
		return delegate().containsAll(c);
	}

	public T get(int index) {
		return delegate().get(index);
	}

	public int indexOf(Object o) {
		return delegate().indexOf(o);
	}

	public int lastIndexOf(Object o) {
		return delegate().lastIndexOf(o);
	}
	
	public boolean isEmpty() {
		return delegate().isEmpty();
	}

	public Iterator<T> iterator() {
		return listIterator();
	}

	public ListIterator<T> listIterator() {
		return new NotifyingListIterator(delegate().listIterator());
	}

	public ListIterator<T> listIterator(int index) {
		return new NotifyingListIterator(delegate().listIterator(index));
	}

	public T remove(int index) {
		final T res = delegate().remove(index);
		eventing.fireSingleEvent(ChangeKind.ELEMENT_REMOVED, delegate().get(index));
		return res;
	}

	public boolean remove(Object o) {
		boolean res = false;
		if (delegate().contains(o))
		{
			@SuppressWarnings("unchecked")
			final T toRemove = (T)o;
			res = delegate().remove(o);
			eventing.fireSingleEvent(ChangeKind.ELEMENT_REMOVED,toRemove);
		}
		return res;
	}

	public boolean removeAll(Collection<?> c) {
		final Collection<T> copy = new ArrayList<T>(delegate());
		copy.retainAll(c);
		final boolean res = delegate.removeAll(c);
		eventing.fireEvent(ChangeKind.ELEMENT_REMOVED, copy);
		return res;
	}

	public boolean retainAll(Collection<?> c) {
		final Collection<T> copy = new ArrayList<T>(delegate());
		copy.removeAll(c);
		final boolean res = delegate().retainAll(c);
		eventing.fireEvent(ChangeKind.ELEMENT_REMOVED, copy);
		return res;
	}

	public T set(int index, T element) {
		final T res = delegate().set(index, element);
		eventing.fireSingleEvent(ChangeKind.ELEMENT_REMOVED, res);
		eventing.fireSingleEvent(ChangeKind.ELEMENT_ADDED, element);
		return res;
	}

	public int size() {
		return delegate().size();
	}

	public List<T> subList(int fromIndex, int toIndex) {
		return delegate().subList(fromIndex, toIndex);
	}

	public Object[] toArray() {
		return delegate().toArray();
	}

	class NotifyingListIterator implements ListIterator<T>
	{
		private final ListIterator<T> delegate;
		private T currentElement;
		public NotifyingListIterator(ListIterator<T> delegate)
		{
			this.delegate = delegate;
		}
		
		public void add(T o)
		{
			delegate.add(o);
			eventing.fireSingleEvent(ChangeKind.ELEMENT_ADDED, o);
		}

		public boolean hasNext() {
			return delegate.hasNext();
		}

		public boolean hasPrevious() {
			return delegate.hasPrevious();
		}

		public T next() {
			return currentElement = delegate.next();
		}

		public int nextIndex() {
			return delegate.nextIndex();
		}

		public T previous() {
			return currentElement = delegate.previous();
		}

		public int previousIndex() {
			return delegate.previousIndex();
		}

		public void remove() {
			delegate.remove();
			eventing.fireSingleEvent(ChangeKind.ELEMENT_REMOVED, currentElement);
		}

		public void set(T o) {
			final T prevCurrent = currentElement;
			delegate.set(o);
			currentElement = o;
			eventing.fireSingleEvent(ChangeKind.ELEMENT_REMOVED, prevCurrent);
			eventing.fireSingleEvent(ChangeKind.ELEMENT_ADDED, o);
		}
	}
}
