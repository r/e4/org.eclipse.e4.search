/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.xml;

import org.w3c.dom.Element;

public interface IPluginXmlUtils extends IXMLUtils{
	
	String PLUGIN_ELEMENT_NAME = "plugin"; //$NON-NLS-1$
	String EXTENSION_ELEMENT_NAME = "extension"; //$NON-NLS-1$
	String POINT_ATTRIBUTE_NAME = "point"; //$NON-NLS-1$
	
	/**
	 * For given extensionPointId, generates the following snippet:
	 * 
	 * <plugin>
	 * <extension point=$extensionPointId>
	 * </extension>
	 * </plugin>
	 * 
	 * Returns the <b>root</b> element created. The extension element can be obtained by calling <code>(Element)plugin.getFirstChild()</code>
	 */
	 Element createPluginSnippet(String extensionPointID);
}
