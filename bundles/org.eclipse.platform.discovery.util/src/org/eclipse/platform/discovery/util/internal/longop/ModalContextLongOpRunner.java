/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.longop;

import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.operation.ModalContext;
import org.eclipse.platform.discovery.util.api.longop.ILongOperation;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.platform.discovery.util.api.longop.LongOpCanceledException;
import org.eclipse.swt.widgets.Display;


public class ModalContextLongOpRunner implements ILongOperationRunner {

	private final IProgressMonitor monitor;
	private final ISchedulingRule rule;
	private final Set<Thread> threadTracker;
	
	public ModalContextLongOpRunner(IProgressMonitor monitor, ISchedulingRule rule)
	{
		this.monitor = monitor;
		this.rule = rule;
		threadTracker = newThreadTracker();
	}
	
	@Override
	public <T> T run(ILongOperation<T> op) throws LongOpCanceledException,
			InvocationTargetException {

		return newSchedulingRuleSynchRunner(new ModalThreadForker(monitor), rule).run(op);
	}
	
	protected ILongOperationRunner newSchedulingRuleSynchRunner(ILongOperationRunner wrapped, ISchedulingRule rule)
	{
		return new SchedulingRuleSynchRunner(wrapped, rule, Job.getJobManager());
	}
	
	private class ModalThreadForker extends SingleForkThreadRunner
	{

		public ModalThreadForker(IProgressMonitor monitor) {
			super(monitor, threadTracker);
		}
		
		@Override
		protected boolean shouldRunInSameThread()
		{
			// Does not fork if already running in modal context
			return ModalContext.isModalContextThread(currentThread()) || super.shouldRunInSameThread();
		}
		
		@Override
		public <T> T runInNewThread(ILongOperation<T> op)
				throws LongOpCanceledException, InvocationTargetException {
			@SuppressWarnings("unchecked")
			final T[] resHolder = (T[])new Object[1];
			try {
				runInModalContext(longOpToRunnable(resHolder, op), monitor);
			} catch (InterruptedException e) {
				//Should not happen. The way for the operation to signal that it has been
				//canceled is to throw LongOpCanceledException. Any interrupted exception
				//thrown by it will be processed as any other checked exception i.e. it will
				//be wrapped by InvocationTargetException
				throw new RuntimeException(e);
			} catch (InvocationTargetException e)
			{
				if (e.getCause() instanceof Error)
				{
					throw (Error)e.getCause();
				}
				if (e.getCause() instanceof RuntimeException)
				{
					throw (RuntimeException)e.getCause();
				}
				if (e.getCause() instanceof LongOpCanceledException)
				{
					throw (LongOpCanceledException)e.getCause();
				}
				throw e;
			}
			return resHolder[0];	

		}
	}
	
	protected void runInModalContext(IRunnableWithProgress rp, IProgressMonitor m) throws InvocationTargetException, InterruptedException
	{
		ModalContext.run(rp, true, m, Display.getDefault());
	}
	
	protected Set<Thread> newThreadTracker() {
		return new HashSet<Thread>();
	}

	private <T> IRunnableWithProgress longOpToRunnable(final T[] resHolder, final ILongOperation<T> op)
	{
		return new IRunnableWithProgress(){

			public void run(IProgressMonitor monitor)
					throws InvocationTargetException {
				try {
					resHolder[0] = op.run(monitor);
				}
				catch (Exception e)
				{
					throw new InvocationTargetException(e);
				}
			}
		};
	}
}

