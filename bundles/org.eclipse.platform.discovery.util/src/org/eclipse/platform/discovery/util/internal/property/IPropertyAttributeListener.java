/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.property;

public interface IPropertyAttributeListener<A>
{
	/**
	 * Invoked when the access state of the property in which this listener had been registered has changed.
	 * The listener get notified after the new access has already been set
	 * @param ace - an event object carrying details about the access state change
	 */
	public void attributeChanged(PropertyAttributeChangedEvent<A> event);
}
