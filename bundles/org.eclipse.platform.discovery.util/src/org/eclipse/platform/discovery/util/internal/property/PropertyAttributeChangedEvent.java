/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.property;


/**
 * 
 * @author Hristo Sabev
 * @see IProperty
 * @see IAccessListener
 */
public class PropertyAttributeChangedEvent <A> {
	private final A oldAttribute;
	private final A newAttribute;
	private final boolean current;
	private final Object notifier;
	
	/**
	 * Creates a new event with the given access change details. Typically these events are sent when
	 * there's a change in the state of the property. In such case <code>getOldAccess</code> shall be
	 * different then <code>getCurrentAccess</code>. However if the event was sent as a first notification
	 * to a listener wishing to receive the current state then the <code>getCurrentState</code> and
	 * <code>getOldState</code> shall be equal, while current shall be true.
	 * @param oldAccess - the old access 
	 * @param currentAccess - the current access state
	 * @param notifier - the notifying property object
	 * @throws NullPointerException - if <code>notifier</code> is null
	 * @throws IllegalArgumentException - if <code>oldAttribute</code> was equal to <code>newAttribute</code>
	 * but <code>current</code> was false or the opposite.
	 */
	public PropertyAttributeChangedEvent(Object notifier, A oldAttribute, A newAttribute)
	{
		if (notifier == null)
		{
			throw new NullPointerException();
		}
		
		if (oldAttribute == null && newAttribute == null)
		{
			current = true;
		} else if (oldAttribute == null && newAttribute != null)
		{
			current = false;
		} else if (oldAttribute != null && newAttribute == null)
		{
			current = false;
		} else if (oldAttribute.equals(newAttribute))
		{
			current = true;
		} else
		{
			current = false;
		}
		this.oldAttribute = oldAttribute;
		this.newAttribute = newAttribute;
		this.notifier = notifier;
	}
	
	public A getOldAttribute()
	{
		return oldAttribute;
	}
	
	public A getNewAttribute()
	{
		return newAttribute;
	}
	
	public boolean isCurrent()
	{
		return current;
	}

	public Object getNotifier() {
		return notifier;
	}
	
}
