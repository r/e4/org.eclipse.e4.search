/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.session;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

public class HistoryTrack<T> implements IHistoryTrack<T> {
	private int pos = -1;
	private final int limit;
	private List<T> history;
	
	public HistoryTrack(int limit) {
		if (limit < 1)
		{
			throw new IllegalArgumentException();
		}
		history = new ArrayList<T>(limit);
		this.limit = limit;
	}

	@Override
	public void track(T object) {
		clearHistoryFromCurrent();
		if (pos + 1 == limit)
		{
			copyByOneToLeft();
			history.set(pos, object);
		} else
		{
			history.add(++pos, object);
		}
	}

	private void clearHistoryFromCurrent()
	{
		for (int i = history.size() - 1; i > pos; i--)
		{
			history.remove(i);
		}
	}
	
	private void copyByOneToLeft()
	{
		if (limit < 2 )
		{
			//this means that the limit is 1 (it can't be 0).
			//In such a case don't do anything the new element
			//will be added on postion 0, overriding the existing
			//content
		}
		else	
		{
			for (int i = 1; i < history.size(); i++)
			{
				history.set(i -1, history.get(i));
			}
		}
	}

	@Override
	public T last() {
		assertNotEmpty();
		return history.get(history.size() - 1);
	}

	@Override
	public T current() {
		assertNotEmpty();
		return history.get(pos);
	}

	@Override
	public List<T> asList() {
		return Collections.unmodifiableList(history);
	}

	@Override
	public T first() {
		assertNotEmpty();
		return history.get(0);
	}

	@Override
	public boolean hasNext() {
		return pos < history.size() - 1;
	}

	@Override
	public boolean hasPrevious() {
		return pos > 0;
	}

	@Override
	public boolean isEmpty() {
		return history.size() == 0;
	}

	@Override
	public T next() {
		if (!hasNext())
		{
			throw new NoSuchElementException();
		}
		return history.get(++pos);
	}

	@Override
	public T previous() {
		if (!hasPrevious())
		{
			throw new NoSuchElementException();
		}
		return history.get(--pos);
	}

	private void assertNotEmpty()
	{
		if (isEmpty())
		{
			throw new NoSuchElementException();
		}
	}

	@Override
	public int historyLimit()
	{
		return this.limit;
	}
}
