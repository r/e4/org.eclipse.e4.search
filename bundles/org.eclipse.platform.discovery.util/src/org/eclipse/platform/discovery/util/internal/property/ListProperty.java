/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.util.internal.property;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.platform.discovery.util.internal.property.PropertyListChangedEvent.ChangeKind;


public class ListProperty<T> extends Property<List<T>> implements IListProperty<T>
{

	private final Set<IListPropertyListener<T>> listeners = new HashSet<IListPropertyListener<T>>();
	
	private final PropertyAttribute<Access> listAccess = new PropertyAttribute<Access>(Access.READ_WRITE, this);

	private final ListChangeListenerMaintainer<T> eventing = new ListenerMaintainerImpl();
	
	public ListProperty()
	{
		this(new ArrayList<T>());
	}
	
	public ListProperty(List<T> initialList)
	{
		set(initialList);
	}

	@Override
	public void set(List<T> l)
	{
		final List<T> copy = new ArrayList<T>(l.size());
		copy.addAll(l);
		final NotifyingList<T> notifyinList = new NotifyingList<T>(copy, eventing);
		super.set(new AccessCheckingList<T>(notifyinList, this));
	}
	
	public void registerListAccessListener(
			IPropertyAttributeListener<Access> listener, boolean current) {
		listAccess.registerListener(listener, current);
		
	}

	public boolean removeListAccesssListener(
			IPropertyAttributeListener<Access> listener) {
		return listAccess.removeListener(listener);
	}

	public void setListAccess(Access attribute) {
		if (attribute == null)
		{
			throw new NullPointerException();
		}
		listAccess.set(attribute);
	}
	
	public Access getListAccess() {
		return listAccess.get();
	}

	public boolean registerCollectionChangedListener(IListPropertyListener<T> l, boolean current) {
		final boolean added = listeners.add(l);
		if (get().size() > 0 && added && current)
		{
			
			eventing.fireEvent(ChangeKind.ELEMENT_ADDED, get());
		}
		return added;
	}

	public boolean removeCollectionChangedListener(IListPropertyListener<T> l) {
		return listeners.remove(l);
	}
	
	private class ListenerMaintainerImpl implements ListChangeListenerMaintainer<T>
	{
		
		public void fireEvent(final ChangeKind kind, final Collection<? extends T> elements)
		{
			final PropertyListChangedEvent<T> event = new PropertyListChangedEvent<T>(ListProperty.this, kind, elements);
			fireEvent(event);
		}
		
		public void fireSingleEvent(final ChangeKind kind, final T element)
		
		{
			@SuppressWarnings("unchecked")
			final PropertyListChangedEvent<T> event = new PropertyListChangedEvent<T>(ListProperty.this, kind, Arrays.asList((T[])new Object[] {element}));
			fireEvent(event);
		}
		
		private void fireEvent(final PropertyListChangedEvent<T> event)
		{
			for (IListPropertyListener<T> l : listeners)
			{
				l.listChanged(event);
			}
		}
	}
	
}
