/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.compatibility.internal.readers.impl;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.compatibility.internal.readers.ISearchPageDescription;
import org.eclipse.platform.discovery.compatibility.internal.readers.ISearchPageParser;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.AbstractExtensionPointParser;
import org.eclipse.search.ui.ISearchPage;

public class SearchPageParser extends AbstractExtensionPointParser<ISearchPageDescription> implements ISearchPageParser{

	public static final String ID = "id"; //$NON-NLS-1$
	public static final String LABEL = "label"; //$NON-NLS-1$
	public static final String CLASS = "class"; //$NON-NLS-1$

	
	public static final String PAGE_ELEMENT = "page"; //$NON-NLS-1$
	public static final String SEARCH_PAGES_XP_ID = "org.eclipse.search.searchPages"; //$NON-NLS-1$

	public SearchPageParser(IExtensionRegistry extensionRegistry) {
		super(extensionRegistry, SEARCH_PAGES_XP_ID, PAGE_ELEMENT);
	}  
	
	@Override
	protected ISearchPageDescription createObject(final IConfigurationElement element) {
		return new SearchPageDescription(element.getAttribute(ID), element.getAttribute(LABEL)) {
			@Override
			public ISearchPage getSearchPage() {
				try {
					return (ISearchPage) element.createExecutableExtension(CLASS);
				} catch (CoreException e) {
					throw new IllegalStateException(e);
				}
			}
		};
	}
	
}
