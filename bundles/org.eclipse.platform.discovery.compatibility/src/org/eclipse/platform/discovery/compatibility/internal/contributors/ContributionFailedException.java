/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.compatibility.internal.contributors;

/**
 * Indicates that a dynamic contribution to an extension registry has failed for some reason.
 * 
 *  @see IDynamicRegistryContributor
 * @author Dimitar Georgiev
 *
 */
@SuppressWarnings("serial")
public class ContributionFailedException extends Exception {


	/**
	 * Constructs an exception whose message will be <code>message<code> concatenated with <code>objects.toString()</code>
	 * @param message
	 * @param objects
	 */
    public ContributionFailedException(String message) {
    	super(message); 
    }
    
}
