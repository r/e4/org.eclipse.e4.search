/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.compatibility.internal.contributors;

import org.eclipse.core.runtime.IContributor;
import org.eclipse.core.runtime.IExtensionRegistry;

/**
 * This interface describes a object which is capable of contributing extensions dynamically to an extension registry.
 * @author Dimitar Georgiev
 * 
 *
 */
public interface IDynamicRegistryContributor {
	
	/**
	 * Contributes to the passed extension registry, using the passed contribution parameters. The contribution is performed in non-persistent mode, meaning that a restart of the framework will
	 * result in loss of the contributions.
	 * 
	 * @param registry - the registry to which to contribute
	 * @param contributor - the contributor to use
	 * @param token - the token to use
	 * @throws ContributionFailedException - in case the contribution was partially or fully unsuccessful
	 * @throws IllegalStateException - if the underlying registry does not accept the passed token
	 */
	public void contribute(IExtensionRegistry registry, IContributor contributor, Object token) throws ContributionFailedException;
}
