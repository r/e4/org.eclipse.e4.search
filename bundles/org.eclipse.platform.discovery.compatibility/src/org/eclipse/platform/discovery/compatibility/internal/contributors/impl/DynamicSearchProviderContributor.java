/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.compatibility.internal.contributors.impl;

import java.util.Collection;

import org.eclipse.platform.discovery.compatibility.internal.contributors.impl.searchprovider.CompatibilitySearchProvider;
import org.eclipse.platform.discovery.compatibility.internal.readers.ISearchPageDescription;
import org.eclipse.platform.discovery.compatibility.internal.readers.ISearchPageParser;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.SearchProvidersExtensionParser;
import org.w3c.dom.Element;

public class DynamicSearchProviderContributor extends DynamicRegistryContributor<ISearchPageDescription> {

	private final IdCalculator idCalculator = new IdCalculator();
	
	public DynamicSearchProviderContributor(ISearchPageParser contributionsReader) {
		super(contributionsReader, SearchProvidersExtensionParser.XP_ID, SearchProvidersExtensionParser.XP_ELEMENT_NAME);
	}

	@Override
	protected String getFailureMessage(Collection<ISearchPageDescription> failedContributors) {
		return "Dynamic search provider contribution failed for the following search pages"+failedContributors; //$NON-NLS-1$
	}

	@Override
	protected String getContributionDescription(ISearchPageDescription data) {
		return "Dynamic search provider for search page:"+data; //$NON-NLS-1$
	}

	@Override
	protected void configureTargetElement(Element element, ISearchPageDescription data) {
		element.setAttribute(SearchProvidersExtensionParser.PROVIDER_ID_ATTR, idCalculator.calculateSearchProviderId(data));
		element.setAttribute(SearchProvidersExtensionParser.OBJECT_ID_ATTR, idCalculator.calculateObjectTypeId(data));
		element.setAttribute(SearchProvidersExtensionParser.PROVIDER_INSTANCE_ATTR, CompatibilitySearchProvider.class.getName());
	}

}
