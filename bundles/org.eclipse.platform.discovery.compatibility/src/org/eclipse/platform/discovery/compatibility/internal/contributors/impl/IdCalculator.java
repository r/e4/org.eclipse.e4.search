/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.compatibility.internal.contributors.impl;

import org.eclipse.platform.discovery.compatibility.internal.readers.ISearchPageDescription;
import static org.eclipse.platform.discovery.util.internal.ContractChecker.nullCheckParam;

public class IdCalculator {
	
	private static final String DOT = "."; //$NON-NLS-1$

	public String calculateObjectTypeId(ISearchPageDescription description) {
		return calculateObjectTypeId(description.getId());
	}
	
	public String calculateObjectTypeId(String searchPageId) {
		nullCheckParam(searchPageId);
		return searchPageId + DOT + "compatibility.objecttype"; //$NON-NLS-1$
	}
	
	public String calculateSearchProviderId(ISearchPageDescription description) {
		return calculateSearchProviderId(description.getId());
	}

	public String calculateSearchProviderId(String searchPageId) {
		nullCheckParam(searchPageId);
		return searchPageId + DOT + "compatibility.searchprovider"; //$NON-NLS-1$
	}
}
