/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.compatibility.internal;

import org.eclipse.core.internal.registry.ExtensionRegistry;
import org.eclipse.core.runtime.ContributorFactoryOSGi;
import org.eclipse.core.runtime.IContributor;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.platform.discovery.compatibility.internal.contributors.ContributionFailedException;
import org.eclipse.platform.discovery.compatibility.internal.contributors.IDynamicRegistryContributor;
import org.eclipse.platform.discovery.compatibility.internal.contributors.impl.DynamicObjectTypeContributor;
import org.eclipse.platform.discovery.compatibility.internal.contributors.impl.DynamicSearchProviderContributor;
import org.eclipse.platform.discovery.compatibility.internal.readers.ISearchPageParser;
import org.eclipse.platform.discovery.compatibility.internal.readers.impl.SearchPageParser;
import org.eclipse.platform.discovery.util.internal.logging.ILogger;
import org.eclipse.platform.discovery.util.internal.logging.Logger;
import org.eclipse.ui.IStartup;
import org.osgi.framework.BundleContext;

/**
 * The compatibility bundle is responsible to read org.eclipse.search contributions, "convert them" to discovery contributions, and write them
 * back to the extension registry.
 * The 'writing' happens in non-persistent mode, meaning that a restart of the framework will result in disappearance of the created extensions.
 * This bundle is optional to the search console framework, which is also unaware of the existence of this bundle. 
 * All the code of the bundle is internal and should be referenced neither by search console framework, nor by external clients; this is emphasized
 * by the root package name.
 * This bundle communicates with the outside world only by means of the extension registry.	
 * 
 */
public class DiscoveryCompatibilityPlugin extends Plugin implements IStartup  {

	// The plug-in ID
	public static final String SYMBOLIC_NAME = "org.eclipse.platform.discovery.compatibility"; //$NON-NLS-1$

	// The shared instance
	private static DiscoveryCompatibilityPlugin plugin;
	
	private final ILogger logger;
	
	/**
	 * The constructor
	 */
	public DiscoveryCompatibilityPlugin() {
		logger = new Logger();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		
		ISearchPageParser parser = new SearchPageParser(getRegistry());
		contributeDynamicObjectTypes(parser);
		contributeDynamicSearchProviders(parser);
	}
	
	private void contributeDynamicObjectTypes(ISearchPageParser parser) {
		makeDynamicContribution(new DynamicObjectTypeContributor( parser ));
		
	}
	
	private void contributeDynamicSearchProviders(ISearchPageParser parser) {
		makeDynamicContribution(new DynamicSearchProviderContributor( parser ));
	}
	
	private void makeDynamicContribution(IDynamicRegistryContributor dynamicRegistryContributor) {
		try{
			dynamicRegistryContributor.contribute(getRegistry(), getContributor(), getUserToken());
		}catch(ContributionFailedException ex) {
			logger().logError(ex.getMessage());
		}
		
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static DiscoveryCompatibilityPlugin getDefault() {
		return plugin;
	}


	//logger to be used by the code in this bundle
	public ILogger logger() {
		return logger;
	}
	
	//the contributor used to write to the registry
	private IContributor getContributor() {
		return ContributorFactoryOSGi.createContributor(getBundle());
	}
	
	private IExtensionRegistry getRegistry() {
		return Platform.getExtensionRegistry();
	}
	
	/*
	 * This is the only way know to us to obtain the user token of the platform registry.
	 * The other possibility is to launch equinox with runtime option not to require a user token, but since we do not have control
	 * how we are launched, that is use\less to us.
	 */
	private Object getUserToken() {
		return ((ExtensionRegistry)getRegistry()).getTemporaryUserToken();
	}
	
	@Override
	public void earlyStartup() {
		//nothing to do, just declare we want to start early
	}
	
	
	
}
