/*******************************************************************************
 * Copyright (c) 2011 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.compatibility.internal.contributors.impl.searchprovider;

import org.eclipse.platform.discovery.runtime.api.ISearchParameters;
import org.eclipse.platform.discovery.runtime.api.ISearchProvider;
import org.eclipse.platform.discovery.runtime.api.ISearchQuery;
import org.eclipse.platform.discovery.runtime.api.SearchCancelledException;
import org.eclipse.platform.discovery.runtime.api.SearchFailedException;
import org.eclipse.platform.discovery.runtime.api.impl.SearchProvider;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;

public class CompatibilitySearchProvider extends SearchProvider implements ISearchProvider {
	@Override
	public ISearchQuery createQuery(ISearchParameters searchParameters) {
		return new CompatibilitySearchQuery();
	}
	
	private class CompatibilitySearchQuery implements ISearchQuery {

		@Override
		public Object execute(ILongOperationRunner lor)
				throws SearchFailedException, SearchCancelledException {
			return "foo"; //$NON-NLS-1$
		}
		
	}

}
