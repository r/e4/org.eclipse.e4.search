/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.compatibility.internal.readers.impl;

import org.eclipse.platform.discovery.compatibility.internal.readers.ISearchPageDescription;
import org.eclipse.platform.discovery.runtime.api.impl.DescriptiveObject;

public abstract class SearchPageDescription extends DescriptiveObject implements ISearchPageDescription{

	public SearchPageDescription(String id, String displayName) {
		super(id, displayName);
	}

}
