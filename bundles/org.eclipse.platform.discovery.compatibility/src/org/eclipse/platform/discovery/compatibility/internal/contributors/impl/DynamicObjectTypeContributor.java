/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.compatibility.internal.contributors.impl;

import java.text.MessageFormat;
import java.util.Collection;

import org.eclipse.platform.discovery.compatibility.internal.contributors.IDynamicRegistryContributor;
import org.eclipse.platform.discovery.compatibility.internal.readers.ISearchPageDescription;
import org.eclipse.platform.discovery.compatibility.internal.readers.ISearchPageParser;
import org.eclipse.platform.discovery.runtime.internal.xp.IObjectTypeExtensionParser;
import org.w3c.dom.Element;

public class DynamicObjectTypeContributor extends DynamicRegistryContributor<ISearchPageDescription> implements IDynamicRegistryContributor {

	private final IdCalculator calculator = new IdCalculator();
	
	public DynamicObjectTypeContributor(ISearchPageParser parser) {
		super(parser, IObjectTypeExtensionParser.XP_ID, IObjectTypeExtensionParser.XP_ELEMENT_NAME);
	}

	@Override
	protected void configureTargetElement(Element element, ISearchPageDescription data) {
		element.setAttribute(IObjectTypeExtensionParser.ID_ATTR_NAME, calculator.calculateObjectTypeId(data));
		element.setAttribute(IObjectTypeExtensionParser.DISPLAY_NAME_ATTR_NAME, data.getDisplayName());
	}

	@Override
	protected String getContributionDescription(ISearchPageDescription data) {
		return MessageFormat.format("Dynamic ObjectType for search page: {0}", data.toString()); //$NON-NLS-1$
	}

	@Override
	protected String getFailureMessage(Collection<ISearchPageDescription> failedContributors) {
		return "Dynamic object type contribution failed for the following search pages:" + failedContributors; //$NON-NLS-1$
	}
	
}