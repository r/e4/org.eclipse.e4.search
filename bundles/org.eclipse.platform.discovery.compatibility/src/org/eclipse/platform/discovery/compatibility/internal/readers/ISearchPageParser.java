/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.compatibility.internal.readers;

import org.eclipse.platform.discovery.runtime.internal.xp.IContributionsReader;

/**
 * Responsible for parsing page contributors to the org.eclipse.search.searchPages extension point.
 * @author Dimitar Georgiev
 *
 */
public interface ISearchPageParser extends IContributionsReader<ISearchPageDescription> {

}
