/*******************************************************************************
 * Copyright (c) 2012 SAP AG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.destprefs.internal.prefpage.ui;

import java.util.Collection;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.swt.widgets.Shell;

public interface IDestinationConfiguratorsView {
    public void setInput(final Collection<CategoryDestinationProviderPair> input);
    public void setStatus(final IStatus status);
    public void setAddEnabled(final boolean enabled);
    public void setEditEnabled(final boolean enabled);
    public void setRemoveEnabled(final boolean enabled);
    public void setTestEnabled(final boolean enabled);
	public Shell getShell();
}
