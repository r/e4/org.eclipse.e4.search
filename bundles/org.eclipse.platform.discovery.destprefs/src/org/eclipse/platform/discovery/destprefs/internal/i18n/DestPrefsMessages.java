/*******************************************************************************
 * Copyright (c) 2012 SAP AG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.destprefs.internal.i18n;

import org.eclipse.osgi.util.NLS;

public class DestPrefsMessages extends NLS {
    private static final String BUNDLE_NAME = "org.eclipse.platform.discovery.destprefs.internal.i18n.DestPrefsMessages"; //$NON-NLS-1$
    static {
        NLS.initializeMessages(BUNDLE_NAME, DestPrefsMessages.class);
    }
    
    public static String DestinationsPrefPage_AddButton;
    public static String DestinationsPrefPage_EditButton;
    public static String DestinationsPrefPage_DeleteButton;
    public static String DestinationsPrefPage_TestButton;
}
