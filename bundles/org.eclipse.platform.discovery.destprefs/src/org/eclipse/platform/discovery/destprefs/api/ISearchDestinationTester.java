/*******************************************************************************
 * Copyright (c) 2012 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.destprefs.api;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.swt.widgets.Shell;

/**
 * Interface for checking availability of a destination.
 * 
 * @author Martin Aleksandrov, Svetlina Shopova
 *
 * @param <T> - type of destination
 */
public interface ISearchDestinationTester<T extends ISearchDestination> {

	/**
	 * Checks for availability for a given destination
	 * 
	 * @param parentShell
	 * @param destination - the tested destination
	 * @return status of connection
	 */
	IStatus test(Shell parentShell, T destination);
	
}
