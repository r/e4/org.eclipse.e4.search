/*******************************************************************************
 * Copyright (c) 2012 SAP AG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.destprefs.internal.xpparser;

import org.eclipse.platform.discovery.destprefs.api.ISearchDestinationConfigurator;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;

/**
 * Wrapper interface for {@link ISearchDestinationConfigurator}
 * @author Danail Branekov
 *
 */
public interface ISearchDestinationsConfiguratorDescription {
    public <T extends ISearchDestination> ISearchDestinationConfigurator<T> createConfigurator();
    public String destinationProviderId();
    public String destinationCategoryId();
}
