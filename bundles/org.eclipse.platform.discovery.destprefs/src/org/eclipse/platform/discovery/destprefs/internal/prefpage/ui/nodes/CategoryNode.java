/*******************************************************************************
 * Copyright (c) 2012 SAP AG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.destprefs.internal.prefpage.ui.nodes;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationsProviderDescription;

public class CategoryNode {
    private final String displayName;
	private final Set<ISearchDestination> destinations;
	private final IDestinationsProviderDescription destinationProvider;
	private final String destinationProviderId;

	public CategoryNode(String displayName, IDestinationsProviderDescription destinationProvider) {
		this.displayName = displayName;
		this.destinations = destinationProvider.createProvider().getSearchDestinations();
		this.destinationProvider = destinationProvider;
		this.destinationProviderId = destinationProvider.getId();
	}

	public List<DestinationNode> getDestinations() {
		final List<DestinationNode> result = new ArrayList<DestinationNode>();
		
		for (ISearchDestination destination : destinations) {
			DestinationNode destinationNode = new DestinationNode(this, destination);
			result.add(destinationNode);
		}
		
		return result;
	}
	
	public int getDestinationsCount() {
		return this.destinations.size();
	}
	
	public String getDisplayName() {
		return this.displayName;
	}

    public String getDestinationProviderId() {
        return destinationProvider.getId();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((destinationProviderId == null) ? 0 : destinationProviderId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        CategoryNode other = (CategoryNode) obj;
        if (destinationProviderId == null) {
            if (other.destinationProviderId != null) {
                return false;
            }
        } else if (!destinationProviderId.equals(other.destinationProviderId)) {
            return false;
        }
        return true;
    }
}
