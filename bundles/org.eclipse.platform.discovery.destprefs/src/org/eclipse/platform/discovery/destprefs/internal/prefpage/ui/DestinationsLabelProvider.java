/*******************************************************************************
 * Copyright (c) 2010, 2012 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.destprefs.internal.prefpage.ui;

import org.eclipse.jface.viewers.StyledCellLabelProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.platform.discovery.destprefs.internal.prefpage.ui.nodes.CategoryNode;
import org.eclipse.platform.discovery.destprefs.internal.prefpage.ui.nodes.DestinationNode;

public class DestinationsLabelProvider extends StyledCellLabelProvider {
    @Override
    public void update(ViewerCell cell) {
        Object object = cell.getElement();
        StyledString styledString = (object instanceof CategoryNode ? getCategoryNodeText((CategoryNode)object) : getDestinationNodeText((DestinationNode)object));
        cell.setText(styledString.toString());
        cell.setStyleRanges(styledString.getStyleRanges());
        super.update(cell);
    }

    private StyledString getDestinationNodeText(DestinationNode destination) {
        return new StyledString(destination.getDestination().getDisplayName());
    }

    private StyledString getCategoryNodeText(CategoryNode category) {
        StyledString styledString = new StyledString(category.getDisplayName());
        styledString.append(" (" + category.getDestinationsCount() + ")", StyledString.COUNTER_STYLER); //$NON-NLS-1$ //$NON-NLS-2$
        return styledString;
    }
}
