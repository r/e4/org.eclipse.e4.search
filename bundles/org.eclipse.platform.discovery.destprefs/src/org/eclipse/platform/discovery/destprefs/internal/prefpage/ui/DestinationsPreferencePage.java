/*******************************************************************************
 * Copyright (c) 2012 SAP AG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.destprefs.internal.prefpage.ui;

import java.util.Collection;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.platform.discovery.destprefs.internal.i18n.DestPrefsMessages;
import org.eclipse.platform.discovery.destprefs.internal.prefpage.ui.nodes.CategoryNode;
import org.eclipse.platform.discovery.destprefs.internal.prefpage.ui.nodes.DestinationNode;
import org.eclipse.platform.discovery.destprefs.internal.xpparser.SearchDestinationsConfiguratorXPParser;
import org.eclipse.platform.discovery.runtime.internal.SearchProviderConfigurationFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.PlatformUI;

public class DestinationsPreferencePage extends PreferencePage implements IWorkbenchPreferencePage, IDestinationConfiguratorsView {
    private final IDestinationConfiguratorsPresenter presenter;
    private TreeViewer destinationsTreeViewer;
    private Button addDestinationButton;
    private Button removeDestinationButton;
    private Button deleteDestinationButton;
    private Button testDestinationButton;
    private StatusLineManager statusLineManager;

    public DestinationsPreferencePage(final IDestinationConfiguratorsPresenter presenter) {
        this.presenter = presenter;
    }

    public DestinationsPreferencePage() {
        this(new DestinationConfiguratorsPresenter(new SearchDestinationsConfiguratorXPParser(Platform.getExtensionRegistry()),
        		new SearchProviderConfigurationFactory().getSearchProviderConfiguration(Platform.getExtensionRegistry())));
    }

    @Override
    public void init(IWorkbench workbench) {
        noDefaultAndApplyButton();
    }

    @Override
    protected Control createContents(final Composite parent) {
        final Composite containerComposite = new Composite(parent, SWT.NONE);
        containerComposite.setLayout(new GridLayout(1, true));
        GridDataFactory.defaultsFor(containerComposite).align(SWT.FILL, SWT.FILL).grab(true, true).applyTo(containerComposite);
        
        final Composite destinationsComposite = new Composite(containerComposite, SWT.NONE);
        destinationsComposite.setLayout(new GridLayout(2, false));
        GridDataFactory.defaultsFor(destinationsComposite).align(SWT.FILL, SWT.FILL).grab(true, true).applyTo(destinationsComposite);
        
        final Composite treeViewerComposite = new Composite(destinationsComposite, SWT.NONE);
        treeViewerComposite.setLayout(new FillLayout(SWT.VERTICAL));
        GridDataFactory.defaultsFor(treeViewerComposite).align(SWT.FILL, SWT.FILL).grab(true, true).applyTo(treeViewerComposite);
        destinationsTreeViewer = createDestinationsTreeViewer(treeViewerComposite);

        final Composite buttonsComposite = new Composite(destinationsComposite, SWT.NONE);
        buttonsComposite.setLayout(new GridLayout(1, true));
        GridDataFactory.defaultsFor(buttonsComposite).align(SWT.END, SWT.FILL).grab(false, true).applyTo(buttonsComposite);
        addDestinationButton = createAddDestinationButton(buttonsComposite);
        removeDestinationButton = createEditDestinationButton(buttonsComposite);
        deleteDestinationButton = createDeleteDestinationButton(buttonsComposite);
        testDestinationButton = createTestDestinationButton(buttonsComposite);

        final Composite statusLineComposite = new Composite(containerComposite, SWT.NONE);
        statusLineComposite.setLayout(new FillLayout(SWT.HORIZONTAL));
        GridDataFactory.defaultsFor(statusLineComposite).align(SWT.FILL, SWT.FILL).grab(true, false).span(2, 1).applyTo(statusLineComposite);
        
        statusLineManager = new StatusLineManager();
        statusLineManager.createControl(statusLineComposite, SWT.NONE);
        
        this.presenter.setView(this);
        
        return destinationsComposite;
    }

    private TreeViewer createDestinationsTreeViewer(final Composite parent) {
        final TreeViewer viewer = new TreeViewer(parent, SWT.SINGLE | SWT.BORDER);
        viewer.addSelectionChangedListener(destinationsSelectionChangeListener());
        viewer.setLabelProvider(new DestinationsLabelProvider());
        viewer.setContentProvider(new CategoryDestinationsContentProvider());
        return viewer;
    }

    private ISelectionChangedListener destinationsSelectionChangeListener() {
        return new ISelectionChangedListener() {
            @Override
            public void selectionChanged(final SelectionChangedEvent event) {
                if (event.getSelection().isEmpty()) {
                    presenter.selectionChanged(new DestinationConfiguratorSelection(null, null));
                    return;
                }

                final Object selectedElement = ((StructuredSelection) event.getSelection()).getFirstElement();
                if (selectedElement instanceof CategoryNode) {
                    presenter.selectionChanged(new DestinationConfiguratorSelection(((CategoryNode) selectedElement).getDestinationProviderId(), null));
                    return;
                }
                if (selectedElement instanceof DestinationNode) {
                    final DestinationNode destNode = (DestinationNode)selectedElement;
                    presenter.selectionChanged(new DestinationConfiguratorSelection(destNode.getParentNode().getDestinationProviderId(), destNode.getDestination()));
                    return;
                }
                throw new IllegalArgumentException("Unsupported element: " + selectedElement); //$NON-NLS-1$
            }
        };
    }

    private Button createAddDestinationButton(final Composite parent) {
        final Button button = createDisabledButton(parent, DestPrefsMessages.DestinationsPrefPage_AddButton);
        button.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                presenter.addDestination();
            }
        });
        return button;
    }

    private Button createEditDestinationButton(final Composite parent) {
        final Button button = createDisabledButton(parent, DestPrefsMessages.DestinationsPrefPage_EditButton);
        button.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                presenter.editDestination();
            }
        });
        return button;
    }

    private Button createDeleteDestinationButton(final Composite parent) {
        final Button button = createDisabledButton(parent, DestPrefsMessages.DestinationsPrefPage_DeleteButton);
        button.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                presenter.removeDestination();
            }
        });
        return button;
    }

    private Button createTestDestinationButton(final Composite parent) {
        final Button button = createDisabledButton(parent, DestPrefsMessages.DestinationsPrefPage_TestButton);
        button.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                presenter.testDestination();
            }
        });
        return button;
    }

    private Button createDisabledButton(final Composite parent, final String label) {
        final Button button = new Button(parent, SWT.PUSH);
        button.setText(label);
        GridDataFactory.defaultsFor(button).applyTo(button);
        button.setEnabled(false);
        return button;
    }

    @Override
    public void setInput(Collection<CategoryDestinationProviderPair> input) {
        final Object[] expandedElements = destinationsTreeViewer.getExpandedElements();
        final ISelection selection = destinationsTreeViewer.getSelection();
        
        destinationsTreeViewer.setInput(input);
        destinationsTreeViewer.setExpandedElements(expandedElements);
        destinationsTreeViewer.setSelection(selection);
    }

    @Override
    public void setStatus(IStatus status) {
        switch (status.getSeverity()) {
        case IStatus.ERROR:
            statusLineManager.setErrorMessage(imageForStatus(status), status.getMessage());
            statusLineManager.setMessage(null, null);
            break;
        case IStatus.OK:
            statusLineManager.setErrorMessage(null, null);
            statusLineManager.setMessage(null, null);
            break;
        default:
            statusLineManager.setMessage(imageForStatus(status), status.getMessage());
            statusLineManager.setErrorMessage(null, null);
        }
    }

    @Override
    public void setAddEnabled(boolean enabled) {
        this.addDestinationButton.setEnabled(enabled);
    }

    @Override
    public void setEditEnabled(boolean enabled) {
        this.removeDestinationButton.setEnabled(enabled);
    }

    @Override
    public void setRemoveEnabled(boolean enabled) {
        this.deleteDestinationButton.setEnabled(enabled);
    }

    @Override
    public void setTestEnabled(boolean enabled) {
        this.testDestinationButton.setEnabled(enabled);
    }
    
    private Image imageForStatus(final IStatus status) {
        switch (status.getSeverity()) {
        case IStatus.ERROR:
            return PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJS_ERROR_TSK);
        case IStatus.WARNING:
            return PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJS_WARN_TSK);
        case IStatus.INFO:
            return PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJS_INFO_TSK);
        }
        return null;
    }
    
}
