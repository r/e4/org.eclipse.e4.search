/*******************************************************************************
 * Copyright (c) 2012 SAP AG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.destprefs.internal.prefpage.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.platform.discovery.destprefs.internal.prefpage.ui.nodes.CategoryNode;

public class CategoryDestinationsContentProvider implements ITreeContentProvider {

    private final List<CategoryDestinationProviderPair> categoryDestinationPairs;

    public CategoryDestinationsContentProvider() {
        this.categoryDestinationPairs = new ArrayList<CategoryDestinationProviderPair>();
    }

    @Override
    public void dispose() {
    }

    @SuppressWarnings("unchecked")
    @Override
    public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        categoryDestinationPairs.clear();

        if (newInput != null) {
            assert (newInput instanceof List);
            categoryDestinationPairs.addAll((List<CategoryDestinationProviderPair>) newInput);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object[] getElements(Object inputElement) {
        final List<CategoryNode> categoryNodes = new ArrayList<CategoryNode>();
        
        final List<CategoryDestinationProviderPair> pairs = (List<CategoryDestinationProviderPair>) inputElement;
        for (CategoryDestinationProviderPair categDescPair : pairs) {
            CategoryNode node = new CategoryNode(categDescPair.category.getDisplayName(), categDescPair.destinationProvider);
            categoryNodes.add(node);
        }
        
        return categoryNodes.toArray();
    }

    @Override
    public Object[] getChildren(Object parentElement) {
        final CategoryNode categoryNode  = (CategoryNode) parentElement;
        return categoryNode.getDestinations().toArray();
    }

    @Override
    public Object getParent(Object element) {
        return null;
    }

    @Override
    public boolean hasChildren(Object element) {
        if(element instanceof CategoryNode) {
            return ((CategoryNode)element).getDestinationsCount() > 0;
        }
        return false;
    }

}
