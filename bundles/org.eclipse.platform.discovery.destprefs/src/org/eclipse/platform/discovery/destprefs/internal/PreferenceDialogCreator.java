package org.eclipse.platform.discovery.destprefs.internal;

import org.eclipse.jface.preference.PreferenceDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.PreferencesUtil;

public class PreferenceDialogCreator implements IPreferenceDialogCreator {

	@Override
	public void openPreferenceDialog(Shell parentShell, String preselectedPageId, String[] pageIds) {
		PreferenceDialog prefDialog = PreferencesUtil.createPreferenceDialogOn(parentShell, preselectedPageId, pageIds, null);
		prefDialog.getTreeViewer().expandAll();
		prefDialog.open();
	}

}
