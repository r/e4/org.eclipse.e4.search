/*******************************************************************************
 * Copyright (c) 2012 SAP AG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.destprefs.internal.xpparser;

import java.util.Collection;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.destprefs.api.ISearchDestinationConfigurator;
import org.eclipse.platform.discovery.runtime.internal.ISearchProviderConfiguration;
import org.eclipse.platform.discovery.runtime.internal.SearchProviderConfigurationFactory;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationsProviderDescription;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.AbstractExtensionPointParser;

public class SearchDestinationsConfiguratorXPParser extends AbstractExtensionPointParser<ISearchDestinationsConfiguratorDescription> implements
        ISearchDestinationsConfiguratorXPParser {
    public static final String XP_ID = "org.eclipse.platform.discovery.destprefs.configurator"; //$NON-NLS-1$
    public static final String CONFIGURATOR_ELEMENT_NAME = "destinationConfigurator"; //$NON-NLS-1$
    public static final String CONFIGURATOR_ID_ELEMENT_NAME = "id"; //$NON-NLS-1$
    public static final String CONFIGURATOR_DEST_PROVIDER_ID_ELEMENT_NAME = "destProviderId"; //$NON-NLS-1$
    public static final String CONFIGURATOR_FQNAME_ELEMENT_NAME = "contributorClass"; //$NON-NLS-1$

    public SearchDestinationsConfiguratorXPParser(final IExtensionRegistry extRegistry) {
        super(extRegistry, XP_ID, CONFIGURATOR_ELEMENT_NAME);
    }

    @Override
    protected ISearchDestinationsConfiguratorDescription createObject(final IConfigurationElement element) throws CoreException {
        final String destProviderId = element.getAttribute(CONFIGURATOR_DEST_PROVIDER_ID_ELEMENT_NAME);
        final String destCategoryId = destinationCategoryIdFor(destProviderId);
        return new ISearchDestinationsConfiguratorDescription() {
            @Override
            public String destinationProviderId() {
                return destProviderId;
            }

            @Override
            public String destinationCategoryId() {
                return destCategoryId;
            }

            @SuppressWarnings("unchecked")
			@Override
            public ISearchDestinationConfigurator<?> createConfigurator() {
                try {
                    return (ISearchDestinationConfigurator<?>) element.createExecutableExtension(CONFIGURATOR_FQNAME_ELEMENT_NAME);
                } catch (CoreException e) {
                    throw new RuntimeException(e);
                }
            }
        };
    }

    private String destinationCategoryIdFor(final String destProviderId) {
        final Collection<IDestinationCategoryDescription> destinationCategories = searchProviderConfiguration().getDestinationCategories();
        for (IDestinationCategoryDescription destCategory : destinationCategories) {
            final IDestinationsProviderDescription provider = getDestinationProvider(destCategory, destProviderId);
            if (provider != null) {
                return destCategory.getId();
            }
        }
        throw new IllegalArgumentException("Could not find destination category for destination provider " + destProviderId); //$NON-NLS-1$
    }

    private IDestinationsProviderDescription getDestinationProvider(IDestinationCategoryDescription destCategory, final String destProviderId) {
        for (IDestinationsProviderDescription destProvider : searchProviderConfiguration().getDestinationProvidersForCategory(destCategory)) {
            if (destProviderId.equals(destProvider.getId())) {
                return destProvider;
            }
        }
        return null;
    }

    protected ISearchProviderConfiguration searchProviderConfiguration() {
        return new SearchProviderConfigurationFactory().getSearchProviderConfiguration(extRegistry);
    }
}
