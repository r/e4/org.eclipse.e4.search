package org.eclipse.platform.discovery.destprefs.internal;

import org.eclipse.swt.widgets.Shell;

/** To facilitate unit testing, the functionality to open a preference dialog is hidden
 * behind this interface.
 **/

public interface IPreferenceDialogCreator {

	public void openPreferenceDialog(Shell parentShell, String preselectedPageId, String[] pageIds);

}
