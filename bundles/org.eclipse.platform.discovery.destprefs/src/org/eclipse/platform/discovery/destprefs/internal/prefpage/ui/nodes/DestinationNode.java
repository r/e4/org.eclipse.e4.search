/*******************************************************************************
 * Copyright (c) 2012 SAP AG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.destprefs.internal.prefpage.ui.nodes;

import org.eclipse.platform.discovery.runtime.api.ISearchDestination;

public class DestinationNode {
    private final CategoryNode parentNode;
	private final ISearchDestination destination;
	
	public DestinationNode(CategoryNode parentNode, ISearchDestination destination) {
		this.parentNode = parentNode;
		this.destination = destination;
	}
	
	public ISearchDestination getDestination() {
		return this.destination;
	}
	
	public CategoryNode getParentNode() {
		return this.parentNode;
	}

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((destination == null) ? 0 : destination.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        DestinationNode other = (DestinationNode) obj;
        if (destination == null) {
            if (other.destination != null) {
                return false;
            }
        } else if (!destination.equals(other.destination)) {
            return false;
        }
        return true;
    }
}
