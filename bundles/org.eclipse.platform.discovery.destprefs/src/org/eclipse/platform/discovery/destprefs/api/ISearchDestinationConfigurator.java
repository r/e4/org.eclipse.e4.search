/*******************************************************************************
 * Copyright (c) 2012 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.destprefs.api;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.swt.widgets.Shell;

/**
 * Interface for configurators which can create/edit/remove destinations for a specific destination category 
 * 
 * @author Martin Alexandrov, Svetlina Shopova
 *
 * @param <T> - type of destination
 */
public interface ISearchDestinationConfigurator<T extends ISearchDestination> {
	
	/**
	 * Creates destination and returns the operation status.
	 * 
	 * @param parentShell
	 * @return creation status
	 */
	IStatus createDestination(Shell parentShell);
	
	/**
	 * Deletes a given destination and returns the operation status.
	 * 
	 * @param parentShell
	 * @param destination Destination to be deleted
	 * @return deletion status
	 */
	IStatus deleteDestination(Shell parentShell, T destination);
	
	/**
	 * Edits a given destination and returns the operation status.
	 * 
	 * @param parentShell
	 * @param destination Destination to be edited
	 * @return edition status
	 */
	IStatus editDestination(Shell parentShell, T destination);
	
	/**
	 * Possible to return null if the test operation is not supported.
	 * 
	 * @return {@link ISearchDestinationTester} for this type of destination
	 */
	ISearchDestinationTester<T> getSearchDestinationTester();

}
