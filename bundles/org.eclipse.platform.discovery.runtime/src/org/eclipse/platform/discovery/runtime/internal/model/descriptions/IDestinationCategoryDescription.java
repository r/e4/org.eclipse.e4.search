/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.model.descriptions;


import java.util.List;

import org.eclipse.platform.discovery.runtime.api.IDescriptiveObject;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;


/**
 * Interface which should be implemented by all destination categories contributed via the destination categories extension point
 * 
 * @author Danail Branekov
 * 
 */
public interface IDestinationCategoryDescription extends IDescriptiveObject
{
	/**
	 * Retrieves a list of destination provider ids which which can provider destinations for this category category
	 */
	public List<String> getDestinationProviderIds();

	/**
	 * Gets the destination class name specified via the "destinaionclass" attribute of the destinations category extension point
	 * 
	 * @return
	 */
	public Class<ISearchDestination> getDestinationsClass();
}
