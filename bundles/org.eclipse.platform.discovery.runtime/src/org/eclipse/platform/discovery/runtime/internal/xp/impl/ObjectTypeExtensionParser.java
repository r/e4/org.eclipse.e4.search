/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.xp.impl;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.impl.ObjectTypeDescription;
import org.eclipse.platform.discovery.runtime.internal.xp.IObjectTypeExtensionParser;


public class ObjectTypeExtensionParser extends AbstractExtensionPointParser<IObjectTypeDescription> implements IObjectTypeExtensionParser
{

	public ObjectTypeExtensionParser()
	{
		this(Platform.getExtensionRegistry());
	}
	
	public ObjectTypeExtensionParser(IExtensionRegistry registry) {
		super(registry, XP_ID, XP_ELEMENT_NAME);
	}

	@Override
	protected IObjectTypeDescription createObject(final IConfigurationElement element)
	{
		return new ObjectTypeDescription(element.getAttribute(ID_ATTR_NAME), element.getAttribute(DISPLAY_NAME_ATTR_NAME));
	}
}
