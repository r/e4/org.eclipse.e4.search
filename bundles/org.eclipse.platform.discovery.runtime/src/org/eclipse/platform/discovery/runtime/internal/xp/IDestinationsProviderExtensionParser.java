/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.xp;

import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationsProviderDescription;

/**
 * Interface for parsing the destination providers extension point
 * 
 * @author Danail Branekov
 * 
 */
public interface IDestinationsProviderExtensionParser extends IContributionsReader<IDestinationsProviderDescription>
{
}
