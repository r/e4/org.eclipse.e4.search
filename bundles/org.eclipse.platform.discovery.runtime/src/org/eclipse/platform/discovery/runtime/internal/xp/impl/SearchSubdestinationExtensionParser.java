/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.xp.impl;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.platform.discovery.runtime.api.IConflict;
import org.eclipse.platform.discovery.runtime.api.ISearchSubdestination;
import org.eclipse.platform.discovery.runtime.internal.xp.ISearchSubdestinationExtensionParser;


/**
 * Implementation of the {@link ISearchSubdestinationExtensionParser} interface
 * 
 * @author Danail Branekov
 */
public class SearchSubdestinationExtensionParser extends AbstractExtensionPointParser<ISearchSubdestination> implements ISearchSubdestinationExtensionParser
{
	public static final String XP_ID = "org.eclipse.platform.discovery.runtime.searchsubdestinations"; //$NON-NLS-1$
	public static final String XP_ELEMENT_NAME = "subdestination"; //$NON-NLS-1$
	public static final String CATEGORY_ID_ATTR = "categoryid"; //$NON-NLS-1$
	public static final String DISPLAY_NAME_ATTR = "displayname"; //$NON-NLS-1$
	public static final String ID_ATTR = "id"; //$NON-NLS-1$
	public static final String OBJECT_ATTR = "objecttypeid"; //$NON-NLS-1$
	public static final String DEFAULT_SELECTED_ATTR = "defaultSelected"; //$NON-NLS-1$
	public static final String CONFLICT_ELEMENT_NAME = "conflict";////$NON-NLS-1$
	public static final String CONF_SUBID_ATTR_NAME = "conflictingSubdID"; //$NON-NLS-1$

	public SearchSubdestinationExtensionParser()
	{
		this(Platform.getExtensionRegistry());
	}

	public SearchSubdestinationExtensionParser(IExtensionRegistry registry) 
	{
		super(registry, XP_ID, XP_ELEMENT_NAME);
	}

	@Override
	protected ISearchSubdestination createObject(final IConfigurationElement element)
	{
		return new ISearchSubdestination() {

			public String getDestinationCategoryId()
			{
				return element.getAttribute(CATEGORY_ID_ATTR);
			}

			public String getObjectTypeId()
			{
				return element.getAttribute(OBJECT_ATTR);
			}

			public String getDisplayName()
			{
				return element.getAttribute(DISPLAY_NAME_ATTR);
			}

			public String getId()
			{
				return element.getAttribute(ID_ATTR);
			}
			
			@Override
			public Set<IConflict> getConflictingSubd() {
				Set<IConflict> retVal = new HashSet<IConflict>();
				
				for(final IConfigurationElement curr:element.getChildren(CONFLICT_ELEMENT_NAME)) {
					retVal.add(new IConflict() {
						@Override
						public String getconflictingSubdID() {
							return curr.getAttribute(CONF_SUBID_ATTR_NAME);
						}
					});
				}
				return retVal;
			} 

			@Override
			public boolean isDefaultSelected()
			{
				final String value = element.getAttribute(DEFAULT_SELECTED_ATTR);
				return value == null ? true : Boolean.valueOf(value);
			}
		};
	}

}
