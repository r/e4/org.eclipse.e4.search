/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.platform.discovery.runtime.api.IConflict;
import org.eclipse.platform.discovery.runtime.api.ISearchSubdestination;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.ISearchProviderDescription;
import org.eclipse.platform.discovery.runtime.internal.xp.ISearchSubdestinationExtensionParser;


public class SubdestinationsActivationConfig implements ISubdestinationsActivationConfig
{
	private final ISearchSubdestinationExtensionParser subdestinationsParser;
	private final Map<String, Boolean> checkedState = new HashMap<String, Boolean>();
	private final Map<String,List<String>> conflictsMap = new HashMap<String, List<String>>();//cache

	public SubdestinationsActivationConfig(final ISearchSubdestinationExtensionParser subdestinationsParser)
	{
		this.subdestinationsParser = subdestinationsParser;
	}
	
	public void activateSubdestination(final IObjectTypeDescription searchObjectType, final IDestinationCategoryDescription destinationCategory,
			final ISearchProviderDescription searchProviderDescription, final ISearchSubdestination subDestination, final boolean activate)
	{
		List<ISearchSubdestination> availableSearchSubdestinations = getAvailableSearchSubdestinations(searchObjectType, destinationCategory, searchProviderDescription);
		Map<String, ISearchSubdestination> subdMap = getIdToSubdestinationMap(availableSearchSubdestinations);
		activate:if(activate) {
			//first check cache
			if(conflictsMap.containsKey(subDestination.getId())) {
				List<String> conflicts = conflictsMap.get(subDestination.getId());
				for(String curr:conflicts) {
					checkedState.put(curr, false);
				}
				break activate;
			}
			//for all conflicts deactivate conflict and put in cache
			List<String> conflicts = new LinkedList<String>();
			for(IConflict conflict: subDestination.getConflictingSubd()) {
				conflicts.add(conflict.getconflictingSubdID());
				if(checkedState.containsKey(conflict.getconflictingSubdID())) {
					checkedState.put(conflict.getconflictingSubdID(), false);
				}
			}
			//handle reflexion - if a conflicts b, then b conflicts a even if not explicitly specified
			for(String id:subdMap.keySet()) {
				List<String> otherConflicts = new LinkedList<String>();
				if(checkedState.get(id)!=null && checkedState.get(id)) {//if checked
					ISearchSubdestination otherSubD=subdMap.get(id);
					assert otherSubD!=null;
					for(IConflict conflict: otherSubD.getConflictingSubd()) {
						otherConflicts.add(conflict.getconflictingSubdID());
						if(conflict.getconflictingSubdID().equals(subDestination.getId())) {
							conflicts.add(otherSubD.getId());
							checkedState.put(otherSubD.getId(), false);
						}
					}
					conflictsMap.put(id, otherConflicts);
				}
			}
			conflictsMap.put(subDestination.getId(), conflicts);
		}
		for(ISearchSubdestination sd : availableSearchSubdestinations)
		{
			if(sd.getId().equals(subDestination.getId()))
			{
				checkedState.put(sd.getId(), activate);
			}
		}
	}

	public List<ISearchSubdestination> getAvailableSearchSubdestinations(final IObjectTypeDescription objectType, final IDestinationCategoryDescription destinationCategory, final ISearchProviderDescription searchProvider)
	{
		final List<ISearchSubdestination> result = new ArrayList<ISearchSubdestination>();
		for (ISearchSubdestination subDest : this.subdestinationsParser.readContributions())
		{
			if (subDest.getObjectTypeId().equals(objectType.getId()) && subDest.getDestinationCategoryId().equals(destinationCategory.getId()))
			{
				result.add(subDest);
			}
		}
		return result;

	}

	public boolean isSubdestinationActive(final ISearchSubdestination subdestination, final IObjectTypeDescription objectType, final IDestinationCategoryDescription destCategory, final ISearchProviderDescription searchProvider)
	{
		Boolean active = checkedState.get(subdestination.getId());
		if (active != null)
		{
			return active;
		}

		activateSubdestination(objectType, destCategory, searchProvider, subdestination, subdestination.isDefaultSelected());
		
		active = checkedState.get(subdestination.getId());
		return active == null ? false : active;
	}
	
	/*utility method*/
	private Map<String, ISearchSubdestination> getIdToSubdestinationMap(List<ISearchSubdestination> subdestinations) {
		Map<String, ISearchSubdestination> retVal = new HashMap<String, ISearchSubdestination>();
		for(ISearchSubdestination current: subdestinations) {
			retVal.put(current.getId(), current);
		}
		return retVal;
	}
	
}
