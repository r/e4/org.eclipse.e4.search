/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal;


import java.util.List;

import org.eclipse.platform.discovery.runtime.api.ISearchSubdestination;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.ISearchProviderDescription;


/**
 * Sub-destinations activation configuration interface
 * @author Danail Branekov
 */
public interface ISubdestinationsActivationConfig
{
	/**
	 * Gets the available sub-destinations for the combination object type - destination category - search provider 
	 * @param objectType the object type
	 * @param destinationCategory the destination category
	 * @param searchProvider search provider
	 * @return a set of active 
	 */
	public List<ISearchSubdestination> getAvailableSearchSubdestinations(final IObjectTypeDescription objectType, final IDestinationCategoryDescription destinationCategory, final ISearchProviderDescription searchProvider);

	/**
	 * Activates/de-activate a sub-destinations for the combination object type - destination category - provider descriptions. This method should persist the
	 * enabled/disabled state in a preference store.<br> 
	 * In case the search subdestination specified is not known (e.g. is not contained in the {@link #getAvailableSearchSubdestinations(IObjectTypeDescription, IDestinationCategoryDescription, ISearchProviderDescription)} result set) this method has no effect
	 * 
	 * @param searchObjectType
	 *            the object type
	 * @param destinationCategory
	 *            the destination category
	 * @param searchProviderDescription
	 *            the search provider description
	 * @param subDestination
	 *            the sub-destination to enabled/disabled
	 * @param activate
	 *            true if the sub-destinations is to be activated, false otherwise
	 */
	public void activateSubdestination(final IObjectTypeDescription searchObjectType, final IDestinationCategoryDescription destinationCategory,
									final ISearchProviderDescription searchProviderDescription, final ISearchSubdestination subDestination, final boolean activate);

	/**
	 * Checks whether the sub-destination specified is active for the specified object type, destination category, search provider<br> 
	 * In case the search subdestination specified is not known (e.g. is not contained in the {@link #getAvailableSearchSubdestinations(IObjectTypeDescription, IDestinationCategoryDescription, ISearchProviderDescription)} result set) this method will return <code>false</code>
	 * @param subdestination the sub-destination
	 * @param objectType the object type
	 * @param destCategory the destination category
	 * @param searchProvider the search provider
	 * @return true in case the sub-destination is active for the specified object type, destination category, search provider; false otherwise 
	 */
	public boolean isSubdestinationActive(final ISearchSubdestination subdestination, final IObjectTypeDescription objectType, final IDestinationCategoryDescription destCategory, final ISearchProviderDescription searchProvider);
}
