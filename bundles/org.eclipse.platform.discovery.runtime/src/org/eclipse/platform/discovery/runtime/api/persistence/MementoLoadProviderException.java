/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.api.persistence;

/**
 * MementoLoadProviderException is thrown when {@link IMementoLoadProvider} 
 * could not load data. 
 * 
 * @author Iliyan Dimov
 *
 */
public class MementoLoadProviderException extends Exception {
	private static final long serialVersionUID = 3309658616948571801L;

	public MementoLoadProviderException() {		
	}

	public MementoLoadProviderException(String message) {
		super(message);
	}

	public MementoLoadProviderException(Throwable cause) {
		super(cause);
	}

	public MementoLoadProviderException(String message, Throwable cause) {
		super(message, cause);
	}
}
