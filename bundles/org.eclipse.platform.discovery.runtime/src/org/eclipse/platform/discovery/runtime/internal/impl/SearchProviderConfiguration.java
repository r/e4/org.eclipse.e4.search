/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.platform.discovery.runtime.api.IDestinationsProvider;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.ISearchSubdestination;
import org.eclipse.platform.discovery.runtime.internal.DestinationCategoryNotFoundException;
import org.eclipse.platform.discovery.runtime.internal.ISearchProviderActivationConfig;
import org.eclipse.platform.discovery.runtime.internal.ISearchProviderConfiguration;
import org.eclipse.platform.discovery.runtime.internal.ISubdestinationsActivationConfig;
import org.eclipse.platform.discovery.runtime.internal.IXPParsersFactory;
import org.eclipse.platform.discovery.runtime.internal.ProviderNotFoundException;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationsProviderDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.ISearchProviderDescription;
import org.eclipse.platform.discovery.runtime.internal.xp.ISearchProvidersExtensionParser;
import org.eclipse.platform.discovery.util.internal.logging.ILogger;
import org.eclipse.platform.discovery.util.internal.logging.Logger;


/**
 * {@link ISearchProviderConfiguration} implementation
 * 
 * @author Danail Branekov
 * 
 */
public class SearchProviderConfiguration implements ISearchProviderConfiguration
{
	private final ISearchProviderActivationConfig searchProviderActivationConfig;

	private final ISubdestinationsActivationConfig subdestinationsActivationConfig;
	
	private final ILogger logger = Logger.instance();

	private IXPParsersFactory parsersFactory;

	/**
	 * Constructor
	 * @param parsersFactory 
	 * 
	 * @param searchProviderActivationConfig
	 *            the search provider activation configuration
	 * @param subdestinationsActivationConfig
	 *            the subdestinations activation configuration
	 */
	public SearchProviderConfiguration(final IXPParsersFactory parsersFactory, final ISearchProviderActivationConfig searchProviderActivationConfig,
									final ISubdestinationsActivationConfig subdestinationsActivationConfig)
	{
		this.parsersFactory = parsersFactory;
		this.searchProviderActivationConfig = searchProviderActivationConfig;
		this.subdestinationsActivationConfig = subdestinationsActivationConfig;
	}

	/**
	 * Get a search providers extension parser instance
	 * 
	 * @return
	 */
	protected ISearchProvidersExtensionParser searchProvidersExtensionParser()
	{
		return parsersFactory.createSearchProviderParser();
	}

	public List<ISearchProviderDescription> getAvailableSearchProviderDescriptions(IObjectTypeDescription searchObjectType)
	{
		final List<ISearchProviderDescription> allProviders = searchProvidersExtensionParser().readContributions();

		final List<ISearchProviderDescription> result = new ArrayList<ISearchProviderDescription>();
		for (ISearchProviderDescription provider : allProviders)
		{
			if (provider.getObjectType().getId().equals(searchObjectType.getId()))
			{
				result.add(provider);
			}
		}

		return result;
	}

	public List<IObjectTypeDescription> getObjectTypes()
	{
		return parsersFactory.createObjectTypeParser().readContributions();
	}

	public void activateSubdestination(IObjectTypeDescription searchObjectType, IDestinationCategoryDescription destinationCategory,
			ISearchProviderDescription searchProviderDescription, ISearchSubdestination subDestination, boolean activate)
	{
		this.subdestinationsActivationConfig.activateSubdestination(searchObjectType, destinationCategory, searchProviderDescription, subDestination, activate);
	}

	public ISearchProviderDescription getActiveSearchProvider(IObjectTypeDescription objectType, IDestinationCategoryDescription destinationCategory)
			throws ProviderNotFoundException
	{
		
		return destinationCategory!=null ? searchProviderActivationConfig.getActiveSearchProviderDescription(objectType, destinationCategory) : searchProviderActivationConfig.getActiveSearchProviderDescription(objectType);
	}

	public List<IDestinationCategoryDescription> getAvailableDestinationCategoriesForObjectType(IObjectTypeDescription objectType)
	{
		final List<IDestinationCategoryDescription> result = new ArrayList<IDestinationCategoryDescription>();
		for(IDestinationCategoryDescription category : getDestinationCategories())
		{
			try
			{
				getActiveSearchProvider(objectType, category);
				result.add(category);
			} catch (ProviderNotFoundException e)
			{
				// No search provider for the object in the current category
				continue;
			}
		}
		
		return result;
	}

	public List<ISearchSubdestination> getAvailableSearchSubdestinations(final IObjectTypeDescription objectType, final IDestinationCategoryDescription destinationCategory, final ISearchProviderDescription provider)
	{
		return this.subdestinationsActivationConfig.getAvailableSearchSubdestinations(objectType, destinationCategory, provider);
	}

	public List<IDestinationCategoryDescription> getDestinationCategories()
	{
		return this.parsersFactory.createDestinationsCategoryParser().readContributions();
	}

	public List<IDestinationsProviderDescription> getDestinationProvidersForCategory(final IDestinationCategoryDescription category)
	{
		final List<IDestinationsProviderDescription> result = new ArrayList<IDestinationsProviderDescription>();
		
		for(IDestinationsProviderDescription destProvider : getAvailableDestinationProviders())
		{
			if(category.getDestinationProviderIds().contains(destProvider.getId()))
			{
				result.add(destProvider);
			}
		}
		
		return result;
	}
	
	public List<ISearchDestination> getSearchDestinations(final IDestinationCategoryDescription category, final IDestinationsProvider destinationsProvider)
	{
		final List<ISearchDestination> result = new ArrayList<ISearchDestination>();
		final Class<?> expectedDestinationClass = category.getDestinationsClass();
		for(ISearchDestination dest : destinationsProvider.getSearchDestinations())
		{
			if(expectedDestinationClass.isAssignableFrom(dest.getClass()))
			{
				result.add(dest);
			}
			else
			{
				logger.logWarn("Provider " + destinationsProvider.getClass().getName() + " did not provide destination of class " + category.getDestinationsClass().getName()); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		
		return result;
	}

	public List<IDestinationCategoryDescription> getDestinationCategoriesForDestination(final ISearchDestination destination) throws DestinationCategoryNotFoundException
	{
		final List<IDestinationCategoryDescription> result = new ArrayList<IDestinationCategoryDescription>();
		
		final List<String> destinationProvidersIds = new ArrayList<String>();
		for(IDestinationsProviderDescription providerDescription : getAvailableDestinationProviders())
		{
			if(providerDescription.createProvider().getSearchDestinations().contains(destination))
			{
				destinationProvidersIds.add(providerDescription.getId());
			}
		}
		
		for(IDestinationCategoryDescription cat : getDestinationCategories())
		{
			if(Collections.disjoint(destinationProvidersIds, cat.getDestinationProviderIds()))
			{
				continue;
			}
			result.add(cat);
		}

		if(result.isEmpty())
		{
			throw new DestinationCategoryNotFoundException("Destination category for destination id " + destination.getDisplayName() + " was not found"); //$NON-NLS-1$ //$NON-NLS-2$
		}
			
		return result;
	}

	public boolean isSubdestinationActive(ISearchSubdestination subdestination, IObjectTypeDescription objectType,
			IDestinationCategoryDescription destCategory, ISearchProviderDescription searchProvider)
	{
		return this.subdestinationsActivationConfig.isSubdestinationActive(subdestination, objectType, destCategory, searchProvider);
	}

	public List<IDestinationsProviderDescription> getAvailableDestinationProviders()
	{
		return parsersFactory.createDestinationsProviderParser().readContributions();
	}
	
}
