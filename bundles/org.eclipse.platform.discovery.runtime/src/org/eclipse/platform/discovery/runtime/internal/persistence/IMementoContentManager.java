/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.persistence;

import java.util.Collection;

import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.ui.IMemento;


/**
 * IContentManager provides interface to load or store a 
 * collection of objects.
 *  
 * @author Iliyan Dimov
 * 
 */
public interface IMementoContentManager <T> {

	/**
	 * Loads data from the container.
	 * 
	 * @param container Location from where to load data.
	 * @return Collection Contains the loaded data.
	 * @throws MementoContentManagerException If data can not be loaded.
	 * @throws NullPointerException If <code>container</code> is null.
	 */
	Collection<T> loadContent(IMemento container, final ILongOperationRunner opRunner) throws MementoContentManagerException;
	
	/**
	 * Saves into the container given collection. 
	 * 
	 * @param container The container where to store data.
	 * @param content The collection that contains data.
	 * @throws MementoContentManagerException If data can not be saved.
	 * @throws NullPointerException If one of the parameteres is null.
	 */
	void saveContent(IMemento container, Collection<T> content, final ILongOperationRunner opRunner) throws MementoContentManagerException;
}
