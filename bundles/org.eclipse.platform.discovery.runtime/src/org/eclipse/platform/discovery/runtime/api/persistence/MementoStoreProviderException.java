/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.api.persistence;

/**
 * MementoStoreProviderException is thrown when {@link IMementoStoreProvider} 
 * could not store data. 
 * 
 * @author Iliyan Dimov
 *
 */
public class MementoStoreProviderException extends Exception {
	private static final long serialVersionUID = 1416221093952936856L;

	public MementoStoreProviderException() {		
	}

	public MementoStoreProviderException(String message) {
		super(message);
	}

	public MementoStoreProviderException(Throwable cause) {
		super(cause);
	}

	public MementoStoreProviderException(String message, Throwable cause) {
		super(message, cause);
	}
}
