/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.api.persistence;

import org.eclipse.platform.discovery.runtime.api.ISearchDestination;

/**
 * Class which holds a pair item - search destination. The pair is used during data persistance via {@link IMementoLoadProvider} and {@link IMementoStoreProvider}  
 * @author Martina Galabova
 */
public class DestinationItemPair
{
	private final Object item;
	private final ISearchDestination destination;

	public DestinationItemPair(final ISearchDestination destination, final Object item)
	{
		this.item = item;
		this.destination = destination;
	}

	public Object getItem() {
		return item;
	}

	public ISearchDestination getDestination() {
		return destination;
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if(obj == null)
		{
			return false;
		}
		if(!obj.getClass().equals(this.getClass()))
		{
			return false;
		}
		DestinationItemPair pair = (DestinationItemPair)obj;
		if((pair.getDestination().equals(this.destination)) && (pair.getItem().equals(this.item)))
		{
			return true;
		}
		return false;
	}
	
	@Override
	public int hashCode() 
	{
		if(destination == null && item == null)
		{
			return 0;
		}
		if(destination == null)
		{
			return item.hashCode();
		}
		if(item == null)
		{
			return destination.hashCode();
		}
		return destination.hashCode() ^ item.hashCode();
	}
}
