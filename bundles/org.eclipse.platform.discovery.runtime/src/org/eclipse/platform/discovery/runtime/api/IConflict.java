/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.api;

/**
 * Interface for Subdestination Conflicts
 */
public interface IConflict  {
	/**
	 * 
	 * @return this Conflict's conflicting subdestination ID 
	 */
	public String getconflictingSubdID();
}
