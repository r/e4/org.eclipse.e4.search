/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.persistence.xp;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.platform.discovery.runtime.api.persistence.IMementoLoadProvider;
import org.eclipse.platform.discovery.runtime.internal.persistence.model.descriptions.IMementoLoadProviderDescription;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.AbstractExtensionPointParser;


/**
 * Implementation of the {@link IMementoLoadProviderExtensionParser} interface.
 * 
 * @author Iliyan Dimov
 */
public class MementoLoadProvidersExtensionParser extends AbstractExtensionPointParser<IMementoLoadProviderDescription> implements IMementoLoadProviderExtensionParser
{
	public static final String XP_ID = "org.eclipse.platform.discovery.integration.persistence.mementoloadprovider"; //$NON-NLS-1$
	public static final String XP_ELEMENT_NAME = "provider"; //$NON-NLS-1$

	public static final String CLASS_ATTR = "class"; //$NON-NLS-1$
	
	public MementoLoadProvidersExtensionParser()
	{
		this(Platform.getExtensionRegistry());
	}
	
	public MementoLoadProvidersExtensionParser(IExtensionRegistry registry)
	{
		super(registry, XP_ID, XP_ELEMENT_NAME);
	}

	@Override
	protected IMementoLoadProviderDescription createObject(final IConfigurationElement element) {
		return new IMementoLoadProviderDescription() {
			
			@Override
			public IMementoLoadProvider createInstance()
			{
				try
				{
					return (IMementoLoadProvider) element.createExecutableExtension(CLASS_ATTR);
				} 
				catch (CoreException e)
				{
					throw new IllegalStateException("Load provider could not be instantiated", e); //$NON-NLS-1$
				}
			}	
		};
	}
}
