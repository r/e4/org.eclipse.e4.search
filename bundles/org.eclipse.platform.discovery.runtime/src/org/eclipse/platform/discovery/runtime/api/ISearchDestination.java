/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.api;

import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationsProviderDescription;

/**
 * Interface for search destinations. Implementers must implement the
 * {@link #equals(Object)} method in order the framework to be able to correctly
 * compare search destinations provided by different destination providers
 * 
 * @see IDestinationsProviderDescription
 * @author Danail Branekov
 */
public interface ISearchDestination
{
	/**
	 * Implementers MUST explicitly provide implementation for this method in
	 * order the framework to be able to compare identical destinations
	 * 
	 * @see Object#equals(Object)
	 */
	public boolean equals(Object obj);

	/**
	 * The display name of this search destination. It will be used for displaying purposes
	 * @return the search destination display name
	 */
	public String getDisplayName();
}
