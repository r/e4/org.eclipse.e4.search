/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.runtime.internal.impl.SearchProviderConfiguration;
import org.eclipse.platform.discovery.runtime.internal.impl.XpParsersFactory;
import org.eclipse.platform.discovery.runtime.internal.search.activation.SearchProviderActivationConfigDummy;

public class SearchProviderConfigurationFactory
{
	private static final Map<IExtensionRegistry, ISearchProviderConfiguration> configurationsMap = new HashMap<IExtensionRegistry, ISearchProviderConfiguration>();

	public ISearchProviderConfiguration getSearchProviderConfiguration(IExtensionRegistry registry)
	{
		if (!configurationsMap.containsKey(registry))
		{
			configurationsMap.put(registry, createConfiguration(registry));
		}
		return configurationsMap.get(registry);
	}

	private ISearchProviderConfiguration createConfiguration(IExtensionRegistry registry)
	{
		final XpParsersFactory parsersFactory = new XpParsersFactory(registry);
		return new SearchProviderConfiguration(parsersFactory, new SearchProviderActivationConfigDummy(parsersFactory.createSearchProviderParser()), new SubdestinationsActivationConfig(parsersFactory.createSubdestinationsParser()));
	}
}
