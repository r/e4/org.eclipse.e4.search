/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.persistence.util;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.platform.discovery.runtime.api.persistence.DestinationItemPair;
import org.eclipse.platform.discovery.runtime.api.persistence.IMementoLoadProvider;
import org.eclipse.platform.discovery.runtime.api.persistence.IMementoStoreProvider;
import org.eclipse.platform.discovery.runtime.internal.persistence.impl.XpPersistenceParsersFactory;
import org.eclipse.platform.discovery.runtime.internal.persistence.model.descriptions.IMementoLoadProviderDescription;
import org.eclipse.platform.discovery.runtime.internal.persistence.model.descriptions.IMementoStoreProviderDescription;
import org.eclipse.ui.IMemento;


/**
 * @author Iliyan Dimov
 *
 */
public class PersistenceProviderRegistryImpl implements IPersistenceProviderRegistry 
{
	private XpPersistenceParsersFactory xpPersistenceFactory = null; 
	private Set<IMementoLoadProvider> loadProviderSet = null;
	private Set<IMementoStoreProvider> storeProviderSet = null;
		
	public PersistenceProviderRegistryImpl()
	{			
		updateRegistry();		
	}
	
	@Override
	public final void updateRegistry() 
	{
		xpPersistenceFactory = new XpPersistenceParsersFactory();
		loadProviderSet = collectAllLoadProviders();
		storeProviderSet = collectAllStoreProviders();				
	}
	
	@Override
	public Set<IMementoLoadProvider> collectAllLoadProviders() 
	{
		final Set<IMementoLoadProvider> resultSet = new HashSet<IMementoLoadProvider>();
		IMementoLoadProvider resultItem = null;
		
		final List<IMementoLoadProviderDescription> descriptionSet = xpPersistenceFactory.createMementoLoadProviderParser().readContributions();
		for (IMementoLoadProviderDescription description : descriptionSet)
		{
			resultItem = description.createInstance();
			resultSet.add(resultItem);			
		}	
		
		return resultSet;
	}

	@Override
	public Set<IMementoStoreProvider> collectAllStoreProviders() 
	{
		final Set<IMementoStoreProvider> resultSet = new HashSet<IMementoStoreProvider>();
		IMementoStoreProvider resultItem = null;
		
		final List<IMementoStoreProviderDescription> descriptionSet = xpPersistenceFactory.createMementoStoreProviderParser().readContributions();
		for (IMementoStoreProviderDescription description : descriptionSet)
		{
			resultItem = description.createInstance();
			resultSet.add(resultItem);			
		}	
		
		return resultSet;
	}	
	
	@Override
	public Set<IMementoLoadProvider> findLoadProviders(IMemento container) 
	{
		final Set<IMementoLoadProvider> resultSet = new HashSet<IMementoLoadProvider>();
		
		for (IMementoLoadProvider loadProvider : loadProviderSet)
		{
			if (loadProvider.canLoad(container))
			{
				resultSet.add(loadProvider);				
			}
		}		
		
		return resultSet;
	}

	@Override
	public Set<IMementoStoreProvider> findStoreProviders(DestinationItemPair pair) 
	{
		final Set<IMementoStoreProvider> resultSet = new HashSet<IMementoStoreProvider>();
		
		for (IMementoStoreProvider storeProvider : storeProviderSet)
		{
			if (storeProvider.canStore(pair))
			{
				resultSet.add(storeProvider);				
			}
		}		
		
		return resultSet;
	}
}
