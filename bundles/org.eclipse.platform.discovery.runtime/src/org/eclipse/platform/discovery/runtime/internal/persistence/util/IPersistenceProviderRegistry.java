/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.persistence.util;

import java.util.Set;

import org.eclipse.platform.discovery.runtime.api.persistence.DestinationItemPair;
import org.eclipse.platform.discovery.runtime.api.persistence.IMementoLoadProvider;
import org.eclipse.platform.discovery.runtime.api.persistence.IMementoStoreProvider;
import org.eclipse.ui.IMemento;


/**
 * @author Iliyan Dimov
 *
 */
public interface IPersistenceProviderRegistry {
	
	public static final IPersistenceProviderRegistry INSTANCE = new PersistenceProviderRegistryImpl();
	
	/**
	 * Refresh providers with new registered/deregisteres ones.
	 */
	public void updateRegistry();
	
	/**
	 * Collects all load providers.
	 * 
	 * @return
	 */
	public Set<IMementoLoadProvider> collectAllLoadProviders();
	
	/**
	 * Collects all store providers.
	 * 
	 * @return
	 */
	public Set<IMementoStoreProvider> collectAllStoreProviders();
	
	/**
	 * Finds load providers for the given container.
	 * 
	 * @param container
	 * @return
	 */
	public Set<IMementoLoadProvider> findLoadProviders(IMemento container);
	
	/**
	 * Finds store providers for the given pair.
	 * 
	 * @param container
	 * @return
	 */
	public Set<IMementoStoreProvider> findStoreProviders(DestinationItemPair pair);
}
