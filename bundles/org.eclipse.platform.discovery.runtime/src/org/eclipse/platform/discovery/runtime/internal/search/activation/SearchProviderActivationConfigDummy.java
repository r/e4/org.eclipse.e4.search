/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.search.activation;

import java.text.MessageFormat;

import org.eclipse.platform.discovery.runtime.internal.ISearchProviderActivationConfig;
import org.eclipse.platform.discovery.runtime.internal.ProviderNotFoundException;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.ISearchProviderDescription;
import org.eclipse.platform.discovery.runtime.internal.xp.ISearchProvidersExtensionParser;

/**
 * Dummy implementation of the {@link ISearchProviderActivationConfig} interface. 
 * The {@link #getActiveSearchProviderDescription(IObjectTypeDescription, IDestinationCategoryDescription)} method would return the first suitable search provider. In future a preference page UI might be introduced to configure preferred search providers
 * @author Danail Branekov
 */
public class SearchProviderActivationConfigDummy implements ISearchProviderActivationConfig
{
	private ISearchProvidersExtensionParser searchProvidersExtensionParser;
	
	public SearchProviderActivationConfigDummy(ISearchProvidersExtensionParser parser)
	{
		this.searchProvidersExtensionParser = parser;
	}

	public ISearchProviderDescription getActiveSearchProviderDescription(IObjectTypeDescription objectType,
									IDestinationCategoryDescription destinationCategory) throws ProviderNotFoundException
	{
		for (ISearchProviderDescription searchProvider : searchProvidersExtensionParser.readContributions())
		{
			if (searchProvider.getObjectType().getId().equals(objectType.getId()))
			{
				for (IDestinationCategoryDescription supportedCategory : searchProvider.getSupportedDestinationCategories())
				{
					if (supportedCategory.getId().equals(destinationCategory.getId()))
					{
						return searchProvider;
					}
				}
			}
		}

		throw new ProviderNotFoundException("No provider found for object id " + objectType.getId() + " and category id " //$NON-NLS-1$ //$NON-NLS-2$
										+ destinationCategory.getId());
	}

	@Override
	public ISearchProviderDescription getActiveSearchProviderDescription (IObjectTypeDescription objectType) throws ProviderNotFoundException {
		for (ISearchProviderDescription searchProvider : searchProvidersExtensionParser.readContributions())
		{
			if (searchProvider.getObjectType().getId().equals(objectType.getId()) && searchProvider.getSupportedDestinationCategories().isEmpty())
			{
				return searchProvider;
			}
		
		}
		throw new ProviderNotFoundException(MessageFormat.format("No provider found for object id {0} that supports no categories", objectType.getId())); //$NON-NLS-1$ 

	}
}
