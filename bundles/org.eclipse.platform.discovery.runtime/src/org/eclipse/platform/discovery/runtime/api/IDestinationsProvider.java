/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.api;

import java.util.Set;

import org.eclipse.platform.discovery.runtime.api.impl.DestinationsProvider;


/**
 * Interface for providing {@link ISearchDestination} instances<br>
 * Do keep in mind that this interface might be a subject of future change. In order to avoid compilation problems it is advisable to use extend {@link DestinationsProvider} instead of implementing the interface directly
 * 
 * @author Danail Branekov
 */
public interface IDestinationsProvider
{
	/**
	 * Get a set of search destinations which this destination provider provides
	 * 
	 * @return a set of search destinations or an empty set if none
	 */
	public Set<ISearchDestination> getSearchDestinations();

	/**
	 * Registers a {@link IDestinationChangeHandler} instance. Implementors can notify the <code>handler</code> instance whenever they consider that
	 * search destinations changed and interested parties should be informed
	 * 
	 * @param handler
	 *            the destinations change handler
	 */
	public void registerDestinationsChangeHandler(final IDestinationChangeHandler handler);
	
	/**
	 * Unregisters the destinations change handler specified
	 * @param handler the handler to unregister
	 */
	public void unregisterDestinationsChangeHandler(final IDestinationChangeHandler handler);
}
