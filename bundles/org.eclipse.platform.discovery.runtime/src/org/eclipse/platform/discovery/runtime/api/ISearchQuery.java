/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.api;

import org.eclipse.platform.discovery.runtime.api.impl.SearchQuery;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;


/**
 * Interface for search queries<br>
 * Do keep in mind that this interface might be a subject of future change. In order to avoid compilation problems it is advisable to use extend {@link SearchQuery} instead of implementing the interface directly
 * @author Danail Branekov
 */
public interface ISearchQuery
{
	/**
	 * Executes the search query
	 * @param lor a {@link ILongOperationRunner} instance which can be used by the query implementation to run long running operations 
	 * @return result of the search
	 * @throws SearchFailedException when search failed
	 * @throws SearchCancelledException when the operation has been canceled by the user
	 */
	public Object execute(ILongOperationRunner lor) throws SearchFailedException, SearchCancelledException;
}
