/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.model.descriptions;

import java.util.Set;

import org.eclipse.platform.discovery.runtime.api.IDescriptiveObject;
import org.eclipse.platform.discovery.runtime.api.ISearchProvider;



/**
 * A contributed search provider description. Implementations of this interface provide all the provider data which is required by the discovery
 * framework in order to operate with it without instantiating it. Implementations also provide a method to instantiate the search provider. This
 * happens right before performing the search
 * 
 * @author Danail Branekov
 */
public interface ISearchProviderDescription extends IDescriptiveObject
{
	/**
	 * Gets the object type this provider is capable of searching for
	 * 
	 * @return supported object type
	 */
	public IObjectTypeDescription getObjectType();

	/**
	 * Retrieve a set of destination category descriptions in which the search provider is capable of searching in  
	 * @return
	 */
	public Set<IDestinationCategoryDescription> getSupportedDestinationCategories();

	/**
	 * Instantiate the search provider
	 * 
	 * @return a search provider instance
	 */
	public ISearchProvider createInstance();

	/**
	 * Returns whether this search provider supports text search
	 * 
	 * @return true if text search is supported, false otherwise
	 */
	public boolean supportsTextSearch();
}
