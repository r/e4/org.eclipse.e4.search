/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.api;

import org.eclipse.platform.discovery.util.internal.ContractChecker;

public final class GroupingHierarchy implements IDisplayableObject
{
	private final String displayName;
	private final Object id;
	
	public GroupingHierarchy(String displayName, Object id)
	{
		ContractChecker.nullCheckParam(displayName, "displayName"); //$NON-NLS-1$
		this.displayName = displayName;
		ContractChecker.nullCheckParam(id, "id"); //$NON-NLS-1$
		this.id = id;
	}
	
	public String getDisplayName()
	{
		return displayName;
	}
	
	public Object getId()
	{
		return id;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (o == null)
		{
			return false;
		}
		if (o.getClass() != getClass())
		{
			return false;
		}
		return id.equals(((GroupingHierarchy)o).getId());
	}
	
	@Override
	public int hashCode()
	{
		return id.hashCode();
	}
	
	@Override
	public String toString()
	{
		return getDisplayName();
	}
}
