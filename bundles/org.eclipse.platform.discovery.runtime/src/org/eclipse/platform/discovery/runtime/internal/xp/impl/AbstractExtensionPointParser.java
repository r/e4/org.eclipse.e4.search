/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.xp.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.platform.discovery.runtime.internal.xp.IContributionsReader;
import org.eclipse.platform.discovery.util.internal.logging.ILogger;
import org.eclipse.platform.discovery.util.internal.logging.Logger;

/**
 * Abstract extension point parser
 * @param <T> - the type of the objects this parser parses
 * @author Danail Branekov
 */
public abstract class AbstractExtensionPointParser<T> implements IContributionsReader<T>
{
	protected final IExtensionRegistry extRegistry;
	private final String xpId;
	private final String elementName;
	
	/**
	 * Constructor
	 * @param extRegistry the eclipse extesnion registry
	 * @param xpId the extension point id
	 * @param elementName extension point element name this parser will search for
	 */
	protected AbstractExtensionPointParser(final IExtensionRegistry extRegistry, final String xpId, final String elementName)
	{
		this.extRegistry = extRegistry;
		this.xpId = xpId;
		this.elementName = elementName;
	}
	
	@Override
	public final List<T> readContributions()
	{
		final List<T> result = new ArrayList<T>();
		for (IConfigurationElement element : getConfigurationElements())
		{
			try 
			{
				result.add(createObject(element));
			} catch (CoreException e) 
			{
				logger().logError("Problem creating contributor from configuration element", e); //$NON-NLS-1$
			}
		}

		return result;
	}
	
	protected ILogger logger()
	{
		return Logger.instance();
	}

	/**
	 * Method to create an object of type T based on a configuration element.
	 * @param element - a configuration element configured for the extension point id passed at construction, having an element name passed at construction
	 * 
	 * @return
	 */
	protected abstract T createObject(IConfigurationElement element) throws CoreException;

	/**
	 * Retrieves a list of configuration elements for the XP name and element name specified in the constructor
	 * @return
	 */
	private List<IConfigurationElement> getConfigurationElements()
	{
		final List<IConfigurationElement> result = new ArrayList<IConfigurationElement>();
		final IExtensionPoint extPoint = this.extRegistry.getExtensionPoint(this.xpId);
		for(IConfigurationElement elem : extPoint.getConfigurationElements())
		{
			if(elem.getName().equals(elementName))
			{
				result.add(elem);
			}
		}
		
		return result;
	}
	
}
