/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.model.descriptions;

/**
 * Common utility for description objects
 * @author Danail Branekov
 */
public interface IDescriptionsUtil
{
	/**
	 * Gets an {@link IObjectTypeDescription} for the sepcified object type id
	 * @param objectTypeId the object type id
	 * @return the object type descritpion; never null 
	 * @throws IllegalArgumentException when there is no object type with the id specified 
	 */
	public IObjectTypeDescription objectTypeForId(final String objectTypeId) throws IllegalArgumentException;
}
