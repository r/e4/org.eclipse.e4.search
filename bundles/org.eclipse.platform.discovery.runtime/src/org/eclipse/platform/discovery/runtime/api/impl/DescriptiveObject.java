/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.api.impl;

import java.text.MessageFormat;

import org.eclipse.platform.discovery.runtime.api.IDescriptiveObject;
import static org.eclipse.platform.discovery.util.internal.ContractChecker.*;

/**
 * Standard IDescriptiveObject implementation. It is convenient for reuse by data object implementations implementing IDescriptiveObject or some of its sub-intrfaces.
 * @author Dimitar Georgiev
 *
 */
public class DescriptiveObject implements IDescriptiveObject{
	
	private final String id;
	private final String displayName;

	
	public DescriptiveObject(String id, String displayName) {
		nullCheckVariable(id, "id"); //$NON-NLS-1$
		nullCheckVariable(displayName, "displayName"); //$NON-NLS-1$

		this.id = id;
		this.displayName = displayName;
	}
	
	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getDisplayName() {
		return displayName;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((displayName == null) ? 0 : displayName.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DescriptiveObject other = (DescriptiveObject) obj;
		if (displayName == null) {
			if (other.displayName != null)
				return false;
		} else if (!displayName.equals(other.displayName))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return MessageFormat.format("id:{0} displayName:{1}", id, displayName); //$NON-NLS-1$
	}
}
