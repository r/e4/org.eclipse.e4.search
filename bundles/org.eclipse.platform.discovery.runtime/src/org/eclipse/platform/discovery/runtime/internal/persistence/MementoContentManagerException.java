/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.persistence;

/**
 * LoadProviderException is throwned when <code>ILoadProvider</code> 
 * could not load data. 
 * 
 * @author Iliyan Dimov
 *
 */
public class MementoContentManagerException extends Exception {
	private static final long serialVersionUID = 3985733799611817393L;

	public MementoContentManagerException() {		
	}

	public MementoContentManagerException(String message) {
		super(message);
	}

	public MementoContentManagerException(Throwable cause) {
		super(cause);
	}

	public MementoContentManagerException(String message, Throwable cause) {
		super(message, cause);
	}
}
