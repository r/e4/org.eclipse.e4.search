/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.api.impl;

import java.util.Collections;
import java.util.Set;

import org.eclipse.platform.discovery.runtime.api.GroupingHierarchy;
import org.eclipse.platform.discovery.runtime.api.ISearchProvider;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.ISearchSubdestination;


/**
 * Abstract "empty" implementation of the {@link ISearchProvider} interface
 * @author Danail Branekov
 *
 */
public abstract class SearchProvider implements ISearchProvider
{
	public Set<GroupingHierarchy> getGroupingHierarchies(ISearchDestination soco, Set<ISearchSubdestination> subdestinations)
	{
		return Collections.emptySet();
	}
}
