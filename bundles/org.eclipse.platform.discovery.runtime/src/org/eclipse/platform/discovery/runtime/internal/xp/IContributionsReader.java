/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.xp;

import java.util.List;


/**
 * Interface for reading contributions
 * 
 * @author Danail Branekov
 * 
 * @param <T>
 *            the contributions type
 */
public interface IContributionsReader<T>
{
	/**
	 * Reads available contributions
	 * 
	 * @return a set of contributions or an empty set if none
	 */
	public List<T> readContributions();
}
