/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.xp;

import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;

/**
 * Parser of the extension point via which object types are contributed
 * @author Danail Branekov
 */
public interface IObjectTypeExtensionParser extends IContributionsReader<IObjectTypeDescription>
{
	public static final String XP_ID = "org.eclipse.platform.discovery.runtime.objecttype"; //$NON-NLS-1$
	public static final String XP_ELEMENT_NAME = "objecttype"; //$NON-NLS-1$
	public static final String ID_ATTR_NAME = "id"; //$NON-NLS-1$
	public static final String DISPLAY_NAME_ATTR_NAME = "displayname"; //$NON-NLS-1$
}
