/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal;

/**
 * Exception thrown when a search provider could not be found. Doesn't provide localized message
 * 
 * @author Danail Branekov
 */
public class ProviderNotFoundException extends Exception
{
	private static final long serialVersionUID = -4845700110883329548L;

	public ProviderNotFoundException()
	{
		super();
	}

	public ProviderNotFoundException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public ProviderNotFoundException(String message)
	{
		super(message);
	}

	public ProviderNotFoundException(Throwable cause)
	{
		super(cause);
	}
}
