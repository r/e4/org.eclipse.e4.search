/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.api;

import java.util.Set;

import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;


/**
 * Interface for search subdestinations
 * 
 * @author Danail Branekov
 * 
 */
public interface ISearchSubdestination extends IDescriptiveObject
{
	/**
	 * Gets the destination category this subdestination refers to
	 * 
	 * @see IDestinationCategoryDescription#getId()
	 */
	public String getDestinationCategoryId();

	/**
	 * Gets the object id this subdestinations is referes to
	 * 
	 * @see IObjectTypeDescription#getId()
	 */
	public String getObjectTypeId();

	/**
	 * Checks whether the subdestination is selected by default
	 * 
	 * @return <code>true</code> in case the subdestination is to be selected by default, <code>false</code> otherwise
	 */
	public boolean isDefaultSelected();
	
	/**
	 * 
	 * @return conflicting subdestinations, or an empty set if none
	 */
	public Set<IConflict> getConflictingSubd();

}
