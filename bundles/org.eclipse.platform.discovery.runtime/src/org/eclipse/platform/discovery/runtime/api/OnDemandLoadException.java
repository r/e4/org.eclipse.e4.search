/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.api;

public class OnDemandLoadException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final String localizedMessage;
	
	/**
	 * Constructs exception with ability to provide localized message
	 * @param message - the detail message (which is saved for later retrieval
     *         by the {@link #getMessage()} method)
	 * @param localizedMessage - the localized message (which is saved for later retrieval
     *         by the {@link #getLocalizedMessage()} method)
	 * 
	 * @throws NullPointerException  - in case localized message is null
	 * */
	public OnDemandLoadException(String localizedMessage, String message) {
		super(message);
		nullCheckParam(localizedMessage, "localizedMessage"); //$NON-NLS-1$
		this.localizedMessage = localizedMessage;
	}

	/**
	 * Constructs exception with ability to provide localized message
	 * @param message - the detail message (which is saved for later retrieval
     *         by the {@link #getMessage()} method)
	 * @param localizedMessage - the localized message (which is saved for later retrieval
     *         by the {@link #getLocalizedMessage()} method)
	 * @param cause -  cause the cause (which is saved for later retrieval by the
     *         {@link #getCause()} method).
	 * 
	 * @throws NullPointerException  - in case localized message is null
	 * */
	
	public OnDemandLoadException(String localizedMessage, String message, Throwable cause) {
		super(message, cause);
		nullCheckParam(localizedMessage, "localizedMessage"); //$NON-NLS-1$
		this.localizedMessage = localizedMessage;
	}
	
	/**
	 * Returns localized message with which the exception was constructed.*/
	@Override
	public String getLocalizedMessage()
	{
		return localizedMessage;
	}
	
	private void nullCheckParam(final Object paramValue, final String paramName)
	{
		if (paramName == null)
		{
			throw new NullPointerException("paramName must not be null"); //$NON-NLS-1$ 
		}
		if (paramValue == null)
		{
			throw new NullPointerException(paramName + " must not be null"); //$NON-NLS-1$ 
		}
	}
}
