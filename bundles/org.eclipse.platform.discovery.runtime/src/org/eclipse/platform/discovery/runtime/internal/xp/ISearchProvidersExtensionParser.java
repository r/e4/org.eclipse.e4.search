/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.xp;

import org.eclipse.platform.discovery.runtime.internal.model.descriptions.ISearchProviderDescription;

/**
 * Extender of the {@link ExtensionManager} interface which is capable of instantiating objects used in the search console
 * @author Danail Branekov
 */
public interface ISearchProvidersExtensionParser extends IContributionsReader<ISearchProviderDescription>
{
}
