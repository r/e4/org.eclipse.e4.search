/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.xp.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationsProviderDescription;
import org.eclipse.platform.discovery.runtime.internal.xp.IDestinationCategoryExtensionParser;
import org.eclipse.platform.discovery.runtime.internal.xp.IDestinationsProviderExtensionParser;
import org.osgi.framework.Bundle;


/**
 * Implementation of the {@link IDestinationCategoryExtensionParser} interface
 * 
 * @author Danail Branekov
 * 
 */
public class DestinationsCategoryExtensionParser extends AbstractExtensionPointParser<IDestinationCategoryDescription> implements IDestinationCategoryExtensionParser
{
	public static final String XP_ID = "org.eclipse.platform.discovery.runtime.destinationcategory"; //$NON-NLS-1$
	public static final String XP_ELEMENT_NAME = "destinationcategory"; //$NON-NLS-1$
	public static final String CATEGORY_ID_ATTRIBUTE = "id"; //$NON-NLS-1$
	public static final String CATEGORY_DISPLAY_NAME_ATTRIBUTE = "displayname"; //$NON-NLS-1$
	public static final String DESTINATIONS_CLASS_ATTRIBUTE = "destinationclass"; //$NON-NLS-1$
	
	private final IDestinationsProviderExtensionParser destinationsProviderExtParser;
	
	public DestinationsCategoryExtensionParser()
	{
		this(Platform.getExtensionRegistry());
	}
	
	public DestinationsCategoryExtensionParser(final IExtensionRegistry registry) 
	{
		super(registry, XP_ID, XP_ELEMENT_NAME);
		this.destinationsProviderExtParser = new DestinationsProviderExtensionParser(registry);
	}

	@Override
	protected IDestinationCategoryDescription createObject(final IConfigurationElement element)
	{
		return new IDestinationCategoryDescription() {

			public List<String> getDestinationProviderIds()
			{
				return getDestProviderIds(getId());
			}

			public String getDisplayName()
			{
				return element.getAttribute(CATEGORY_DISPLAY_NAME_ATTRIBUTE);
			}

			public String getId()
			{
				return element.getAttribute(CATEGORY_ID_ATTRIBUTE);
			}
			
			public Class<ISearchDestination> getDestinationsClass()
			{
				return classForDestinationName(element);
			}
		};
	}

	private List<String> getDestProviderIds(final String categoryId)
	{
		final List<String> result = new ArrayList<String>();
		for (IDestinationsProviderDescription provider : destinationsProviderExtParser.readContributions())
		{
			if (provider.getDestinationCategoryId().equals(categoryId))
			{
				result.add(provider.getId());
			}
		}

		return result;
	}
	
	@SuppressWarnings("unchecked")
	private Class<ISearchDestination> classForDestinationName(final IConfigurationElement configElement)
	{
		final Bundle bundle = Platform.getBundle(configElement.getNamespaceIdentifier());
		try
		{
			return (Class<ISearchDestination>) bundle.loadClass(configElement.getAttribute(DESTINATIONS_CLASS_ATTRIBUTE));
		} catch (ClassNotFoundException e)
		{
			throw new IllegalStateException(e);
		}
	}
}
