/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.model.descriptions;

import org.eclipse.platform.discovery.runtime.api.IDescriptiveObject;
import org.eclipse.platform.discovery.runtime.api.IDestinationChangeHandler;
import org.eclipse.platform.discovery.runtime.api.IDestinationsProvider;

/**
 * Interface which describes a destinations provider instace
 * 
 * @author Danail Branekov
 */
public interface IDestinationsProviderDescription extends IDescriptiveObject
{
	/**
	 * Creates the destination provider instance
	 * 
	 * @return destination provider instance
	 */
	public IDestinationsProvider createProvider();

	/**
	 * Gets the destination category ID for which this destination provider provides destinations
	 * 
	 * @return
	 */
	public String getDestinationCategoryId();

	/**
	 * Gets the preference page ID which this destination provider declares
	 * 
	 * @return 
	 */
	public String getPreferencePageId();
	
	/**
	 * Registers a change handler which will be notified once the destinations provided by this provider changed
	 * @param changeHandler
	 */
	public void registerDestinationsChangeHandler(final IDestinationChangeHandler changeHandler);
}
