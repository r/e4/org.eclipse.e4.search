/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.persistence.xp;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.platform.discovery.runtime.api.persistence.IMementoStoreProvider;
import org.eclipse.platform.discovery.runtime.internal.persistence.model.descriptions.IMementoStoreProviderDescription;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.AbstractExtensionPointParser;

/**
 * Implementation of the {@link IMementoStoreProviderExtensionParser} interface.
 * 
 * @author Iliyan Dimov
 */
public class MementoStoreProvidersExtensionParser extends AbstractExtensionPointParser<IMementoStoreProviderDescription> implements IMementoStoreProviderExtensionParser
{
	public static final String XP_ID = "org.eclipse.platform.discovery.integration.persistence.mementostoreprovider"; //$NON-NLS-1$
	public static final String XP_ELEMENT_NAME = "provider"; //$NON-NLS-1$

	public static final String CLASS_ATTR = "class"; //$NON-NLS-1$

	public MementoStoreProvidersExtensionParser()
	{
		this(Platform.getExtensionRegistry());
	}
	
	public MementoStoreProvidersExtensionParser(IExtensionRegistry registry)
	{
		super(registry, XP_ID, XP_ELEMENT_NAME);
	}

	@Override
	protected IMementoStoreProviderDescription createObject(final IConfigurationElement element)
	{
		return new IMementoStoreProviderDescription() {
			
			@Override
			public IMementoStoreProvider createInstance()
			{
				try
				{
					return (IMementoStoreProvider) element.createExecutableExtension(CLASS_ATTR);
				} 
				catch (CoreException e)
				{
					throw new IllegalStateException("Store provider could not be instantiated", e); //$NON-NLS-1$
				}
			}	
		};
	}
}
