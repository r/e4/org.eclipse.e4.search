/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal;

import java.util.List;

import org.eclipse.platform.discovery.runtime.api.IDestinationsProvider;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.ISearchSubdestination;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationsProviderDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.ISearchProviderDescription;


/**
 * Interface for search providers configuration
 * 
 * @author Danail Branekov
 */
public interface ISearchProviderConfiguration
{
	/**
	 * Retrieves a set of descriptions for all search providers which are capable of searching for the specified object type
	 * 
	 * @param searchObjectType
	 *            the object type
	 * @return a set of descriptions for all search providers which are capable of searching for the specified object type or an empty set if none
	 */
	public List<ISearchProviderDescription> getAvailableSearchProviderDescriptions(IObjectTypeDescription searchObjectType);

	/**
	 * Retrieves a set of destination categories which can be searched by all the currently registered search providers
	 * 
	 * @param objectType
	 *            the object type
	 * @return a set of destination categories which can be searched or empty set if none
	 */
	public List<IDestinationCategoryDescription> getAvailableDestinationCategoriesForObjectType(final IObjectTypeDescription objectType);

	/**
	 * Retrieves a set of destination provider descriptions which can provide search destinations for the destination category specified
	 * 
	 * @param category
	 *            the destination category
	 * @return a set of destination provider descriptions or an empty set if none
	 */
	public List<IDestinationsProviderDescription> getDestinationProvidersForCategory(final IDestinationCategoryDescription category);

	/**
	 * Retrieves a set of available destination providers
	 * @return
	 */
	public List<IDestinationsProviderDescription> getAvailableDestinationProviders();

	/**
	 * Gets the description of the active search provider which can search for the specified object type in the specified destination category<br>
	 * There might be several registered search providers for a combination of object type - destination category. However, at a given point of time
	 * there is only one search provider that will be selected by the framework in order to perform the search. The active search provider settings
	 * should be persisted in a preference store
	 * 
	 * @param objectType
	 *            the object type
	 * @param destinationCategory
	 *            the destination category. Can be null. Passing null will determine the active search provider for an object type, which does not
	 *            support categories.
	 * @return the description of the active search provider which can search for the specified object type in the specified destination or null if
	 *         none
	 */
	public ISearchProviderDescription getActiveSearchProvider(IObjectTypeDescription objectType, IDestinationCategoryDescription destinationCategory)
									throws ProviderNotFoundException;

	/**
	 * Retrieves a set of all registered object types
	 * 
	 * @return a set of all registered object types or an empty set if none
	 */
	public List<IObjectTypeDescription> getObjectTypes();

	/**
	 * Retrieves a set of descriptions for all registered destination categories
	 * 
	 * @return a set of descriptions of all registered destinations or an empty set if none
	 */
	public List<IDestinationCategoryDescription> getDestinationCategories();

	/**
	 * Gets the available search subdestinations for the combination object type - destination category - search provider.
	 * 
	 * @param objectType
	 *            the object type
	 * @param destinationCategory
	 *            the destination category
	 * @param searchProvider
	 *            search provider
	 * @return a set of currently active subdestinations for the input parameters specified or an empty set if none
	 */
	public List<ISearchSubdestination> getAvailableSearchSubdestinations(final IObjectTypeDescription objectType,
									final IDestinationCategoryDescription destinationCategory, final ISearchProviderDescription searchProvider);

	/**
	 * Activates/deactivate a subdestinations for the combination object type - destination category - provider descriptions. This method should
	 * persist the enabled/disabled state in a preference store
	 * 
	 * @param searchObjectType
	 *            the object type
	 * @param destinationCategory
	 *            the destination category
	 * @param searchProviderDescription
	 *            the search provider description
	 * @param subDestination
	 *            the dubdestination to enabled/disabled
	 * @param activate
	 *            true if the subdestinations is to be activated, false otherwise
	 */
	public void activateSubdestination(final IObjectTypeDescription searchObjectType, final IDestinationCategoryDescription destinationCategory,
									final ISearchProviderDescription searchProviderDescription, final ISearchSubdestination subDestination,
									final boolean activate);

	/**
	 * Retrieves a set of category that the search destination belongs to
	 * 
	 * @param destination
	 *            the search destination
	 * @return a set of category that the search destination belongs to
	 * @throws DestinationCategoryNotFoundException
	 *             when such a destination category was not found
	 */
	public List<IDestinationCategoryDescription> getDestinationCategoriesForDestination(final ISearchDestination destination)
									throws DestinationCategoryNotFoundException;

	/**
	 * Checks whether the subdestination specified is active for the specified object type, destination category, search provider
	 * 
	 * @param subdestination
	 *            the subdestination
	 * @param objectType
	 *            the object type
	 * @param destCategory
	 *            the destination category
	 * @param searchProvider
	 *            the search provider
	 * @return true in case the subdestination is active for the specified object type, destination category, search provider; false otherwise
	 */
	public boolean isSubdestinationActive(final ISearchSubdestination subdestination, final IObjectTypeDescription objectType,
									final IDestinationCategoryDescription destCategory, final ISearchProviderDescription searchProvider);

	/**
	 * Utility method for retrieving search destinations for the destination category and destinations provider specified<br>
	 * Do note that this method will check whether the search destinations are instances of the destination class which the category expected. If this
	 * is not the case, the destination will be ignored and a warning will be logged
	 * 
	 * @param category
	 *            the destination category
	 * @param providerDescription
	 *            the search destination provider description
	 * @return a set of search destinations or an empty set if none
	 */
	public List<ISearchDestination> getSearchDestinations(final IDestinationCategoryDescription category,
									final IDestinationsProvider destinationsProvider);
	
}
