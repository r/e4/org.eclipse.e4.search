/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.xp.impl;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.platform.discovery.runtime.api.IDestinationChangeHandler;
import org.eclipse.platform.discovery.runtime.api.IDestinationsProvider;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationsProviderDescription;
import org.eclipse.platform.discovery.runtime.internal.xp.IDestinationsProviderExtensionParser;


/**
 * Implementation of the {@link IDestinationsProviderExtensionParser} interface
 * 
 * @author Danail Branekov
 * 
 */
public class DestinationsProviderExtensionParser extends AbstractExtensionPointParser<IDestinationsProviderDescription> implements IDestinationsProviderExtensionParser
{
	public static final String XP_ID = "org.eclipse.platform.discovery.runtime.destinationsprovider"; //$NON-NLS-1$
	public static final String XP_ELEMENT_NAME = "destinationsprovider"; //$NON-NLS-1$
	public static final String DEST_CATEGORY_ATTR = "destcategoryid"; //$NON-NLS-1$
	public static final String PROVIDER_INSTANCE_ATTR = "provider"; //$NON-NLS-1$
	public static final String PROVIDER_ID_ATTR = "id"; //$NON-NLS-1$
	public static final String PROVIDER_PREF_PAGE_ID = "preferencepageid"; //$NON-NLS-1$

	public DestinationsProviderExtensionParser()
	{
		this(Platform.getExtensionRegistry());
	}
	
	public DestinationsProviderExtensionParser(IExtensionRegistry registry) 
	{
		super(registry, XP_ID, XP_ELEMENT_NAME);
	}

	@Override
	protected IDestinationsProviderDescription createObject(final IConfigurationElement element)
	{
		return new IDestinationsProviderDescription() {

			private IDestinationChangeHandler changeHandler;
			
			public String getDestinationCategoryId()
			{
				return element.getAttribute(DEST_CATEGORY_ATTR);
			}

			public String getDisplayName()
			{
				return element.getAttribute(PROVIDER_INSTANCE_ATTR);
			}

			public String getId()
			{
				return element.getAttribute(PROVIDER_ID_ATTR);
			}
			
			public String getPreferencePageId()
			{
				return element.getAttribute(PROVIDER_PREF_PAGE_ID);
			}

			public IDestinationsProvider createProvider()
			{
				try
				{
					final IDestinationsProvider provider = (IDestinationsProvider) element.createExecutableExtension(PROVIDER_INSTANCE_ATTR);
					if(changeHandler != null)
					{
						provider.registerDestinationsChangeHandler(changeHandler);
					}
					
					return provider;
				} catch (CoreException e)
				{
					throw new IllegalStateException("Destinations provider could not be instantiated", e); //$NON-NLS-1$
				}
			}

			public void registerDestinationsChangeHandler(final IDestinationChangeHandler changeHandler)
			{
				this.changeHandler = changeHandler;
			}
		};
	}

}
