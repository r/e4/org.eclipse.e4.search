/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal;

import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.ISearchProviderDescription;

/**
 * Interface for configuring the active search provider for the couple object type - search destination category<br>
 * For a given couple object type - search destination category there might be at most one search provider which will be used when
 * searching for the given object type in the given destination
 * 
 * @author Danail Branekov
 */
public interface ISearchProviderActivationConfig
{
	/**
	 * Get the search provider ID which is active for the object type and destination category specified
	 * 
	 * @param objectType
	 *            the object type
	 * @param destinationCategory
	 *            the destination category
	 * @return the description of the provider which is active for the specified object type and destination
	 * @throws ProviderNotFoundException if no such provider is found
	 */
	public ISearchProviderDescription getActiveSearchProviderDescription(final IObjectTypeDescription objectType,
									final IDestinationCategoryDescription destinationCategory) throws ProviderNotFoundException;
	
	/**
	 * Gets search provider which is active for the specified object type, and does not specify supported destination categories.
	 * This method should be used when no destination category is selected and the search provider needs to be determined solely by the object type. 
	 * @throws ProviderNotFoundException if no provider is found which matches the object type, and specifies no supported destination categories.

	 */
	public ISearchProviderDescription getActiveSearchProviderDescription(final IObjectTypeDescription objectType) throws ProviderNotFoundException;

}
