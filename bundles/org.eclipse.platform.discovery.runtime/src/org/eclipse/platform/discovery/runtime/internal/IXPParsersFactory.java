/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal;

import org.eclipse.platform.discovery.runtime.internal.xp.IDestinationCategoryExtensionParser;
import org.eclipse.platform.discovery.runtime.internal.xp.IDestinationsProviderExtensionParser;
import org.eclipse.platform.discovery.runtime.internal.xp.IObjectTypeExtensionParser;
import org.eclipse.platform.discovery.runtime.internal.xp.ISearchProvidersExtensionParser;
import org.eclipse.platform.discovery.runtime.internal.xp.ISearchSubdestinationExtensionParser;

/**
 * Interface for creating instances of extension points parsers
 * @author Danail Branekov
 *
 */
public interface IXPParsersFactory {


		/**
		 * Creates an instance of {@link IDestinationCategoryExtensionParser}
		 */
		public IDestinationsProviderExtensionParser createDestinationsProviderParser();

		/**
		 * Creates an instance of {@link IDestinationCategoryExtensionParser}
		 */
		public IDestinationCategoryExtensionParser createDestinationsCategoryParser();

		/**
		 * Creates an instance of {@link IObjectTypeExtensionParser}
		 */
		public IObjectTypeExtensionParser createObjectTypeParser();

		/**
		 * Creates an instance of {@link ISearchProvidersExtensionParser}
		 */
		public ISearchProvidersExtensionParser createSearchProviderParser();

		/**
		 * Creates an instance of {@link ISearchSubdestinationExtensionParser}
		 */
		public ISearchSubdestinationExtensionParser createSubdestinationsParser();
		
}
