/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.api;

import java.util.Set;

import org.eclipse.platform.discovery.runtime.api.impl.SearchProvider;


/**
 * Interface which must be implemented by all registered search providers<br>
 * Do keep in mind that this interface might be a subject of future change. In order to avoid compilation problems it is advisable to use extend {@link SearchProvider} instead of implementing the interface directly
 * 
 * @author Danail Branekov
 */
public interface ISearchProvider
{
	/**
	 * Creates a search query with the search parameters specified
	 * @param searchParameters search parameters
	 * @return search query
	 */
	public ISearchQuery createQuery(final ISearchParameters searchParameters);

	/**
	 * Retrieves a set of {@link GroupingHierarchy} instances which represents the capabilities of the search provider to group
	 * 
	 * @param searchDestination
	 *            the selected search destination
	 * @param subdestinations
	 *            the set of selected subdestinations
	 * @return a set of supported grouping hierarchies or an empty set if none
	 */
	public Set<GroupingHierarchy> getGroupingHierarchies(final ISearchDestination searchDestination, final Set<ISearchSubdestination> subdestinations);

}
