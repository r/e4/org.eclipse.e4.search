/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.api.persistence;

import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.ui.IMemento;


/**
 * Interface for loading object from {@link IMemento}. 
 * 
 * @author Iliyan Dimov
 *
 */
public interface IMementoLoadProvider {
				
	/**
	 * Provider description of the provider.
	 */
	String getDescriptor();	
	
	/**
	 * Provides type of the child that provider can handle.
	 * @see IMemento#getChildren(String)
	 */
	String getChildType();
	
	/**
	 * Check whether object can be loaded from resource.
	 * 
	 * @param container Resource that contains the object. 
	 * @return boolean If <code>true</code> than object can be loaded.
	 */
	boolean canLoad(IMemento container);
	
	/**
	 * Loads object using the resource.
	 * 
	 * @param container Resource that contains the object. 
	 * @param opRunner long operations runner 
	 * @return 
	 * @throws MementoLoadProviderException If object 
	 * could not be loaded from container for any reason. 
	 * @throws NullPointerException If <code>container</code> is null.
	 */
	DestinationItemPair load(IMemento container, ILongOperationRunner opRunner) throws MementoLoadProviderException;
}
