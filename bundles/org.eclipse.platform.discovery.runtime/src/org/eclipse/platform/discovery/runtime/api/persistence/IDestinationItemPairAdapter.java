/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.api.persistence;

import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;

/**
 * Interface for implementations which can adapt to {@link DestinationItemPair}[]
 * @author Martina Galabova
 */
public interface IDestinationItemPairAdapter {
	DestinationItemPair[] adapt(ILongOperationRunner opRunner);
}
