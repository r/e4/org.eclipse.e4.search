/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.xp.impl;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.platform.discovery.runtime.api.ISearchProvider;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.ISearchProviderDescription;
import org.eclipse.platform.discovery.runtime.internal.xp.IDestinationCategoryExtensionParser;
import org.eclipse.platform.discovery.runtime.internal.xp.IObjectTypeExtensionParser;
import org.eclipse.platform.discovery.runtime.internal.xp.ISearchProvidersExtensionParser;


/**
 * Implementation of the {@link ISearchProvidersExtensionParser} interface
 * 
 * @author Danail Branekov
 */
public class SearchProvidersExtensionParser extends AbstractExtensionPointParser<ISearchProviderDescription> implements ISearchProvidersExtensionParser
{
	public static final String XP_ID = "org.eclipse.platform.discovery.runtime.searchprovider"; //$NON-NLS-1$
	public static final String XP_ELEMENT_NAME = "searchprovider"; //$NON-NLS-1$
	public static final String CATEGORY_ELEMENT_NAME = "category"; //$NON-NLS-1$

	public static final String PROVIDER_ID_ATTR = "id"; //$NON-NLS-1$
	public static final String PROVIDER_INSTANCE_ATTR = "instance"; //$NON-NLS-1$
	public static final String OBJECT_ID_ATTR = "objecttypeid"; //$NON-NLS-1$
	public static final String SUPPORTS_TEXT_SEARCH_ATTR = "supportstextsearch"; //$NON-NLS-1$
	public static final String CATEGORY_ID_ATTR = "categoryid"; //$NON-NLS-1$

	private final IDestinationCategoryExtensionParser destCategoryExtParser;
	private final IObjectTypeExtensionParser objTypeExtParser;

	public SearchProvidersExtensionParser()
	{
		this(Platform.getExtensionRegistry());
	}
	public SearchProvidersExtensionParser(final IDestinationCategoryExtensionParser destCategoryParser,
			final IObjectTypeExtensionParser objectTypeExtParser)
	{
		super(Platform.getExtensionRegistry(), XP_ID, XP_ELEMENT_NAME);
		this.destCategoryExtParser = destCategoryParser;
		this.objTypeExtParser = objectTypeExtParser;
	}


	public SearchProvidersExtensionParser(IExtensionRegistry extensionRegistry)
	{
		super(extensionRegistry, XP_ID, XP_ELEMENT_NAME);
		this.destCategoryExtParser = new DestinationsCategoryExtensionParser(extensionRegistry);
		this.objTypeExtParser = new ObjectTypeExtensionParser(extensionRegistry);
	}
	@Override
	protected ISearchProviderDescription createObject(final IConfigurationElement element)
	{
		final Set<IDestinationCategoryDescription> categories = getDestinationCategories(element);
		final IObjectTypeDescription objectType = getObjectTypeById(element.getAttribute(OBJECT_ID_ATTR));
		return new ISearchProviderDescription() {
			public ISearchProvider createInstance()
			{
				try
				{
					return (ISearchProvider) element.createExecutableExtension(PROVIDER_INSTANCE_ATTR);
				} catch (CoreException e)
				{
					throw new IllegalStateException("Search provider could not be instantiated", e); //$NON-NLS-1$
				}
			}

			public IObjectTypeDescription getObjectType()
			{
				return objectType;
			}

			public Set<IDestinationCategoryDescription> getSupportedDestinationCategories()
			{
				return categories;
			}

			public boolean supportsTextSearch()
			{
				return new Boolean(element.getAttribute(SUPPORTS_TEXT_SEARCH_ATTR));
			}

			public String getDisplayName()
			{
				return element.getAttribute(PROVIDER_INSTANCE_ATTR);
			}

			public String getId()
			{
				return element.getAttribute(PROVIDER_ID_ATTR);
			}
		};
	}

	private IObjectTypeDescription getObjectTypeById(final String objectTypeId)
	{
		for (IObjectTypeDescription objType : objTypeExtParser.readContributions())
		{
			if (objType.getId().equals(objectTypeId))
			{
				return objType;
			}
		}

		throw new IllegalStateException("Object type with id " + objectTypeId + " not found"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	private Set<IDestinationCategoryDescription> getDestinationCategories(final IConfigurationElement element)
	{
		final Set<IDestinationCategoryDescription> categories = new HashSet<IDestinationCategoryDescription>();
		final Set<String> supportedCategories = new HashSet<String>();
		for (IConfigurationElement catElement : element.getChildren(CATEGORY_ELEMENT_NAME))
		{
			supportedCategories.add(catElement.getAttribute(CATEGORY_ID_ATTR));
		}

		for (IDestinationCategoryDescription cat : destCategoryExtParser.readContributions())
		{
			if (supportedCategories.contains(cat.getId()))
			{
				categories.add(cat);
			}
		}

		return categories;
	}

}
