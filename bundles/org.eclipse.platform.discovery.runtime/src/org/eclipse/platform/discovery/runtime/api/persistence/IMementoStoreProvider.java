/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.api.persistence;

import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.ui.IMemento;

/**
 * Interface for storing objects into {@link IMemento}.
 * 
 * @author Iliyan Dimov
 * 
 */
public interface IMementoStoreProvider
{

	/**
	 * Provider description of the provider. The description will be used when reporting issues connected with this provider
	 */
	String getDescriptor();

	/**
	 * Check whether object can be stored.
	 * 
	 * @param resource
	 *            object to be stored.
	 * @return boolean If <code>true</code> than object can be stored.
	 */
	boolean canStore(DestinationItemPair resource);

	/**
	 * Stores object into Memento.
	 * 
	 * @param container
	 *            Store location.
	 * @param resource
	 *            object to be stored.
	 * @param opRunner
	 *            runner for performing long running operations
	 * @throws MementoStoreProviderException
	 *             If resource could not be stored in container for any reason.
	 * @throws NullPointerException
	 *             If one of the parameteres is null.
	 */
	void store(IMemento container, DestinationItemPair resource, ILongOperationRunner opRunner) throws MementoStoreProviderException;
}
