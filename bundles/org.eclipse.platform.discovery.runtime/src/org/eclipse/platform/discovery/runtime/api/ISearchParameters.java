/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.api;

import java.util.Map;
import java.util.Set;


/**
 * Search parameters used for creation of {@link ISearchQuery} instances. Implementations of this interface contain the data which the user entered in
 * the common UI (e.g. the keyword search text)
 * 
 * @author Danail Branekov
 */
public interface ISearchParameters
{
	/**
	 * The string for keyword search
	 * @return the keyword string or <code>null</code> in case keyword string is not available
	 */
	public String getKeywordString();
	
	/**
	 * Retrieve the selected grouping hierarchy
	 * @return grouping hierarchy selected or null if none
	 */
	public GroupingHierarchy getGroupingHierarchy();
	
	/**
	 * Retrieve the object type ID for which the search has been performed
	 * @return the object type id
	 */
	public String getObjectTypeId();
	
	/**
	 * Retrieve the search destination the search has been performed in 
	 * @return search destination
	 */
	public ISearchDestination getSearchDestination();
	
	/**
	 * Retrieve a set of search subdestinations which were active during the search
	 * @return a set of search subdestinations or an empty set
	 */
	public Set<ISearchSubdestination> getSubdestinations();
	
	/**
	 * Retrieves a map of custom search parameters. The framework does not control their values as these parameters are contributor specific 
	 * @return a map of custom search parameters
	 */
	public Map<Object, Object> getCustomParameters();
}
