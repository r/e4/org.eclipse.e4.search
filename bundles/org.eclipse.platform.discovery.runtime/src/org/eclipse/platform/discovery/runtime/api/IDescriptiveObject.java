/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.api;


/**
 * The parent interface for describing objects which are contributed in the framework
 * @author Danail Branekov
 */
public interface IDescriptiveObject extends IDisplayableObject 
{
	/**
	 * Gets the object id (the "id" attribute of the extension point via which the object is contributed)
	 * @return the object id
	 */
	public String getId();
}
