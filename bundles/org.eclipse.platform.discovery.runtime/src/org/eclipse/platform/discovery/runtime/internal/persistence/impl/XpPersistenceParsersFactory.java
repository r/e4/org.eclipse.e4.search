/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.persistence.impl;

import org.eclipse.platform.discovery.runtime.internal.persistence.xp.IMementoLoadProviderExtensionParser;
import org.eclipse.platform.discovery.runtime.internal.persistence.xp.IMementoStoreProviderExtensionParser;
import org.eclipse.platform.discovery.runtime.internal.persistence.xp.MementoLoadProvidersExtensionParser;
import org.eclipse.platform.discovery.runtime.internal.persistence.xp.MementoStoreProvidersExtensionParser;


/**
 * @author Iliyan Dimov
 *
 */
public class XpPersistenceParsersFactory {
	
	/**
	 * Creates an instance of {@link IMementoLoadProviderExtensionParser}
	 */
	public IMementoLoadProviderExtensionParser createMementoLoadProviderParser()
	{
		return new MementoLoadProvidersExtensionParser();
	}

	/**
	 * Creates an instance of {@link IMementoStoreProviderExtensionParser}
	 */
	public IMementoStoreProviderExtensionParser createMementoStoreProviderParser()
	{
		return new MementoStoreProvidersExtensionParser();
	}
}
