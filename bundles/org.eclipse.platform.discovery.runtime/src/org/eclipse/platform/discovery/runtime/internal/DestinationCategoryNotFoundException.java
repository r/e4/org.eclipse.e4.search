/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal;


/**
 * Doesn't provide localized message
 * */

public class DestinationCategoryNotFoundException extends Exception
{
	private static final long serialVersionUID = 8580391798913623877L;

	public DestinationCategoryNotFoundException()
	{
		super();
	}

	public DestinationCategoryNotFoundException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public DestinationCategoryNotFoundException(String message)
	{
		super(message);
	}

	public DestinationCategoryNotFoundException(Throwable cause)
	{
		super(cause);
	}
}
