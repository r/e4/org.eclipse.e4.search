/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.api;

/**
 * A search failed exception which indicates that the failure reason is known and it is not in the eclipse tools domain. Typically, this exception can be thrown under the following circumstances:
 * <ul>
 * <li>a remote system failed to process a request and returned error (e.g. HTTP 500 Internal server error). In this case the there is no point the user to check eclipse log file as the problem is obviously on the remote system</li>
 * <li>there are illegal values in a a remote system configuration. There is no point in logging the error, just tell the user to fix those values
 * </ul>
 * This exception is localized
 * 
 * @author Danail Branekov
 * 
 */
public class SearchFailedForKnownReasonException extends SearchFailedException
{
	private static final long serialVersionUID = -2194271726207483712L;
	private final String localizedMessage;

	/**
	 * Constructor. The created instance will return the localized message returned by cause's localized message
	 * 
	 * @param cause
	 *            the cause exception. Must be localized
	 */
	public SearchFailedForKnownReasonException(final Exception cause)
	{
		super(cause.getLocalizedMessage(), cause.getMessage(), cause);
		localizedMessage = cause.getLocalizedMessage();
	}
	
	public SearchFailedForKnownReasonException(final String localizedMessage, final String message)
	{
		super(localizedMessage, message);
		this.localizedMessage = localizedMessage;
	}
	
	@Override
	public String getLocalizedMessage()
	{
		return this.localizedMessage;
	}
}
