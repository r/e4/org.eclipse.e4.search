/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.impl;

import org.eclipse.platform.discovery.runtime.internal.ISearchProviderConfiguration;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDescriptionsUtil;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;

public class DescriptionsUtil implements IDescriptionsUtil
{
	private final ISearchProviderConfiguration searchProviderConfig;

	public DescriptionsUtil(final ISearchProviderConfiguration searchProviderConfig)
	{
		this.searchProviderConfig = searchProviderConfig;
	}
	
	public IObjectTypeDescription objectTypeForId(String objectTypeId) throws IllegalArgumentException
	{
		for(IObjectTypeDescription descr : searchProviderConfig.getObjectTypes())
		{
			if(descr.getId().equals(objectTypeId))
			{
				return descr;
			}
		}
		
		throw new IllegalArgumentException("Object type id not found:"+objectTypeId); //$NON-NLS-1$
	}

}
