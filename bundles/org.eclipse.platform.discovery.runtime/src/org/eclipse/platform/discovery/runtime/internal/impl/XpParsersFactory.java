/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.runtime.internal.impl;

import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.platform.discovery.runtime.internal.IXPParsersFactory;
import org.eclipse.platform.discovery.runtime.internal.xp.IDestinationCategoryExtensionParser;
import org.eclipse.platform.discovery.runtime.internal.xp.IDestinationsProviderExtensionParser;
import org.eclipse.platform.discovery.runtime.internal.xp.IObjectTypeExtensionParser;
import org.eclipse.platform.discovery.runtime.internal.xp.ISearchProvidersExtensionParser;
import org.eclipse.platform.discovery.runtime.internal.xp.ISearchSubdestinationExtensionParser;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.DestinationsCategoryExtensionParser;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.DestinationsProviderExtensionParser;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.ObjectTypeExtensionParser;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.SearchProvidersExtensionParser;
import org.eclipse.platform.discovery.runtime.internal.xp.impl.SearchSubdestinationExtensionParser;


/**
 * Factory class for creating instances of extension points parsers
 * @author Danail Branekov
 *
 */
public class XpParsersFactory implements IXPParsersFactory
{
	private final IExtensionRegistry extensionRegistry;

	public XpParsersFactory() {
		this(Platform.getExtensionRegistry());
	}
	public XpParsersFactory(IExtensionRegistry extensionRegistry) {
		this.extensionRegistry = extensionRegistry;
	}
	@Override
	public IDestinationsProviderExtensionParser createDestinationsProviderParser()
	{
		return new DestinationsProviderExtensionParser(extensionRegistry);
	}

	@Override
	public IDestinationCategoryExtensionParser createDestinationsCategoryParser()
	{
		return new DestinationsCategoryExtensionParser(extensionRegistry);
	}

	@Override
	public IObjectTypeExtensionParser createObjectTypeParser()
	{
		return new ObjectTypeExtensionParser(extensionRegistry);
	}

	@Override
	public ISearchProvidersExtensionParser createSearchProviderParser()
	{
		return new SearchProvidersExtensionParser(extensionRegistry);
	}

	@Override
	public ISearchSubdestinationExtensionParser createSubdestinationsParser()
	{
		return new SearchSubdestinationExtensionParser(extensionRegistry);
	}
	
}
