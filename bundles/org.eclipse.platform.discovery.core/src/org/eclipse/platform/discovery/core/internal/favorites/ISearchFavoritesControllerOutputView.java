/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.internal.favorites;

import java.util.Set;


/**
 * The search favorites view "controller view" interface. The controller uses this interface to make the view do something
 * @author Danail Branekov
 */
public interface ISearchFavoritesControllerOutputView
{
	/**
	 * Shows the favorites specified
	 */
	public void showFavorites(Set<Object> favorites);

	/**
	 * Notifies the user that the current content is not supported by the view
	 */
	public void showUnsupportedContentWindow();
	
	/**
	 * Notifies the user that there is currently no content to display
	 */
	public void showNoContentFoundWindow();
}
