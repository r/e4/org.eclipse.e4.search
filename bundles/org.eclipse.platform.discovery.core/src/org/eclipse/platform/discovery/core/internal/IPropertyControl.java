/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.internal;

import org.eclipse.platform.discovery.core.api.IEnablable;


/**
 * Interface for controls which allow setting and getting properties
 * 
 * @author Danail Branekov
 * 
 */
public interface IPropertyControl<T> extends IEnablable
{
	/**
	 * Sets the property value
	 * 
	 * @param value
	 *            the property value to set
	 */
	public void set(T value);

	/**
	 * Gets the property value
	 * 
	 * @return the property value
	 */
	public T get();
}
