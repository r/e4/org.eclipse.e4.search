/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.internal;

import java.util.List;

import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.platform.discovery.core.api.ISearchContext;


/**
 * @author Iliyan Dimov
 *
 */
public class ContextStructuredSelection extends StructuredSelection 
	implements IContextStructuredSelection 
{
	private final ISearchContext context;
	
	public ContextStructuredSelection(List<?> elements, ISearchContext context) 
	{
		super(elements);
			
		this.context = context;
	}
	
	@Override
	public ISearchContext getContext()
	{
		return context;		
	}
}
