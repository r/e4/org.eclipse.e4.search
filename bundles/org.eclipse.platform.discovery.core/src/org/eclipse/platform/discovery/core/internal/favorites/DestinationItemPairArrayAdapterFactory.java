/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.internal.favorites;

import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.platform.discovery.core.internal.IContextStructuredSelection;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.persistence.DestinationItemPair;
import org.eclipse.platform.discovery.runtime.api.persistence.IDestinationItemPairAdapter;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;

public class DestinationItemPairArrayAdapterFactory implements IAdapterFactory {

	public DestinationItemPairArrayAdapterFactory() 
	{
	}

	@Override
	public Object getAdapter(Object adaptableObject, @SuppressWarnings("rawtypes") Class adapterType) 
	{
		if( adapterType != IDestinationItemPairAdapter.class || !(adaptableObject instanceof IContextStructuredSelection))
		{
			return null;
		}
		return new DestinationItemPairAdapter((IContextStructuredSelection)adaptableObject);
	}

	@Override
	public Class<?>[] getAdapterList() 
	{
		return new Class[]{IDestinationItemPairAdapter.class};
	}
	
	private class DestinationItemPairAdapter implements IDestinationItemPairAdapter {
		private final IContextStructuredSelection adaptableObject;
		
		public DestinationItemPairAdapter(
				IContextStructuredSelection adaptableObject) {
			this.adaptableObject = adaptableObject; 
		}

		@Override
		public DestinationItemPair[] adapt(final ILongOperationRunner opRunner) {
			final ISearchDestination dest = adaptableObject.getContext().searchParameters().getSearchDestination();
			final Object[] items = adaptableObject.toArray();
			final DestinationItemPair[] pairs = new DestinationItemPair[items.length];
				
			for(int i = 0; i < items.length; i++)
			{
				pairs[i] = new DestinationItemPair(dest, items[i]);
			}
			return pairs;
		}
	}
}
