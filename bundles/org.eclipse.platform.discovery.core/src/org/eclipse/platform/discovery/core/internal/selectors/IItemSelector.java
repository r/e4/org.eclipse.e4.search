/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.internal.selectors;

import java.util.List;

import org.eclipse.platform.discovery.core.api.IEnablable;


/**
 * Interface for items selector which is capable of retrieving the current selection at any time via the {@link #getSelectedItem()} method
 * 
 * @author Danail Branekov
 * 
 * @param <T> the type of elements which will be returned by the selector
 * @param <K> the type of the elements which will be set to its input
 */
public interface IItemSelector<T, K> extends IEnablable
{
	/**
	 * Retrieves the item currently selected or null if no item is selected
	 */
	public T getSelectedItem();

	/**
	 * Sets the input set of items to be selected from. Input may change at any time
	 * 
	 * @param input
	 */
	public void setInput(List<K> input);
	
	/**
	 * Handle selection change
	 * @param newSelection the newly selected element or null if the selection is cleared
	 */
	public void handleSelectionChange(final T newSelection);
	
	/**
	 * Updates the selector
	 */
	public void update();
}
 