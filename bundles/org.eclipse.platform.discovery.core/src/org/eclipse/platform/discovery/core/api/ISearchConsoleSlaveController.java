/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.api;


/**
 * Interface for "slave" controllers which contribute to the search console "master" controller
 * @author Danail Branekov
 */
public interface ISearchConsoleSlaveController extends ISlaveController
{
	/**
	 * Sets the "master" controller. This method is invoked by the framework immediately after slave controller instantiation
	 * @param masterController the search console master controller
	 */
	public void setMasterController(final ISearchConsoleMasterController masterController);
}
