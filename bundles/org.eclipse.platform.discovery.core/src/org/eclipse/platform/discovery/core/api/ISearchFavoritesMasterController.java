/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.api;

import java.util.Set;

import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.platform.discovery.core.internal.ContextStructuredSelection;
import org.eclipse.platform.discovery.runtime.api.persistence.DestinationItemPair;
import org.eclipse.platform.discovery.runtime.api.persistence.IDestinationItemPairAdapter;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;

/**
 * The search favorites master controller
 * 
 * @author Danail Branekov
 * 
 */
public interface ISearchFavoritesMasterController
{
	/**
	 * Gets the discovery environment
	 */
	public IDiscoveryEnvironment getEnvironment();

	/**
	 * Deletes given set of data
	 * 
	 * @param data
	 */
	public void deleteItems(Set<Object> data);

	/**
	 * Imports given data. This can be used for import of any data that can be adapted to {@link IDestinationItemPairAdapter} or is {@link DestinationItemPair}[] itself<br>
	 * The search console framework has a predefined adapter which adapts {@link ContextStructuredSelection} to {@link DestinationItemPair}[] in order to support drag and drop from the search view to the favorites view. Extenders should implement their own adapters only in case they want to store
	 * favorite items programatically
	 * 
	 * @param data
	 * @see {@link IAdapterFactory}
	 */
	public void importData(Object data);

	/**
	 * Checks whether this data can be imported into the search favorites view
	 * 
	 * @param data
	 * @return <c>true</c> if the data can be imported and <c>false</c> otherwise
	 */
	public boolean isImportPossible(final Object data);
}
