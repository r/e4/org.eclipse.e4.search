/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.internal;

import org.eclipse.platform.discovery.core.internal.console.ISearchConsoleController;
import org.eclipse.platform.discovery.core.internal.console.ISearchConsoleControllerOutputView;


/**
 * The "view" interface for the model-view-controller architecture of the search console
 * 
 * @see SearchConsoleController
 * @author Danail Branekov
 */
public interface ISearchConsoleView extends IDiscoveryView<ISearchConsoleControllerOutputView, ISearchConsoleController>
{
	/**
	 * Sets the default session ID
	 * @param sessionId
	 */
	public void setDefaultSessionId(final String sessionId);
	
	public void setDestinationsManager(final IDestinationsManager destManager);
}
