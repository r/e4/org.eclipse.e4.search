/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.internal.favorites;

import java.io.IOException;
import java.util.Set;

import org.eclipse.platform.discovery.runtime.api.persistence.DestinationItemPair;
import org.eclipse.platform.discovery.runtime.internal.persistence.MementoContentManagerException;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.ui.WorkbenchException;


/**
 * Interface for reading, adding and deleting items from the persistence store used for search favorites items
 * 
 * @author Danail Branekov
 */
public interface IPersistenceUtil
{
	/**
	 * Loads all items
	 */
	public Set<DestinationItemPair> loadItems(final ILongOperationRunner opRunner) throws WorkbenchException, IOException, MementoContentManagerException;

	/**
	 * Deletes the items specified
	 */
	public void deleteItems(final Set<Object> itemsToDelete, final ILongOperationRunner opRunner) throws WorkbenchException, IOException, MementoContentManagerException;

	/**
	 * Adds the items specified to the persistence store. The items which are currently stored in the persistence store remain unchanged
	 */
	public void addItems(final Set<DestinationItemPair> itemsToAdd, final ILongOperationRunner opRunner) throws WorkbenchException, IOException, MementoContentManagerException;
}