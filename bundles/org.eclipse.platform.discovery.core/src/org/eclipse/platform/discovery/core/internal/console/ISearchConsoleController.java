/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.internal.console;

import org.eclipse.platform.discovery.core.api.ISearchConsoleMasterController;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.ISearchSubdestination;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;

/**
 * The search console controller interface which deals with internal search console changes (such as changing user selection)
 * 
 * @author Danail Branekov
 */
public interface ISearchConsoleController extends ISearchConsoleMasterController
{
	/**
	 * Handles "search for" object type selection
	 * 
	 * @param selectedObject
	 *            the selected object
	 */
	public void objectTypeSelected(final IObjectTypeDescription selectedObject);

	/**
	 * Notifies the controller that the available search destinations changed. Implementations should typically reload them
	 */
	public void searchDestinationsChanged();

	/**
	 * Notifies the controller that subdestinations activation configuration has been changed
	 * 
	 * @param searchObjectType
	 *            the currently selected search object type
	 * @param searchDestination
	 *            the currently selected search destination
	 * @param subDestination
	 *            the subdestination which changed
	 * @param newActivationState
	 *            true if the subdestination has been enabled, false otherwise
	 */
	public void subdestinationActivationChanged(final IObjectTypeDescription searchObjectType, final ISearchDestination searchDestination, final ISearchSubdestination subDestination,
									final boolean newActivationState);

	/**
	 * Notifies the controller that a search destination has been selected
	 * 
	 * @param objectType
	 *            the currently selected object type
	 * @param selectedDestination
	 *            the newly selected search destination
	 */
	public void searchDestinationSelected(final IObjectTypeDescription objectType, final ISearchDestination selectedDestination);
}
