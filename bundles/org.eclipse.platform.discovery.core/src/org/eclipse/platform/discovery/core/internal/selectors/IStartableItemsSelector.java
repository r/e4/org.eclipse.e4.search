/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.internal.selectors;

/**
 * Interface for selectors which are started programatically via invoking the {@link #select()} method  
 * @author Danail Branekov
 *
 * @param <T>
 */
public interface IStartableItemsSelector<T>
{
	/**
	 * Start the selection process, e.g. via some user interaction
	 */
	public void select();
	
	/**
	 * Handles an item selection
	 * 
	 * @param item
	 *            the item
	 * @param selected
	 *            true if the item is selected, false otherwise
	 */
	public void handleSelection(T item, boolean selected);
}
