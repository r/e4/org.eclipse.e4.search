/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.api;

import org.eclipse.platform.discovery.runtime.api.SearchFailedException;
import org.eclipse.platform.discovery.runtime.internal.ProviderNotFoundException;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;

/**
 * The search console controller interface. {@link ISearchConsoleSlaveController} instances are aware of the master controller
 * 
 * @author Danail Branekov
 */
public interface ISearchConsoleMasterController
{
	public IDiscoveryEnvironment getEnvironment();

	/**
	 * Starts a search
	 * 
	 * @param event
	 *            the search event which carries information on search parameters
	 * 
	 * @see org.eclipse.platform.discovery.ui.api.impl.DefaultSessionIds
	 * @see SearchEvent
	 * @throws ProviderNotFoundException
	 *             if a search provider capable of performing the search has not been found
	 * @throws SearchFailedException
	 *             if the search failed
	 */
	public void search(final SearchEvent event) throws ProviderNotFoundException, SearchFailedException;
}
