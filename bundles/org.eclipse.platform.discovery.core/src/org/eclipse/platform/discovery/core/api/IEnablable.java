/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.api;

/**
 * Interface for indicating that something can be enabled or disabled
 * @author Danail Branekov
 */
public interface IEnablable
{
	/**
	 * Sets the enabled state
	 * @param enabled 
	 */
	public void setEnabled(final boolean enabled);
	
	/**
	 * @return <code>true</code> in case enabled, <code>false</code> otherwise
	 */
	public boolean isEnabled();
}
