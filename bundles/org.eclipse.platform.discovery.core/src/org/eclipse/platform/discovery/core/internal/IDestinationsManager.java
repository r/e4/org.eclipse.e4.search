package org.eclipse.platform.discovery.core.internal;

import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.swt.widgets.Shell;

public interface IDestinationsManager {
	/**
	 * Opens a preference dialog to manage destinations. It contains all registered preference pages, and a preference page pre-selected based on the
	 * current destination category selected.<br>
	 * This method can block and must be called in the UI thread.
	 * @param selectedDestCategory can be null
	 * @param searchConfig
	 */
	public void manageDestinations(Shell parentShell, IDestinationCategoryDescription selectedDestCategory);
}
