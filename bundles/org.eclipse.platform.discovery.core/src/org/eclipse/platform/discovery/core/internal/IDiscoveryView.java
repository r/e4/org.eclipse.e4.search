/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.internal;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;


/**
 * Interface for all views in the discovery framework
 * @author Danail Branekov
 */
public interface IDiscoveryView<O, C>
{
	public void setEnvironment(final IDiscoveryEnvironment env);
	
	/**
	 * Gets the progress monitor of the view. Callers can use this progress monitor to report some work progress
	 * @return progress monitor instance
	 */
	public IProgressMonitor getProgressMonitor();
	
	/**
	 * Notifies the view that the initialization has been completed and the view is ready to be used
	 */
	public void initializationCompleted();
	
	/**
	 * Gets the view interface visible for controller
	 * @return
	 */
	public O getControllerView();

	/**
	 * Registers controller interface visible for view
	 * @param controller
	 */
	public void registerController(C controller);
}
