/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.api;

import org.eclipse.platform.discovery.runtime.api.ISearchParameters;

/**
 * Event which indicates that a search is to be started
 * 
 * @author Danail Branekov
 */
public class SearchEvent
{
	private final ISearchParameters searchParameters;

	private final String session;

	private final String searchTitle;

	private final String searchDescription;

	/**
	 * Constructor
	 * 
	 * @param searchParameters
	 *            the parameters of the search
	 * @param session
	 *            the ID of the search session
	 * @param searchTitle
	 *            the title of the search. This value is used as tab title in case the result is displayed in a new tab
	 * @param searchDescription
	 *            a description of the search or null. This value is going to be used in a label above search results.
	 * 
	 * @see org.eclipse.platform.discovery.ui.api.impl.DefaultSessionIds
	 */
	public SearchEvent(final ISearchParameters searchParameters, String session, String searchTitle, String searchDescription)
	{
		this.searchParameters = searchParameters;
		this.session = session;
		this.searchDescription = searchDescription;
		this.searchTitle = searchTitle;
	}

	/**
	 * Gets the search parameters
	 */
	public ISearchParameters getSearchParameters()
	{
		return searchParameters;
	}

	public String sessionId()
	{
		return session;
	}

	public String searchTitle()
	{
		return searchTitle;
	}

	public String searchDescription()
	{
		return searchDescription;
	}

}
