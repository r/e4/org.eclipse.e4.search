/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.api;

import java.util.Set;

import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;

/**
 * Interface for actions contributed to the search console. Do note that this interface represents only the logic which would be executed upon running
 * the action. This logic is later on installed by the framework
 * 
 * @see org.eclipse.platform.discovery.ui.api.IGenericViewCustomization
 * @author Danail Branekov
 * 
 */
public interface IContributedAction
{
	/**
	 * Performs the action
	 * 
	 * @param operationRunner
	 *            runner which can be used to run long running operations
	 * @param selectedObjects
	 *            a set of selected objects upon which the action has been started
	 */
	public void perform(final ILongOperationRunner operationRunner, final Set<Object> selectedObjects);

	/**
	 * Gets the action ID
	 * 
	 * @return action ID;
	 */
	public Object getActionId();
}
