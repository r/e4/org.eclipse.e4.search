/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.internal.favorites;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.platform.discovery.runtime.api.persistence.DestinationItemPair;
import org.eclipse.platform.discovery.runtime.internal.persistence.IMementoContentManager;
import org.eclipse.platform.discovery.runtime.internal.persistence.MementoContentManagerException;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.XMLMemento;


/**
 * Implementation of the {@link IPersistenceUtil} interface to be used by {@link SearchFavoritesController}
 * 
 * @author Danail Branekov
 */
public class SearchFavoritesPersistenceUtil implements IPersistenceUtil
{
	/**
	 * Context for working with {@link IMementoContentManager}
	 * @author Danail Branekov
	 *
	 * @param <T> the type of the object which will be stored/loaded
	 */
	public interface IPersistenceContext<T>
	{
		/**
		 * Retrieves the content manager which is responsible for storing/loading objects
		 * @return {@link IMementoContentManager} instance
		 * @throws MementoContentManagerException
		 */
		public IMementoContentManager<T> getContentManager() throws MementoContentManagerException;

		/**
		 * The file which is used for objects storage
		 */
		public File getFile();
	}
	
	private final IPersistenceContext<DestinationItemPair> persistenceContext;

	public SearchFavoritesPersistenceUtil(final IPersistenceContext<DestinationItemPair> persistenceContext)
	{
		this.persistenceContext = persistenceContext;
	}

	@Override
	public void addItems(final Set<DestinationItemPair> itemsToAdd, final ILongOperationRunner opRunner) throws WorkbenchException, IOException, MementoContentManagerException
	{
		final Set<DestinationItemPair> originalItems = loadItems(opRunner);
		originalItems.addAll(itemsToAdd);

		// store data
		final IMemento tempContentStorage = provideStorageMemento();
		persistenceContext.getContentManager().saveContent(tempContentStorage, originalItems, opRunner);
		saveMemento(tempContentStorage);
	}

	@Override
	public void deleteItems(final Set<Object> itemsToDelete, final ILongOperationRunner opRunner) throws WorkbenchException, IOException, MementoContentManagerException
	{
		final Set<DestinationItemPair> loadedPairs = loadItems(opRunner);
		final Set<DestinationItemPair> pairsToDelete = new HashSet<DestinationItemPair>();
		for(DestinationItemPair pair : loadedPairs)
		{
			if(itemsToDelete.contains(pair.getItem()))
			{
				pairsToDelete.add(pair);
			}
		}
		loadedPairs.removeAll(pairsToDelete);
		
		// store result items
		final IMemento mementoStorage = createStorageMemento();
		persistenceContext.getContentManager().saveContent(mementoStorage, loadedPairs, opRunner);
		saveMemento(mementoStorage);
	}

	protected void saveMemento(final IMemento memento) throws IOException
	{
		assert memento instanceof XMLMemento;
		((XMLMemento) memento).save(new FileWriter(persistenceContext.getFile()));
	}

	@Override
	public Set<DestinationItemPair> loadItems(final ILongOperationRunner opRunner) throws WorkbenchException, IOException, MementoContentManagerException
	{
		final IMemento tempContentStorage = provideStorageMemento();
		return new HashSet<DestinationItemPair>(persistenceContext.getContentManager().loadContent(tempContentStorage, opRunner));
	}

	protected IMemento provideStorageMemento() throws IOException, WorkbenchException
	{
		if (persistenceContext.getFile().exists())
		{
			return XMLMemento.createReadRoot(new FileReader(persistenceContext.getFile()));
		}

		return createStorageMemento();
	}

	protected IMemento createStorageMemento()
	{
		return XMLMemento.createWriteRoot("memento-root"); //$NON-NLS-1$
	}
}
