/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.internal.console;

import java.util.List;

import org.eclipse.platform.discovery.core.api.ISearchContext;
import org.eclipse.platform.discovery.runtime.api.GroupingHierarchy;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;

/**
 * The search console "output" view. This interface is used by the controller to make the view do something
 * @author Danail Branekov
 */
public interface ISearchConsoleControllerOutputView
{
	/**
	 * Show the specified set of object types
	 * 
	 * @param objectTypes
	 *            a set of object types to be shown by the view
	 */
	public void showObjectTypes(final List<IObjectTypeDescription> objectTypes);

	/**
	 * Show the destination categories specified
	 * 
	 * @param destinations
	 *            a set of destinations to be displayed by the view
	 */
	public void showDestinationsCategories(final List<IDestinationCategoryDescription> destinationCategories);

	/**
	 * Show the result of the search query
	 * 
	 * @param searchContext
	 *            the search context to be displayed
	 */
	public void showResult(final ISearchContext searchContext);	
	
	/**
	 * Updates the search destinations selector after a destinations change
	 */
	public void updateDestinationsSelector();
	
	/**
	 * Shows the grouping hierarchies specified
	 * @param groupingHierarchies a set of hierarchies. never null; 
	 */
	public void showGroupingHierarchies(final List<GroupingHierarchy> groupingHierarchies);
	
	/**
	 * Sets the string specified as status message
	 * @param message
	 */
	public void setStatusMessage(final String message);
}
