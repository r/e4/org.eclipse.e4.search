/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.internal.favorites;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdapterManager;
import org.eclipse.core.runtime.Platform;
import org.eclipse.platform.discovery.core.api.ISearchFavoritesMasterController;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.persistence.DestinationItemPair;
import org.eclipse.platform.discovery.runtime.api.persistence.IDestinationItemPairAdapter;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;


public class SearchFavoritesController implements ISearchFavoritesMasterController {

	private final ISearchFavoritesControllerOutputView favoritesView;
	private final IDiscoveryEnvironment environment;
	private final IPersistenceUtil persistenceUtil;

	public SearchFavoritesController(ISearchFavoritesControllerOutputView view,
			IDiscoveryEnvironment env, IPersistenceUtil persistenceUtil) {
		this.favoritesView = view;
		this.environment = env;
		this.persistenceUtil = persistenceUtil;
		updateView();
	}

	private void updateView() {
		favoritesView.showFavorites(loadItems());
	}

	@Override
	public IDiscoveryEnvironment getEnvironment() {
		return environment;
	}


	@Override
	public void deleteItems(Set<Object> data) 
	{
		try 
		{
			persistenceUtil.deleteItems(data, environment.operationRunner());
		}
		catch (Exception e) 
		{
			getEnvironment().errorHandler().handleException(e);
		}
		updateView();
	}

	@Override
	public void importData(Object data) 
	{
		//if dropped data is null we should not search for adapters for it.
		if(data == null)
		{
			favoritesView.showUnsupportedContentWindow();
			return;
		}
		
		final DestinationItemPair[] pairs;
		try 
		{
			pairs = toPairs(data);
		}
		catch(Exception e) {
			environment.errorHandler().handleException(e);
			return;
		}
		
		if(pairs == null)
		{
			favoritesView.showUnsupportedContentWindow();
			return;
		}
		
		//favorites content is empty
		if(pairs.length == 0)
		{
			favoritesView.showNoContentFoundWindow();
			return;
		}
		
		saveInFavorites(pairs);
		updateView();
	}
	
	/**
	 * Converts that data specified to a collection of {@link DestinationItemPair}
	 * @return collection of {@link DestinationItemPair} or null if the data is not supported 
	 */
	private DestinationItemPair[] toPairs(final Object data)
	{
		if(data instanceof DestinationItemPair[])
		{
			return (DestinationItemPair[])data;
		}
		
		return adaptToPairs(data);
	}
	
	private DestinationItemPair[] adaptToPairs(final Object data)
	{
		final IDestinationItemPairAdapter adapter = adapterFor(data);
		if (adapter == null) {
			return null;
		}
		final DestinationItemPair[] pairs = adapter.adapt(environment.operationRunner());
		return pairs;
	}
	
	@Override
	public boolean isImportPossible(Object data) 
	{
		return adapterFor(data) != null;
	}

	private void saveInFavorites(final DestinationItemPair[] pairs) 
	{
		final Map<ISearchDestination, Set<DestinationItemPair>> destinationsMap = groupByDestination(pairs);
		try {
			for (ISearchDestination searchDestination : destinationsMap.keySet()) 
			{
				persistenceUtil.addItems(destinationsMap.get(searchDestination), environment.operationRunner());
			}
		}
		catch (Exception e) {
			getEnvironment().errorHandler().handleException(e);
		}
	}

	private Map<ISearchDestination, Set<DestinationItemPair>> groupByDestination(DestinationItemPair[] pairs) 
	{
		final Map<ISearchDestination, Set<DestinationItemPair>> destinationsMap = new HashMap<ISearchDestination, Set<DestinationItemPair>>();

		for (DestinationItemPair importedData : pairs) 
		{
			final ISearchDestination destination = importedData.getDestination();
			if (!destinationsMap.containsKey(destination)) 
			{
				destinationsMap.put(destination, new HashSet<DestinationItemPair>());
			}
			destinationsMap.get(destination).add(importedData);
		}
		return destinationsMap;
	}

	private IDestinationItemPairAdapter adapterFor(final Object object) 
	{
		final IDestinationItemPairAdapter adapter = (IDestinationItemPairAdapter) getAdapterManager().getAdapter(object, IDestinationItemPairAdapter.class);
		return adapter == null ? (IDestinationItemPairAdapter) getAdapterManager()
				.loadAdapter(object, IDestinationItemPairAdapter.class.getName())
				: adapter;
	}

	protected IAdapterManager getAdapterManager() {
		return Platform.getAdapterManager();
	}

	private Set<Object> loadItems()
	{
		final Set<Object> loadedItems = new HashSet<Object>();
		try 
		{
			for(DestinationItemPair pair : persistenceUtil.loadItems(getEnvironment().operationRunner()))
			{
				loadedItems.add(pair.getItem());
			}
		}
		catch (Exception e) {
			getEnvironment().errorHandler().handleException(e);
			return Collections.emptySet();
		}
		return loadedItems;
	}
}
