/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.internal;

import java.util.Collection;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.platform.discovery.core.api.IEnablable;

/**
 * Interface for selectors which returns only one item at a time - for example combo boxes
 * @author Mladen Tomov
 *
 * @param <T>
 */
public interface ISingleValueSelector<T>  extends IEnablable
{

	/**
	 * 	Returns selected item
	 */
	public T getSelectedItem();
	
	/**
	 * */
	public void setInput(Collection<T> input);
	
	/**
	 * Handles an item selection
	 * 
	 * @param item
	 *            the item
	 *            
	 */
	public void registerSelectionChangedListener(ISelectionChangedListener listener);
}
