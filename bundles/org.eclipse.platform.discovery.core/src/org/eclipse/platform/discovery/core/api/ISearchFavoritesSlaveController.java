/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.api;

/**
 * Interface for contributed to search favorites master controller slave controllers
 * @author Danail Branekov
 */
public interface ISearchFavoritesSlaveController extends ISlaveController
{
	/**
	 * Sets the search favorites master controller. The framework will call this method immediately after slave controller creation 
	 * @param masterController a search favorites master controller instance
	 */
	public void setMasterController(final ISearchFavoritesMasterController masterController);
}
