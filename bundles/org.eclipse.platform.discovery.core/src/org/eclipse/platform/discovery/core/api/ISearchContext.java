/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.api;

import java.util.Map;

import org.eclipse.platform.discovery.core.internal.ISearchSession;
import org.eclipse.platform.discovery.runtime.api.ISearchParameters;


/**
 * Interface for describing a search context
 * @author Danail Branekov
 */
public interface ISearchContext
{
	/**
	 * The very search result 
	 */
	public Object searchResult();
	
	/**
	 * The search provider description which created the search result
	 */
	public String searchProviderId();
	
	/**
	 * The search parameters the search has been performed with
	 */
	public ISearchParameters searchParameters();

	public ISearchSession session();

	public String title();

	public String description();

	public Map<Object, Object> data();
}
