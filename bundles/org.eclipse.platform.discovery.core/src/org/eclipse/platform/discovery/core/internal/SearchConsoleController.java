/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.internal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.eclipse.platform.discovery.core.api.ISearchConsoleMasterController;
import org.eclipse.platform.discovery.core.api.ISearchContext;
import org.eclipse.platform.discovery.core.api.SearchEvent;
import org.eclipse.platform.discovery.core.internal.console.ISearchConsoleController;
import org.eclipse.platform.discovery.core.internal.console.ISearchConsoleControllerOutputView;
import org.eclipse.platform.discovery.core.internal.events.handlers.ISearchEventHandler;
import org.eclipse.platform.discovery.core.internal.plugin.text.DiscoveryCoreMessages;
import org.eclipse.platform.discovery.runtime.api.GroupingHierarchy;
import org.eclipse.platform.discovery.runtime.api.ISearchDestination;
import org.eclipse.platform.discovery.runtime.api.ISearchParameters;
import org.eclipse.platform.discovery.runtime.api.ISearchProvider;
import org.eclipse.platform.discovery.runtime.api.ISearchQuery;
import org.eclipse.platform.discovery.runtime.api.ISearchSubdestination;
import org.eclipse.platform.discovery.runtime.api.SearchCancelledException;
import org.eclipse.platform.discovery.runtime.api.SearchFailedException;
import org.eclipse.platform.discovery.runtime.internal.DestinationCategoryNotFoundException;
import org.eclipse.platform.discovery.runtime.internal.ISearchProviderConfiguration;
import org.eclipse.platform.discovery.runtime.internal.ProviderNotFoundException;
import org.eclipse.platform.discovery.runtime.internal.impl.DescriptionsUtil;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDescriptionsUtil;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IDestinationCategoryDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.IObjectTypeDescription;
import org.eclipse.platform.discovery.runtime.internal.model.descriptions.ISearchProviderDescription;
import org.eclipse.platform.discovery.util.api.env.IDiscoveryEnvironment;
import org.eclipse.platform.discovery.util.internal.session.ISessionManager;


/**
 * The controller in the model-view-controller architecture of the search console
 * @see ISearchConsoleView
 * @author Danail Branekov
 */
public class SearchConsoleController implements ISearchConsoleMasterController, ISearchConsoleController
{
	private final ISearchProviderConfiguration searchProviderConfiguration;
	private final ISearchConsoleControllerOutputView view;
	private final IDescriptionsUtil descriptionsUtil;
	private final IDiscoveryEnvironment consoleEnv;
	private final ISessionManager<ISearchSession> sessionManager;
	private final String defaultSessionId;
	
	/**
	 * Constructor
	 * @param providerConfiguration search provider configuration 
	 * @param searchConsoleView the view
	 * @param errorHandler handler for handling errors
	 * @param slaveControllersConfig slave controllers configuration
	 * @param consoleEnv search console environment
	 */
	public SearchConsoleController(final ISearchProviderConfiguration providerConfiguration, final ISearchConsoleControllerOutputView searchConsoleView, final IDiscoveryEnvironment consoleEnv, ISessionManager<ISearchSession> sessionManager, final String defaultSessionId)
	{
		this.searchProviderConfiguration = providerConfiguration;
		this.view = searchConsoleView;
		this.descriptionsUtil = new DescriptionsUtil(this.searchProviderConfiguration);
		this.consoleEnv = consoleEnv;
		this.sessionManager = sessionManager;
		this.defaultSessionId = defaultSessionId;
		view.showObjectTypes(this.searchProviderConfiguration.getObjectTypes());
	}
	
	@Override
	public void objectTypeSelected(final IObjectTypeDescription selectedObject)
	{
		if(selectedObject == null) return;
		final List<IDestinationCategoryDescription> destinationCategories = searchProviderConfiguration.getAvailableDestinationCategoriesForObjectType(selectedObject);
		view.showDestinationsCategories(destinationCategories);
	}
	
	@Override
	public void search(final SearchEvent event) throws ProviderNotFoundException, SearchFailedException
	{
		createSearchEventHandler().handleSearch(event);
	}
	
	protected ISearchEventHandler createSearchEventHandler()
	{
		return new SearchEventHandler();
	}
	
	@Override
	public void searchDestinationsChanged()
	{
		view.updateDestinationsSelector();
	}
	
	@Override
	public void searchDestinationSelected(final IObjectTypeDescription objectType, final ISearchDestination selectedDestination)
	{
		updateGroupHierarchies(objectType, selectedDestination);
	}
	
	@Override
	public void subdestinationActivationChanged(IObjectTypeDescription searchObjectType, ISearchDestination searchDestination, ISearchSubdestination subDestination, boolean newActivationState)
	{
		updateGroupHierarchies(searchObjectType, searchDestination);		
	}

	private IDestinationCategoryDescription getDestinationCategory(ISearchDestination destination)
	{
		try
		{
			return searchProviderConfiguration.getDestinationCategoriesForDestination(destination).iterator().next();
		} catch (DestinationCategoryNotFoundException e)
		{
			throw new IllegalStateException(e);
		}
	}
	
	private List<ISearchSubdestination> filterActiveSubdestinations(final List<ISearchSubdestination> allSubdestinations, final IObjectTypeDescription objectType, final IDestinationCategoryDescription destCategory, ISearchProviderDescription searchProvider)
	{
		final List<ISearchSubdestination> result = new ArrayList<ISearchSubdestination>();
		for(ISearchSubdestination subDest : allSubdestinations)
		{
			if(searchProviderConfiguration.isSubdestinationActive(subDest, objectType, destCategory, searchProvider))
			{
				result.add(subDest);
			}
		}
		
		return result;
	}
	
	private void updateGroupHierarchies(final IObjectTypeDescription objectType, final ISearchDestination destination)
	{
		if(objectType != null && destination != null)
		{
			view.showGroupingHierarchies(determineGroupingHierarchy(objectType, destination));
		}
		else
		{
			view.showGroupingHierarchies(new ArrayList<GroupingHierarchy>());
		}
	}
	
	private List<GroupingHierarchy> determineGroupingHierarchy(final IObjectTypeDescription objectType, final ISearchDestination destination)
	{
		try
		{
			final ISearchProviderDescription searchProviderDescr = searchProviderConfiguration.getActiveSearchProvider(objectType, getDestinationCategory(destination));
			final ISearchProvider providerInstance = searchProviderDescr.createInstance();
			final List<ISearchSubdestination> allSubdestintations = searchProviderConfiguration.getAvailableSearchSubdestinations(objectType, getDestinationCategory(destination), searchProviderDescr);
			final List<ISearchSubdestination> activeSubdestinations = filterActiveSubdestinations(allSubdestintations, objectType, getDestinationCategory(destination), searchProviderDescr);
			
			return new ArrayList<GroupingHierarchy>(providerInstance.getGroupingHierarchies(destination, new HashSet<ISearchSubdestination>(activeSubdestinations)));
		}
		catch(ProviderNotFoundException e)
		{
			throw new IllegalStateException(e);
		}
	}

	@Override
	public IDiscoveryEnvironment getEnvironment() {
		return consoleEnv;
	}
	
	private class SearchEventHandler implements ISearchEventHandler
	{
		public void handleSearch(final SearchEvent event) throws ProviderNotFoundException, SearchFailedException
		{
			ISearchDestination destination = event.getSearchParameters().getSearchDestination();
			IDestinationCategoryDescription category;
			if(destination!=null)
			{
				final List<IDestinationCategoryDescription> destCategories = destinationCategories(event.getSearchParameters().getSearchDestination());
				category = destCategories.iterator().next(); 
			}
			else
			{
				category=null;
			}
			
			final IObjectTypeDescription objectType = descriptionsUtil.objectTypeForId(event.getSearchParameters().getObjectTypeId());
			final ISearchProviderDescription searchProvider = searchProvider(objectType, category);
			
			final ISearchQuery searchQuery = createQuery(event.getSearchParameters(), searchProvider);
			
			try
			{
				final ISearchContext sc = executeQuery(searchQuery, searchProvider, event);
				view.showResult(sc);
			}
			catch(SearchCancelledException e)
			{
				view.setStatusMessage(DiscoveryCoreMessages.SearchConsoleController_SEARCH_CANCELLED);
			}
		}
		
		private ISearchContext executeQuery(final ISearchQuery searchQuery, final ISearchProviderDescription searchProvider, final SearchEvent event) throws SearchFailedException
		{
			final Object result = searchQuery.execute(consoleEnv.operationRunner());
			final ISearchContext sc = new ISearchContext()
			{
				private Map<Object, Object> data = new HashMap<Object, Object>();
				public ISearchParameters searchParameters()
				{
					return event.getSearchParameters();
				}

				public String searchProviderId()
				{
					return searchProvider.getId();
				}

				public Object searchResult()
				{
					return result;
				}

				@Override
				public ISearchSession session() {
					return sessionManager.session(event.sessionId(), historySize(event.sessionId()));
				}

				@Override
				public String description() {
					return event.searchDescription();
				}

				@Override
				public String title() {
					return event.searchTitle();
				}

				@Override
				public Map<Object, Object> data() {
					return data;
				}
				
				private int historySize(final String sessionId)
				{
					return sessionId.equals(defaultSessionId) ? 1 : 5;
				}
			};
			sc.session().historyTrack().track(sc);
			return sc;
		}
		
		private List<IDestinationCategoryDescription> destinationCategories(final ISearchDestination destination)
		{
			final List<IDestinationCategoryDescription> destCategories;
			try
			{
				destCategories = searchProviderConfiguration.getDestinationCategoriesForDestination(destination);
			} catch (DestinationCategoryNotFoundException e)
			{
				throw new IllegalStateException(e);
			}
			if (destCategories.size() != 1)
			{
				throw new IllegalStateException("One destination category expected, found " + destCategories.size()); //$NON-NLS-1$
			}

			return destCategories;
		}

		private ISearchQuery createQuery(final ISearchParameters searchParams, final ISearchProviderDescription searchProvider) throws ProviderNotFoundException
		{
			final ISearchProvider searchProviderInstance = searchProvider.createInstance();
			return searchProviderInstance.createQuery(searchParams);
		}
		
		private ISearchProviderDescription searchProvider(final IObjectTypeDescription objectType, final IDestinationCategoryDescription category) throws ProviderNotFoundException
		{
			return searchProviderConfiguration.getActiveSearchProvider(objectType, category);
		}
	}
}
