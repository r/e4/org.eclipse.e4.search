/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.internal.events.handlers;

import org.eclipse.platform.discovery.core.api.SearchEvent;
import org.eclipse.platform.discovery.runtime.api.SearchFailedException;
import org.eclipse.platform.discovery.runtime.internal.ProviderNotFoundException;


/**
 * Handler for handling a search event
 * 
 * @author Danail Branekov
 */
public interface ISearchEventHandler
{
	/**
	 * Handle the search event specified
	 * 
	 * @param event
	 *            the event to handle
	 * @throws ProviderNotFoundException
	 *             when handler could not determine the search provider which search should be invoked as a reaction to the event
	 * @throws SearchFailedException when the search failed
	 */
	public void handleSearch(final SearchEvent event) throws ProviderNotFoundException, SearchFailedException;
}
