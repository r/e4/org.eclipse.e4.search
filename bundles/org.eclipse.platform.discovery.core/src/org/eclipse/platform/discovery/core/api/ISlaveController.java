/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.api;

import java.util.Set;

/**
 * Interface for slave controllers
 * @author Danail Branekov
 */
public interface ISlaveController
{
	/**
	 * Creates a set of {@link IContributedAction} which are to be contributed to the search console
	 * @return set of actions; must not be null
	 */
	public Set<IContributedAction> createActions();
}
