/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.internal.plugin.text;

import org.eclipse.osgi.util.NLS;

public class DiscoveryCoreMessages
{
	private static final String BUNDLE_NAME = "org.eclipse.platform.discovery.core.internal.plugin.text.DiscoveryCoreMessages"; //$NON-NLS-1$

	public static String SEARCH_IN_PROGRESS;
	
	public static String SearchConsoleController_SEARCH_CANCELLED;
	
	static
	{
		NLS.initializeMessages(BUNDLE_NAME, DiscoveryCoreMessages.class);
	}

}
