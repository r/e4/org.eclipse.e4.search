/*******************************************************************************
 * Copyright (c) 2010 SAP AG, Walldorf.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     SAP AG - initial API and implementation
 *******************************************************************************/
package org.eclipse.platform.discovery.core.internal.favorites;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import org.eclipse.platform.discovery.runtime.api.persistence.DestinationItemPair;
import org.eclipse.platform.discovery.runtime.api.persistence.IMementoLoadProvider;
import org.eclipse.platform.discovery.runtime.api.persistence.IMementoStoreProvider;
import org.eclipse.platform.discovery.runtime.api.persistence.MementoLoadProviderException;
import org.eclipse.platform.discovery.runtime.api.persistence.MementoStoreProviderException;
import org.eclipse.platform.discovery.runtime.internal.persistence.IMementoContentManager;
import org.eclipse.platform.discovery.runtime.internal.persistence.MementoContentManagerException;
import org.eclipse.platform.discovery.runtime.internal.persistence.util.IPersistenceProviderRegistry;
import org.eclipse.platform.discovery.util.api.longop.ILongOperationRunner;
import org.eclipse.platform.discovery.util.internal.ContractChecker;
import org.eclipse.platform.discovery.util.internal.logging.ILogger;
import org.eclipse.platform.discovery.util.internal.logging.Logger;
import org.eclipse.ui.IMemento;


/**
 * SearchFavoritesContentManager provides interface to manage content 
 * that should be loaded or saved.
 * 
 * Each item that is expected to be load or save at most one correspondent 
 * <code>IMementoLoadProvider</code> or <code>IMementoSaveProvider</code> 
 * should be provided through the designated extension point. 
 * 
 * If the provider is not found than the load or save
 * of the particular object is skipped. 
 * 
 * @author Iliyan Dimov
 *
 */
public class SearchFavoritesContentManager implements IMementoContentManager<DestinationItemPair>  
{		
	private final IPersistenceProviderRegistry providerRegistry;
	private final Collection<IMementoLoadProvider> loadProviderCollection;	
	private final Collection<IMementoStoreProvider> storeProviderCollection;	

	private final ILogger logger;
	
	public SearchFavoritesContentManager(IPersistenceProviderRegistry providerRegistry)
		throws MementoContentManagerException
	{
		this.providerRegistry = providerRegistry;
		this.loadProviderCollection = this.providerRegistry.collectAllLoadProviders();		
		this.storeProviderCollection = this.providerRegistry.collectAllStoreProviders();			
		this.logger = Logger.instance();
	}
	
	@Override
	public Collection<DestinationItemPair> loadContent(final IMemento container, final ILongOperationRunner opRunner) throws MementoContentManagerException 
	{ 		
		if (container == null)
		{
			throw new NullPointerException();
		}
				
		final Collection<DestinationItemPair> content = new ArrayList<DestinationItemPair>();				
		
		final Collection<IMemento> mementoRegistry = new HashSet<IMemento>();				
		Collection<IMemento> mementoCollection = null; 
		
		final Collection<IMementoLoadProvider> brokenLoadProviderCollection = new ArrayList<IMementoLoadProvider> (); 
		
		for (IMementoLoadProvider loadProvider : loadProviderCollection)
		{
			mementoCollection = selectContentForLoadProvider(container, loadProvider);			
			verifyValidContentForLoadProvider(mementoRegistry, mementoCollection);
			
			mementoRegistry.addAll(mementoCollection); // update registry.
			
			try
			{
				createContentForLocalProvider(content, loadProvider, mementoCollection, opRunner);
			}
			catch (MementoLoadProviderException exc)
			{
				brokenLoadProviderCollection.add(loadProvider);
			}
		}
		
		if (! brokenLoadProviderCollection.isEmpty())
		{
			handleLoadStrategyBrokenContracts(brokenLoadProviderCollection);
		}
		
		return content;
	}
	
	/**
	 * Filters the <code>container</code> data for given <code>localProvider</code>. 
	 *
	 * @param container 
	 * @param loadProvider
	 * @return
	 */
	private Collection<IMemento> selectContentForLoadProvider(final IMemento container, 
															  final IMementoLoadProvider loadProvider)
	{	
		final Collection<IMemento> providerMementos = new ArrayList<IMemento>();		
			
		final IMemento[] mementos = container.getChildren(loadProvider.getChildType());		
		for (IMemento memento : mementos)
		{
			if (loadProvider.canLoad(memento))
			{
				providerMementos.add(memento);
			}
		}
		
		return providerMementos;		
	}
	
	/**
	 * Verify content about to be load that it will be loaded at most by single 
	 * load provider. 
	 * 
	 * @param mementoRegistry
	 * @param mementoList
	 * @throws MementoContentManagerException
	 * @return
	 */
	private boolean verifyValidContentForLoadProvider(final Collection<IMemento> mementoRegistry, 
			                                     final Collection<IMemento> mementoCollection) 
		throws MementoContentManagerException
	{
		final Collection<IMemento> collisionMementoCollection = new ArrayList<IMemento>(mementoCollection);		
		collisionMementoCollection.retainAll(mementoRegistry);
				
		if (collisionMementoCollection.size() != 0)
		{
			// Provides fail fast mechanism for validation.
			// Fail fast to prevent loading again the content. 
						
			return false;			
		}		
			
		final Collection<IMementoLoadProvider> collisionProviderCollection = 
			new ArrayList<IMementoLoadProvider>();		
				
		for (IMemento memento : mementoCollection)
		{
			for (IMementoLoadProvider loadProvider : loadProviderCollection)
			{
				if (loadProvider.canLoad(memento))
				{
					if (! collisionProviderCollection.contains(loadProvider))
					{
						collisionProviderCollection.add(loadProvider);
					}
				}				
			}
		}	
		
		assert collisionProviderCollection.size() <= 1 : "At most one load provider is permited.";  //$NON-NLS-1$
	
		final boolean validContent = (collisionProviderCollection.size() <= 1);		
		if (! validContent)
		{			
			handleLoadStrategyCollisions(collisionProviderCollection, collisionMementoCollection);			
		}
			
		return validContent;		
	}
	
	/**
	 * Handles load strategy collisions. 
	 * 
	 * @param collisionProviderCollection
	 * @param collisionMementoCollection
	 * @throws MementoContentManagerException
	 */
	private void handleLoadStrategyCollisions(final Collection<IMementoLoadProvider> collisionProviderCollection,
											  final Collection<IMemento> collisionMementoCollection)	
		throws MementoContentManagerException
	{
		final StringBuilder collisionMessage = new StringBuilder();	
		collisionMessage.append("Unexpected collisions loading items."); //$NON-NLS-1$
		collisionMessage.append("\n" + "Load providers causing collisions : "); //$NON-NLS-1$ //$NON-NLS-2$
		
		for (IMementoLoadProvider loadProvider : collisionProviderCollection)
		{
			collisionMessage.append("\n" + loadProvider.getDescriptor()); //$NON-NLS-1$
		}	
	
		collisionMessage.append("\n" + "Mementos part of the collisions : "); //$NON-NLS-1$ //$NON-NLS-2$
		
		for (IMemento memento : collisionMementoCollection)
		{
			collisionMessage.append("\n" + "Memento ID : " + memento.getID());  //$NON-NLS-1$ //$NON-NLS-2$
			collisionMessage.append(", Memento Type : " + memento.getType()); //$NON-NLS-1$			
		}
		
		logger.logError(collisionMessage.toString());
		
		// take action.

		throw new MementoContentManagerException(collisionMessage.toString());			
	}	
	
	/**
	 * Handles load strategy broken contracts. 
	 * 
	 * @param collisionProviderCollection
	 * @param collisionMementoCollection
	 * @throws MementoContentManagerException
	 */
	private void handleLoadStrategyBrokenContracts(final Collection<IMementoLoadProvider> brokenLoadProviderCollection)	
		throws MementoContentManagerException
	{
		final StringBuilder brokenContractMessage = new StringBuilder();
		brokenContractMessage.append("Broken load providers: "); //$NON-NLS-1$
	
		for (IMementoLoadProvider loadProvider : brokenLoadProviderCollection)
		{
			brokenContractMessage.append("\n" + loadProvider.getDescriptor()); //$NON-NLS-1$	
		}
		
		logger.logError(brokenContractMessage.toString());	
		
		// take action.
		
		this.loadProviderCollection.removeAll(brokenLoadProviderCollection);		
	}	
	
	
	/**
	 * Creates content for load provider.
	 */
	private void createContentForLocalProvider(final Collection<DestinationItemPair> content, 
											   final IMementoLoadProvider loadProvider, 
											   final Collection<IMemento> mementoCollection, final ILongOperationRunner opRunner) 
		throws MementoLoadProviderException		
	{
		for (IMemento memento : mementoCollection)
		{
			content.add(loadProvider.load(memento, opRunner));
		}		
	}	

	@Override
	public void saveContent(IMemento container, Collection<DestinationItemPair> collection, final ILongOperationRunner opRunner) throws MementoContentManagerException 
	{
		ContractChecker.nullCheckParam(container, "container"); //$NON-NLS-1$
		ContractChecker.nullCheckParam(collection, "collection"); //$NON-NLS-1$
				
		final Collection<IMementoStoreProvider> brokenStoreProviderCollection = new ArrayList<IMementoStoreProvider>(); 	
		IMementoStoreProvider storeProvider = null; 
				
		for (DestinationItemPair pair : collection)
		{
			storeProvider = selectStoreProviderForPair(pair);
			
			if ((storeProvider != null) && (pair != null))
			{
				try
				{					
					storeProvider.store(container, pair, opRunner);					
				}
				catch (MementoStoreProviderException exc)
				{
					brokenStoreProviderCollection.add(storeProvider);										
				}
			}
		}		
		
		if (! brokenStoreProviderCollection.isEmpty())
		{
			handleStoreStrategyBrokenContracts(brokenStoreProviderCollection);
		}		
	}
	
	/**
	 * Selects a store provider to save the the pair.  
	 * load provider. 
	 * 
	 * @param pair
	 * @return
	 * @throws MementoContentManagerException
	 */
	private IMementoStoreProvider selectStoreProviderForPair(DestinationItemPair pair) 
		throws MementoContentManagerException
	{		
		final Collection<IMementoStoreProvider> collisionProviderCollection = new ArrayList<IMementoStoreProvider>();		
				
		for (IMementoStoreProvider storeProvider : storeProviderCollection)
		{
			if (storeProvider.canStore(pair))
			{
				collisionProviderCollection.add(storeProvider);
			}
		}		
				
		verifyValidStoreProvidersForPair(collisionProviderCollection, pair);
				
		IMementoStoreProvider storeProvider = null;				
		if (collisionProviderCollection.iterator().hasNext())
		{
			storeProvider = collisionProviderCollection.iterator().next();			
		}
				
		return storeProvider;		
	}
	
	/**
	 * Verifies that at most one store provider can save the pair.
	 * 
	 * @param collisionProviderCollection
	 * @param pair
	 * @throws MementoContentManagerException
	 */
	private void verifyValidStoreProvidersForPair(final Collection<IMementoStoreProvider> collisionProviderCollection,
											   final DestinationItemPair pair)
		throws MementoContentManagerException
	{	
		if (collisionProviderCollection.size() > 1)
		{
			handleStoreStrategyCollisions(collisionProviderCollection, pair);			
		}				
	}
	
	/**
	 * Handles collisions for store strategy. 
	 *  
	 * @param collisionProviderCollection
	 * @param pair
	 * @throws MementoContentManagerException
	 */
	private void handleStoreStrategyCollisions(final Collection<IMementoStoreProvider> collisionProviderCollection,
											   final DestinationItemPair pair)	
		throws MementoContentManagerException
	{
		final StringBuilder collisionMessage = new StringBuilder();	
		collisionMessage.append("Unexpected collisions storing items."); //$NON-NLS-1$
		collisionMessage.append("\n" + "Store providers causing collisions : "); //$NON-NLS-1$ //$NON-NLS-2$
		
		for (IMementoStoreProvider storeProvider : collisionProviderCollection)
		{
			collisionMessage.append("\n" + storeProvider.getDescriptor()); //$NON-NLS-1$
		}	
	
		collisionMessage.append("\n" + "Pair for collision : " + pair); //$NON-NLS-1$ //$NON-NLS-2$
			
		logger.logError(collisionMessage.toString());
		
		// take action.

		throw new MementoContentManagerException(collisionMessage.toString());			
	}	
	
	/**
	 * Handles broken contracts for store strategy. 
	 *  
	 * @param brokenStoreProviderCollection
	 * @throws MementoContentManagerException
	 */
	private void handleStoreStrategyBrokenContracts(final Collection<IMementoStoreProvider> brokenStoreProviderCollection)	
		throws MementoContentManagerException
	{
		final StringBuilder brokenContractMessage = new StringBuilder();
		brokenContractMessage.append("Broken store providers: "); //$NON-NLS-1$
	
		for (IMementoStoreProvider storeProvider : brokenStoreProviderCollection)
		{
			brokenContractMessage.append("\n" + storeProvider.getDescriptor()); //$NON-NLS-1$	
		}
		
		logger.logError(brokenContractMessage.toString());	
		
		// take action.
		
		this.storeProviderCollection.removeAll(brokenStoreProviderCollection);		
	}		
}
